#!/bin/sh

# $1: working directory

basedir=$1

if [ ! -d $basedir ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

hash git 2>/dev/null || { echo  "git was not found; cannot pull" >&2; exit 1; }

cd "$basedir"

git pull

if [ ! $? -eq 0 ]; then
	exit 1
	echo "git pull failed" >&2
fi
