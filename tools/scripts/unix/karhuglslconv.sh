#!/bin/bash

# USAGE:
# ./karhuglslconv.sh <system[mac|linux]> <type[metal]> <suffixless-output> <full-inputs...>

if [ "$#" -lt "2" ]; then
	echo "You need to specify at least four arguments (the system, the output format (metal), one output (without suffix) and one or more inputs)!" >&2
	exit 1
fi

systemname=$1
outputformat=$2
outputname=$3

if [ "$systemname" != "mac" ] && [ "$systemname" != "linux" ]; then
	echo "An argument for the system ('mac' or 'linux') must be passed to this script!" >&2
	exit 1
fi

if [ "$outputformat" != "metal" ]; then
	echo "The output format (second argument) must be one of the following: metal" >&2
	exit 1
fi

inputnames="";
for i in ${@:4}; do
	if [ ! -f "$i" ]; then
		echo "Input file '$i' does not exist!" >&2
		exit 1
	fi

	inputnames+=" \"$i\""
done

toolpath="$KARHU_HOME/tools/$systemname"

if [ ! -d "$toolpath" ]; then
	echo "Unable to find tool path (have you defined \$KARHU_HOME?)!" >&2
	exit 1
fi

if [ ! -f "$toolpath/glslang/glslangValidator" ]; then
	echo "Unable to find glslangValidator in tool directory!" >&2
	exit 1
fi

if [ ! -f "$toolpath/spirv-cross" ]; then
	echo "Unable to find spirv-cross in tool directory!" >&2
	exit 1
fi

errormessage=$(eval "\"$toolpath/glslang/glslangValidator\"" $inputnames -G -o "\"$outputname.spv\"" --auto-map-locations)

if [ ! $? -eq 0 ]; then
	echo "glslangValidator failed to convert input(s) to SPIR-V:" >&2
	echo "$errormessage" >&2
	exit 1
fi

outputflag=""
outputsuffix=""

if [ "$outputformat" = "metal" ]; then
	outputflag="--msl"
	outputsuffix="metal"
fi

errormessage=$(eval "\"$toolpath/spirv-cross\"" "\"$outputname.spv\"" $outputflag --flatten-ubo --output "\"$outputname.$outputsuffix\"")

if [ ! $? -eq 0 ]; then
	echo "SPIRV-cross failed to convert SPIR-V to output!" >&2
	echo "$errormessage" >&2
	exit 1
fi

rm "$outputname.spv"

echo "$outputname.$outputsuffix"
