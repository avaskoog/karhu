#!/bin/sh

# $1: host
# $2: port

host=$1
port1=$2
port2=$(($port1+1))

cd ${0%/*}

if [ ! -d "$KARHU_HOME" ]; then
	echo "Invalid KARHU_HOME directory" >&2
	exit 1
fi

exepath="$KARHU_HOME/tools/websockify/websockify.py"

if [ ! -f "$exepath" ]; then
	echo "websockify not found at expected location '$exepath'" >&2
	exit 1
fi

hash python 2>/dev/null || { echo  "python was not found" >&2; exit 1; }

python "$exepath" "$host:$port2" "$host:$port1" --verbose --run-once

exit 0
