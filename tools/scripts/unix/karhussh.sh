#!/bin/sh

cd ${0%/*}

# $1:  working directory
# $2:  target system for actions
# $3:  target name for actions
# $4:  SSH address (user at host)
# $5:  configuration [debug/release]
# $6:  directory on remote machine with project
# $7:  arbitrary hint
# $8:  JSON make settings

# $9:  call clean action [y/n]
# $10:  call make action [y/n]
# $11:  call build action [y/n]
# $12:  call run action [y/n]
# $13: push before applying [y/n]
# $14: pull before applying [y/n]
# $15: update before applying [y/n]

# $16: karhu: call clean action [y/n]
# $17: karhu: call make action [y/n]
# $18: karhu: call build action [y/n]
# $19: karhu: push before applying [y/n]
# $20: karhu: pull before applying [y/n]
# $21: karhu: update before applying [y/n]

# $22: whether to capture run output [y/n]

basedir=$1
systemname=$2
targetname=$3
sshaddress=$4
configuration=$5
remotedir=$6
hint=$7
settings=$8

actclean=$9
actmake=${10}
actbuild=${11}
actrun=${12}
preactpush=${13}
preactpull=${14}
preactupdate=${15}

kactclean=${16}
kactmake=${17}
kactbuild=${18}
kpreactpush=${19}
kpreactpull=${20}
kpreactupdate=${21}

captureoutput=${22}

if [ ! -d $basedir ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

hash ssh 2>/dev/null || { echo  "ssh was not found" >&2; exit 1; }

cd "$basedir"

commandstring=$'#!/bin/sh\nexport DISPLAY=:0\n'

commandstring+="if [ ! -d \"$remotedir\" ]; then"
commandstring+=$'\n'
	commandstring+="echo \"Invalid project directory\" >&2"
	commandstring+=$'\n'
	commandstring+="exit 1"
	commandstring+=$'\n'
commandstring+="fi"
commandstring+=$'\n'

commandstring+="cd $remotedir"
commandstring+=$'\n'

# Check whether to push to repo first.
if [ $preactpush == "y" ]; then
	karhuproj sync --acts push

	if [ ! $? -eq 0 ]; then
		exit 1
	fi
fi

if [ $kpreactpush == "y" ]; then
	karhuproj sync --acts push --dir karhu

	if [ ! $? -eq 0 ]; then
		exit 1
	fi
fi

if [ $kactclean == "y" ] || [ $kactmake == "y" ] || [ $kactbuild == "y" ] || [ $kpreactpull == "y" ] || [ $kpreactupdate == "y" ]; then
	commandstring+="karhubuild apply --dir karhu --sys $systemname --conf $configuration"

	if [ "$hint" != "none" ]; then
		commandstring+=" --hint \"$hint\""
	fi

	if [ "$settings" != "none" ]; then
		commandstring+=" --settings \"$settings\""
	fi
	
	if [ $kactclean == "y" ] || [ $kactmake == "y" ] || [ $kactbuild == "y" ]; then
		commandstring+=" --acts"

		if [ $kactclean == "y" ]; then
			commandstring+=" clean"
		fi

		if [ $kactmake == "y" ]; then
			commandstring+=" make"
		fi

		if [ $kactbuild == "y" ]; then
			commandstring+=" build"
		fi
	fi

	if [ $kpreactpull == "y" ] || [ $kpreactupdate == "y" ]; then
		commandstring+=" --preacts"

		if [ $kpreactpull == "y" ]; then
			commandstring+=" pull"
		fi

		if [ $kpreactupdate == "y" ]; then
			commandstring+=" update"
		fi
	fi

	commandstring+=$'\n'
fi

# Check whether to pull from repo first.
if [ $preactpull == "y" ] || [ $preactupdate == "y" ]; then
	commandstring+="karhuproj sync --acts "

	if [ $preactpull == "y" ]; then
		commandstring+=" pull"
	fi

	if [ $preactupdate == "y" ]; then
		commandstring+=" update"
	fi

	commandstring+=$'\n'
	commandstring+="if [ ! $? -eq 0 ]; then"
	commandstring+=$'\n'
		commandstring+="exit 1"
		commandstring+=$'\n'
	commandstring+="fi"
	commandstring+=$'\n'
fi

# Check whether to clean.
if [ $actclean == "y" ]; then
	commandstring+="karhubuild clean --sys $systemname"

	if [ "$hint" != "none" ]; then
		commandstring+=" --hint \"$hint\""
	fi

	commandstring+=$'\n'
	commandstring+="if [ ! $? -eq 0 ]; then"
	commandstring+=$'\n'
		commandstring+="exit 1"
		commandstring+=$'\n'
	commandstring+="fi"
	commandstring+=$'\n'
fi

# Check whether to make.
if [ $actmake == "y" ]; then
	commandstring+="karhubuild make --sys $systemname --target $targetname"

	if [ "$hint" != "none" ]; then
		commandstring+=" --hint \"$hint\""
	fi

	if [ "$settings" != "none" ]; then
		commandstring+=" --settings \"$settings\""
	fi

	commandstring+=$'\n'
	commandstring+="if [ ! $? -eq 0 ]; then"
	commandstring+=$'\n'
		commandstring+="exit 1"
		commandstring+=$'\n'
	commandstring+="fi"
	commandstring+=$'\n'
fi

# Check whether to build.
if [ $actbuild == "y" ]; then
	commandstring+="karhubuild build --sys $systemname --conf $configuration --target $targetname"

	if [ "$hint" != "none" ]; then
		commandstring+=" --hint \"$hint\""
	fi

	commandstring+=$'\n'
	commandstring+="if [ ! $? -eq 0 ]; then"
	commandstring+=$'\n'
		commandstring+="exit 1"
		commandstring+=$'\n'
	commandstring+="fi"
	commandstring+=$'\n'
fi

# Check whether to run.
if [ $actrun == "y" ]; then
	commandstring+="eval karhubuild run --sys $systemname --conf $configuration"

	if [ "$hint" != "none" ]; then
		commandstring+=" --hint \"$hint\""
	fi

	if [ $captureoutput == "y" ]; then
		commandstring+=" --capture"
	fi

	if [ "$#" -gt "22" ]; then
		commandstring+=" --args"

		for i in "${@:23}"; do
			commandstring+=" \"$i\""
		done
	fi

	commandstring+=$'\n'
	commandstring+="if [ ! $? -eq 0 ]; then"
	commandstring+=$'\n'
		commandstring+="exit 1"
		commandstring+=$'\n'
	commandstring+="fi"
	commandstring+=$'\n'
fi

commandstring+="exit 0"

echo "$commandstring" | ssh -o "StrictHostKeyChecking no" "$sshaddress"

if [ ! $? -eq 0 ]; then
	echo "SSH error" >&2
	exit 1
fi

exit 0
