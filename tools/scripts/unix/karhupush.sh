#!/bin/sh

# $1: working directory
# $2: commit message

basedir=$1
commitmessage=$2

if [ ! -d $basedir ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

hash git 2>/dev/null || { echo  "git was not found; cannot push" >&2; exit 1; }

cd "$basedir"

git add -A
git commit -m "$commitmessage"
git push -u origin master

if [ ! $? -eq 0 ]; then
	exit 1
	echo "git push failed" >&2
fi
