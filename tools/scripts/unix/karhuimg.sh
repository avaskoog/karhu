#!/bin/bash

# $1: working directory
# $2: output platform
# $3: output type [icon/splash]
# $4: path to templates.karhutarget.json

basedir=$1
platform=$2
outputtype=$3
templatepath=$4

# We need ImageMagick convert to work on images.
hash convert 2>/dev/null || { echo  "convert was not found" >&2; exit 1; }
hash identify 2>/dev/null || { echo  "identify was not found" >&2; exit 1; }

# All the image names and dimensions, comma-separated as NAME,W,H.
infos=$(karhudata json --act format --file "$templatepath" --data object "$outputtype" sizes --separators , ,)

# Comma-separated list of necessary subdirectories.
subdirs=$(karhudata json --act format --file "$templatepath" --data array "$outputtype" subdirs --separators , ,)

# Can we use the mask on this target platform?
maskable=$(karhudata json --act format --file "$templatepath" --data boolean "$outputtype" maskable)

outputpath="$basedir/generated/$platform"
cd "$outputpath"

if [ ! -d "$outputtype" ]; then
	mkdir "$outputtype"
fi

outputpath="$outputpath/$outputtype"
cd "$outputpath"

if [ "" != "$subdirs" ]; then
	# Split all the names and dimensions into an array.
	IFS="," read -r -a subdirarr <<< "$subdirs"
	for i in "${subdirarr[@]}"; do
		if [ ! -d "$i" ]; then
	    	mkdir "$i"
	    fi
	done
fi

cd "$basedir/style"

if [ "" != "$infos" ]; then
	# Get the per-project icon/splash settings.
	bgcol=$(karhudata json --act format --file style.karhuproj.json --data array "$outputtype" bgcol --separators ,)
	bgimg=$(karhudata json --act format --file style.karhuproj.json --data string "$outputtype" bgimg)
	fgimg=$(karhudata json --act format --file style.karhuproj.json --data string "$outputtype" fgimg)
	fgsize=$(karhudata json --act format --file style.karhuproj.json --data float "$outputtype" fgsize)
	maskimg=$(karhudata json --act format --file style.karhuproj.json --data string "$outputtype" maskimg)
	fgimgsize="0x0"

	# Disable the mask if not permitted.
	if [ "false" = "$maskable" ]; then
		maskimg=""
	fi

	# Validate background.
	if [ "" != "$bgimg" ]; then
		if [ ! -f "./$bgimg" ]; then
	    	echo "Icon bgimg '$bgimg' not found!"
	    	exit 1
		fi
	elif [ "" = "$bgcol" ]; then
		echo "No background colour or image found; defaulting to [255,0,0]."
		bgcol="255,0,0"
	fi

	# Validate foreground.
	if [ "" != "$fgimg" ]; then
		if [ ! -f "./$fgimg" ]; then
	    	echo "Icon fgimg '$fgimg' not found!"
	    	exit 1
		fi

		fgimgsize=$(identify -format "%wx%h" "$fgimg")
	fi

	# Validate foreground scale.
	if [ "" = "$fgsize" ]; then
		echo "No foreground image size found; defaulting to 0.75."
		fgsize=0.75
	fi

	# Validate mask.
	if [ "" != "$maskimg" ]; then
		if [ ! -f "./$maskimg" ]; then
	    	echo "Icon maskimg '$maskimg' not found!"
	    	exit 1
		fi
	fi

	# Split all the names and dimensions into an array.
	IFS="," read -r -a infoarr <<< "$infos"
	infocount=${#infoarr[@]}
	stride=3
	infodiv=$(expr $infocount / $stride)

	outputstring=""
	for (( i=0; i < $infocount; i += $stride ))
	do
		# List the filenames for the user.
		outputstring+="\"$outputtype/${infoarr[$i]}\" "

		# Final foreground dimensions.
		fgbase=${infoarr[$i+1]}
		if [ "$fgbase" -gt "${infoarr[$i+2]}" ]; then
			fgbase=${infoarr[$i+2]}
		fi
echo "FGBASE = $fgbase"
echo "FGSIZE = $fgsize"
		fgdim=$(echo "$fgbase*$fgsize" | bc)
echo "FGDIM1 = $fgdim"
		fgdim=$(LC_NUMERIC="en_US.UTF-8" printf "%.0f" "$fgdim")
echo "FGDIM2 = $fgdim"
		# Final mask dimensions.
		maskwidth=$(expr ${infoarr[$i+1]} - 2)
		maskheight=$(expr ${infoarr[$i+1]} - 2)

		# Start building the ImageMagick command.		
		cmdstr="convert "

		if [ "" != "$maskimg" ]; then
			cmdstr+="\\( -resize ${maskwidth}x$maskheight -extent ${infoarr[$i+1]}x${infoarr[$i+2]} -gravity center -background \"rgba(0,0,0,0)\" \"$maskimg\" \\) "
		fi

		if [ "" != "$fgimg" ] && [ "" != "$maskimg" ]; then
			cmdstr+="\\( "
		fi

		if [ "" != "$bgimg" ]; then
			cmdstr+="\\( -resize ${infoarr[$i+1]}x${infoarr[$i+2]} -gravity center \"$bgimg\" \\) "
		else
			cmdstr+="\\( -size ${infoarr[$i+1]}x${infoarr[$i+2]} xc:\"rgba($bgcol)\" \\) "
		fi

		if [ "" != "$fgimg" ]; then
			cmdstr+="\\( -size ${fgimgsize} -background \"rgba(0,0,0,0)\" -compose src \"$fgimg\" -trim -resize ${fgdim}x$fgdim -extent ${infoarr[$i+1]}x${infoarr[$i+2]} -gravity center \\) "
			cmdstr+="-compose src-over -composite "
		fi

		if [ "" != "$fgimg" ] && [ "" != "$maskimg" ]; then
			cmdstr+=" \\) "
		fi

		if [ "" != "$maskimg" ]; then
			cmdstr+="-channel RGBA -compose multiply -composite "
		fi

		cmdstr+="\"$outputpath/${infoarr[$i]}\""
echo $cmdstr
		# Run the command!
		eval $cmdstr
	done

	echo "$outputstring"
fi

cd $basedir
