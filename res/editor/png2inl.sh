#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"
cd png

d="../../../src/karhu/app/editor/inl"
all="$d/textures.inl"
tmp="img"

if [ -f "$all" ]; then
	rm "$all"
fi

touch "$all"

for i in *.png; do
	n="${i%.*}"
	f="$n.inl"
	o="$d/$f"
	echo "$o"
	mv "$i" "$tmp"
	xxd -i "$tmp" > "$o"
	mv "$tmp" "$i"
	cat "../template.inl" >> "$all"
	sed -i "" "s/\\\$NAME\\\$/$n/g" "$all"
done

echo "$all"
