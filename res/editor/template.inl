// $NAME$

{
	#include "$NAME$.inl"

	Bufferview<Byte> buffer
	{
		reinterpret_cast<Byte *>(&img[0]),
		img_len
	};

	auto texture(app.gfx().createTexture());

	if (!texture)
	{
		log::err("Karhu")
			<< "Failed to initialise editor: could not initialise texture for icon '"
			<< "$NAME$"
			<< '\'';
		
		return false;
	}

	gfx::Pixelbuffer pixels
	(
		gfx::loadPixelsFromFileInMemory(buffer)
	);
		
	if (!pixels.components)
	{
		log::err("Karhu")
			<< "Failed to initialise editor: could not load pixels for icon '"
			<< "$NAME$"
			<< '\'';
		
		return false;
	}

	texture->setFromBuffer(std::move(pixels));
			
	if (!texture->bake())
		log::err("Karhu")
			<< "Failed to initialise editor: could not bake texture for icon '"
			<< "$NAME$"
			<< '\'';

	m_textures.emplace
	(
		"$NAME$",
		std::move(texture)
	);
}			

