/**
 * @author		Ava Skoog
 * @date		2017-07-15
 * @copyright   2017-2023 Ava Skoog
 */

//! @todo: Currently JSON unicode escape sequences have only been tested, and are only written during serialisation as, UTF-32; it may not work with other widths, and will not serialise to other widths, and so might not be compatible with other JSON libraries in this sense, while it most certainly is compatible with itself. There's always the option to disable escaping Unicode entirely during serialisation.
//! @todo: C++14 and the STL have not updated string related facilities such as streams to work fully with char16_t and char32_t, and so the parser will currently not compile if the template argument for character type is set to any of these; use char or wchar_t for now. This is really on the standard committee to fix rather than this library. Should they fix this, however, this JSON library as it has been written should work as is right away.
//! @todo: Fix all the array prepends/inserts.

/*
 * This is a C++14 JSON interface and parser.
 *
 * It can be easily extracted for external use, as the only
 * dependencies are the STL and the following two files,
 * themselves dependent only on the STL.
 *
 *  -- karhu/core/Nullable.hpp
 *  -- karhu/core/string.hpp
 *
 * The Context template class is the main entry point
 * to the JSON interface.
 *
 * A context is created using template arguments to
 * specify what character width to work with, and what
 * precision and size of integer and floating point
 * values to use as well as whether or not to escape
 * Unicode characters when serialising data (escaped
 * Unicode characters will still be understood when
 * parsing JSON input string).
 *
 * The context then serves as a the JSON
 * namespace for types, so that the template arguments
 * do not have to be manually specified again.
 *
 * JSON objects can be assembled manually with a syntax
 * based on initialiser list that mimics actual JSON
 * syntax as closely as possible; most types can be
 * deduced, but objects and arrays have to be explicitly
 * disambiguated using the Obj and Arr types of the context.
 *
 * JSON objects can also be parsed from a JSON string.
 * Values can be serialised to compact strings or formatted.
 *
 * Example of assembling a JSON object in C++:
 *
 * -----
 * using J = karhu::JSON::Context
 * <
 *     char,
 *     int,
 *     float,
 *     karhu::JSON::Escape::withUnicode
 * >;
 *
 * J::Obj obj
 * {
 *     {"name here": "value here"},
 *
 *     {"int":    5},
 *     {"float":  4.05f},
 *     {"bool":   true},
 *     {"null",   nullptr},
 *     {"array":  J::Arr{100, 200, 300}},
 *     {"object": J::Obj
 *     {
 *         {"string": "hello"}
 *     }}
 * };
 * -----
 *
 * More pairs can be pushed dynamically:
 *
 * -----
 * J::Pair p{"name", "value"};
 * obj.push(p);
 *
 * obj.push({"without", "variable"});
 *
 * obj.push
 * ({
 *     {"initialiser list syntax", "is necessary"},
 *     {"for more than one value": "to be pushed"}
 * });
 * -----
 *
 * Values can be extracted using brackets. A pointer is
 * returned, which is null if the value does not exist.
 *
 * -----
 * J::Val *val{obj["array"]};
 * -----
 *
 * This returns a Value instance, of which the underlying
 * type must be queried and then extracted using an accessor;
 * the accessor returns null if it does not match the type.
 *
 * -----
 * if (val->is(karhu::JSON::TypeValue::array)
 *     J::Arr *arr{val->array()};
 * -----
 *
 * Objects and arrays can be traversed with foreach loops, in
 * which case they act like STL maps and vectors, respectively.
 *
 * -----
 * for (auto &i : obj)
 * {
 *     const J::Str &name {i.first};
 *     const J::Val &value{i.second};
 * }
 *
 * for (auto &i : arr)
 *     const J::Val &value{i};
 * -----
 *
 * Parsing is done by using an instance of the context's
 * Parser type, and the parse() method of this instace,
 * which returns a nullable wrapper which contains a valid
 * value in case of success. In case of failure, an error
 * can be retrieved using the error() method.
 *
 * -----
 * J::Parser parser;
 * if (karhu::Nullable<J::Val> val = parser.parse(string))
 *     karhu::JSON::TypeValue type{val->type()};
 * else
 *     J::Str err{parser.error()};
 * -----
 */

#ifndef KARHU_DATA_JSON_H_
	#define KARHU_DATA_JSON_H_

	#include <karhu/core/Nullable.hpp>
	#include <karhu/core/string.hpp>

	#include <string>
	#include <sstream>
	#include <iomanip>
	#include <memory>
	#include <array>
	#include <vector>
	#include <map>
	#include <cmath>
	#include <limits>

	namespace karhu
	{
		namespace JSON
		{
			/**
			 * Represents a valid JSON value type.
			 */
			enum class TypeValue : std::uint8_t
			{
				null    = 1 << 0,
				object  = 1 << 1,
				array   = 1 << 2,
				boolean = 1 << 3,
				string  = 1 << 4,
				number  = 1 << 5
			};
						
			/**
			 * Converts a value type to a compile-time string.
			 *
			 * @param t The type to convert.
			 *
			 * @return The string representation of the type.
			 */
			inline constexpr auto typeValueToString(TypeValue t) noexcept
			{
				switch (t)
				{
					case TypeValue::null:    return "null";
					case TypeValue::object:  return "object";
					case TypeValue::array:   return "array";
					case TypeValue::number:  return "number";
					case TypeValue::string:  return "string";
					case TypeValue::boolean: return "boolean";
				}
			}
			
			inline Nullable<TypeValue> typeValueFromString(const std::string &s) noexcept
			{
				if      (s == "null")    return {true, TypeValue::null};
				else if (s == "object")  return {true, TypeValue::object};
				else if (s == "array")   return {true, TypeValue::array};
				else if (s == "number")  return {true, TypeValue::number};
				else if (s == "string")  return {true, TypeValue::string};
				else if (s == "boolean") return {true, TypeValue::boolean};

				return {};
			}
			
			/**
			 * Represents the type of number stored in a JSON number object.
			 */
			enum class TypeNumber : std::uint8_t
			{
				integer  = 1 << 0,
				floating = 1 << 1
			};
			
			/**
			 * Converts a number type to a compile-time string.
			 *
			 * @param t The type to convert.
			 *
			 * @return The string representation of the type.
			 */
			inline constexpr auto typeNumberToString(TypeNumber t) noexcept
			{
				switch (t)
				{
					case TypeNumber::integer:  return "integer";
					case TypeNumber::floating: return "float";
				}
			}

			inline Nullable<TypeNumber> typeNumberFromString(const std::string &s) noexcept
			{
				if      (s == "integer") return {true, TypeNumber::integer};
				else if (s == "float")   return {true, TypeNumber::floating};

				return {};
			}
		}
	}
	
	// Bitwise operations for the value type bitflags.

	inline constexpr karhu::JSON::TypeValue operator|
	(
		const karhu::JSON::TypeValue &a,
		const karhu::JSON::TypeValue &b
	)
	{
		using T = std::underlying_type_t<karhu::JSON::TypeValue>;
		return static_cast<karhu::JSON::TypeValue>(static_cast<T>(a) | static_cast<T>(b));
	}
	
	inline constexpr karhu::JSON::TypeValue operator&
	(
		const karhu::JSON::TypeValue &a,
		const karhu::JSON::TypeValue &b
	)
	{
		using T = std::underlying_type_t<karhu::JSON::TypeValue>;
		return static_cast<karhu::JSON::TypeValue>(static_cast<T>(a) & static_cast<T>(b));
	}
	
	inline constexpr karhu::JSON::TypeValue &operator|=
	(
		karhu::JSON::TypeValue &a,
		const karhu::JSON::TypeValue &b
	)
	{
		return a = a | b;
	}
	
	inline constexpr karhu::JSON::TypeValue &operator&=
	(
		karhu::JSON::TypeValue &a,
		const karhu::JSON::TypeValue &b
	)
	{
		return a = a & b;
	}
	
	// Bitwise operations for the number type bitflags.

	inline constexpr karhu::JSON::TypeNumber operator|
	(
		const karhu::JSON::TypeNumber &a,
		const karhu::JSON::TypeNumber &b
	)
	{
		using T = std::underlying_type_t<karhu::JSON::TypeNumber>;
		return static_cast<karhu::JSON::TypeNumber>(static_cast<T>(a) | static_cast<T>(b));
	}
	
	inline constexpr karhu::JSON::TypeNumber operator&
	(
		const karhu::JSON::TypeNumber &a,
		const karhu::JSON::TypeNumber &b
	)
	{
		using T = std::underlying_type_t<karhu::JSON::TypeNumber>;
		return static_cast<karhu::JSON::TypeNumber>(static_cast<T>(a) & static_cast<T>(b));
	}
	
	inline constexpr karhu::JSON::TypeNumber &operator|=
	(
		karhu::JSON::TypeNumber &a,
		const karhu::JSON::TypeNumber &b
	)
	{
		return a = a | b;
	}
	
	inline constexpr karhu::JSON::TypeNumber &operator&=
	(
		karhu::JSON::TypeNumber &a,
		const karhu::JSON::TypeNumber &b
	)
	{
		return a = a & b;
	}
	
	namespace karhu
	{
		namespace JSON
		{
			/**
			 * A JSON string.
			 */
			template<typename TChar>
			using String = std::basic_string<TChar>;
			
			enum class Escape : std::uint8_t
			{
				withoutUnicode,
				withUnicode
			};
			
			/**
			 * Escapes a string according to JSON rules.
			 * Does not escape Unicode characters.
			 *
			 * @param s The string to escape.
			 *
			 * @return The escaped string.
			 */
			template<typename TChar>
			inline String<TChar> escapeWithoutUnicode(const String<TChar> &s)
			{
				//! @todo: Clean up Unicode escaping depending on TChar.
				string::Stream<TChar> r;
				
				for (auto &c : s)
				{
					// Single-character sequence?
					if      (c == '\b') r << "\\b";
					else if (c == '\f') r << "\\f";
					else if (c == '\n') r << "\\n";
					else if (c == '\r') r << "\\r";
					else if (c == '\t') r << "\\t";
					else if (c == '"')  r << "\\\"";
					else if (c == '\\') r << "\\\\";
					else                r << c;
				}

				return r.str();
			}

			/**
			 * Escapes a string according to JSON rules,
			 * including Unicode characters.
			 *
			 * @param s The string to escape.
			 *
			 * @return The escaped string.
			 */
			template<typename TChar>
			inline String<TChar> escapeWithUnicode(const String<TChar> &s)
			{
				std::wstringstream r;
				const auto w(string::convert<wchar_t>(s));

				for (auto &c : w)
				{
					// Unicode?
					if (static_cast<wchar_t>(c) > 127)
					{
						std::wstringstream converter;
						converter << std::hex << std::setfill(L'0') << std::setw(4) << static_cast<unsigned short>(c);
						r << L"\\u" << converter.str();
					}
					// Single-character sequence?
					else if (c == '\b') r << "\\b";
					else if (c == '\f') r << "\\f";
					else if (c == '\n') r << "\\n";
					else if (c == '\r') r << "\\r";
					else if (c == '\t') r << "\\t";
					else if (c == '"')  r << "\\\"";
					else if (c == '\\') r << "\\\\";
					else                r << c;
				}

				return string::convert<TChar>(r.str());
			}

			/**
			 * Escapes a string according to JSON rules,
			 * according to the specified setting.
			 *
			 * @param s       The string to escape.
			 * @param setting The escape setting.
			 *
			 * @return The escaped string.
			 */
			template<typename TChar>
			inline String<TChar> escape(const String<TChar> &s, const Escape setting)
			{
				return (Escape::withUnicode == setting) ? escapeWithUnicode<TChar>(s) : escapeWithoutUnicode<TChar>(s);
			}
			
			/**
			 * A JSON boolean.
			 */
			using Boolean = bool;

			/**
			 * A JSON number which holds an integer or a float.
			 */
			template<typename TChar, typename TInt, typename TFloat, Escape Escaping>
			class Number
			{
				private:
					using TNumber = Number<TChar, TInt, TFloat, Escaping>;
					using TString = String<TChar>;

				public:
					Number(const TNumber &) = default;
					Number(TNumber &&) = default;
				
					TNumber &operator=(const TNumber &) = default;
					TNumber &operator=(TNumber &&) = default;

					/**
					 * Initialises a number from an integer, a float, or an enum.
					 *
					 * @param v The value to store.
					 */
					template<typename T, typename = typename std::enable_if_t<(std::is_integral<T>{} || std::is_floating_point<T>{} || std::is_enum<T>{})>>
					Number(T const v) { setValue(v); }
				
					/**
					 * Compares the number to another.
					 *
					 * @param o The number to compare against.
					 *
					 * @return Whether the numbers are equal.
					 */
					bool operator==(const Number &o) const
					{
						if (m_type != o.m_type) return false;
						
						switch (m_type)
						{
							case TypeNumber::integer:  return (m_data.i == o.m_data.i);
							
							// Compares the serialised values rather than the actual numbers
							// as the latter can be done manually anyway, and the string
							// comparison is more suitable to serialising differences, since
							// the exact runtime representation may differ from the serialised
							// value (with a limited number of digits after the decimal point).
							case TypeNumber::floating:
							{
								TFloat f[2];
								TFloat const *v[2]{&m_data.f, &o.m_data.f};
								std::stringstream s[2];
								
								for (std::size_t i{0}; i < 2; ++ i)
								{
									s[i] << *(v[i]);
									
									std::modf(*(v[i]), &f[i]);
									if (std::abs(*(v[i]) - f[i]) <= std::numeric_limits<TFloat>::epsilon())
										s[i] << ".0";
								}
								
								return (s[0].str() == s[1].str());
							}
						}
					}

					/**
					 * Negative compares the number to another.
					 *
					 * @param o The number to compare against.
					 *
					 * @return Whether the numbers are not equal.
					 */
					bool operator!=(const Number &o) const
					{
						return !operator==(o);
					}

					/**
					 * Returns whether the number if of the specified type (integer or float).
					 *
					 * @param t The type to check.
					 *
					 * @return Whether the type matches that of the number.
					 */
					bool is(const TypeNumber t) const noexcept { return (t == m_type); }
				
					/**
					 * Returns the type of the number (integer or float).
					 *
					 * @return The underlying number type.
					 */
					TypeNumber type() const noexcept { return m_type; }
				
					TString typedump() const
					{
						return string::convert<TChar>(typeNumberToString(m_type));
					}

					/**
					 * Returns the number as a (pointer to an) integer if it is one, or null.
					 *
					 * @return The underlying integer, if there is one, or null.
					 */
					TInt *integer() noexcept { return (is(TypeNumber::integer)) ? &m_data.i : nullptr; }
				
					/**
					 * Returns the number as a (pointer to a) float if it is one, or null.
					 *
					 * @return The underlying float, if there is one, or null.
					 */
					TFloat *floating() noexcept { return (is(TypeNumber::floating)) ? &m_data.f : nullptr; }

					/**
					 * Returns the number as a (pointer to an) integer if it is one, or null.
					 *
					 * @return The underlying integer, if there is one, or null.
					 */
					const TInt *integer() const noexcept { return (is(TypeNumber::integer)) ? &m_data.i : nullptr; }
				
					/**
					 * Returns the number as a (pointer to a) float if it is one, or null.
					 *
					 * @return The underlying float, if there is one, or null.
					 */
					const TFloat *floating() const noexcept { return (is(TypeNumber::floating)) ? &m_data.f : nullptr; }

				private:
					template<typename T>
					std::enable_if_t<(std::is_integral<T>{} || std::is_enum<T>{}), void> setValue(T const v)
					{
						m_type = TypeNumber::integer;
						m_data.i = static_cast<TInt>(v);
					}

					template<typename T>
					std::enable_if_t<std::is_floating_point<T>{}, void> setValue(T const v)
					{
						m_type = TypeNumber::floating;
						m_data.f = static_cast<TFloat>(v);
					}

				private:
					TypeNumber m_type;
					union
					{
						TInt   i;
						TFloat f;
					} m_data;
			};
			
			template<typename TChar, typename TInt, typename TFloat, Escape Escaping> class Object;
			template<typename TChar, typename TInt, typename TFloat, Escape Escaping> class Array;

			// Convenience fake functions for conversion.
			template<typename T>
			struct type { constexpr operator TypeValue() const noexcept { return TypeValue::null; } };
			
			template<typename TChar, typename TInt, typename TFloat, Escape Escaping>
			struct type<Object<TChar, TInt, TFloat, Escaping>> { constexpr operator TypeValue() const noexcept { return TypeValue::object; } };
			
			template<typename TChar, typename TInt, typename TFloat, Escape Escaping>
			struct type<Array<TChar, TInt, TFloat, Escaping>> { constexpr operator TypeValue() const noexcept { return TypeValue::array; } };
			
			template<typename TChar, typename TInt, typename TFloat, Escape Escaping>
			struct type<Number<TChar, TInt, TFloat, Escaping>> { constexpr operator TypeValue() const noexcept { return TypeValue::number; } };
			
			template<typename TChar>
			struct type<String<TChar>> { constexpr operator TypeValue() const noexcept { return TypeValue::string; } };
			
			template<>
			struct type<Boolean> { constexpr operator TypeValue() const noexcept { return TypeValue::boolean; } };

			// Convenience fake function for validation.
			template<typename T>
			struct valid { constexpr operator bool() const noexcept { return (TypeValue::null != type<T>{}); } };

			// Defined down here so that the types used are known.
			/**
			 * Holds any kind of JSON value.
			 */
			template<typename TChar, typename TInt, typename TFloat, Escape Escaping>
			class Value
			{
				private:
					using TValue   = Value<TChar, TInt, TFloat, Escaping>;
					using TObject  = Object<TChar, TInt, TFloat, Escaping>;
					using TArray   = Array<TChar, TInt, TFloat, Escaping>;
					using TNumber  = Number<TChar, TInt, TFloat, Escaping>;
					using TString  = String<TChar>;
					using WString  = string::String<TChar>;
					using TBoolean = Boolean;

				public:
					// Undocumented construction and assignment.

					Value() = default;
					Value(const TValue &o) { *this = o; }
					Value(TValue &&) = default;

					TValue &operator=(const TValue &o)
					{
						switch ((m_type = o.m_type))
						{
							case TypeValue::null:    break;
							case TypeValue::object:  new (&m_data.object) std::unique_ptr<TObject>{std::make_unique<TObject>(*o.m_data.object)}; break;
							case TypeValue::array:   new (&m_data.array) std::unique_ptr<TArray>{std::make_unique<TArray>(*o.m_data.array)}; break;
							case TypeValue::string:  new (&m_data.string) TString{o.m_data.string}; break;
							case TypeValue::number:  m_data.number = o.m_data.number; break;
							case TypeValue::boolean: m_data.boolean = o.m_data.boolean; break;
						}
						
						return *this;
					}
				
					TValue &operator=(TValue &&) = default;
				
					// Documented construction and assignment.

					/**
					 * Sets the value to null.
					 */
					Value(std::nullptr_t) {}
				
					/**
					 * Constructs the value from an object.
					 *
					 * @param v The object to copy into the value.
					 */
					Value(const TObject &v) noexcept
					:
					m_type{TypeValue::object}
					{
						new (&m_data.object) std::unique_ptr<TObject>{std::make_unique<TObject>(v)};
					}

					/**
					 * Constructs the value from an object.
					 *
					 * @param v The object to move into the value.
					 */
					Value(const TObject &&v) noexcept
					:
					m_type{TypeValue::object}
					{
						new (&m_data.object) std::unique_ptr<TObject>{std::make_unique<TObject>(std::move(v))};
					}
					
					/**
					 * Constructs the value from an array.
					 *
					 * @param v The array to copy into the value.
					 */
					Value(const TArray &v) noexcept
					:
					m_type{TypeValue::array}
					{
						new (&m_data.array) std::unique_ptr<TArray>{std::make_unique<TArray>(v)};
					}
					
					/**
					 * Constructs the value from an array.
					 *
					 * @param v The array to move into the value.
					 */
					Value(const TArray &&v) noexcept
					:
					m_type{TypeValue::array}
					{
						new (&m_data.array) std::unique_ptr<TArray>{std::make_unique<TArray>(std::move(v))};
					}

					/**
					 * Constructs the value from a numeral.
					 *
					 * @param v The number to copy into the value.
					 */
					template<typename T, typename = typename std::enable_if_t<(std::is_integral<T>{} || std::is_floating_point<T>{} || std::is_enum<T>{})>>
					Value(T const v) { setValue(v); }
					
					/**
					 * Constructs the value from a string.
					 *
					 * @param v The string to copy into the value.
					 */
					Value(const WString &v) noexcept
					:
					m_type{TypeValue::string}
					{
						new (&m_data.string) TString{v};
					}
				
					/**
					 * Constructs the value from a string.
					 *
					 * @param v The string to move into the value.
					 */
					Value(const TString &&v) noexcept
					:
					m_type{TypeValue::string}
					{
						new (&m_data.string) TString{std::move(v)};
					}

					/**
					 * Constructs the value from a string.
					 *
					 * @param v The string to copy into the value.
					 */
					Value(const TString &v) noexcept
					:
					m_type{TypeValue::string}
					{
						new (&m_data.string) TString{v};
					}
				
					/**
					 * Constructs the value from a string.
					 *
					 * @param v The string to move into the value.
					 */
					Value(const WString &&v) noexcept
					:
					m_type{TypeValue::string}
					{
						new (&m_data.string) TString{std::move(v)};
					}
					
					/**
					 * Constructs the value from a string literal.
					 *
					 * @param v The string to copy into the value.
					 */
					template<typename T, typename = typename std::enable_if<string::validTypeChar<T>()>>
					Value(const T *v) noexcept
					:
					m_type{TypeValue::string}
					{
						new (&m_data.string) TString{string::convert<TChar>(v)};
					}

					/**
					 * Compares the value to another value.
					 *
					 * @param o The other value.
					 *
					 * @return Whether the two values are equal.
					 */
					bool operator==(const TValue &o) const
					{
						if (m_type != o.m_type) return false;

						switch (m_type)
						{
							case TypeValue::null:    return true;
							case TypeValue::object:  return (*m_data.object == *o.m_data.object);
							case TypeValue::array:   return (*m_data.array  == *o.m_data.array);
							case TypeValue::boolean: return (m_data.boolean == o.m_data.boolean);
							case TypeValue::string:  return (m_data.string  == o.m_data.string);
							case TypeValue::number:  return (m_data.number  == o.m_data.number);
						}
					}

					/**
					 * Negative compares the value to another value.
					 *
					 * @param o The other value.
					 *
					 * @return Whether the two values are not equal.
					 */
					bool operator!=(const TValue &o) const { return !operator==(o); }

					// Type checking.

					/**
					 * Returns whether this value is of the specified type.
					 *
					 * @param t The type to check against.
					 *
					 * @return Whether the value matches the type.
					 */
					bool is(const TypeValue t) const noexcept { return (t == m_type); }
				
					/**
					 * Returns the type of the value.
					 *
					 * @return The value type.
					 */
					TypeValue type() const noexcept { return m_type; }
				
					/**
					 * Returns the type of the value as a string.
					 *
					 * @return The value type as a string.
					 */
					TString typedump() const { return string::convert<TChar>(typeValueToString(m_type)); }

					// Accessors to underlying value.

					/**
					 * Returns the internal object if it is the underlying type, or null.
					 *
					 * @return The internal object or null.
					 */
					TObject *object() noexcept { return (is(TypeValue::object)) ? m_data.object.get() : nullptr; }
					
					/**
					 * Returns the internal array if it is the underlying type, or null.
					 *
					 * @return The internal array or null.
					 */
					TArray *array() noexcept { return (is(TypeValue::array)) ? m_data.array.get() : nullptr; }
					
					/**
					 * Returns the internal number if it is the underlying type, or null.
					 *
					 * Note that the number further needs to be queried for an integer
					 * or a floating point value; consider using integer() or floating()
					 * directly instead.
					 *
					 * @return The internal number or null.
					 */
					TNumber *number() noexcept { return (is(TypeValue::number)) ? &m_data.number : nullptr; }
					
					/**
					 * Returns the internal number's integer if it is the underlying type, or null.
					 * This is the actual integer and not a wrapper and can be used directly.
					 *
					 * @return The internal integer or null.
					 */
					TInt *integer() noexcept { if (auto n = number()) return n->integer(); return nullptr; }

					/**
					 * Returns the internal number's float if it is the underlying type, or null.
					 * This is the actual float and not a wrapper and can be used directly.
					 *
					 * @return The internal float or null.
					 */
					TFloat *floating() noexcept { if (auto n = number()) return n->floating(); return nullptr; }
					
					/**
					 * Returns the string object if it is the underlying type, or null.
					 * This is the actual string and not a wrapper and can be used directly.
					 *
					 * @return The internal string or null.
					 */
					TString *string() noexcept { return (is(TypeValue::string)) ? &m_data.string : nullptr; }
					
					/**
					 * Returns the internal boolean if it is the underlying type, or null.
					 * This is the actual boolean and not a wrapper and can be used directly.
					 *
					 * @return The internal boolean or null.
					 */
					TBoolean *boolean() noexcept { return (is(TypeValue::boolean)) ? &m_data.boolean : nullptr; }
					
					/**
					 * Returns the internal object if it is the underlying type, or null.
					 *
					 * @return The internal object or null.
					 */
					const TObject *object() const noexcept { return (is(TypeValue::object)) ? m_data.object.get() : nullptr; }
					
					/**
					 * Returns the internal array if it is the underlying type, or null.
					 *
					 * @return The internal array or null.
					 */
					const TArray *array() const noexcept { return (is(TypeValue::array)) ? m_data.array.get() : nullptr; }
					
					/**
					 * Returns the internal number if it is the underlying type, or null.
					 *
					 * Note that the number further needs to be queried for an integer
					 * or a floating point value; consider using integer() or floating()
					 * directly instead.
					 *
					 * @return The internal number or null.
					 */
					const TNumber *number() const noexcept { return (is(TypeValue::number)) ? &m_data.number : nullptr; }
					
					/**
					 * Returns the internal number's integer if it is the underlying type, or null.
					 * This is the actual integer and not a wrapper and can be used directly.
					 *
					 * @return The internal integer or null.
					 */
					const TInt *integer() const noexcept { if (auto n = number()) return n->integer(); return nullptr; }

					/**
					 * Returns the internal number's float if it is the underlying type, or null.
					 * This is the actual float and not a wrapper and can be used directly.
					 *
					 * @return The internal float or null.
					 */
					const TFloat *floating() const noexcept { if (auto n = number()) return n->floating(); return nullptr; }
					
					/**
					 * Returns the string object if it is the underlying type, or null.
					 * This is the actual string and not a wrapper and can be used directly.
					 *
					 * @return The internal string or null.
					 */
					const TString *string() const noexcept { return (is(TypeValue::string)) ? &m_data.string : nullptr; }
					
					/**
					 * Returns the internal boolean if it is the underlying type, or null.
					 * This is the actual boolean and not a wrapper and can be used directly.
					 *
					 * @return The internal boolean or null.
					 */
					const TBoolean *boolean() const noexcept { return (is(TypeValue::boolean)) ? &m_data.boolean : nullptr; }

					// Conversion.

					/**
					 * Returns a compact string representation of the
					 * JSON value and any nested values without any
					 * tabs, spaces or newlines.
					 *
					 * @param arrsep Optionally specifies the array value separator to be something other than comma.
					 * @param objsep Optionally specifies the object value separator to be something other than comma.
					 * @param stream Optionally sets whether all values should be output in a continuous stream without braces.
					 *
					 * @return The formatted string.
					 */
					TString serialise(const TChar *arrsep = nullptr, const TChar *objsep = nullptr, const bool stream = false) const
					{
						// There is some duplicate code between this and
						// dump(), but it's better than a lot of inefficient
						// conditionals as serialisation needs to be quick.

						string::Stream<TChar> s;

						switch (m_type)
						{
							// Single line values.
							case TypeValue::null:
							{
								s << "null";
								break;
							}

							case TypeValue::boolean:
							{
								s << ((m_data.boolean) ? "true" : "false");
								break;
							}

							case TypeValue::number:
							{
								if (auto n = integer())
									s << *n;
								else
								{
									s << *floating();
									
									// Make sure there is always a decimal point.
									TFloat i;
									std::modf(*floating(), &i);
									if (std::abs(*floating() - i) <= std::numeric_limits<TFloat>::epsilon())
										s << ".0";
								}
								
								break;
							}
							
							case TypeValue::string:
							{
								if (!stream) s << '"';
								s << escape<TChar>(m_data.string, Escaping);
								if (!stream) s << '"';
								break;
							}

							// Multiline values.
							case TypeValue::object:
							{
								if (!stream) s << '{';
								std::size_t j{0};
								for (auto &i : *m_data.object)
								{
									s << '"' << escape<TChar>(i.first, Escaping) << "\":";
									s << i.second.serialise(arrsep, objsep, stream);

									if (m_data.object->count() - 1 > (j ++))
									{
										if (objsep)
											s << objsep;
										else
											s << ',';
									}
								}
								if (!stream) s << '}';

								break;
							}

							case TypeValue::array:
							{
								if (!stream) s << '[';
								std::size_t j{0};
								for (auto &i : *m_data.array)
								{
									s << i.serialise(arrsep, objsep, stream);
									if (m_data.array->count() - 1 > (j ++))
									{
										if (arrsep)
											s << arrsep;
										else
											s << ',';
									}
								}
								if (!stream) s << ']';
								
								break;
							}
						}
						
						return s.str();
					}

					/**
					 * Returns a formatted string representation of the
					 * JSON value and any nested values with tabs, spaces
					 * and newlines.
					 *
					 * @param level The tab level to start at; zero if omitted.
					 *
					 * @return The formatted string.
					 */
					TString dump(const std::size_t level = 0) const
					{
						// There is some duplicate code between this and
						// serialise(), but it's better than a lot of inefficient
						// conditionals as serialisation needs to be quick.

						string::Stream<TChar> s;
						
						auto indent([&s, &level](const std::size_t &offset = 0)
						{
							for (std::size_t i{0}; i < level + offset; ++ i)
								s << '\t';
						});

						switch (m_type)
						{
							// Single line values.
							case TypeValue::null:
							{
								s << "null";
								break;
							}

							case TypeValue::boolean:
							{
								s << ((m_data.boolean) ? "true" : "false");
								break;
							}

							case TypeValue::number:
							{
								if (auto n = integer())
									s << *n;
								else
								{
									s << *floating();
									
									// Make sure there is always a decimal point.
									TFloat i;
									std::modf(*floating(), &i);
									if (std::abs(*floating() - i) <= std::numeric_limits<TFloat>::epsilon())
										s << ".0";
								}
								
								break;
							}
							
							case TypeValue::string:
							{
								s << '"' << escape<TChar>(m_data.string, Escaping) << '"';
								break;
							}

							// Multiline values.
							case TypeValue::object:
							{
								bool objFirst{false};
								for (auto &i : *m_data.object)
								{
									objFirst = (i.second.is(TypeValue::object) || i.second.is(TypeValue::array));
									break;
								}
								const bool m{m_data.object->count() > 1 || objFirst};

								if (level > 0 && m) s << '\n';

								if (m) indent();
								s << '{';
								if (m) s << '\n';
								std::size_t j{0};
								for (auto &i : *m_data.object)
								{
									if (m) indent(1);
									s << '"' << escape<TChar>(i.first, Escaping) << "\": ";
									s << i.second.dump(level + 1);

									if (m_data.object->count() - 1 > (j ++))
									{
										s << ',';
										s << '\n';
									}
								}
								if (m) s << '\n';
								if (m) indent();
								s << '}';

								break;
							}

							case TypeValue::array:
							{
								bool objFirst{false};
								for (auto &i : *m_data.array)
								{
									objFirst = (i.is(TypeValue::object) || i.is(TypeValue::array));
									break;
								}
								const bool m{m_data.array->count() > 1 || objFirst};

								if (level > 0 && m) s << '\n';

								if (m) indent();
								s << '[';
								if (m) s << '\n';
								std::size_t j{0};
								for (auto &i : *m_data.array)
								{
									if (m) indent(1);
									s << i.dump(level + 1);
									if (m_data.array->count() - 1 > (j ++))
									{
										s << ',';
										s << '\n';
									}
								}
								if (m) s << '\n';
								if (m) indent();
								s << ']';
								
								break;
							}
						}
						
						return s.str();
					}
					
					/**
					 * Returns the underlying value as a string.
					 *
					 * @param arrsep Optionally specifies the array value separator to be something other than comma.
					 * @param objsep Optionally specifies the object value separator to be something other than comma.
					 * @param stream Optionally sets whether all values should be output in a continuous stream without braces.
					 *
					 * @return The string.
					 */
					TString stringify(const TChar *arrsep = nullptr, const TChar *objsep = nullptr, const bool stream = false) const
					{
						string::Stream<TChar> s;

						switch (m_type)
						{
							// Single line values.
							case TypeValue::null:
							case TypeValue::boolean:
							case TypeValue::number:
							case TypeValue::object:
							case TypeValue::array:
								return serialise(arrsep, objsep, stream);

							case TypeValue::string:
								return m_data.string;
						}
						
						return s.str();
					}

					~Value()
					{
						if (is(TypeValue::object))
							m_data.object.~unique_ptr();
						else if (is(TypeValue::array))
							m_data.array.~unique_ptr();
						else if (is(TypeValue::string))
							m_data.string.~basic_string();
					}

				private:
					// Helper to set the value from a boolean.
					template<typename T>
					std::enable_if_t<std::is_same<T, TBoolean>{}, void> setValue(T const v)
					{
						m_type = TypeValue::boolean;
						m_data.boolean = v;
					}

					// Helper to set the value from an integer.
					template<typename T>
					std::enable_if_t<(!std::is_same<T, TBoolean>{} && (std::is_integral<T>{} || std::is_enum<T>{})), void> setValue(T const v)
					{
						m_type = TypeValue::number;
						m_data.number = static_cast<TInt>(v);
					}
					
					// Helper to set the value from an floating point number.
					template<typename T>
					std::enable_if_t<std::is_floating_point<T>{}, void> setValue(T const v)
					{
						m_type = TypeValue::number;
						m_data.number = static_cast<TFloat>(v);
					}

				private:
					TypeValue m_type{TypeValue::null};

					// Have to use pointers for object and
					// arrays due to circular dependencies.
					union Data
					{
						std::unique_ptr<TObject> object;
						std::unique_ptr<TArray>  array;
						TNumber                  number;
						TString                  string;
						TBoolean                 boolean;
						
						Data() {}
						~Data() {}
					} m_data;
			};

			/**
			 * A key and value JSON pair.
			 */
			template<typename TChar, typename TInt, typename TFloat, Escape Escaping>
			using Pair = std::pair<const String<TChar>, Value<TChar, TInt, TFloat, Escaping>>;


			// Base classes of array.
			// A little lengthy but prevents having to update in two places in case of changes.
			template<typename TChar, typename TInt, typename TFloat, Escape Escaping>
			using ContainerObject = std::map<typename Pair<TChar, TInt, TFloat, Escaping>::first_type, typename Pair<TChar, TInt, TFloat, Escaping>::second_type>;

			/**
			 * A JSON object containing pairs.
			 *
			 * Can be stepped over using iterators or foreach,
			 * in which case it works just like std::map.
			 */
			template<typename TChar, typename TInt, typename TFloat, Escape Escaping>
			class Object : private ContainerObject<TChar, TInt, TFloat, Escaping>
			{
				private:
					using TContainer = ContainerObject<TChar, TInt, TFloat, Escaping>;

				public:
					// Again prevents having to update in multiple places in case of changes.
					using TPair   = typename TContainer::value_type;
					using TKey    = typename TContainer::key_type;
					using TValue  = typename TContainer::mapped_type;

					using TObject  = Object<TChar, TInt, TFloat, Escaping>;
					using TArray   = Array<TChar, TInt, TFloat, Escaping>;
					using TNumber  = Number<TChar, TInt, TFloat, Escaping>;
					using TString  = String<TChar>;
					using TBoolean = Boolean;
				
				public:
					Object() = default;
					Object(const TObject &) = default;
					Object(TObject &&) = default;
					
					Object &operator=(const TObject &o)
					{
						TContainer::clear();
						
						for (const auto &i : o)
							TContainer::emplace(i.first, i.second);
						
						return *this;
					}
				
					Object &operator=(TObject &&) = default;

					/**
					 * Initialises the container with a list of values.
					 */
					Object(std::initializer_list<TPair> v)
					{
						push(std::move(v));
					}

					/**
					 * Compares the object to another.
					 *
					 * @param o The other object.
					 *
					 * @return Whether the two objects are equal.
					 */
					bool operator==(const TObject &o) const
					{
						return (count() == o.count() && std::equal(begin(), end(), o.begin()));
					}

					/**
					 * Negative compares the object to another.
					 *
					 * @param o The other object.
					 *
					 * @return Whether the two objects are not equal.
					 */
					bool operator!=(const TObject &o) const
					{
						return !operator==(o);
					}

					// Borrowing these from the underlying container so that
					// instances can be looped through in foreach loops.
					using TContainer::begin;
					using TContainer::end;
					using TContainer::rbegin;
					using TContainer::rend;
					using TContainer::cbegin;
					using TContainer::cend;
					using TContainer::crbegin;
					using TContainer::crend;

					using TContainer::erase;
				
					/**
					 * Copies a single value into the container.
					 * The reference returned may be invalidated later, so do not hold on to it.
					 *
					 * @param v The value to copy.
					 *
					 * @return A reference to the inserted value.
					 */
					TValue &push(const TPair &v) { return TContainer::emplace(v.first, v.second).first->second; }

					/**
					 * Moves a single value into the container.
					 * The reference returned may be invalidated later, so do not hold on to it.
					 *
					 * @param v The value to move.
					 *
					 * @return A reference to the inserted value.
					 */
					TValue &push(TPair &&v) { return TContainer::emplace(std::move(v.first), std::move(v.second)).first->second; }

					/**
					 * Inserts values from an initialiser list into the container.
					 *
					 * @param v The list of values to insert.
					 */
					void push(std::initializer_list<TPair> &&v)
					{
						for (auto &i : v)
							TContainer::emplace(std::move(i.first), std::move(i.second));
					}

					/**
					 * Copy pushes a value if it does not exist or overwrites it.
					 * The reference returned may be invalidated later, so do not hold on to it.
					 *
					 * @param k The key of the value to set.
					 * @param v The value to copy.
					 *
					 * @return A reference to the inserted value.
					 */
					TValue &set(const TKey &k, const TValue &v)
					{
						const auto i(TContainer::find(k));
						if (i != TContainer::end())
						{
							i->second = v;
							return i->second;
						}
						else
							return push({k, v});
					}
				
					/**
					 * Move pushes a value if it does not exist or overwrites it.
					 * The reference returned may be invalidated later, so do not hold on to it.
					 *
					 * @param k The key of the value to set.
					 * @param v The value to move.
					 *
					 * @return A reference to the inserted value.
					 */
					TValue &set(const TKey &k, TValue &&v)
					{
						const auto i(TContainer::find(k));
						if (i != TContainer::end())
						{
							i->second = std::move(v);
							return i->second;
						}
						else
							return push({k, std::move(v)});
					}

					/**
					 * Returns the value with the specified key if it exists, or null.
					 * The pointer may be invalidated later, so do not hold on to it.
					 *
					 * @param key The name of the value to find.
					 *
					 * @return A pointer to the value, if found, or null.
					 */
					TValue *get(const TKey &key) noexcept
					{
						const auto i(TContainer::find(key));
						if (i != TContainer::end())
							return &i->second;
						
						return nullptr;
					}

					//! @todo: Comment all of the specific getters.

					TObject *getObject(const TKey &key) noexcept
					{
						if (auto r = get(key))
							if (r->object())
								return r->object();

						return nullptr;
					}
				
					TArray *getArray(const TKey &key) noexcept
					{
						if (auto r = get(key))
							if (r->array())
								return r->array();

						return nullptr;
					}
				
					TString *getString(const TKey &key) noexcept
					{
						if (auto r = get(key))
							if (r->string())
								return r->string();

						return nullptr;
					}
				
					TNumber *getNumber(const TKey &key) noexcept
					{
						if (auto r = get(key))
							if (r->number())
								return r->number();

						return nullptr;
					}
				
					TInt *getInt(const TKey &key) noexcept
					{
						if (auto r = getNumber(key))
							if (r->integer())
								return r->integer();

						return nullptr;
					}

					TFloat *getFloat(const TKey &key) noexcept
					{
						if (auto r = getNumber(key))
							if (r->floating())
								return r->floating();

						return nullptr;
					}
				
					TBoolean *getBool(const TKey &key) noexcept
					{
						if (auto r = get(key))
							if (r->boolean())
								return r->boolean();

						return nullptr;
					}
					
					/**
					 * Returns the value with the specified key if it exists, or null.
					 * The pointer may be invalidated later, so do not hold on to it.
					 *
					 * @param key The name of the value to find.
					 *
					 * @return A pointer to the value, if found, or null (may be invalidated; do not hold on to it).
					 */
					const TValue *get(const TKey &key) const noexcept
					{
						const auto i(TContainer::find(key));
						if (i != TContainer::end())
							return &i->second;
						
						return nullptr;
					}
				
					//! @todo: Comment all of the specific getters.

					const TObject *getObject(const TKey &key) const noexcept
					{
						if (const auto r = get(key))
							if (r->object())
								return r->object();

						return nullptr;
					}
				
					const TArray *getArray(const TKey &key) const noexcept
					{
						if (const auto r = get(key))
							if (r->array())
								return r->array();

						return nullptr;
					}
				
					const TString *getString(const TKey &key) const noexcept
					{
						if (const auto r = get(key))
							if (r->string())
								return r->string();

						return nullptr;
					}
				
					const TNumber *getNumber(const TKey &key) const noexcept
					{
						if (const auto r = get(key))
							if (r->number())
								return r->number();

						return nullptr;
					}
				
					const TInt *getInt(const TKey &key) const noexcept
					{
						if (const auto r = getNumber(key))
							if (r->integer())
								return r->integer();

						return nullptr;
					}

					const TFloat *getFloat(const TKey &key) const noexcept
					{
						if (const auto r = getNumber(key))
							if (r->floating())
								return r->floating();

						return nullptr;
					}
				
					const TBoolean *getBool(const TKey &key) const noexcept
					{
						if (const auto r = get(key))
							if (r->boolean())
								return r->boolean();

						return nullptr;
					}

					/**
					 * Tries to find a value by recursively going through all of the
					 * keys specified in the vector, trying to find the next key in
					 * each successive nested object until reaching the final key.
					 *
					 * @param hierarchy A list of successive keys to recurse through.
					 *
					 * @return The corresponding value if found, or null.
					 */
					TValue *find(const std::vector<TString> &hierarchy) noexcept
					{
						if (hierarchy.size() == 0) return nullptr;

						auto v(get(hierarchy[0]));
						for (std::size_t i{1}; i < hierarchy.size(); ++ i)
						{
							if (!v || !v->object())
							 	return nullptr;

							v = v->object()->get(hierarchy[i]);
						}

						return v;
					}

					//! @todo: Comment all of the specific finders.

					TObject *findObject(const std::vector<TString> &hierarchy) noexcept
					{
						if (auto r = find(hierarchy))
							if (r->object())
								return r->object();

						return nullptr;
					}
				
					TArray *findArray(const std::vector<TString> &hierarchy) noexcept
					{
						if (auto r = find(hierarchy))
							if (r->array())
								return r->array();

						return nullptr;
					}
				
					TString *findString(const std::vector<TString> &hierarchy) noexcept
					{
						if (auto r = find(hierarchy))
							if (r->string())
								return r->string();

						return nullptr;
					}
				
					TNumber *findNumber(const std::vector<TString> &hierarchy) noexcept
					{
						if (auto r = find(hierarchy))
							if (r->number())
								return r->number();

						return nullptr;
					}
				
					TInt *findInt(const std::vector<TString> &hierarchy) noexcept
					{
						if (auto r = findNumber(hierarchy))
							if (r->integer())
								return r->integer();

						return nullptr;
					}

					TFloat *findFloat(const std::vector<TString> &hierarchy) noexcept
					{
						if (auto r = findNumber(hierarchy))
							if (r->floating())
								return r->floating();

						return nullptr;
					}
				
					TBoolean *findBool(const std::vector<TString> &hierarchy) noexcept
					{
						if (auto r = find(hierarchy))
							if (r->boolean())
								return r->boolean();

						return nullptr;
					}

					/**
					 * Tries to find a value by recursively going through all of the
					 * keys specified in the vector, trying to find the next key in
					 * each successive nested object until reaching the final key.
					 *
					 * @param hierarchy A list of successive keys to recurse through.
					 *
					 * @return The corresponding value if found, or null.
					 */
					const TValue *find(const std::vector<TString> &hierarchy) const noexcept
					{
						if (hierarchy.size() == 0) return nullptr;

						auto v(get(hierarchy[0]));

						for (std::size_t i{1}; i < hierarchy.size(); ++ i)
							if (!v || !v->object() || !(v = v->object()->get(hierarchy[i])))
								break;

						return v;
					}

					//! @todo: Comment all of the specific finders.

					const TObject *findObject(const std::vector<TString> &hierarchy) const noexcept
					{
						if (const auto r = find(hierarchy))
							if (r->object())
								return r->object();

						return nullptr;
					}
				
					const TArray *findArray(const std::vector<TString> &hierarchy) const noexcept
					{
						if (const auto r = find(hierarchy))
							if (r->array())
								return r->array();

						return nullptr;
					}
				
					const TString *findString(const std::vector<TString> &hierarchy) const noexcept
					{
						if (const auto r = find(hierarchy))
							if (r->string())
								return r->string();

						return nullptr;
					}
				
					const TNumber *findNumber(const std::vector<TString> &hierarchy) const noexcept
					{
						if (const auto r = find(hierarchy))
							if (r->number())
								return r->number();

						return nullptr;
					}
				
					const TInt *findInt(const std::vector<TString> &hierarchy) const noexcept
					{
						if (const auto r = findNumber(hierarchy))
							if (r->integer())
								return r->integer();

						return nullptr;
					}

					const TFloat *findFloat(const std::vector<TString> &hierarchy) const noexcept
					{
						if (const auto r = findNumber(hierarchy))
							if (r->floating())
								return r->floating();

						return nullptr;
					}
				
					const TBoolean *findBool(const std::vector<TString> &hierarchy) const noexcept
					{
						if (const auto r = find(hierarchy))
							if (r->boolean())
								return r->boolean();

						return nullptr;
					}
				
					/**
					 * Removes the pair by the specified key if it exists.
					 *
					 * @param key The name of the pair to remove.
					 *
					 * @return Whether anything was removed.
					 */
					bool remove(const TKey &key)
					{
						const auto i(TContainer::find(key));
						if (i != TContainer::end())
						{
							TContainer::erase(i);
							return true;
						}
						
						return false;
					}
				
					/**
					 * Removes all children.
					 *
					 * @return The updated object.
					 */
					TObject &clear() { TContainer::clear(); return *this; }
				
					/**
					 * Returns the number of pairs in the object.
					 *
					 * @return The pair count.
					 */
					std::size_t count() const noexcept { return TContainer::size(); }
			};

			// Base classes of array.
			template<typename TChar, typename TInt, typename TFloat, Escape Escaping>
			using ContainerArray = std::vector<Value<TChar, TInt, TFloat, Escaping>>;

			/**
			 * A JSON array containing unnamed values.
			 *
			 * Can be stepped over using iterators or foreach,
			 * in which case it works just like std::vector.
			 */
			template<typename TChar, typename TInt, typename TFloat, Escape Escaping>
			class Array : private ContainerArray<TChar, TInt, TFloat, Escaping>
			{
				private:
					using TContainer = ContainerArray<TChar, TInt, TFloat, Escaping>;

				public:
					using TArray = Array<TChar, TInt, TFloat, Escaping>;
					using TValue = typename TContainer::value_type;

				public:
					Array() = default;
					Array(const TArray &) = default;
					Array(TArray &&) = default;

					Array &operator=(const TArray &) = default;
					Array &operator=(TArray &&) = default;

					/**
					 * Initialises the container from a list of values.
					 *
					 * @param v The list of values.
					 */
					Array(std::initializer_list<TValue> v)
					{
						push(std::move(v));
					}
				
					/**
					 * Copy initialises the container from a vector of values.
					 *
					 * @param v The vector of values.
					 */
					Array(const std::vector<TValue> &v)
					:
					TContainer{v}
					{
					}
				
					/**
					 * Move initialises the container from a vector of values.
					 *
					 * @param v The vector of values.
					 */
					Array(std::vector<TValue> &&v)
					:
					TContainer{std::move(v)}
					{
					}
					
					/**
					 * Compares the array to another.
					 *
					 * @param o The other array.
					 *
					 * @return Whether the two arrays are equal.
					 */
					bool operator==(const TArray &o) const
					{
						return (count() == o.count() && std::equal(begin(), end(), o.begin()));
					}

					/**
					 * Negative compares the array to another.
					 *
					 * @param o The other array.
					 *
					 * @return Whether the two arrays are not equal.
					 */
					bool operator!=(const TArray &o) const
					{
						return !operator==(o);
					}

					/**
					 * Copies a single value into the container.
					 *
					 * @param v The value to copy.
					 */
					void push(const TValue &v) { TContainer::emplace_back(v); }
				
					/**
					 * Moves a single value into the container.
					 *
					 * @param v The value to move.
					 */
					void push(TValue &&v) { TContainer::emplace_back(std::move(v)); }

					/**
					 * Copies or moves two or more values into the container.
					 *
					 * @param v  The first value to copy.
					 * @param vs The rest of the values to copy or move.
					 */
					template<typename... Ts>
					std::enable_if_t<(sizeof...(Ts) > 0), void> push(const TValue &v, Ts... vs)
					{
						push(v);
						push(std::forward<Ts>(vs)...);
					}

					/**
					 * Copies or moves two or more values into the container.
					 *
					 * @param v  The first value to move.
					 * @param vs The rest of the values to copy or move.
					 */
					template<typename... Ts>
					std::enable_if_t<(sizeof...(Ts) > 0), void> push(TValue &&v, Ts... vs)
					{
						push(v);
						push(std::forward<Ts>(vs)...);
					}

					/**
					 * Inserts values from an initialiser list into the container.
					 *
					 * @param v The list of values to insert.
					 */
					void push(std::initializer_list<TValue> &&v)
					{
						for (auto &i : v)
							TContainer::emplace_back(std::move(i));
					}

					/**
					 * Copies a single value to the beginning of the container.
					 *
					 * @param v The value to copy.
					 */
					void prepend(const TValue &v) { TContainer::emplace(TContainer::begin(), v); }
				
					/**
					 * Copies a single value to the beginning of the container.
					 *
					 * @param v The value to copy.
					 */
					void prepend(TValue &&v) { TContainer::emplace(TContainer::begin(), std::move(v)); }
				
					/**
					 * Copies another JSON array to the beginning of the container.
					 *
					 * @param v The value to copy.
					 */
					void prepend(const TArray &v) { TContainer::insert(TContainer::begin(), v.begin(), v.end()); }

					// Borrowing these from the underlying container so that
					// instances can be looped through in foreach loops.
					using TContainer::begin;
					using TContainer::end;
					using TContainer::rbegin;
					using TContainer::rend;
					using TContainer::cbegin;
					using TContainer::cend;
					using TContainer::crbegin;
					using TContainer::crend;
				
					using TContainer::erase;

					/**
					 * Returns the value at the specified index if it exists, or null.
					 * The pointer may be invalidated later, so do not hold on to it.
					 *
					 * @param index The index of the value to get.
					 *
					 * @return The value, if existing, or null.
					 */
					TValue *get(const std::size_t &index) noexcept
					{
						return (index >= count()) ? nullptr : &TContainer::operator[](index);
					}

					/**
					 * Returns the value at the specified index if it exists, or null.
					 * The pointer may be invalidated later, so do not hold on to it.
					 *
					 * @param index The index of the value to get.
					 *
					 * @return The value, if existing, or null.
					 */
					const TValue *get(const std::size_t &index) const noexcept
					{
						return (index >= count()) ? nullptr : &TContainer::operator[](index);
					}

					/**
					 * Removes all children.
					 *
					 * @return The updated array.
					 */
					TArray &clear() { TContainer::clear(); return *this; }

					/**
					 * Returns the number of values in the object.
					 *
					 * @return The value count.
					 */
					std::size_t count() const noexcept { return TContainer::size(); }
			};

			/**
			 * This is the main entry point to the JSON interface.
			 * A context is created using template arguments to
			 * specify what character width to work with, and what
			 * precision and size of integer and floating point
			 * values to use, as well as whether or not to escape
			 * Unicode characters when serialising data (escaped
			 * Unicode characters will still be understood when
			 * parsing JSON input string).
			 *
			 * The context then serves as a the JSON namespace
			 * for types, so that the template arguments do not
			 * have to be manually specified again.
			 *
			 * JSON objects can be assembled manually with a syntax
			 * based on initialiser list that mimics actual JSON
			 * syntax as closely as possible; most types can be
			 * deduced, but objects and arrays have to be explicitly
			 * disambiguated using the Obj and Arr types of the
			 * context.
			 *
			 * JSON objects can also be parsed from a JSON string.
			 * Values can be serialised to compact strings or
			 * formatted.
			 *
			 * Example of assembling a JSON object in C++:
			 *
			 * using J = karhu::JSON::Context<char, int, float>;
			 *
			 * J::Obj o
			 * {
			 *     {"name here": "value here"},
			 *
			 *     {"int":    5},
			 *     {"float":  4.05f},
			 *     {"bool":   true},
			 *     {"null",   nullptr},
			 *     {"array":  J::Arr{100, 200, 300}},
			 *     {"object": J::Obj
			 *     {
			 *         {"string": "hello"}
			 *     }}
			 * };
			 */
			template
			<
				typename TChar    = char,
				typename TInt     = std::uint32_t,
				typename TFloat   = float,
				Escape   Escaping = Escape::withoutUnicode
			>
			class Context
			{
				static_assert(std::is_integral<TInt>{}, "Invalid integer type");
				static_assert(std::is_floating_point<TFloat>{}, "Invalid floating point type");
				static_assert(string::validTypeChar<TChar>(), "Invalid character type");

				public:
					using Char  = TChar;
					using Int   = TInt;
					using Float = TFloat;
					using Val   = Value<TChar, TInt, TFloat, Escaping>;
					using Obj   = Object<TChar, TInt, TFloat, Escaping>;
					using Arr   = Array<TChar, TInt, TFloat, Escaping>;
					using Num   = Number<TChar, TInt, TFloat, Escaping>;
					using Str   = String<TChar>;
					using Bool  = Boolean;
				
				private:
					using TPair = Pair<TChar, TInt, TFloat, Escaping>;

				public:
					/*
					 * The parser analyses the grammar/syntax of the tokens
					 * extracted by the lexer (a subclass) to validate the
					 * JSON input and build up the final instance to return.
					 */
					class Parser
					{
						private:
							/*
							 * Represents a terminal token. Some of them actually
							 * do have some grammar associated with them, like the
							 * strings and numbers, but are considered terminals
							 * anyway for simplicity's sake during the parse step.
							 */
							struct Token
							{
								std::size_t line;
								
								enum class Type
								{
									begObj,
									endObj,
									begArr,
									endArr,
									comma,
									colon,
									string,
									integer,
									floating,
									boolean,
									null
								} type;

								union
								{
									Str    string;
									Bool   boolean;
									TInt   integer;
									TFloat floating;
								};
								
								Token(Token &&) = default;
								
								Token(const Token &o)
								:
								type{o.type}
								{
									switch (type)
									{
										case Type::string:   new (&string) Str{o.string}; break;
										case Type::boolean:  boolean  = o.boolean;        break;
										case Type::integer:  integer  = o.integer;        break;
										case Type::floating: floating = o.floating;       break;
										default: break;
									}
								}
								
								Token(const std::size_t &line, Token::Type &&t) : line{line}, type{std::move(t)} {}
								Token(const std::size_t &line, const Str &&v)   : line{line}, type{Type::string} { new (&string) Str{std::move(v)}; }
								Token(const std::size_t &line, Bool &&v)        : line{line}, type{Type::boolean}, boolean{std::move(v)} {}
								
								Token(const std::size_t &line, Num &&v)
								:
								line{line}
								{
									if (auto n = v.integer())
									{
										type = Type::integer;
										integer = std::move(*n);
									}
									else
									{
										type = Type::floating;
										floating = std::move(*v.floating());
									}
								}
								
								~Token() { if (Type::string == type) string.~basic_string(); }
							};

							/*
							 * Extracts tokens without validating grammar,
							 * except for string and number syntax which is
							 * done here to extract such tokens instead of
							 * in the parser. We alternate one lex and one
							 * parse so that we can break on first error.
							 */
							class Lexer
							{
								public:
									enum class Status : std::uint8_t
									{
										more,
										done,
										error
									};

								public:
									/*
									 * Passes a reference to the error stream of
									 * the parent parser, so that the lexer can
									 * write errors to the same output as it.
									 */
									Lexer(string::Stream<TChar> &error) : m_error{error} {}

									/*
									 * Passes the input stream of JSON string data.
									 */
									Lexer &data(std::basic_istream<TChar> &data)
									{
										m_data = &data;
										return *this;
									}

									/*
									 * Lexes a single token or returns an error.
									 *
									 * Handles simple terminals directly and calls
									 * methods to deal with the more complex ones.
									 */
									Status lex()
									{
										// These are the only keywords we need to find.
										static constexpr char tokenNull []{"null"};
										static constexpr char tokenTrue []{"true"};
										static constexpr char tokenFalse[]{"false"};

										if (!m_data)
										{
											m_error << "no input";
											return finish(Status::error);
										}

										// Otherwise, the lexing is on!
										if (!done())
										{
											// Skip whitespaces and exit if nothing left to read.
											while (whitespace(get()))
												if (!advance())
													return finish(Status::done);
											
											// Comments.
											while (!done())
											{
												const auto c(get());
												
												if (c == '/')
												{
													if (!lexComment())
														return finish(Status::error);
												}
												else if (!whitespace(c))
													break;
												
												if (!advance())
													return finish(Status::done);
											}
											
											const auto c(get());

											// Single character tokens.
											if      (c == '{') token(m_line, Token::Type::begObj);
											else if (c == '}') token(m_line, Token::Type::endObj);
											else if (c == '[') token(m_line, Token::Type::begArr);
											else if (c == ']') token(m_line, Token::Type::endArr);
											else if (c == ',') token(m_line, Token::Type::comma);
											else if (c == ':') token(m_line, Token::Type::colon);

											// Keywords by their first letter.
											else if (c == 'n') { if (!lexWord(tokenNull,  Token::Type::null)) return finish(Status::error); }
											else if (c == 't') { if (!lexWord(tokenTrue,  true))              return finish(Status::error); }
											else if (c == 'f') { if (!lexWord(tokenFalse, false))             return finish(Status::error); }

											// Complex literals by their first character.
											else if (c == '"')             { if (!lexString()) return finish(Status::error); }
											else if (c == '-' || digit(c)) { if (!lexNumber()) return finish(Status::error); }
											
											// Error.
											else
											{
												m_error << "unexpected '" << c << '\'';
												return finish(Status::error);
											}
											
											advance();
											return Status::more;
										}
										
										return finish(Status::done);
									}

									/*
									 * Returns the list of tokens for the parser to access.
									 */
									const std::vector<Token> &tokens() const noexcept { return m_tokens; }

									/*
									 * For debugging if necessary.
									 */
									Str tokendump(Token i) const
									{
										string::Stream<TChar> s;

										switch (i.type)
										{
											case Token::Type::begObj:
												s << "{ ";
												break;
												
											case Token::Type::endObj:
												s << "} ";
												break;
												
											case Token::Type::begArr:
												s << "[ ";
												break;
												
											case Token::Type::endArr:
												s << "] ";
												break;
												
											case Token::Type::comma:
												s << ", ";
												break;
												
											case Token::Type::colon:
												s << ": ";
												break;
												
											case Token::Type::string:
												s << "STRING(" << i.string << ") ";
												break;
												
											case Token::Type::integer:
												s << "INT(" << i.integer << ") ";
												break;
												
											case Token::Type::floating:
												s << "FLOAT(" << i.floating << ") ";
												break;
												
											case Token::Type::boolean:
												s << "BOOL(" << std::boolalpha << i.boolean << ") ";
												break;
												
											case Token::Type::null:
												s << "NULL ";
												break;
											
											default:
												s << "UNKNOWN";
												break;
										}
										
										return s.str();
									}

								private:
									/*
									 * Finishes by resetting the stream and returning a status.
									 */
									Status finish(const Status s)
									{
										m_data = nullptr;
										return s;
									}

									// Complex terminals.

									/*
									 * Tries to match a keyword passed to the function.
									 */
									template<std::size_t Length, typename T>
									bool lexWord(const char (&word)[Length], T &&v)
									{
										constexpr auto length(Length - 1);
										for (std::size_t i{0}; i < length; ++ i)
										{
											const auto c(get());
											if (c == word[i])
											{
												if (i < length - 1 && !advanceOrError())
													return false;
											}
											else
											{
												m_error << "unexpected '" << c << '\'';
												return false;
											}
										}
										
										token(m_line, std::forward<T>(v));
										return true;
									}

									/*
									 * Lexes a string literal, starting from after
									 * the first quote, which has already been found
									 * when entering this method, and parses until
									 * the ending quote or EOF.
									 */
									bool lexString()
									{
										string::Stream<TChar> s;
										
										// The first quote has already been found. Moving on.
										while (advance() && get() != '"')
										{
											const auto c(get());

											// Escape?
											if (c == '\\')
											{
												if (!advanceOrError()) return false;
												
												const auto c(get());

												// Single-character sequence?
												if      (c == 'b')  s << '\b';
												else if (c == 'f')  s << '\f';
												else if (c == 'n')  s << '\n';
												else if (c == 'r')  s << '\r';
												else if (c == 't')  s << '\t';
												else if (c == '"')  s << '"';
												else if (c == '\\') s << '\\';
												
												// Unicode?
												else if (c == 'u')
												{
													// Use this to get the correct result before converting.
													std::wstringstream w;
													if (!lexEscapeUnicode(s, w)) return false;

													// Convert to the right kind of string and append.
													s << string::convert<TChar>(w.str());
												}
												
												// Invalid escape character.
												else
												{
													m_error << "invalid escape sequence '\\" << c << '\'';
													return false;
												}
											}
											// Regular character.
											else
												s << c;
										}

										// End of a correct string.
										if (get() == '"')
										{
											token(m_line, std::move(s.str()));
											return true;
										}
										// End of file error.
										else
											return advanceOrError();

										return false;
									}

									/*
									 * Tries to lex a valid number and determine whether
									 * it is a floating point literal or an integer.
									 */
									bool lexNumber()
									{
										string::Stream<TChar> s;
										
										// Starts with an integer literal either way.
										if (!lexInteger(s)) return false;
										
										// Fraction?
										if (next() && peek() == '.')
										{
											advance();
											s << get();

											if (!lexSuffixNumber(s)) return false;
											
											// Exponent?
											if (next() && exponent(peek()))
											{
												advance();
												s << get();
												
												// Plus or minus?
												if (next() && (peek() == '-' || peek() == '+'))
												{
													advance();
													s << get();
												}
												
												if (!lexSuffixNumber(s)) return false;
											}
											
											// Floating.
											TFloat v;
											s >> v;
											token(m_line, Num{std::move(v)});
										}
										// Integer.
										else
										{
											TInt v;
											s >> v;
											token(m_line, Num{std::move(v)});
										}
										
										return true;
									}

									/*
									 * Lexes the first part of a valid number, which is
									 * the same for integers as floats, since floats are
									 * determined by finding a decimal point later.
									 */
									bool lexInteger(string::Stream<TChar> &s)
									{
										// Optional.
										{
											const auto c(get());
											if (c == '-')
											{
												s << c;
												if (!advanceOrError()) return false;
											}
										}
										
										// Need a digit not starting with zero.
										const auto c(get());
										if (c != '0' && digit(c))
											lexDigit(s);
										else if (c == '0')
										{
											if (!next()) return advanceOrError();
											if (digit(next()))
											{
												m_error << "non-zero number cannot start with zero";
												return false;
											}
											s << '0';
										}
										else
										{
											m_error << "unexpected '" << c << "' in number";
											return false;
										}
					
										return true;
									}

									/*
									 * Tried to match a valid fraction or exponent of a number.
									 */
									bool lexSuffixNumber(string::Stream<TChar> &s)
									{
										if (!advanceOrError()) return false;
									
										// Next we need another digit.
										const auto c(get());
										if (digit(c))
											lexDigit(s);
										else
										{
											m_error << "unexpected '" << c << "' in number";
											return false;
										}

										return true;
									}
								
									//! @todo: Clean up Unicode escaping depending on TChar.
									/*
									 * Parses a sequence of Unicode escape sequences and
									 * inserts the decoded character(s) into the string.
									 */
									bool lexEscapeUnicode(string::Stream<TChar> &s, std::wstringstream &w)
									{
										// Now we need to find four hex characters.
										std::array<char, 4> letters;
										for (std::size_t i{0}; i < letters.size(); ++ i)
										{
											if (!advanceOrError()) return false;
											
											const auto c(get());
											if (hex(c))
												letters[i] = static_cast<char>(c);
											else
											{
												m_error << "invalid '" << c << "' in Unicode escape sequence";
												return false;
											}
										}
										
										// Turn it into the corresponding character.
										w << static_cast<wchar_t>(std::stoul(letters.data(), nullptr, 16));
										
										// Is there another escape sequence?
										if (next() && peek() == '\\' && next(2) && peek(2) == 'u')
										{
											// Advance those two and lex the next one.
											advance(2);
											if (!lexEscapeUnicode(s, w)) return false;
										}
										
										return true;
									}

									// Helpers.
								
									/*
									 * Keeps appending characters as long as they are numeric.
									 */
									void lexDigit(string::Stream<TChar> &s)
									{
										do
										{
											const char &c(get());
											if (digit(c))
												s << c;
											else
												return;
										}
										while (next() && digit(peek()) && advance());
									}
								
									/*
									 * Lexes a comment, starting from after
									 * the first slash, which has already been found
									 * when entering this method, and parses until
									 * the ending newline or token or EOF.
									 */
									bool lexComment()
									{
										if (!advanceOrError())
											return false;
										
										const auto c(get());
										const bool multiline{(c == '*')};
										
										if (!multiline && c != '/')
										{
											m_error << "unexpected '" << c << '\'';
											return false;
										}
										
										while (advance())
										{
											const auto c(get());
											
											if (c == '\n')
											{
												++ m_line;
												
												if (!multiline)
													return true;
											}
											else if (multiline && c == '*')
											{
												if (!advanceOrError())
													return false;
												
												const auto c(get());
												
												if (c != '/')
												{
													m_error << "unexpected '" << c << '\'';
													return false;
												}
												
												return true;
											}
										}
										
										if (multiline)
										{
											m_error << "unterminated multiline comment";
											return false;
										}
										
										return true;
									}

									// Movement.
								
									/*
									 * Returns the current character without advancing.
									 */
									const TChar get() const { return static_cast<TChar>(m_data->peek()); }
								
									/*
									 * Looks at the next character, or several steps ahead
									 * if an offset greater than one is specified.
									 */
									const TChar peek(const std::size_t offset = 1) const
									{
										m_data->seekg(static_cast<std::streamoff>(offset), std::ios_base::cur);
										const TChar c{get()};
										m_data->seekg(-static_cast<std::streamoff>(offset), std::ios_base::cur);
										return c;
									}
								
									/*
									 * Returns whether the lexer has reached the end of input.
									 */
									bool done() { return (m_data->peek() == EOF); }
								
									/*
									 * Returns whether there is another character to read
									 * ahead, or several steps ahead if an offset greater
									 * than one is specified.
									 */
									bool next(const std::size_t offset = 1)
									{
										for (std::size_t i{0}; i < offset; ++ i)
											if (m_data->get() == EOF)
												return false;
											
										for (std::size_t i{0}; i < offset; ++ i)
											m_data->unget();

										return true;
									}
								
									/*
									 * Actually advances the cursor in the stream and
									 * returns whether there is something to read there
									 * or the end has been reached.
									 */
									bool advance(const std::size_t offset = 1)
									{
										m_data->seekg(static_cast<std::streamoff>(offset), std::ios_base::cur);
										return !done();
									}

									/*
									 * Tries to advance but fills the error stream with
									 * an EOF error in case there is nothing more to read;
									 * used when more characters are expected.
									 */
									bool advanceOrError()
									{
										if (!advance())
										{
											m_error << "unexpected EOF";
											return false;
										}
										
										return true;
									}

									// Validation.
									bool whitespace(const TChar &c) const { if (c == '\n') ++ m_line; return (c == ' ' || c == '\t' || c == '\n' || c == '\r'); }
									bool digit(const TChar &c) const { return std::isdigit(static_cast<char>(c)); }
									bool hex(const TChar &c) const { return (digit(c) || (c >= 'A' && c <= 'F') || (c >= 'a' || c <= 'f')); }
									bool exponent(const TChar &c) const { return (c == 'e' || c == 'E'); }

									/*
									 * Pushes a token onto the list to be read in by the parser.
									 */
									template<typename T>
									void token(const std::size_t &line, T &&v)
									{
										m_tokens.emplace_back(line, std::forward<T>(v));
									}

								private:
									string::Stream<TChar>     &m_error;
									std::vector<Token>         m_tokens;
									std::basic_istream<TChar> *m_data{nullptr};
								
									mutable std::size_t m_line{1};
							};

						public:
							// Shares the error stream with the lexer so that it can write to it.
							Parser() : m_lexer{m_error} {}

							/**
							 * Tries to parse a JSON value from an input stream and
							 * returns a nullable wrapper which is valid on success.
							 *
							 * In case of failure, an error message can be retrieved
							 * by calling error().
							 *
							 * @param data The stream to parse.
							 *
							 * @return A nullable containing a valid value on success.
							 */
							Nullable<Val> parse(std::basic_istream<TChar> &data)
							{
								m_lexer.data(data);
								return parse();
							}

							/**
							 * Returns an error string in case of an unsuccessful parse.
							 *
							 * @return The error string.
							 */
							const Str &error() const { return m_error.str(); }

						private:
							/*
							 * Starting point of the entire recursive parse.
							 */
							Nullable<Val> parse()
							{
								if (Lexer::Status::more != m_lexer.lex())
									return {};

								if (auto o = parseValue())
								{
									m_error.str("");
									return o;
								}

								return {};
							}

							/*
							 * Tries to parse a JSON object.
							 */
							Nullable<Obj> parseObject()
							{
								if (Token::Type::begObj == get().type)
									advance();
								else
								{
									m_error.str(std::to_string(get().line) + ": expected token '{'");
									return {};
								}
								
								// Build an object from pairs.
								Nullable<Obj> o{true};
								while (auto v = parsePair())
								{
									o->push(std::move(*v));

									// Stop if there are no more pairs coming up.
									if (Token::Type::comma != get().type)
										break;
									else if (!advanceOrError())
										return {};
								}

								if (Token::Type::endObj == get().type)
									advance();
								else
								{
									m_error.str(std::to_string(get().line) + ": expected token '}'");
									return {};
								}
								
								return o;
							}

							/*
							 * Tries to parse a JSON key-value pair.
							 */
							Nullable<TPair> parsePair()
							{
								Str key;

								// We need a name.
								const auto &c(get());
								if (Token::Type::string == c.type)
									key = std::move(c.string);
								else
								{
									m_error.str(std::to_string(get().line) + ": expected string as name of pair");
									return {};
								}
								
								if (!advanceOrError()) return {};
								if (Token::Type::colon != get().type)
								{
									m_error.str(std::to_string(get().line) + ": expected token ':'");
									return {};
								}
								if (!advanceOrError()) return {};

								// Then any value.
								if (auto v = parseValue())
									return {true, std::move(key), std::move(*v)};
								else
								{
									m_error.str(std::to_string(get().line) + ": expected value of pair");
									return {};
								}
							}

							/*
							 * Tries to parse a JSON value.
							 */
							Nullable<Val> parseValue()
							{
								Val r;

								const auto &c(get());
								switch (c.type)
								{
									// Simple literals.
									// If we use these we have to set the value before advancing,
									// to make sure the token isn't invalidated before we move it.
									case Token::Type::null:     r = {nullptr}; break;
									case Token::Type::boolean:  r = {Bool{std::move(c.boolean)}}; break;
									case Token::Type::integer:  r = {std::move(c.integer)}; break;
									case Token::Type::floating: r = {std::move(c.floating)}; break;
									case Token::Type::string:   r = {std::move(c.string)}; break;
									
									// Object.
									case Token::Type::begObj: if (auto o = parseObject()) return {true, std::move(*o)};
									
									// Array.
									case Token::Type::begArr: if (auto o = parseArray()) return {true, std::move(*o)};
									
									default: m_error.str(std::to_string(get().line) + ": expected value"); return {};
								}
								
								// We get here in case of a simple literal. Advance!
								advance();
								return {true, std::move(r)};
							}

							/*
							 * Tries to parse a JSON array.
							 */
							Nullable<Arr> parseArray()
							{
								if (Token::Type::begArr == get().type)
									advance();
								else
								{
									m_error.str(std::to_string(get().line) + ": expected token '['");
									return {};
								}

								// Build an array from values.
								Nullable<Arr> o{true};
								while (auto v = parseValue())
								{
									o->push(std::move(*v));

									// Stop if there are no more values coming up.
									if (Token::Type::comma != get().type)
										break;
									else if (!advanceOrError())
										return {};
								}

								if (Token::Type::endArr == get().type)
									advance();
								else
								{
									m_error.str(std::to_string(get().line) + ": expected token ']'");
									return {};
								}
								
								return o;
							}

							/*
							 * Returns the current token without advancing.
							 */
							const Token &get() const { return m_lexer.tokens()[m_index]; }
						
							/*
							 * Advances to the next token and returns whether
							 * there is one at the current position or if the
							 * end has been reached or the lexer has an error.
							 */
							bool advance()
							{
								switch (m_lexer.lex())
								{
									case Lexer::Status::done:
									case Lexer::Status::error:
										return false;
									
									case Lexer::Status::more:
										return ((++ m_index) < m_lexer.tokens().size());
								}
							}

							/*
							 * Tries to advance but fills the error stream with
							 * an EOF error in case there is nothing more to read;
							 * used when more tokens are expected.
							 */
							bool advanceOrError()
							{
								if (!advance())
								{
									m_error.str("unexpected EOF");
									return false;
								}
								
								return true;
							}

						private:
							string::Stream<TChar> m_error;
							Lexer                 m_lexer;
							std::size_t           m_index{0};
					};
			};
		}
	}
#endif
