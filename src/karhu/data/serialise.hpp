/**
 * @author		Ava Skoog
 * @date		2018-10-07
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Be able to serialise things like std::vector<std::unique_ptr<Child>>, since they are unique they are safe to reconstruct in place?

#ifndef KARHU_DATA_SERIALISE_H_
	#define KARHU_DATA_SERIALISE_H_

	#include <karhu/data/preserialise.hpp>
	#include <karhu/data/JSON.hpp>
	#include <karhu/core/string.hpp>
	#include <karhu/core/meta.hpp>

	#include <array>
	#include <vector>
	#include <map>
	#include <unordered_map>

	namespace karhu
	{
		namespace serialise
		{
			/**
			 * Only used by the karhuPROP macro; no need to use directly.
			 */
			template<typename T>
			struct Prop
			{
				explicit Prop(const char *name, T &value) : name{name}, value{value} {}
				const char *name;
				T &value;
			};

			/**
			 * Simple JSON serialiser and deserialiser.
			 *
			 * Due to the nature of its implementation, only include
			 * preserialise.hpp in headers to get predeclared types,
			 * and only actually include and use the serialiser from
			 * serialise.hpp in source files.
			 *
			 * Takes a reference to an existing JSON object that
			 * can be read from and written to using the karhuPROP
			 * macro to map it to variables in for example a struct.
			 *
			 * Serialiser<...> s{obj};
			 * s << karhuIN(pos.x) << karhuIN(pos.y) << karhuIN(pos.z);
			 * s >> karhuOUT(pos.x) << karhuIN(pos.y) << karhuIN(pos.z);
			 */
			template<typename TContextJSON>
			class Serialiser
			{
				public:
					using JSON = TContextJSON;
				
				private:
					template<typename T>
					class CheckSerialisable
					{
						typedef char Yes;
						typedef long No;

						template<typename C> static Yes test(decltype(&C::serialise));
						template<typename C> static No  test(...);

						public:
							static constexpr bool success() noexcept
							{
								return (sizeof(test<T>(0)) == sizeof(char));
							};
					};
				
					template<typename T>
					class CheckDeserialisable
					{
						typedef char Yes;
						typedef long No;

						template<typename C> static Yes test(decltype(&C::deserialise));
						template<typename C> static No  test(...);

						public:
							static constexpr bool success() noexcept
							{
								return (sizeof(test<T>(0)) == sizeof(char));
							};
					};

					template<typename T>
					static constexpr bool hasSerialise() noexcept
					{
						return CheckSerialisable<T>::success();
					}

					template<typename T>
					static constexpr bool hasDeserialise() noexcept
					{
						return CheckDeserialisable<T>::success();
					}

				public:
					template<typename T>
					static constexpr bool basic() noexcept
					{
						return
						(
							std::is_fundamental<T>{} ||
							string::validTypeString<std::remove_const_t<T>>()
						);
					}
				
					template<typename T>
					static constexpr bool list() noexcept
					{
						return
						(
							meta::specialises<std::remove_const_t<T>, std::vector>{} ||
							meta::specialisesArray<std::remove_const_t<T>>{}
						);
					}
				
					template<typename T>
					static constexpr bool dict() noexcept
					{
						return
						(
							meta::specialises<std::remove_const_t<T>, std::map>{} ||
							meta::specialises<std::remove_const_t<T>, std::unordered_map>{} ||
							meta::specialises<std::remove_const_t<T>, std::multimap>{} ||
							meta::specialises<std::remove_const_t<T>, std::unordered_multimap>{}
						);
					}
				
					template<typename T>
					static constexpr bool container() noexcept
					{
						return
						(
							list<T>() ||
							dict<T>() ||
							std::is_array<std::remove_const_t<T>>{}
						);
					}

					template<typename T>
					static constexpr bool potentiallySerialisableOrDeserialisable() noexcept
					{
						return
						(
							!std::is_same<std::remove_const_t<T>, typename JSON::Val>{} &&
							!std::is_enum<T>{} &&
							!basic<T>() &&
							!container<T>() &&
							!std::is_pointer<T>{} &&
							!std::is_reference<T>{}
						);
					}

					template<typename T>
					static constexpr bool potentiallySerialisable() noexcept
					{
						return
						(
							potentiallySerialisableOrDeserialisable<T>() &&
							hasSerialise<T>()
						);
					}

					template<typename T>
					static constexpr bool potentiallyDeserialisable() noexcept
					{
						return
						(
							potentiallySerialisableOrDeserialisable<T>() &&
							hasDeserialise<T>()
						);
					}
				
				public:
					/**
					 * Specifies the object to map to the serialiser.
					 *
					 * @param target The JSON object that will be used for reading and writing.
					 */
					Serialiser(typename JSON::Obj &target)
					:
					m_target   {&target},
					m_usesConst{false}
					{
					}

					/**
					 * Specifies the object to map to the serialiser.
					 *
					 * @param target The JSON object that will be used for reading and writing.
					 */
					Serialiser(const typename JSON::Obj &target)
					:
					m_targetConst{&target},
					m_usesConst  {true}
					{
					}
				
					Serialiser() = delete;

					// Serialises pure JSON.
					Serialiser<JSON> &operator<<(const Prop<const typename JSON::Val> &prop)
					{
						if (!target())
							return *this;

						const auto n(name(prop.name));
						push(n, prop.value);
						return *this;
					}

					// Serialises a primitive or a string.
					template<typename T>
					std::enable_if_t
					<
						basic<T>(),
						Serialiser<JSON> &
					>
					operator<<(const Prop<T> &prop)
					{
						if (!target())
							return *this;

						const auto n(name(prop.name));
						push(n, prop.value);
						return *this;
					}

					// Serialises a strongly typed enumerated value.
					template<typename T>
					std::enable_if_t
					<
						std::is_enum<T>{},
						Serialiser<JSON> &
					>
					operator<<(const Prop<T> &prop)
					{
						if (!target())
							return *this;

						const auto n(name(prop.name));
						push(n, static_cast<std::underlying_type_t<T>>(prop.value));
						return *this;
					}

					// Serialises a primitive fixed-size array.
					template<typename T, std::size_t N>
					Serialiser &operator<<(const Prop<T[N]> &prop)
					{
						if (!target())
							return *this;

						const auto n(name(prop.name));
						
						const auto old(targetArr());
						targetArr(push(n, typename JSON::Arr{}).array());
						
						for (const auto &v : prop.value)
							*this << karhuIN(v);
						
						targetArr(old);

						return *this;
					}

					// Serialises a fixed-size array wrapper.
					template<typename T, std::size_t N>
					Serialiser &operator<<(const Prop<std::array<T, N>> &prop)
					{
						if (!target())
							return *this;

						const auto n(name(prop.name));
						
						const auto old(targetArr());
						targetArr(push(n, typename JSON::Arr{}).array());
						
						for (const auto &v : prop.value)
							*this << karhuIN(v);
						
						targetArr(old);

						return *this;
					}
				
					// Serialises some sort of container without keys.
					template<typename T>
					std::enable_if_t
					<
						list<T>(),
						Serialiser<JSON> &
					>
					operator<<(const Prop<T> &prop)
					{
						if (!target())
							return *this;

						const auto n(name(prop.name));
						
						const auto old(targetArr());
						targetArr(push(n, typename JSON::Arr{}).array());
						
						for (const auto &v : prop.value)
							*this << karhuIN(v);
						
						targetArr(old);

						return *this;
					}
				
					// Serialises some sort of container with keys.
					template<typename T>
					std::enable_if_t
					<
						dict<T>(),
						Serialiser<JSON> &
					>
					operator<<(const Prop<T> &prop)
					{
						if (!target())
							return *this;

						const auto n(name(prop.name));
						auto child(push(n, typename JSON::Arr{}).array());
						
						for (const auto &v : prop.value)
						{
							child->push(typename JSON::Arr{});
							const auto old(targetArr());
							targetArr(child->get(child->count() - 1)->array());
							
							*this << karhuIN(v.first);
							*this << karhuIN(v.second);
							
							targetArr(old);
						}

						return *this;
					}

					// Serialises another serialisable struct.
					template<typename T>
					std::enable_if_t
					<
						potentiallySerialisable<T>(),
						Serialiser<JSON> &
					>
					operator<<(const Prop<T> &prop)
					{
						if (!target())
							return *this;

						const auto n(name(prop.name));
						
						auto old(target());
						auto arr(targetArr());
						target(push(n, typename JSON::Obj{}).object());
						m_targetArr = nullptr;
						m_targetConstArr = nullptr;
						
						prop.value.serialise(*this);
						
						targetArr(arr);
						target(old);
						
						return *this;
					}

					// Deserialises pure JSON.
					const Serialiser<JSON> &operator>>(const Prop<typename JSON::Val> &prop) const
					{
						const auto n(name(prop.name));
						
						if (const auto e = pop(n))
							prop.value = *e;
						
						return *this;
					}

					// Deserialises primitives or strings.
					template<typename T>
					std::enable_if_t
					<
						basic<T>(),
						const Serialiser<JSON> &
					>
					operator>>(const Prop<T> &prop) const
					{
						const auto n(name(prop.name));
						
						if (const auto e = pop(n))
						{
							if (const auto v = e->boolean())
								prop.value = convert<T>(*v);
							else if (const auto v = e->integer())
								prop.value = convert<T>(*v);
							else if (const auto v = e->floating())
								prop.value = convert<T>(*v);
							else if (const auto v = e->string())
								prop.value = convert<T>(*v);
						}
						
						return *this;
					}

					// Deserialises strongly typed enumerated values.
					template<typename T>
					std::enable_if_t
					<
						std::is_enum<T>{},
						const Serialiser<JSON> &
					>
					operator>>(const Prop<T> &prop) const
					{
						const auto n(name(prop.name));
						
						if (const auto e = pop(n))
							if (const auto v = e->integer())
								prop.value = static_cast<T>(*v);
						
						return *this;
					}

					// Deserialises a primitive fixed-size array.
					template<typename T, std::size_t N>
					const Serialiser &operator>>(const Prop<T[N]> &prop) const
					{
						const auto n(name(prop.name));

						if (const auto a = pop(n))
						{
							if (const auto child = a->array())
							{
								const auto old(targetArr());
								const auto ind(m_indexArr);
								targetArr(child);
								m_indexArr = 0;
								
								const std::size_t count{std::min(N, child->count())};
								for (; m_indexArr < count; ++ m_indexArr)
									*this >> karhuOUT(prop.value[m_indexArr]);
								
								m_indexArr = ind;
								targetArr(old);
							}
						}
						
						return *this;
					}

					// Deserialises a fixed-size array wrapper.
					template<typename T, std::size_t N>
					const Serialiser &operator>>(const Prop<std::array<T, N>> &prop) const
					{
						const auto n(name(prop.name));

						if (const auto a = pop(n))
						{
							if (const auto child = a->array())
							{
								const auto old(targetArr());
								const auto ind(m_indexArr);
								targetArr(child);
								m_indexArr = 0;
								
								const std::size_t count{std::min(N, child->count())};
								for (; m_indexArr < count; ++ m_indexArr)
									*this >> karhuOUT(prop.value[m_indexArr]);
								
								m_indexArr = ind;
								targetArr(old);
							}
						}
						
						return *this;
					}
				
					// Deserialises some dynamic container without keys.
					template<typename T>
					std::enable_if_t
					<
						list<T>(),
						const Serialiser<JSON> &
					>
					operator>>(const Prop<T> &prop) const
					{
						const auto n(name(prop.name));

						if (const auto a = pop(n))
						{
							prop.value.clear();
						
							if (const auto child = a->array())
							{
								prop.value.resize(child->count());

								const auto old(targetArr());
								const auto ind(m_indexArr);
								targetArr(child);
								m_indexArr = 0;
								
								for (; m_indexArr < child->count(); ++ m_indexArr)
									*this >> karhuOUT(prop.value[m_indexArr]);
								
								m_indexArr = ind;
								targetArr(old);
							}
						}
						
						return *this;
					}
				
					// Deserialises some sort of dynamic container with keys.
					template<typename T>
					std::enable_if_t
					<
						dict<T>(),
						const Serialiser<JSON> &
					>
					operator>>(const Prop<T> &prop) const
					{
						using K = std::remove_const_t<typename T::key_type>;
						using V = std::remove_const_t<typename T::mapped_type>;
						
						const auto n(name(prop.name));

						if (const auto a = pop(n))
						{
							prop.value.clear();
						
							if (const auto child = a->array())
							{
								for (const auto &e : *child)
								{
									if (const auto pair = e.array())
									{
										const auto key(pair->get(0));
										const auto val(pair->get(1));
										
										if (key && val)
										{
											const auto old(targetArr());
											const auto ind(m_indexArr);
											targetArr(pair);
											
											K k;
											V v;
											
											m_indexArr = 0;
											*this >> karhuOUT(k);
											m_indexArr = 1;
											*this >> karhuOUT(v);
											
											prop.value.emplace(std::move(k), std::move(v));
											
											m_indexArr = ind;
											targetArr(old);
										}
									}
								}
							}
						}
						
						return *this;
					}

					// Deserialises another serialisable struct.
					template<typename T>
					std::enable_if_t
					<
						potentiallyDeserialisable<T>(),
						const Serialiser<JSON> &
					>
					operator>>(const Prop<T> &prop) const
					{
						const auto n(name(prop.name));

						if (const auto child = pop(n))
						{
							if (const auto o = child->object())
							{
								auto arr(targetArr());
								auto old(target());
								target(o);
								m_targetArr = nullptr;
								m_targetConstArr = nullptr;
								prop.value.deserialise(*this);
								targetArr(arr);
								target(old);
							}
						}
						
						return *this;
					}

					typename JSON::Str name(const char *n) const
					{
						return string::convert<typename JSON::Char>(n);
					}

					// Using these we can deserialise primitives
					// and strings in the same function.
					template<typename T, typename U>
					std::enable_if_t
					<
						(
							std::is_fundamental<T>{} &&
							std::is_fundamental<U>{}
						),
						T
					>
					convert(const U &value) const
					{
						return static_cast<T>(value);
					}

					// Using these we can deserialise primitives
					// and strings in the same function.
					template<typename T, typename U>
					std::enable_if_t
					<
						(
							string::validTypeString<std::remove_const_t<T>>() &&
							string::validTypeString<std::remove_const_t<U>>()
						),
						T
					>
					convert(const U &value) const
					{
						return string::convert<typename T::value_type>(value);
					}

					// This one is just a dummy to save us work elsewhere.
					template<typename T, typename U>
					std::enable_if_t
					<
						(
							(
								string::validTypeString<std::remove_const_t<T>>() &&
								!string::validTypeString<std::remove_const_t<U>>()
							) ||
							(
								!string::validTypeString<std::remove_const_t<T>>() &&
								string::validTypeString<std::remove_const_t<U>>()
							)
						),
						T
					>
					convert(const U &) const
					{
						return T{};
					}
					
					typename JSON::Obj *target() noexcept
					{
						return (m_target) ? m_target : nullptr;
					}

					const typename JSON::Obj *target() const noexcept
					{
						return (m_usesConst) ? m_targetConst : m_target;
					}

					typename JSON::Arr *targetArr() noexcept
					{
						return (m_targetArr) ? m_targetArr : nullptr;
					}

					const typename JSON::Arr *targetArr() const noexcept
					{
						return (m_usesConst) ? m_targetConstArr : m_targetArr;
					}
				
				private:
					void target(typename JSON::Obj *t) const { m_target = t; }
				
					void target(const typename JSON::Obj *t) const
					{
						if (m_usesConst)
							m_targetConst = t;
						else
							m_target = const_cast<typename JSON::Obj *>(t);
					}

					void targetArr(typename JSON::Arr *t) const { m_targetArr = t; }
				
					void targetArr(const typename JSON::Arr *t) const
					{
						if (m_usesConst)
							m_targetConstArr = t;
						else
							m_targetArr = const_cast<typename JSON::Arr *>(t);
					}

					template<typename TKey, typename TVal>
					typename JSON::Val &push(TKey &&key, TVal &&val)
					{
						if (targetArr())
						{
							targetArr()->push(std::forward<TVal &&>(val));
							return *targetArr()->get(targetArr()->count() - 1);
						}
						else
							return target()->set(std::forward<TKey &&>(key), std::forward<TVal &&>(val));
					}

					template<typename TKey>
					const typename JSON::Val *pop(TKey &&key) const
					{
						if (targetArr())
							return targetArr()->get(m_indexArr);
						else
							return target()->get(std::forward<TKey &&>(key));
					}

				private:
					// Using pointer because it will sometimes temporarily
					// be swapped out but it is guaranteed to never be null.
					mutable typename JSON::Obj *m_target{nullptr};
					mutable const typename JSON::Obj *m_targetConst{nullptr};
					mutable typename JSON::Arr *m_targetArr{nullptr};
					mutable const typename JSON::Arr *m_targetConstArr{nullptr};
					mutable std::size_t m_indexArr{0};
					const bool m_usesConst;
			};
		}
	}
#endif
