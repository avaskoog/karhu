/**
 * @author		Ava Skoog
 * @date		2018-10-22
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_DATA_PRESERIALISE_H_
	#define KARHU_DATA_PRESERIALISE_H_

	#include <type_traits>

	#define karhuIN(variable) \
		(::karhu::serialise::Prop<std::remove_reference_t<std::add_const_t<decltype(variable)>>>{#variable, variable})

	#define karhuOUT(variable) \
		(::karhu::serialise::Prop<std::remove_reference_t<decltype(variable)>>{#variable, variable})

	#define karhuINn(identifier, variable) \
		(::karhu::serialise::Prop<std::remove_reference_t<std::add_const_t<decltype(variable)>>>{identifier, variable})

	#define karhuOUTn(identifier, variable) \
		(::karhu::serialise::Prop<std::remove_reference_t<decltype(variable)>>{identifier, variable})

	namespace karhu
	{
		namespace serialise
		{
			template<typename>
			struct Prop;

			template<typename>
			class Serialiser;
		}
	}
#endif
