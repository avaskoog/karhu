/**
 * @author		Ava Skoog
 * @date		2019-10-30
 * @copyright	2018-2021 Ava Skoog
 */

#ifndef KARHU_CORE_STOPWATCH_H_
	#define KARHU_CORE_STOPWATCH_H_

	#include <string>
	#include <chrono>

	namespace karhu
	{
		/**
		 * Debug utility to time things.
		 */
		class Stopwatch
		{
			public:
				Stopwatch() : start{std::chrono::steady_clock::now()} {}
			
				void reset() { start = std::chrono::steady_clock::now(); }
			
				std::string stringify() const;
				void log(const char *description = nullptr) const;

			private:
				std::chrono::steady_clock::time_point start;
		};
	}
#endif
