/**
 * @author		Ava Skoog
 * @date		2017-07-11
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_CORE_META_H_
	#define KARHU_CORE_META_H_

	#include <type_traits>
	#include <tuple>

	namespace karhu
	{
		namespace meta
		{
			/// @todo: Can be removed should we ever upgrade to C++17 as there is std::size() that does just this there.
			template<typename T, std::size_t N>
			constexpr std::size_t size(T (&)[N]) { return N; }

			template<typename T, typename First, typename... Rest>
			inline constexpr bool same() noexcept
			{
				if (sizeof...(Rest) > 0)
					return (std::is_same<T, First>{} && same<T, Rest...>());
				else
					return std::is_same<T, First>{};
			}

			template<typename T, typename First, typename... Rest>
			inline constexpr std::enable_if_t<(sizeof...(Rest) > 0), bool> base() noexcept
			{
				return (std::is_base_of<T, First>{} && base<T, Rest...>());
			}
			
			template<class T, class First>
			inline constexpr bool base() noexcept
			{
				return std::is_base_of<T, First>{};
			}
			
			template<class TParent, class TChild>
			inline constexpr bool derived() noexcept
			{
				return (std::is_base_of<TParent, TChild>{} && !std::is_same<TParent, TChild>{});
			}
			
			template<typename T, typename First>
			inline constexpr bool contains() noexcept
			{
				return std::is_same<T, First>{};
			}
			
			template<typename T, typename First, typename... Rest>
			inline constexpr std::enable_if_t<(sizeof...(Rest) > 0), bool> contains() noexcept
			{
				return (std::is_same<T, First>{} || contains<T, Rest...>());
			}
			
			template<class T, template<class...> class Template>
			struct specialises : std::false_type {};
			
			template<template<class...> class Template, typename... Args>
			struct specialises<Template<Args...>, Template> : std::true_type {};
			
			template<class T>
			struct specialisesArray : std::false_type {};
			
			template<class T, std::size_t N>
			struct specialisesArray<std::array<T, N>> : std::true_type {};

			namespace string
			{
				inline constexpr bool equal(const char *a, const char *b) noexcept
				{
					if (!a && !b) return true;
					if (!a || !b) return false;
					
					for (;; ++ a, ++ b)
					{
						if (!*a && !*b)
							break;

						if (!*a && *b)
							return false;

						if (*a && !*b)
							return false;

						if (*a != *b)
							return false;
					}
					
					return true;
				}
			}
		}
	}
#endif
