/**
 * @author		Ava Skoog
 * @date		2021-05-29
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_CORE_REF_H_
	#define KARHU_CORE_REF_H_

	#include <karhu/core/log.hpp>
	#include <karhu/core/Buffer.hpp>

	#include <map>
	#include <type_traits>
	#include <cstdint>

	namespace karhu
	{
		using IDRef = std::uint64_t;
		
		class Reference;
		class Refbank;
		
		class Reftracker
		{
			public:
				Reftracker() = delete;
				Reftracker(Refbank &, IDRef const);
				Reftracker(Reftracker const &) = delete;
				Reftracker(Reftracker &&) = default;
				Reftracker &operator=(Reftracker const &) = delete;
				Reftracker &operator=(Reftracker &&) = default;
				~Reftracker() = default;
			
			public:
				void increase();
				void decrease();
			
				Refbank
					&bank;
			
				IDRef const
					identifier;
			
				void
					*pointer;
			
			private:
				std::size_t
					m_count;
		};
		
		class Refbank
		{
			public:
				virtual ~Refbank() = default;
			
			public:
				template<typename TReference, typename TTracker, typename... TArgs>
				TReference createReference(IDRef const identifier, TArgs &&... args)
				{
					static_assert(std::is_base_of<Reference, TReference>{}, "Ref must derive from Reference");
					static_assert(std::is_base_of<Reftracker, TTracker>{}, "Ref tracker must derive from Reftracker");
					
					auto it(m_trackers.find(identifier));
					
					if (it == m_trackers.end())
					{
						it = m_trackers.emplace
						(
							identifier,
							std::make_unique<TTracker>
							(
								*this,
								identifier,
								std::forward<TArgs>(args)...
							)
						).first;
						
						it->second->pointer = getPointerForReference(*it->second);
					}
					
					return TReference{*it->second};
				}
			
				void refreshReferences();
			
			protected:
				virtual void *getPointerForReference(Reftracker const &) = 0;
				virtual bool onRefcountReachedZero(Reftracker const &) = 0;
			
			private:
				std::map<IDRef, std::unique_ptr<Reftracker>> m_trackers;
			
			friend class Reference;
			friend class Reftracker;
		};
		
		class Reference
		{
			public:
				Reference() = default;
				Reference(Reference const &);
				Reference(Reference &&);
				Reference &operator=(Reference const &);
				Reference &operator=(Reference &&);
				~Reference();
			
			protected:
				Reference(Reftracker &tracker) : tracker{&tracker} {}
			
			public:
				void *getVoidPointer() const noexcept { return (tracker) ? tracker->pointer : nullptr; }
			
			public:
				Reftracker
					*tracker{nullptr};
			
			friend class Refbank;
		};
		
		template<typename T>
		class RefBase : public Reference
		{
			protected:
				using Reference::Reference;
			
			public:
				T       *getRawPointer()       noexcept { return static_cast<T *>(this->getVoidPointer()); }
				T const *getRawPointer() const noexcept { return static_cast<T *>(this->getVoidPointer()); }
			
				T       *operator->()       noexcept { return getRawPointer(); }
				T const *operator->() const noexcept { return getRawPointer(); }
			
				operator bool() const noexcept { return getRawPointer(); }
		};
	}
#endif
