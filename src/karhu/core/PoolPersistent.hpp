/**
 * @author		Ava Skoog
 * @date		2018-01-10
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_CORE_POOL_PERSISTENT_H_
	#define KARHU_CORE_POOL_PERSISTENT_H_

	#include <karhu/core/Buffer.hpp>
	#include <karhu/core/Byte.hpp>

	#include <map>
	#include <vector>
	#include <memory>
	#include <cmath>

	namespace karhu
	{
		/**
		 * Memory pool that guarantees persistent memory addresses for
		 * instances by keeping track of vacant indices instead of using
		 * the swap trick, growing and shrinking as necessary to fit them,
		 * and cataloguing instances by ID of a chosen type.
		 *
		 * The count template parameter is to choose the size of each
		 * block size that is filled up until the pool needs to grow.
		 * This size is in instance count and not in bytes and so the
		 * actual byte size of each block depends on the stored type.
		 */
		template<typename TID, typename T, std::size_t Count>
		class PoolPersistent
		{
			private:
				using IndexInstance = std::size_t;
				using IndexSubpool  = std::size_t;
			
			public:
				PoolPersistent() { addBufferAtEnd(); }
			
				template<typename... Ts>
				T *create(const TID &ID, Ts &&... args)
				{
					if (full())
						addBufferAtEnd();
					
					IndexInstance index;
					
					if (0 != m_vacancies.size())
					{
						index = m_vacancies.back();
						m_vacancies.pop_back();
						m_vacanciesByBuffer.erase(vacancyByBuffer(index));
					}
					else
						index = (m_end ++);
					
					T *r{buffer(index)};
					
					new(r) T{std::forward<Ts>(args)...};
					
					m_instances.emplace(ID, index);
					
					return r;
				}
			
				T *get(const TID &ID)
				{
					auto it(m_instances.find(ID));
					
					if (it != m_instances.end())
						return buffer(it->second);
					
					return nullptr;
				}
			
				bool destroy(const TID &ID)
				{
					auto it(m_instances.find(ID));
					
					if (it != m_instances.end())
					{
						buffer(it->second)->~T();
						
						if (it->second == m_end)
							-- m_end;
						else
						{
							m_vacancies.emplace_back(it->second);
							m_vacanciesByBuffer.emplace(bufferindex(it->second), it->second);
						}
						
						m_instances.erase(it);
						
						removeBufferAtEndIfEmpty();
						
						return true;
					}
					
					return false;
				}
			
				void clear()
				{
					clean();
					
					m_buffers.clear();
					m_instances.clear();
					m_vacancies.clear();
					m_vacanciesByBuffer.clear();
					
					addBufferAtEnd();
					
					m_end = 0;
				}
			
				~PoolPersistent()
				{
					clean();
				}
			
			private:
				void addBufferAtEnd()
				{
					constexpr std::size_t s{Count * sizeof(T)};
					m_buffers.emplace_back(Buffer<Byte>{std::make_unique<Byte[]>(s), s});
				}
			
				void removeBufferAtEndIfEmpty()
				{
					if (m_buffers.empty())
						return;
						
					const std::size_t b{m_buffers.size() - 1};
					
					if (m_end < Count * b)
					{
						m_buffers.erase(m_buffers.begin() + b);
				
						auto jt(m_vacanciesByBuffer.equal_range(b));
				
						if (jt.first != jt.second)
						{
							for (auto kt(jt.first); kt != jt.second; ++ kt)
							{
								auto lt(std::find(m_vacancies.begin(), m_vacancies.end(), kt->second));
								if (lt != m_vacancies.end())
									m_vacancies.erase(lt);
							}
						
							m_vacanciesByBuffer.erase(jt.first, jt.second);
						}
					}
				}
			
				std::size_t count() const noexcept
				{
					return Count * m_buffers.size();
				}
			
				bool full() const noexcept
				{
					return (m_end == count() && 0 == m_vacancies.size());
				}
			
				std::size_t bufferindex(const IndexInstance &i) noexcept
				{
					return static_cast<std::size_t>
					(
						std::floor
						(
							static_cast<float>(i) /
							static_cast<float>(Count)
						)
					);
				}
			
				T *buffer(const IndexInstance &i)
				{
					return reinterpret_cast<T *>(m_buffers[bufferindex(i)].data.get()) + i % Count;
				}
			
				auto vacancyByBuffer(const IndexInstance &i)
				{
					auto it(m_vacanciesByBuffer.equal_range(bufferindex(i)));
					return std::find_if(it.first, it.second, [&i](const auto &v)
					{
						return (v.second == i);
					});
				}
			
				void clean()
				{
					while (m_instances.size() > 0)
						destroy(m_instances.begin()->first);
				}
			
			private:
				std::vector<Buffer<Byte>> m_buffers;
			
				// Marks the last index used;
				// any previous indices since freed
				// will be stored in the below vector.
				IndexInstance m_end{0};
			
				// Lists of indices in use.
				std::map<TID, IndexInstance> m_instances;
			
				// List of slots free to use.
				std::vector<IndexInstance> m_vacancies;
				std::multimap<IndexSubpool, IndexInstance> m_vacanciesByBuffer;
		};
	}
#endif
