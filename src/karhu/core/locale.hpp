/**
 * @author		Ava Skoog
 * @date		2017-04-15
 * @copyright   2017-2023 Ava Skoog
 */

/**
 * Language codes can be listed on a UNIX system like so:
 * $ locale -a
 */

#ifndef KARHU_CORE_LOCALE_H_
	#define KARHU_CORE_LOCALE_H_

	#include <locale>

	namespace karhu
	{
		namespace locale
		{
			/**
			 * Attempting to interact with invalid C++ locales will crash the program.
			 * This utility function makes sure that the locale is safe first.
			 *
			 * @param locale The locale to validate.
			 *
			 * @return Whether the locale is valid.
			 */
			inline bool valid(const std::locale &locale) noexcept
			{
				try
				{
					locale.name();
				}
				catch (std::runtime_error &e)
				{
					return false;
				}
				
				return true;
			}
		}
	}
#endif
