/**
 * @author		Ava Skoog
 * @date		2017-07-03
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: The code that writes to a log file is temporary since I need it for Windows testing. Should be fixed up later.

#include <karhu/core/log.hpp>
#include <karhu/core/platform.hpp>

#ifdef KARHU_PLATFORM_ANDROID
	#include <android/log.h>
#else
	#include <iostream>
#endif

#include <iomanip>

namespace karhu
{
	namespace log
	{
		Implementation::~Implementation()
		{
			// To make sure the implementation is not removed during logging.
			/// @todo: Removing this for now because it crashes the program at quit. Find a solution!
//			std::lock_guard<std::mutex> lock{Logger::c_mutex};

			if (this == Logger::c_implementation)
				Logger::implementation(nullptr);
		}

		Logger::~Logger()
		{
			using namespace std::literals::string_literals;
			
			// For threadsafe logging.
			std::lock_guard<std::mutex> lock{c_mutex};
			
			#ifdef KARHU_PLATFORM_WINDOWS
				if (!fout.is_open())
					fout.open("karhulog.txt", std::ofstream::trunc);
			#endif

			// Implementation overrides default functionality.
			if (c_implementation)
				if (!c_implementation->log(m_level, m_tag, m_stream.str()))
					return;

			// For filling out the tag margin to line up output when tag width differs.
			constexpr int w{24}, wt{w - 2};
			constexpr char f{' '};
			
			// Format the tag.
			const std::string tag
			{
				"["s +
				((m_tag.length() <= wt) ? m_tag : m_tag.substr(0, wt)) +
				"] "
			};

			// Write everything to a stream.
			std::string text;
			{
				std::stringstream s;
				
				s << std::boolalpha;
				
				if (m_tag.length() > 0)
				{
					s << std::setfill(f) << std::setw(w) << std::left << tag;
				
					switch (m_level)
					{
						case Level::msg:
							s << "     ";
							break;
							
						case Level::dbg:
							s << "DBG: ";
							break;
						
						case Level::err:
							s << "ERR: ";
							break;
						
						case Level::wrn:
							s << "WRN: ";
							break;
					}
				}

				s << m_stream.str();
				text = s.str();
			}

			// This is the default functionality.
			// On Android the actual logcat tag is always 'karhu' and the actual
			// tag is appended to the message just like elsewhere, just to make
			// it easy to filter for the single tag 'karhu' when running on Android.
			switch (m_level)
			{
				case Level::msg:
				{
					#ifndef KARHU_PLATFORM_ANDROID
						#ifdef KARHU_PLATFORM_WINDOWS
							if (fout.is_open()) fout << text << '\n';
						#endif
						std::cout << text << '\n';
					#else
						__android_log_print(ANDROID_LOG_INFO, "Karhu", text.c_str());
					#endif
					
					break;
				}

				case Level::dbg:
				{
					#ifdef DEBUG
						#ifndef KARHU_PLATFORM_ANDROID
							#ifdef KARHU_PLATFORM_WINDOWS
								if (fout.is_open()) fout << text << '\n';
							#endif
							std::cout << text << '\n';
						#else
							__android_log_print(ANDROID_LOG_INFO, "Karhu", text.c_str());
						#endif
					#endif
					
					break;
				}

				case Level::err:
				{
					#ifndef KARHU_PLATFORM_ANDROID
						#ifdef KARHU_PLATFORM_WINDOWS
							if (fout.is_open()) fout << text << '\n';
						#endif
						std::cerr << text << '\n';
					#else
						__android_log_print(ANDROID_LOG_ERROR, "Karhu", text.c_str());
					#endif
				
					break;
				}

				case Level::wrn:
				{
					#ifndef KARHU_PLATFORM_ANDROID
						#ifdef KARHU_PLATFORM_WINDOWS
							if (fout.is_open()) fout << text << '\n';
						#endif
						std::cout << text << '\n';
					#else
						__android_log_print(ANDROID_LOG_WARN, "Karhu", text.c_str());
					#endif
				
					break;
				}
			}
		}
		
		Implementation *Logger::c_implementation{nullptr};
		std::mutex      Logger::c_mutex;
		
		#ifdef KARHU_PLATFORM_WINDOWS
			std::ofstream Logger::fout;
		#endif
	}
}
