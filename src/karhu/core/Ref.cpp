/**
 * @author		Ava Skoog
 * @date		2021-05-29
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/core/Ref.hpp>

namespace karhu
{
	/// @todo: Remove core/Ref.cpp if not needed.
	
	Reftracker::Reftracker(Refbank &bank, IDRef const identifier)
	:
	bank      {bank},
	identifier{identifier},
	m_count   {1}
	{
	}
	
	void Reftracker::increase()
	{
		++ m_count;
		
		if (!pointer)
			pointer = bank.getPointerForReference(*this);
	}

	void Reftracker::decrease()
	{
		if (0 == (-- m_count))
			if (bank.onRefcountReachedZero(*this))
				pointer = nullptr;
	}
	
	Reference::Reference(Reference const &o)
	{
		operator=(o);
	}
	
	Reference::Reference(Reference &&o)
	{
		operator=(o);
	}
	
	Reference &Reference::operator=(Reference const &o)
	{
		if (tracker != o.tracker)
			if (tracker)
				tracker->decrease();
		
		tracker = o.tracker;
		
		if (tracker)
			tracker->increase();
		
		return *this;
	}
	
	Reference &Reference::operator=(Reference &&o)
	{
		if (tracker != o.tracker)
			if (tracker)
				tracker->decrease();
		
		tracker   = o.tracker;
		o.tracker = nullptr;
		
		return *this;
	}
	
	Reference::~Reference()
	{
		if (tracker)
			tracker->decrease();
	}
	
	void Refbank::refreshReferences()
	{
		for (auto const &it : m_trackers)
			it.second->pointer = getPointerForReference(*it.second);
	}
}
