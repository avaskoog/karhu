/**
 * @author		Ava Skoog
 * @date		2019-11-12
 * @copyright	2018-2021 Ava Skoog
 */

#ifndef KARHU_CORE_DELTATIMER_H_
	#define KARHU_CORE_DELTATIMER_H_

	#include <chrono>

	namespace karhu
	{
		/**
		 * Debug utility to track deltatime.
		 */
		class Deltatimer
		{
			public:
				Deltatimer() = default;
				Deltatimer(const float timestep) : m_step{timestep} {}
			
				void tick();
				void reset();
			
				float normal() const noexcept { return m_normal; }
				float fixed()  const noexcept { return m_fixed; }
			
				void timestep(const float v) { m_step = v; }
				float timestep() const noexcept { return m_step; }

			private:
				std::chrono::time_point<std::chrono::system_clock> m_last;
			
				float
					m_step  {1.0f / 60.0f},
					m_normal{0.0f},
					m_fixed {0.0f},
					m_accum {0.0f};
		};
	}
#endif
