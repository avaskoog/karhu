/**
 * @author		Ava Skoog
 * @date		2019-10-30
 * @copyright	2018-2021 Ava Skoog
 */

#include <karhu/core/Stopwatch.hpp>
#include <karhu/core/log.hpp>

#include <iomanip>

namespace karhu
{
	std::string Stopwatch::stringify() const
	{
		auto diff(std::chrono::steady_clock::now() - start);
		auto ms(std::chrono::duration_cast<std::chrono::nanoseconds>(diff));
		std::stringstream s;
		s << std::fixed << (static_cast<double>(ms.count()) / 1000000000.0) << std::scientific;
		return s.str();
	}

	void Stopwatch::log(const char *description) const
	{
		std::stringstream s;
		
		if (description)
			s << description;
		
		s << stringify();
		s << 's';
		
		log::dbg() << s.str();
	}
}
