/**
 * @author		Ava Skoog
 * @date		2017-04-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_CORE_STRING_H_
	#define KARHU_CORE_STRING_H_

	#include <vector>
	#include <string>
	#include <sstream>
	#include <codecvt>
	#include <locale>

	namespace karhu
	{
		namespace string
		{
			// Utility to check that a template argument is a valid char type for templates intended to support any type of string.
			template<typename TChar> inline constexpr bool validTypeChar()           noexcept { return false; }
			template<>               inline constexpr bool validTypeChar<char>()     noexcept { return true; }
			template<>               inline constexpr bool validTypeChar<wchar_t>()  noexcept { return true; }
			template<>               inline constexpr bool validTypeChar<char16_t>() noexcept { return true; }
			template<>               inline constexpr bool validTypeChar<char32_t>() noexcept { return true; }

			// Utility to check if it is any type of string.
			template<typename T> inline constexpr bool validTypeString()                              noexcept { return false; }
			template<>           inline constexpr bool validTypeString<char *>()                      noexcept { return true; }
			template<>           inline constexpr bool validTypeString<wchar_t *>()                   noexcept { return true; }
			template<>           inline constexpr bool validTypeString<char16_t *>()                  noexcept { return true; }
			template<>           inline constexpr bool validTypeString<char32_t *>()                  noexcept { return true; }
			template<>           inline constexpr bool validTypeString<std::basic_string<char>>()     noexcept { return true; }
			template<>           inline constexpr bool validTypeString<std::basic_string<wchar_t>>()  noexcept { return true; }
			template<>           inline constexpr bool validTypeString<std::basic_string<char16_t>>() noexcept { return true; }
			template<>           inline constexpr bool validTypeString<std::basic_string<char32_t>>() noexcept { return true; }

			template<typename TTo, typename TFrom>
			std::basic_string<TTo> convert(const std::basic_string<TFrom> &);

			template<>
			inline std::basic_string<char> convert(const std::basic_string<char> &s)
			{
				return s;
			}

			template<>
			inline std::basic_string<char> convert(const std::basic_string<wchar_t> &s)
			{
				return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>{}.to_bytes(s);
			}

			template<>
			inline std::basic_string<char> convert(const std::basic_string<char16_t> &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t>{}.to_bytes(s);
			}

			template<>
			inline std::basic_string<char> convert(const std::basic_string<char32_t> &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.to_bytes(s);
			}

			template<>
			inline std::basic_string<wchar_t> convert(const std::basic_string<char> &s)
			{
				return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>{}.from_bytes(s);
			}

			template<>
			inline std::basic_string<wchar_t> convert(const std::basic_string<wchar_t> &s)
			{
				return s;
			}

			template<>
			inline std::basic_string<wchar_t> convert(const std::basic_string<char16_t> &s)
			{
				return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<wchar_t> convert(const std::basic_string<char32_t> &s)
			{
				return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char16_t> convert(const std::basic_string<char> &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t>{}.from_bytes(s);
			}

			template<>
			inline std::basic_string<char16_t> convert(const std::basic_string<wchar_t> &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char16_t> convert(const std::basic_string<char16_t> &s)
			{
				return s;
			}

			template<>
			inline std::basic_string<char16_t> convert(const std::basic_string<char32_t> &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char32_t> convert(const std::basic_string<char> &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.from_bytes(s);
			}

			template<>
			inline std::basic_string<char32_t> convert(const std::basic_string<wchar_t> &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char32_t> convert(const std::basic_string<char16_t> &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char32_t> convert(const std::basic_string<char32_t> &s)
			{
				return s;
			}
			
			template<typename TTo, typename TFrom>
			std::basic_string<TTo> convert(const TFrom *);

			template<>
			inline std::basic_string<char> convert(const char *s)
			{
				return s;
			}

			template<>
			inline std::basic_string<char> convert(const wchar_t *s)
			{
				return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>{}.to_bytes(s);
			}

			template<>
			inline std::basic_string<char> convert(const char16_t *s)
			{
				return std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t>{}.to_bytes(s);
			}

			template<>
			inline std::basic_string<char> convert(const char32_t *s)
			{
				return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.to_bytes(s);
			}

			template<>
			inline std::basic_string<wchar_t> convert(const char *s)
			{
				return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>{}.from_bytes(s);
			}

			template<>
			inline std::basic_string<wchar_t> convert(const wchar_t *s)
			{
				return s;
			}

			template<>
			inline std::basic_string<wchar_t> convert(const char16_t *s)
			{
				return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<wchar_t> convert(const char32_t *s)
			{
				return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char16_t> convert(const char *s)
			{
				return std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t>{}.from_bytes(s);
			}

			template<>
			inline std::basic_string<char16_t> convert(const wchar_t *s)
			{
				return std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char16_t> convert(const char16_t *s)
			{
				return s;
			}

			template<>
			inline std::basic_string<char16_t> convert(const char32_t *s)
			{
				return std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char32_t> convert(const char *s)
			{
				return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.from_bytes(s);
			}

			template<>
			inline std::basic_string<char32_t> convert(const wchar_t *s)
			{
				return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char32_t> convert(const char16_t *s)
			{
				return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char32_t> convert(const char32_t *s)
			{
				return s;
			}

			template<typename TTo, typename TFrom>
			std::basic_string<TTo> convert(const TFrom &);

			template<>
			inline std::basic_string<char> convert(const char &s)
			{
				std::basic_string<char> r;
				r += s;
				return r;
			}

			template<>
			inline std::basic_string<char> convert(const wchar_t &s)
			{
				return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>{}.to_bytes(s);
			}

			template<>
			inline std::basic_string<char> convert(const char16_t &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t>{}.to_bytes(s);
			}

			template<>
			inline std::basic_string<char> convert(const char32_t &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.to_bytes(s);
			}

			template<>
			inline std::basic_string<wchar_t> convert(const char &s)
			{
				return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>{}.from_bytes(s);
			}

			template<>
			inline std::basic_string<wchar_t> convert(const wchar_t &s)
			{
				std::basic_string<wchar_t> r;
				r += s;
				return r;
			}

			template<>
			inline std::basic_string<wchar_t> convert(const char16_t &s)
			{
				return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<wchar_t> convert(const char32_t &s)
			{
				return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char16_t> convert(const char &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t>{}.from_bytes(s);
			}

			template<>
			inline std::basic_string<char16_t> convert(const wchar_t &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char16_t> convert(const char16_t &s)
			{
				std::basic_string<char16_t> r;
				r += s;
				return r;
			}

			template<>
			inline std::basic_string<char16_t> convert(const char32_t &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char32_t> convert(const char &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.from_bytes(s);
			}

			template<>
			inline std::basic_string<char32_t> convert(const wchar_t &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char32_t> convert(const char16_t &s)
			{
				return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.from_bytes(convert<char>(s));
			}

			template<>
			inline std::basic_string<char32_t> convert(const char32_t &s)
			{
				std::basic_string<char32_t> r;
				r += s;
				return r;
			}
			
			template<typename TChar>
			inline std::basic_string<TChar> escapeQuotes(const std::basic_string<TChar> &s)
			{
				static_assert(validTypeChar<TChar>(), "Invalid character type");
				
				std::basic_string<TChar> r;
				
				for (const auto &c : s)
				{
					if (c == static_cast<TChar>('"'))
					{
						r.push_back(static_cast<TChar>('\\'));
						r.push_back(static_cast<TChar>('"'));
					}
					else
						r.push_back(c);
				}
				
				return r;
			}
			
			template<typename TChar>
			inline std::basic_string<TChar> escapeQuotesDouble(const std::basic_string<TChar> &s)
			{
				static_assert(validTypeChar<TChar>(), "Invalid character type");
				
				std::basic_string<TChar> r;
				
				for (const auto &c : s)
				{
					if (c == static_cast<TChar>('"'))
					{
						r.push_back(static_cast<TChar>('\\'));
						r.push_back(static_cast<TChar>('\\'));
						r.push_back(static_cast<TChar>('\\'));
						r.push_back(static_cast<TChar>('"'));
					}
					else
						r.push_back(c);
				}
				
				return r;
			}
			
			template<typename TChar>
			inline std::vector<std::basic_string<TChar>> split
			(
				const std::basic_string<TChar> &s,
				const TChar token
			)
			{
				static_assert(validTypeChar<TChar>(), "Invalid character type");
				
				std::vector<std::basic_string<TChar>> r;
				
				for (std::size_t i{0}, last{0}; i < s.length(); ++ i)
				{
					if (token == s[i])
					{
						r.emplace_back(s.substr(last, i - last));
						last = i + 1;
					}
					else if (i == (s.length() - 1) && last != i)
						r.emplace_back(s.substr(last));
				}
				
				return r;
			}
			
			template<typename TChar>
			inline std::basic_string<TChar> join
			(
				const std::vector<std::basic_string<TChar>> &s,
				const TChar token
			)
			{
				static_assert(validTypeChar<TChar>(), "Invalid character type");
				
				std::basic_string<TChar> r;
				
				for (std::size_t i{0}; i < s.size(); ++ i)
				{
					r += s[i];
					
					if ((i + 1) < s.size())
						r.push_back(token);
				}
				
				return r;
			}
			
			/**
			 * A character width agnostic string class for convenience when
			 * writing template library code that is to be equally agnostic.
			 * Not meant for use elsewhere, as native STL strings are used.
			 */
			template<typename TChar>
			class String
			{
				private:
					using TString = std::basic_string<TChar>;

				public:
					// Mirror the necessary STL string typedefs.
					using value_type = TChar;

				public:
					String() = default;
					String(const String &) = default;
					String(String &&) = default;
					String<TChar> &operator=(const String<TChar> &) = default;
					String<TChar> &operator=(String<TChar> &&) = default;

					String(const TString &v)
					:
					m_string{v}
					{
					}

					String(TString &&v)
					:
					m_string{std::move(v)}
					{
					}

					String(const TChar *v)
					:
					m_string{v}
					{
					}
				
					String &operator=(const TString &v)
					{
						m_string = v;
						return *this;
					}
				
					String &operator=(TString &&v)
					{
						m_string = std::move(v);
						return *this;
					}
				
					String &operator=(const TChar *v)
					{
						m_string = v;
						return *this;
					}

					template<typename T, typename = typename std::enable_if<!std::is_same<TChar, T>{}>>
					String(const std::basic_string<T> &v)
					:
					m_string{convert<TChar>(v)}
					{
					}

					template<typename T, typename = typename std::enable_if<!std::is_same<TChar, T>{}>>
					String(const T *v)
					:
					m_string{convert<TChar>(v)}
					{
					}
				
					template<typename T, typename = typename std::enable_if<!std::is_same<TChar, T>{}>>
					String &operator=(const T *v)
					{
						m_string = convert<TChar>(v);
						return *this;
					}
				
					operator TString() { return m_string; }
					operator TString() const { return m_string; }

					TString &underlying() noexcept { return m_string; }
					const TString &underlying() const noexcept { return m_string; }

					// Mirror the necessary STL string methods.
					std::size_t length() const { return m_string.length(); }
					std::size_t size() const { return m_string.size(); }
					TChar &operator[](const std::size_t index) { return m_string[index]; }
					const TChar &operator[](const std::size_t index) const { return m_string[index]; }
					bool operator==(const TString &o) const { return (m_string == o); }
					bool operator!=(const TString &o) const { return (m_string != o); }
					bool operator<(const TString &o) const { return (m_string < o); }
					bool operator>(const TString &o) const { return (m_string > o); }
					bool operator<=(const TString &o) const { return (m_string <= o); }
					bool operator>=(const TString &o) const { return (m_string >= o); }
					const TChar *c_str() const { return m_string.c_str(); }
					const TChar *data() const { return m_string.c_str(); }
				
					typename TString::iterator begin() { return m_string.begin(); }
					typename TString::const_iterator begin() const { return m_string.begin(); }
					typename TString::iterator end() { return m_string.end(); }
					typename TString::const_iterator end() const { return m_string.end(); }
				
					typename TString::reverse_iterator rbegin() { return m_string.rbegin(); }
					typename TString::const_reverse_iterator rbegin() const { return m_string.rbegin(); }
					typename TString::reverse_iterator rend() { return m_string.rend(); }
					typename TString::const_reverse_iterator rend() const { return m_string.rend(); }
				
					typename TString::const_iterator cbegin() const noexcept { return m_string.cbegin(); }
					typename TString::const_iterator cend() const noexcept { return m_string.cend(); }
				
					typename TString::const_reverse_iterator crbegin() const noexcept { return m_string.crbegin(); }
					typename TString::const_reverse_iterator crend() const noexcept { return m_string.crend(); }

				private:
					TString m_string;
			};
			
			/**
			 * A stringstream implementation for any character width
			 * that also accepts input or output of any width, making
			 * it handy to use when the width is unknown, especially
			 * when hardcoded literals need to be appended, in which
			 * case there is no need for manual conversion or specification.
			 */
			template<typename TChar>
			class Stream
			{
				static_assert(validTypeChar<TChar>(), "Invalid character type");

				public:
					template<typename T>
					std::enable_if_t<(!validTypeChar<T>() && !std::is_array<T>{}), Stream &> operator<<(const T &v)
					{
						// Fall back to default implementation.
						std::stringstream s;
						s << v;
						m_string += convert<TChar>(s.str());
						return *this;
					}

					template<typename T>
					std::enable_if_t<std::is_same<TChar, T>{}, Stream &> operator<<(const std::basic_string<T> &v)
					{
						m_string += v;
						return *this;
					}
				
					template<typename T>
					std::enable_if_t<!std::is_same<TChar, T>{}, Stream &> operator<<(const std::basic_string<T> &v)
					{
						m_string += convert<TChar>(v);
						return *this;
					}

					template<typename T>
					std::enable_if_t<std::is_same<TChar, T>{}, Stream &> operator<<(const String<T> &v)
					{
						m_string += static_cast<std::basic_string<T>>(v);
						return *this;
					}
				
					template<typename T>
					std::enable_if_t<!std::is_same<TChar, T>{}, Stream &> operator<<(const String<T> &v)
					{
						m_string += convert<TChar>(static_cast<std::basic_string<T>>(v));
						return *this;
					}

					template<typename T>
					std::enable_if_t<(!std::is_same<T, TChar>{} && (sizeof(TChar) < sizeof(T)) && validTypeChar<T>()), Stream &> operator<<(const T &c)
					{
						m_string += convert<TChar>(c);
						return *this;
					}

					template<typename T>
					std::enable_if_t<(!std::is_same<T, TChar>{} && (sizeof(TChar) >= sizeof(T)) && validTypeChar<T>()), Stream &> operator<<(const T &c)
					{
						m_string.push_back(static_cast<TChar>(c));
						return *this;
					}

					template<typename T>
					std::enable_if_t<std::is_same<T, TChar>{}, Stream &> operator<<(const T &c)
					{
						m_string.push_back(c);
						return *this;
					}
				
					template<typename T>
					std::enable_if_t<(std::is_same<TChar, T>{} && validTypeChar<T>()), Stream &> operator<<(const T *v)
					{
						m_string += v;
						return *this;
					}

					template<typename T>
					std::enable_if_t<(!std::is_same<TChar, T>{} && validTypeChar<T>()), Stream &> operator<<(const T *v)
					{
						m_string += convert<TChar>(v);
						return *this;
					}
				
					template<typename T>
					Stream &operator>>(T &v)
					{
						// Fall back to default implementation.
						std::stringstream s;
						s << convert<char>(m_string);
						s >> v;
						return *this;
					}
				
					template<typename T>
					Stream &operator>>(T *v)
					{
						// Fall back to default implementation.
						std::stringstream s;
						s << convert<char>(m_string);
						s >> v;
						return *this;
					}
				
					template<typename T>
					std::enable_if_t<(!std::is_same<TChar, T>{} && validTypeChar<T>()), std::basic_string<T>> str() const
					{
						return convert<T>(m_string);
					}
				
					template<typename T = TChar>
					std::enable_if_t<std::is_same<TChar, T>{}, const std::basic_string<T> &> str() const
					{
						return m_string;
					}
				
					template<typename T>
					std::enable_if_t<(!std::is_same<TChar, T>{} && validTypeChar<T>()), Stream &> str(const std::basic_string<T> &v)
					{
						m_string = convert<TChar>(v);
						return *this;
					}
				
					template<typename T = TChar>
					std::enable_if_t<std::is_same<TChar, T>{}, Stream &> str(const std::basic_string<T> &v)
					{
						m_string = v;
						return *this;
					}
				
					template<typename T>
					std::enable_if_t<(!std::is_same<TChar, T>{} && validTypeChar<T>()), Stream &> str(const T *v)
					{
						m_string = convert<TChar>(v);
						return *this;
					}
				
					template<typename T = TChar>
					std::enable_if_t<std::is_same<TChar, T>{}, Stream &> str(const T *v)
					{
						m_string = v;
						return *this;
					}
				
				private:
					// We use a string of the proper type underlyingly.
					std::basic_string<TChar> m_string;
			};
		}
	}
#endif
