/**
 * @author		Ava Skoog
 * @date		2019-05-19
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_BITPAIR_H_
	#define KARHU_BITPAIR_H_

	#include <limits>

	namespace karhu
	{
		using TBitpair = std::uint32_t;
		
		enum class Bitpair : TBitpair {};
		
		inline constexpr TBitpair bitpairRange() noexcept
		{
			return (1 << (std::numeric_limits<TBitpair>::digits / 2)) - 1;
		}
		
		inline constexpr TBitpair bitpairFirst(const Bitpair &p) noexcept
		{
			return static_cast<TBitpair>(p) & ~bitpairRange();
		}
		
		inline constexpr TBitpair bitpairSecond(const Bitpair &p) noexcept
		{
			return static_cast<TBitpair>(p) & ~(std::numeric_limits<TBitpair>::max() & ~bitpairRange());
		}
		
		inline constexpr void bitpairFirst(Bitpair &p, TBitpair value) noexcept
		{
			value &= ~(std::numeric_limits<TBitpair>::max() & ~bitpairRange());
			
			p = static_cast<Bitpair>
			(
				(value << (std::numeric_limits<TBitpair>::digits / 2)) |
				bitpairSecond(p)
			);
		}
		
		inline constexpr void bitpairSecond(Bitpair &p, TBitpair value) noexcept
		{
			value &= ~(std::numeric_limits<TBitpair>::max() & ~bitpairRange());
			
			p =  static_cast<Bitpair>
			(
				bitpairFirst(p) |
				value
			);
		}
	}

	template<typename TBitpair>
	inline constexpr bool operator<(const karhu::Bitpair &a, const karhu::Bitpair &b) noexcept
	{
		return static_cast<TBitpair>(a) < static_cast<TBitpair>(b);
	}

	template<typename TBitpair>
	inline constexpr bool operator>(const karhu::Bitpair &a, const karhu::Bitpair &b) noexcept
	{
		return static_cast<TBitpair>(a) > static_cast<TBitpair>(b);
	}

	template<typename TBitpair>
	inline constexpr bool operator==(const karhu::Bitpair &a, const karhu::Bitpair &b) noexcept
	{
		return static_cast<TBitpair>(a) == static_cast<TBitpair>(b);
	}

	template<typename TBitpair>
	inline constexpr bool operator!=(const karhu::Bitpair &a, const karhu::Bitpair &b) noexcept
	{
		return static_cast<TBitpair>(a) != static_cast<TBitpair>(b);
	}
#endif
