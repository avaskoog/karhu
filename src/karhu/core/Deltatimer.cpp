/**
 * @author		Ava Skoog
 * @date		2019-11-12
 * @copyright	2018-2021 Ava Skoog
 */

#include <karhu/core/Deltatimer.hpp>

namespace karhu
{
	void Deltatimer::reset()
	{
		m_normal =
		m_fixed  =
		m_accum  = 0.0f;
		
		m_last = std::chrono::system_clock::now();
	}
	
	void Deltatimer::tick()
	{
		const auto now
		{
			std::chrono::system_clock::now()
		};
		
		const auto diff
		(
			static_cast<float>
			(
				std::chrono::duration_cast<std::chrono::milliseconds>
				(
					now - m_last
				).count()
			) / 1000.0f
		);

		m_normal = (diff < m_step) ? diff : m_step;
		m_fixed  = 0.0f;
		m_accum += m_normal;

		while (m_accum >= m_step)
		{
			m_accum -= m_step;
			m_fixed  += m_step;
		}

		m_last = now;
	}
}
