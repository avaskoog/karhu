/**
 * @author		Ava Skoog
 * @date		2018-10-29
 * @copyright	2018-2021 Ava Skoog
 */

#ifndef KARHU_CORE_RESULT_H_
	#define KARHU_CORE_RESULT_H_

	#include <string>

	namespace karhu
	{
		/**
		 * Used to provide a success state alongside a return value.
		 * In case of failure, the error string will be filled in.
		 */
		template<typename T>
		struct Result
		{
			T value;
			bool success;
			std::string error;
		};
		
		/**
		 * Used to provide a success state without a return value.
		 * In case of failure, the error string will be filled in.
		 */
		struct Status
		{
			bool success;
			std::string error;
		};
	}
#endif
