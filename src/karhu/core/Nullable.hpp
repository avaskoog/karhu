/**
 * @author		Ava Skoog
 * @date		2017-04-04
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_CORE_NULLABLE_H_
	#define KARHU_CORE_NULLABLE_H_

	#include <utility>
	#include <type_traits>
	#include <stdexcept>

	namespace karhu
	{
		/**
		 * Used to return a non-pointer value that may be considered invalid,
		 * using a smart pointer syntax for consistency with functions that
		 * actually do return nullable pointers to express an invalid state.
		 *
		 * After writing this class, I found out about std::optional, which
		 * I would prefer to use instead, but unfortunately Karhu is meant to
		 * be C++14 compatible for now. An update might happen in the future,
		 * in which case it would be necessary to change construction of any
		 * instance of Nullable, but hopefully not necessary to change any
		 * use of one, as std::optional also appears to behave like a pointer.
		 */
		template<typename T>
		class Nullable
		{
			static_assert(!std::is_pointer<T>{} && !std::is_reference<T>{}, "Nullable type needs to be primitive");

			public:
				/**
				 * Creates an invalid nullable.
				 */
				Nullable() = default;

				/**
				 * Sets the state and initialises the underlying value through perfect forwarding.
				 *
				 * @param valid If false, this will be treated like a null pointer; false if omitted.
				 * @param args  Arguments to be passed to the underlying value's constructor.
				 */
				template<typename... Args>
				Nullable(bool valid, Args... args)
				:
				m_valid{valid},
				m_value{std::forward<Args>(args)...}
				{
				}

				/**
				 * Returns whether null, or invalid.
				 *
				 * @return Whether the value is valid.
				 */
				operator bool() const noexcept { return m_valid; }

				/**
				 * Tries to dereference the underlying value.
				 * Throws an exception if invalid to emulate a real pointer.
				 *
				 * @return The underlying value.
				 *
				 * @throws std::runtime_error
				 */
				T &operator*()
				{
					if (!m_valid) throw std::runtime_error{"Dereferencing invalid nullable"};
					return m_value;
				}

				/**
				 * Tries to dereference the underlying value.
				 * Throws an exception if invalid to emulate a real pointer.
				 *
				 * @return The underlying value.
				 *
				 * @throws std::runtime_error
				 */
				const T &operator*() const
				{
					if (!m_valid) throw std::runtime_error{"Dereferencing invalid nullable"};
					return m_value;
				}

				/**
				 * Provides member access into the underlying value.
				 * The value will be treated as null if invalid, so
				 * member access into an invalid value is not supported.
				 *
				 * @return A pointer (in)to underlying value.
				 */
				T *operator->() noexcept { return (m_valid) ? &m_value : nullptr; }

				/**
				 * Provides member access into the underlying value.
				 * The value will be treated as null if invalid, so
				 * member access into an invalid value is not supported.
				 *
				 * @return A pointer (in)to underlying value.
				 */
				const T *operator->() const noexcept { return (m_valid) ? &m_value : nullptr; }

			private:
				const bool m_valid{false};
				T m_value;
		};
	}
#endif
