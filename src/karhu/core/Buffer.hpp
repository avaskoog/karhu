/**
 * @author		Ava Skoog
 * @date		2018-10-28
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_CORE_BUFFER_H_
	#define KARHU_CORE_BUFFER_H_

	#include <vector>
	#include <array>
	#include <memory>

	namespace karhu
	{
		template<typename T>
		struct Bufferview
		{
			T *data{nullptr};
			std::size_t size{0};
			
			std::size_t count() const noexcept
			{
				return size / sizeof(T);
			}
		};
		
		struct ArrayviewBasic
		{
			void *address{nullptr};
			
			std::int32_t
				count{0},
				sizeElement{0}; // Slight overhead but easy way to make play nicely with AngelScript.
			
			bool empty() const noexcept
			{
				return (!address || 0 == count);
			}
		};
		
		template<typename T>
		struct Arrayview : public ArrayviewBasic
		{
			Arrayview(T *address = nullptr, std::int32_t const count = 0)
			:
			ArrayviewBasic
			{
				static_cast<void *>
				(
					/// @todo: Why did I remove const?
					const_cast<std::remove_const_t<T> *>
					(
						address
					)
				),
				count,
				sizeof(T)
			}
			{
			}
			
			Arrayview(std::vector<T> &vector)
			:
			ArrayviewBasic
			{
				static_cast<void *>(vector.data()),
				static_cast<std::int32_t>(vector.size()),
				sizeof(T)
			}
			{
			}
			
			Arrayview(std::vector<T> const &vector)
			:
			ArrayviewBasic
			{
				static_cast<void *>(vector.data()),
				static_cast<std::int32_t>(vector.size()),
				sizeof(T)
			}
			{
			}
			
			template<std::size_t N>
			Arrayview(std::array<T, N> &array)
			:
			ArrayviewBasic
			{
				static_cast<void *>(array.data()),
				static_cast<std::int32_t>(N),
				sizeof(T)
			}
			{
			}
			
			template<std::size_t N>
			Arrayview(std::array<T, N> const &array)
			:
			ArrayviewBasic
			{
				static_cast<void *>(array.data()),
				static_cast<std::int32_t>(N),
				sizeof(T)
			}
			{
			}
			
			T *data() { return static_cast<T *>(address); }
			T const *data() const { return static_cast<T *>(address); }
			
			T &operator[](std::int32_t const &index)
			{
				return data()[index];
			}
			
			T const &operator[](std::int32_t const &index) const
			{
				return data()[index];
			}
			
			std::int32_t size() const noexcept
			{
				return count * sizeof(T);
			}
		};
		
		template<typename T>
		struct Buffer
		{
			std::unique_ptr<T[]> data;
			std::size_t size{0};
			
			std::size_t count() const noexcept
			{
				return size / sizeof(T);
			}
			
			Bufferview<T> bufferview() const
			{
				return {data.get(), size};
			}
			
			Arrayview<T> arrayview() const
			{
				return {data.get(), count()};
			}
			/*
			Buffer() = default;
			
			Buffer(std::unique_ptr<T[]> &&data, const std::size_t &size)
			:
			data{std::move(data)},
			size{size}
			{
			}
			
			Buffer(Buffer<T> &&) = default;
			Buffer<T> &operator=(Buffer<T> &&) = default;
			
			Buffer(const Buffer<T> &o)
			{
				operator=(o);
			}
		
			Buffer<T> &operator=(const Buffer<T> &o)
			{
				size = o.size;
			 
				if (!o.data.get())
					data.reset();
				else
				{
					data = std::make_unique<T[]>(size);
					std::memcpy(data.get(), o.data.get(), size);
				}
			 
				return *this;
			}*/
		};
	}
#endif
