/**
 * @author		Ava Skoog
 * @date		2018-10-18
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Just use std::byte should we ever move over to C++17.

#ifndef KARHU_CORE_BYTE_H_
	#define KARHU_CORE_BYTE_H_

	#include <iostream>
	#include <type_traits>
	#include <cstdint>

	namespace karhu
	{
		enum class Byte : std::uint8_t {};
	}

	inline std::ostream &operator<<(std::ostream &s, const karhu::Byte &v)
	{
		return s << static_cast<std::uint32_t>(v);
	}

	#define KARHU_DEF_UNOP_BYTE_PREFIX_NOREF(operatorsymbol) \
		inline constexpr karhu::Byte operator operatorsymbol (const karhu::Byte v) noexcept \
		{ \
			using V = std::underlying_type_t<karhu::Byte>; \
			return static_cast<karhu::Byte>( operatorsymbol static_cast<V>(v) ); \
		}

	#define KARHU_DEF_BINOP_BYTE_NOREF(operatorsymbol) \
		template<typename T> \
		inline constexpr std::enable_if_t<std::is_integral<T>{} || std::is_same<karhu::Byte, T>{}, karhu::Byte> \
		operator operatorsymbol (const karhu::Byte a, const T b) noexcept \
		{ \
			using V = std::underlying_type_t<karhu::Byte>; \
			return static_cast<karhu::Byte>(static_cast<V>(a) operatorsymbol static_cast<V>(b)); \
		} \
		\
		template<typename T> \
		inline constexpr std::enable_if_t<std::is_integral<T>{} && !std::is_same<karhu::Byte, T>{}, karhu::Byte> \
		operator operatorsymbol (const T a, const karhu::Byte b) noexcept \
		{ \
			using V = std::underlying_type_t<karhu::Byte>; \
			return static_cast<karhu::Byte>(static_cast<V>(a) operatorsymbol static_cast<V>(b)); \
		}

	#define KARHU_DEF_BINOP_BYTE_REF(operatorassign, operatorsymbol) \
		template<typename T> \
		inline constexpr std::enable_if_t<std::is_integral<T>{} || std::is_same<karhu::Byte, T>{}, karhu::Byte &> \
		operator operatorassign (karhu::Byte &a, const T b) noexcept \
		{ \
			return (a = (a operatorsymbol b)); \
		}

	KARHU_DEF_UNOP_BYTE_PREFIX_NOREF(!)
	KARHU_DEF_UNOP_BYTE_PREFIX_NOREF(+)
	KARHU_DEF_UNOP_BYTE_PREFIX_NOREF(-)
	KARHU_DEF_UNOP_BYTE_PREFIX_NOREF(~)

	KARHU_DEF_BINOP_BYTE_NOREF(+)
	KARHU_DEF_BINOP_BYTE_NOREF(-)
	KARHU_DEF_BINOP_BYTE_NOREF(*)
	KARHU_DEF_BINOP_BYTE_NOREF(/)
	KARHU_DEF_BINOP_BYTE_NOREF(%)
	KARHU_DEF_BINOP_BYTE_NOREF(&&)
	KARHU_DEF_BINOP_BYTE_NOREF(||)
	KARHU_DEF_BINOP_BYTE_NOREF(&)
	KARHU_DEF_BINOP_BYTE_NOREF(|)
	KARHU_DEF_BINOP_BYTE_NOREF(<<)
	KARHU_DEF_BINOP_BYTE_NOREF(>>)
	KARHU_DEF_BINOP_BYTE_NOREF(^)
	KARHU_DEF_BINOP_BYTE_NOREF(==)
	KARHU_DEF_BINOP_BYTE_NOREF(!=)
	KARHU_DEF_BINOP_BYTE_NOREF(<)
	KARHU_DEF_BINOP_BYTE_NOREF(>)
	KARHU_DEF_BINOP_BYTE_NOREF(<=)
	KARHU_DEF_BINOP_BYTE_NOREF(>=)

	KARHU_DEF_BINOP_BYTE_REF(+=, +)
	KARHU_DEF_BINOP_BYTE_REF(-=, -)
	KARHU_DEF_BINOP_BYTE_REF(*=, *)
	KARHU_DEF_BINOP_BYTE_REF(/=, /)
	KARHU_DEF_BINOP_BYTE_REF(%=, %)
	KARHU_DEF_BINOP_BYTE_REF(&=, &)
	KARHU_DEF_BINOP_BYTE_REF(|=, |)
	KARHU_DEF_BINOP_BYTE_REF(<<=, <<)
	KARHU_DEF_BINOP_BYTE_REF(>>=, >>)
	KARHU_DEF_BINOP_BYTE_REF(^=, ^)

	inline constexpr karhu::Byte &operator++(karhu::Byte &v) noexcept
	{
		return (v = (v + 1));
	}

	inline constexpr karhu::Byte &operator--(karhu::Byte &v) noexcept
	{
		return (v = (v - 1));
	}

	inline constexpr karhu::Byte operator++(karhu::Byte &v, int) noexcept
	{
		const karhu::Byte tmp{v};
		v = (v + 1);
		return tmp;
	}

	inline constexpr karhu::Byte operator--(karhu::Byte &v, int) noexcept
	{
		const karhu::Byte tmp{v};
		v = (v - 1);
		return tmp;
	}

	#undef KARHU_DEF_UNOP_BYTE_PREFIX_NOREF
	#undef KARHU_DEF_BINOP_BYTE_NOREF
	#undef KARHU_DEF_BINOP_BYTE_REF
#endif
