/**
 * @author		Ava Skoog
 * @date		2017-07-03
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: The code that writes to a log file is temporary since I need it for Windows testing. Should be fixed up later.

/*
 * All code written for and using Karhu should use these utilities
 * for logging instead of the STL functionality to ensure that the
 * output ends up in the correct place depending on configuration.
 *
 * Logging uses a mutex to be threadsafe so there is no need to do
 * any manual work to make sure that the output does not get jumbled.
 *
 * An appropriate logger function is used to log at the desired level:
 *
 * • print(): log at info level in debug as well as release builds
 * • debug(): log at info level in debug builds only
 * • error(): log at error level
 * • level(): specify a level manually using an enum value
 *
 * These are used just like STL output utilities like std::cout
 * and std::err with the << operator to append any type of value
 * to the stream. RAII is used to write to the appropriate output
 * when the logger object returned by any of the functions described
 * above goes out of scope. Unlike the STL utilities, strings of
 * any width can be appended to the same stream; there are no
 * separate log utilities like std::wcout.
 *
 * Example of usage:
 *
 * karhu::log::msg() << "Hello, world";
 *
 * The logger implementation can be overridden by deriving and
 * implementing the functionality of the abstract class Implementation
 * and passing a pointer to an instantiation of the derived class
 * (which needs to be alive as long as it is to remain in use) to
 * the static function Logger::implementation(). The destructor of
 * the parent class makes sure to unregister the implementation if
 * when it is destroyed, so there is no need to do this manually.
 */

#ifndef KARHU_CORE_LOG_H_
	#define KARHU_CORE_LOG_H_

	#ifdef KARHU_APPLICATION_NAME
		#define karhuLOGTAG KARHU_APPLICATION_NAME
	#else
		#define karhuLOGTAG {}
	#endif

	#include <karhu/core/string.hpp>
	#include <karhu/core/platform.hpp>

	#include <string>
	#include <sstream>
	#include <cstdint>
	#include <mutex>

	#include <fstream>

	namespace karhu
	{
		namespace log
		{
			enum class Level : std::uint8_t
			{
				msg,
				dbg,
				err,
				wrn
			};
			
			/**
			 * Allows one to override the default logger implementation,
			 * for example to route the output through the network when
			 * debugging. Not really meant for custom use.
			 */
			class Implementation
			{
				public:
					/**
					 * Logs the data.
					 *
					 * @param level   The log level. @see Level.
					 * @param tag     The log tag, indicating origin.
					 * @param message The text to log.
					 *
					 * @return Whether to also log normally.
					 */
					virtual bool log
					(
						const Level        level,
						const std::string &tag,
						const std::string &message
					) = 0;

					virtual ~Implementation();
			};

			/**
			 * Used by the logging system. There should be no need to
			 * instantiate manually. Instead use the log functions.
			 */
			class Logger
			{
				public:
					/**
					 * Overrides the default logger implementation with one
					 * provided by a derivation from Implementation, which
					 * must be alive throughout the duration of the program.
					 *
					 * @param i The implementation to use, or null if none.
					 */
					static void implementation(Implementation *const i) noexcept { c_implementation = i; }

					/**
					 * Returns the current implementation, if any, or null.
					 *
					 * @return A pointer to the current implementation or null.
					 */
					static Implementation *implementation() noexcept { return c_implementation; }

					#ifdef KARHU_PLATFORM_WINDOWS
						static std::ofstream fout;
					#endif

				public:
					Logger() : m_level{Level::msg} {}
				
					Logger(const Level level, const string::String<char> &tag)
					:
					m_level{level},
					m_tag  {tag}
					{
					}
				
					Logger(Logger &&) = default;
					Logger(const Logger &) = default;
				
					Logger &operator=(Logger &&) = default;
					Logger &operator=(const Logger &) = default;
				
					template<typename T>
					Logger &operator<<(const T &v)
					{
						m_stream << v;
						return *this;
					}

					~Logger();

				private:
					static Implementation *c_implementation;
					static std::mutex      c_mutex;

				private:
					Level                m_level;
					std::string          m_tag;
					string::Stream<char> m_stream;
				
				friend class Implementation;
			};

			/**
			 * Logs with the selected level and tag.
			 *
			 * @param level The log level.
			 * @param tag   An optional log tag to specify the origin of the message.
			 *
			 * @return A RAII logger object which is used like the STL logging utilities with the << operator and writes to the correct output on destruction.
			 */
			inline Logger lvl(const Level level, const string::String<char> &tag = karhuLOGTAG) { return {level, tag}; }
			
			/**
			 * Logs with the selected tag at print (information) level.
			 *
			 * @param tag An optional log tag to specify the origin of the message.
			 *
			 * @return A RAII logger object which is used like the STL logging utilities with the << operator and writes to the standard output on destruction.
			 */
			inline Logger msg(const string::String<char> &tag = karhuLOGTAG) { return {Level::msg, tag}; }
			
			/**
			 * Logs with the selected tag at debug (information) level,
			 * but only in a debug build; in release nothing is logged.
			 *
			 * @param tag An optional log tag to specify the origin of the message.
			 *
			 * @return A RAII logger object which is used like the STL logging utilities with the << operator and writes to the standard output on destruction.
			 */
			inline Logger dbg(const string::String<char> &tag = karhuLOGTAG) { return {Level::dbg, tag}; }
			
			/**
			 * Logs with the selected tag at error level.
			 *
			 * @param tag An optional log tag to specify the origin of the message.
			 *
			 * @return A RAII logger object which is used like the STL logging utilities with the << operator and writes to the standard error output on destruction.
			 */
			inline Logger err(const string::String<char> &tag = karhuLOGTAG) { return {Level::err, tag}; }
			
			/**
			 * Logs with the selected tag at warning level.
			 *
			 * @param tag An optional log tag to specify the origin of the message.
			 *
			 * @return A RAII logger object which is used like the STL logging utilities with the << operator and writes to the standard error output on destruction.
			 */
			inline Logger wrn(const string::String<char> &tag = karhuLOGTAG) { return {Level::wrn, tag}; }
		}
	}

	#undef karhuLOGTAG
#endif
