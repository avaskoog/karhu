/**
 * @author		Ava Skoog
 * @date		2019-07-19
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GENERATOR_ID_H_
	#define KARHU_GENERATOR_ID_H_

	#include <vector>
	#include <algorithm>

	namespace karhu
	{
		template<typename TID>
		class GeneratorID
		{
			public:
				bool reset(std::vector<TID> used);
				void release(const TID &);
				TID next();

			private:
				TID m_last{0};
				std::vector<TID> m_unused;
		};
		
		/*
		 * Resets the pool of available ID numbers.
		 *
		 * Should be called once at the start after the application
		 * after going through the files containing existing objects
		 * with ID's that need to be marked as used so that any further
		 * ID's generated will not overlap with those.
		 *
		 * @return Whether the generator was successfully reset (i.e. no double ID's encountered).
		 */
		template<typename TID>
		bool GeneratorID<TID>::reset(std::vector<TID> used)
		{
			m_last = TID{0};
			m_unused.clear();

			// Make a list of the unused ID's before the current maximum.
			if (0 != used.size())
			{
				std::sort(used.begin(), used.end());
				
				const auto u(std::unique(used.begin(), used.end()));
				
				if (u != used.end())
				{
					log::err("Karhu") << "Could not initialise world; duplicate ID's found across scenes in application";
					return false;
				}
				
				m_last = used.back();

				for (TID i{0}; i < m_last; ++ i)
					if (std::find(used.begin(), used.end(), i) == used.end())
						m_unused.emplace_back(i);
			}
			
			return true;
		}

		/*
		 * Tells the generator that a previously taken ID is now free for use.
		 */
		template<typename TID>
		void GeneratorID<TID>::release(const TID &value)
		{
			if (std::find(m_unused.begin(), m_unused.end(), value) == m_unused.end())
				m_unused.emplace_back(value);
		}

		/*
		 * Returns a unique ID not yet in use.
		 */
		template<typename TID>
		TID GeneratorID<TID>::next()
		{
			// Use up unused gaps before increasing counter.
			if (0 != m_unused.size())
			{
				const TID n{m_unused.back()};
				m_unused.pop_back();
				return n;
			}
			
			return (++ m_last);
		}
	}
#endif
