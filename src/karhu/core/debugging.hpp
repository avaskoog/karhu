/**
 * @author		Ava Skoog
 * @date		2019-11-17
 * @copyright	2018-2021 Ava Skoog
 */

#ifndef KARHU_CORE_DEBUGGING_H_
	#define KARHU_CORE_DEBUGGING_H_

	#ifdef DEBUG
		#include <karhu/core/log.hpp>
		#include <cstdlib>

		#define karhuASSERT(karhu_assert_val) \
			if (!(karhu_assert_val)) \
			{ \
				karhu::log::err("Karhu") << "Assertion failed on line " << __LINE__ << " in " __FILE__ ": " << #karhu_assert_val; \
				std::abort(); \
			}
	#else
		#define karhuASSERT(x)
	#endif
#endif
