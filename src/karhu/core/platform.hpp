/**
 * @author		Ava Skoog
 * @date		2017-04-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_CORE_PLATFORM_H_
	#define KARHU_CORE_PLATFORM_H_

	#include <karhu/core/meta.hpp>
	
	#include <type_traits>
	#include <cstdint>
	#include <string>

	#if __APPLE__
		#include "TargetConditionals.h"
	#endif

	// Define Karhu platforms so that we can avoid this mess elsewhere.
	#ifdef __ANDROID__
		#define KARHU_PLATFORM_ANDROID
	#elif __EMSCRIPTEN__
		#define KARHU_PLATFORM_WEB
	#elif __APPLE__
		#if TARGET_OS_IPHONE
			#define KARHU_PLATFORM_IOS
		#else
			#define KARHU_PLATFORM_MAC
			#define KARHU_PLATFORM_UNIX
		#endif
	#elif defined(__linux__) || defined(__linux) || defined(linux) || defined(LINUX)
		#define KARHU_PLATFORM_LINUX
		#define KARHU_PLATFORM_UNIX
	#elif defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
		#define KARHU_PLATFORM_WINDOWS
	#else
		#define KARHU_PLATFORM_UNKNOWN
	#endif

	namespace karhu
	{
		enum class Platform : std::uint64_t
		{
			unknown =  0,
			
			mac     =  1 << 0,
			linux   =  1 << 1,
			windows =  1 << 2,
			iOS     =  1 << 3,
			android =  1 << 4,
			web     =  1 << 5,
			
			all     = (1 << 6) - 1, // All available platforms.

			UNIX    = mac | linux,  // For convenience when any UNIX variant works.

			// Needs to be updated when new platforms are added.
			#if defined(KARHU_PLATFORM_MAC)
				current = mac
			#elif defined(KARHU_PLATFORM_LINUX)
				current = linux
			#elif defined(KARHU_PLATFORM_WINDOWS)
				current = windows
			#elif defined(KARHU_PLATFORM_IOS)
				current = iOS
			#elif defined(KARHU_PLATFORM_ANDROID)
				current = android
			#elif defined(KARHU_PLATFORM_WEB)
				current = web
			#else
				current = unknown
			#endif
		};
	}

	// Bitwise operations for the platform bitflags.

	inline constexpr karhu::Platform operator|
	(
		const karhu::Platform &a,
		const karhu::Platform &b
	)
	{
		using T = std::underlying_type_t<karhu::Platform>;
		return static_cast<karhu::Platform>(static_cast<T>(a) | static_cast<T>(b));
	}
	
	inline constexpr karhu::Platform operator&
	(
		const karhu::Platform &a,
		const karhu::Platform &b
	)
	{
		using T = std::underlying_type_t<karhu::Platform>;
		return static_cast<karhu::Platform>(static_cast<T>(a) & static_cast<T>(b));
	}
	
	inline constexpr karhu::Platform &operator|=
	(
		karhu::Platform &a,
		const karhu::Platform &b
	)
	{
		return a = a | b;
	}
	
	inline constexpr karhu::Platform &operator&=
	(
		karhu::Platform &a,
		const karhu::Platform &b
	)
	{
		return a = a & b;
	}
	
	namespace karhu
	{
		namespace platform
		{
			/**
			 * Returns whether the platform is any of the platforms specified
			 * by using bitwise or to concatenate multiple flags.
			 *
			 * Example: is(Platform::current, Platform::iOS | Platform::android)
			 *
			 * @param platform  A single platform to check against one or more other platforms.
			 * @param platforms Bitset of platforms to match.
			 *
			 * @return Whether the platform matches any of those in the bitset.
			 */
			inline constexpr bool is(const Platform &platform, const Platform &platforms)
			{
				return static_cast<bool>(platform & platforms);
			}

			/**
			 * Returns whether the current platform is any of the platforms
			 * specified by using bitwise or to concatenate multiple flags.
			 *
			 * Example: is(Platform::iOS | Platform::android)
			 *
			 * @param platforms Bitset of platforms to match.
			 *
			 * @return Whether the current platform matches any of those in the bitset.
			 */
			inline constexpr bool is(const Platform &platforms)
			{
				return is(Platform::current, platforms);
			}

			/**
			 * Returns the all-lowercase, all-ASCII, no-whitespace string
			 * identifier corresponding to the platform specified.
			 *
			 * @param p The platform.
			 *
			 * @return The string identifier corresponding to the platform.
			 */
			inline constexpr auto identifier(const karhu::Platform &p) noexcept
			{
				switch (p)
				{
					case karhu::Platform::mac:     return "mac";
					case karhu::Platform::linux:   return "linux";
					case karhu::Platform::windows: return "windows";
					case karhu::Platform::iOS:     return "ios";
					case karhu::Platform::android: return "android";
					case karhu::Platform::web:     return "web";
					default: return "unknown";
				}
			}
			
			/**
			 * Returns the formatted, proper name of the platform specified.
			 *
			 * @param p The platform.
			 *
			 * @return The string name corresponding to the platform.
			 */
			inline constexpr auto name(const karhu::Platform &p) noexcept
			{
				switch (p)
				{
					case karhu::Platform::mac:     return "macOS";
					case karhu::Platform::linux:   return "Linux";
					case karhu::Platform::windows: return "Windows";
					case karhu::Platform::iOS:     return "iOS";
					case karhu::Platform::android: return "Android";
					case karhu::Platform::web:     return "web";
					default: return "unknown";
				}
			}

			/**
			 * Returns the platform enumeration value that corresponds
			 * to the identifier (same as returned by the identifier()
			 * function) specified, or unknown if invalid.
			 *
			 * @param value The identifier string.
			 *
			 * @return The enumeration value corresponding to the string, or unknown.
			 */
			inline constexpr karhu::Platform fromIdentifier(const char *value) noexcept
			{
				using T = std::underlying_type_t<karhu::Platform>;
				
				// Goes through every bit, starting from the bigger ones.
				for (T pf{static_cast<T>(karhu::Platform::all)}; pf; pf >>= 1)
				{
					// Gets the platform of the current bit.
					const auto p{static_cast<karhu::Platform>(pf - (pf >> 1))};
					
					// Returns the platform if its identifier matches.
					if (meta::string::equal(identifier(p), value))
						return p;
				}
				
				return karhu::Platform::unknown;
			}
		}
	}
#endif
