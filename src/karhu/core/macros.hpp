/**
 * @author		Ava Skoog
 * @date		2019-08-03
 * @copyright	2018-2021 Ava Skoog
 */

#ifndef KARHU_CORE_MACROS_H_
	#define KARHU_CORE_MACROS_H_

	#include <type_traits>

	// Defines the operators necessary to use enum class values as bitflags.
	// These are | and & and |= and &=.
	#define karhuBITFLAGGIFY(enum_name) \
		static_assert(std::is_enum<enum_name>{}, "Cannot bitflaggify non-enum!"); \
		inline constexpr enum_name operator|(enum_name const a, enum_name const b) \
		{ \
			using T = std::underlying_type_t<enum_name>; \
			return static_cast<enum_name>(static_cast<T>(a) | static_cast<T>(b)); \
		} \
		inline constexpr enum_name operator&(enum_name const a, enum_name const b) \
		{ \
			using T = std::underlying_type_t<enum_name>; \
			return static_cast<enum_name>(static_cast<T>(a) & static_cast<T>(b)); \
		} \
		inline constexpr enum_name &operator|=(enum_name &a, enum_name const b) \
		{ \
			return a = a | b; \
		} \
		inline constexpr enum_name &operator&=(enum_name &a, enum_name const b) \
		{ \
			return a = a & b; \
		} \
		inline constexpr enum_name operator~(enum_name const &v) \
		{ \
			using T = std::underlying_type_t<enum_name>; \
			return static_cast<enum_name>(~static_cast<T>(v)); \
		}

	#define karhuBITFLAGGIFY_NESTED(enum_name) \
		static_assert(std::is_enum<enum_name>{}, "Cannot bitflaggify non-enum!"); \
		friend inline constexpr enum_name operator|(enum_name const a, enum_name const b) \
		{ \
			using T = std::underlying_type_t<enum_name>; \
			return static_cast<enum_name>(static_cast<T>(a) | static_cast<T>(b)); \
		} \
		friend inline constexpr enum_name operator&(enum_name const a, enum_name const b) \
		{ \
			using T = std::underlying_type_t<enum_name>; \
			return static_cast<enum_name>(static_cast<T>(a) & static_cast<T>(b)); \
		} \
		friend inline constexpr enum_name &operator|=(enum_name &a, enum_name const b) \
		{ \
			return a = a | b; \
		} \
		friend inline constexpr enum_name &operator&=(enum_name &a, enum_name const b) \
		{ \
			return a = a & b; \
		} \
		friend inline constexpr enum_name operator~(enum_name const &v) \
		{ \
			using T = std::underlying_type_t<enum_name>; \
			return static_cast<enum_name>(~static_cast<T>(v)); \
		}
#endif
