/**
 * @author		Ava Skoog
 * @date		2020-10-01
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/UserdataEntity.hpp>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace edt
	{
		void UserdataEntity::serialise(conv::Serialiser &) const
		{
			// In here we only serialise a few non-defaults so each entity
			// won't have a lot of unnecessary extra data associated with it.
		}
		
		void UserdataEntity::deserialise(const conv::Serialiser &)
		{
		}
	}
}

#endif
