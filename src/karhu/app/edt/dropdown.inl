#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/App.hpp>
#include <karhu/app/ecs/World.hpp>
#include <karhu/conv/graphics.hpp>
#include <karhu/app/phx/common.hpp>
#include <karhu/app/karhuimgui.hpp>

#include <type_traits>

namespace karhu
{
	namespace edt
	{
		/// @todo: Quick fix for later cleanup (move to meta header?), from https://stackoverflow.com/a/54446459/5012939
		template <typename T, bool = std::is_enum<T>::value>
		struct relaxed_underlying_type {
			using type = typename std::underlying_type<T>::type;
		};
		template <typename T>
		struct relaxed_underlying_type<T, false> {
			using type = T;
		};
		
		template<typename T, typename Iterator>
		inline Iterator getItDropdownAssoc(T const &curr, Iterator const &begin, Iterator const &end)
		{
			return std::find_if
			(
				begin,
				end,
				[&curr](auto const &v) -> bool
				{
					return (v.second == curr);
				}
			);
		}
		
		template<typename T, typename Iterator>
		inline Iterator getItDropdownIndexed(T const &curr, Iterator const &begin, Iterator const &end)
		{
			return begin + static_cast<std::ptrdiff_t>(curr);
		}
		
		template<typename Iterator>
		inline char const *getStrDropdownAssoc(Iterator const &it)
		{
			return it->first;
		}
		
		template<typename Iterator>
		inline char const *getCStrDropdownAssoc(Iterator const &it)
		{
			return it->first.c_str();
		}
		
		template<typename Iterator>
		inline char const *getStrDropdownIndexed(Iterator const &it)
		{
			return *it;
		}
		
		template<typename Iterator>
		inline char const *getCStrDropdownIndexed(Iterator const &it)
		{
			return it->c_str();
		}
		
		template<typename T, typename Iterator>
		inline T getValDropdownAssoc(Iterator const &it, Iterator const &)
		{
			return it->second;
		}
		
		template<typename T, typename Iterator>
		inline T getValDropdownIndexed(Iterator const &it, Iterator const &begin)
		{
			return static_cast<T>(it - begin);
		}

		template<typename T, typename Iterator>
		inline bool dropdown
		(
			char     const * const label,
			T              &       curr,
			Iterator       &&      begin,
			Iterator       &&      end,
			bool     const         multi = false,
			Iterator (*fGetIt)(T const &, Iterator const &, Iterator const &) = getItDropdownAssoc,
			char const *(*fGetStr)(Iterator const &) = getStrDropdownAssoc,
			T (*fGetVal)(Iterator const &, Iterator const &) = getValDropdownAssoc
		)
		{
			bool r{false};
			
			auto const cast([](T const v)
			{
				return static_cast<typename relaxed_underlying_type<T>::type>(v);
			});
			
			auto const
				itCurr(fGetIt(curr, begin, end));
			
			char const *preview;
			
			if (itCurr != end)
				preview = fGetStr(itCurr);
			else if (static_cast<bool>(curr))
				preview = "(multiple...)";
			else
				preview = "(none)";
			
			if (ImGui::BeginCombo(label, preview))
			{
				for (auto it(begin); it != end; ++ it)
				{
					auto const &
						val(static_cast<T>(fGetVal(it, begin)));
					
					bool const
						selected
						{
							(multi)
							?
								static_cast<bool>(cast(val) & cast(curr))
							:
								(curr == val)
						};
					
					if (ImGui::Selectable(fGetStr(it), selected))
					{
						if (multi)
						{
							if (selected)
								curr = static_cast<T>(cast(curr) & ~cast(val));
							else
								curr = static_cast<T>(cast(curr) | cast(val));
							
							r = true;
						}
						else if (val != curr)
						{
							curr = val;
							r = true;
						}
					}
					
					if (selected)
						ImGui::SetItemDefaultFocus();
				}
				
				ImGui::EndCombo();
			}
			
			return r;
		}
		
		inline bool dropdownTagsECS(app::App &app, ecs::TagsEntity &tags)
		{
			bool r{false};
			
			auto const nameOfTag([&app](ecs::TagEntity const tag) -> char const *
			{
				if (ecs::DefaulttagsEntity::none == tag)
					return "(none)";
				
				char const
					*const name(app.ecs().tags().name(tag));
				
				if (0 == std::strlen(name))
					return "(multiple...)";
				
				return name;
			});
		
			auto const selectableForLayer([&r, &tags](ecs::TagEntity const tag, char const *const name)
			{
				bool const selected{static_cast<bool>((tag & tags))};
				
				if (ImGui::Selectable(name, selected))
				{
					if (selected)
						tags &= ~tag;
					else
						tags |= tag;
					
					r = true;
				}
				
				if (selected)
					ImGui::SetItemDefaultFocus();
			});
		
			char const
				*const name{nameOfTag(tags)};
		
			if (ImGui::BeginCombo("Tags", name))
			{
				auto const &values(app.ecs().tags().values());
				
				if (!values.empty())
					for (auto const &it : values)
						selectableForLayer(it.first, it.second.c_str());
				
				ImGui::EndCombo();
			}
			
			return r;
		}
		
		inline bool dropdownLayersGraphics
		(
			app::App &app,
			gfx::Layer &layers,
			bool const forceStandard = true
		)
		{
			bool r{false};
			
			auto const nameOfLayer([&app](gfx::Layer const layer) -> char const *
			{
				if (gfx::Defaultlayers::none == layer)
					return "(none)";
				else if (gfx::Defaultlayers::standard == layer)
					return "Standard";
				else if (gfx::Defaultlayers::GUI == layer)
					return "GUI";
				
				char const
					*const name(app.gfx().layers().name(layer));
				
				if (0 == std::strlen(name))
					return "(multiple...)";
				
				return name;
			});
		
			auto const selectableForLayer([&r, &layers](gfx::Layer const layer, char const *const name)
			{
				bool const selected{static_cast<bool>((layer & layers))};
				
				if (ImGui::Selectable(name, selected))
				{
					if (selected)
						layers &= ~layer;
					else
						layers |= layer;
					
					r = true;
				}
				
				if (selected)
					ImGui::SetItemDefaultFocus();
			});
		
			char const
				*const name{nameOfLayer(layers)};
		
			if (ImGui::BeginCombo("Layers", name))
			{
				selectableForLayer(gfx::Defaultlayers::standard, nameOfLayer(gfx::Defaultlayers::standard));
				selectableForLayer(gfx::Defaultlayers::GUI, nameOfLayer(gfx::Defaultlayers::GUI));
				
				auto const &values(app.gfx().layers().values());
				
				if (!values.empty())
				{
					ImGui::Separator();
					
					for (auto const &it : values)
						selectableForLayer(it.first, it.second.c_str());
				}
				
				ImGui::EndCombo();
			}
		
			if (forceStandard && gfx::Defaultlayers::none == layers)
				layers = gfx::Defaultlayers::standard;
			
			return r;
		}
		
		inline bool dropdownLayerPhysics(app::App &app, phx::Layer &layer)
		{
			bool r{false};
			
			auto const nameOfLayer([&app](phx::Layer const layer) -> char const *
			{
				if (phx::Defaultlayers::standard == layer)
					return "Standard";
				
				char const
					*const name(app.phx().layers().name(layer));
				
				if (0 == std::strlen(name))
					return "(multiple...)";
				
				return name;
			});
		
			auto const selectableForLayer([&r, &layer](phx::Layer const val, char const *const name)
			{
				bool const selected{static_cast<bool>((val & layer))};
				
				if (ImGui::Selectable(name, selected))
				{
					if (val != layer)
					{
						layer = val;
						r = true;
					}
				}
				
				if (selected)
					ImGui::SetItemDefaultFocus();
			});
		
			char const
				*const name{nameOfLayer(layer)};
		
			if (ImGui::BeginCombo("Layer", name))
			{
				selectableForLayer(phx::Defaultlayers::standard, nameOfLayer(phx::Defaultlayers::standard));
				
				auto const &values(app.phx().layers().values());
				
				if (!values.empty())
				{
					ImGui::Separator();
				
					for (auto const &it : values)
						selectableForLayer(it.first, it.second.c_str());
				}
				
				ImGui::EndCombo();
			}
		
			if (phx::Defaultlayers::none == layer)
				layer = phx::Defaultlayers::standard;
			
			return r;
		}
	}
}
#endif
