/**
 * @author		Ava Skoog
 * @date		2020-08-03
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDITOR_UTIL_H_
	#define KARHU_APP_EDITOR_UTIL_H_

	#include <karhu/core/Buffer.hpp>
	#include <karhu/core/Byte.hpp>
	#include <karhu/core/debugging.hpp>
	#include <karhu/conv/graphics.hpp>

	namespace karhu
	{
		namespace edt
		{
			mth::Vec2f sizeWithinBounds(float const w, float const h, float const wb, float const hb);
			
			// Clicking in hierarchies.
			
			using IndexHierarchy = std::int64_t;
			
			enum : IndexHierarchy { indexHierarchyNone = -1 };
			
			// Runtime serialisation.
			
			void serialiseToBytesRaw(void const *const source, std::size_t const size, Buffer<Byte> &target);
			void deserialiseFromBytesRaw(void *const destination, std::size_t const size, Buffer<Byte> const &source);
			
			template<typename T>
			void serialiseToBytes(T const &source, Buffer<Byte> &target)
			{
				serialiseToBytesRaw
				(
					static_cast<void const *const>(&source),
					sizeof(source),
					target
				);
			}
			
			template<typename T>
			void deserialiseFromBytes(T &destination, Buffer<Byte> const &source)
			{
				deserialiseFromBytesRaw
				(
					static_cast<void *const>(&destination),
					sizeof(destination),
					source
				);
			}
			
			bool compareSerialisationsFromBytes(Buffer<Byte> const &, Buffer<Byte> const &);
			
			float beginGroupPanel();
			void endGroupPanel();
			void lineGroupPanel(float const widthGroup);
			
			bool editInputsGraphics(edt::Editor &, app::App &, gfx::ContainerInputs &inputs);
		}
	}
#endif

#endif
