/**
 * @author		Ava Skoog
 * @date		2019-11-09
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Styling stack where necessary widget-specific settings are set up to add borders, tints and so on.
/// @todo: Work undo/redo into all state-changing widgets (checkboxes, radio button groups, text inputs, selections…)
/// @todo: Abstract multiple and single select nested trees with proper double click distinction and so on. Multi-select with shift or ctrl/cmd.
/// @todo: Abstract autoscrolling scrollboxes.
/// @todo: Make menu items with shortcuts automatically have those shortcuts work and activate the menu items (if enabled) to get rid of duplicate code and ensure the enabled state is always respected.

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDITOR_H_
	#define KARHU_APP_EDITOR_H_

	#include <karhu/app/edt/actions/IAction.hpp>
	#include <karhu/app/edt/actions/common.hpp>
	#include <karhu/app/edt/resources.hpp>
	#include <karhu/app/edt/input.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/app/gfx/resources/Texture.hpp>
	#include <karhu/app/gfx/Rendertarget.hpp>
	#include <karhu/core/Buffer.hpp>
	#include <karhu/conv/maths.hpp>
	#include <karhu/conv/files.hpp>
	#include <karhu/core/macros.hpp>
	#include <karhu/core/log.hpp>

	#include <vector>
	#include <stack>
	#include <set>
	#include <functional>
	#include <memory>
	#include <string>
	#include <type_traits>

	class asITypeInfo;
	class asIScriptFunction;
	class asIScriptObject;

	namespace boost
	{
		namespace process
		{
			class child;
		}
	}

	namespace karhu
	{
		namespace res
		{
			class Texture;
			class Script;
		}
		
		namespace gfx
		{
			class Transform;
		}
		
		namespace app
		{
			class App;
		}
		
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					class Project;
				}
			}
		}
		
		namespace edt
		{
			class Server;
			class Editor;
			struct Event;
			
			class Logger : public log::Implementation
			{
				public:
					Logger(app::App &app) : m_app{app} {}
				
				public:
					bool log
					(
						log::Level  const,
						std::string const &tag,
						std::string const &message
					) override;
				
				public:
					void clear();
				
				public:
					enum class Category : std::uint8_t
					{
						server,
						client
					} category{Category::server};
				
					std::vector<Logger::Category> categories;
					std::vector<log::Level>       levels;
					std::vector<std::string>      tags;
					std::vector<std::string>      texts;
				
				private:
					app::App &m_app;
			};
			
			enum class StateEditor
			{
				normal,
				building,
				connecting,
				connected
			};
			
			struct VtableEditorScripted
			{
				asITypeInfo
					*type{nullptr};
				
				asIScriptFunction
					*factory                  {nullptr},
					*init                     {nullptr},
					*updateAndRender          {nullptr},
					*updateInViewportAndRender{nullptr},
					*event                    {nullptr};
			};
			
			struct DataScript
			{
				std::unique_ptr<res::Script> script;
				VtableEditorScripted vtable;
			};
			
			class EditorScripted : public IEditor
			{
				public:
					using IEditor::IEditor;
				
				public:
					bool init(app::App &, Editor &) override;

					void updateAndRender(app::App &, Editor &) override;
					void updateInViewportAndRender(app::App &, Editor &, mth::Vec2f const &size) override;

					void event(app::App &, Editor &, Identifier const, Event const &) override;
				
				public:
					~EditorScripted();
				
				private:
					std::string m_pathToScript;
					asIScriptObject *m_object{nullptr};
					DataScript m_data;
					bool m_valid{false};
				
				friend class Editor;
			};
			
			struct Camera
			{
				std::unique_ptr<gfx::Rendertarget>
					target;
				
				std::unique_ptr<gfx::Texture>
					texture,
					depth;
				
				ecs::IDEntity
					entity;
				
				ecs::IDComponent
					transform,
					camera;
				
				mth::Radsf
					yaw  {0.0f},
					pitch{0.0f};
				
				mth::Scalf
					turnspeed{0.01f},
					panspeed {0.1f},
					zoomspeed{1.0f},
					dist     {20.0f};
				
				mth::Vec3f
					position{0.0f, 0.0f, 0.0f};
				
				bool
					invertX{false},
					invertY{false};
			};
			
			using FCallbackEvent = std::function<void(app::App &, Editor &, Identifier const caller, Event const &)>;
			
			class Editor
			{
				public:
					Editor(std::unique_ptr<Logger> &&);
					~Editor();
				
				// Scripting GUI interface.
				public:
					app::App &app() const noexcept { return *m_app; }
				
					// Generic styling
				
					void pushColour(gfx::ColRGBAf const &);
					void popColour();
				
					void pushFilled(bool const);
					void popFilled();
				
					void pushThickness(float const);
					void popThickness();
				
					// Im3d
				
					void lineInViewport(mth::Vec3f const &a, mth::Vec3f const &b);
					void planeInViewport(mth::Vec3f const &origin, mth::Vec3f const &normal, mth::Vec2f const &size);
					void sphereInViewport(mth::Vec3f const &position, float const diameter);
					void boxInViewport(mth::Vec3f const &min, mth::Vec3f const &max);
					void cylinderInViewport(mth::Vec3f const &start, mth::Vec3f const &end, float const diameter);
				
					// ImGuizmo
				
					bool gizmoInViewport(Gizmo &);
				
					// ImGui
				
					void pushDisabled();
					void popDisabled();
				
					void line();
					void space();
				
					void text(char const *const);
					void text(std::string const &);
				
					void header(char const *const);
					void header(std::string const &);
				
					void image
					(
						res::Texture const &texture,
						mth::Vec2f          size    = {-1.0f, -1.0f},
						mth::Vec2f   const &UV1     = {0.0f, 0.0f},
						mth::Vec2f   const &UV2     = {1.0f, 1.0f}
						/// @todo: Get rid of these and work them into a general styling system/stack
		//				gfx::ColRGBAf const &tint   = {1.0f, 1.0f, 1.0f, 1.0f},
		//				gfx::ColRGBAf const &border = {0.0f, 0.0f, 0.0f, 0.0f}
					);
				
					bool imagebutton
					(
						res::Texture const &texture,
						mth::Vec2f          size    = {-1.0f, -1.0f},
						mth::Vec2f   const &UV1     = {0.0f, 0.0f},
						mth::Vec2f   const &UV2     = {1.0f, 1.0f}
					);
				
					bool button
					(
						char const *const ID,
						char const *const label
					);
				
					bool input
					(
						char        const  *const ID,
						char        const  *const label,
						std::string        &      buffer,
						bool               *const tracker = nullptr,
						FlagsInput  const         flags   = FlagsInput::none
					);
				
					bool input
					(
						char        const  *const ID,
						char        const  *const label,
						InputString        &      data,
						FlagsInput  const         flags = FlagsInput::none
					);
				
					bool input
					(
						char       const *const ID,
						char       const *const label,
						float            &      buffer,
						float      const        step    = 0.0f,
						float      const        speed   = 0.0f,
						bool             *const tracker = nullptr,
						FlagsInput const        flags   = FlagsInput::none
					);
				
					bool input
					(
						char       const *const ID,
						char       const *const label,
						InputFloat       &      data,
						float      const        step  = 0.0f,
						float      const        speed = 0.0f,
						FlagsInput const        flags = FlagsInput::none
					);
				
					bool input
					(
						char          const *const ID,
						char          const *const label,
						std::int32_t        &      buffer,
						std::int32_t  const        step    = 0,
						std::int32_t  const        speed   = 0,
						bool         *const        tracker = nullptr,
						FlagsInput    const        flags   = FlagsInput::none
					);
				
					bool input
					(
						char         const *const ID,
						char         const *const label,
						InputInt           &      data,
						std::int32_t const        step  = 0,
						std::int32_t const        speed = 0,
						FlagsInput   const        flags = FlagsInput::none
					);
				
					/// @todo: May have to change to Ref<T> when that's back.
					bool pickerResource
					(
						char                const *const ID,
						char                const *const label,
						res::IDResource           &      data,
						res::IDTypeResource const        type
					);
				
					void nameResource
					(
						res::IDResource     const data,
						res::IDTypeResource const type
					);
				
				// Specific to scripting.
				private:
					bool buttonAS
					(
						std::string const &ID,
						std::string const &label
					);
				
					bool inputStringAS
					(
						std::string const &ID,
						std::string const &label,
						InputString       &buffer,
						FlagsInput  const  flags = FlagsInput::none
					);
				
					bool inputFloatAS
					(
						std::string const &ID,
						std::string const &label,
						InputFloat        &buffer,
						float       const  step   = 0.0f,
						float       const  speed  = 0.0f,
						FlagsInput  const  flags  = FlagsInput::none
					);
				
					bool inputIntAS
					(
						std::string  const &ID,
						std::string  const &label,
						InputInt           &buffer,
						std::int32_t const  step   = 0.0f,
						std::int32_t const  speed  = 0.0f,
						FlagsInput   const  flags  = FlagsInput::none
					);
				
				// Other public stuff.
				public:
					bool initBeforeHook(app::App &, std::string const &basepath, int argc, char *argv[]);
					bool initAfterHook(app::App &);
					void updateAndRender(app::App &);
					void updateInViewportAndRender(app::App &, mth::Vec2f const &size);
				
					tool::proj::file::Project *project() const noexcept { return m_project.get(); }
					DataResources &resources() noexcept { return m_resources.data; }
					StateResources &stateResources() noexcept { return m_resources.state; }
					void refreshResource(res::IDResource const, res::IDTypeResource const);
				
					void scene(res::IDResource const, ecs::IDScene const);
					res::IDResource IDSceneResource() const noexcept { return m_scene.resource; }
					ecs::IDScene IDSceneECS() const noexcept { return m_scene.scene; }
				
					Server *server() const;
				
					void drawDropdownPlatform(int const ID, float const width = -1.0f);
					void drawCheckboxesBuildstepsProject(bool const sameLine);
					void drawCheckboxesBuildstepsKarhu(bool const sameLine);
					void drawRadiobuttonsBuildconfig(bool const sameLine);
					void drawBuildconfigNetworking(bool const sameLine, float const width = -1.0f);
				
					bool gizmo() const noexcept { return m_gizmo; }
				
					res::Texture *texture(char const *const) const;
				
					Camera       &camera()       noexcept { return m_camera; }
					Camera const &camera() const noexcept { return m_camera; }
				
					void active(bool const);
					bool active() const noexcept { return m_active; }
				
					Logger &logger() { return *m_logger; }
					Logger const &logger() const { return *m_logger; }
				
					bool karhuInstalled() const noexcept
					{
						return m_karhuInstalled;
					}
				
					std::vector<IAction const *> const &actionsHistory() const noexcept
					{
						return m_history.pointers;
					}
				
					/**
					 * Returns the index where the next history item will go,
					 * which is also the item to redo if the index is not at
					 * the end of the history. To get the current item to undo,
					 * subtract one.
					 *
					 * @return The current history index.
					 */
					std::size_t indexHistory() const noexcept
					{
						return m_history.index;
					}
				
					bool connectedOrConnecting() const noexcept
					{
						return
						(
							StateEditor::connecting == m_state ||
							StateEditor::connected  == m_state
						);
					}
				
					StateEditor state() const noexcept { return m_state; }
				
					Platform platform() const noexcept;
				
					void entitiesSelected
					(
						std::size_t   const &      count,
						ecs::IDEntity const *const,
						Identifier    const &      caller = callerless
					);
				
					const std::vector<ecs::IDEntity> &entitiesSelected() const noexcept
					{
						return m_values.entitiesSelected;
					}
				
					bool entitySelected(ecs::IDEntity const entity) const;
				
					void entitiesTransformation
					(
						Transformation const,
						std::size_t    const &      count,
						ecs::IDEntity  const *const,
						mth::Vec4f     const *const,
						bool           const        done,
						Identifier     const &      caller = callerless
					);
				
					void targetViewport
					(
						mth::Vec3f const &,
						Identifier const  caller = callerless
					);
				
					mth::Vec3f const &targetViewport() const noexcept
					{
						return m_values.targetViewport;
					}
				
					/// @todo: Be able to specify scene to create entity in.
					bool createEntity
					(
						ecs::IDEntity       &inOutEntity,
						Identifier    const  caller      = callerless
					);
				
					bool destroyEntity
					(
						ecs::IDEntity const entity,
						Identifier    const caller = callerless
					);
				
					bool duplicateEntity
					(
						ecs::IDEntity const  entity,
						ecs::IDEntity       &inOutEntity,
						Identifier    const  caller = callerless
					);
				
					bool createComponent
					(
						ecs::IDTypeComponent const  type,
						ecs::IDEntity        const  entity,
						ecs::IDComponent           &inOutComponent,
						Identifier           const  caller         = callerless
					);
				
					bool destroyComponent
					(
						ecs::IDEntity    const entity,
						ecs::IDComponent const component,
						Identifier       const caller    = callerless
					);
				
					template<typename T>
					bool registerEditor(bool const create, std::string const &name);
				
					bool registerCallbackEvent(FCallbackEvent);
				
					void queueEditorReload(std::string const &name);
				
					bool pushAction
					(
						Identifier const caller,
						IAction          &&
					);
				
					IAction const *peekAction() const;
				
					void emitEvent
					(
						Event      const &,
						Identifier const caller = callerless
					);
				
					bool emitEvent
					(
						Event         const &,
						std::size_t   const &count,
						ecs::IDEntity const *entities,
						Identifier    const &caller    = callerless
					);
				
					Nullable<DataScript> loadDataForEditorScripted(std::string const &filename);
				
					Nullable<std::string> basepathToScripts() const;
				
					void refreshResources();
					bool reloadResources();
				
				private:
					template<typename T>
					bool registerEditor(bool const create, std::string const &name, std::string const &pathToScript);
				
					bool initScripting();
					bool initEditors();
					void loadEditorsScriptedRecursively();
					void loadEditorsScriptedRecursively(std::string const &basedir, std::string const &dir = {});
					bool initPlatforms();
					bool popAction();
					bool unpopAction();
					void refreshStateHistory();
					bool apply();
					bool disconnect();
					void validateSettings(Platform const);
					static void callbackOnRefreshResource(Editor &, res::IDResource const);
				
					bool duplicateEntityChild
					(
						ecs::IDEntity const  entity,
						ecs::IDEntity       &inOutEntity,
						ecs::IDEntity const  parent,
						Identifier    const  caller = callerless
					);
				
				private:
					bool m_active{true}, m_karhuInstalled{false};
					app::App *m_app{nullptr};
					std::unique_ptr<Logger> m_logger;
					std::string m_basepath, m_programpath;
					std::map<std::string, ecs::IDTypeComponent> m_typesComponents;
				
					bool m_gizmo{false};
				
					std::map<std::string, std::unique_ptr<res::Texture>> m_textures;
				
					res::Texture
						*m_textureLogo       {nullptr},
						*m_textureIconPlay   {nullptr},
						*m_textureIconConnect{nullptr},
						*m_textureIconStop   {nullptr};
				
					Camera m_camera;
				
					using FactoryEditor = std::function<std::unique_ptr<IEditor>()>;
				
					struct DataEditor
					{
						std::unique_ptr<IEditor> editor;
						FactoryEditor factory;
						bool open, scripted;
					};
			
					std::map<std::string, DataEditor> m_editors;
					std::vector<FCallbackEvent> m_callbacksEvents;
					std::set<std::string> m_editorsToReload;
					Identifier m_identifierEditorLast{callerless - 1};
				
					struct
					{
						res::IDResource resource{0};
						ecs::IDScene    scene   {0};
					} m_scene;
				
					struct
					{
						std::stack<gfx::ColRGBAf> colours;
						std::stack<bool> filleds;
						std::stack<float> thicknesses;
						
						gfx::ColRGBAf const &colour() const noexcept
						{
							static gfx::ColRGBAf const base{};
							return (colours.empty()) ? base : colours.top();
						}
						
						bool filled() const noexcept
						{
							return (filleds.empty()) ? false : filleds.top();
						}
						
						float thickness() const noexcept
						{
							return (thicknesses.empty()) ? 1.0f : thicknesses.top();
						}
					} m_stateGUI;
				
					struct
					{
						std::vector<std::unique_ptr<IAction>>
							actions;
						
						std::vector<IAction const *>
							pointers;
						
						std::size_t
							index{0};
						
						std::string
							labelUndo,
							labelRedo;
						
						bool
							canUndo,
							canRedo;
					} m_history;
				
					struct
					{
						std::vector<ecs::IDEntity>
							entitiesSelected;
						
						mth::Vec3f
							targetViewport{0.0f, 0.0f, 0.0f};
					} m_values;
				
					struct
					{
						DataResources                data;
						StateResources               state;
						StateResources               statePicker;
						SettingsDrawEntriesResources settingsDraw;
					} m_resources;
				
					/// @todo: Change this into a proper state system with an enum.
					StateEditor m_state{StateEditor::normal};
				
					std::unique_ptr<Server>                     m_server;
					std::unique_ptr<boost::process::child>      m_process;
					std::unique_ptr<tool::proj::file::Project>  m_project;
				
					enum class TypePlatform : std::uint8_t
					{
						current,
						local,
						remote,
						unavailable
					};
				
					struct DataPlatform
					{
						Platform     platform;
						TypePlatform type;
					};
				
					struct Platforms
					{
						struct Settings
						{
							bool
								clean, make, build, run,
								cleanKarhu, makeKarhu, buildKarhu,
								debug,
								manual{false},
								valid{true},
								online{true};
							
							int port;
						};
						
						std::vector<DataPlatform> datas;
						std::map<Platform, std::unique_ptr<Settings>> settings;
						
						int length, count{1}, index{0};
					} m_platforms;
				
					class AdapterRead : public conv::AdapterFilesystemRead
					{
						public:
							AdapterRead(Editor &editor) : m_editor{editor} {}
							AdapterRead(AdapterRead &&) = default;
							AdapterRead(const AdapterRead &) = default;
							AdapterRead &operator=(AdapterRead &&) = default;
							AdapterRead &operator=(const AdapterRead &) = default;
				
						public:
							Result<std::string> readStringFromResource
							(
								const char *filepathRelativeToResourcepath
							) override;
				
							Result<Buffer<Byte>> readBytesFromResource
							(
								const char *filepathRelativeToResourcepath
							) override;
						
						private:
							Editor &m_editor;
					} m_adapterRead{*this};
			};
			
			enum class TypeEvent
			{
				entity,
				component,
				entitiesSelected,
				entitiesTransformed,
				viewportTargetChanged,
				connected,
				disconnected,
				resource
			};
			
			struct EEntity
			{
				TypeActionEntity const action;
				ecs::IDEntity    const entity;
				bool             const selected{false};
			};
			
			struct EComponent
			{
				TypeActionComponent  const action;
				ecs::IDTypeComponent const type;
				ecs::IDComponent     const component;
				ecs::IDEntity        const entity;
			};
			
			struct EEntitiesSelected
			{
				Arrayview<ecs::IDEntity const> const entities;
			};
			
			struct EEntitiesTransformed
			{
				Transformation                       transformation;
				Arrayview<ecs::IDEntity const> const entities;
				Arrayview<mth::Vec4f    const> const values;
				bool                           const done;
			};
			
			struct EViewportTargetChanged
			{
				mth::Vec3f value;
			};
			
			/// @todo: Merge connect and disconnect editor events with an enum?
			struct EConnected {};
			struct EDisconnected {};
			
			enum class TypeEventResource
			{
				modifiedOrAdded,
				deleted,
				moved
			};
			
			struct EResource
			{
				res::IDResource   const ID;
				TypeEventResource const type;
			};
			
			struct Event
			{
				TypeEvent type;
				
				union
				{
					EEntity                entity;
					EComponent             component;
					EEntitiesSelected      entitiesSelected;
					EEntitiesTransformed   entitiesTransformed;
					EViewportTargetChanged viewportTargetChanged;
					EConnected             connected;
					EDisconnected          disconnected;
					EResource              resource;
				};
				
				Event(EEntity &&v)
				:
				type{TypeEvent::entity},
				entity{std::move(v)}
				{
				}
				
				Event(EComponent &&v)
				:
				type{TypeEvent::component},
				component{std::move(v)}
				{
				}
				
				Event(EEntitiesSelected &&v)
				:
				type{TypeEvent::entitiesSelected},
				entitiesSelected{std::move(v)}
				{
				}
				
				Event(EEntitiesTransformed &&v)
				:
				type{TypeEvent::entitiesTransformed},
				entitiesTransformed{std::move(v)}
				{
				}
				
				Event(EViewportTargetChanged &&v)
				:
				type{TypeEvent::viewportTargetChanged},
				viewportTargetChanged{std::move(v)}
				{
				}
				
				Event(EConnected &&v)
				:
				type     {TypeEvent::connected},
				connected{std::move(v)}
				{
				}
				
				Event(EDisconnected &&v)
				:
				type        {TypeEvent::disconnected},
				disconnected{std::move(v)}
				{
				}
				
				Event(EResource &&v)
				:
				type    {TypeEvent::resource},
				resource{std::move(v)}
				{
				}
			};
			
			template<typename T>
			bool Editor::registerEditor(bool const create, std::string const &name)
			{
				return registerEditor<T>(create, name, "");
			}
			
			template<typename T>
			bool Editor::registerEditor
			(
				bool const create,
				std::string const &name,
				std::string const &pathToScript
			)
			{
				static_assert
				(
					(
						 std::is_base_of<IEditor, T>{} &&
						!std::is_same   <IEditor, T>{}
					),
					"Editor must inherit from IEditor"
				);
			
				if (m_editors.find(name) != m_editors.end())
					return true;
				
				std::unique_ptr<IEditor> p;
				
				FactoryEditor const f
				{
					[ID = m_identifierEditorLast, name, pathToScript]()
					{
						auto r(std::make_unique<T>(ID, name));
						
						if (!r)
							log::err("Karhu")
								<< "Failed to create subeditor '"
								<< name
								<< '\'';
						
						// Special case for scripted editors.
						if (auto scripted = dynamic_cast<EditorScripted *>(r.get()))
							scripted->m_pathToScript = pathToScript;
						
						return r;
					}
				};
				
				if (create)
					p = f();
			
				bool const open{p};
			
				m_editors.emplace
				(
					name,
					DataEditor
					{
						std::move(p),
						std::move(f),
						open,
						!pathToScript.empty()
					}
				);
				
				// Editor identifiers are negative so as
				// not to clash with component identifiers.
				-- m_identifierEditorLast;
				
				return true;
			}
		}
	}
#endif

#endif
