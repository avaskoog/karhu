/**
 * @author		Ava Skoog
 * @date		2019-11-27
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_EDITOR_SUPPORT_H_
	#define KARHU_APP_EDITOR_SUPPORT_H_

	#include <karhu/core/platform.hpp>
	#include <karhu/conv/maths.hpp>

	#include <string>

	#if defined(KARHU_EDITOR) && (defined(KARHU_PLATFORM_MAC) || defined(KARHU_PLATFORM_LINUX))
		#define KARHU_EDITOR_SERVER_SUPPORTED 1

		namespace karhu
		{
			namespace app
			{
				class App;
			}
			
			namespace edt
			{
				class  Editor;
				struct Event;
				
				enum class Transformation : std::uint8_t
				{
					position = 1 << 0,
					rotation = 1 << 1,
					scale    = 1 << 2,
					origin   = 1 << 3
				};
				
				using Identifier = std::int64_t;
				
				enum : Identifier
				{
					// This is reserved for no caller,
					// so all editor identifiers must be less;
					// component identifiers are always positive.
					callerless = -1
				};
				
				class IEditor
				{
					public:
						virtual bool init(app::App &, Editor &) = 0;
						virtual void updateAndRender(app::App &, Editor &) = 0;
						virtual void updateInViewportAndRender(app::App &, Editor &, mth::Vec2f const &size) = 0;
						virtual void event(app::App &, Editor &, Identifier const caller, Event const &) = 0;
					
					public:
						IEditor
						(
							Identifier  const  identifier,
							std::string const &name
						)
						:
						m_identifier{identifier},
						m_name      {name}
						{
						}
					
						Identifier identifier() const noexcept { return m_identifier; }
						std::string const &name() const noexcept { return m_name; }
					
						virtual ~IEditor() = default;
					
					private:
						Identifier  const m_identifier;
						std::string const m_name;
				};
			}
		}
	#else
		#define KARHU_EDITOR_SERVER_SUPPORTED 0
	#endif
#endif
