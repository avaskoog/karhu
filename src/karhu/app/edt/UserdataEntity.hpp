/**
 * @author		Ava Skoog
 * @date		2019-12-07
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Should probably move ECS user data out of edt and make it normally visible to the scene serialiser too, so that it can just be a setting rather than a spaghettified hardcoded ifdef when the editor needs userdata loaded and stored.

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDITOR_USERDATA_ENTITY_H_
	#define KARHU_APP_EDITOR_USERDATA_ENTITY_H_

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/app/nav/common.hpp>

	namespace karhu
	{
		namespace edt
		{
			struct UserdataEntity : public ecs::Userdata
			{
				void serialise(conv::Serialiser &) const override;
				void deserialise(const conv::Serialiser &) override;
				
				struct DataComponent
				{
					bool
						destroyed{false},
						activeBeforeDestroyed;
				};
				
				std::map<ecs::IDComponent, DataComponent>
					components;
				
				ecs::IDScene
					scene{0};
				
				bool
					top      {false},
					destroyed{false},
					activeBeforeDestroyed;
			};
		}
	}
#endif

#endif
