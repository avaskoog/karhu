/**
 * @author		Ava Skoog
 * @date		2019-11-11
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDITORS_EDITOR_INSPECTOR_H_
	#define KARHU_APP_EDITORS_EDITOR_INSPECTOR_H_

	#include <karhu/app/edt/Editor.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/core/Byte.hpp>
	#include <karhu/core/Buffer.hpp>

	#include <map>

	namespace karhu
	{
		namespace edt
		{
			class EditorInspector : public IEditor
			{
				public:
					using IEditor::IEditor;
				
				public:
					bool init(app::App &, Editor &) override;
				
					void updateAndRender(app::App &, Editor &) override;
					void updateInViewportAndRender(app::App &, Editor &, mth::Vec2f const &) override;
					
					void event(app::App &, Editor &, Identifier const, Event const &) override;
				
				private:
					struct Component
					{
						bool active;
						Buffer<Byte> prev;
					};
				
					struct Entity
					{
						bool any{false}, many{false};
					
						ecs::IDEntity entity;
						ecs::TagsEntity tags;
						bool active;
						
						InputString inputName;
						
						std::map<ecs::IDComponent, Component> components;
					} m_entity;
			};
		}
	}
#endif

#endif
