/**
 * @author		Ava Skoog
 * @date		2019-12-04
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/editors/EditorHistory.hpp>
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/edt/actions/IAction.hpp>

#include <karhu/app/karhuimgui.hpp>

namespace karhu
{
	namespace edt
	{
		void EditorHistory::updateAndRender
		(
			app::App &,
			Editor   &editor
		)
		{
			ImGui::BeginChild("scroll-history");
			{
				const auto &actions(editor.actionsHistory());
				
				for (std::size_t i{0}; i < actions.size(); ++ i)
				{
					const IAction *const action{actions[i]};
					
					if ((editor.indexHistory() - 1) == i)
						ImGui::TextUnformatted(action->name());
					else
						ImGui::TextDisabled("%s", action->name());
					
					m_autoscroll = (ImGui::GetScrollY() == ImGui::GetScrollMaxY());
					
					if (m_autoscroll)
						ImGui::SetScrollHereY(1.0f);
				}
			}
			ImGui::EndChild();
		}
	}
}

#endif
