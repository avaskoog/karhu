/**
 * @author		Ava Skoog
 * @date		2020-07-28
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDITORS_EDITOR_RESOURCES_H_
	#define KARHU_APP_EDITORS_EDITOR_RESOURCES_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/app/edt/resources.hpp>

	namespace karhu
	{
		namespace edt
		{
			/**
			 * Editor window for browsing the project's resource folder.
			 */
			class EditorResources : public IEditor
			{
				public:
					using IEditor::IEditor;
				
				public:
					bool init(app::App &, Editor &) override { return true; }
				
					void updateAndRender(app::App &, Editor &) override;
					void updateInViewportAndRender(app::App &, Editor &, mth::Vec2f const &) override;
				
					void event(app::App &, Editor &, Identifier const, Event const &) override {}
				
				protected:
					void drawPreview(app::App &, Editor &);
				
				protected:
					struct
					{
						bool
							any {false},
							many{false};
						
						TypeEntryResource   typeEntry;
						res::IDTypeResource typeResource;
						res::IDResource     identifier;
						std::string         name;
					} m_resource;
			};
		}
	}
#endif

#endif
