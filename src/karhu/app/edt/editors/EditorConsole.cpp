/**
 * @author		Ava Skoog
 * @date		2019-11-22
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/editors/EditorConsole.hpp>
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/App.hpp>
#include <karhu/app/Scriptmanager.hpp>

#include <karhu/app/karhuimgui.hpp>

namespace
{
	using namespace karhu;
	using namespace karhu::edt;
	
	// Adapted from the AngelScript script helper sample to suit our needs.
	/// @todo: Does module need to be released? It is not in the original sample code.
	/// @todo: Make editor console commands also/instead be sent to client if running.
	bool executeAS(app::App &app, Editor &editor, char const *const code)
	{
		// Wrap the code in a function so that it can be compiled and executed.
		std::string wrapper{"void _executeAS(app::App &app, edt::Editor &editor) {\n"};
		wrapper += code;
		wrapper += "\n;}";
		
		asIScriptEngine &engine{app.scr().engine()};

		asIScriptModule   *module{engine.GetModule("_executeAS", asGM_ALWAYS_CREATE)};
		asIScriptFunction *func  {nullptr};
		
		int r{module->CompileFunction("_executeAS", wrapper.c_str(), -1, 0, &func)};
		
		if (r < 0)
			return false;
			
		asIScriptContext &context{app.scr().borrowContext()};

		r = context.Prepare(func);
		
		if (r >= 0)
			AS::call<app::App &, Editor &>(context, nullptr, *func, app, editor);

		func->Release();
		
		app.scr().returnContext(context);

		return (r >= 0);
	}
}

namespace karhu
{
	namespace edt
	{
		bool EditorConsole::init(app::App &, Editor &)
		{
			return true;
		}
		
		void EditorConsole::updateAndRender(app::App &app, Editor &editor)
		{
			// Clearing.
			
			ImGui::PushMultiItemsWidths(6, ImGui::GetContentRegionAvail().x);
			
			if (ImGui::Button("Clear"))
				editor.logger().clear();
			
			ImGui::PopItemWidth();
			ImGui::SameLine();
			
			ImGui::Checkbox("Clear on connection", &m_clearOnConnection);
			ImGui::PopItemWidth();
			ImGui::SameLine();
			
			ImGui::SeparatorEx(ImGuiSeparatorFlags_Vertical);
			ImGui::PopItemWidth();
			ImGui::SameLine();
			
			// Filtering.
			
			ImGui::Checkbox("Server", &m_filterServer);
			ImGui::PopItemWidth();
			ImGui::SameLine();
			ImGui::Checkbox("Client", &m_filterClient);
			ImGui::PopItemWidth();
			ImGui::SameLine();
			
			ImGuiStyle const &style{ImGui::GetStyle()};
			
			/// @todo: Make editor console tag filter work.
			constexpr char const * textLabelTags{"Tags"};
			ImGui::SetNextItemWidth
			(
				ImGui::GetContentRegionAvail().x -
				ImGui::CalcTextSize(textLabelTags).x -
				style.ItemSpacing.x
			);
			editor.input("_ed-console-tags", textLabelTags, m_inputTags);
			ImGui::PopItemWidth();
			
			ImGui::Separator();
	
			// Input.
			/// @todo: Make editor console input work.
			/// @todo: Add a multiline mode?
			
			constexpr char const *textButton{"Run"};
			
			float const
				widthButton
				{
					ImGui::CalcTextSize(textButton).x +
					style.FramePadding.x * 2.0f +
					style.ItemSpacing.x
				},
				widthInput
				{
					ImGui::GetContentRegionAvail().x -
					widthButton
				};
			
			auto const
				&fonts{ImGui::GetIO().Fonts->Fonts};
			
			float const
				diffSizeFonts{fonts.front()->FontSize - fonts.back()->FontSize};
			
			ImVec2 const
				paddingInputCommand
				{
					style.FramePadding.x,
					style.FramePadding.y + diffSizeFonts / 4.0f // Font sizes are double.
				};
			
			ImGui::SetNextItemWidth(widthInput);
			ImGui::AlignTextToFramePadding();
			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, paddingInputCommand);
			ImGui::PushFont(ImGui::GetIO().Fonts->Fonts.back());
			
			// Will be set either by pressing enter in the
			// text input field or by clicking the run button.
			bool run{false};
			
			/// @todo: Hacky solution not to lose focus after pressing enter, try and fix.
			
			if (m_modifiedBuffer)
			{
				ImGui::SetKeyboardFocusHere();
			}
			
			m_modifiedBuffer = false;
			
			if (ImGui::InputText("##_ed-console-input", &m_inputCommand.buffer, ImGuiInputTextFlags_EnterReturnsTrue))
			//if (editor.input("_ed-console-input", nullptr, m_inputCommand))
			{
				/// @todo: This works, but maybe look into ImGuiInputTextFlags_EnterReturnsTrue and putting this functionality into Editor::input()?
//				if (ImGui::IsKeyPressedMap(ImGuiKey_Enter))
					run = true;
			}
			
			if (ImGui::IsItemActive())
			{
//				if (ImGui::IsKeyPressedMap(ImGuiKey_Enter))
//					run = true;
				
				if (!ImGui::GetIO().KeyShift)
				{
					bool
						moved{false};
					
					bool const
						bottom{(m_indexCurrHistory == m_offsetCurrHistory)};
					
					std::size_t const
						limit{m_countCurrHistory - 1};
					
					if (ImGui::IsKeyPressedMap(ImGuiKey_UpArrow, false))
					{
						moved = true;
						
						if (0 != m_indexCurrHistory)
							-- m_indexCurrHistory;
						else
							m_indexCurrHistory = limit;
					}
					else if (ImGui::IsKeyPressedMap(ImGuiKey_DownArrow, false))
					{
						moved = true;
						
						if (limit != m_indexCurrHistory)
							++ m_indexCurrHistory;
						else
							m_indexCurrHistory = 0;
					}
					
					if (moved)
					{
						if (bottom)
							m_current = m_inputCommand.buffer.c_str(); /// @todo: Not sure why, but only c_str works with the buffer string; figure it out?
						
						// Required by ImGui for manual buffer changes to work.
						ImGui::ClearActiveID();
						
						m_modifiedBuffer = true;
						
						if (m_indexCurrHistory == m_offsetCurrHistory)
							m_inputCommand.buffer = m_current;
						else
							m_inputCommand.buffer = m_history[m_indexCurrHistory];
					}
				}
			}
			
			ImGui::PopFont();
			ImGui::PopStyleVar();
			ImGui::SameLine();
			ImGui::SetNextItemWidth(widthButton);
			
			if (ImGui::Button(textButton))
				run = true;
			
			if (run)
			{
				executeAS(app, editor, m_inputCommand.buffer.c_str());
				
				m_history[m_offsetCurrHistory ++] = m_inputCommand.buffer; /// @todo: Not sure why, but only c_str works with the buffer string; figure it out?
				
				m_countCurrHistory = mth::min(m_countCurrHistory + 1, m_history.size());
				
				if (m_offsetCurrHistory >= m_countCurrHistory)
					m_offsetCurrHistory = 0;
				
				m_indexCurrHistory = m_offsetCurrHistory;
				
				// Required by ImGui for manual buffer changes to work.
				ImGui::ClearActiveID();
				
				m_modifiedBuffer = true;
				
				m_inputCommand.buffer.clear();
			}
	 
			// Output.
			
			if
			(
				ImGui::BeginChild
				(
					"_ed-console-scroll",
					ImVec2{0.0f, 0.0f},
					true,
					ImGuiWindowFlags_HorizontalScrollbar
				)
			)
			{
				ImGui::PushFont(ImGui::GetIO().Fonts->Fonts.back());
				
				/// @todo: Unify the hard-coded editor text colour constants.
				
				ImVec4 const colsLevel[]
				{
					// Message
					ImGui::GetStyleColorVec4(ImGuiCol_Text),
					
					// Debug
					ImGui::GetStyleColorVec4(ImGuiCol_TextDisabled),
					
					// Error
					{1.0f, 0.4f, 0.4f, 1.0f},
					
					// Warning
					{1.0f, 0.85f, 0.4f, 1.0f}
				};
				
				auto        const &categories(editor.logger().categories);
				auto        const &levels    (editor.logger().levels);
				auto        const &tags      (editor.logger().tags);
				auto        const &texts     (editor.logger().texts);
				std::size_t const  count     {texts.size()};
				
				auto const filter([&](std::size_t const &i) -> bool
				{
					return
					(
						(m_filterServer && categories[i] == Logger::Category::server) ||
						(m_filterClient && categories[i] == Logger::Category::client)
					);
				});
				
				ImGui::Columns(3);
				
				/// @todo: Don't need this when upgrading ImGui to use new tables API.
				if (!m_didInit)
				{
					ImGui::SetColumnWidth(0, 80.0f);
					ImGui::SetColumnWidth(1, 80.0f);
					m_didInit = true;
				}

				for (std::size_t i{0}; i < count; ++ i)
					if (filter(i))
						ImGui::TextUnformatted
						(
							(Logger::Category::server == categories[i])
							?
								"server"
							:
								"client"
						);

				ImGui::NextColumn();

				for (std::size_t i{0}; i < count; ++ i)
					if (filter(i))
						ImGui::TextUnformatted(tags[i].c_str());
				
				ImGui::NextColumn();
				
				for (std::size_t i{0}; i < count; ++ i)
				{
					if (!filter(i))
						continue;
					
					ImGui::PushStyleColor
					(
						ImGuiCol_Text,
						colsLevel[static_cast<int>(levels[i])]
					);
					
					ImGui::TextUnformatted(texts[i].c_str());
					
					ImGui::PopStyleColor();
				}
				
				ImGui::EndColumns();
				
				m_autoscroll = (ImGui::GetScrollY() == ImGui::GetScrollMaxY());
				
				if (m_autoscroll)
					ImGui::SetScrollHereY(1.0f);
				
				ImGui::PopFont();
			}
			ImGui::EndChild();
		}
		
		void EditorConsole::event
		(
			app::App         &,
			Editor           &editor,
			Identifier const,
			Event      const &e
		)
		{
			if (TypeEvent::connected == e.type && m_clearOnConnection)
				editor.logger().clear();
		}
	}
}

#endif
