/**
 * @author		Ava Skoog
 * @date		2019-11-09
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/editors/EditorHierarchy.hpp>
#include <karhu/app/edt/actions/AEntitiesSelected.hpp>
#include <karhu/app/edt/actions/AViewportTargetChanged.hpp>
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/edt/UserdataEntity.hpp>
#include <karhu/app/App.hpp>
#include <karhu/app/ecs/World.hpp>

#include <karhu/tool/lib/IconFontCppHeaders/IconsForkAwesome.h>

#include <sstream>
#include <iomanip>

namespace
{
	using namespace karhu;
	using namespace karhu::edt;
	
	enum class StateNode : std::uint8_t
	{
		none     = 0,
		dragging = 1 << 0,
		dropped  = 1 << 1
	};

	karhuBITFLAGGIFY(StateNode)

	using Children = std::multimap<ecs::IDEntity, ecs::IDEntity>;
	
	thread_local std::vector<ecs::IDEntity> s_entityToScrollTo;
	
	StateNode drawEntity
	(
		app::App              &      app,
		Editor                &      editor,
		Identifier      const        identifier,
		res::IDResource              IDScene,
		ecs::IDEntity   const        entity,
		UserdataEntity  const *const data,
		Children        const *const children
	)
	{
		StateNode r{StateNode::none};
		
		ImGuiTreeNodeFlags flags
		{
			ImGuiTreeNodeFlags_SpanAvailWidth |
			ImGuiTreeNodeFlags_OpenOnArrow
		};
		
		using It = Children::const_iterator;
		std::pair<It, It> range;
		
		if (children)
			range = children->equal_range(entity);
		
		if (!children || range.first == range.second)
			flags |= ImGuiTreeNodeFlags_Leaf;
		
		bool const
			selected{editor.entitySelected(entity)},
			isEntityToScrollTo
			{(
				!s_entityToScrollTo.empty() &&
				s_entityToScrollTo.front() == entity
			)};
		
		if (selected)
			flags |= ImGuiTreeNodeFlags_Selected;
		
		if
		(
			!isEntityToScrollTo       &&
			!s_entityToScrollTo.empty() &&
			std::find
			(
				s_entityToScrollTo.begin(),
				s_entityToScrollTo.end(),
				entity
			) != s_entityToScrollTo.end()
		)
			ImGui::SetNextItemOpen(true);
		
		bool const
			active{app.ecs().entityActiveInHierarchy(entity)};
		
		if (!active)
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4{0.5f, 0.5f, 0.5f, 1.0f});
		// fiks id
		// fiks ikkje visa rute om ein prøver dra inn i ugyldig forelder
		ImGui::PushID(static_cast<int>(entity));
		
		static std::string name;
		
		if (data && 0 != data->scene && IDScene != data->scene)
		{
			name = ICON_FK_CUBES;
			IDScene = static_cast<res::IDResource>(data->scene); /// @todo: Why do we need this cast if it actually holds the resource ID and not the ECS ID?
		}
		else
			name = ICON_FK_CUBE;
		
		name += ' ';
		name += app.ecs().entityName(entity);
		
		bool const open
		(
			ImGui::TreeNodeEx
			(
				name.c_str(),
				flags
			)
		);
		
		if (isEntityToScrollTo)
			ImGui::SetScrollHereY(0.0f);
		
		ImGui::PopID();
		
		if (!ImGui::IsItemToggledOpen() && ImGui::IsItemHovered())
		{
			if (ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
			{
				if (auto transform = app.ecs().componentInEntity<gfx::Transform>(entity))
					editor.pushAction
					(
						identifier,
						AViewportTargetChanged
						{
							app,
							editor,
							transform->global().position
						}
					);
			}
			else if (ImGui::IsMouseClicked(ImGuiMouseButton_Left))
			{
				editor.pushAction
				(
					identifier,
					AEntitiesSelected
					{
						app,
						editor,
						1,
						&entity,
						!selected,
						!ImGui::GetIO().KeyShift
					}
				);
			}
		}
		
		if (ImGui::BeginDragDropSource())
		{
			r = StateNode::dragging;
			
			if (active)
				ImGui::TextUnformatted(app.ecs().entityName(entity).c_str());
			else
				ImGui::TextDisabled("%s", app.ecs().entityName(entity).c_str());
			
			ImGui::SetDragDropPayload("entity", &entity, sizeof(ecs::IDEntity));
			ImGui::EndDragDropSource();
		}
		
		if (ImGui::BeginDragDropTarget())
		{
			r = StateNode::dragging;
			
			if (ImGuiPayload const *const payload = ImGui::AcceptDragDropPayload("entity", ImGuiDragDropFlags_AcceptPeekOnly))
			{
				ImGuiStyle const
					&style{ImGui::GetStyle()};
				
				float const
					width {ImGui::GetContentRegionAvail().x},
					height{ImGui::GetItemRectSize().y};
				
				ImVec2 const
					cursor
					{
						ImGui::GetCursorPos() +
						ImGui::GetWindowPos() -
						ImVec2{0.0f, height + style.ItemSpacing.y} -
						ImVec2{ImGui::GetScrollX(), ImGui::GetScrollY()}
					};
				
				float const
					mouse  {ImGui::GetMousePos().y},
					epsilon{height * 0.2f},
					top    {cursor.y + epsilon},
					bottom {cursor.y + height - epsilon};
				
				bool const
					above{(mouse <= top)},
					below{(mouse >= bottom)};
				
				ImDrawList *const dl{ImGui::GetWindowDrawList()};
				
				if (above || below)
				{
					float const
						y{cursor.y + ((below) ? height : 0.0f)};
					
					ImVec2 const
						a{cursor.x, y},
						b{cursor.x + width, y};
					
					dl->AddLine
					(
						a,
						b,
						ImColor{style.Colors[ImGuiCol_DragDropTarget]},
						2.0f
					);
				}
				else
				{
					ImVec2 const
						max{cursor.x + width, cursor.y + height};
					
					dl->AddRect
					(
						cursor,
						max,
						ImColor{style.Colors[ImGuiCol_DragDropTarget]},
						0.0f,
						ImDrawCornerFlags_None,
						2.0f
					);
				}
				
				if (payload->IsDelivery())
				{
					auto const
						target(*static_cast<ecs::IDEntity *>(payload->Data));
				
					if (target != entity)
					{
						if (app.ecs().entityParentOf(entity, target))
						{
							log::msg("DEBUG") << "accepted pl " << target;
							
							// Sets the parent tree node and self open.
							ImGui::GetStateStorage()->SetInt(ImGui::GetItemID(), 1);
							ImGui::GetStateStorage()->SetInt(payload->SourceId,  1);
							
							r = StateNode::dropped;
						}
						else
							log::msg("DEBUG") << "accepted pl RECURSIVE";
					}
					else
						log::msg("DEBUG") << "accepted pl SELF";
				}
			}
			
			ImGui::EndDragDropTarget();
		}
		
		if (open)
		{
			if (!static_cast<bool>(StateNode::dropped & r) && children)
			{
				for (auto it(range.first); it != range.second; ++ it)
				{
					auto data{static_cast<UserdataEntity const *const>
					(
						app.ecs().entityUserdata(it->second)
					)};
					
					// Don't show destroyed entities.
					if (data && data->destroyed)
						continue;
					
					r |= drawEntity(app, editor, identifier, IDScene, it->second, data, children);
					
					if (static_cast<bool>(StateNode::dropped & r))
						break;
				}
			}
			
			ImGui::TreePop();
		}
		
		if (!active)
			ImGui::PopStyleColor();
		
		return r;
	}
	
	StateNode drawScene
	(
		app::App              &app,
		Editor                &editor,
		Identifier      const  identifier,
		res::IDResource const  IDScene,
		ecs::IDScene    const  scene
	)
	{
		StateNode r{StateNode::none};
		
		// This is the special behind the scenes scene.
		/// @todo: Would actually be practical to be able to access special editor scene components like the camera to move it precisely or change FOV etc as long as they can't be removed or have other components added, think about that?
//		if (0 == scene)
//			return r;
		
		if (!app.ecs().sceneLoaded(scene))
			return r;
		
		/// @todo: Cannot assume entity to scroll to is in this particular scene, but that's something to figure out whenever multiple scenes at the same time are being coded for in general.
		if (!s_entityToScrollTo.empty())
			ImGui::SetNextItemOpen(true);
		
		ImGuiTreeNodeFlags flags
		{
			ImGuiTreeNodeFlags_SpanAvailWidth |
			ImGuiTreeNodeFlags_OpenOnArrow    |
			ImGuiTreeNodeFlags_Framed
		};
		
		std::stringstream ss;
		ss << static_cast<int>(scene);
		
		if
		(
			ImGui::TreeNodeEx
			(
				ss.str().c_str(),
				flags
			)
		)
		{
			const auto children
			(
				app.ecs().childrenInScene(scene)
			);
			
			if (auto es = app.ecs().grandparentsInScene(scene))
			{
				for (ecs::IDEntity const e : *es)
				{
					auto data{static_cast<UserdataEntity const *const>
					(
						app.ecs().entityUserdata(e)
					)};
					
					// Don't show destroyed entities.
					if (data && data->destroyed)
						continue;
					
					r |= drawEntity(app, editor, identifier, IDScene, e, data, children);
					
					if (static_cast<bool>(StateNode::dropped & r))
						break;
				}
			}
			
			ImGui::TreePop();
		}
		
		return r;
	}
}

namespace karhu
{
	namespace edt
	{
		bool EditorHierarchy::init(app::App &, Editor &)
		{
			return true;
		}
		
		void EditorHierarchy::updateAndRender(app::App &app, Editor &editor)
		{
			StateNode r{StateNode::none};
			
			for (ecs::IDScene const scene : app.ecs().scenes())
			{
				/// @todo: Need to get the actual resource ID of each scene, either by actually giving them that ID when loading them into the ECS, or by putting it in some userdata.
				r |= drawScene(app, editor, identifier(), editor.IDSceneResource(), scene);
				
				if (static_cast<bool>(StateNode::dropped & r))
					break;
			}
			
			if (!s_entityToScrollTo.empty())
				s_entityToScrollTo.clear();
		}
		
		void EditorHierarchy::event(app::App &app, Editor &, Identifier const, Event const &e)
		{
			if (TypeEvent::entitiesSelected == e.type)
			{
				if (!e.entitiesSelected.entities.empty())
				{
					ecs::IDEntity const
						ID{e.entitiesSelected.entities[0]};
					
					app.ecs().forEachParentOfEntity
					(
						ID,
						[](ecs::IDEntity const ID) -> bool
						{
							s_entityToScrollTo.emplace_back(ID);
							return true;
						},
						true
					);
				}
			}
		}
	}
}

#endif
