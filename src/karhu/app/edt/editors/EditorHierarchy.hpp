/**
 * @author		Ava Skoog
 * @date		2019-11-09
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDITORS_EDITOR_ECS_H_
	#define KARHU_APP_EDITORS_EDITOR_ECS_H_

	#include <karhu/app/edt/util.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/core/macros.hpp>

	#include <unordered_map>
	#include <cstdint>

	namespace karhu
	{
		namespace edt
		{
			struct UserdataEntity;
			
			class EditorHierarchy : public IEditor
			{
				public:
					using IEditor::IEditor;
				
				public:
					bool init(app::App &, Editor &) override;
				
					void updateAndRender(app::App &, Editor &) override;
					void updateInViewportAndRender(app::App &, Editor &, mth::Vec2f const &) override {}
					void event(app::App &, Editor &, Identifier const, Event const &) override;
			};
		}
	}
#endif

#endif
