/**
 * @author		Ava Skoog
 * @date		2019-12-04
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDITORS_EDITOR_SETTINGS_BUILD_H_
	#define KARHU_APP_EDITORS_EDITOR_SETTINGS_BUILD_H_

	#include <memory>

	namespace karhu
	{
		namespace edt
		{
			class EditorSettingsBuild : public IEditor
			{
				public:
					EditorSettingsBuild(Identifier const identifier, std::string const &name);
					~EditorSettingsBuild();
				
				public:
					bool init(app::App &, Editor &) override;
				
					void updateAndRender(app::App &, Editor &) override;
					void updateInViewportAndRender(app::App &, Editor &, mth::Vec2f const &) override {}
				
					void event(app::App &, Editor &, Identifier const, Event const &) override {}
				
				private:
					struct Data;
					std::unique_ptr<Data> m_data;
			};
		}
	}
#endif

#endif
