/**
 * @author		Ava Skoog
 * @date		2021-01-13
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/editors/EditorPipeline.hpp>
#include <karhu/app/edt/Server.hpp>
#include <karhu/app/gfx/Pipeline.hpp>
#include <karhu/app/App.hpp>
#include <karhu/tool/SDK.hpp>

#include <karhu/app/karhuimgui.hpp>
#include <karhu/tool/lib/IconFontCppHeaders/IconsForkAwesome.h>

namespace
{
	using namespace karhu;
	using namespace karhu::edt;
	
	using P = karhu::network::Protocol;
	
	static void sendMessageToClient(Editor &editor, conv::JSON::Obj &&data)
	{
		if (auto server = editor.server())
		{
			server->messenger().send
			(
				P::serialise
				(
					P::TypeMessage::debug,
					conv::JSON::Val{std::move(data)}
				)
			);
		}
	}
	
	static void showStep
	(
		Editor                        &editor,
		app::App                      &app,
		gfx::Pipeline::DataStep       &data,
		std::size_t             const  i,
		bool                    const  isLast
	)
	{
		std::size_t
			count{data.textures.size()};
		
		if (data.extrabuffer)
			++ count;
	
		constexpr ImGuiTreeNodeFlags
			flagsBuffer
			{
				ImGuiTreeNodeFlags_SpanAvailWidth |
				ImGuiTreeNodeFlags_OpenOnArrow    |
				ImGuiTreeNodeFlags_DefaultOpen
			},
			flagsStep{flagsBuffer | ImGuiTreeNodeFlags_Framed};
		
		ImGui::PushID(static_cast<int>(i));
		
		static std::string label;
		
		label = "Step #";
		label += std::to_string(i + 1);
		
		using Type = gfx::Pipeline::Step::Type;
		
		switch (data.step.type)
		{
			case Type::groups:  label += " (groups)";  break;
			case Type::quad:    label += " (quad)";    break;
			case Type::lights:  label += " (lights)";  break;
			case Type::shadows: label += " (shadows)"; break;
		}
		
		if (ImGui::TreeNodeEx(label.c_str(), flagsStep))
		{
			if (!isLast)
			{
				if (ImGui::Checkbox("Active", &data.step.active))
				{
					app.backend().shouldRepaintViewportEditor(true);
					
					sendMessageToClient
					(
						editor,
						conv::JSON::Obj
						{
							{"e",     EventProtocol::pipelineStepActivated},
							{"index", i},
							{"set",   data.step.active}
						}
					);
				}
			}
			
			switch (data.step.type)
			{
				case Type::groups:
					break;
					
				case Type::quad:
					break;
					
				case Type::lights:
					break;
				
				case Type::shadows:
				{
					ImGui::DragFloat("Scale", &data.step.scaleShadowmaps, 0.1f, 0.1f, 100.0f);
					
					if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
					{
						app.backend().shouldRepaintViewportEditor(true);
						
						sendMessageToClient
						(
							editor,
							conv::JSON::Obj
							{
								{"e",     EventProtocol::pipelineScaleShadowmap},
								{"index", i},
								{"set",   data.step.scaleShadowmaps},
							}
						);
					}
					
					ImGui::DragFloat("Margin", &data.step.marginShadowmaps, 1.0f);
					
					if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
					{
						app.backend().shouldRepaintViewportEditor(true);
						
						sendMessageToClient
						(
							editor,
							conv::JSON::Obj
							{
								{"e",     EventProtocol::pipelineMarginShadowmap},
								{"index", i},
								{"set",   data.step.marginShadowmaps},
							}
						);
					}
					
					ImGui::DragFloat("Cutoff (fraction)", &data.step.cutoffShadowmaps, 1.0f, 0.0f, 1.0f);
					
					if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
					{
						app.backend().shouldRepaintViewportEditor(true);
						
						sendMessageToClient
						(
							editor,
							conv::JSON::Obj
							{
								{"e",     EventProtocol::pipelineCutoffShadowmap},
								{"index", i},
								{"set",   data.step.cutoffShadowmaps},
							}
						);
					}
					
					ImGui::DragFloat("Bias", &data.step.biasShadowmaps, 0.00001f, 0.000001f, 1.0f, "%.5f");
					
					if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
					{
						app.backend().shouldRepaintViewportEditor(true);
						
						sendMessageToClient
						(
							editor,
							conv::JSON::Obj
							{
								{"e",     EventProtocol::pipelineBiasShadowmap},
								{"index", i},
								{"set",   data.step.biasShadowmaps},
							}
						);
					}
					
					break;
				}
			}
			
			ImGui::TextDisabled("%lu buffers", count);
			
			std::size_t j{0};
			
			static auto const
				showBuffer([&j](std::string const &name, res::Texture const &texture)
				{
					label = '#';
					label += std::to_string(j + 1);
					label += ": ";
					label += name;
					
					if (ImGui::TreeNodeEx(label.c_str(), flagsBuffer))
					{
						ImGui::TextDisabled("%ux%u", texture.width(), texture.height());
						
						if (texture.valid())
						{
							ImVec2
								size
								{
									static_cast<float>(texture.width()),
									static_cast<float>(texture.height())
								};
							
							float const
								w{ImGui::GetContentRegionAvailWidth()};
							
							size.y *= w / size.x;
							size.x =  w;
							
							std::uint32_t
								ID{*reinterpret_cast<const std::uint32_t *>(texture.handle())};
							
							ImGui::Image
							(
								reinterpret_cast<void *>(ID),
								size,
								{0.0f, 1.0f},
								{1.0f, 0.0f},
								{1.0f, 1.0f, 1.0f, 1.0f},
								{1.0f, 1.0f, 1.0f, 1.0f}
							);
						}
						
						ImGui::TreePop();
					}
				});
			
			for (; j < data.textures.size(); ++ j)
				if (data.step.buffers[j].name != "__karhuTexID")
					showBuffer(data.step.buffers[j].name, *data.textures[j]);
		
			if (data.extrabuffer)
				showBuffer(data.step.nameExtrabuffer, *data.extrabuffer);
			
			ImGui::TreePop();
		}
		
		ImGui::PopID();
	}
	
	/// @todo: A lot of hardcoded stuff with regards to pipeline JSON formatting here.
	static void savePipeline(app::App &app, gfx::Pipeline &pipeline)
	{
		constexpr char const
			*path {SDK::paths::project::res::graphics()},
			*error{"Failed to save graphics settings: "};
		
		conv::JSON::Obj
			val;
		
		conv::Serialiser
			ser{val};
			
		pipeline.serialise(ser);

		if (0 == val.count())
		{
			log::err("Karhu")
				<< error
				<< "serialisation failed";
			
			return;
		}
		
		auto const
			input(app.fio().readStringFromResource(path));
		
		if (!input)
		{
			log::err("Karhu")
				<< error
				<< "error loading project resource '"
				<< path
				<< '\'';
			
			return;
		}
		
		conv::JSON::Parser
			parser;
		
		std::stringstream
			s;
		
		s << *input;
		
		auto
			JSON(parser.parse(s));
		
		if (!JSON)
		{
			log::err("Karhu")
				<< error
				<< "error parsing JSON from project resource '"
				<< path
				<< '\'';
			
			return;
		}
		
		if (!JSON->object())
			*JSON = conv::JSON::Obj{};
		
		if (conv::JSON::Obj *obj = JSON->object()->getObject("pipeline"))
			*obj = std::move(val);
		else
			JSON->object()->push({"pipeline", std::move(val)});
		
		std::string const
			data{JSON->dump()};
		
		if (app.fio().writeStringToResource(path, data.c_str()))
			log::msg("Karhu") << "Successfully saved graphics settings";
		else
			log::err("Karhu")
				<< error
				<< "error writing project resource '"
				<< path
				<< '\'';
	}
}

namespace karhu
{
	namespace edt
	{
		EditorPipeline::EditorPipeline
		(
			Identifier const identifier,
			std::string const &name
		)
		:
		IEditor{identifier, name}
		{
		}
		
		bool EditorPipeline::init(app::App &, Editor &)
		{
			return true;
		}
		
		void EditorPipeline::updateAndRender(app::App &app, Editor &editor)
		{
			gfx::Pipeline
				&pipeline{app.gfx().pipeline()};
			
			if (ImGui::Button(ICON_FK_FLOPPY_O))
				savePipeline(app, pipeline);
			
			if (ImGui::BeginChild("_ed-pipeline"))
			{
				ImGui::InputScalar("Min size shadowmap", ImGuiDataType_U32, &pipeline.sizeShadowmapMin);
				
				if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				{
					if (pipeline.sizeShadowmapMin < 1)
						pipeline.sizeShadowmapMin = 1;
					
					if (pipeline.sizeShadowmapMin > pipeline.sizeShadowmapMax)
						pipeline.sizeShadowmapMin = pipeline.sizeShadowmapMax;
					
					app.backend().shouldRepaintViewportEditor(true);
					
					sendMessageToClient
					(
						editor,
						conv::JSON::Obj
						{
							{"e",   EventProtocol::pipelineSizeShadowmapMin},
							{"set", pipeline.sizeShadowmapMin},
						}
					);
				}
				
				ImGui::InputScalar("Max size shadowmap", ImGuiDataType_U32, &pipeline.sizeShadowmapMax);
				
				if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				{
					if (pipeline.sizeShadowmapMax < pipeline.sizeShadowmapMin)
						pipeline.sizeShadowmapMax = pipeline.sizeShadowmapMin;
					
					app.backend().shouldRepaintViewportEditor(true);
					
					sendMessageToClient
					(
						editor,
						conv::JSON::Obj
						{
							{"e",   EventProtocol::pipelineSizeShadowmapMax},
							{"set", pipeline.sizeShadowmapMax},
						}
					);
				}
				
				for (std::size_t i{0}, count{pipeline.countSteps()}; i < count; ++ i)
					showStep(editor, app, *pipeline.step(i), i, ((i + 1) == count));
			}
			ImGui::EndChild();
		}
	}
}

#endif
