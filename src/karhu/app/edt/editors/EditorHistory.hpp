/**
 * @author		Ava Skoog
 * @date		2019-12-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDITORS_EDITOR_HISTORY_H_
	#define KARHU_APP_EDITORS_EDITOR_HISTORY_H_

	namespace karhu
	{
		namespace edt
		{
			class EditorHistory : public IEditor
			{
				public:
					using IEditor::IEditor;
				
				public:
					bool init(app::App &, Editor &) override { return true; }
				
					void updateAndRender(app::App &, Editor &) override;
					void updateInViewportAndRender(app::App &, Editor &, mth::Vec2f const &) override {}
				
					void event(app::App &, Editor &, Identifier const, Event const &) override {}
				
				private:
					bool m_autoscroll{true};
			};
		}
	}
#endif

#endif
