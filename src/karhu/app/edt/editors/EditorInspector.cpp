/**
 * @author		Ava Skoog
 * @date		2019-11-11
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/editors/EditorInspector.hpp>
#include <karhu/app/edt/actions/AValuesChanged.hpp>
#include <karhu/app/edt/actions/AEntity.hpp>
#include <karhu/app/edt/actions/AComponent.hpp>
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/edt/UserdataEntity.hpp>
#include <karhu/app/App.hpp>
#include <karhu/app/ecs/common.hpp>
#include <karhu/app/ecs/Octree.hpp>
#include <karhu/core/meta.hpp>
#include <karhu/app/edt/dropdown.inl>

#include <karhu/tool/lib/IconFontCppHeaders/IconsForkAwesome.h>

#include <sstream>
#include <iomanip>
#include <cstdlib>

namespace karhu
{
	namespace edt
	{
		bool EditorInspector::init(app::App &, Editor &)
		{
			return true;
		}
		
		void EditorInspector::updateAndRender(app::App &app, Editor &editor)
		{
			/// @todo: Deal with multi-editing.
			/// @todo: Probably get entity tags etc (more?) dynamically since they might change in a script or something and need to be updated… But I guess if it's gonna be tied into undo/redo in editor it's gonna have to be a thing a script can make the editor aware of anyway so it ties into that larger system…
			if (m_entity.any)
			{
				if (ImGui::Checkbox("##_ed-insp-e-active", &m_entity.active))
				{
					app.ecs().entityActive(m_entity.entity, m_entity.active);
					app.backend().shouldRepaintViewportEditor(true);
				}
				
				ImGui::SameLine();
				
				if (editor.input("_ed-insp-e-name", nullptr, m_entity.inputName))
					app.ecs().entityName(m_entity.entity, m_entity.inputName.buffer);
			
				ImGui::AlignTextToFramePadding();
				
				/*
				auto const
					depthAndIndex(app.octree().entityDepthAndIndexInOctree(m_entity.entity));
				
				ImGui::TextDisabled
				(
					"ID %i | Octree depth %i, index %i",
					static_cast<int>(m_entity.entity),
					static_cast<int>(depthAndIndex.first),
					static_cast<int>(depthAndIndex.second)
				);
				*/
				
				ImGui::SameLine();
				
				if (ImGui::Button(ICON_FK_TRASH))
				{
					editor.pushAction
					(
						identifier(),
						AEntity
						{
							app,
							editor,
							TypeActionEntity::destroy,
							m_entity.entity
						}
					);
					
					return;
				}
				
				if (dropdownTagsECS(app, m_entity.tags))
					app.ecs().entityTags(m_entity.entity, m_entity.tags);
				
				/// @todo: Deal with modified name.
				
				auto const components
				(
					app.ecs().componentsByIDInEntity
					(
						m_entity.entity
					)
				);
				
				auto data(static_cast<UserdataEntity const *const>(app.ecs().entityUserdata(m_entity.entity)));
				bool destroyed{false};
				
				for (auto const &ct : components)
				{
					// Do not show destroyed components.
					if (data)
					{
						auto const it(data->components.find(ct.first));
						
						if (it != data->components.end())
							if (it->second.destroyed)
								continue;
					}
					
					ImGuiTreeNodeFlags const flags
					{
						ImGuiTreeNodeFlags_SpanAvailWidth |
						ImGuiTreeNodeFlags_OpenOnArrow    |
						ImGuiTreeNodeFlags_DefaultOpen    |
						ImGuiTreeNodeFlags_Framed
					};
					
					ImGui::PushID(static_cast<int>(ct.first));
					
					if
					(
						ImGui::TreeNodeEx
						(
							app.ecs().nameOfTypeComponent(ct.second).c_str(),
							flags
						)
					)
					{
						/// @todo: Should not have to cast here when componentInEntity is updated to return ecs::Component instead of kenno::Component.
						auto c(static_cast<ecs::Component *>
						(
							app.ecs().componentInEntity
							(
								ct.first,
								m_entity.entity
							)
						));
						
						ImGui::AlignTextToFramePadding();
						ImGui::TextDisabled("%i", static_cast<int>(ct.first));
						ImGui::SameLine();
						
						Component &component{m_entity.components[ct.first]};
						
						if (ImGui::Checkbox("##_ed-insp-c-active", &component.active))
						{
							app.ecs().componentActiveInEntity(ct.first, m_entity.entity, component.active);
							app.backend().shouldRepaintViewportEditor(true);
						}
						
						ImGui::SameLine();
						
						destroyed = ImGui::Button(ICON_FK_TRASH);
						
						ImGui::Separator();
						ImGui::Spacing();
						
						if (destroyed)
							editor.pushAction
							(
								identifier(),
								AComponent
								{
									app,
									editor,
									TypeActionComponent::destroy,
									c->typeComponent(),
									m_entity.entity,
									c->identifier()
								}
							);
						else
						{
							if (c->editorEdit(app, editor))
							{
								std::size_t const size{app.ecs().sizeOfTypeOfComponent(c->typeComponent())};
								Buffer<Byte> curr;
								
								serialiseToBytesRaw(c, size, curr);
								
								if (component.prev.data)
								{
									if (!compareSerialisationsFromBytes(component.prev, curr))
									{
										Buffer<Byte> copyCurr;
										copyCurr.size = curr.size;
										copyCurr.data = std::make_unique<Byte[]>(curr.size);
										std::memcpy(copyCurr.data.get(), curr.data.get(), curr.size);
										
										editor.pushAction
										(
											identifier(),
											AValuesChanged
											{
												app,
												editor,
												c,
												size,
												std::move(component.prev),
												std::move(copyCurr),
												"component"
											}
										);
									}
								}
								
								component.prev = std::move(curr);
							}
							
							ImGui::Spacing();
						}
					
						ImGui::TreePop();
					}
					
					ImGui::PopID();
					
					if (destroyed)
						return;
				}
			}
			else if (m_entity.many)
				ImGui::Text("Multiple entities selected.");
			else
				ImGui::Text("No entity selected.");
		}
		
		///! @todo: Slett oktregreier når ferdigtesta.
		void EditorInspector::updateInViewportAndRender(app::App &app, Editor &editor, mth::Vec2f const &)
		{
//			if (m_entity.any)
//				app.octree().drawOctantForEntityInEditor(editor, app, m_entity.entity);
		}
		
		void EditorInspector::event
		(
			app::App         &app,
			Editor           &,
			Identifier const,
			Event      const &e
		)
		{
			if (TypeEvent::entitiesSelected == e.type)
			{
				m_entity.any = false;
				
				/// @todo: Deal with multi-editing.
				if (1 != e.entitiesSelected.entities.count)
				{
					m_entity.many = (0 != e.entitiesSelected.entities.count);
					return;
				}
				
				m_entity.many             = false;
				m_entity.any              = true;
				m_entity.entity           = *e.entitiesSelected.entities.data();
				m_entity.tags             = app.ecs().entityTags(m_entity.entity);
				m_entity.inputName.buffer = app.ecs().entityName(m_entity.entity);
				m_entity.active           = app.ecs().entityActiveSelf(m_entity.entity);
				
				auto const components(app.ecs().componentsByIDInEntity(m_entity.entity));
				
				m_entity.components.clear();
				
				for (auto const &it : components)
				{
					auto const
						c(app.ecs().componentInEntity(it.first, m_entity.entity));
					
					std::size_t const size{app.ecs().sizeOfTypeOfComponent(it.second)};
					Buffer<Byte> prev;
					serialiseToBytesRaw(c, size, prev);
					
					m_entity.components.emplace
					(
						it.first,
						Component
						{
							c->activeSelf(),
							std::move(prev)
						}
					);
				}
			}
			else if (TypeEvent::entity == e.type)
			{
				/// @todo: Deal with destruction of multiple entities in inspector editor event callback.
				
				if (TypeActionEntity::destroy == e.entity.action)
				{
					if (m_entity.any && !m_entity.many)
						if (e.entity.entity == m_entity.entity)
							m_entity.any = false;
				}
			}
			else if (TypeEvent::component == e.type)
			{
				/// @todo: Deal with creation and destruction of multiple components in inspector editor event callback?
				
				if (TypeActionComponent::create == e.component.action)
				{
					if (m_entity.any && m_entity.entity == e.component.entity)
					{
						std::size_t const size{app.ecs().sizeOfTypeOfComponent(e.component.type)};
						Buffer<Byte> prev;
						serialiseToBytesRaw(app.ecs().componentInEntity(e.component.component, e.component.entity), size, prev);
						
						m_entity.components.emplace
						(
							e.component.component,
							Component
							{
								true, // Should be safe to assume active on creation in the editor.
								std::move(prev)
							}
						);
					}
				}
				else if (TypeActionComponent::destroy == e.component.action)
				{
					if (m_entity.any)
					{
						auto &as(m_entity.components);
						auto const it(as.find(e.component.component));
						
						if (it != as.end())
							as.erase(it);
					}
				}
			}
		}
	}
}

#endif
