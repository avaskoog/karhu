/**
 * @author		Ava Skoog
 * @date		2019-12-04
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/editors/EditorSettingsBuild.hpp>
#include <karhu/app/edt/Editor.hpp>
#include <karhu/tool/proj/file/Project.hpp>
#include <karhu/core/platform.hpp>

#include <karhu/app/karhuimgui.hpp>

#include <map>
#include <vector>

namespace karhu
{
	namespace edt
	{
		using SettingsSystem = tool::proj::file::build::Build::SettingsSystem;
		using SettingsSSH    = tool::proj::file::build::Build::SettingsSSH;
		
		struct EditorSettingsBuild::Data
		{
			std::vector<std::unique_ptr<SettingsSSH>>           settingsSSH;
			std::map<Platform, std::unique_ptr<SettingsSystem>> settingsSystem;
		};
		
		EditorSettingsBuild::EditorSettingsBuild
		(
			Identifier const identifier,
			std::string const &name
		)
		:
		IEditor{identifier, name},
		m_data {std::make_unique<Data>()}
		{
		}
		
		bool EditorSettingsBuild::init(app::App &, Editor &editor)
		{
			/// @todo: Move this stuff to a project load event instead.
			/// @todo: How to reconcile project port settings with per-platform ones?
			
			auto const project(editor.project());
			
			if (!project)
				return true;
			
			auto const build(project->build());
			
			if (!build)
				return true;
			
			{
				auto const systems(build->systems());
				
				using T = std::underlying_type_t<Platform>;
				
				for (T pf{static_cast<T>(Platform::all)}; pf; pf >>= 1)
				{
					auto const p{static_cast<Platform>(pf - (pf >> 1))};
					
					bool found{false};
					std::size_t i{0};
					for (; i < systems.size(); ++ i)
					{
						SettingsSystem const &s{systems[i]};
						
						if (s.name == platform::identifier(p))
						{
							found = true;
							break;
						}
					}
					
					m_data->settingsSystem.emplace
					(
						p,
						std::make_unique<SettingsSystem>
					 	(
							((found) ? std::move(systems[i]) : SettingsSystem{})
						)
					);
				}
			}
			
			{
				auto const SSHs(build->SSHs());
				
				for (SettingsSSH const &SSH : SSHs)
					m_data->settingsSSH.emplace_back
					(
						std::make_unique<SettingsSSH>
						(
							std::move(SSH)
						)
					);
			}
			
			return true;
		}
		
		/// @todo: Actually save changes and make everything work with undo/redo.
		void EditorSettingsBuild::updateAndRender
		(
			app::App &,
			Editor   &editor
		)
		{
			constexpr ImGuiTreeNodeFlags const
				flagsClosed
				{
					ImGuiTreeNodeFlags_SpanAvailWidth |
					ImGuiTreeNodeFlags_OpenOnArrow    |
					ImGuiTreeNodeFlags_Framed,
				},
				flagsOpen
				{
					flagsClosed                       |
					ImGuiTreeNodeFlags_DefaultOpen
				};
	
			if (editor.karhuInstalled())
			{
				editor.drawDropdownPlatform(1);
			
				if (ImGui::TreeNodeEx("App build steps", flagsOpen))
				{
					editor.drawCheckboxesBuildstepsProject(false);
					ImGui::TreePop();
				}
		
				if (ImGui::TreeNodeEx("Karhu build steps", flagsOpen))
				{
					editor.drawCheckboxesBuildstepsKarhu(false);
					ImGui::TreePop();
				}
				
				if (ImGui::TreeNodeEx("Configuration", flagsOpen))
				{
					editor.drawRadiobuttonsBuildconfig(false);
					ImGui::TreePop();
				}
			
				const Platform p{editor.platform()};
				
				// Hide this altogether if not found since it
				// exposes more engine details than necessary.
				if (!m_data->settingsSystem.empty())
				{
					using namespace std::string_literals;
					
					auto           const &SSHs  (m_data->settingsSSH);
					SettingsSystem       &system{*m_data->settingsSystem[p]};
					
					const std::string title{"Specific to "s + platform::name(p)};
				
					if (ImGui::TreeNodeEx(title.c_str(), flagsClosed))
					{
						static std::size_t index{0}; /// @todo: Should not be a static variable.
						
						auto const name([&SSHs](const std::size_t index)
						{
							if (0 == index)
								return "None";
							else
								return SSHs[index - 1]->name.c_str();
						});
						
						auto const string([&SSHs](const std::size_t index)
						{
							if (0 == index)
								return "";
							else
								return SSHs[index - 1]->name.c_str();
						});
						
						/// @todo: Only do this when the platform changes and at start.
						for (std::size_t i{0}; i < SSHs.size() + 1; ++ i)
							index = (system.SSH == string(i));
						
						if
						(
							ImGui::BeginCombo
							(
								"SSH",
								name(index)
							)
						)
						{
							for (std::size_t i{0}; i < SSHs.size() + 1; ++ i)
							{
								bool const selected{(index == i)};
								
								if (ImGui::Selectable(name(i), selected))
								{
									index       = i;
									system.SSH = (0 == index) ? "" : name(i);
								}
								
								if (selected)
									ImGui::SetItemDefaultFocus();
							}
							
							ImGui::EndCombo();
						}
						
						switch (p)
						{
							case Platform::mac:
								break;
								
							case Platform::linux:
								break;
							
							case Platform::windows:
								break;
							
							case Platform::iOS:
								editor.input("build-team-id", "Team ID", system.teamID);
								break;
							
							case Platform::android:
								break;
								
							case Platform::web:
								break;
							
							default:
								break;
						}
				
						ImGui::TreePop();
					}
				}
				
				if (!m_data->settingsSSH.empty())
				{
					if (ImGui::TreeNodeEx("SSH setup", flagsClosed))
					{
						for (std::size_t i{0}; i < m_data->settingsSSH.size(); ++ i)
						{
							auto &SSH(*m_data->settingsSSH[i]);
							
							constexpr ImGuiTreeNodeFlags const flags
							{
								ImGuiTreeNodeFlags_SpanAvailWidth |
								ImGuiTreeNodeFlags_OpenOnArrow    |
								ImGuiTreeNodeFlags_DefaultOpen
							};
							
							// To prevent closing the node when the name changes.
							/// @todo: This doesn't work, textfield still loses focus when editing, so will have to store title in a different buffer and only update it when editing finishes.
							std::string const title{SSH.name + "##ssh-" + std::to_string(i)};
							
							if (ImGui::TreeNodeEx(title.c_str(), flags))
							{
								editor.input("build-ssh-name", "Name",      SSH.name);
								editor.input("build-ssh-addr", "user@host", SSH.addr);
								editor.input("build-ssh-dir",  "Directory", SSH.dir);
								
								ImGui::TreePop();
							}
						}
						
						/// @todo: Buttons to add/remove.
						
						ImGui::TreePop();
					}
				}
			}
			
			if (ImGui::TreeNodeEx("Debug network", flagsClosed))
			{
				editor.drawBuildconfigNetworking(false);
				ImGui::TreePop();
			}
		}
		
		EditorSettingsBuild::~EditorSettingsBuild() = default;
	}
}

#endif
