/**
 * @author		Ava Skoog
 * @date		2019-11-22
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDITORS_EDITOR_CONSOLE_H_
	#define KARHU_APP_EDITORS_EDITOR_CONSOLE_H_

	#include <karhu/app/edt/input.hpp>

	namespace karhu
	{
		namespace edt
		{
			class EditorConsole : public IEditor
			{
				public:
					using IEditor::IEditor;
				
				public:
					bool init(app::App &, Editor &) override;
				
					void updateAndRender(app::App &, Editor &) override;
					void updateInViewportAndRender(app::App &, Editor &, mth::Vec2f const &) override {}
				
					void event(app::App &, Editor &, Identifier const, Event const &) override;
				
				private:
					/// @todo: Set up with editor settings file.
					bool
						m_didInit          {false},
						m_autoscroll       {true},
						m_clearOnConnection{true},
						m_filterServer     {true},
						m_filterClient     {true},
						m_modifiedBuffer   {false};
				
					InputString
						m_inputTags,
						m_inputCommand;
				
					std::size_t
						m_countCurrHistory {0},
						m_offsetCurrHistory{0},
						m_indexCurrHistory {0};
				
					std::string
						m_current;
				
					std::array<std::string, 128>
						m_history;
			};
		}
	}
#endif

#endif
