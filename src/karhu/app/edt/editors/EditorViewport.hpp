/**
 * @author		Ava Skoog
 * @date		2019-11-09
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDITORS_EDITOR_VIEWPORT_H_
	#define KARHU_APP_EDITORS_EDITOR_VIEWPORT_H_

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/maths.hpp>
	#include <karhu/core/Deltatimer.hpp>
	
	#include <memory>

	namespace karhu
	{
		namespace edt
		{
			class EditorViewport : public IEditor
			{
				public:
					using IEditor::IEditor;
				
				public:
					bool init(app::App &, Editor &) override;
				
					void updateAndRender(app::App &, Editor &) override;
					void updateInViewportAndRender(app::App &, Editor &, mth::Vec2f const &) override {}
					void event(app::App &, Editor &, Identifier const, Event const &) override;
				
				private:
					void updateCamera(app::App &, Editor &);
					void updateTransformation(app::App &, Editor &, mth::Vec4f *const output = nullptr);
				
				private:
					enum class Drag : std::uint8_t
					{
						none,
						turn,
						pan,
						gizmo
					} m_drag{Drag::none};
				
					struct Entity
					{
						bool
							any{false};
						
						ecs::IDEntity
							entity;
						
						ecs::IDComponent
							transform;
						
						Transformation
							gizmo{Transformation::position};
						
						mth::Mat4f
							matrix,
							deltamatrix;
						
						mth::Vec4f
							prev;
					} m_entity;
				
					mth::Vec2f
						m_size    {800.0f, 600.0f},
						m_sizePrev{m_size};
			};
		}
	}
#endif

#endif
