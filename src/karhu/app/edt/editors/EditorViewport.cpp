/**
 * @author		Ava Skoog
 * @date		2019-11-09
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Gizmo weird from some angles / directions.
/// @todo: Have to be able to drag in window when undocked without moving window.

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/editors/EditorViewport.hpp>
#include <karhu/app/edt/actions/AEntitiesSelected.hpp>
#include <karhu/app/edt/actions/AEntitiesTransformed.hpp>
#include <karhu/app/edt/actions/AViewportTargetChanged.hpp>
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/App.hpp>

#include <karhu/app/lib/imguizmo/ImGuizmo.h>

namespace
{
	using namespace karhu;
	using namespace karhu::edt;
	
	/// @todo: Perhaps these shouldn't be static, but they will be for now until I'm sure where to put it.
	
	// According to ImGuizmo's GitHub page,
	// "snap points to a float[3] for translation and to a single float for scale or rotation"
	
	static float
		s_snapScalar  {1.0f},
		s_snapRotation{10.0f};
	
	static float
		s_snapVector[3]{s_snapScalar, s_snapScalar, s_snapScalar};
}

namespace karhu
{
	namespace edt
	{
		/// @todo: Move the editor camera creation code to the main editor init instead of viewport editor init.
		bool EditorViewport::init(app::App &app, Editor &editor)
		{
			app.gfx().modeRendersize(gfx::ModeRendersize::custom);
			
			// Create the editor camera in the special scene 0.

			{
				editor.camera().texture = app.gfx().createTexture
				(
					gfx::Texture::Type::colour,
					gfx::Pixelbuffer::Format::RGBA
				);
				
				editor.camera().depth = app.gfx().createTexture
				(
					gfx::Texture::Type::depth
				);
				
				editor.camera().target = app.gfx().createRendertarget();
				
				if (!editor.camera().texture || !editor.camera().depth || !editor.camera().target)
				{
					log::err("Karhu") << "Failed to set up editor viewport camera rendertarget";
					return false;
				}
				
				editor.camera().target->texture(*editor.camera().texture);
				editor.camera().target->depth(*editor.camera().depth);
				editor.camera().target->tieSizeToViewport(true);
				editor.camera().target->size(static_cast<std::uint32_t>(m_size.x), static_cast<std::uint32_t>(m_size.y));
				
				if (!editor.camera().target->bake())
				{
					log::err("Karhu") << "Failed to bake editor viewport camera rendertarget";
					return false;
				}
				
				ecs::IDEntity const entity{app.ecs().createEntityInScene(0, "_cam-editor")};
				
				if (0 == entity)
				{
					log::err("Karhu") << "Failed to set up editor viewport camera entity";
					return false;
				}
				
				editor.camera().entity = entity;
				
				auto transform(app.ecs().createComponentInEntity<gfx::Transform>(entity));
				
				/// @todo: Shouldn't be needing this if we get dependencies for components working, since camera should depend on transform and create one automatically.
				if (!transform)
				{
					log::err("Karhu") << "Failed to set up editor viewport camera transform component";
					return false;
				}
				
				editor.camera().transform = transform->identifier();
				
				auto camera(app.ecs().createComponentInEntity<gfx::Camera>(entity));
				
				if (!camera)
				{
					log::err("Karhu") << "Failed to set up editor viewport camera camera component";
					return false;
				}
				
				editor.camera().camera = camera->identifier();
				
				camera->target(editor.camera().target.get());
				
				/// @todo: Set up editor camera like we want.
			}
			
			return true;
		}
		
		void EditorViewport::updateAndRender(app::App &app, Editor &editor)
		{
			if (ImGui::RadioButton("Move", (Transformation::position == m_entity.gizmo)))
				m_entity.gizmo = Transformation::position;
			
			ImGui::SameLine();
			
			if (ImGui::RadioButton("Rotate", (Transformation::rotation == m_entity.gizmo)))
				m_entity.gizmo = Transformation::rotation;
			
			ImGui::SameLine();
			
			if (ImGui::RadioButton("Scale", (Transformation::scale == m_entity.gizmo)))
				m_entity.gizmo = Transformation::scale;
			
			ImGui::SameLine();
			
			ImGui::PushItemWidth(50.0f);
			
			if (ImGui::InputFloat("Snap move/scale", &s_snapScalar))
				s_snapVector[0] = s_snapVector[1] = s_snapVector[2] = s_snapScalar;
			
			ImGui::SameLine();
			
			ImGui::InputFloat("Snap rotate (deg)", &s_snapRotation);
			
			ImGui::PopItemWidth();
		/*
			ImGui::Columns(5);
			ImGui::DragFloat("Pan speed", &editor.camera().panspeed, 0.01f);
			ImGui::NextColumn();
			ImGui::DragFloat("Turn speed", &editor.camera().turnspeed, 0.01f);
			ImGui::NextColumn();
			ImGui::DragFloat("Zoom speed", &editor.camera().zoomspeed, 0.01f);
			ImGui::NextColumn();
			ImGui::Checkbox("Invert X", &editor.camera().invertX);
			ImGui::NextColumn();
			ImGui::Checkbox("Invert Y", &editor.camera().invertY);
			ImGui::Columns(1);
			*/
			
			gfx::Camera *camera
			(
				app.ecs().componentInEntity<gfx::Camera>
				(
					editor.camera().camera,
					editor.camera().entity
				)
			);
			
			gfx::Transform *transform
			{
				app.ecs().componentInEntity<gfx::Transform>
				(
					m_entity.entity
				)
			};
			
			ImVec2 const
				avail{ImGui::GetContentRegionAvail()};
			
			mth::Vec2f const
				size{sizeWithinBounds
				(
					editor.camera().texture->width(),
					editor.camera().texture->height(),
					avail.x,
					avail.y
				)};
			
			if (m_size != size)
			{
				auto const w(static_cast<std::uint32_t>(size.x * app.win().top()->scaleWidth()));
				auto const h(static_cast<std::uint32_t>(size.y * app.win().top()->scaleHeight()));
				
				app.gfx().rendersizeCustom(w, h);
				
//				camera->target()->size(w, h);
//				camera->target()->bake();
				
				m_size = size;
				
				app.backend().shouldRepaintViewportEditor(true);
			}
		
			const auto cursor{ImGui::GetCursorScreenPos()};
			
			/// @todo: Add border.
			editor.image
			(
				*editor.camera().texture,
				size,
				{0.0f, 1.0f},
				{1.0f, 0.0f}
			);
			
			bool const
				clicked     {ImGui::IsItemClicked()},
				rightclicked{ImGui::IsItemClicked(ImGuiMouseButton_Right)};
			
			// Initiate drag.
			if (clicked)
			{
				if (ImGui::GetIO().KeyAlt)
					m_drag = Drag::turn;
				else if (ImGui::GetIO().KeyCtrl)
					m_drag = Drag::pan;
			}
			// Process drag.
			else if (Drag::none != m_drag)
			{
				if (!ImGui::IsMouseDown(0))
				{
					if (Drag::gizmo == m_drag)
					{
						/// @todo: Deal with multiselect.
						
						std::vector<ecs::IDEntity>
							entities{editor.entitiesSelected()};
						
						std::vector<mth::Vec4f>
							curr(1),
							prev{m_entity.prev};
						
						switch (m_entity.gizmo)
						{
							case Transformation::position:
								curr[0] = mth::Vec4f{transform->global().position, 0.0f};
								break;
								
							case Transformation::origin:
								curr[0] = mth::Vec4f{transform->global().origin, 0.0f};
								break;
								
							case Transformation::scale:
								curr[0] = mth::Vec4f{transform->global().scale, 0.0f};
								break;
								
							case Transformation::rotation:
								curr[0] = reinterpret_cast<mth::Vec4f const &>(transform->global().rotation);
								break;
						}
						
						editor.pushAction
						(
							identifier(),
							AEntitiesTransformed
							{
								app,
								editor,
								m_entity.gizmo,
								std::move(entities),
								std::move(curr),
								std::move(prev)
							}
						);
					}
					
					m_drag = Drag::none;
				}
				else
				{
					const ImVec2 drag{ImGui::GetMouseDragDelta()};
					
					if (Drag::turn == m_drag)
					{
						const float
							x{(editor.camera().invertX) ? -1.0f : 1.0f},
							y{(editor.camera().invertY) ? -1.0f : 1.0f};
						
						editor.camera().yaw   -= drag.x * editor.camera().turnspeed * x;
						editor.camera().pitch += drag.y * editor.camera().turnspeed * y;
					}
					else if (Drag::pan == m_drag)
					{
						auto transform(app.ecs().componentInEntity<gfx::Transform>
						(
							editor.camera().transform,
							editor.camera().entity
						));
						
						const auto &t(transform->local());
						
						editor.camera().position += t.right() * drag.x * editor.camera().panspeed;
						editor.camera().position += t.up()    * drag.y * editor.camera().panspeed;
					}
					
					updateCamera(app, editor);
					
					ImGui::ResetMouseDragDelta();
				}
			}
			
			const bool hovered{ImGui::IsItemHovered()};
			bool usingGizmo{false};
			
			ImGuizmo::Enable
			((
				Drag::none  == m_drag ||
				Drag::gizmo == m_drag
			));
			
			ImGuizmo::SetDrawlist();
			ImGui::PushClipRect(cursor, {cursor.x + size.x, cursor.y + size.y}, false);
			ImGuizmo::SetRect(cursor.x, cursor.y, size.x, size.y);
			
			editor.updateInViewportAndRender(app, size);
			
			camera =
			(
				app.ecs().componentInEntity<gfx::Camera>
				(
					editor.camera().camera,
					editor.camera().entity
				)
			);
			
			transform =
			(
				app.ecs().componentInEntity<gfx::Transform>
				(
					editor.camera().transform,
					editor.camera().entity
				)
			);
	
			if (!editor.gizmo())
			{
				if (m_entity.any)
				{
					mth::Mat4f const
						 V{camera->matrixView(*transform)},
						&P{camera->matrixProjection()};
					
					ImGuizmo::OPERATION operation;
					
					switch (m_entity.gizmo)
					{
						case Transformation::position:
						case Transformation::origin:
							operation = ImGuizmo::TRANSLATE;
							break;
							
						case Transformation::rotation:
							operation = ImGuizmo::ROTATE;
							break;
							
						case Transformation::scale:
							operation = ImGuizmo::SCALE;
							break;
					}
					
					ImGuizmo::Manipulate
					(
						&V[0][0],
						&P[0][0],
						operation,
						ImGuizmo::WORLD,
						&m_entity.matrix[0][0],
						&m_entity.deltamatrix[0][0],
						(
							(ImGui::GetIO().KeyShift)
							?
								((ImGuizmo::ROTATE == operation) ? &s_snapRotation : &s_snapVector[0])
							:
								nullptr
						)
					);
					
					if ((usingGizmo = ImGuizmo::IsUsing()))
					{
						if (Drag::gizmo != m_drag)
						{
							m_drag = Drag::gizmo;
							
							auto transform
							(
								app.ecs().componentInEntity<gfx::Transform>
								(
									m_entity.transform,
									m_entity.entity
								)
							);
							
							switch (m_entity.gizmo)
							{
								case Transformation::position:
									m_entity.prev = mth::Vec4f{transform->global().position, 0.0f};
									break;
								
								case Transformation::rotation:
									m_entity.prev = reinterpret_cast<mth::Vec4f const &>(transform->global().rotation);
									break;
								
								case Transformation::scale:
									m_entity.prev = mth::Vec4f{transform->global().scale, 0.0f};
									break;
								
								case Transformation::origin:
									break;
							}
						}
						
						updateTransformation(app, editor);
					}
					
					ImGui::SetCursorPos(cursor);
				}
			}
			else
				usingGizmo = ImGuizmo::IsUsing();
			
			// Click to select.
			if (Drag::none == m_drag)
			{
				// Deselect all.
				if (rightclicked)
				{
					ecs::IDEntity const
						entity{0};
					
					editor.pushAction
					(
						identifier(),
						AEntitiesSelected
						{
							app,
							editor,
							1,
							&entity,
							false,
							true
						}
					);
				}
				// Select or focus clicked.
				else if (clicked)
				{
					ImVec2 const
						pos{ImGui::GetMousePos() - cursor};
					
					gfx::Pipeline const
						&pipeline{app.gfx().pipeline()};

					gfx::Pixelbuffer
						buffer;

					for (std::size_t i{0}; i < pipeline.countSteps(); ++ i)
					{
						auto &step(*pipeline.step(i));
						
						if (gfx::Pipeline::Step::Type::groups == step.step.type)
						{
							// The editor adds the ID step at the end so it's guaranteed last.
							if (step.textures.back()->copy(buffer))
							{
								auto const
									data(reinterpret_cast<std::int32_t const *>(buffer.components.get()));
								
								auto const
									w(static_cast<float>(buffer.width)),
									h(static_cast<float>(buffer.height));
								
								auto const
									x    (static_cast<std::ptrdiff_t>(mth::round(pos.x * (w / m_size.x)))),
									y    (static_cast<std::ptrdiff_t>(mth::round(h - pos.y * (h / m_size.y)))),
									index(x + static_cast<std::ptrdiff_t>(buffer.width) * y);
								
								if (index < static_cast<std::ptrdiff_t>(buffer.width * buffer.height))
								{
									auto const
										entity(static_cast<ecs::IDEntity>(*(data + index)));
									
									if (ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
									{
										if (0 != entity)
											if (auto transform = app.ecs().componentInEntity<gfx::Transform>(entity))
												editor.pushAction
												(
													identifier(),
													AViewportTargetChanged
													{
														app,
														editor,
														transform->global().position
													}
												);
									}
									else if (0 != entity || !ImGui::GetIO().KeyShift)
									{
										bool const
											set {(0 != entity && !editor.entitySelected(entity))},
											solo{(0 == entity || !ImGui::GetIO().KeyShift)};
										
										editor.pushAction
										(
											identifier(),
											AEntitiesSelected
											{
												app,
												editor,
												1,
												&entity,
												set,
												solo
											}
										);
									}
								}
							
								break;
							}
						}
					}
				}
			}
			
			ImGui::PopClipRect();
			
			// Process zoom.
			if (hovered && !usingGizmo)
			{
				const float scroll{ImGui::GetIO().MouseWheel};
				
				if (mth::abs(scroll) > 0.001f)
				{
					editor.camera().dist -= scroll * editor.camera().zoomspeed;
					editor.camera().dist = mth::clamp(editor.camera().dist, 0.01f, 10000.0f);
					updateCamera(app, editor);
				}
			}
			
			auto const
				&IO{ImGui::GetIO()};
			
			if
			(
				Drag::none == m_drag &&
				/// @todo: Try to generalise/centralise editor input so we don't need local stuff like this.
				!IO.WantCaptureKeyboard &&
				!IO.KeyAlt &&
				!IO.KeyShift &&
				!IO.KeyCtrl &&
				!IO.KeySuper)
			{
				if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_X)))
					m_entity.gizmo = Transformation::position;
				else if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_C)))
					m_entity.gizmo = Transformation::rotation;
				else if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_V)))
					m_entity.gizmo = Transformation::scale;
			}
		}
		
		void EditorViewport::event
		(
				  app::App   &app,
				  Editor     &editor,
			Identifier const,
			const Event      &e
		)
		{
			if (TypeEvent::entitiesSelected == e.type)
			{
				/// @todo: Support transforming multiple entities in viewport editor.
				
				m_entity.any = false;
			
				if (1 != e.entitiesSelected.entities.count)
					return;
				
				m_entity.entity = *e.entitiesSelected.entities.data();
				
				const auto transform
				(
					app.ecs().componentInEntity<gfx::Transform>
					(
						m_entity.entity
					)
				);
				
				if (transform)
				{
					m_entity.transform = transform->identifier();
					m_entity.matrix    = transform->global().matrix();
					m_entity.any       = true;
				}
			}
 			else if (TypeEvent::entitiesTransformed == e.type)
			{
				if (!m_entity.any)
					return;
				
		 		app.backend().shouldRepaintViewportEditor(true);
				
				const ecs::IDEntity *entity{nullptr};
				const mth::Vec4f    *value {nullptr};
				
				for (std::int32_t i{0}; i < e.entitiesTransformed.entities.count; ++ i)
				{
					const ecs::IDEntity *curr{e.entitiesTransformed.entities.data() + i};
					
					if (*curr == m_entity.entity)
					{
						entity = curr;
						value = e.entitiesTransformed.values.data() + i;
						break;
					}
				}
				
				if (!entity)
					return;
				
				mth::Transformf t;
				
				switch (e.entitiesTransformed.transformation)
				{
					case Transformation::position:
					{
						t.position = value->xyz();
						
						mth::decompose
						(
							m_entity.matrix,
							nullptr,
							&t.rotation,
							&t.scale
						);
						
						break;
					}
					
					case Transformation::rotation:
					{
						t.rotation = reinterpret_cast<mth::Quatf const &>(*value);
						
						mth::decompose
						(
							m_entity.matrix,
							&t.position,
							nullptr,
							&t.scale
						);
						
						break;
					}
					
					case Transformation::scale:
					{
						t.scale = value->xyz();
						
						mth::decompose
						(
							m_entity.matrix,
							&t.position,
							&t.rotation,
							nullptr
						 
						);
						
						break;
					}
					
					case Transformation::origin:
						break;
				}
				
				m_entity.matrix = t.matrix();
			}
			else if (TypeEvent::viewportTargetChanged == e.type)
			{
				app.backend().shouldRepaintViewportEditor(true);
				
				editor.camera().position = e.viewportTargetChanged.value;
				editor.camera().dist = 3.0f; /// @todo: Base distance on bounds.
				
				updateCamera(app, editor);
			}
			else if (TypeEvent::entity == e.type)
			{
				/// @todo: Deal with destruction of multiple entities in viewport editor event callback.
				if (TypeActionEntity::destroy == e.entity.action)
					if (m_entity.any)
						if (e.entity.entity == m_entity.entity)
							m_entity.any = false;
			}
			else if (TypeEvent::component == e.type)
			{
				/// @todo: Deal with destruction of multiple components in viewport editor event callback.
				if (TypeActionComponent::destroy == e.component.action)
					if (m_entity.any)
						if (e.component.component == m_entity.transform)
							m_entity.any = false;
			}
		}
		
		void EditorViewport::updateCamera(app::App &app, Editor &editor)
		{
		 	app.backend().shouldRepaintViewportEditor(true);
			
			auto transform(app.ecs().componentInEntity<gfx::Transform>
			(
				editor.camera().transform,
				editor.camera().entity
			));
		
			auto t(transform->local());
		
			t.rotation = mth::euler(editor.camera().pitch, editor.camera().yaw, 0.0f);
			
			const mth::Scalf
				distHor{editor.camera().dist * mth::cos(editor.camera().pitch)},
				distVer{editor.camera().dist * mth::sin(editor.camera().pitch)};
		
			t.position =
			{
				editor.camera().position.x - distHor * mth::sin(editor.camera().yaw),
				editor.camera().position.y + distVer,
				editor.camera().position.z - distHor * mth::cos(editor.camera().yaw)
			};
			
			transform->local(std::move(t));
		}
		
		void EditorViewport::updateTransformation
		(
			app::App &app,
			Editor &editor,
			mth::Vec4f *const output
		)
		{
		 	app.backend().shouldRepaintViewportEditor(true);
			
			mth::Vec4f r;
			
			auto transform
			(
				app.ecs().componentInEntity<gfx::Transform>
				(
					m_entity.transform,
					m_entity.entity
				)
			);
			
			mth::Transformf const
				&t{transform->global()};
			
			switch (m_entity.gizmo)
			{
				case Transformation::position:
				{
					mth::Vec3f const delta{mth::position(m_entity.deltamatrix)};
					r = mth::Vec4f{t.position + delta, 0.0f};
					break;
				}
				
				case Transformation::rotation:
				{
					mth::Quatf const
						delta{mth::rotation(m_entity.deltamatrix)},
						total{mth::inverse(delta) * t.rotation};
					
					r = reinterpret_cast<mth::Vec4f const &>(total);
					
					break;
				}
				
				case Transformation::scale:
				{
					mth::Vec3f const delta{mth::scale(m_entity.deltamatrix)};
					r = m_entity.prev * mth::Vec4f{delta, 0.0f};
					break;
				}
				
				case Transformation::origin:
					break;
			}
			
			if (output)
				*output = r;
			
			editor.entitiesTransformation
			(
				m_entity.gizmo,
				1,
				&m_entity.entity,
				&r,
				false,
				identifier()
			);
		}
	}
}

#endif
