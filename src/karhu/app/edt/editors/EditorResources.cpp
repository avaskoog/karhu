/**
 * @author		Ava Skoog
 * @date		2020-07-28
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/editors/EditorResources.hpp>
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/edt/UserdataEntity.hpp>
#include <karhu/app/App.hpp>
#include <karhu/res/Bank.hpp>
#include <karhu/res/Texture.hpp>
#include <karhu/res/Rig.hpp>
#include <karhu/res/Animation.hpp>
#include <karhu/res/Data.hpp>
#include <karhu/res/Animationcontroller.hpp>
#include <karhu/res/Animationclips.hpp>
#include <karhu/res/Font.hpp>
#include <karhu/app/nav/resources/Navmesh.hpp>
#include <karhu/app/nav/components/Navigable.hpp>

#include <karhu/app/karhuimgui.hpp>

#include <karhu/tool/lib/IconFontCppHeaders/IconsForkAwesome.h>

/// @todo: Possibly temporary
#include <karhu/app/phx/components/ColliderBox.hpp>
#include <karhu/app/phx/components/ColliderSphere.hpp>
#include <karhu/app/phx/components/ColliderMesh.hpp>
#include <karhu/app/phx/components/ColliderCylinder.hpp>
#include <karhu/app/phx/components/ColliderCapsule.hpp>
#include <karhu/app/edt/dropdown.inl>

/// @todo: Temporary for dbg dump meshes
#include <iomanip>

namespace
{
	/// @todo: Inline meshes to generate navmeshes, probably should go somewhere else later
	
	namespace cube
	{
		#include <karhu/app/lib/prim/cube-projected-smooth.inl>
	}
	
	namespace sphere
	{
		#include <karhu/app/lib/prim/sphere-smooth.inl>
	}
	
	namespace cylinder
	{
		#include <karhu/app/lib/prim/cylinder-smooth.inl>
	}
	
	namespace capsuletop
	{
		#include <karhu/app/lib/prim/capsule-top-smooth.inl>
	}
	
	namespace capsulemiddle
	{
		#include <karhu/app/lib/prim/capsule-middle-smooth.inl>
	}
	
	namespace capsulebottom
	{
		#include <karhu/app/lib/prim/capsule-bottom-smooth.inl>
	}
	
	/// @todo: Clean up and generalise ImGui splitter function.
	static bool splitter(bool split_vertically, float thickness, float* size1, float* size2, float min_size1, float min_size2, float splitter_long_axis_size = -1.0f)
	{
		using namespace ImGui;
		ImGuiContext& g = *GImGui;
		ImGuiWindow* window = g.CurrentWindow;
		ImGuiID id = window->GetID("##Splitter");
		ImRect bb;
		bb.Min = window->DC.CursorPos;
		ImVec2 const axis{(split_vertically ? ImVec2(*size1, 0.0f) : ImVec2(0.0f, *size1))};
		bb.Min.x += axis.x;
		bb.Min.y += axis.y;
		bb.Max = bb.Min;
		ImVec2 const size{CalcItemSize(split_vertically ? ImVec2(thickness, splitter_long_axis_size) : ImVec2(splitter_long_axis_size, thickness), 0.0f, 0.0f)};
		bb.Max.x += size.x;
		bb.Max.y += size.y;
		return SplitterBehavior(bb, id, split_vertically ? ImGuiAxis_X : ImGuiAxis_Y, size1, size2, min_size1, min_size2, 0.0f);
	}
	
	using namespace karhu;
	using namespace karhu::edt;
	
	/// @todo: Create some virtual method in colliders to return their geometry instead.
	static void addGeometryFromCollidersToNavmesh
	(
		app::App &app,
		nav::Navmesh &navmesh
	)
	{
		navmesh.clearGeometries();
		
		auto const
			navigables(app.ecs().componentsInActiveScenes<nav::Navigable>());
		
		std::vector<mth::Vec3f> verticesBox(cube::countVertices);
		std::memcpy(verticesBox.data(), cube::vertices, sizeof(cube::vertices));
		std::vector<gfx::Index> indicesBox(cube::countIndices);
		std::memcpy(indicesBox.data(), cube::indices, sizeof(cube::indices));
		
		std::vector<mth::Vec3f> verticesSphere(sphere::countVertices);
		std::memcpy(verticesSphere.data(), sphere::vertices, sizeof(sphere::vertices));
		std::vector<gfx::Index> indicesSphere(sphere::countIndices);
		std::memcpy(indicesSphere.data(), sphere::indices, sizeof(sphere::indices));
		
		std::vector<mth::Vec3f> verticesCylinder(cylinder::countVertices);
		std::memcpy(verticesCylinder.data(), cylinder::vertices, sizeof(cylinder::vertices));
		std::vector<gfx::Index> indicesCylinder(cylinder::countIndices);
		std::memcpy(indicesCylinder.data(), cylinder::indices, sizeof(cylinder::indices));
		
		std::vector<mth::Vec3f> verticesCapsuleTop(capsuletop::countVertices);
		std::memcpy(verticesCapsuleTop.data(), capsuletop::vertices, sizeof(capsuletop::vertices));
		std::vector<gfx::Index> indicesCapsuleTop(capsuletop::countIndices);
		std::memcpy(indicesCapsuleTop.data(), capsuletop::indices, sizeof(capsuletop::indices));
		
		std::vector<mth::Vec3f> verticesCapsuleMiddle(capsulemiddle::countVertices);
		std::memcpy(verticesCapsuleMiddle.data(), capsulemiddle::vertices, sizeof(capsulemiddle::vertices));
		std::vector<gfx::Index> indicesCapsuleMiddle(capsulemiddle::countIndices);
		std::memcpy(indicesCapsuleMiddle.data(), capsulemiddle::indices, sizeof(capsulemiddle::indices));
		
		std::vector<mth::Vec3f> verticesCapsuleBottom(capsulebottom::countVertices);
		std::memcpy(verticesCapsuleBottom.data(), capsulebottom::vertices, sizeof(capsulebottom::vertices));
		std::vector<gfx::Index> indicesCapsuleBottom(capsulebottom::countIndices);
		std::memcpy(indicesCapsuleBottom.data(), capsulebottom::indices, sizeof(capsulebottom::indices));
		
		for (nav::Navigable *const navigable : navigables)
		{
			if (!navigable->activeInHierarchy())
				continue;
			
			ecs::IDEntity const
				entity{navigable->entity()};
			
			/// @todo: Hotfix for editor navmesh build reading from destroyed entities, but need robust general solution for telling ECS not to return "destroyed" entities, separate from the acive state, or more editor issues will crop up, especially in cusotm editor scripts.
			
			auto const
				data(dynamic_cast<edt::UserdataEntity *>(app.ecs().entityUserdata(entity)));
			
			if (data)
			{
				if (data->destroyed)
					continue;
				
				auto const it(data->components.find(navigable->identifier()));
				
				if (it != data->components.end())
					if (it->second.destroyed)
						continue;
			}
			
			nav::Area const
				area{navigable->area()};
			
			auto const transform(app.ecs().componentInEntity<gfx::Transform>(entity));
			auto const collidersBox(app.ecs().componentsInEntity<phx::ColliderBox>(entity));
			auto const collidersSphere(app.ecs().componentsInEntity<phx::ColliderSphere>(entity));
			auto const collidersCylinder(app.ecs().componentsInEntity<phx::ColliderCylinder>(entity));
			auto const collidersCapsule(app.ecs().componentsInEntity<phx::ColliderCapsule>(entity));
			auto const collidersMesh(app.ecs().componentsInEntity<phx::ColliderMesh>(entity));
			
			mth::Mat4f const model{transform->global().matrix()};
			constexpr float epsilon{1.01f};
			
			for (phx::ColliderBox const *const collider : collidersBox)
			{
				std::vector<mth::Vec3f> positions{verticesBox};
				
				for (mth::Vec3f &p : positions)
				{
					p *= collider->size();
					p *= epsilon;
					p += collider->offset();
					p -= collider->origin();
					p = (model * mth::Vec4f{p, 1.0f}).xyz();
				}
				
				if (navmesh.addGeometry
				(
					area,
					positions,
					indicesBox
				))
				{
					log::msg("Karhu")
						<< "Added box collider geometry from entity "
						<< entity
						<< " ("
						<< app.ecs().entityName(entity)
						<< ')';
				}
				else
					log::err("Karhu")
						<< "Failed to add box collider geometry from entity "
						<< entity
						<< " ("
						<< app.ecs().entityName(entity)
						<< ')';
			}
			
			for (phx::ColliderSphere const *const collider : collidersSphere)
			{
				std::vector<mth::Vec3f> positions{verticesSphere};
				
				for (mth::Vec3f &p : positions)
				{
					p *= collider->diameter();
					p *= epsilon;
					p += collider->offset();
					p -= collider->origin();
					p = (model * mth::Vec4f{p, 1.0f}).xyz();
				}
				
				if (navmesh.addGeometry
				(
					area,
					positions,
					indicesSphere
				))
				{
					log::msg("Karhu")
						<< "Added sphere collider geometry from entity "
						<< entity
						<< " ("
						<< app.ecs().entityName(entity)
						<< ')';
				}
				else
					log::err("Karhu")
						<< "Failed to add sphere collider geometry from entity "
						<< entity
						<< " ("
						<< app.ecs().entityName(entity)
						<< ')';
			}
			
			for (phx::ColliderCylinder const *const collider : collidersCylinder)
			{
				std::vector<mth::Vec3f> positions{verticesCylinder};
				
				for (mth::Vec3f &p : positions)
				{
					p.x *= collider->diameter();
					p.y *= collider->height();
					p.z *= collider->diameter();
					p *= epsilon;
					p += collider->offset();
					p -= collider->origin();
					p = (model * mth::Vec4f{p, 1.0f}).xyz();
				}
				
				if (navmesh.addGeometry
				(
					area,
					positions,
					indicesCylinder
				))
				{
					log::msg("Karhu")
						<< "Added cylinder collider geometry from entity "
						<< entity
						<< " ("
						<< app.ecs().entityName(entity)
						<< ')';
				}
				else
					log::err("Karhu")
						<< "Failed to add cylinder collider geometry from entity "
						<< entity
						<< " ("
						<< app.ecs().entityName(entity)
						<< ')';
			}
			
			for (phx::ColliderCapsule const *const collider : collidersCapsule)
			{
				std::vector<mth::Vec3f>
					positions{verticesCapsuleMiddle},
					positionsTop{verticesCapsuleMiddle},
					positionsBottom{verticesCapsuleMiddle};
				
				for (mth::Vec3f &p : positions)
				{
					p.x *= collider->diameter();
					p.y *= collider->height();
					p.z *= collider->diameter();
				}
				
				for (mth::Vec3f &p : positionsTop)
				{
					p *= collider->diameter();
					p.y += collider->height() * 0.5f;
				}
				
				for (mth::Vec3f &p : positionsBottom)
				{
					p *= collider->diameter();
					p.y -= collider->height() * 0.5f;
				}
				
				positions.resize(positions.size() + positionsTop.size() + positionsBottom.size());
				
				std::memcpy
				(
					positions.data() + sizeof(capsulemiddle::vertices),
					positionsTop.data(),
					sizeof(capsuletop::vertices)
				);
				
				std::memcpy
				(
					positions.data() + sizeof(capsuletop::vertices) + sizeof(capsulemiddle::vertices),
					positionsBottom.data(),
					sizeof(capsulebottom::vertices)
				);
				
				for (mth::Vec3f &p : positions)
				{
					p *= epsilon;
					p += collider->offset();
					p -= collider->origin();
					p = (model * mth::Vec4f{p, 1.0}).xyz();
				}
				
				std::vector<gfx::Index> indices
				(
					capsulemiddle::countIndices +
					capsuletop::countIndices +
					capsulebottom::countIndices
				);
				
				std::memcpy
				(
					indices.data(),
					capsulemiddle::indices,
					sizeof(capsulemiddle::indices)
				);
				
				std::memcpy
				(
					indices.data() + sizeof(capsulemiddle::indices),
					capsuletop::indices,
					sizeof(capsuletop::indices)
				);
				
				std::memcpy
				(
					indices.data() + sizeof(capsulemiddle::indices) + sizeof(capsuletop::indices),
					capsulebottom::indices,
					sizeof(capsulebottom::indices)
				);
				
				if (!navmesh.addGeometry
				(
					area,
					positions,
					indices
				))
				{
					log::msg("Karhu")
						<< "Added capsule collider geometry from entity "
						<< entity
						<< " ("
						<< app.ecs().entityName(entity)
						<< ')';
				}
				else
					log::err("Karhu")
						<< "Failed to add capsule collider geometry from entity "
						<< entity
						<< " ("
						<< app.ecs().entityName(entity)
						<< ')';
			}
			
			for (phx::ColliderMesh const *const collider : collidersMesh)
			{
				const auto mesh(app.res().get<res::Mesh>(collider->mesh()));
				
				if (!mesh)
					continue;
				
				auto geometry(mesh->geometryAtLOD(0));
				
				std::vector<mth::Vec3f> positions{geometry->positions};
				
				for (mth::Vec3f &p : positions)
				{
					p *= epsilon;
					p += collider->offset();
					p -= collider->origin();
					p = (model * mth::Vec4f{p, 1.0f}).xyz();
				}

				if (navmesh.addGeometry
				(
					area,
					positions,
					geometry->indices
				))
				{
					log::msg("Karhu")
						<< "Added mesh collider geometry from entity "
						<< entity
						<< " ("
						<< app.ecs().entityName(entity)
						<< ") with "
						<< geometry->positions.size()
						<< " verts and "
						<< geometry->indices.size()
						<< " indices";
				}
				else
					log::err("Karhu")
						<< "Faild to add mesh collider geometry from entity "
						<< entity
						<< " ("
						<< app.ecs().entityName(entity)
						<< " ) with "
						<< geometry->positions.size()
						<< " verts and "
						<< geometry->indices.size()
						<< " indices";
			}
		}
	}
	
	static void drawPreviewTexture
	(
		app::App              &app,
		Editor                &editor,
		res::IDResource const  identifier
	)
	{
		auto const resource(app.res().get<res::Texture>(identifier));
		
		if (!resource || !resource->valid())
			/// @todo: Render error? Or assert?
			return;
		
		ImVec2 const
			avail{ImGui::GetContentRegionAvail()};

		mth::Vec2f const
			size{sizeWithinBounds
			(
				resource->width(),
				resource->height(),
				avail.x,
				avail.y - ImGui::GetCursorPos().y
			)};

		editor.image
		(
			*resource,
			size
		);
		
		ImGui::BeginGroup();
			ImGui::Text("%i x %i", resource->width(), resource->height());
		ImGui::EndGroup();
	}
	
	/// @todo: Make navmesh settings changes undoable.
	static void drawPreviewNavmesh
	(
		app::App              &app,
		Editor                &editor,
		res::IDResource const  identifier
	)
	{
		auto const resource(app.res().get<nav::Navmesh>(identifier));
		
		if (!resource)
			return;
		
		editor.pushDisabled();
		
		if (!resource->valid())
			editor.text("Not built");
		else
			editor.text("Built");
		
		editor.popDisabled();
		
		editor.header("Settings");
		
		ImGui::InputFloat("Height limit",      &resource->limitHeight);
		ImGui::InputFloat("Diameter limit",    &resource->limitDiameter);
		ImGui::InputFloat("Slope limit",       &resource->limitSlope);
		ImGui::InputFloat("Slope angle limit", &resource->limitAngleSlope);
		ImGui::InputFloat("Tile size",         &resource->sizeTile);
		
		if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
			log::msg("DEBUG")<< "Slope angle limit to degrees = " << mth::degrees(resource->limitAngleSlope);
		
		editor.header("Build");
		
		ImGuiButtonFlags
			flags{ImGuiButtonFlags_None};
		
		bool const
			disabled{resource->valid()};
		
		if (disabled)
		{
			flags |= ImGuiButtonFlags_Disabled;
			editor.pushDisabled();
		}
		
		if (ImGui::ButtonEx("Build", {0, 0}, flags))
		{
			log::msg("Karhu") << "Building navmesh...";

			addGeometryFromCollidersToNavmesh(app, *resource);
			
			if (resource->bake())
				log::msg("Karhu") << "Built navmesh";
		}
		
		if (disabled)
			editor.popDisabled();
		
		ImGui::SameLine();
		
		if (ImGui::Button("Unbuild"))
		{
			resource->clean();
			log::msg("Karhu") << "Unbuilt navmesh";
		}
		
		ImGui::SameLine();
		
		flags = ImGuiButtonFlags_None;
		
		if (!resource->valid())
		{
			flags |= ImGuiButtonFlags_Disabled;
			editor.pushDisabled();
		}
		
		if (ImGui::ButtonEx("Save", {0, 0}, flags))
		{
			log::msg("Karhu") << "Saving navmesh file...";
			
			if (app.res().save(identifier))
				log::msg("Karhu") << "Navmesh saved";
		}
		
		if (!resource->valid())
			editor.popDisabled();
	}
}

namespace karhu
{
	namespace edt
	{
		void EditorResources::updateAndRender
		(
			app::App &app,
			Editor   &editor
		)
		{				
			StateResources &state{editor.stateResources()};
			
			DataResources const &resources{editor.resources()};
			
			if (ImGui::BeginChild("_ed-res", {-1.0f, -1.0f}, false, ImGuiWindowFlags_NoScrollbar))
			{
				ImVec2 const avail{ImGui::GetContentRegionAvail()};
				ImVec2 sizesPreview, sizesList;
				
				constexpr float const sizeSplitter{2.0f}, sizeMin{20.0f};
				
				if (avail.x > avail.y)
				{
					static float sizePreview{mth::min(300.0f, avail.x / 4.0f)};
					
					const float targetSizeList{avail.x - sizePreview - sizeSplitter};
					static float sizeList{targetSizeList};
					
					if (sizeList < targetSizeList)
						sizeList = targetSizeList;
					
					sizesPreview.x = sizePreview;
					sizesList.x    = sizeList;
					sizesPreview.y = sizesList.y = avail.y;
					
					splitter(true, sizeSplitter, &sizePreview, &sizeList, sizeMin, sizeMin, avail.y);
				}
				else
				{
					static float sizePreview{mth::min(300.0f, avail.y / 4.0f)};
					
					const float targetSizeList{avail.y - sizePreview - sizeSplitter};
					static float sizeList{targetSizeList};
					
					if (sizeList < targetSizeList)
						sizeList = targetSizeList;
					
					sizesPreview.y = sizePreview;
					sizesList.y    = sizeList;
					sizesPreview.x = sizesList.x = avail.x;
					
					splitter(false, sizeSplitter, &sizePreview, &sizeList, sizeMin, sizeMin, avail.x);
				}
				
				ImVec2 cursor{ImGui::GetCursorPos()};
				
				// Preview box.
				
				if (ImGui::BeginChild("_ed-res-preview", sizesPreview, false))
					drawPreview(app, editor);
				ImGui::EndChild();
				
				// Splitter.
				
				if (avail.x > avail.y)
				{
					cursor.x += sizesPreview.x + sizeSplitter;
					ImGui::SameLine();
				}
				else
					cursor.y += sizesPreview.y + sizeSplitter;
				
				// Browser.
				
				ImGui::SetCursorPos(cursor);
				
				bool changed{false};
				
				if (ImGui::BeginChild("_ed-res-list", sizesList, false))
				{
					for (auto const &child : resources.root.children)
						changed |= drawEntriesResources(app, editor, child, state);
				}
				ImGui::EndChild();
				
				if (changed)
				{
					if (state.selected.empty())
						m_resource.any = m_resource.many = false;
					else if (state.selected.size() > 1)
						m_resource.any = m_resource.many = true;
					else
					{
						m_resource.any = m_resource.many = false;
						
						auto const it(resources.lookup.find(state.selected[0]));
						
						if (it != resources.lookup.end())
						{
							m_resource.any          = true;
							m_resource.typeEntry    = it->second->typeEntry;
							m_resource.typeResource = it->second->typeResource;
							m_resource.identifier   = it->second->resource;
							m_resource.name         = it->second->label;
						}
					}
				}
			}
			ImGui::EndChild();
		}
		
		void EditorResources::updateInViewportAndRender(app::App &app, Editor &, mth::Vec2f const &)
		{
			if (m_resource.any && !m_resource.many)
			{
				if (app.res().IDOfType<nav::Navmesh>() == m_resource.typeResource)
				{
					auto const resource(app.res().get<nav::Navmesh>(m_resource.identifier));
					
					if (resource)
						resource->draw();
				}
			}
		}
		
		void EditorResources::drawPreview(app::App &app, Editor &editor)
		{
			auto const &state(editor.stateResources());
			
			if (!m_resource.any)
				ImGui::Text("No files selected.");
			else if (m_resource.many)
				ImGui::Text("%lu selections.", state.selected.size());
			else
			{
				if (ImGui::Button(ICON_FK_REFRESH))
					editor.refreshResource(m_resource.identifier, m_resource.typeResource);
			
				ImGui::SameLine();
				
				ImGui::BeginGroup();
					ImGui::AlignTextToFramePadding();
					ImGui::TextUnformatted(m_resource.name.c_str());
				ImGui::EndGroup();
				
				if (TypeEntryResource::directory != m_resource.typeEntry)
				{
					ImGui::SameLine();
					ImGui::TextDisabled("%llu", m_resource.identifier);
		
					if (app.res().IDOfType<res::Texture>() == m_resource.typeResource)
						return drawPreviewTexture(app, editor, m_resource.identifier);
					else if (app.res().IDOfType<nav::Navmesh>() == m_resource.typeResource)
						return drawPreviewNavmesh(app, editor, m_resource.identifier);
					else if (app.res().IDOfType<res::Mesh>() == m_resource.typeResource)
					{
						if (ImGui::Button("DBG DUMP"))
						{
							if (auto mesh = app.res().get<res::Mesh>(m_resource.identifier))
							{
								if (auto geom = mesh->geometryAtLOD(0))
								{
									std::stringstream s;
									s << std::fixed;
									s << "\n\n\n\n\n\n\n\n\n\n\n";
									
									s << "size_t const countIndices = " << geom->indices.size() << ";\n";
									s << "size_t const countVertices = " << geom->positions.size() << ";\n";
									
									s << "uint32_t const indices[] = { ";
									
									for (std::size_t i{0}; i < geom->indices.size(); ++ i)
									{
										auto const &p(geom->indices[i]);
										s << (int)p;
										if ((i + 1) != geom->indices.size()) s << ", ";
									}
									
									s << " };\n";
									
									s << "float const vertices[] = { ";
									
									for (std::size_t i{0}; i < geom->positions.size(); ++ i)
									{
										auto const &p(geom->positions[i]);
										s << p.x << "f, " << p.y << "f, " << p.z << 'f';
										if ((i + 1) != geom->positions.size()) s << ", ";
									}
									
									s << " };\n";
									
									s << "float const normals[] = { ";
									
									for (std::size_t i{0}; i < geom->normals.size(); ++ i)
									{
										auto const &p(geom->normals[i]);
										s << p.x << "f, " << p.y << "f, " << p.z << 'f';
										if ((i + 1) != geom->normals.size()) s << ", ";
									}
									
									s << " };\n";
									
									s << "float const texcoords[] = { ";
									
									for (std::size_t i{0}; i < geom->UVs.size(); ++ i)
									{
										auto const &p(geom->UVs[i]);
										s << p.x << "f, " << p.y << 'f';
										if ((i + 1) != geom->UVs.size()) s << ", ";
									}
									
									s << " };\n";
									
									s << "\n\n\n\n\n\n\n\n\n\n\n";
									printf("%s", s.str().c_str());
								}
							}
						}
					}
					
					/// @todo: Implement drawing resource preview of all types.
				}
			}
		}
	}
}

#endif
