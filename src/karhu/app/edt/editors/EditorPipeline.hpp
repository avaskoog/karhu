/**
 * @author		Ava Skoog
 * @date		2021-01-13
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDITORS_EDITOR_SETTINGS_PIPELINE_H_
	#define KARHU_APP_EDITORS_EDITOR_SETTINGS_PIPELINE_H_

	#include <memory>

	namespace karhu
	{
		namespace edt
		{
			class EditorPipeline : public IEditor
			{
				public:
					EditorPipeline(Identifier const identifier, std::string const &name);
				
				public:
					bool init(app::App &, Editor &) override;
				
					void updateAndRender(app::App &, Editor &) override;
					void updateInViewportAndRender(app::App &, Editor &, mth::Vec2f const &) override {}
				
					void event(app::App &, Editor &, Identifier const, Event const &) override {}
			};
		}
	}
#endif

#endif
