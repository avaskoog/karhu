// icon-connect

{
	#include "icon-connect.inl"

	Bufferview<Byte> buffer
	{
		reinterpret_cast<Byte *>(&img[0]),
		img_len
	};

	auto texture(app.gfx().createTexture());

	if (!texture)
	{
		log::err("Karhu")
			<< "Failed to initialise editor: could not initialise texture for icon '"
			<< "icon-connect"
			<< '\'';
		
		return false;
	}

	gfx::Pixelbuffer pixels
	(
		gfx::loadPixelsFromFileInMemory(buffer)
	);
		
	if (!pixels.components)
	{
		log::err("Karhu")
			<< "Failed to initialise editor: could not load pixels for icon '"
			<< "icon-connect"
			<< '\'';
		
		return false;
	}

	texture->setFromBuffer(std::move(pixels));
			
	if (!texture->bake())
		log::err("Karhu")
			<< "Failed to initialise editor: could not bake texture for icon '"
			<< "icon-connect"
			<< '\'';

	m_textures.emplace
	(
		"icon-connect",
		std::move(texture)
	);
}			

// icon-play

{
	#include "icon-play.inl"

	Bufferview<Byte> buffer
	{
		reinterpret_cast<Byte *>(&img[0]),
		img_len
	};

	auto texture(app.gfx().createTexture());

	if (!texture)
	{
		log::err("Karhu")
			<< "Failed to initialise editor: could not initialise texture for icon '"
			<< "icon-play"
			<< '\'';
		
		return false;
	}

	gfx::Pixelbuffer pixels
	(
		gfx::loadPixelsFromFileInMemory(buffer)
	);
		
	if (!pixels.components)
	{
		log::err("Karhu")
			<< "Failed to initialise editor: could not load pixels for icon '"
			<< "icon-play"
			<< '\'';
		
		return false;
	}

	texture->setFromBuffer(std::move(pixels));
			
	if (!texture->bake())
		log::err("Karhu")
			<< "Failed to initialise editor: could not bake texture for icon '"
			<< "icon-play"
			<< '\'';

	m_textures.emplace
	(
		"icon-play",
		std::move(texture)
	);
}			

// icon-stop

{
	#include "icon-stop.inl"

	Bufferview<Byte> buffer
	{
		reinterpret_cast<Byte *>(&img[0]),
		img_len
	};

	auto texture(app.gfx().createTexture());

	if (!texture)
	{
		log::err("Karhu")
			<< "Failed to initialise editor: could not initialise texture for icon '"
			<< "icon-stop"
			<< '\'';
		
		return false;
	}

	gfx::Pixelbuffer pixels
	(
		gfx::loadPixelsFromFileInMemory(buffer)
	);
		
	if (!pixels.components)
	{
		log::err("Karhu")
			<< "Failed to initialise editor: could not load pixels for icon '"
			<< "icon-stop"
			<< '\'';
		
		return false;
	}

	texture->setFromBuffer(std::move(pixels));
			
	if (!texture->bake())
		log::err("Karhu")
			<< "Failed to initialise editor: could not bake texture for icon '"
			<< "icon-stop"
			<< '\'';

	m_textures.emplace
	(
		"icon-stop",
		std::move(texture)
	);
}			

// logo

{
	#include "logo.inl"

	Bufferview<Byte> buffer
	{
		reinterpret_cast<Byte *>(&img[0]),
		img_len
	};

	auto texture(app.gfx().createTexture());

	if (!texture)
	{
		log::err("Karhu")
			<< "Failed to initialise editor: could not initialise texture for icon '"
			<< "logo"
			<< '\'';
		
		return false;
	}

	gfx::Pixelbuffer pixels
	(
		gfx::loadPixelsFromFileInMemory(buffer)
	);
		
	if (!pixels.components)
	{
		log::err("Karhu")
			<< "Failed to initialise editor: could not load pixels for icon '"
			<< "logo"
			<< '\'';
		
		return false;
	}

	texture->setFromBuffer(std::move(pixels));
			
	if (!texture->bake())
		log::err("Karhu")
			<< "Failed to initialise editor: could not bake texture for icon '"
			<< "logo"
			<< '\'';

	m_textures.emplace
	(
		"logo",
		std::move(texture)
	);
}			

