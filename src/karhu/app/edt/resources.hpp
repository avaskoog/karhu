/**
 * @author		Ava Skoog
 * @date		2020-08-02
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDITOR_RESOURCES_H_
	#define KARHU_APP_EDITOR_RESOURCES_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/app/edt/util.hpp>
	#include <karhu/core/macros.hpp>

	#include <vector>
	#include <map>
	#include <string>
	#include <cstdint>
	#include <type_traits>

	namespace karhu
	{
		namespace edt
		{
			enum class ModeSortEntryResource : std::uint8_t
			{
				name,
				type
			};
			
			enum class TypeEntryResource : std::uint8_t
			{
				directory = 1 << 0,
				file      = 1 << 1
			};
			
			karhuBITFLAGGIFY(TypeEntryResource)

			struct EntryResource
			{
				IndexHierarchy
					index;
				
				TypeEntryResource
					typeEntry;
				
				res::IDTypeResource
					typeResource{0};
				
				res::IDResource
					resource{0};
				
				std::string
					name, path, label;
				
				std::vector<EntryResource>
					children;
			};
			
			struct DataResources
			{
				EntryResource root;
				std::map<IndexHierarchy, EntryResource *> lookup;
			};
			
			struct SettingsDrawEntriesResources
			{
				TypeEntryResource
					filterTypeEntryResource
					{
						TypeEntryResource::directory |
						TypeEntryResource::file
					};
				
				std::vector<res::IDTypeResource>
					filterTypeResource;
			};
			
			struct StateResources
			{
				std::vector<IndexHierarchy>
					selected;
			};
			
			Result<DataResources> loadEntriesResources(app::App &);
			void rebuildLookupEntriesResources(DataResources &);
			void sortEntriesResources(app::App &, ModeSortEntryResource const, EntryResource &root);
			bool drawEntriesResources(app::App &, Editor &, EntryResource const &root, StateResources &, SettingsDrawEntriesResources const & = {});
			void openPickerResources(app::App &, Editor &, res::IDResource &resource, StateResources &);
			bool drawPickerResources(app::App &, Editor &, res::IDResource &resource, DataResources &data, EntryResource const &root, StateResources &, SettingsDrawEntriesResources const & = {});
			char const *iconForTypeResourceByID(app::App &, res::IDTypeResource const);
		}
	}
#endif

#endif
