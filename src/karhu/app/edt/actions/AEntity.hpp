/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDT_ACTIONS_AENTITY_H_
	#define KARHU_APP_EDT_ACTIONS_AENTITY_H_

	#include <karhu/app/edt/actions/IAction.hpp>
	#include <karhu/app/edt/actions/common.hpp>
	#include <karhu/app/ecs/common.hpp>

	namespace karhu
	{
		namespace edt
		{
			class AEntity : public IActionWithMove<AEntity>
			{
				public:
					AEntity
					(
						app::App               &,
						Editor                 &,
						TypeActionEntity const,
						ecs::IDEntity    const = 0
					);
				
				public:
					char const *name() const override;
					bool valid(app::App &, Editor &) const override;
					bool apply(app::App &, Editor &) override;
					bool undo(app::App &, Editor &) override;
					void clean(app::App &, Editor &) override;
				
					TypeActionEntity type() const noexcept { return m_type; }
					ecs::IDEntity entity() const noexcept { return m_entity; }
					ecs::IDEntity entityDuplicated() const noexcept { return m_entityDuplicated; }
				
				private:
					TypeActionEntity
						m_type;
				
					ecs::IDEntity
						m_entity          {0},
						m_entityDuplicated{0};
			};
		}
	}
#endif

#endif
