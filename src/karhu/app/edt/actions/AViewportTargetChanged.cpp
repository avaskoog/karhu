/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/actions/AViewportTargetChanged.hpp>
#include <karhu/app/edt/Editor.hpp>

namespace karhu
{
	namespace edt
	{
		AViewportTargetChanged::AViewportTargetChanged
		(
			app::App         &app,
			Editor           &editor,
			mth::Vec3f const &curr
		)
		:
		IActionWithMove{app, editor},
		m_curr         {curr},
		m_prev         {editor.targetViewport()}
		{
		}
		
		char const *AViewportTargetChanged::name() const
		{
			return "change viewport target";
		}
		
		bool AViewportTargetChanged::valid(app::App &, Editor &) const
		{
			return (m_curr != m_prev);
		}
		
		bool AViewportTargetChanged::apply(app::App &, Editor &editor)
		{
			editor.targetViewport(m_curr, caller);
			return true;
		}
		
		bool AViewportTargetChanged::undo(app::App &, Editor &editor)
		{
			editor.targetViewport(m_prev, caller);
			return true;
		}
		
		void AViewportTargetChanged::clean(app::App &, Editor &) {}
	}
}

#endif
