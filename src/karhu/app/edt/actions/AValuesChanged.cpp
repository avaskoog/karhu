/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/actions/AValuesChanged.hpp>
#include <karhu/core/debugging.hpp>

namespace karhu
{
	namespace edt
	{
		/**
		 * Previous and current values are expected to have
		 * already been serialised and compared before pushing
		 * this action, so the buffers should be moved straight
		 * into the action at construction.
		 *
		 * @see serialiseToBytes()
		 * @see deserialiseFromBytes()
		 * @see compareSerialisationsFromBytes()
		 */
		AValuesChanged::AValuesChanged
		(
			app::App     &       app,
			Editor       &       editor,
			void         * const data,
			std::size_t    const typesize,
			Buffer<Byte> &&      prev,
			Buffer<Byte> &&      curr,
			char const   * const label
		)
		:
		IActionWithMove{app, editor},
		m_name         {std::string{"modify "} + label},
		m_data         {data},
		m_size         {typesize},
		m_prev         {std::move(prev)},
		m_curr         {std::move(curr)}
		{
			karhuASSERT(m_data);
			karhuASSERT(0 != m_size)
			karhuASSERT(m_prev.data);
			karhuASSERT(m_curr.data);
			karhuASSERT(m_size == m_prev.size);
			karhuASSERT(m_size == m_curr.size);
		}
		
		char const *AValuesChanged::name() const
		{
			return m_name.c_str();
		}
		
		bool AValuesChanged::valid(app::App &, Editor &) const
		{
			return true;
		}
		
		bool AValuesChanged::apply(app::App &, Editor &)
		{
			// Do nothing the first time; changes already made.
			if (!m_initial)
				std::memcpy(m_data, m_curr.data.get(), m_size);
			else
				m_initial = false;
			
			return true;
		}
		
		bool AValuesChanged::undo(app::App &, Editor &)
		{
			std::memcpy(m_data, m_prev.data.get(), m_size);
			return true;
		}
		
		void AValuesChanged::clean(app::App &, Editor &) {}
	}
}

#endif
