/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/actions/AEntity.hpp>
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/edt/UserdataEntity.hpp>
#include <karhu/app/App.hpp>
#include <karhu/app/ecs/World.hpp>

namespace karhu
{
	namespace edt
	{
		AEntity::AEntity
		(
			app::App               &app,
			Editor                 &editor,
			TypeActionEntity const  type,
			ecs::IDEntity    const  entity
		)
		:
		IActionWithMove{app, editor},
		m_type         {type},
		m_entity       {entity}
		{
		}
		
		char const *AEntity::name() const
		{
			switch (m_type)
			{
				case TypeActionEntity::create:
					return "create entity";
					
				case TypeActionEntity::destroy:
					return "destroy entity";
					
				case TypeActionEntity::duplicate:
					return "duplicate entity";
			}
		}
		
		bool AEntity::valid(app::App &, Editor &) const
		{
			return true;
		}
		
		bool AEntity::apply(app::App &, Editor &editor)
		{
			switch (m_type)
			{
				case TypeActionEntity::create:
					return editor.createEntity(m_entity);
					
				case TypeActionEntity::destroy:
					return editor.destroyEntity(m_entity);
					
				case TypeActionEntity::duplicate:
					return editor.duplicateEntity(m_entity, m_entityDuplicated);
			}
		}
		
		bool AEntity::undo(app::App &, Editor &editor)
		{
			switch (m_type)
			{
				case TypeActionEntity::create:
					return editor.destroyEntity(m_entity);
					
				case TypeActionEntity::destroy:
					return editor.createEntity(m_entity);
					
				case TypeActionEntity::duplicate:
					return editor.destroyEntity(m_entityDuplicated);
			}
		}
		
		void AEntity::clean(app::App &app, Editor &)
		{
			if (0 == m_entity)
				return;
			
			// Safe to destroy the entity for real at this point.
			if (TypeActionEntity::create == m_type)
				app.ecs().destroyEntity(m_entity);
		}
	}
}

#endif
