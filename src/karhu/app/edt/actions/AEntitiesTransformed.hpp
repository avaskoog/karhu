/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDT_ACTIONS_AENTITIESTRANSFORMED_H_
	#define KARHU_APP_EDT_ACTIONS_AENTITIESTRANSFORMED_H_

	#include <karhu/app/edt/actions/IAction.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/maths.hpp>

	#include <vector>

	namespace karhu
	{
		namespace edt
		{
			class AEntitiesTransformed : public IActionWithMove<AEntitiesTransformed>
			{
				public:
					AEntitiesTransformed
					(
						app::App                         &,
						Editor                           &,
						Transformation             const,
						std::vector<ecs::IDEntity>       &&,
						std::vector<mth::Vec4f>          &&prev,
						std::vector<mth::Vec4f>          &&curr
					);
				
				public:
					char const *name() const override;
					bool valid(app::App &, Editor &) const override;
					bool apply(app::App &, Editor &) override;
					bool undo(app::App &, Editor &) override;
					void clean(app::App &, Editor &) override;
				
				private:
					bool perform(app::App &, std::vector<mth::Vec4f> const &);
				
				private:
					Transformation             m_transformation;
					std::vector<ecs::IDEntity> m_entities;
					std::vector<mth::Vec4f>    m_curr, m_prev;
			};
		}
	}
#endif

#endif
