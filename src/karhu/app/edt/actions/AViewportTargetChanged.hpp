/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDT_ACTIONS_AVIEWPORTTARGETCHANGED_H_
	#define KARHU_APP_EDT_ACTIONS_AVIEWPORTTARGETCHANGED_H_

	#include <karhu/app/edt/actions/IAction.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
		namespace edt
		{
			class AViewportTargetChanged : public IActionWithMove<AViewportTargetChanged>
			{
				public:
					AViewportTargetChanged
					(
							  app::App   &,
							  Editor     &,
						const mth::Vec3f &curr
					);
				
				public:
					char const *name() const override;
					bool valid(app::App &, Editor &) const override;
					bool apply(app::App &, Editor &) override;
					bool undo(app::App &, Editor &) override;
					void clean(app::App &, Editor &) override;
				
				private:
					mth::Vec3f m_curr, m_prev;
			};
		}
	}
#endif

#endif
