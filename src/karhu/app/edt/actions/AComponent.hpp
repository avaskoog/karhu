/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDT_ACTIONS_ACOMPONENT_H_
	#define KARHU_APP_EDT_ACTIONS_ACOMPONENT_H_

	#include <karhu/app/edt/actions/IAction.hpp>
	#include <karhu/app/edt/actions/common.hpp>
	#include <karhu/app/ecs/common.hpp>

	namespace karhu
	{
		namespace edt
		{			
			class AComponent : public IActionWithMove<AComponent>
			{
				public:
					AComponent
					(
						app::App                   &,
						Editor                     &,
						TypeActionComponent  const,
						ecs::IDTypeComponent const, /// @todo: Make component action type optional since not needed for destruction, can be inout in creation like ID?
						ecs::IDEntity        const,
						ecs::IDComponent     const = 0
					);
				
				public:
					char const *name() const override;
					bool valid(app::App &, Editor &) const override;
					bool apply(app::App &, Editor &) override;
					bool undo(app::App &, Editor &) override;
					void clean(app::App &, Editor &) override;
				
				private:
					TypeActionComponent       m_type;
					ecs::IDTypeComponent m_typeComponent;
					ecs::IDEntity        m_entity;
					ecs::IDComponent     m_component;
			};
		}
	}
#endif

#endif
