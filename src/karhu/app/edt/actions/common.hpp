/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDT_ACTIONS_COMMON_H_
	#define KARHU_APP_EDT_ACTIONS_COMMON_H_

	#include <cstdint>

	namespace karhu
	{
		namespace edt
		{
			enum class TypeActionEntity : std::uint8_t
			{
				create,
				destroy,
				duplicate
			};
			
			enum class TypeActionComponent : std::uint8_t
			{
				create,
				destroy
			};
		}
	}
#endif

#endif
