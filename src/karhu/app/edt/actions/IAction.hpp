/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDT_ACTIONS_IACTION_H_
	#define KARHU_APP_EDT_ACTIONS_IACTION_H_

	#include <memory>

	namespace karhu
	{
		namespace edt
		{
			class IAction
			{
				public:
					// Dummies to ensure that these are always passed, for future-proofing.
					IAction(app::App &, Editor &) {}
				
				public:
					virtual char const *name() const = 0;
					virtual std::unique_ptr<IAction> move() = 0;
					virtual bool valid(app::App &, Editor &) const = 0;
					virtual bool apply(app::App &, Editor &) = 0;
					virtual bool undo(app::App &, Editor &) = 0;
					virtual void clean(app::App &, Editor &) = 0;
				
				public:
					virtual ~IAction() = default;
				
				public:
					Identifier caller{callerless};
			};
			
			template<typename T>
			class IActionWithMove : public IAction
			{
				public:
					using IAction::IAction;
				
				public:
					virtual std::unique_ptr<IAction> move() override
					{
						return std::make_unique<T>(std::move(*static_cast<T *>(this)));
					}
			};
		}
	}
#endif

#endif
