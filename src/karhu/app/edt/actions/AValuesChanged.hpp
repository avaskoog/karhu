/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDT_ACTIONS_AVALUESCHANGED_H_
	#define KARHU_APP_EDT_ACTIONS_AVALUESCHANGED_H_

	#include <karhu/app/edt/actions/IAction.hpp>
	#include <karhu/core/Buffer.hpp>
	#include <karhu/core/Byte.hpp>

	#include <string>

	namespace karhu
	{
		namespace edt
		{
			/**
			 * @class AValuesChanged
			 *
			 * Use with caution.
			 *
			 * Used to runtime serialise changes in an object straight to bytes
			 * so that it can be quickly and easily compared and restored with
			 * a memory copy; should not be problematic in this specific use case.
			 *
			 * The action is unaware of the underlying type and needs the size
			 * specified, so make sure you use sizeof() on the exact type.
			 *
			 * The pointer is stored for restoration, so make sure that the data
			 * cannot move to a different memory location between applying and
			 * undoing the action; it is therefore best restricted to internal
			 * use where this can be guaranteed, such as serialising ECS components,
			 * which are guaranteed a persistent place in a memory pool, and are
			 * not actually freed but merely hidden when "deleted" in the editor.
			 */
			class AValuesChanged : public IActionWithMove<AValuesChanged>
			{
				public:
					AValuesChanged
					(
						app::App     &,
						Editor       &,
						void         * const data,
						std::size_t    const typesize,
						Buffer<Byte> &&      prev,
						Buffer<Byte> &&      curr,
						char const   * const label = "values"
					);
				
				public:
					char const *name() const override;
					bool valid(app::App &, Editor &) const override;
					bool apply(app::App &, Editor &) override;
					bool undo(app::App &, Editor &) override;
					void clean(app::App &, Editor &) override;
				
				private:
					bool               m_initial{true};
					std::string  const m_name;
					void        *const m_data;
					std::size_t  const m_size;
					Buffer<Byte>       m_prev, m_curr;
			};
		}
	}
#endif

#endif
