/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDT_ACTIONS_AENTITIESSELECTED_H_
	#define KARHU_APP_EDT_ACTIONS_AENTITIESSELECTED_H_

	#include <karhu/app/edt/actions/IAction.hpp>
	#include <karhu/app/ecs/common.hpp>

	#include <vector>

	namespace karhu
	{
		namespace edt
		{
			class AEntitiesSelected : public IActionWithMove<AEntitiesSelected>
			{
				public:
					AEntitiesSelected
					(
						app::App            &,
						Editor              &,
						std::size_t   const &count,
						ecs::IDEntity const *entities,
						bool          const  set,
						bool          const  solo
					);
				
				public:
					char const *name() const override;
					bool valid(app::App &, Editor &) const override;
					bool apply(app::App &, Editor &) override;
					bool undo(app::App &, Editor &) override;
					void clean(app::App &, Editor &) override;
				
				private:
					std::vector<ecs::IDEntity> m_curr, m_prev;
			};
		}
	}
#endif

#endif
