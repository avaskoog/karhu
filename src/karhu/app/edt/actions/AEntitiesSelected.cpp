/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/actions/AEntitiesSelected.hpp>
#include <karhu/app/edt/Editor.hpp>
#include <karhu/core/debugging.hpp>

namespace karhu
{
	namespace edt
	{
		AEntitiesSelected::AEntitiesSelected
		(
			app::App            &app,
			Editor              &editor,
			std::size_t   const &count,
			ecs::IDEntity const *entities,
			bool          const  set,
			bool          const  solo
		)
		:
		IActionWithMove{app, editor}
		{
			m_prev = editor.entitiesSelected();
			
			karhuASSERT(entities || 0 == count);
			
			if (solo)
			{
				if (set)
				{
					m_curr.resize(count);
					
					std::memcpy
					(
						m_curr.data(),
						entities,
						count * sizeof(ecs::IDEntity)
					);
				}
			}
			else
			{
				if (set)
				{
					m_curr.resize(editor.entitiesSelected().size());
					
					std::memcpy
					(
						m_curr.data(),
						editor.entitiesSelected().data(),
						count * sizeof(ecs::IDEntity)
					);
					
					for (std::size_t i{0}; i < count; ++ i, ++ entities)
						if (std::find(m_curr.begin(), m_curr.end(), *entities) == m_curr.end())
							m_curr.emplace_back(*entities);
				}
				else
				{
					for (ecs::IDEntity const entity : editor.entitiesSelected())
					{
						bool found{false};
						
						for (std::size_t i{0}; i < count && !found; ++ i)
							if (*(entities + i) == entity)
								found = true;
						
						if (!found)
							m_curr.emplace_back(entity);
					}
				}
			}
		}
		
		char const *AEntitiesSelected::name() const
		{
			return "change entity selection";
		}
		
		bool AEntitiesSelected::valid(app::App &, Editor &) const
		{
			if (m_curr.size() != m_prev.size())
				return true;
			
			std::vector<ecs::IDEntity>
				curr(m_curr.size()),
				prev(m_prev.size());
			
			std::partial_sort_copy
			(
				m_curr.begin(),
				m_curr.end(),
				curr.begin(),
				curr.end()
			);
			
			std::partial_sort_copy
			(
				m_prev.begin(),
				m_prev.end(),
				prev.begin(),
				prev.end()
			);
			
			return (curr != prev);
		}
		
		bool AEntitiesSelected::apply(app::App &, Editor &editor)
		{
			editor.entitiesSelected
			(
				m_curr.size(),
				m_curr.data(),
				caller
			);
			
			return true;
		}
		
		bool AEntitiesSelected::undo(app::App &, Editor &editor)
		{
			editor.entitiesSelected
			(
				m_prev.size(),
				m_prev.data(),
				caller
			);
			
			return true;
		}
		
		void AEntitiesSelected::clean(app::App &, Editor &) {}
	}
}

#endif
