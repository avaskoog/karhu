/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/actions/AComponent.hpp>
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/edt/UserdataEntity.hpp>
#include <karhu/app/App.hpp>
#include <karhu/app/ecs/World.hpp>

namespace karhu
{
	namespace edt
	{
		AComponent::AComponent
		(
			app::App                   &app,
			Editor                     &editor,
			TypeActionComponent  const  type,
			ecs::IDTypeComponent const  typeComponent,
			ecs::IDEntity        const  entity,
			ecs::IDComponent     const  component
		)
		:
		IActionWithMove{app, editor},
		m_type         {type},
		m_typeComponent{typeComponent},
		m_entity       {entity},
		m_component    {component}
		{
		}
		
		char const *AComponent::name() const
		{
			switch (m_type)
			{
				case TypeActionComponent::create:
					return "create component";
					
				case TypeActionComponent::destroy:
					return "destroy component";
			}
		}
		
		bool AComponent::valid(app::App &, Editor &) const
		{
			return true;
		}
		
		bool AComponent::apply(app::App &, Editor &editor)
		{
			switch (m_type)
			{
				case TypeActionComponent::create:
					return editor.createComponent(m_typeComponent, m_entity, m_component);
					
				case TypeActionComponent::destroy:
					return editor.destroyComponent(m_entity, m_component);
			}
		}
		
		bool AComponent::undo(app::App &, Editor &editor)
		{
			switch (m_type)
			{
				case TypeActionComponent::create:
					return editor.destroyComponent(m_entity, m_component);
					
				case TypeActionComponent::destroy:
					return editor.createComponent(m_typeComponent, m_entity, m_component);
			}
		}
		
		void AComponent::clean(app::App &app, Editor &)
		{
			if (0 == m_component)
				return;
			
			// Safe to destroy the component for real at this point.
			if (TypeActionComponent::create == m_type)
				app.ecs().destroyComponentInEntity(m_component, m_entity);
		}
	}
}

#endif
