/**
 * @author		Ava Skoog
 * @date		2020-08-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/actions/AEntitiesTransformed.hpp>
#include <karhu/app/edt/Editor.hpp>
#include <karhu/core/debugging.hpp>

namespace karhu
{
	namespace edt
	{
		AEntitiesTransformed::AEntitiesTransformed
		(
			app::App                         & app,
			Editor                           & editor,
			Transformation             const   transformation,
			std::vector<ecs::IDEntity>       &&entities,
			std::vector<mth::Vec4f>          &&curr,
			std::vector<mth::Vec4f>          &&prev
		)
		:
		IActionWithMove {app, editor},
		m_transformation{transformation},
		m_entities      {std::move(entities)},
		m_curr          {std::move(curr)},
		m_prev          (std::move(prev))
		{
			karhuASSERT(m_curr.size() == m_prev.size());
			karhuASSERT(m_entities.size() == m_curr.size());
			
			for (std::size_t i{0}; i < m_entities.size(); ++ i)
			{
				if (m_curr[i] == m_prev[i])
				{
					auto const offset{static_cast<std::ptrdiff_t>(i)};
					
					m_curr.    erase(m_curr.begin()     + offset);
					m_prev.    erase(m_prev.begin()     + offset);
					m_entities.erase(m_entities.begin() + offset);
					
					-- i;
				}
			}
		}
		
		char const *AEntitiesTransformed::name() const
		{
			return "transform entity";
		}
		
		bool AEntitiesTransformed::valid(app::App &, Editor &) const
		{
			return !m_entities.empty();
		}
		
		bool AEntitiesTransformed::apply(app::App &, Editor &editor)
		{
			editor.entitiesTransformation
			(
				m_transformation,
				m_entities.size(),
				m_entities.data(),
				m_curr.data(),
				true,
				caller
			);
			
			return  true;
		}
		
		bool AEntitiesTransformed::undo(app::App &, Editor &editor)
		{
			editor.entitiesTransformation
			(
				m_transformation,
				m_entities.size(),
				m_entities.data(),
				m_prev.data(),
				true,
				caller
			);
			
			return true;
		}
		
		void AEntitiesTransformed::clean(app::App &, Editor &) {}
	}
}

#endif
