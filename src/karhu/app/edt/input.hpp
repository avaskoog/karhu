/**
 * @author		Ava Skoog
 * @date		2020-08-03
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDT_INPUT_H_
	#define KARHU_APP_EDT_INPUT_H_

	#include <karhu/conv/maths.hpp>
	#include <karhu/core/macros.hpp>

	#include <string>
	#include <cstdint>

	namespace karhu
	{
		namespace edt
		{
			// ImGuizmo

			enum class ModeGizmo
			{
				move,
				rotate,
				scale
			};

			struct Gizmo
			{
				ModeGizmo mode{ModeGizmo::move};
				mth::Vec3f start, current, delta, snapPerAxis{};
				bool lockX{false}, lockY{false}, lockZ{false};
				
				void reset(mth::Vec3f const &);
			
				private:
					bool       m_using{false};
					mth::Mat4f m_matrix{1};
				
				friend class Editor;
			};

			// ImGui

			enum class FlagsInput
			{
				none,
				password = 1 << 0
			};

			karhuBITFLAGGIFY(FlagsInput)

			template<typename T>
			struct Input
			{
				T    buffer {};
				bool tracker{false};
			};

			using InputString = Input<std::string>;
			using InputFloat  = Input<float>;
			using InputInt    = Input<std::int32_t>;
		}
	}
#endif

#endif
