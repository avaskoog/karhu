/**
 * @author		Ava Skoog
 * @date		2019-11-23
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#ifndef KARHU_APP_EDITOR_SERVER_H_
	#define KARHU_APP_EDITOR_SERVER_H_

	#include <karhu/tool/network/ServerDebug.hpp>

	namespace karhu
	{
		namespace app
		{
			class App;
		}
		
		namespace edt
		{	
			class Editor;
			
			enum class EventProtocol : std::int32_t
			{
				resourceRefreshed,
				pipelineStepActivated,
				pipelineSizeShadowmapMin,
				pipelineSizeShadowmapMax,
				pipelineScaleShadowmap,
				pipelineMarginShadowmap,
				pipelineCutoffShadowmap,
				pipelineBiasShadowmap
			};
		
			class Server : public tool::network::ServerDebug
			{
				public:
					Server(app::App &, Editor &, tool::proj::file::Project &);
				
				protected:
					void performLog
					(
						const log::Level,
						const conv::String &tag,
						const conv::String &text
					) override;
				
				private:
					app::App &m_app;
					Editor   &m_editor;
			};
		}
	}
#endif

#endif
