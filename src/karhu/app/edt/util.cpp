/**
 * @author		Ava Skoog
 * @date		2020-08-03
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/App.hpp>
#include <karhu/app/edt/util.hpp>
#include <karhu/core/log.hpp>
#include <karhu/app/edt/dropdown.inl>
#include <karhu/app/karhuimgui.hpp>

#include <memory>
#include <cstring> // memcpy

/// @see: https://github.com/ocornut/imgui/issues/1496#issuecomment-655048353

namespace karhu
{
	namespace edt
	{
		float beginGroupPanel()
		{
			ImVec2 const
				&padding{ImGui::GetStyle().WindowPadding};
			
			ImGui::SetCursorPos(ImGui::GetCursorPos() + padding);
			
			ImGui::BeginGroup();
			
			float const
				width
				{
					ImGui::CalcItemWidth() -
					padding.x * 1.0f
				};
			
			ImGui::PushItemWidth(width);
			
			return ImGui::GetContentRegionAvailWidth() - padding.x * 2.0f;
		}

		void endGroupPanel()
		{
			ImVec2 const
				&padding{ImGui::GetStyle().WindowPadding};
			
			ImGui::PopItemWidth();
			ImGui::EndGroup();
			
			ImColor
				col{ImGui::GetStyleColorVec4(ImGuiCol_Text)};
			
			ImVec2
				min{ImGui::GetItemRectMin()},
				max{ImGui::GetItemRectMax()};
			
			max.x = min.x + ImGui::GetContentRegionAvailWidth() - ImGui::GetStyle().IndentSpacing;
			min -= padding;
			max += padding;
			
			ImGui::GetWindowDrawList()->AddRect
			(
				min,
				max,
				col,
				ImGui::GetStyle().ChildRounding,
				ImDrawCornerFlags_All,
				1.0f
			);
			
			ImGui::SetCursorPos(ImGui::GetCursorPos() + ImVec2{0.0f, padding.y});
		}
	
		mth::Vec2f sizeWithinBounds
		(
			float const w,
			float const h,
			float const wb,
			float const hb
		)
		{
			mth::Vec2f
				size{wb, hb};
			
			float const
				height{size.x * h / w};

			if (height > size.y)
			{
				size.x *= mth::min(height, size.y) / mth::max(height, size.y);
				size.y  = mth::min(height, size.y);
			}
			else
				size.y = height;
			
			return size;
		}
		
		void serialiseToBytesRaw(void const *const source, std::size_t const size, Buffer<Byte> &target)
		{
			target.data = std::make_unique<Byte[]>(size);
			target.size = size;
		
			std::memcpy
			(
				target.data.get(),
				source,
				size
			);
		}
		
		void deserialiseFromBytesRaw(void *const destination, std::size_t const size, Buffer<Byte> const &source)
		{
			karhuASSERT((size == source.size));
			karhuASSERT(source.data);
			karhuASSERT((0 != source.size));
			
			std::memcpy
			(
				destination,
				source.data.get(),
				size
			);
		}
		
		bool compareSerialisationsFromBytes(Buffer<Byte> const &a, Buffer<Byte> const &b)
		{
			if ((!a.data && b.data) || (a.data && !b.data))
				return false;
			
			if (a.size != b.size)
				return false;
			
			if (0 == a.size)
				return true;
			
			for (std::size_t i{0}; i < a.size; ++ i)
				if (*(a.data.get() + i) != *(b.data.get() + i))
					return false;
			
			return true;
		}
		
		/// @todo: Graphics inputs could have ranges?
		/// @todo: Add/remove material inputs in editor, can't just serialise since there is dynamic vector/map allocation involved.
		bool editInputsGraphics(edt::Editor &editor, app::App &app, gfx::ContainerInputs &inputs)
		{
			using namespace karhu::gfx;
			
			bool r{false};
			
			static std::string ID;
			
			float const
				width{edt::beginGroupPanel()};
			
			std::size_t i{0};
			for (auto &it : inputs.values())
			{
				ID = "__";
				ID += it.first;
				
				ImGui::PushID(ID.c_str());
				
				if (editor.input("_gfx-inp-name", "Name", *const_cast<std::string *>(&it.first)))
					r = true;
				
				constexpr std::pair<char const *, TypeInput>
					types[]
					{
						{"Boolean",    TypeInput::boolean},
						{"Integer",    TypeInput::integer},
						{"Float",      TypeInput::floating},
						{"Vector2i",   TypeInput::vector2i},
						{"Vector3i",   TypeInput::vector3i},
						{"Vector4i",   TypeInput::vector4i},
						{"Vector2f",   TypeInput::vector2f},
						{"Vector3f",   TypeInput::vector3f},
						{"Vector4f",   TypeInput::vector4f},
						{"Matrix2",    TypeInput::matrix2},
						{"Matrix3",    TypeInput::matrix3},
						{"Matrix4",    TypeInput::matrix4},
						{"Integer[]",  TypeInput::integerv},
						{"Float[]",    TypeInput::floatingv},
						{"Vector2i[]", TypeInput::vector2iv},
						{"Vector3i[]", TypeInput::vector3iv},
						{"Vector4i[]", TypeInput::vector4iv},
						{"Vector2f[]", TypeInput::vector2fv},
						{"Vector3f[]", TypeInput::vector3fv},
						{"Vector4f[]", TypeInput::vector4fv},
						{"Matrix2[]",  TypeInput::matrix2v},
						{"Matrix3[]",  TypeInput::matrix3v},
						{"Matrix4[]",  TypeInput::matrix4v},
						{"Texture",    TypeInput::texture}
					};
				
				if (dropdown("Type", it.second.type, std::begin(types), std::end(types)))
				{
					gfx::Input input;
					
					switch ((input.type = it.second.type))
					{
						case TypeInput::integerv:
						case TypeInput::floatingv:
						case TypeInput::vector2iv:
						case TypeInput::vector3iv:
						case TypeInput::vector4iv:
						case TypeInput::vector2fv:
						case TypeInput::vector3fv:
						case TypeInput::vector4fv:
						case TypeInput::matrix2v:
						case TypeInput::matrix3v:
						case TypeInput::matrix4v:
						{
							input.integerv.data  = nullptr;
							input.integerv.count = 0;
							break;
						}
						
						case TypeInput::texture:
						{
							input.texture.type = gfx::Input::Texref::Type::ref;
							input.texture.ref  = 0;
							break;
						}
						
						default:
						{
							std::memset(&input.matrix4, 0, sizeof(input.matrix4));
							break;
						}
					}
					
					r = true;
				}
				
				switch (it.second.type)
				{
					case TypeInput::boolean:
					{
						if (ImGui::Checkbox("Value", &it.second.boolean))
							r = true;
						
						break;
					}
					
					case TypeInput::integer:
					{
						ImGui::DragInt("Value", &it.second.integer);
						
						if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
							r = true;
						
						break;
					}
					
					case TypeInput::floating:
					{
						ImGui::DragFloat("Value", &it.second.floating);
						
						if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
							r = true;
						
						break;
					}
					
					case TypeInput::vector2i:
					{
						ImGui::DragInt2("Value", &it.second.vector2i.x);
						
						if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
							r = true;
						
						break;
					}
					
					case TypeInput::vector3i:
					{
						ImGui::DragInt3("Value", &it.second.vector3i.x);
						
						if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
							r = true;
						
						break;
					}
						
					case TypeInput::vector4i:
					{
						ImGui::DragInt4("Value", &it.second.vector4i.x);
						
						if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
							r = true;
						
						break;
					}
						
					case TypeInput::vector2f:
					{
						ImGui::DragFloat2("Value", &it.second.vector2f.x);
						
						if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
							r = true;
						
						break;
					}
						
					case TypeInput::vector3f:
					{
						ImGui::DragFloat3("Value", &it.second.vector3f.x);
						
						if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
							r = true;
						
						break;
					}
						
					case TypeInput::vector4f:
					{
						ImGui::DragFloat4("Value", &it.second.vector4f.x);
						
						if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
							r = true;
						
						break;
					}
						
					case TypeInput::matrix2:
						break;
						
					case TypeInput::matrix3:
						break;
						
					case TypeInput::matrix4:
						break;
						
					case TypeInput::integerv:
						break;
						
					case TypeInput::floatingv:
						break;
						
					case TypeInput::vector2iv:
						break;
						
					case TypeInput::vector3iv:
						break;
						
					case TypeInput::vector4iv:
						break;
						
					case TypeInput::vector2fv:
						break;
						
					case TypeInput::vector3fv:
						break;
						
					case TypeInput::vector4fv:
						break;
						
					case TypeInput::matrix2v:
						break;
						
					case TypeInput::matrix3v:
						break;
						
					case TypeInput::matrix4v:
						break;
					
					case TypeInput::texture:
					{
						if (editor.pickerResource("_gfx-inp-tex", "Value", it.second.texture.ref, app.res().IDOfType<res::Texture>()))
						{
							it.second.texture.type = gfx::Input::Texref::Type::ref;
							r = true;
						}
						
						break;
					}
					
					case TypeInput::invalid:
						break;
				}
				
				if ((++ i) != inputs.values().size())
					edt::lineGroupPanel(width);
				
				ImGui::PopID();
			}
			
			edt::endGroupPanel();
			
			return r;
		}
		
		void lineGroupPanel(float const widthGroup)
		{
			ImVec2 const
				cursor{ImGui::GetCursorScreenPos()};
			
			ImGui::GetWindowDrawList()->AddLine
			(
				cursor,
				cursor + ImVec2{widthGroup, 0.0f},
				ImGui::GetColorU32(ImGuiCol_Separator),
				1.0f
			);
			
			ImGui::Spacing();
		}
	}
}

#endif
