/**
 * @author		Ava Skoog
 * @date		2020-08-02
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/resources.hpp>
#include <karhu/app/App.hpp>
#include <karhu/res/Bank.hpp>
#include <karhu/res/Rig.hpp>
#include <karhu/res/Animation.hpp>
#include <karhu/res/Data.hpp>
#include <karhu/res/Animationcontroller.hpp>
#include <karhu/res/Animationclips.hpp>
#include <karhu/res/Font.hpp>
#include <karhu/app/nav/resources/Navmesh.hpp>

#include <karhu/tool/lib/IconFontCppHeaders/IconsForkAwesome.h>

#include <karhu/app/karhuimgui.hpp>

namespace
{
	using namespace karhu;
	using namespace karhu::edt;
	
	static ImVec2 s_positionPickerResources;
	static res::IDResource *s_resourcePickerResources{nullptr};
	constexpr char const *s_identifierPickerResources{"_res-picker"};
	
	static void rebuildLookupEntriesResources(DataResources &data, EntryResource &entry)
	{
		for (EntryResource &child : entry.children)
		{
			data.lookup.emplace(child.index, &child);
			rebuildLookupEntriesResources(data, child);
		}
	}
	
	static int orderSortForTypeResourceByID
	(
		app::App                  &app,
		res::IDTypeResource const  type
	)
	{
		// Data and code.
		
		constexpr int const valueData{0};
		
		if (type == app.res().IDOfType<res::Data>())
			return valueData + 0;
		
		if (type == app.res().IDOfType<res::Scene>())
			return valueData + 1;
		
		if (type == app.res().IDOfType<res::Inputmap>())
			return valueData + 2;
		
		if (type == app.res().IDOfType<res::Script>())
			return valueData + 3;
		
		// Graphics.
		
		constexpr int const valueGraphics{100};
		
		if (type == app.res().IDOfType<res::Shader>())
			return valueGraphics + 0;
		
		if (type == app.res().IDOfType<res::Material>())
			return valueGraphics + 1;
		
		if (type == app.res().IDOfType<res::Texture>())
			return valueGraphics + 2;
		
		if (type == app.res().IDOfType<res::Mesh>())
			return valueGraphics + 3;
		
		if (type == app.res().IDOfType<res::Rig>())
			return valueGraphics + 4;
		
		if (type == app.res().IDOfType<res::Animation>())
			return valueGraphics + 5;
		
		if (type == app.res().IDOfType<res::Animationcontroller>())
			return valueGraphics + 6;
		
		if (type == app.res().IDOfType<res::Animationclips>())
			return valueGraphics + 7;
		
		if (type == app.res().IDOfType<res::Font>())
			return valueGraphics + 8;
		
		return std::numeric_limits<int>::max();
	}
}

namespace karhu
{
	namespace edt
	{
		Result<DataResources> loadEntriesResources(app::App &app)
		{
			// Bit of a hack, but since folders do not have resource
			// identifiers, and since resources never have negative
			// identifiers, we can use the negative range to assign
			// identifiers to the folders, making sure they don't
			// clash with the negative value reserved for 'none'.
			IndexHierarchy IDFolderLast{indexHierarchyNone - 1};
			
			std::string path;
			DataResources data;
			EntryResource *entryCurr;
			
			data.root.typeEntry = TypeEntryResource::directory;
			data.root.index     = (IDFolderLast --);
		
			auto &all(app.res().database().all());
			
			for (auto const &entry : all)
			{
				// Ignore pure sources.
				if (static_cast<res::IDTypeResource>(0) == entry.second.type)
					continue;
				
				auto const parts(string::split(entry.second.path, '/'));
				
				entryCurr = &data.root;
				path.clear();
				
				for (std::size_t i{0}; i < parts.size() - 1; ++ i)
				{
					path += parts[i];
					
					if ((i + 1) < parts.size())
						path += '/';
					
					auto it(std::find_if
					(
						entryCurr->children.begin(),
						entryCurr->children.end(),
						[&parts, &i](EntryResource const &v) -> bool
						{
							return (v.name == parts[i]);
						}
					));
					
					if (it != entryCurr->children.end())
						entryCurr = &*it;
					else
					{
						entryCurr->children.emplace_back(EntryResource{});
						
						entryCurr             = &entryCurr->children.back();
						entryCurr->typeEntry  = TypeEntryResource::directory;
						entryCurr->index      = (IDFolderLast --);
						entryCurr->name       = parts[i];
						entryCurr->path       = path;
						entryCurr->label      = ICON_FK_FOLDER;
						entryCurr->label     += ' ';
						entryCurr->label     += parts[i];
					}
				}

				path += parts.back();
				
				EntryResource e;
				
				e.typeEntry     = TypeEntryResource::file;
				e.index         = static_cast<IndexHierarchy>(entry.first);
				e.typeResource  = entry.second.type;
				e.resource      = entry.first;
				e.name          = parts.back();
				e.path          = path;
				e.label         = iconForTypeResourceByID(app, e.typeResource);
				e.label        += ' ';
				e.label        += parts.back();
				
				entryCurr->children.emplace_back(std::move(e));
			}
			
			// Default sort.
			sortEntriesResources(app, ModeSortEntryResource::name, data.root);
			
			rebuildLookupEntriesResources(data);
			
			return {std::move(data), true};
		}
		
		void rebuildLookupEntriesResources(DataResources &data)
		{
			data.lookup.clear();
			::rebuildLookupEntriesResources(data, data.root);
		}
		
		void sortEntriesResources
		(
			app::App                    &app,
			ModeSortEntryResource const  mode,
			EntryResource               &root
		)
		{
			if (TypeEntryResource::directory != root.typeEntry)
				return;
			
			std::sort
			(
				root.children.begin(),
				root.children.end(),
				[&app, &mode](EntryResource const &a, EntryResource const &b) -> bool
				{
					switch (mode)
					{
						case ModeSortEntryResource::name:
							return (a.name.compare(b.name) < 0);
						
						case ModeSortEntryResource::type:
							return
							(
								// Directories before files.
								(
									static_cast<int>(a.typeEntry) <
									static_cast<int>(b.typeEntry)
								) ||
								// By resource type.
								(
									orderSortForTypeResourceByID(app, a.typeResource) <
									orderSortForTypeResourceByID(app, b.typeResource)
								)
							);
					}
					
					return false;
				}
			);
			
			for (EntryResource &child : root.children)
				if (TypeEntryResource::directory == child.typeEntry)
					sortEntriesResources(app, mode, child);
		}
		
		bool drawEntriesResources
		(
			app::App                           &app,
			Editor                             &editor,
			EntryResource                const &root,
			StateResources                     &state,
			SettingsDrawEntriesResources const &settings
		)
		{
			bool selected{false};
			
			bool const filteredByTypeEntry
			{
				!static_cast<bool>
				(
					root.typeEntry &
					settings.filterTypeEntryResource
				)
			};
			
			if (TypeEntryResource::directory != root.typeEntry)
			{
				// Filter by entry type.
				if (filteredByTypeEntry)
					return false;
				
				// Filter by resource type.
				if (!settings.filterTypeResource.empty())
				{
					auto const it(std::find
					(
						settings.filterTypeResource.begin(),
						settings.filterTypeResource.end(),
						root.typeResource
					));
					
					if (it == settings.filterTypeResource.end())
						return false;
				}
			}
			
			// Check selected status.
			auto const it(std::find
			(
				state.selected.begin(),
				state.selected.end(),
				root.index
			));
			
			selected = (it != state.selected.end());
			
			bool changed{false};
			
			if (!filteredByTypeEntry)
			{
				ImGuiTreeNodeFlags flags
				{
					ImGuiTreeNodeFlags_SpanAvailWidth |
					ImGuiTreeNodeFlags_OpenOnArrow
				};
				
				if (root.children.empty())
					flags |= ImGuiTreeNodeFlags_Leaf;
				
				if (selected)
					flags |= ImGuiTreeNodeFlags_Selected;
				
				bool const open
				(
					ImGui::TreeNodeEx
					(
						root.label.c_str(),
						flags
					)
				);
				
				if (!ImGui::IsItemToggledOpen() && ImGui::IsItemHovered())
				{
					if (ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
					{
						/// @todo: Double click should work to select a file in resource dropdowns, not sure if it should do anything in the resource subeditor, maybe open in OS file browser?
					}
					else if (ImGui::IsMouseClicked(ImGuiMouseButton_Left))
					{
						changed = true;
						
						if (!ImGui::GetIO().KeyShift)
						{
							state.selected.clear();
	 
							if (!selected)
								state.selected.emplace_back(root.index);
						}
						else
						{
							auto const it(std::find
							(
								state.selected.begin(),
								state.selected.end(),
								root.index
							));
	 
							if (it == state.selected.end())
								state.selected.emplace_back(root.index);
							else
								state.selected.erase(it);
						}
					}
				}
				
				if (open)
				{
					for (auto const &child : root.children)
						changed |= drawEntriesResources(app, editor, child, state, settings);
					
					ImGui::TreePop();
				}
			}
			else
				for (auto const &child : root.children)
					changed |= drawEntriesResources(app, editor, child, state, settings);
			
			return changed;
		}
		
		void openPickerResources
		(
			app::App        &,
			Editor          &,
			res::IDResource &data,
			StateResources  &state
		)
		{
			ImGui::OpenPopup(s_identifierPickerResources);
			s_positionPickerResources = /*ImGui::GetWindowPos() + */ImGui::GetCursorPos();// - ImVec2{ImGui::GetScrollX(), ImGui::GetScrollY()};
			s_resourcePickerResources = &data;
			state.selected.clear();
		}
		
		bool drawPickerResources
		(
			app::App                           &app,
			Editor                             &editor,
			res::IDResource                    &resource,
			DataResources                      &data,
			EntryResource                const &root,
			StateResources                     &state,
			SettingsDrawEntriesResources const &settings
		)
		{
			bool r{false};
			
			if (&resource != s_resourcePickerResources)
				return r;
			
			if (!ImGui::IsPopupOpen(s_identifierPickerResources))
				return r;
			
			ImGui::SetCursorPos(s_positionPickerResources);
			
			if (ImGui::BeginPopup(s_identifierPickerResources))
			{
				drawEntriesResources(app, editor, root, state, settings);
				
				/// @todo: Figure out select/multiselect for resource picker.
				if (!state.selected.empty())
				{
					auto const it(data.lookup.find(state.selected[0]));
					
					/// @todo: Should validate by resource type as well in case filter does not hide other resource types? Or should make those disabled and non-selectable?
					if (it != data.lookup.end() && it->second)
					{
						if (it->second->typeEntry != TypeEntryResource::directory)
						{
							*s_resourcePickerResources = it->second->resource;
							s_resourcePickerResources  = nullptr;
							r = true;
							
							ImGui::CloseCurrentPopup();
						}
					}
				}
				
				ImGui::EndPopup();
			}
			
			ImGui::SetCursorPos(s_positionPickerResources);
			
			return r;
		}
		
		char const *iconForTypeResourceByID
		(
			app::App                  &app,
			res::IDTypeResource const  type
		)
		{
			// See: https://forkaweso.me/Fork-Awesome/icons/
			
			if (type == app.res().IDOfType<res::Material>())
				return ICON_FK_PAINT_BRUSH;
			
			if (type == app.res().IDOfType<res::Shader>())
				return ICON_FK_ADJUST;
			
			if (type == app.res().IDOfType<res::Texture>())
				return ICON_FK_PICTURE_O;
			
			if (type == app.res().IDOfType<res::Mesh>())
				return ICON_FK_CUBE;
			
			if (type == app.res().IDOfType<res::Rig>())
				return ICON_FK_STREET_VIEW;
			
			if (type == app.res().IDOfType<res::Animation>())
				return ICON_FK_PLAY;
			
			if (type == app.res().IDOfType<res::Data>())
				return ICON_FK_DATABASE;
			
			if (type == app.res().IDOfType<res::Scene>())
				return ICON_FK_CUBES;
			
			if (type == app.res().IDOfType<res::Animationcontroller>())
				return ICON_FK_PLAY_CIRCLE_O;
			
			if (type == app.res().IDOfType<res::Animationclips>())
				return ICON_FK_PEERTUBE;
			
			if (type == app.res().IDOfType<res::Inputmap>())
				return ICON_FK_GAMEPAD;
			
			if (type == app.res().IDOfType<res::Font>())
				return ICON_FK_FONT;
			
			if (type == app.res().IDOfType<res::Script>())
				return ICON_FK_CODE;
			
			if (type == app.res().IDOfType<nav::Navmesh>())
				return ICON_FK_MAP;
			
			/// @todo: Uncomment these resource icons when these resource types exist.
			
//			if (type == app.res().IDOfType<res::Sound>())
//				return ICON_FK_VOLUME_UP;
			
			return ICON_FK_FILE;
		}
	}
}

#endif
