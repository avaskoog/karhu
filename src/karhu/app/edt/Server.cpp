/**
 * @author		Ava Skoog
 * @date		2019-11-23
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/Server.hpp>
#include <karhu/app/edt/Editor.hpp>

namespace karhu
{
	namespace edt
	{
		Server::Server
		(
			app::App                  &app,
			Editor                    &editor,
			tool::proj::file::Project &project
		)
		:
		tool::network::ServerDebug{project},
		m_app                     {app},
		m_editor                  {editor}
		{
		}
		
		void Server::performLog
		(
			const log::Level    level,
			const conv::String &tag,
			const conv::String &text
		)
		{
			m_editor.logger().category = Logger::Category::client;
			log::lvl(level, tag) << text;
			m_editor.logger().category = Logger::Category::server;
		}
	}
}

#endif
