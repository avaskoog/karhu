/**
 * @author		Ava Skoog
 * @date		2019-11-09
 * @copyright   2017-2023 Ava Skoog
 */

//! @todo: Slett oktregreier når ferdigtesta.
#include <karhu/app/ecs/Octree.hpp>

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED

#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/edt/Server.hpp>
#include <karhu/app/edt/UserdataEntity.hpp>

#include <karhu/app/edt/editors/EditorViewport.hpp>
#include <karhu/app/edt/editors/EditorHierarchy.hpp>
#include <karhu/app/edt/editors/EditorInspector.hpp>
#include <karhu/app/edt/editors/EditorResources.hpp>
#include <karhu/app/edt/editors/EditorConsole.hpp>
#include <karhu/app/edt/editors/EditorHistory.hpp>
#include <karhu/app/edt/editors/EditorSettingsBuild.hpp>
#include <karhu/app/edt/editors/EditorPipeline.hpp>

#include <karhu/app/edt/actions/AEntity.hpp>
#include <karhu/app/edt/actions/AComponent.hpp>
#include <karhu/app/edt/actions/AEntitiesSelected.hpp>

#include <karhu/app/gfx/components/Transform.hpp>
#include <karhu/app/gfx/utilities.hpp>

#include <karhu/app/win/SubsystemWindow.hpp>
#include <karhu/app/win/Window.hpp>

#include <karhu/res/Bank.hpp>
#include <karhu/res/Script.hpp>
#include <karhu/res/Scene.hpp>
#include <karhu/res/Texture.hpp>

#include <karhu/app/App.hpp>
#include <karhu/app/scripting.hpp>
#include <karhu/app/scriptutil.hpp>

#include <karhu/tool/proj/file/Project.hpp>
#include <karhu/tool/SDK.hpp>

#include <karhu/core/platform.hpp>
#include <karhu/core/debugging.hpp>

#include <karhu/app/karhuimgui.hpp>

#include <karhu/app/lib/imguizmo/ImGuizmo.h>
#include <karhu/app/lib/im3d/im3d.h>
#include <karhu/app/lib/im3d/im3d_math.h>

#include <karhu/tool/lib/IconFontCppHeaders/IconsForkAwesome.h>
#include <efsw/efsw.hpp>

#include <boost/process.hpp>

/// @todo: Replace filesystem if upgrading to C++17.
#include <boost/filesystem.hpp>

#include <fstream>
#include <algorithm>
#include <chrono>
#include <type_traits>

namespace
{
	using namespace karhu;
	using namespace karhu::edt;
	
	/// @todo: Can we do this a better way?
	static constexpr char const
		*const signatureEditorScriptedInit
		{
			"bool init(edt::EditorScripted &, app::App &, edt::Editor &)"
		},
		*const signatureEditorScriptedUpdateAndRender
		{
			"void updateAndRender(edt::EditorScripted &, app::App &, edt::Editor &)"
		},
		*const signatureUpdateInViewportAndRender
		{
			"void updateInViewportAndRender(edt::EditorScripted &, app::App &, edt::Editor &, const mth::Vec2f &in)"
		},
		*const signatureEditorScriptedEvent
		{
			"void event(edt::EditorScripted &, app::App &, edt::Editor &, const edt::Identifier, const edt::Event &)"
		};
	
	template<typename F, typename TBuffer, typename... Ts>
	bool performInput
	(
		F                            f,
		char       const *           ID,
		char       const *           label,
		TBuffer          &&          buffer,
		bool             *  const    tracker,
		FlagsInput          const    flags,
	    Ts               &&       ...args
	)
	{
		karhuASSERT(ID != nullptr)

		bool const last{(tracker) ? *tracker : false};
		
		ImGuiInputTextFlags flagsImGui
		{
			ImGuiInputTextFlags_NoUndoRedo       |
			ImGuiInputTextFlags_EnterReturnsTrue
		};
		
		if (static_cast<bool>(FlagsInput::password & flags))
			flagsImGui |= ImGuiInputTextFlags_Password;
		
		ImGui::PushID(ID);

		/// @todo: Make this return a boolean and check for enter and make it not lose focus after that; see EditorConsole for implementation; can probably use global state instead of local, for the ID of the last pressed input or something?
		bool const
			r
			{
				f
				(
					((label) ? label : ""),
					std::forward<TBuffer>(buffer),
					flagsImGui,
					std::forward<Ts>(args)...
				)
			};
		
		ImGui::PopID();
		
		/// @todo: Don't think we need the tracker anymore, can just use ImGui::IsItemDeactivated() instead...
		if (tracker)
		{
			*tracker = ImGui::IsItemActive();
			return (last && !*tracker);
		}
		
		return r;
	}
	
	static void selectNewEntity(app::App &app, Editor &editor)
	{
		/// @todo: This whole thing is a bit ugly, probably AEntity should just call entitiesSelected and keep track of that as well so that we don't get two history steps but.

		auto const
			action(dynamic_cast<AEntity const *>(editor.peekAction()));

		if (action)
		{
			ecs::IDEntity const
				ID
				{
					(TypeActionEntity::create == action->type())
					?
						action->entity()
					:
						action->entityDuplicated()
				};
			
			if (0 != ID)
				editor.pushAction
				(
					callerless,
					AEntitiesSelected
					{
						app,
						editor,
						1,
						&ID,
						true,
						true
					}
				);
		}
	}
	
	static void duplicateSelected(app::App &app, Editor &editor)
	{
		if (1 != editor.entitiesSelected().size())
			return;
		
		editor.pushAction
		(
			callerless,
			AEntity
			{
				app,
				editor,
				TypeActionEntity::duplicate,
				editor.entitiesSelected()[0]
			}
		);

		selectNewEntity(app, editor);
	}
	
	static void destroySelected(app::App &app, Editor &editor)
	{
		if (1 != editor.entitiesSelected().size())
			return;
		
		editor.pushAction
		(
			callerless,
			AEntity
			{
				app,
				editor,
				TypeActionEntity::destroy,
				editor.entitiesSelected()[0]
			}
		);
	}
	
	static void saveScene(app::App &app, Editor &editor)
	{
		if (res::Scene *scene = app.res().get<res::Scene>(editor.IDSceneResource()))
		{
			conv::JSON::Val root;
			
			if (app.res().serialiserECS().serialiseSceneFromWorld
			(
				*scene,
				app.ecs(),
				editor.IDSceneECS(),
				root
			))
			{
				scene->set(std::move(root));
				
				if (!app.res().save(editor.IDSceneResource()))
					log::err("Karhu") << "Failed to save scene";
				else
					log::msg("Karhu") << "Successfully saved scene";
			}
			else
				log::err("Karhu") << "Failed to serialise scene for saving";
		}
	}
	
	class Filewatcher : public efsw::FileWatchListener
	{
		public:
			bool directory(std::string const &path)
			{
				namespace fs = boost::filesystem;
				
				fs::path p{path};
				p.make_preferred();
				
				if (!fs::exists(p) || !fs::is_directory(p))
				{
					log::err("Karhu")
						<< "Failed to set directory '"
						<< p.string()
						<< "' for file watching: "
						<< "does not exist";
					
					return false;
				}
				
				try
				{
					m_filewatcher.addWatch
					(
						p.string(),
						this,
						true // Recursive.
					);
				}
				catch (FW::FileNotFoundException const &e)
				{
					log::err("Karhu")
						<< "Failed to set directory '"
						<< p.string()
						<< "' for file watching: "
						<< e.what();
					
					return false;
				}
				
				// Starts up a new thread.
				m_filewatcher.watch();
				
				return true;
			}
		
			void update
			(
				Editor            &editor,
				app::App          &app,
				std::string const &respath
			)
			{
				namespace fs    = boost::filesystem;
				namespace bp    = boost::process;
				namespace tools = SDK::paths::home::tools;
				
				static std::vector<std::string>
					srcs;
				
				auto const
					&resources(app.res().database().all());
				
				std::time_t const
					now{std::time(nullptr)};
				
				std::lock_guard<std::mutex>
					guard{m_mutex};
				
				srcs.clear();
				
				for (auto it{m_filesToRefresh.begin()}; it != m_filesToRefresh.end();)
				{
					fs::path const
						path{fs::relative(it->first, respath)};
					
					bool
						processed{false};
					
					// Avoid hidden files, current folder and parent folder.
					// Specifically avoid updates to the database or we'll get stuck in a loop
					// since reloading the database below counts as modifying the file at least
					// in macOS, perhaps due to access events also being counted by efsw.
					if ('.' == path.string()[0] || path.string() == res::Bank::subpathDB())
						processed = true;
					else
					{						
						// Give the file some time to become valid; one second is the
						// lowest precision we have here, so it will have to do.
						// This also helps against an issue with more events than
						// necessary firing at least on some operating systems.
						if ((now - it->second.time) >= 1)
						{
							switch (it->second.action)
							{
								case efsw::Actions::Add:
								case efsw::Actions::Modified:
								{
									fs::path p{respath};
									p.append(path.string());
									p.make_preferred();
									
									using namespace std::chrono_literals;
									srcs.emplace_back(path.string());
									processed = true;
									log::msg("DEBUG") << "FILE (" << it->first << ") has event " << ((efsw::Actions::Add == it->second.action) ? "Add" : "Modified");
									break;
								}
									
								case efsw::Actions::Delete:
									/// @todo: Clear database, clean up metadata etc, invalidate references when Ref<T> is back.
									log::msg("DEBUG") << "FILE (" << it->first << ") has event Delete";
									//processed = true;
									break;
								
								case efsw::Actions::Moved:
									/// @todo: Update database; resources need not be reloaded nor references updated.
									log::msg("DEBUG") << "FILE (" << it->first << ") has event Moved from (" << it->second.fileOld << ')';
									//processed = true;
									break;
							}
						}
					}
					
					if (!processed)
						++ it;
					else
						it = m_filesToRefresh.erase(it);
				}
				
				if (!srcs.empty())
				{
					log::msg("DEBUG")
						<< "SRC: "
						<< std::accumulate
						(
							srcs.begin(),
							srcs.end(),
							std::string{},
							[](std::string const &a, std::string const &b)
							{
								return a.empty() ? b : (a + ", " + b);
							}
						);
					
					app.backend().shouldRepaintViewportEditor(true);
					
					auto const
						p(SDK::env::home(tools::karhuproj::local()));
		
					if (!p || !fs::exists(*p))
					{
						log::err("Karhu") << "Failed to refresh resources: could not find karhuproj tool";
						return;
					}

					std::string
						line;
					
					std::vector<std::string>
						args,
						output;

					args.emplace_back("asset");
					
					args.emplace_back("--act");
					args.emplace_back("refresh");
					
					args.emplace_back("--dir");
					args.emplace_back(editor.project()->location().path());

					/// @todo: Pass all resources at the same time.
					args.emplace_back("--srcs");
					
					for (std::string const &src : srcs)
						args.emplace_back(src);

					bp::ipstream
						pi;
					
					bp::child
						c{*p, args, bp::std_out > pi, bp::std_err > stderr};

					while (c.running() && std::getline(pi, line) && !line.empty())
					{
						log::msg("DEBUG") << "LINE: " << line;
						output.emplace_back(line);
					}
					
					try
					{
						c.wait();
					}
					catch (bp::process_error &e)
					{
						log::err("Karhu") << "Failed to refresh resources: wait for karhuproj asset threw: " << e.what();
						return;
					}

					if (0 != c.exit_code())
					{
						log::err("Karhu") << "Failed to refresh resources: karhuproj asset failed: " << c.exit_code();
						return;
					}
					
					log::msg("DEBUG") << "LINES COUNT: " << output.size();
					
					for (std::size_t i{0}; (i + 4) < output.size(); i += 5)
					{
						auto const
							ID(static_cast<res::IDResource>(std::atoi(output[i + 2].c_str())));
						
						auto const
							type(static_cast<res::IDTypeResource>(std::atoi(output[i + 3].c_str())));
						
						auto const
							sourcesAsString(string::split(output[i + 4], ' '));
						
						std::vector<res::IDResource>
							sourcesAsID(sourcesAsString.size());
						
						for (std::size_t j{0}; j < sourcesAsString.size(); ++ j)
							sourcesAsID[j] = std::atoi(sourcesAsString[j].c_str());
						
						std::stringstream
							ss;
						
						ss
							<< "src " << i
							<< " id " << int(ID)
							<< " type " << int(type)
							<< " sources(" << sourcesAsID.size() << ")";
						
						for (res::IDResource const ID : sourcesAsID)
							ss << ' ' << ID;
						
						log::msg("DEBUG") << ss.str();
						
						if (0 != ID && 0 != type)
						{
							std::string const
								&name{output[i]};
							
							log::msg("Karhu") << "Refreshed resource " << ID << ": " << name;
							
							app.res().database().add
							(
								type,
								name,
								name,
								false, // Do not overwrite.
								sourcesAsID
							);
							
							editor.refreshResource(ID, type);
							editor.emitEvent(EResource{ID, TypeEventResource::modifiedOrAdded});
						}
					}
					
					editor.reloadResources(); /// @todo: Don't reload all resources, just the new ones.
				}
			}
		
			void handleFileAction
			(
				efsw::WatchID        ID,
				std::string   const &directory,
				std::string   const &file,
				efsw::Action         action,
				std::string          fileOld = ""
			) override
			{
				namespace fs = boost::filesystem;
				
				std::lock_guard<std::mutex> guard{m_mutex};
				
				fs::path path{directory};
				path.append(file);
				path.make_preferred();
				
				Info &info{m_filesToRefresh[path.string()]};
				
				info.action = action;
				info.time   = std::time(nullptr);
				
				if (efsw::Actions::Moved == action)
					info.fileOld = fileOld;
			}
		
		private:
			efsw::FileWatcher m_filewatcher;
		
			struct Info
			{
				efsw::Action action;
				std::time_t  time;
				std::string  fileOld;
			};
		
			std::unordered_map<std::string, Info> m_filesToRefresh;
			std::mutex m_mutex;
	};
	
	// At least on macOS this will start up a really CPU-heavy thread just
	// from construction which we don't want in the client, so we keep it a
	// pointer and don't actually allocate anything until editor initialisation.
	static std::unique_ptr<Filewatcher> s_filewatcher;
	
	static void generateScriptsymbols(Editor &editor)
	{
		namespace fs = boost::filesystem;
		namespace su = app::scriptutil;
		
		fs::path
			path{editor.project()->location().path()};

		path.append("generated");
		path.make_preferred();

		if (!fs::exists(path))
		{
			boost::system::error_code
				err;
			
			if (!fs::create_directory(path, err))
			{
				log::err("Karhu")
					<< "Failed to create directory '"
					<< path.string()
					<< "' for generated files: "
					<< err.message();
				
				return;
			}
		}
		
		path.append(SDK::paths::project::generated::scriptsymbols());
		path.make_preferred();

		std::ofstream
			fo{path.string(), std::ios::trunc};
		
		if (!fo.is_open())
		{
			log::err("Karhu")
				<< "Failed to open file '"
				<< path.string()
				<< "' for writing";
		
			return;
		}
		
		if (su::outputBoundAPIAsCPP
		(
			editor.app().scr().engine(),
			fo,
			su::FlagsOutput::includeMagic
		))
		{
			log::msg("Karhu")
				<< "Successfully wrote symbols to '"
				<< path.string()
				<< "'";
		}
	}
}

namespace karhu
{
	namespace edt
	{
		bool Logger::log
		(
			log::Level  const level,
			std::string const &tag,
			std::string const &message
		)
		{
			m_app.backend().shouldRepaintEditor(true);
			
			categories.emplace_back(category);
			levels    .emplace_back(level);
			tags      .emplace_back(tag);
			texts     .emplace_back(message);
			
			return true;
		}
		
		void Logger::clear()
		{
			categories.clear();
			levels    .clear();
			tags      .clear();
			texts     .clear();
		}
		
		EditorScripted::~EditorScripted()
		{
			if (m_object)
				m_object->Release();
		}
		
		bool EditorScripted::init(app::App &app, Editor &editor)
		{
			constexpr char const
				*error{"Failed to initialise scripted editor: "};
			
			m_valid = false;
			
			if (auto data = editor.loadDataForEditorScripted(m_pathToScript))
				m_data = std::move(*data);
			else
				return true;

			asIScriptContext &context{app.scr().borrowContext()};
			
			context.Prepare(m_data.vtable.factory);

			if (asEXECUTION_FINISHED == context.Execute())
				m_object = *reinterpret_cast<asIScriptObject **>(context.GetAddressOfReturnValue());
			
			if (m_object)
			{
				m_object->AddRef();
				
				if (m_data.vtable.init)
				{
					m_valid = AS::callr
					<
						bool,
						EditorScripted &,
						app::App       &,
						Editor         &
					>
					(
						context,
						m_object,
						*m_data.vtable.init,
						*this,
						app,
						editor
					);
				}
			}
			else
				log::err("Karhu")
					<< error
					<< '\''
					<< name()
					<< "': failed to create script object";
			
			app.scr().returnContext(context);
			
			return true;
		}
		
		void EditorScripted::updateAndRender(app::App &app, Editor &editor)
		{
			if (ImGui::BeginMenuBar())
			{
				if (ImGui::BeginMenu("Script"))
				{
					if (ImGui::MenuItem("Reload"))
						editor.queueEditorReload(name());
					
					ImGui::EndMenu();
				}
				
				ImGui::EndMenuBar();
			}
			
			if (!m_valid)
			{
				/// @todo: Unify the hard-coded editor text colour constants.
				ImGui::PushStyleColor(ImGuiCol_Text, {1.0f, 0.4f, 0.4f, 1.0f});
				ImGui::TextWrapped("Error in script! See console.");
				ImGui::PopStyleColor();
				return;
			}
			
			if (!m_data.vtable.updateAndRender)
				return;
			
			asIScriptContext &context{app.scr().borrowContext()};
			
			AS::call
			<
				EditorScripted &,
				app::App       &,
				Editor         &
			>
			(
				context,
				m_object,
				*m_data.vtable.updateAndRender,
				*this,
				app,
				editor
			);
			
			app.scr().returnContext(context);
		}
		
		void EditorScripted::updateInViewportAndRender(app::App &app, Editor &editor, mth::Vec2f const &size)
		{
			if (!m_valid)
				return;
			
			if (!m_data.vtable.updateInViewportAndRender)
				return;
			
			asIScriptContext &context{app.scr().borrowContext()};
			
			AS::call
			<
				EditorScripted       &,
				app::App             &,
				Editor               &,
				mth::Vec2f     const &
			>
			(
				context,
				m_object,
				*m_data.vtable.updateInViewportAndRender,
				*this,
				app,
				editor,
				size
			);
			
			app.scr().returnContext(context);
		}

		void EditorScripted::event
		(
			app::App         &app,
			Editor           &editor,
			Identifier const  caller,
			Event      const &e
		)
		{
			if (!m_valid)
				return;
			
			if (!m_data.vtable.event)
				return;
			
			asIScriptContext &context{app.scr().borrowContext()};
			
			AS::call
			<
				EditorScripted       &,
				app::App             &,
				Editor               &,
				Identifier     const,
				Event          const &
			>
			(
				context,
				m_object,
				*m_data.vtable.event,
				*this,
				app,
				editor,
				caller,
				e
			);
			
			app.scr().returnContext(context);
		}
		
		void Gizmo::reset(mth::Vec3f const &v)
		{
			mth::Transformf t;
			
			switch (mode)
			{
				case ModeGizmo::move:   t.position = v;             break;
				case ModeGizmo::rotate: t.rotation = mth::euler(v); break;
				case ModeGizmo::scale:  t.scale    = v;             break;
			}
			
			m_matrix = t.matrix();
			delta    = {};
			start    =
			current  = v;
		}
		
		// Generic styling
		
		void Editor::pushColour(const gfx::ColRGBAf &v)
		{
			m_stateGUI.colours.emplace(v);
		}
		
		void Editor::popColour()
		{
			m_stateGUI.colours.pop();
		}
		
		void Editor::pushFilled(const bool v)
		{
			m_stateGUI.filleds.emplace(v);
		}
		
		void Editor::popFilled()
		{
			m_stateGUI.filleds.pop();
		}
		
		void Editor::pushThickness(float const v)
		{
			m_stateGUI.thicknesses.emplace(v);
		}

		void Editor::popThickness()
		{
			m_stateGUI.thicknesses.pop();
		}
		
		// Im3d
		
		void Editor::lineInViewport(mth::Vec3f const &a, mth::Vec3f const &b)
		{
			Im3d::PushDrawState();
			
			auto const &
				c{m_stateGUI.colour()};
			
			Im3d::Vec3 const
				s{a.x, a.y, a.z},
				e{b.x, b.y, b.z};
			
			Im3d::DrawLine(s, e, m_stateGUI.thickness(), {c.r, c.g, c.b, c.a});
			
			Im3d::PopDrawState();
		}
		
		void Editor::planeInViewport(mth::Vec3f const &origin, mth::Vec3f const &normal, mth::Vec2f const &size)
		{
			Im3d::PushDrawState();
			
			auto const &c{m_stateGUI.colour()};
			Im3d::SetColor({c.r, c.g, c.b, c.a});
			
			Im3d::Vec3 const
				o{origin.x, origin.y, origin.z},
				n{normal.x, normal.y, normal.z};
			
			Im3d::Vec2 const
				s{size.y * 0.5f, size.x * 0.5f};
			
			Im3d::SetSize(m_stateGUI.thickness());
			
			if (m_stateGUI.filled())
				Im3d::DrawQuadFilled(o, n, s);
			else
				Im3d::DrawQuad(o, n, s);
			
			Im3d::PopDrawState();
		}
		
		void Editor::sphereInViewport(mth::Vec3f const &position, float const diameter)
		{
			Im3d::PushDrawState();
			
			auto const &c{m_stateGUI.colour()};
			Im3d::SetColor({c.r, c.g, c.b, c.a});
			
			Im3d::Vec3 const p{position.x, position.y, position.z};
			float      const r{diameter * 0.5f};
			
			Im3d::SetSize(m_stateGUI.thickness());
			
			if (m_stateGUI.filled())
				Im3d::DrawSphereFilled(p, r);
			else
				Im3d::DrawSphere(p, r);
			
			Im3d::PopDrawState();
		}
		
		void Editor::boxInViewport(mth::Vec3f const &min, mth::Vec3f const &max)
		{
			Im3d::PushDrawState();
			
			auto const &c{m_stateGUI.colour()};
			Im3d::SetColor({c.r, c.g, c.b, c.a});
			
			Im3d::Vec3 const
				a{min.x, min.y, min.z},
				b{max.x, max.y, max.z};
			
			Im3d::SetSize(m_stateGUI.thickness());
			
			if (m_stateGUI.filled())
				Im3d::DrawAlignedBoxFilled(a, b);
			else
				Im3d::DrawAlignedBox(a, b);
			
			Im3d::PopDrawState();
		}
		
		void Editor::cylinderInViewport
		(
			mth::Vec3f const &start,
			mth::Vec3f const &end,
			float      const  diameter
		)
		{
			Im3d::PushDrawState();
			
			auto const &c{m_stateGUI.colour()};
			Im3d::SetColor({c.r, c.g, c.b, c.a});
			
			Im3d::Vec3 const
				a{start.x, start.y, start.z},
				b{end.x,   end.y,   end.z};
			
			float const r{diameter * 0.5f};
			
			Im3d::SetSize(m_stateGUI.thickness());
			
			/// @todo: Figure out filled cylinders with Im3D.
			/*
			if (m_stateGUI.filled())
				;
			else
			*/
				Im3d::DrawCylinder(a, b, r);
			
			Im3d::PopDrawState();
		}
		
		// ImGuizmo
		
		bool Editor::gizmoInViewport(Gizmo &gizmo)
		{
			/// @todo: Make sure gizmo only gets used in the viewport callback?
			
			if (m_gizmo)
				return false;
			
			m_gizmo = true;
			
			auto const camera
			(
				m_app->ecs().componentInEntity<gfx::Camera>
				(
					m_camera.camera,
					m_camera.entity
				)
			);
			
			if (!camera)
				return ((gizmo.m_using = false));
	
			auto const transformCamera
			(
				m_app->ecs().componentInEntity<gfx::Transform>
				(
					m_camera.transform,
					m_camera.entity
				)
			);
			
			if (!transformCamera)
				return ((gizmo.m_using = false));
	
			mth::Mat4f const
				 V{camera->matrixView(*transformCamera)},
				&P{camera->matrixProjection()};
			
			ImGuizmo::OPERATION operation;
			
			switch (gizmo.mode)
			{
				case ModeGizmo::move:
					operation = ImGuizmo::TRANSLATE;
					break;
					
				case ModeGizmo::rotate:
					operation = ImGuizmo::ROTATE;
					break;
					
				case ModeGizmo::scale:
					operation = ImGuizmo::SCALE;
					break;
			}
			
			ImGuizmo::Manipulate
			(
				&V[0][0],
				&P[0][0],
				operation,
				ImGuizmo::WORLD,
				&gizmo.m_matrix[0][0],
				nullptr,
				&gizmo.snapPerAxis.x
			);
			
			if (ImGuizmo::IsUsing())
			{
				switch (gizmo.mode)
				{
					case ModeGizmo::move:
						gizmo.current = mth::position(gizmo.m_matrix);
						break;
						
					case ModeGizmo::rotate:
						gizmo.current = mth::euler(mth::inverse(mth::rotation(gizmo.m_matrix)));
						break;
						
					case ModeGizmo::scale:
						gizmo.current = mth::scale(gizmo.m_matrix);
						break;
				}
			}
			
			bool reset{false};
			
			if (gizmo.lockX && gizmo.start.x != gizmo.current.x)
			{
				gizmo.current.x = gizmo.start.x;
				reset = true;
			}
			
			if (gizmo.lockY && gizmo.start.y != gizmo.current.y)
			{
				gizmo.current.y = gizmo.start.y;
				reset = true;
			}
			
			if (gizmo.lockZ && gizmo.start.z != gizmo.current.z)
			{
				gizmo.current.z = gizmo.start.z;
				reset = true;
			}
			
			bool const done{(gizmo.m_using && !ImGuizmo::IsUsing())};
			
			if (reset || done)
			{
				mth::Transformf t;
				
				switch (gizmo.mode)
				{
					case ModeGizmo::move:
						t.position = gizmo.current;
						break;
						
					case ModeGizmo::rotate:
						t.rotation = mth::euler(gizmo.current);
						break;
						
					case ModeGizmo::scale:
						t.scale = gizmo.current;
						break;
				}
				
				gizmo.m_matrix = t.matrix();
			}
			
			if (!gizmo.m_using && ImGuizmo::IsUsing())
				gizmo.start = gizmo.current;
			
			gizmo.delta = gizmo.current - gizmo.start;
			
			gizmo.m_using = ImGuizmo::IsUsing();
			
			return done;
		}
		
		// ImGui
		
		void Editor::pushDisabled()
		{
			ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
		}
		
		void Editor::popDisabled()
		{
			ImGui::PopStyleVar();
			ImGui::PopItemFlag();
		}
		
		void Editor::line()
		{
			ImGui::Separator();
		}
		
		void Editor::space()
		{
			ImGui::Spacing();
		}
		
		void Editor::text(char const *const s)
		{
			ImGui::TextUnformatted(s);
		}
		
		void Editor::text(std::string const &s)
		{
			text(s.c_str());
		}
		
		void Editor::header(char const *const s)
		{
			space();
			text(s);
			space();
		}
		
		void Editor::header(std::string const &s)
		{
			header(s.c_str());
		}
		
		void Editor::image
		(
			res::Texture const &texture,
			mth::Vec2f          size,
			mth::Vec2f   const &UV1,
			mth::Vec2f   const &UV2
//			gfx::ColRGBAf const &tint,
//			gfx::ColRGBAf const &border
		)
		{
			if (size.x < 0.0f && size.y < 0.0f)
			{
				size.x = static_cast<float>(texture.width());
				size.y = static_cast<float>(texture.height());
			}
			else if (size.x < 0.0f)
			{
				float const ratio{size.y / static_cast<float>(texture.height())};
				size.x = ratio * static_cast<float>(texture.width());
			}
			else if (size.y < 0.0f)
			{
				float const ratio{size.x / static_cast<float>(texture.width())};
				size.y = ratio * static_cast<float>(texture.height());
			}
			
			std::uint32_t ID
			{
				*reinterpret_cast<std::uint32_t const *>
				(
					texture.handle()
				)
			};
			
			ImGui::Image
			(
				reinterpret_cast<void *>(ID),
				{size.x, size.y},
				{UV1.x, UV1.y},
				{UV2.x, UV2.y}
//				{tint.r, tint.g, tint.b, tint.a},
//				{border.r, border.g, border.b, border.a}
			);
		}
		
		bool Editor::imagebutton
		(
			res::Texture const &texture,
			mth::Vec2f          size,
			mth::Vec2f   const &UV1,
			mth::Vec2f   const &UV2
		)
		{
			if (size.x < 0.0f && size.y < 0.0f)
			{
				size.x = static_cast<float>(texture.width());
				size.y = static_cast<float>(texture.height());
			}
			else if (size.x < 0.0f)
			{
				float const ratio{size.y / static_cast<float>(texture.height())};
				size.x = ratio * static_cast<float>(texture.width());
			}
			else if (size.y < 0.0f)
			{
				float const ratio{size.x / static_cast<float>(texture.width())};
				size.y = ratio * static_cast<float>(texture.height());
			}
			
			std::uint32_t ID
			{
				*reinterpret_cast<std::uint32_t const *>
				(
					texture.handle()
				)
			};
		
			if
			(
				ImGui::ImageButton
				(
					reinterpret_cast<void *>(ID),
					{size.x, size.y},
					{UV1.x, UV1.y},
					{UV2.x, UV2.y},
					0
				)
			)
			{
				m_app->backend().shouldRepaintViewportEditor(true);
				return true;
			}
			
			return false;
		}
		
		bool Editor::button
		(
			const char *const ID,
			const char *const label
		)
		{
			ImGui::PushID(ID);
			bool const r{ImGui::Button(label)};
			ImGui::PopID();
			
			if (r)
				m_app->backend().shouldRepaintViewportEditor(true);
			
			return r;
		}
		
		bool Editor::input
		(
			char        const  *const ID,
			char        const  *const label,
			std::string        &      buffer,
			bool               *const tracker,
			FlagsInput  const         flags
		)
		{
			if
			(
				performInput
				(
					[](auto label, auto &buffer, auto flags) -> bool
					{
						return ImGui::InputText
						(
							label,
							&buffer,
							flags
						);
					},
					ID,
					label,
					buffer,
					tracker,
					flags
				)
			)
			{
				m_app->backend().shouldRepaintViewportEditor(true);
				return true;
			}
			
			return false;
		}
		
		bool Editor::input
		(
			char        const  *const ID,
			char        const  *const label,
			InputString        &      data,
			FlagsInput  const         flags
		)
		{
			return input(ID, label, data.buffer, &data.tracker, flags);
		}
		
		bool Editor::input
		(
			char       const *const ID,
			char       const *const label,
			float            &      buffer,
			float      const        step,
			float      const        speed,
			bool             *const tracker,
			FlagsInput const        flags
		)
		{
			if
			(
				performInput
				(
					[](auto label, auto &buffer, auto flags, auto step, auto speed) -> bool
					{
						return ImGui::InputFloat
						(
							label,
							&buffer,
							step,
							speed,
							"%.3f",
							flags
						);
					},
					ID,
					label,
					buffer,
					tracker,
					flags,
					step,
					speed
				)
			)
			{
				m_app->backend().shouldRepaintViewportEditor(true);
				return true;
			}
			
			return false;
		}
	
		bool Editor::input
		(
			char       const *const ID,
			char       const *const label,
			InputFloat       &      data,
			float      const        step,
			float      const        speed,
			FlagsInput const        flags
		)
		{
			return input(ID, label, data.buffer, step, speed, &data.tracker, flags);
		}
		
		bool Editor::input
		(
			char          const *const ID,
			char          const *const label,
			std::int32_t        &      buffer,
			std::int32_t  const        step,
			std::int32_t  const        speed,
			bool         *const        tracker,
			FlagsInput    const        flags
		)
		{
			if (
				performInput
				(
					[](auto label, auto &buffer, auto flags, auto step, auto speed) -> bool
					{
						return ImGui::InputInt
						(
							label,
							&buffer,
							step,
							speed,
							flags
						);
					},
					ID,
					label,
					buffer,
					tracker,
					flags,
					step,
					speed
				)
			)
			{
				m_app->backend().shouldRepaintViewportEditor(true);
				return true;
			}
			
			return false;
		}
	
		bool Editor::input
		(
			char         const *const ID,
			char         const *const label,
			InputInt           &      data,
			std::int32_t const        step,
			std::int32_t const        speed,
			FlagsInput   const        flags
		)
		{
			return input(ID, label, data.buffer, step, speed, &data.tracker, flags);
		}
		
		bool Editor::pickerResource
		(
			char                const *const ID,
			char                const *const label,
			res::IDResource           &      data,
			res::IDTypeResource const        type
		)
		{
			static std::string stringID;
			
			ImGui::BeginGroup();
			
			float const
				widthAvail  {ImGui::GetContentRegionAvail().x},
				widthContent{widthAvail * 0.65f},
				widthButton {ImGui::GetFrameHeight()},
				widthInput  {widthButton * 1.5f},
				widthGap    {ImGui::GetStyle().ItemInnerSpacing.x},
				widthName   {mth::max(1.0f, ImGui::CalcItemWidth() - (widthButton * 3.0f + widthInput + widthGap * 4.0f))},
				widthLabel  {widthAvail - widthContent};
			
			ImVec2 const
				sizeButton {widthButton, widthButton},
				sizePadding{1.0f, 0.0f};
			
			// Resource type icon.
			
			ImGui::AlignTextToFramePadding();
			ImGui::TextUnformatted(iconForTypeResourceByID(*m_app, type));
			
			// Resource name.
			
			ImGui::SameLine(sizeButton.x, widthGap);
			
			auto const it(m_resources.data.lookup.find(data));
			
			if (it != m_resources.data.lookup.end())
				ImGui::TextUnformatted(it->second->name.c_str());
			else
				ImGui::TextDisabled("(none)");
			
			// ID input.
			
			stringID = "##";
			stringID += ID;
			
			ImGui::SameLine(widthButton + widthGap + widthName, widthGap);
			ImGui::PushItemWidth(widthInput);
			ImGui::InputScalar(stringID.c_str(), app::datatypeImGui<res::IDResource>(), &data);
			ImGui::PopItemWidth();
			
			/// @todo: Show name too, probably easier once Ref<T> is back.
			
			// Picker button.
			
			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, sizePadding);
			ImGui::SameLine(0, widthGap);
			
			stringID += '_';
			
			ImGui::PushID(stringID.c_str());
			
			bool const open{ImGui::Button(ICON_FK_FOLDER_OPEN, sizeButton)};
			
			ImGui::PopID();
			
			// Reset button.
			
			stringID += '_';
			
			ImGui::SameLine(0, widthGap);

			ImGui::PushID(stringID.c_str());
			
			if (ImGui::Button(ICON_FK_TRASH, sizeButton))
				data = 0;
			
			ImGui::PopID();
			ImGui::PopStyleVar();
			
			ImGui::EndGroup();
			
			// Label.
			
			ImGui::SameLine(0, widthGap);
			
			ImGui::TextUnformatted(label);
			
			// Do this at the end to get the cursor in the right place.
			if (open)
			{
				m_resources.settingsDraw.filterTypeResource.clear();
				m_resources.settingsDraw.filterTypeResource.emplace_back(type);
				m_resources.settingsDraw.filterTypeEntryResource = TypeEntryResource::file;
				openPickerResources(*m_app, *this, data, m_resources.statePicker);
			}
			
			return drawPickerResources
			(
				*m_app,
				*this,
				data,
				m_resources.data,
				m_resources.data.root,
				m_resources.statePicker,
				m_resources.settingsDraw
			);
		}
		
		void Editor::nameResource
		(
			res::IDResource     const data,
			res::IDTypeResource const type
		)
		{
			float const
				widthButton{ImGui::GetFrameHeight()},
				widthGap   {ImGui::GetStyle().ItemInnerSpacing.x};
			
			// Resource type icon.
			
			ImVec2
				cursor{ImGui::GetCursorPos()};
			
			ImGui::AlignTextToFramePadding();
			ImGui::TextUnformatted(iconForTypeResourceByID(*m_app, type));
			
			// Resource name.
			
			ImGui::SameLine();
			
			cursor.x += widthButton + widthGap;
			ImGui::SetCursorPos(cursor);
			
			auto const it(m_resources.data.lookup.find(data));
			
			if (it != m_resources.data.lookup.end())
				ImGui::TextUnformatted(it->second->name.c_str());
			else
				ImGui::TextDisabled("(none)");
		}
		
		bool Editor::buttonAS
		(
			std::string const &ID,
			std::string const &label
		)
		{
			return button(ID.c_str(), label.c_str());
		}
		
		bool Editor::inputStringAS
		(
			std::string const &ID,
			std::string const &label,
			InputString       &buffer,
			FlagsInput  const  flags
		)
		{
			return input(ID.c_str(), label.c_str(), buffer, flags);
		}
		
		bool Editor::inputFloatAS
		(
			std::string const &ID,
			std::string const &label,
			InputFloat        &buffer,
			float       const  step,
			float       const  speed,
			FlagsInput  const  flags
		)
		{
			return input(ID.c_str(), label.c_str(), buffer, step, speed, flags);
		}
		
		bool Editor::inputIntAS
		(
			std::string  const &ID,
			std::string  const &label,
			InputInt           &buffer,
			std::int32_t const  step,
			std::int32_t const  speed,
			FlagsInput   const  flags
		)
		{
			return input(ID.c_str(), label.c_str(), buffer, step, speed, flags);
		}
		
		Editor::Editor
		(
			std::unique_ptr<Logger> &&logger
		)
		:
		m_logger{std::move(logger)}
		{
		}
		
		Editor::~Editor() = default;
		
		bool Editor::initBeforeHook(app::App &app, std::string const &basepath, int argc, char *argv[])
		{
			namespace fs = boost::filesystem;
			
			constexpr char const
				*error{"Failed to initialise editor: "};
			
			refreshStateHistory();
			
			m_karhuInstalled = SDK::env::home();
			
			m_app      = &app;
			m_basepath = basepath;
			
			auto
				&io(ImGui::GetIO());
			
			// Enable docking!
			io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
			
			// To prevent dragging in the viewport from moving the window and such.
			io.ConfigWindowsMoveFromTitleBarOnly = true;
			
			// Different name so the client game can have its own ImGui file.
			io.IniFilename = "imgui_editor.ini";
			
			/// @todo: Should try to run from local path? Or offer dialogue to set path if there isn't one?
			if (m_basepath.empty())
			{
				log::err("Karhu")
					<< error
					<< "no project basepath set with -projdir";
				
				return false;
			}
			
			if (argc > 0)
			{
				m_programpath = argv[0];
				
				log::msg("Karhu")
					<< "Program path set to '"
					<< m_programpath
					<< '\'';
			}
			else
			{
				log::err("Karhu")
					<< error
					<< "could not find program path";
				
				return false;
			}
			
			constexpr float const scaleFont{2.0f};
			
			io.FontGlobalScale = 1.0f / scaleFont;
			
			// Load the font.
			{
				// Important for ImGui that we make this buffer static.
				static
				#include "inl/Roboto-Medium.inl"
				
				io.Fonts->AddFontFromMemoryTTF
				(
					Roboto_Medium_ttf,
					Roboto_Medium_ttf_len,
					15.5f * scaleFont
				);
			}
			
			// Add the icon font.
			{
				// Important for ImGui that we make this buffer static.
				static
				#include "inl/forkawesome-webfont.inl"
				
				// Same for this value.
				static ImWchar const ranges[]{ICON_MIN_FK, ICON_MAX_FK, 0};

				ImFontConfig config;
				config.MergeMode        = true;
				config.GlyphMinAdvanceX = 20.0f * scaleFont;
				
				io.Fonts->AddFontFromMemoryTTF
				(
					forkawesome_webfont_ttf,
					forkawesome_webfont_ttf_len,
					14.0f * scaleFont,
					&config,
					ranges
				);
			}
			
			// Add the default monospace font.
			{
				ImFontConfig config;
				config.SizePixels = 13.0f * scaleFont;
				io.Fonts->AddFontDefault(&config);
			}
			
			#include "inl/textures.inl"
			
			if (!(m_textureLogo = texture("logo")))
				return false;
			
			if (!(m_textureIconPlay = texture("icon-play")))
				return false;
			
			if (!(m_textureIconConnect = texture("icon-connect")))
				return false;
			
			if (!(m_textureIconStop = texture("icon-stop")))
				return false;
			
			using Project = tool::proj::file::Project;
			
			auto p(Project::load(m_basepath));
			
			if (!p)
			{
				log::err("Karhu")
					<< error
					<< "could not load Karhu project from '"
					<< m_basepath
					<< '\'';
				
				return false;
			}
			
			m_project = std::make_unique<Project>(std::move(*p));
			
			auto const info(m_project->info());
			
			if (!info)
			{
				log::err("Karhu")
					<< error
					<< "could not load info file for Karhu project from '"
					<< m_basepath
					<< '\'';
				
				return false;
			}
			
			log::msg("Karhu")
				<< "Loaded project '"
				<< *info->name()
				<< "' ("
				<< info->identifier()->full()
				<< ')';
			
			/// @todo: Not hardcode resource subfolder?
			fs::path respath{m_basepath};
			respath.append("res");
			respath.make_preferred();
			
			s_filewatcher = std::make_unique<Filewatcher>();
			
			if (!s_filewatcher->directory(respath.string()))
				return false;
			
			log::msg("Karhu") << "Started filewatcher in '" << respath.string() << '\'';
			
			if (!initScripting())
				return false;
			
			if (!initPlatforms())
				return false;
			
			if (!reloadResources())
				return false;
			
			{
				auto const types(app.ecs().typesOfComponentsRegistered());
				
				for (ecs::IDTypeComponent const type : types)
				{
					m_typesComponents.emplace
					(
						app.ecs().nameOfTypeComponent(type),
						type
					);
				}
			}
			
			return true;
		}
		
		bool Editor::initAfterHook(app::App &)
		{
			if (!initEditors())
				return false;
			
			return true;
		}
		
		void Editor::updateAndRender(app::App &app)
		{
			/// @todo: Can't think of a better solution to this right now but due to the double buffering this needs to happen the first two frames after editor startup.
			{
				static int
					refreshedFirstTime{0};
				
				if (refreshedFirstTime < 2)
				{
					app.backend().shouldRepaintViewportEditor(true);
					++ refreshedFirstTime;
				}
			}
			
			namespace fs = boost::filesystem;
			
			if (!m_active)
				return;
			
			if (!app.win().top())
				return;
			
			bool const playing{connectedOrConnecting()};
			
			Platform const p{platform()};
	
			bool const cannotPlay
			{(
				playing ||
				!m_platforms.settings[p]->valid
			)};
			
			{
				gfx::Transform const *const transform
				(
					app.ecs().componentInEntity<gfx::Transform>
					(
						m_camera.transform,
						m_camera.entity
					)
				);
				
				gfx::Camera const *const camera
				(
					app.ecs().componentInEntity<gfx::Camera>
					(
						m_camera.camera,
						m_camera.entity
					)
				);
				
				if (camera && transform)
				{
					app.backend().updateIm3D
					(
						transform,
						camera,
						static_cast<float>(camera->target()->width()),
						static_cast<float>(camera->target()->height())
					);
				}
				else
					app.backend().updateIm3D(nullptr, nullptr, 0.0f, 0.0f);
			}
			
			#ifdef KARHU_PLATFORM_MAC
				#define karhuEDITOR_COMBOKEY "cmd"
				bool const combokey{ImGui::GetIO().KeySuper};
			#else
				#define karhuEDITOR_COMBOKEY "ctrl"
				bool const combokey{ImGui::GetIO().KeyCtrl};
			#endif
			
			if (!ImGui::GetIO().WantCaptureKeyboard)
			{
				if (combokey)
				{
					if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Z)))
						popAction();
					else if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Y)))
						unpopAction();
					else if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_X)))
						destroySelected(*m_app, *this);
					// @todo: ASCII is mapped to corresponding keys in both SDL and Karhu so this is fine but needs to be fixed before more backends are added; update both backend and here to use Karhu key codes instead; there is already a Karhu to SDL translation function somewhere.
					else if (ImGui::IsKeyPressed('d'))
						duplicateSelected(*m_app, *this);
					else if (ImGui::IsKeyPressed('s'))
						saveScene(*m_app, *this);
					else if (ImGui::IsKeyPressed('r') && !cannotPlay)
						apply();
				}
			}
			
			if (m_server)
			{
				m_server->update();
				
				switch (m_server->state())
				{
					case network::State::connecting:
						break;
					
					case network::State::disconnected:
					{
						if
						(
							StateEditor::connecting == m_state ||
							StateEditor::connected  == m_state
						)
							disconnect();
						
						break;
					}
					
					case network::State::connected:
					{
						if (StateEditor::connected != m_state)
						{
							m_state = StateEditor::connected;
							log::msg() << "Successfully connected to client";
							emitEvent(EConnected{});
						}
						
						break;
					}
				}
			}
			
			/// @todo: Not hardcode resource subfolder?
			/// @todo: Store respath and not make it every frame.
			fs::path respath{m_basepath};
			respath.append("res");
			respath.make_preferred();
			
			// Make sure this is updated after the server in case it disconnects.
			s_filewatcher->update
			(
				*this,
				app,
				respath.string()
			);
			
			constexpr ImGuiWindowFlags winflags
			{
				ImGuiWindowFlags_MenuBar               |
				ImGuiWindowFlags_NoTitleBar            |
				ImGuiWindowFlags_NoCollapse            |
				ImGuiWindowFlags_NoResize              |
				ImGuiWindowFlags_NoMove                |
				ImGuiWindowFlags_NoBringToFrontOnFocus |
				ImGuiWindowFlags_NoNavFocus            |
				ImGuiWindowFlags_NoBackground
			};
			
			const ImGuiViewport &viewport{*ImGui::GetMainViewport()};
			ImGui::SetNextWindowPos(viewport.Pos);
			ImGui::SetNextWindowSize(viewport.Size);
			ImGui::SetNextWindowViewport(viewport.ID);
			
			ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2{0.0f, 0.0f});
			ImGui::PushStyleColor(ImGuiCol_WindowBg, 0xff0000ff);
			ImGui::Begin("Editor", nullptr, winflags);
			ImGui::PopStyleColor();
			ImGui::PopStyleVar(3);
			{
				ImGui::BeginMainMenuBar();
				{
					if (ImGui::BeginMenu("File"))
					{
						if (ImGui::MenuItem("Save scene", karhuEDITOR_COMBOKEY " S"))
							saveScene(*m_app, *this);
						ImGui::EndMenu();
					}
					
					if (ImGui::BeginMenu("Edit"))
					{
						if
						(
							ImGui::MenuItem
							(
								m_history.labelUndo.c_str(),
								karhuEDITOR_COMBOKEY " Z",
								false,
								m_history.canUndo
							)
						)
							popAction();
						
						if
						(
							ImGui::MenuItem
							(
								m_history.labelRedo.c_str(),
								karhuEDITOR_COMBOKEY " Y",
								false,
								m_history.canRedo
							)
						)
							unpopAction();
						
						ImGui::Separator();
						
						if
						(
							ImGui::MenuItem
							(
								"Delete",
								karhuEDITOR_COMBOKEY " X",
								false,
								!entitiesSelected().empty()
							)
						)
							destroySelected(*m_app, *this);
						
						if
						(
							ImGui::MenuItem
							(
								"Duplicate",
								karhuEDITOR_COMBOKEY " D",
								false,
								!entitiesSelected().empty()
							)
						)
							duplicateSelected(*m_app, *this);
//						ImGui::MenuItem("Cut",   karhuEDITOR_COMBOKEY " X");
//						ImGui::MenuItem("Copy",  karhuEDITOR_COMBOKEY " C");
//						ImGui::MenuItem("Paste", karhuEDITOR_COMBOKEY " V");
						
						ImGui::EndMenu();
					}
					
					if (ImGui::BeginMenu("Create"))
					{
						if (ImGui::MenuItem("Entity"))
						{
							pushAction
							(
								callerless,
								AEntity{app, *this, TypeActionEntity::create}
							);
							
							selectNewEntity(*m_app, *this);
						}
						
						bool canAddComponent{(1 == m_values.entitiesSelected.size())};
						
						if (ImGui::BeginMenu("Component", canAddComponent))
						{
							for (auto const &it : m_typesComponents)
								if (ImGui::MenuItem(it.first.c_str(), nullptr, false, canAddComponent))
									pushAction
									(
										callerless,
										AComponent
										{
											app,
											*this,
											TypeActionComponent::create,
											it.second,
											m_values.entitiesSelected[0]
										}
									);
							
							ImGui::EndMenu();
						}
						
						ImGui::EndMenu();
					}
					
					if (ImGui::BeginMenu("Window"))
					{
						for (auto &it : m_editors)
						{
							if (ImGui::MenuItem(it.first.c_str(), nullptr, it.second.open))
							{
								if (it.second.open)
								{
									// Close.
									it.second.editor.reset();
									it.second.open = false;
								}
								else
								{
									// Try to reöpen.
									if ((it.second.editor = it.second.factory()))
										if (it.second.editor->init(app, *this))
											it.second.open = true;
								}
							}
						}
						
						ImGui::EndMenu();
					}
					
					if (ImGui::BeginMenu("Tools"))
					{
						if (ImGui::MenuItem("Refresh resources"))
							refreshResources();
						
						if (ImGui::MenuItem("Generate script API symbol file"))
							generateScriptsymbols(*this);
						
						if (ImGui::BeginMenu("Localisation"))
						{
							if (ImGui::MenuItem("Reload"))
							{
								m_app->reloadLocalisations();
								m_app->backend().shouldRepaintViewportEditor(true);
							}
							
							for (auto const &code : m_app->loc().locales())
							{
								if (ImGui::MenuItem(code.c_str()))
								{
									m_app->reloadLocalisations(code);
									m_app->backend().shouldRepaintViewportEditor(true);
								}
							}
							
							ImGui::EndMenu();
						}
						
						ImGui::EndMenu();
					}
				}
				ImGui::EndMainMenuBar();
				
				constexpr float const
					heightBarInner{40.0f},
					paddingBar    {6.0f},
					heightBarOuter{heightBarInner + paddingBar * 2.0f};
				
				float const
					widthPanel{ImGui::GetContentRegionAvail().x - paddingBar * 2.0f};
				
				float
					widthImage{0.0f};
				
				ImVec2 cursor{ImGui::GetCursorPos()};
				
				/// @todo: Quick hardcoded logo test. Fix.
				{
					widthImage = static_cast<float>(m_textureLogo->width());
					
					ImGui::SetCursorPos
					({
						cursor.x + paddingBar + widthPanel - widthImage,
						cursor.y + paddingBar
					});
				
					ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2{0.0f, 0.0f});
					
					image(*m_textureLogo);
					
					ImGui::PopStyleVar();
				}
				
				ImGui::SetCursorPos
				({
					cursor.x + paddingBar,
					cursor.y + paddingBar
				});
				
				ImGui::BeginChild
				(
					"editor-bar",
					ImVec2
					{
						widthPanel - widthImage - paddingBar,
						heightBarInner
					},
					false,
					(
						ImGuiWindowFlags_NoScrollbar |
						ImGuiWindowFlags_NoScrollWithMouse
					)
				);
				{
					/// @todo: Wrap disabled stuff.
					if (cannotPlay)
						pushDisabled();
					
					if (imagebutton(*m_textureIconPlay))
						apply();
					
					/// @todo: Wrap disabled stuff.
					if (cannotPlay)
						popDisabled();
					
					ImGui::SameLine();
					
					/// @todo: Wrap disabled stuff.
					if (!playing)
						pushDisabled();
					
					if (imagebutton(*m_textureIconStop))
						disconnect();
					
					/// @todo: Wrap disabled stuff.
					if (!playing)
						popDisabled();
					
					if (karhuInstalled())
					{
						ImGui::SameLine();
						
						ImVec2 const cursor{ImGui::GetCursorPos()};
						
						ImGui::SetCursorPos
						({
							cursor.x,
							cursor.y + (heightBarInner - ImGui::GetFrameHeight()) * 0.5f
						});
						
						ImGui::BeginChild
						(
							"editor-bar-inner",
							ImVec2
							{
								widthPanel - widthImage - paddingBar,
								ImGui::GetFrameHeight()
							},
							false,
							(
								ImGuiWindowFlags_NoScrollbar |
								ImGuiWindowFlags_NoScrollWithMouse
							)
						);
						{
							drawDropdownPlatform(0, 150.0f);
							
							// App build actions.
							
							ImGui::SameLine();
							ImGui::Text("App:");

							ImGui::SameLine();
							drawCheckboxesBuildstepsProject(true);
							
							ImGui::SameLine();
							ImGui::SeparatorEx(ImGuiSeparatorFlags_Vertical);
							
							// Karhu build actions.
							
							ImGui::SameLine();
							ImGui::Text("Karhu:");
							
							ImGui::SameLine();
							drawCheckboxesBuildstepsKarhu(true);
						}
						ImGui::EndChild();
					}
				}
				ImGui::EndChild();
				
				cursor.y += heightBarOuter - 2.0f;
				ImGui::SetCursorPos(cursor);
				
				constexpr ImGuiDockNodeFlags const dockflags
				{
					ImGuiDockNodeFlags_None
				};
				
				ImGui::DockSpace
				(
					ImGui::GetID("Dockspace"),
					ImVec2{0.0f, 0.0f},
					dockflags
				);
			}
			ImGui::End();
			
			#undef karhuEDITOR_COMBOKEY
		
			//
//! @todo: Slett oktregreier når ferdigtesta.
//m_app->octree().drawInEditor(*this, *m_app);

			constexpr ImGuiWindowFlags
				flagsNormal  {ImGuiWindowFlags_HorizontalScrollbar},
				flagsScripted{ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_HorizontalScrollbar};
			
			for (auto &it : m_editors)
			{
				if (it.second.open)
				{
					if
					(
						ImGui::Begin
						(
							it.first.c_str(),
							&it.second.open,
							((it.second.scripted) ? flagsScripted : flagsNormal)
						)
					)
						it.second.editor->updateAndRender(app, *this);
					
					ImGui::End();
				}
			}
			
			for (auto const &name : m_editorsToReload)
			{
				auto it(m_editors.find(name));
				
				if (it == m_editors.end())
					continue;
				
				it->second.editor.reset();
				it->second.open = false;
			
				// Try to reöpen.
				if ((it->second.editor = it->second.factory()))
					if (it->second.editor->init(app, *this))
						it->second.open = true;
			}
			
			m_editorsToReload.clear();
		}
		
		void Editor::updateInViewportAndRender(app::App &app, mth::Vec2f const &size)
		{
			m_gizmo = false;
			
			if (!m_active)
				return;
			
			if (!app.win().top())
				return;
			
			for (auto &it : m_editors)
				if (it.second.open)
					it.second.editor->updateInViewportAndRender(app, *this, size);
		}
		
		void Editor::scene(res::IDResource const resource, ecs::IDScene const scene)
		{
			m_scene.resource = resource;
			m_scene.scene    = scene;
		}
		
		Server *Editor::server() const
		{
			if (StateEditor::connected == m_state)
				return m_server.get();
			
			return nullptr;
		}
		
		void Editor::drawDropdownPlatform(int const ID, float const width)
		{
			bool const playing{connectedOrConnecting()};
			
			if (playing)
				pushDisabled();
			
			if (width > 0.0f)
				ImGui::SetNextItemWidth(width);
			
			ImGui::PushID(ID);
			
			if
			(
				ImGui::BeginCombo
				(
					"",
					platform::name
					(
						m_platforms.datas[m_platforms.index].platform
					)
				)
			)
			{
				std::stringstream label;
				
				for (int i{0}; i < m_platforms.datas.size(); ++ i)
				{
					char const *const name
					{
						platform::name
						(
							m_platforms.datas[i].platform
						)
					};
					
					bool const selected{(m_platforms.index == i)};
					
					label.str("");
					label << name;
					
					bool available{true};
					
					switch (m_platforms.datas[i].type)
					{
						case TypePlatform::unavailable:
							label << " (unavailable)";
							available = false;
							pushDisabled();
							break;
					
						case TypePlatform::current:
							label << " (current)";
							break;
							
						case TypePlatform::local:
							label << " (local)";
							break;
						
						case TypePlatform::remote:
							label << " (remote)";
							break;
					}
					
					if (ImGui::Selectable(label.str().c_str(), selected))
						m_platforms.index = i;
					
					if (!available)
						popDisabled();
					
					if (selected)
						ImGui::SetItemDefaultFocus();
				}
				
				ImGui::EndCombo();
			}
			
			ImGui::PopID();
			
			if (playing)
				popDisabled();
		}
		
		void Editor::drawCheckboxesBuildstepsProject(bool const sameLine)
		{
			Platform const p{platform()};
			
			bool const
				playing{connectedOrConnecting()},
				current{(Platform::current == p)},
				disable{(playing || current)};
			
			if (disable)
				pushDisabled();
			
			if (ImGui::Checkbox("Clean", &m_platforms.settings[p]->clean))
				validateSettings(p);
	
			if (sameLine)
				ImGui::SameLine();
	
			if (ImGui::Checkbox("Make", &m_platforms.settings[p]->make))
				validateSettings(p);
	
			if (sameLine)
				ImGui::SameLine();
	
			if (ImGui::Checkbox("Build", &m_platforms.settings[p]->build))
				validateSettings(p);
	
			if (sameLine)
				ImGui::SameLine();
	
			if (ImGui::Checkbox("Run", &m_platforms.settings[p]->run))
				validateSettings(p);
			
			if (disable)
				popDisabled();
		}
		
		void Editor::drawCheckboxesBuildstepsKarhu(bool const sameLine)
		{
			Platform const p{platform()};
			
			bool const
				playing{connectedOrConnecting()},
				current{(Platform::current == p)},
				disable{(playing || current)};
			
			if (disable)
				pushDisabled();
			
			if (ImGui::Checkbox("Clean", &m_platforms.settings[p]->cleanKarhu))
				validateSettings(p);
	
			if (sameLine)
				ImGui::SameLine();

			if (ImGui::Checkbox("Make", &m_platforms.settings[p]->makeKarhu))
				validateSettings(p);
			
			if (sameLine)
				ImGui::SameLine();

			if (ImGui::Checkbox("Build", &m_platforms.settings[p]->buildKarhu))
				validateSettings(p);
			
			if (disable)
				popDisabled();
		}
		
		void Editor::drawRadiobuttonsBuildconfig(bool const sameLine)
		{
			Platform const p{platform()};
			
			bool const
				playing{connectedOrConnecting()},
				current{(Platform::current == p)},
				disable{(playing || current)};
			
			if (disable)
				pushDisabled();
			
			if (ImGui::RadioButton("Debug", (true == m_platforms.settings[p]->debug)))
				m_platforms.settings[p]->debug = true;

			if (sameLine)
				ImGui::SameLine();

			if (ImGui::RadioButton("Release", (false == m_platforms.settings[p]->debug)))
				m_platforms.settings[p]->debug = false;
			
			if (disable)
				popDisabled();
		}
		
		void Editor::drawBuildconfigNetworking(bool const sameLine, float const width)
		{
			Platform const p{platform()};
			
			bool const playing{connectedOrConnecting()};
			
			if (playing)
				pushDisabled();
			
			if (ImGui::RadioButton("Online", (true == m_platforms.settings[p]->online)))
				m_platforms.settings[p]->online = true;
			
			if (sameLine)
				ImGui::SameLine();
			
			if (ImGui::RadioButton("Offline", (false == m_platforms.settings[p]->online)))
				m_platforms.settings[p]->online = false;
			
			if (!m_platforms.settings[p]->online)
				pushDisabled();
			
			if (width > 0.0f)
				ImGui::SetNextItemWidth(width);
	
			input("build-net-port", "Port", m_platforms.settings[p]->port, 0);
	
			if (sameLine)
				ImGui::SameLine();
			
			ImGui::Checkbox("Manual client", &m_platforms.settings[p]->manual);
	
			if (sameLine)
				ImGui::SameLine();
			
			if (!m_platforms.settings[p]->online)
				popDisabled();
			
			if (playing)
				popDisabled();
		}
		
		res::Texture *Editor::texture(char const *const name) const
		{
			auto const it(m_textures.find(name));
			
			if (it == m_textures.end())
			{
				log::err("Karhu")
					<< "Unable to find editor texture '"
					<< name
					<< '\'';
				
				return nullptr;
			}
			
			return it->second.get();
		}
		
		void Editor::active(bool const set)
		{
			m_active = set;
		}
		
		Platform Editor::platform() const noexcept
		{
			if (m_platforms.index < m_platforms.datas.size())
				return m_platforms.datas[m_platforms.index].platform;
			
			return Platform::current;
		}
		
		void Editor::entitiesSelected
		(
			std::size_t   const &      count,
			ecs::IDEntity const *const entities,
			std::int64_t  const &      caller
		)
		{
			karhuASSERT(entities || 0 == count);
			
			m_values.entitiesSelected.resize(count);
			
			for (std::size_t i{0}; i < count; ++ i)
				m_values.entitiesSelected[i] = *(entities + i);
			
			emitEvent
			(
				EEntitiesSelected
				{{
					entities,
					static_cast<std::int32_t>(count)
				}},
				count,
				entities,
				caller
			);
		}
		
		bool Editor::entitySelected(ecs::IDEntity const entity) const
		{
			return
			(
				std::find
				(
					m_values.entitiesSelected.begin(),
					m_values.entitiesSelected.end(),
					entity
				) != m_values.entitiesSelected.end()
			);
		}
		
		void Editor::entitiesTransformation
		(
			Transformation const         transformation,
			std::size_t    const  &      count,
			ecs::IDEntity  const  *const entities,
			mth::Vec4f     const  *const values,
			bool           const         done,
			std::int64_t   const  &      caller
		)
		{
			karhuASSERT(entities && values);
			
			if (count <= 0)
				return;
			
			for (std::size_t i{0}; i < count; ++ i)
			{
				ecs::IDEntity const
					entity{*(entities + i)};
				
				auto const
					transform(m_app->ecs().componentInEntity<gfx::Transform>(entity));
				
				mth::Transformf
					t{transform->global()};
				
				mth::Vec4f const
					&value(*(values + i));
			
				switch (transformation)
				{
					case Transformation::position:
						t.position = value.xyz();
						break;
					
					case Transformation::rotation:
						t.rotation = reinterpret_cast<mth::Quatf const &>(value);
						break;
					
					case Transformation::scale:
						t.scale = value.xyz();
						break;
					
					case Transformation::origin:
						t.origin = value.xyz();
						break;
				}
				
				transform->global(std::move(t));
			}
			
			auto const c(static_cast<std::int32_t>(count));
			
			emitEvent
			(
				EEntitiesTransformed
				{
					transformation,
					{entities, c},
					{values, c},
					done
				},
				count,
				entities,
				caller
			);
		}
		
		void Editor::targetViewport
		(
			mth::Vec3f const &value,
			Identifier const caller
		)
		{
			m_values.targetViewport = value;
			
			emitEvent
			(
				EViewportTargetChanged
				{
					value
				},
				caller
			);
		}
		
		/// @todo: Be able to specify scene to create entity in.
		bool Editor::createEntity
		(
			ecs::IDEntity       &inOutEntity,
			Identifier    const  caller
		)
		{
			ecs::IDScene const scene{m_scene.scene};
			
			// Create new entity if this is the first time.
			if (0 == inOutEntity)
			{
				inOutEntity = m_app->ecs().createEntityInScene(scene, "new entity");
				
				if (0 == inOutEntity)
					return false;
			
				// Add a default transform component.
				m_app->ecs().createComponentInEntity<gfx::Transform>(inOutEntity);
				
				// Create default userdata.
				m_app->ecs().entityUserdata(inOutEntity, std::make_unique<UserdataEntity>());
			}
			// Otherwise restore existing entity.
			else
			{
				auto data{static_cast<UserdataEntity *const>(m_app->ecs().entityUserdata(inOutEntity))};
				
				if (!data)
					return false;
				
				data->destroyed = false;
				
				if (data->activeBeforeDestroyed)
					m_app->ecs().entityActive(inOutEntity, true);
			}
			
			emitEvent
			(
				EEntity
				{
					TypeActionEntity::create,
					inOutEntity
				},
				caller
			);
			
			/// @todo: Also select the newly created entity?
			
			/// @todo: Deal with selection after moving code out of AEntitiesSelected and into an Editor method that takes care of soloing etc.
		
			return true;
		}
		
		bool Editor::destroyEntity
		(
			ecs::IDEntity const entity,
			Identifier    const caller
		)
		{
			// Mark existing entity as removed rather than actually
			// removing it so that it can be easily restored.
		
			auto data{static_cast<UserdataEntity *const>(m_app->ecs().entityUserdata(entity))};
		
			if (!data)
				return false;
		
			data->destroyed = true;
		
			// Inactive so not shown in viewport; keep track
			// of whether to activate again when restoring.
			
			data->activeBeforeDestroyed = m_app->ecs().entityActiveSelf(entity);
			m_app->ecs().entityActive(entity, false);
			
			auto &s(m_values.entitiesSelected);
			auto const it(std::find(s.begin(), s.end(), entity));
			bool const selected{(it != s.end())};
			
			if (selected)
				s.erase(it);
			
			emitEvent
			(
				EEntity
				{
					TypeActionEntity::destroy,
					entity,
					selected
				},
				caller
			);
			
			/// @todo: Deal with selection after moving code out of AEntitiesSelected and into an Editor method that takes care of soloing etc.
		
			return true;
		}
		
		bool Editor::duplicateEntity
		(
			ecs::IDEntity const  entity,
			ecs::IDEntity       &inOutEntity,
			Identifier    const  caller
		)
		{
			return duplicateEntityChild(entity, inOutEntity, 0, caller);
		}
		
		bool Editor::duplicateEntityChild
		(
			ecs::IDEntity const  entity,
			ecs::IDEntity       &inOutEntity,
			ecs::IDEntity const  parent,
			Identifier    const  caller
		)
		{
			auto const
				dataOriginal(static_cast<UserdataEntity const *const>(m_app->ecs().entityUserdata(entity)));
			
			if (dataOriginal && dataOriginal->destroyed)
				return true;
			
			bool const
				preexisting{(0 != inOutEntity)};
			
			if (!createEntity(inOutEntity, caller))
				return false;
			
			if (!preexisting)
			{
				if (0 != parent)
					m_app->ecs().entityParentOf(parent, inOutEntity);
				
				// Duplicate the name.
				
				std::string const
					name{m_app->ecs().entityName(entity) + " copy"};
				
				m_app->ecs().entityName(inOutEntity, name);
				
				// Duplicate the tags.
				m_app->ecs().entityTags(inOutEntity, m_app->ecs().entityTags(entity));
				
				// Duplicate the active state.
				m_app->ecs().entityActive(inOutEntity, m_app->ecs().entityActiveSelf(entity));
				
				// Duplicate the editor data.
				
				auto
					data(static_cast<UserdataEntity *const>(m_app->ecs().entityUserdata(inOutEntity)));
				
				if (dataOriginal)
				{
					*data = *dataOriginal;
					data->components.clear();
				}
				
				// Duplicate the components.
				
				auto const
					componentsOriginal(m_app->ecs().componentsByIDInEntity(entity));
				
				for (auto const &it : componentsOriginal)
				{
					// Do not duplicate destroyed components.
					if (dataOriginal)
					{
						auto const
							jt(dataOriginal->components.find(it.first));
						
						if (jt != dataOriginal->components.end())
							if (jt->second.destroyed)
								continue;
					}
					
					conv::JSON::Obj
						inOutObj;
					
					// Making a special case for transform components since entities are
					// created with one already so the deserialisation function does not
					// work here; maybe if we can change how the function to deserialise
					// an existing component works this can be improved, but it's fine.
					if (it.second == m_app->ecs().IDOfTypeComponent<gfx::Transform>())
					{
						auto const
							transformOriginal(m_app->ecs().componentInEntity<gfx::Transform>(it.first, entity));
						
						auto
							transform(m_app->ecs().componentInEntity<gfx::Transform>(inOutEntity));
						
						transform->local(transformOriginal->local());
					}
					else if (m_app->ecs().serialiseComponentInEntity(it.first, it.second, entity, inOutObj))
						m_app->ecs().deserialiseComponentInEntity(inOutEntity, inOutObj);
				}
				
				// Recursively duplicate children.
				// Since we can't iterate over relationships while
				// inserting new ones, we'll copy the identifiers.
				
				std::vector<ecs::IDEntity> children;
				
				m_app->ecs().forEachChildOfEntity
				(
					entity,
					[this, &children](ecs::IDEntity const ID) -> bool
					{
						children.emplace_back(ID);
						return true;
					},
					false,
					false
				);
				
				for (ecs::IDEntity const ID : children)
				{
					ecs::IDEntity
						inOutEntityChild{0};
			
					duplicateEntityChild(ID, inOutEntityChild, inOutEntity, caller);
				}
			}
			
			return true;
		}
		
		bool Editor::createComponent
		(
			ecs::IDTypeComponent const  type,
			ecs::IDEntity        const  entity,
			ecs::IDComponent           &inOutComponent,
			Identifier           const  caller
		)
		{
			// Create new component if this is the first time.
			if (0 == inOutComponent)
			{
				ecs::Component const
					*const c{m_app->ecs().createComponentInEntity(type, entity)};
				
				if (!c)
					return false;
				
				inOutComponent = c->identifier();
			}
			// Otherwise restore existing component.
			else
			{
				auto
					data{static_cast<UserdataEntity *const>(m_app->ecs().entityUserdata(entity))};
				
				if (!data)
					return false;
				
				auto
					it(data->components.find(inOutComponent));
				
				if (it != data->components.end())
				{
					it->second.destroyed = false;
					
					if (it->second.activeBeforeDestroyed)
						m_app->ecs().componentActiveInEntity(inOutComponent, entity, true);
				}
			}
			
			emitEvent
			(
				EComponent
				{
					TypeActionComponent::create,
					type,
					inOutComponent,
					entity
				},
				caller
			);
			
			return true;
		}

		bool Editor::destroyComponent
		(
			ecs::IDEntity    const entity,
			ecs::IDComponent const component,
			Identifier       const caller
		)
		{
			// Mark existing component as removed rather than actually
			// removing it so that it can be easily restored.
			
			auto c(m_app->ecs().componentInEntity(component, entity));
			
			if (!c)
				return false;
		
			auto data{static_cast<UserdataEntity *const>(m_app->ecs().entityUserdata(entity))};
		
			if (!data)
				return false;
		
			auto &cdata(data->components[component]);
		
			cdata.destroyed = true;
		
			// Inactive so not shown in viewport; keep track
			// of whether to activate again when restoring.
		
			cdata.activeBeforeDestroyed = c->activeSelf();
			m_app->ecs().componentActiveInEntity(component, entity, false);
			
			emitEvent
			(
				EComponent
				{
					TypeActionComponent::destroy,
					c->typeComponent(),
					component,
					entity
				},
				caller
			);
		
			return true;
		}
		
		void Editor::queueEditorReload(const std::string &name)
		{
			m_editorsToReload.emplace(name);
		}
		
		bool Editor::registerCallbackEvent(FCallbackEvent callback)
		{
			m_callbacksEvents.emplace_back(std::move(callback));
			return true;
		}
		
		/**
		 * Pushes a reversible action onto the history stack.
		 *
		 * @param caller The origin of the emission (editor or component ID); used to prevent triggering the caller's action callback. Defaults to callerless if omitted.
		 * @param action The action to be moved.
		 *
		 * @return Whether the action was successfully initialised, applied and pushed.
		 */
		bool Editor::pushAction
		(
			Identifier const   caller,
			IAction          &&action
		)
		{
			m_app->backend().shouldRepaintViewportEditor(true);
			
			/// @todo: Deal with editor history size limit.
			/// @todo: Log or anything else on error?
			
			action.caller = caller;
			
			if (!action.valid(*m_app, *this))
				return false;
			
			if (!action.apply(*m_app, *this))
				return false;
			
			// We have to clean backwards.
			for
			(
				auto i{static_cast<std::ptrdiff_t>(m_history.actions.size()) - 1};
				i >= static_cast<std::ptrdiff_t>(m_history.index);
				-- i
			)
				m_history.actions[i]->clean(*m_app, *this);
			
			m_history.actions.resize(m_history.index);
			m_history.pointers.resize(m_history.index);
			
			auto p(action.move());
			
			if (!p)
				return false;
			
			m_history.pointers.emplace_back(p.get());
			m_history.actions.emplace_back(std::move(p));
			
			++ m_history.index;
			
			refreshStateHistory();
			
			return true;
		}
		
		IAction const *Editor::peekAction() const
		{
			if (m_history.actions.empty())
				return nullptr;
			
			return m_history.actions.back().get();
		}
	
		void Editor::emitEvent(Event const &e, Identifier const caller)
		{
			for (auto &it : m_editors)
				if (it.second.editor)
					it.second.editor->event(*m_app, *this, caller, e);
			
			for (auto &callback : m_callbacksEvents)
				callback(*m_app, *this, caller, e);
		}
		
		bool Editor::emitEvent
		(
			Event         const &e,
			std::size_t   const &count,
			ecs::IDEntity const *entities,
			std::int64_t  const &caller
		)
		{
			/// @todo: Log or anything else on error?
		
			karhuASSERT(entities || 0 == count);
			
			emitEvent(e, caller);
			
			for (std::size_t i{0}; i < count; ++ i)
			{
				ecs::IDEntity const entity{*(entities + i)};
				
				auto const components
				(
					m_app->ecs().componentsByIDInEntity
					(
						entity
					)
				);
				
				for (auto const &ct : components)
				{
					auto c(m_app->ecs().componentInEntity
					(
						ct.first,
						entity
					));
					
					if (c)
					{
						/// @todo: Should not have to cast here when componentInEntity is updated to return ecs::Component instead of kenno::Component.
						auto component(static_cast<ecs::Component *>(c));
						
						if (component)
							component->editorEvent(*m_app, *this, caller, e);
					}
				}
			}
			
			return true;
		}

		Nullable<DataScript> Editor::loadDataForEditorScripted(std::string const &filename)
		{
			constexpr char const *error{"Failed to load editor script "};
			
			auto res(m_app->res().createNonowned<res::Script>());
			
			if (!res)
			{
				log::err("Karhu")
					<< error
					<< '\''
					<< filename
					<< "': could not allocate resource";
				
				return {false};
			}
			
			auto const s(res->load(filename.c_str(), "", m_adapterRead));
			
			if (!s.success)
			{
				log::err("Karhu")
					<< error
					<< '\''
					<< filename
					<< "': "
					<< s.error;
				
				return {false};
			}
			
			VtableEditorScripted vtable;
			
			auto const r(m_app->scr().loadDerivedTypeWithFactoryAndOptionalMethodsFromModule
			(
				res->module(),
				"edt::IEditor",
				vtable.type,
				"",
				vtable.factory,
				{
					{
						signatureEditorScriptedInit,
						vtable.init
					},
					{
						signatureEditorScriptedUpdateAndRender,
						vtable.updateAndRender
					},
					{
						signatureUpdateInViewportAndRender,
						vtable.updateInViewportAndRender
					},
					{
						signatureEditorScriptedEvent,
						vtable.event
					}
				}
			));
			
			if (!r.success)
			{
				log::err("Karhu")
					<< error
					<< '\''
					<< filename
					<< "': "
					<< r.error;
				
				return {false};
			}
			
			return {true, DataScript{std::move(res), std::move(vtable)}};
		}
		
		bool Editor::initScripting()
		{
			/// @todo: Init all the editor scripting.
			
			app::App &app(*m_app);
			
			m_app->scr().engine().SetDefaultNamespace("edt");
			
			karhuAS_REGISTER_TYPEDEF(app, Identifier)
			
			karhuAS_REGISTER_REFTYPE (app, Editor)
			karhuAS_REGISTER_METHODrp(app, Editor, IDSceneECS, ecs::IDScene, () const)
			
			karhuAS_REGISTER_REFTYPE(app, EditorScripted)
			karhuAS_REGISTER_METHODd(app, EditorScripted, identifier, "edt::Identifier identifier() const")
			
			karhuAS_REGISTER_ENUM   (app, TypeEvent)
			karhuAS_REGISTER_ENUMVAL(app, TypeEvent, entitiesSelected)
			karhuAS_REGISTER_ENUMVAL(app, TypeEvent, entitiesTransformed)
			karhuAS_REGISTER_ENUMVAL(app, TypeEvent, viewportTargetChanged)
			karhuAS_REGISTER_ENUMVAL(app, TypeEvent, connected)
			karhuAS_REGISTER_ENUMVAL(app, TypeEvent, disconnected)
			
			karhuAS_REGISTER_REFTYPE(app, EEntitiesSelected)
			karhuAS_REGISTER_MEMBER (app, EEntitiesSelected, entities, "const Arrayview<ecs::IDEntity>")
			
			karhuAS_REGISTER_REFTYPE(app, EEntitiesTransformed)
			karhuAS_REGISTER_REFTYPE(app, EViewportTargetChanged)
			karhuAS_REGISTER_REFTYPE(app, EConnected)
			karhuAS_REGISTER_REFTYPE(app, EDisconnected)
			
			karhuAS_REGISTER_REFTYPE(app, Event)
			karhuAS_REGISTER_MEMBER (app, Event, type,                  "TypeEvent")
			karhuAS_REGISTER_MEMBER (app, Event, entitiesSelected,      "EEntitiesSelected")
			karhuAS_REGISTER_MEMBER (app, Event, entitiesTransformed,   "EEntitiesTransformed")
			karhuAS_REGISTER_MEMBER (app, Event, viewportTargetChanged, "EViewportTargetChanged")
			karhuAS_REGISTER_MEMBER (app, Event, connected,             "EConnected")
			karhuAS_REGISTER_MEMBER (app, Event, disconnected,          "EDisconnected")

			karhuAS_REGISTER_INTERFACE(app, "IEditor")
			karhuAS_REGISTER_METHODi  (app, "IEditor", signatureEditorScriptedInit)
			karhuAS_REGISTER_METHODi  (app, "IEditor", signatureEditorScriptedUpdateAndRender)
			karhuAS_REGISTER_METHODi  (app, "IEditor", signatureEditorScriptedEvent)
			
			// GUI.
			
			karhuAS_REGISTER_METHODd (app, Editor, pushColour,    "void pushColour(const gfx::ColRGBAf &in)")
			karhuAS_REGISTER_METHODrp(app, Editor, popColour,     void, ())
			karhuAS_REGISTER_METHODrp(app, Editor, pushFilled,    void, (const bool))
			karhuAS_REGISTER_METHODrp(app, Editor, popFilled,     void, ())
			karhuAS_REGISTER_METHODrp(app, Editor, pushThickness, void, (const float))
			karhuAS_REGISTER_METHODrp(app, Editor, popThickness,  void, ())
			karhuAS_REGISTER_METHODrp(app, Editor, pushDisabled,  void, ())
			karhuAS_REGISTER_METHODrp(app, Editor, popDisabled,   void, ())
			
			karhuAS_REGISTER_METHODd(app, Editor, lineInViewport,     "void lineInViewport(const mth::Vec3f &in, const mth::Vec3f &in)")
			karhuAS_REGISTER_METHODd(app, Editor, planeInViewport,    "void planeInViewport(const mth::Vec3f &in, const mth::Vec3f &in, const mth::Vec2f &in)")
			karhuAS_REGISTER_METHODd(app, Editor, sphereInViewport,   "void sphereInViewport(const mth::Vec3f &in, const float)")
			karhuAS_REGISTER_METHODd(app, Editor, boxInViewport,      "void boxInViewport(const mth::Vec3f &in, const mth::Vec3f &in)")
			karhuAS_REGISTER_METHODd(app, Editor, cylinderInViewport, "void cylinderInViewport(const mth::Vec3f &in, const mth::Vec3f &in, const float)")
			
			karhuAS_REGISTER_METHODd(app, Editor, buttonAS, "bool button(const std::string &in, const std::string &in)")
			
			karhuAS_REGISTER_ENUM   (app, ModeGizmo)
			karhuAS_REGISTER_ENUMVAL(app, ModeGizmo, move)
			karhuAS_REGISTER_ENUMVAL(app, ModeGizmo, rotate)
			karhuAS_REGISTER_ENUMVAL(app, ModeGizmo, scale)
			
			karhuAS_REGISTER_RCTYPE (app, Gizmo)
			karhuAS_REGISTER_FACTORY(app, Gizmo)
			karhuAS_REGISTER_MEMBER (app, Gizmo, mode,        "ModeGizmo")
			karhuAS_REGISTER_MEMBER (app, Gizmo, start,       "mth::Vec3f")
			karhuAS_REGISTER_MEMBER (app, Gizmo, current,     "mth::Vec3f")
			karhuAS_REGISTER_MEMBER (app, Gizmo, delta,       "mth::Vec3f")
			karhuAS_REGISTER_MEMBER (app, Gizmo, snapPerAxis, "mth::Vec3f")
			karhuAS_REGISTER_MEMBER (app, Gizmo, lockX,       "bool")
			karhuAS_REGISTER_MEMBER (app, Gizmo, lockY,       "bool")
			karhuAS_REGISTER_MEMBER (app, Gizmo, lockZ,       "bool")
		
			karhuAS_REGISTER_METHODd(app, Gizmo, reset, "void reset(const mth::Vec3f &in)")
			
			karhuAS_REGISTER_METHODrp(app, Editor, gizmoInViewport, bool, (Gizmo &))
			
			karhuAS_REGISTER_ENUM   (app, FlagsInput)
			karhuAS_REGISTER_ENUMVAL(app, FlagsInput, none)
			karhuAS_REGISTER_ENUMVAL(app, FlagsInput, password)
			
			karhuAS_REGISTER_METHODrp (app, Editor, line,   void, ())
			karhuAS_REGISTER_METHODrp (app, Editor, space,  void, ())
			karhuAS_REGISTER_METHODrpd(app, Editor, text,   void, (std::string const &s), "void text(const std::string &in)")
			karhuAS_REGISTER_METHODrpd(app, Editor, header, void, (std::string const &s), "void header(const std::string &in)")
			
			karhuAS_REGISTER_RCTYPE (app, InputString)
			karhuAS_REGISTER_FACTORY(app, InputString)
			karhuAS_REGISTER_MEMBER (app, InputString, buffer, "std::string")
			karhuAS_REGISTER_MEMBER (app, InputString, tracker, "bool")
			
			karhuAS_REGISTER_RCTYPE (app, InputFloat)
			karhuAS_REGISTER_FACTORY(app, InputFloat)
			karhuAS_REGISTER_MEMBER (app, InputFloat, buffer, "float")
			karhuAS_REGISTER_MEMBER (app, InputFloat, tracker, "bool")
			
			karhuAS_REGISTER_RCTYPE (app, InputInt)
			karhuAS_REGISTER_FACTORY(app, InputInt)
			karhuAS_REGISTER_MEMBER (app, InputInt, buffer, "int")
			karhuAS_REGISTER_MEMBER (app, InputInt, tracker, "bool")
			
			karhuAS_REGISTER_METHODd(app, Editor, inputStringAS, "bool input(const std::string &in, const std::string &in, InputString &, const edt::FlagsInput = edt::FlagsInput::none)")
			karhuAS_REGISTER_METHODd(app, Editor, inputFloatAS,  "bool input(const std::string &in, const std::string &in, InputFloat &, const float = 0.0f, const float = 0.0f, const edt::FlagsInput = edt::FlagsInput::none)")
			karhuAS_REGISTER_METHODd(app, Editor, inputIntAS,    "bool input(const std::string &in, const std::string &in, InputInt &, const int = 0, const int = 0, const edt::FlagsInput = edt::FlagsInput::none)")
			
			m_app->scr().engine().SetDefaultNamespace("");
			
			return true;
		}
		
		bool Editor::initEditors()
		{
			// Inbuilt editors.
		
			#define karhuCREATE_EDITOR(name) \
				if (!registerEditor<Editor##name>(true, #name)) return false;
		
			#define karhuCREATE_EDITORn(type, name) \
				if (!registerEditor<Editor##type>(true, name)) return false;
		
			karhuCREATE_EDITOR(Viewport)
			karhuCREATE_EDITOR(Hierarchy)
			karhuCREATE_EDITOR(Inspector)
			karhuCREATE_EDITOR(Resources)
			karhuCREATE_EDITOR(Console)
			karhuCREATE_EDITOR(History)
		
			if (karhuInstalled())
				karhuCREATE_EDITORn(SettingsBuild, "Build settings")
				
			karhuCREATE_EDITORn(Pipeline, "Render pipeline")
			
			#undef karhuCREATE_EDITORn
			#undef karhuCREATE_EDITOR
			
			// Scripted editors.
			loadEditorsScriptedRecursively();
			
			for (auto &it : m_editors)
			{
				if (!it.second.editor)
					continue;
				
				if (!it.second.editor->init(*m_app, *this))
				{
					log::err("Karhu")
						<< "Failed to initialise subeditor '"
						<< it.first
						<< '\'';
					
					return false;
				}
			}
			
			// Failing to load a scripted editor
			// does not abort init all together.
			return true;
		}
		
		Nullable<std::string> Editor::basepathToScripts() const
		{
			namespace fs = boost::filesystem;
			
			fs::path dir{m_project->location().path()};
			
			if (m_project)
			{
				dir.append("editor"); /// @todo: Hardcoded editor script subdirectory?
				dir.make_preferred();
				
				if (!fs::exists(dir) || !fs::is_directory(dir))
					return {false};
			}
			
			return {true, dir.string()};
		}
		
		void Editor::loadEditorsScriptedRecursively()
		{
			if (auto const path = basepathToScripts())
				loadEditorsScriptedRecursively(*path);
		}
		
		void Editor::loadEditorsScriptedRecursively
		(
			std::string const &basedir,
			std::string const &dir
		)
		{
			constexpr char const *error{"Failed to load editor script "};
			
			namespace fs = boost::filesystem;
			
			fs::path fullpath{basedir}, subpath{dir};
			
			subpath.make_preferred();
			
			fullpath.append(subpath.string());
			fullpath.make_preferred();
			
			for (fs::directory_iterator it{fullpath}; it != fs::directory_iterator(); ++ it)
			{
				if ('.' == it->path().filename().string()[0])
					continue;
				
				try
				{
					fullpath = it->path();
					subpath  = fs::relative(fullpath, basedir);
					
					fullpath.make_preferred();
					subpath. make_preferred();
				
					if (fs::is_directory(fullpath))
						loadEditorsScriptedRecursively(basedir, subpath.string());
					else if (fs::is_regular_file(fullpath))
					{
						/// @todo: Use some kind of class attribute to set the name instead.
						bool const success(registerEditor<EditorScripted>
						(
							true,
							subpath.string(),
							subpath.string()
						));
						
						auto it(m_editors.find(subpath.string()));
						
						if (!success || it == m_editors.end())
						{
							log::err("Karhu")
								<< error
								<< '\''
								<< subpath.string()
								<< "': failed to create instance";
						
							continue;
						}
					}
				}
				catch (const fs::filesystem_error &e)
				{
					log::err("Karhu")
						<< error
						<< '\''
						<< subpath.string()
						<< "': "
						<< e.what();
				}
			}
		}
		
		bool Editor::initPlatforms()
		{
			using namespace std::string_literals;
			
			namespace fs = boost::filesystem;
			
			auto const build(m_project->build());

			if (!build)
				log::err("Karhu") << "Failed to load project build settings";
	
			std::vector<Platform> platformsLocal;
			
			using T = std::underlying_type_t<Platform>;
			
			for (T pf{static_cast<T>(Platform::all)}; pf; pf >>= 1)
			{
				auto const p{static_cast<Platform>(pf - (pf >> 1))};
				
				if (p == Platform::current)
					continue;
				
				TypePlatform
					type{TypePlatform::unavailable};
				
				std::string const
					identifier{platform::identifier(p)};
				
				for (auto const &system : build->systems())
				{
					if
					(
						system.name == platform::identifier(p) &&
						!system.SSH.empty()
					)
					{
						type = TypePlatform::remote;
						break;
					}
				}
				
				m_platforms.datas.emplace_back
				(
					DataPlatform
					{
						p,
						type
					}
				);
			}
			
			std::reverse
			(
				m_platforms.datas.begin(),
				m_platforms.datas.end()
			);

			auto const basepath(SDK::env::home("targets"));
			
			if (!basepath || !fs::exists(*basepath) || !fs::is_directory(*basepath))
				log::msg("Karhu") << "No Karhu installation found";
			else
			{
				log::msg("Karhu") << "Karhu installation found";

				for
				(
					fs::directory_iterator i{*basepath};
					i != fs::directory_iterator();
					++ i
				)
				{
					auto const hostname(i->path().filename().string());
					
					if
					(
						fs::is_directory(i->path()) &&
						Platform::current == platform::fromIdentifier(hostname.c_str())
					)
					{
						for
						(
							fs::directory_iterator j{i->path()};
							j != fs::directory_iterator();
							++ j
						)
						{
							if (fs::is_directory(j->path()))
							{
								auto const
									targetname(j->path().filename().string());
								
								Platform const
									p{platform::fromIdentifier(targetname.c_str())};
								
								platformsLocal.emplace_back(p);
								
								for (auto &d : m_platforms.datas)
								{
									if (p == d.platform)
									{
										d.type = TypePlatform::local;
										break;
									}
								}
							}
						}
					}
				}
			
				// One extra for running the editor itself in client mode.
				m_platforms.count = m_platforms.datas.size() + 1;
			}
			
			m_platforms.datas.emplace
			(
				m_platforms.datas.begin(),
				DataPlatform
				{
					Platform::current,
					TypePlatform::current
				}
			);
			
			std::sort
			(
				m_platforms.datas.begin(),
				m_platforms.datas.end(),
				[](auto const &a, auto const &b)
				{
					return
					(
						static_cast<int>(a.type) <
						static_cast<int>(b.type)
					);
				}
			);
			
			m_platforms.length = std::strlen
			(
			 	platform::name
			 	(
			 		m_platforms.datas[0].platform
				)
			);
			
			for (std::size_t i{1}; i < m_platforms.count; ++ i)
			{
				int const l
				(
					std::strlen
					(
						platform::name
						(
							m_platforms.datas[i].platform
						)
					)
				);
				
				if (l > m_platforms.length)
					m_platforms.length = l;
			}
			
			for (auto const &data : m_platforms.datas)
			{
				Platforms::Settings s;
				
				/// @todo: Load port from build settings.
//				if (const auto p = build->port())
//				{
//					port = *p;
//					log::msg("Karhu") << "Found port specified for project: " << port;
//				}
//				else
//					log::msg("Karhu") << "Found no port specified for project; defaulting to " << port;
				
				s.port = static_cast<int>
				(
					karhu::network::protocol::defaultport()
				);
				
				if (Platform::current == data.platform)
				{
					s.clean      =
					s.cleanKarhu =
					s.make       =
					s.makeKarhu  =
					s.build      =
					s.buildKarhu = false;
					s.run        = true;
					
					#ifdef DEBUG
					s.debug = true;
					#else
					s.debug = false;
					#endif
				}
				else
				{
					/// @todo: Save/load clean/make/build/run settings for non-current platforms in editor.
					s.clean      =
					s.cleanKarhu = false;
					s.make       =
					s.makeKarhu  =
					s.build      =
					s.buildKarhu =
					s.run        =
					s.debug      = true;
				}
				
				m_platforms.settings.emplace
				(
					data.platform,
					std::make_unique<Platforms::Settings>(std::move(s))
				);
			}
			
			return true;
		}
		
		bool Editor::popAction()
		{
			m_app->backend().shouldRepaintViewportEditor(true);
		
			if (0 == m_history.index)
				return false;
			
			-- m_history.index;
			
			bool const r
			{
				m_history.actions[m_history.index]->undo
				(
					*m_app,
					*this
				)
			};
			
			refreshStateHistory();
			
			return r;
		}
		
		bool Editor::unpopAction()
		{
			m_app->backend().shouldRepaintViewportEditor(true);
		
			if (m_history.index >= m_history.actions.size())
				return false;
			
			bool const r
			{
				m_history.actions[m_history.index]->apply
				(
					*m_app,
					*this
				)
			};
			
			++ m_history.index;
			
			refreshStateHistory();
			
			return r;
		}
		
		void Editor::refreshStateHistory()
		{
			if (m_history.actions.empty() || 0 == m_history.index)
			{
				m_history.canUndo   = false;
				m_history.labelUndo = "Undo";
			}
			else
			{
				m_history.canUndo   =  true;
				m_history.labelUndo =  "Undo ";
				m_history.labelUndo += m_history.actions[m_history.index - 1]->name();
			}

			if (m_history.actions.empty() || m_history.index >= m_history.actions.size())
			{
				m_history.canRedo   = false;
				m_history.labelRedo = "Redo";
			}
			else
			{
				m_history.canRedo   =  true;
				m_history.labelRedo =  "Redo ";
				m_history.labelRedo += m_history.actions[m_history.index]->name();
			}
		}
		
		bool Editor::apply()
		{
			constexpr char const
				*const error{"Failed to apply build actions: "};
			
			if (m_server)
			 	return true;
			
			Platform const
				pf{platform()};
			
			bool const
				current{(Platform::current == pf)};
			
			auto const
				&settings{*m_platforms.settings[pf]};
			
			if (!settings.valid)
			{
				log::err("Karhu")
					<< error
					<< "no build actions selected";
				
				return false;
			}
			
			if (!m_project)
			{
				log::err("Karhu")
					<< error
					<< "no project loaded";
				
				return false;
			}
			
			auto const build(m_project->build());
			
			if (!build)
			{
				log::err("Karhu")
					<< error
					<< "unable to load project build settings";
				
				return false;
			}
			
			std::string path;
			
			if (!current)
			{
				namespace tools = SDK::paths::home::tools;
				
				auto p(SDK::env::home(tools::karhubuild::local()));
				
				if (!p || !boost::filesystem::exists(*p))
				{
					log::err("Karhu")
						<< error
						<< " could not find 'karhubuild' utility";
					
					return false;
				}
				
				path = *p;
			}
			else
				path = m_programpath;
			
			using namespace tool::proj::file::build;
			
			std::vector<std::string>
				argsTarget{"-client"},
				argsBasic {"apply"},
				argsOther {},
				argsRun   {};
			
			if (!current)
			{
				if (settings.cleanKarhu || settings.makeKarhu  || settings.buildKarhu)
				{
					argsOther.emplace_back("--kacts");
					
					if (settings.cleanKarhu)
						argsOther.emplace_back("clean");
					
					if (settings.makeKarhu)
						argsOther.emplace_back("make");
					
					if (settings.buildKarhu)
						argsOther.emplace_back("build");
				}
				
				if (settings.clean || settings.make || settings.build)
				{
					argsOther.emplace_back("--acts");
					
					if (settings.clean)
						argsOther.emplace_back("clean");
					
					if (settings.make)
						argsOther.emplace_back("make");
					
					if (settings.build)
						argsOther.emplace_back("build");
				}
				
				/// @todo: Add preacts and kpreacts to editor build.
				/// @todo: Add hint (iOS simulator) to editor build.
				if (Platform::iOS == pf)
				{
					argsBasic.emplace_back("--hint");
					argsBasic.emplace_back("device");
				}
				
				/// @todo: Add JSON settings to editor build.
				argsBasic.emplace_back("--settings");
				argsBasic.emplace_back("{\"defines\":[\"KARHU_EDITOR\"]}");
				
				argsBasic.emplace_back("--dir");
				argsBasic.emplace_back(m_project->location().path());
				
				argsBasic.emplace_back("--conf");
				argsBasic.emplace_back
				(
					config::toString
					(
						(settings.debug)
						?
							config::Type::debug
						:
							config::Type::release
					)
				);
				
				argsBasic.emplace_back("--sys");
				argsBasic.emplace_back(platform::identifier(pf));
				
				if (settings.run)
				{
					argsRun.emplace_back("--acts");
					argsRun.emplace_back("run");
				}
			}
			
			log::msg("Karhu") << "Running build actions...";
			
			auto const logCommand([&](std::vector<std::string> const &args)
			{
				std::string command{path};
			
				for (auto const &arg : args)
				{
					command += ' ';
					command += arg;
				}
			
				log::msg("Karhu") << command;
			});
			
			namespace bp = boost::process;

			bp::environment env{boost::this_process::environment()};
			
			log::msg("Karhu") << "PATH = " << env["PATH"].to_string();
			
			if (!current && !argsOther.empty())
			{
				argsOther.insert
				(
					argsOther.begin(),
					argsBasic.begin(),
					argsBasic.end()
				);
				
				logCommand(argsOther);
				
				bp::child c
				{
					path,
					argsOther,
					env
					/// @todo: Figure out how to get the karhubuild output into the editor console; see: https://stackoverflow.com/questions/48657409/how-to-redirect-stdin-and-stdout-using-boost-process
				};
				
				try
				{
					c.wait();
				}
				catch (bp::process_error &e)
				{
					log::err("Karhu")
						<< error
						<< "wait for karhubuild threw: " << e.what();
					
					return false;
				}
				
				if (0 != c.exit_code())
				{
					log::err("Karhu")
						<< error
						<< "karhubuild encountered an error: "
						<< c.exit_code();
					
					return false;
				}
			}
			
			auto const
				port{settings.port};
			
			std::string const
				host{Server::hostname(port)};
			
			if (settings.run)
			{
				if (settings.online)
				{
					log::msg("Karhu")
						<< "Server "
						<< host
						<< ':'
						<< port
						<< "...";
					
					argsTarget.emplace_back("-host");
					argsTarget.emplace_back(host);
					argsTarget.emplace_back("-port");
					argsTarget.emplace_back(std::to_string(port));
				}
				else
				{
					log::msg("Karhu") << "Running offline without server";
					
					argsTarget.emplace_back("-offline");
				}
				
				/// @todo: Support -projdir in editor for non-current run targets?
				if (current)
				{
					argsTarget.emplace_back("-projdir");
					argsTarget.emplace_back(m_basepath);
				}
			}
			
			if (settings.run)
			{
				if (settings.manual)
					log::msg("Karhu") << "Manual app boot expected";
				else
				{
					log::msg("Karhu") << "Running app...";
				
					if (current)
					{
						logCommand(argsTarget);
						
						bp::child
						{
							path,
							argsTarget,
							env,
							bp::std_out > bp::null,
							bp::std_err > bp::null,
							bp::on_exit = [](int, std::error_code const &) {}
						}.detach();
					}
					else
					{
						argsRun.insert
						(
							argsRun.begin(),
							argsBasic.begin(),
							argsBasic.end()
						);
						
						/// @todo: Add custom args to editor build?
						argsRun.emplace_back("--args");
						argsRun.insert
						(
							argsRun.end(),
							std::make_move_iterator(argsTarget.begin()),
							std::make_move_iterator(argsTarget.end())
						);
						
						logCommand(argsRun);
						
						bp::child
						{
							path,
							argsRun,
							env,
							bp::std_out > bp::null,
							bp::std_err > bp::null,
							bp::on_exit = [](int, std::error_code const &) {}
							/// @todo: Figure out how to get the karhubuild output into the editor console; see: https://stackoverflow.com/questions/48657409/how-to-redirect-stdin-and-stdout-using-boost-process
						}.detach();
					}
				}
				
				if (settings.online)
				{
					log::msg("Karhu")
						<< "Starting up server on "
						<< host
						<< ':'
						<< port
						<< "...";
				
					m_server = std::make_unique<Server>(*m_app, *this, *m_project);
					m_state  = StateEditor::connecting;
					
					if (!m_server->connectAsync(pf, port))
					{
						log::err() << "Failed to connect to client";
						disconnect();
					}
				}
			}
			
			return true;
		}
		
		/// @todo: Should we actually keep track of the child process and kill it, in case the disconnect message can't get through?
		bool Editor::disconnect()
		{
			log::msg("Karhu") << "Disconnected";
			
			if (StateEditor::connected == m_state)
				emitEvent(EDisconnected{});
			
			m_state = StateEditor::normal;
			m_server.reset();
			
			return true;
		}
		
		void Editor::validateSettings(Platform const p)
		{
			m_platforms.settings[p]->valid =
			(
				m_platforms.settings[p]->clean      ||
				m_platforms.settings[p]->make       ||
				m_platforms.settings[p]->build      ||
				m_platforms.settings[p]->run        ||
				m_platforms.settings[p]->cleanKarhu ||
				m_platforms.settings[p]->makeKarhu  ||
				m_platforms.settings[p]->buildKarhu
			);
		}
		
		void Editor::refreshResource(res::IDResource const ID, res::IDTypeResource const type)
		{
			using P = karhu::network::Protocol;
			
			log::dbg("Karhu") << "Reloading resource " << ID;
			
			m_app->res().destroy(ID);
			
			if (m_app->res().IDOfType<res::Script>() == type)
			{
				// Reload to rebuild and check errors.
				m_app->res().get<res::Script>(ID);
				
				// Refresh any components.
				app::scriptutil::refreshScript(*m_app, ID);
			}
			
			if (m_server)
			{
				m_server->messenger().send
				(
					P::serialise
					(
						P::TypeMessage::debug,
						conv::JSON::Val{conv::JSON::Obj
						{
							{"e",    EventProtocol::resourceRefreshed},
							{"id",   ID},
							{"type", type}
						}}
					)
				);
			}
		}
		
		void Editor::refreshResources()
		{
			namespace fs    = boost::filesystem;
			namespace bp    = boost::process;
			namespace tools = SDK::paths::home::tools;
			
			auto p(SDK::env::home(tools::karhuproj::local()));
			
			if (!p || !fs::exists(*p))
			{
				log::err("Karhu") << "Failed to refresh resources: could not find karhuproj tool";
				return;
			}
			
			std::string line;
			std::vector<std::string> args, output;
			
			args.emplace_back("asset");
			args.emplace_back("--act");
			args.emplace_back("refresh");
			
			bp::ipstream pi;
			bp::child c{*p, args, bp::std_out > pi, bp::std_err > stderr};
			
			{log::msg("Karhu") << *p << ' ' << std::accumulate(args.begin(), args.end(), std::string{}, [](auto &a, auto &b) { return a + ' ' + b; });}

			while (c.running() && std::getline(pi, line) && !line.empty())
				output.emplace_back(line);
			
			//log::msg("DEBUG") << "boost child valid? " << c.valid();

			try
			{
				c.wait();
			}
			catch (bp::process_error &e)
			{
				log::err("Karhu") << "Failed to refresh resources: wait for karhuproj asset threw: " << e.what();
				return;
			}

			if (0 != c.exit_code())
			{
				log::err("Karhu") << "Failed to refresh resources: karhuproj asset failed: " << c.exit_code();
				return;
			}
			
			// Skip every other line and only get the lines with resource identifiers.
			for (std::size_t i{0}; i < output.size(); i += 4)
			{
				auto const
					ID(static_cast<res::IDResource>(std::atoi(output[i + 2].c_str())));
				
				auto const
					type(static_cast<res::IDTypeResource>(std::atoi(output[i + 3].c_str())));
				
				if (0 != ID)
				{
					//log::msg("Karhu") << "Refreshed resource " << ID;
					//m_app->res().destroy(ID);
					
					refreshResource(ID, type);
				}
			}
			
			// Refresh the database.
			/// @todo: Don't think it's actually necessary to reload the resource database in the editor.
/*
			auto const status(m_app->res().load());
 
			if (!status.success)
			{
				log::err("Karhu") << "Failed to load resources: " << status.error;
				return;
			}
*/
			// Refresh the editor entries.
			
//			auto const result(loadEntriesResources(*m_app));
//
//			if (!result.success)
//			{
//				log::err("Karhu") << "Failed to load resources: " << result.error;
//				return;
//			}
//
//			m_resources.data = std::move(result.value);
		}
		
		bool Editor::reloadResources()
		{
			auto r{loadEntriesResources(*m_app)};
			
			if (!r.success)
			{
				log::err("Karhu") << "Failed to load project resources: " << r.error;
				return false;
			}
			
			m_resources.data = std::move(r.value);
			
			return true;
		}
		
		Result<std::string> Editor::AdapterRead::readStringFromResource
		(
			const char *filepathRelativeToResourcepath
		)
		{
			namespace fs = boost::filesystem;
			
			if (auto const project = m_editor.project())
			{
				fs::path path{project->location().path()};
				path.append("editor");
				path.append(filepathRelativeToResourcepath);
				path.make_preferred();
				
				std::ifstream f{path.string()};
				
				if (f.is_open())
				{
					std::stringstream s;
					s << f.rdbuf();
					
					return {std::move(s.str()), true};
				}
			}
			
			std::stringstream s;
			s
				<< "Could not open file '"
				<< filepathRelativeToResourcepath
				<< "' for reading";
			
			return {{}, false, s.str()};
		}

		Result<Buffer<Byte>> Editor::AdapterRead::readBytesFromResource
		(
			const char *filepathRelativeToResourcepath
		)
		{
			namespace fs = boost::filesystem;
			
			const char *error{nullptr};
			
			if (auto const project = m_editor.project())
			{
				fs::path path{project->location().path()};
				path.append("editor");
				path.append(filepathRelativeToResourcepath);
				path.make_preferred();
				
				std::ifstream f{path.string(), std::ifstream::binary};
				
				if (f.is_open())
				{
					f.seekg(0, f.end);
					
					auto const size(static_cast<std::int64_t>(f.tellg()));
					
					if (size > 0)
					{
						Buffer<Byte> buffer;
						
						buffer.data = std::make_unique<Byte[]>(size);
						buffer.size = size;
						
						f.seekg(0, f.beg);
						
						bool success{false};
						
						try
						{
							f.read
							(
								reinterpret_cast<char *>(buffer.data.get()),
								static_cast<std::streamsize>(size)
							);
							
							success = true;
						}
						catch (std::ios_base::failure const &e)
						{
							error = e.what();
						}
						
						f.close();
					
						if (success)
							return {std::move(buffer), true};
					}
				}
			}
			
			std::stringstream s;
			s
				<< "Could not open file '"
				<< filepathRelativeToResourcepath
				<< "' for reading";
			
			if (error)
				s << ": " << error;
			
			return {{}, false, s.str()};
		}
	}
}

#endif
