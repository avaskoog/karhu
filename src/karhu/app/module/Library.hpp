/**
 * @author		Ava Skoog
 * @date		2017-09-18
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_MODULE_LIBRARY_H_
	#define KARHU_MODULE_LIBRARY_H_

	#include <string>

	namespace karhu
	{
		namespace module
		{
			class Library
			{
				public:
					bool load(const std::string &path)
					{
						return (m_loaded = performLoad(path));
					}
				
					void unload()
					{
						m_loaded = false;
						performUnload();
					}

					void *function(const std::string &function)
					{
						if (!m_loaded) return nullptr;
						return performGetFunction(function);
					}

					bool loaded() const noexcept { return m_loaded; }

					virtual ~Library() = default;

				protected:
					virtual bool performLoad(const std::string &path) = 0;
					virtual void performUnload() = 0;
					virtual void *performGetFunction(const std::string &function) = 0;
				
				private:
					bool m_loaded{false};
			};
		}
	}
#endif
