/**
 * @author		Ava Skoog
 * @date		2017-09-18
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_MODULE_POOL_H_
	#define KARHU_MODULE_POOL_H_

	#include <karhu/app/module/Modular.hpp>
	#include <karhu/core/log.hpp>
	#include <karhu/core/meta.hpp>

	#include <type_traits>
	#include <functional>
	#include <memory>
	#include <string>
	#include <vector>
	#include <map>

	namespace karhu
	{
		namespace module
		{
			class Pool
			{
				public:
					struct DataModule
					{
						std::unique_ptr<Modular> module;
						/// @todo: Add serialised data.
					};

				public:
					template<class T>
					static constexpr std::enable_if_t<meta::derived<Modular, T>(), bool> supports() noexcept
					{
						return true;
					}
				
					template<class T>
					static constexpr std::enable_if_t<!meta::derived<Modular, T>(), bool> supports() noexcept
					{
						return false;
					}

				private:
					using Factory = std::function<DataModule()>;
					using Modules = std::vector<std::unique_ptr<DataModule>>;

				public:
					void callDestructorsBeforeReload();

					template<typename TDerived>
					bool registerOrUpdateDerivedType(const std::string &name)
					{
						if (!supports<TDerived>())
						{
							log::err("Karhu")
								<< "Failed to register derived modular type '" << name
								<< "'; not derived from pool's modular base class";
							return false;
						}

						Factory f([]()
						{
							return DataModule
							{
								std::make_unique<TDerived>(),
								/// @todo: Add any extra params later.
							};
						});

						auto it(m_factories.find(name));
						if (it == m_factories.end())
							m_factories.emplace(name, std::move(f));
						else
						{
							it->second = std::move(f);

							auto jt(m_modules.find(name));
							if (jt != m_modules.end())
							{
								for (auto &m : jt->second)
								{
									auto n(f());
									/// @todo: Deserialise the data.
									m->module = std::move(n.module);
								}
							}
						}
						
						return true;
					}

					DataModule *create(const std::string &type);
					void destroy(const Modular &module);

				private:
					std::map<std::string, Modules> m_modules;
					std::map<std::string, Factory> m_factories;
				
				friend class WrapperModule;
			};
		}
	}
#endif
