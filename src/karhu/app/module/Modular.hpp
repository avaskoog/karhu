/**
 * @author		Ava Skoog
 * @date		2017-09-18
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_MODULE_MODULAR_H_
	#define KARHU_MODULE_MODULAR_H_

	namespace karhu
	{
		namespace module
		{
			class Modular
			{
				public:
					virtual void dothing() = 0;
					virtual ~Modular() = default;

				protected:
					Modular() = default;
			};
		}
	}
#endif
