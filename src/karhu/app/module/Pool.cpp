/**
 * @author		Ava Skoog
 * @date		2017-09-18
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/module/Pool.hpp>
#include <karhu/core/log.hpp>

#include <algorithm>

namespace karhu
{
	namespace module
	{
		void Pool::callDestructorsBeforeReload()
		{
			for (auto &it : m_factories)
				it.second = {};

			for (auto &it : m_modules)
			{
				for (auto &jt : it.second)
				{
					/// @todo: Store the serialised data.
					jt->module.reset();
				}
			}
		}

		Pool::DataModule *Pool::create(const std::string &type)
		{
			const auto f(m_factories.find(type));
			if (f == m_factories.end())
			{
				log::err("Karhu")
					<< "Error creating module of type '" << type
					<< "': no such type registered in pool";
				return nullptr;
			}

			auto m(f->second());
			if (!m.module)
			{
				log::err("Karhu")
					<< "Error creating module of type '" << type
					<< "': construction failed";
				return nullptr;
			}

			auto it(m_modules.find(type));
			if (it == m_modules.end())
				it = m_modules.emplace(type, Modules{}).first;

			it->second.emplace_back(std::make_unique<DataModule>(std::move(m)));
			return it->second.back().get();
		}

		void Pool::destroy(const Modular &module)
		{
			/// @todo: Optimise this?
			for (auto &list : m_modules)
			{
				const auto it(std::find_if(list.second.begin(), list.second.end(), [&module](const auto &v)
				{
					return (&module == v->module.get());
				}));
				
				if (it != list.second.end())
					list.second.erase(it);
			}
		}
	}
}
