/**
 * @author		Ava Skoog
 * @date		2017-09-16
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/module/SubsystemModule.hpp>
#include <karhu/app/App.hpp> /// @todo: Try to decouple this.
#include <karhu/core/log.hpp> /// @todo: Try to decouple this.

namespace karhu
{
	namespace module
	{
		Pool *SubsystemModule::createPool(const std::string &name)
		{
			auto it(m_pools.emplace(name, std::make_unique<Pool>()));
			return it.first->second.get();
		}
		
		Pool *SubsystemModule::getPool(const std::string &name)
		{
			auto it(m_pools.find(name));
			if (it != m_pools.end()) return it->second.get();
			return nullptr;
		}

		Library *SubsystemModule::reloadLibrary(const std::string &path)
		{
			auto it(m_libraries.find(path));
			if (it == m_libraries.end())
			{
				auto lib(performCreateLibrary());
				if (!lib)
				{
					log::err("Karhu") << "Failed to load dynamic library: construction failed";
					return nullptr;
				}

				it = m_libraries.emplace(path, std::move(lib)).first;
			}
			
			it->second->unload();
			if (!it->second->load(path))
			{
				log::err("Karhu") << "Failed to update pool from library: loading failed";
				return nullptr;
			}

			return it->second.get();
		}

		bool SubsystemModule::updatePoolFromLibrary(Pool &pool, const std::string &path)
		{
			// This has to be done first to avoid crashing.
			pool.callDestructorsBeforeReload();

			auto lib(reloadLibrary(path));
			if (!lib) return false;

			// Try to find the function to set the app instance.
			const auto nameAppInstance("karhu_set_app_instance");
			auto appInstance(reinterpret_cast<void (*)(app::App *const)>(lib->function(nameAppInstance)));
			if (!appInstance)
			{
				log::err("Karhu")
					<< "Failed to update pool from library: function '"
					<< nameAppInstance << "' not found";
				return false;
			}
			else
				appInstance(app::App::instance());
			
			// Failing to set the logger implementation is bad but not enough for failure.
			const auto nameLoggerImpl("karhu_set_logger_implementation");
			auto loggerImpl(reinterpret_cast<void (*)(log::Implementation *const)>(lib->function(nameLoggerImpl)));
			if (!loggerImpl)
			{
				log::wrn("Karhu")
					<< "Failed to connect pool to logger: function '"
					<< nameLoggerImpl << "' not found";
			}
			else
				// Update the logger implementation.
				loggerImpl(log::Logger::implementation());
			
			// Try to find the function to update the pool.
			const auto nameUpdatePool("karhu_module_update_pool");
			auto updatePool(reinterpret_cast<void (*)(Pool &)>(lib->function(nameUpdatePool)));
			if (!updatePool)
			{
				log::err("Karhu")
					<< "Failed to update pool from library: function '"
					<< nameUpdatePool << "' not found";
				return false;
			}
			
			// Update the pool.
			updatePool(pool);

			return true;
		}
	}
}
