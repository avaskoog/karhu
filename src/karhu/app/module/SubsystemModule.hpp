/**
 * @author		Ava Skoog
 * @date		2017-09-16
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_MODULE_SUBSYSTEM_MODULE_H_
	#define KARHU_MODULE_SUBSYSTEM_MODULE_H_

	#include <karhu/app/module/Pool.hpp>
	#include <karhu/app/module/Library.hpp>

	#include <string>
	#include <vector>
	#include <memory>

	namespace karhu
	{
		namespace module
		{
			class SubsystemModule
			{
				public:
					Pool *createPool(const std::string &name);
					Pool *getPool(const std::string &name);

					std::map<std::string, std::unique_ptr<Pool>> &pools() { return m_pools; }

					bool updatePoolFromLibrary(Pool &pool, const std::string &path);

					virtual ~SubsystemModule() = default;

				protected:
					virtual std::unique_ptr<Library> performCreateLibrary() = 0;
				
				private:
					Library *reloadLibrary(const std::string &path);

				private:
					std::map<std::string, std::unique_ptr<Library>> m_libraries;
					std::map<std::string, std::unique_ptr<Pool>> m_pools;
			};
		}
	}
#endif
