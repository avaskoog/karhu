/**
 * @author		Ava Skoog
 * @date		2019-02-28
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_NAVIGATION_COMMON_H_
	#define KARHU_NAVIGATION_COMMON_H_

	#include <limits>
	#include <type_traits>
	#include <cstdint>

	namespace karhu
	{
		namespace nav
		{
			using Area = unsigned short; // To match Detour.
			
			struct Areas
			{
				enum : Area
				{
					none    = 0,
					invalid = 1 << 0,
					walk    = 1 << 1,
					door    = 1 << 2,
					swim    = 1 << 3,
					jump    = 1 << 4,
					climb   = 1 << 5,
					custom  = 1 << 6,
					all     = std::numeric_limits<Area>::max()
				};
			};
			
			inline constexpr Area area(const std::size_t &index) noexcept
			{
				return static_cast<Area>(Areas::custom << index);
			}
		}
	}
#endif
