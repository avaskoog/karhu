/**
 * @author		Ava Skoog
 * @date		2019-02-28
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_NAVIGATION_SUBSYSTEM_NAVMESH_H_
	#define KARHU_NAVIGATION_SUBSYSTEM_NAVMESH_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/app/nav/common.hpp>
	#include <karhu/conv/graphics.hpp>
	#include <karhu/conv/maths.hpp>
	#include <karhu/core/result.hpp>

	#include <vector>
	#include <memory>

	namespace karhu
	{
		namespace nav
		{
			/**
			 * A navmesh to set up with geometry data partioned by area
			 * and settings for baking a valid instance to find paths on.
			 * There are sensible default settings so they need only be
			 * customised if desired; only geometry needs to be supplied.
			 */
			class Navmesh : public res::Resource
			{
				public:
					Navmesh();
					
					bool addGeometry
					(
						Area                    const  area,
						std::vector<mth::Vec3f> const &vertices,
						std::vector<gfx::Index> const &indices
					);
				
					void clearGeometries();
				
					std::vector<mth::Vec3f> path
					(
						mth::Vec3f const &from,
						mth::Vec3f const &to,
						Area       const  filter = Areas::all
					) const;
				
					void draw();
					void update(float dt); /// @todo: Temporary navmesh crowd test
				
					bool bake();
					void clean();
					bool valid() const { return m_valid; }
					/// @todo: reset()?
				
					~Navmesh();
				
				private:
					bool bakeQuery();
					void cleanRC();
				
				protected:
					bool hasMetadata() const noexcept override { return false; }
					Status performInit(std::vector<std::string> const &/*extraPathsRelativeToSourcePath*/) override { return {true}; }
				
					Status performLoadInAdditionToMetadata
					(
						const conv::JSON::Obj *const metadata,
						const char *filepathRelativeToResourcepath,
						const char *suffix,
						conv::AdapterFilesystemRead &filesystem
					) override;
				
					Status performSaveInAdditionToMetadata
					(
						conv::JSON::Obj *const metadata,
						char const *filepathRelativeToResourcepath,
						char const *suffix,
						conv::AdapterFilesystemWrite &filesystem
					) override;
				
					void performSerialise(conv::Serialiser &) const override {}
					void performDeserialise(const conv::Serialiser &) override {}
				
				public:
					float
						limitHeight  {2.0f},
						limitDiameter{0.5f},
						limitSlope   {0.05f},
						sizeTile     {32.0f};
				
					mth::Radsf
						limitAngleSlope{mth::pif() * 0.25f}; // 45 degrees
				
				private:
					struct Data;
				
					struct Geometrydata
					{
						Area               area;
						std::vector<float> vertices;
						std::vector<int>   indices;
					};
					
				private:
					bool m_valid{false};
					std::vector<Geometrydata> m_geometries;
					std::unique_ptr<Data> m_data;
			};
		}
	}
#endif
