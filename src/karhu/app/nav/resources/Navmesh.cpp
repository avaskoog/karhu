/**
 * @author		Ava Skoog
 * @date		2019-02-28
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Look into navmesh agent avoidance / crowds; see http://www.stevefsp.org/projects/rcndoc/prod/group__crowd.html

#include <karhu/app/nav/resources/Navmesh.hpp>
#include <karhu/core/log.hpp>
#include <karhu/core/debugging.hpp>

#include <karhu/app/lib/recastnavigation/Recast/Include/Recast.h>
#include <karhu/app/lib/recastnavigation/Detour/Include/DetourCommon.h>
#include <karhu/app/lib/recastnavigation/Detour/Include/DetourNavMesh.h>
#include <karhu/app/lib/recastnavigation/Detour/Include/DetourNavMeshBuilder.h>
#include <karhu/app/lib/recastnavigation/Detour/Include/DetourNavMeshQuery.h>
#include <karhu/app/lib/recastnavigation/DetourCrowd/Include/DetourCrowd.h>

#include <karhu/app/lib/im3d/im3d.h>
#include <karhu/app/lib/im3d/im3d_math.h>

/// @todo: Very temporary, see load method
#include <karhu/app/App.hpp>
#include <karhu/app/fio/SubsystemFile.hpp>

namespace
{
	struct HeaderNavmesh
	{
		dtNavMeshParams
			params;
		
		std::uint8_t
			version,
			magic,
			tilecount;
	};

	struct HeaderTile
	{
		dtTileRef
			ref;
		
		std::uint64_t
			size;
	};
	
	static std::vector<int> s_agents; /// @todo: Temporary navmesh crowd test
}

namespace karhu
{
	namespace nav
	{
		// Recast context needs to be derived and virtual method doLog()
		// implemented in order for us to display its error messages.
		struct Context : public rcContext
		{
			public:
				std::string getAndFlushMessageLogged()
				{
					std::string const m{m_messageLogged};
					m_messageLogged.clear();
					return m;
				}
			
			protected:
				void doLog
				(
					rcLogCategory const /*category*/,
					char const *msg,
					int const /*len*/
				) override
				{
					m_messageLogged = msg;
				}
			
			private:
				std::string m_messageLogged;
		};
		
		struct Navmesh::Data
		{
			Context context;
			
			rcHeightfield        *solid     {nullptr};
			rcCompactHeightfield *compact   {nullptr};
			rcContourSet         *contours  {nullptr};
			rcPolyMesh           *polymesh  {nullptr};
			rcPolyMeshDetail     *detailmesh{nullptr}; /// @todo: Do we need this?
			
			dtQueryFilter filter;
			
			dtNavMesh *navmesh{nullptr};
			dtNavMeshQuery *query{nullptr};
			
			/// @todo: These seem to be optional; figure out if we need them at all.
//			static constexpr int MAX_OFFMESH_CONNECTIONS = 256;
//			float offMeshConVerts[MAX_OFFMESH_CONNECTIONS*3*2];
//			float offMeshConRads[MAX_OFFMESH_CONNECTIONS];
//			unsigned char offMeshConDirs[MAX_OFFMESH_CONNECTIONS];
//			unsigned char offMeshConAreas[MAX_OFFMESH_CONNECTIONS];
//			unsigned short offMeshConFlags[MAX_OFFMESH_CONNECTIONS];
//			unsigned int offMeshConId[MAX_OFFMESH_CONNECTIONS];
			
			float extents[3];
			
			dtCrowd crowd;
		};
		
		Navmesh::Navmesh()
		:
		m_data{std::make_unique<Data>()}
		{
			// Set up the query filter with defaults.
			/// @todo: Also consider custom areas.
			/// @todo: Should these be user-customisable?
			m_data->filter.setAreaCost(Areas::walk,  1.0f);
			m_data->filter.setAreaCost(Areas::door,  1.5f);
			m_data->filter.setAreaCost(Areas::swim,  5.0f);
			m_data->filter.setAreaCost(Areas::jump,  2.0f);
			m_data->filter.setAreaCost(Areas::climb, 5.0f);
		}
		
		bool Navmesh::addGeometry
		(
			Area                    const  area,
			std::vector<mth::Vec3f> const &vertices,
			std::vector<gfx::Index> const &indices
		)
		{
			/// @todo: Prevent adding to baked for now.
			if (valid())
			{
				log::err("Karhu") << "Cannot add geometry to baked navmesh";
				return false;
			}
			
			if (0 == vertices.size())
			{
				log::err("Karhu") << "Cannot add vertices with count of zero to navmesh";
				return false;
			}
		
			if (0 == vertices.size() || 0 != indices.size() % 3)
			{
				log::err("Karhu") << "Cannot add indices with count of zero or not divisible by three to navmesh";
				return false;
			}
			
			std::vector<float> vs(vertices.size() * 3);
			std::vector<int>   is(indices.size());
			
			for (std::size_t i{0}; i < vertices.size(); ++ i)
			{
				vs[i * 3 + 0] = vertices[i].x;
				vs[i * 3 + 1] = vertices[i].y;
				vs[i * 3 + 2] = vertices[i].z;
			}
			
			for (std::size_t i{0}; i < indices.size(); ++ i)
				is[i] = static_cast<int>(indices[i]);
			
			m_geometries.emplace_back
			(
				Geometrydata
				{
					area,
					std::move(vs),
					std::move(is)
				}
			);
			
			return true;
		}
		
		void Navmesh::clearGeometries()
		{
			m_geometries.clear();
		}
		
		/// @todo: Rewrite this to only depend on data->navmesh since there will be no polymesh if loaded from file.
		void Navmesh::draw()
		{
			if (!valid())
				return;
				
			Im3d::PushDrawState();
			Im3d::PushMatrix();
			
			dtNavMesh const *const mesh{m_data->navmesh};
			
			for (int i = 0; i < mesh->getMaxTiles(); ++ i)
			{
				const dtMeshTile* tile = mesh->getTile(i);
				
				if (!tile || !tile->header || !tile->dataSize)
					continue;
				
				for (int j = 0; j < tile->header->polyCount; ++ j)
				{
					dtPoly const *const poly{tile->polys + j};
					
					for (int k = 0; k < poly->vertCount; ++ k)
					{
						const int curr(poly->verts[k]);
						const int next(poly->verts[((k + 1) == poly->vertCount) ? 0 : (k + 1)]);
						
						const auto va(tile->verts + curr * 3);
						const auto vb(tile->verts + next * 3);
					
						const auto ax{va + 0};
						const auto ay{va + 1};
						const auto az{va + 2};
						
						const auto bx{vb + 0};
						const auto by{vb + 1};
						const auto bz{vb + 2};
						
						Im3d::Color col;
						
						if (RC_NULL_AREA == poly->getArea())
							col = {1.0f, 0.5f, 0.0f, 0.75f};
						else if (0 == (tile->header->x + tile->header->y) % 2)
							col = {1.0f, 1.0f, 1.0f, 0.75f};
						else
							col = {0.25f, 0.25f, 0.25f, 0.75f};
						
						Im3d::DrawLine
						(
							{*ax, *ay, *az},
							{*bx, *by, *bz},
							4.0f,
						 	col
						);
					}
				}
			}
			/*
			/// @todo: Temporary navmesh crowd test
			for (std::size_t i = 0; i < s_agents.size(); ++ i)
			{
				auto agent(m_data->crowd.getAgent(s_agents[i]));
				
				Im3d::Vec3 a, b;
				a.x = agent->npos[0];
				a.y = agent->npos[1];
				a.z = agent->npos[2];
				b = a;
				b.y += 2.0f;
				
				Im3d::SetSize(6.0f);
				
				if (0 == i)
					Im3d::SetColor({1.0f, 0.0f, 0.0f, 1.0f});
				else if (1 == i)
					Im3d::SetColor({0.0f, 1.0f, 0.0f, 1.0f});
				else
					Im3d::SetColor({0.0f, 0.0f, 1.0f, 1.0f});
				
				Im3d::DrawCylinder(a, b, limitDiameter * 0.5f);
			}
			*/
			Im3d::PopMatrix();
			Im3d::PopDrawState();
		}

		bool Navmesh::bake()
		{
			if (valid())
				clean();
			
			m_data->context.enableLog(true);
			
			// For path queries.
			/// @todo: Diameter should be loaded from the file too.
			m_data->extents[0] =
			m_data->extents[1] =
			m_data->extents[2] = limitDiameter;
			
			dtStatus status;

			// Set up the configuration.
			
			// Horizontal and vertical voxel density.
			constexpr float const
				dh{0.1f},
				dv{0.1f};
			
			rcConfig config;
			
			std::memset(&config, 0, sizeof(config));
			
			config.cs = dh;
			config.ch = dv;
			
			// These are the customisable settings.
			config.walkableHeight     = mth::max(1, static_cast<int>(mth::round(limitHeight / dv)));
			config.walkableRadius     = mth::max(1, static_cast<int>(mth::round((limitDiameter * 0.5f) / dh)));
			config.walkableClimb      = mth::max(1, static_cast<int>(mth::round(mth::min(0.1f, limitSlope) / dv)));
			config.walkableSlopeAngle = mth::clamp(mth::degrees(limitAngleSlope), 0.0f, 90.0f);
			
			// We only want triangles.
			config.maxVertsPerPoly = 3;
			
			// Setting the rest to sensible defaults. Magic numbers.
			/// @todo: Play with these. Read up properly on each. Make decisions.
			config.maxEdgeLen             = static_cast<int>(mth::round(128.0f / dh));
			config.maxSimplificationError = 0.1f;
			config.detailSampleDist       = 6.0f * dh;
			config.detailSampleMaxError   = 0.25f * dv;
			config.minRegionArea          = static_cast<int>(mth::round(rcSqrt(50.0f)));
			config.mergeRegionArea        = static_cast<int>(mth::round(rcSqrt(20.0f)));
			
			config.tileSize   = static_cast<int>(mth::round(mth::max(1.0f, sizeTile) / dh));
			config.borderSize = config.walkableRadius + 3;
			config.width      =
			config.height     = config.tileSize + config.borderSize * 2;
			
			// These are going to be filled in when processing geometry.
			std::memset(&config.bmin, 0, sizeof(config.bmin));
			std::memset(&config.bmax, 0, sizeof(config.bmax));
			
			// Pack all the geometries together.
			
			std::vector<float> vertices;
			std::vector<int>   indices;
			
			for (std::size_t i{0}, offset{0}, max{0}; i < m_geometries.size(); ++ i)
			{
				if (Areas::invalid == m_geometries[i].area)
					continue;
				
				indices.reserve(indices.size() + m_geometries[i].indices.size());
				indices.insert(indices.end(), m_geometries[i].indices.begin(), m_geometries[i].indices.end());
				
				for (std::size_t j{offset}; j < offset + m_geometries[i].indices.size(); ++ j)
					indices[j] += max;

				vertices.reserve(vertices.size() + m_geometries[i].vertices.size());
				vertices.insert(vertices.end(), m_geometries[i].vertices.begin(), m_geometries[i].vertices.end());
				
				offset += m_geometries[i].indices.size();
				max += m_geometries[i].vertices.size() / 3;
			}
			
			// Calculate bounds.
			for (std::size_t i{0}; i < vertices.size(); i += 3)
			{
				float const
					&x{vertices[i + 0]},
					&y{vertices[i + 1]},
					&z{vertices[i + 2]};
				
				if (x < config.bmin[0]) config.bmin[0] = x;
				if (y < config.bmin[1]) config.bmin[1] = y;
				if (z < config.bmin[2]) config.bmin[2] = z;
			
				if (x > config.bmax[0]) config.bmax[0] = x;
				if (y > config.bmax[1]) config.bmax[1] = y;
				if (z > config.bmax[2]) config.bmax[2] = z;
			}
			
			float const
				minX      {config.bmin[0]},
				minZ      {config.bmin[2]},
				maxX      {config.bmax[0]},
				maxZ      {config.bmax[2]},
				sizeBorder{static_cast<float>(config.borderSize) * dh},
				sizeTileXZ{static_cast<float>(config.tileSize) * dh * 0.5f},
				sizeWorldX{maxX - minX},
				sizeWorldZ{maxZ - minZ};
			
			std::size_t const
				countTilesX{static_cast<std::size_t>(mth::ceil(sizeWorldX / sizeTileXZ))},
				countTilesZ{static_cast<std::size_t>(mth::ceil(sizeWorldZ / sizeTileXZ))};
			
			if (0 == countTilesX || 0 == countTilesZ)
			{
				log::err("Karhu") << "Navmesh width or depth is zero";
				return false;
			}
			
			m_data->navmesh = dtAllocNavMesh();
					
			if (!m_data->navmesh)
			{
				log::err("Karhu") << "Failed to allocate data for navmesh";
				return false;
			}
			
			/// @todo: This, and addTile() below, seems to be all that's needed to make it work with tiles instead, but do clean it up and then figure out how to stitch tiles together.
			dtNavMeshParams paramsNavmesh;
			rcVcopy(paramsNavmesh.orig, &config.bmin[0]);
			paramsNavmesh.tileWidth  = sizeTileXZ;
			paramsNavmesh.tileHeight = sizeTileXZ;
			paramsNavmesh.maxTiles   = 100; /// @todo: Figure out good navmesh max tile limit or actually use countTilesX*countTilesZ.
			paramsNavmesh.maxPolys   = 9999; /// @todo: Figure out good navmesh max poly limit or actually figure out how many it needs.
			
			auto const statusToString([](dtStatus s) -> std::string
			{
				std::stringstream ss;
				
				if (s & DT_WRONG_MAGIC)
					ss << "DT_WRONG_MAGIC ";
				
				if (s & DT_WRONG_VERSION)
					ss << "DT_WRONG_VERSION ";
				
				if (s & DT_OUT_OF_MEMORY)
					ss << "DT_OUT_OF_MEMORY ";
				
				if (s & DT_INVALID_PARAM)
					ss << "DT_INVALID_PARAM ";
				
				if (s & DT_BUFFER_TOO_SMALL)
					ss << "DT_BUFFER_TOO_SMALL ";
				
				if (s & DT_OUT_OF_NODES)
					ss << "DT_OUT_OF_NODES ";
				
				if (s & DT_PARTIAL_RESULT)
					ss << "DT_PARTIAL_RESULT ";
				
				if (s & DT_ALREADY_OCCUPIED)
					ss << "DT_ALREADY_OCCUPIED ";
				
				return ss.str();
			});
			
			status = m_data->navmesh->init(&paramsNavmesh);
			
			if (dtStatusFailed(status))
			{
				log::err("Karhu") << "Failed to initialise path mesh for navmesh: " << statusToString(status);
				return false;
			}
			
			log::msg("Karhu") << "Building " << countTilesX << " x " << countTilesZ << " square navmesh tiles with a side of " << (sizeTileXZ * 2.0f) << " world units...";
			
			for (std::size_t z{0}; z < countTilesZ; ++ z)
			{
				for (std::size_t x{0}; x < countTilesX; ++ x)
				{
					cleanRC();
					
					log::msg("Karhu") << "Building tile at " << x << " x " << z << "...";
					
					config.bmin[0] = minX + static_cast<float>(x) * sizeTileXZ - sizeBorder;
					config.bmin[2] = minZ + static_cast<float>(z) * sizeTileXZ - sizeBorder;
					
					config.bmax[0] = minX + static_cast<float>(x + 1) * sizeTileXZ + sizeBorder;
					config.bmax[2] = minZ + static_cast<float>(z + 1) * sizeTileXZ + sizeBorder;
					
					// Fill in configuration's width and height.
					rcCalcGridSize
					(
						config.bmin,
						config.bmax,
						config.cs,
						&config.width,
						&config.height
					);
					
					m_data->solid = rcAllocHeightfield();
					
					if (!m_data->solid || !rcCreateHeightfield
					(
						&m_data->context,
						*m_data->solid,
						config.width,
						config.height,
						config.bmin,
						config.bmax,
						config.cs,
						config.ch
					))
					{
						log::err("Karhu") << "Failed to create solid heightfield for navmesh: " << m_data->context.getAndFlushMessageLogged();
						return false;
					}
					
					// Mark walkable triangles based on settings, filling in the areas.
					
					std::vector<unsigned char> areas(indices.size(), RC_NULL_AREA);
					
					rcMarkWalkableTriangles
					(
						&m_data->context,
						config.walkableSlopeAngle,
						&vertices[0],
						static_cast<int>(vertices.size() / 3),
						&indices[0],
						static_cast<int>(indices.size() / 3),
						&areas[0]
					);
					
					// Set correct areas.
					for (std::size_t i{0}, offset{0}; i < m_geometries.size(); ++ i)
					{
						if (Areas::invalid == m_geometries[i].area)
							continue;
					
						for (std::size_t j{offset}; j < offset + m_geometries[i].indices.size(); ++ j)
							areas[j] = static_cast<unsigned char>(m_geometries[i].area);
						
						offset += m_geometries[i].indices.size();
					}
					
					// Figure out the final geometry.
					rcRasterizeTriangles
					(
						&m_data->context,
						&vertices[0],
						static_cast<int>(vertices.size() / 3),
						&indices[0],
						&areas[0],
						static_cast<int>(indices.size() / 3),
						*m_data->solid
					);
					
					// Filter walkable surfaces.
					
					rcFilterLowHangingWalkableObstacles
					(
						&m_data->context,
						config.walkableClimb,
						*m_data->solid
					);
					
					rcFilterLedgeSpans
					(
						&m_data->context,
						config.walkableHeight,
						config.walkableClimb,
						*m_data->solid
					);
					
					rcFilterWalkableLowHeightSpans
					(
						&m_data->context,
						config.walkableClimb,
						*m_data->solid
					);
					
					// Carry out all that voxel magic to build the final mesh.
					
					m_data->compact = rcAllocCompactHeightfield();
					
					if (!m_data->compact || !rcBuildCompactHeightfield
					(
						&m_data->context,
						config.walkableHeight,
						config.walkableClimb,
						*m_data->solid,
						*m_data->compact
					))
					{
						log::err("Karhu") << "Failed to create compact heightfield for navmesh: " << m_data->context.getAndFlushMessageLogged();
						return false;
					}
					
					/// @todo: Quick and dirty hull adapted from the recast demo.
					// Returns true if 'c' is left of line 'a'-'b'.
					auto const left([](const float* a, const float* b, const float* c) -> bool
					{
						const float u1 = b[0] - a[0];
						const float v1 = b[2] - a[2];
						const float u2 = c[0] - a[0];
						const float v2 = c[2] - a[2];
						return u1 * v2 - v1 * u2 < 0;
					});

					// Returns true if 'a' is more lower-left than 'b'.
					auto const cmppt([](const float* a, const float* b) -> bool
					{
						if (a[0] < b[0]) return true;
						if (a[0] > b[0]) return false;
						if (a[2] < b[2]) return true;
						if (a[2] > b[2]) return false;
						return false;
					});
					// Calculates convex hull on xz-plane of points on 'pts',
					// stores the indices of the resulting hull in 'out' and
					// returns number of points on hull.
					auto const convexhull([&left, &cmppt](const float* pts, int npts, std::vector<gfx::Index> &out)
					{
						// Find lower-leftmost point.
						int hull = 0;
						for (int i = 1; i < npts; ++i)
							if (cmppt(&pts[i*3], &pts[hull*3]))
								hull = i;
						// Gift wrap hull.
						int endpt = 0;
						int i = 0;
						do
						{
							out.resize(++ i);
							out[i] = hull;
							endpt = 0;
							for (int j = 1; j < npts; ++j)
								if (hull == endpt || left(&pts[hull*3], &pts[endpt*3], &pts[j*3]))
									endpt = j;
							hull = endpt;
						}
						while (endpt != out[0]);
					});
					
					// Set correct areas.
					for (std::size_t i{0}; i < m_geometries.size(); ++ i)
					{
						if (Areas::invalid == m_geometries[i].area)
						{
							float
								hmin{0.0f}, hmax{0.0f};
							
							for (std::size_t j{0}; j < m_geometries[i].vertices.size(); j += 3)
							{
								float const
									y{m_geometries[i].vertices[j + 1]};
								
								if (y < hmin) hmin = y;
								if (y > hmax) hmax = y;
							}
							/*
							std::vector<gfx::Index> indicesHull;
							convexhull(&m_geometries[i].vertices[0], static_cast<int>(m_geometries[i].vertices.size() / 3), indicesHull);
							std::vector<float> verticesHull(indicesHull.size() * 3);

							for (std::size_t j{0}; j < indicesHull.size(); ++ j)
							{
								gfx::Index const index{indicesHull[j]};
								verticesHull[j * 3 + 0] = m_geometries[i].vertices[index + 0];
								verticesHull[j * 3 + 1] = m_geometries[i].vertices[index + 1];
								verticesHull[j * 3 + 2] = m_geometries[i].vertices[index + 2];
							}*/
							auto &verticesHull(m_geometries[i].vertices);
							
		//					log::msg("DEBUG") << "rcMarkConvexPolyArea, " << indicesHull.size() <<
		//					"indices, hmin " << hmin << ", hmax " << hmax;
							
							rcMarkConvexPolyArea
							(
								&m_data->context,
								&verticesHull[0],
								static_cast<int>(verticesHull.size() / 3),
								hmin,
								hmax,
								RC_NULL_AREA,
								*m_data->compact
							);
						}
					}
				
					if (!rcErodeWalkableArea
					(
						&m_data->context,
						config.walkableRadius,
						*m_data->compact
					))
					{
						log::err("Karhu") << "Failed to erode walkable area for navmesh: " << m_data->context.getAndFlushMessageLogged();
						return false;
					}
				
					if (!rcBuildDistanceField(&m_data->context, *m_data->compact))
					{
						log::err("Karhu") << "Failed to build distance field for navmesh: " << m_data->context.getAndFlushMessageLogged();
						return false;
					}
				
					if (!rcBuildRegions
					(
						&m_data->context,
						*m_data->compact,
						config.borderSize,
						config.minRegionArea,
						config.mergeRegionArea
					))
					{
						log::err("Karhu") << "Failed to build regions for navmesh: " << m_data->context.getAndFlushMessageLogged();
						return false;
					}
					
					m_data->contours = rcAllocContourSet();
				
					if (!m_data->contours || !rcBuildContours
					(
						&m_data->context,
						*m_data->compact,
						config.maxSimplificationError,
						config.maxEdgeLen,
						*m_data->contours
					))
					{
						log::err("Karhu") << "Failed to build contours for navmesh: " << m_data->context.getAndFlushMessageLogged();
						return false;
					}
					
					m_data->polymesh = rcAllocPolyMesh();
				
					if (!m_data->polymesh || !rcBuildPolyMesh
					(
						&m_data->context,
						*m_data->contours,
						config.maxVertsPerPoly,
						*m_data->polymesh
					))
					{
						log::err("Karhu") << "Failed to build polymesh for navmesh: " << m_data->context.getAndFlushMessageLogged();
						return false;
					}
					
					log::msg("Karhu") << "Built polymesh with " << m_data->polymesh->npolys << " polygons";
					
					if (0 == m_data->polymesh->npolys)
					{
						log::msg("Karhu") << "Ignoring empty tile";
						continue;
					}
					
					m_data->detailmesh = rcAllocPolyMeshDetail();
				
					if (!m_data->detailmesh || !rcBuildPolyMeshDetail
					(
						&m_data->context,
						*m_data->polymesh,
						*m_data->compact,
						config.detailSampleDist,
						config.detailSampleMaxError,
						*m_data->detailmesh
					))
					{
						log::err("Karhu") << "Failed to build detail mesh for navmesh: " << m_data->context.getAndFlushMessageLogged();
						return false;
					}
					
					log::msg("Karhu") << "Built detailmesh with " << m_data->detailmesh->nmeshes << " meshes";
					
					if (0 == m_data->detailmesh->nmeshes)
					{
						log::msg("Karhu") << "Ignoring empty tile";
						continue;
					}
					
					// Set up all the areas.
					// Recast docs say to use maxpolys rather than npolys but that causes
					// an overflow, and the official Recast demo uses npolys, so that seems
					// to be the actually correct value to use.
					for (int i{0}; i < m_data->polymesh->npolys; ++ i)
						m_data->polymesh->flags[i] = m_data->polymesh->areas[i];
					
					// Time to set up things for detour so that paths can be found.
					
					dtNavMeshCreateParams params;
					std::memset(&params, 0, sizeof(params));
					
					params.cs = config.cs;
					params.ch = config.ch;
					params.tileX = static_cast<int>(x);
					params.tileY = static_cast<int>(z);
					
					params.verts            = m_data->polymesh->verts;
					params.vertCount        = m_data->polymesh->nverts;
					params.polys            = m_data->polymesh->polys;
					params.polyCount        = m_data->polymesh->npolys;
					params.polyAreas        = m_data->polymesh->areas;
					params.polyFlags        = m_data->polymesh->flags;
					params.nvp              = m_data->polymesh->nvp;
					params.detailMeshes     = m_data->detailmesh->meshes;
					params.detailVerts      = m_data->detailmesh->verts;
					params.detailVertsCount = m_data->detailmesh->nverts;
					params.detailTris       = m_data->detailmesh->tris;
					params.detailTriCount   = m_data->detailmesh->ntris;
					
					rcVcopy(params.bmin, m_data->polymesh->bmin);
					rcVcopy(params.bmax, m_data->polymesh->bmax);
					
					params.walkableHeight = static_cast<float>(config.walkableHeight) * dv;
					params.walkableRadius = static_cast<float>(config.walkableRadius) * dh;
					params.walkableClimb  = static_cast<float>(config.walkableClimb) * dv;

					/// @todo: These seem to be optional; figure out if we need them at all.
		//			params.offMeshConVerts = m_data->offMeshConVerts;
		//			params.offMeshConRad = m_data->offMeshConRads;
		//			params.offMeshConDir = m_data->offMeshConDirs;
		//			params.offMeshConAreas = m_data->offMeshConAreas;
		//			params.offMeshConFlags = m_data->offMeshConFlags;
		//			params.offMeshConUserID = m_data->offMeshConId;
		//			params.offMeshConCount = 0;

					// Detour will allocate memory in this which need only
					// be freed if navmesh initalisation fails; otherwise
					// the navmesh destructor will free it later.
					unsigned char *navdata{nullptr};
					int navdatasize{0};
					
					if (!dtCreateNavMeshData(&params, &navdata, &navdatasize))
					{
						log::err("Karhu") << "Failed to create path data for navmesh: " << statusToString(status);
						return false;
					}
					
					status = m_data->navmesh->addTile(navdata, navdatasize, DT_TILE_FREE_DATA, 0, nullptr);
			
					if (dtStatusFailed(status))
					{
						dtFree(navdata);
						log::err("Karhu") << "Failed to add tile to navmesh: " << statusToString(status);
						return false;
					}
				}
			}
			
			if (!bakeQuery())
				return false;
			
			m_valid = true;
			
			return true;
		}
		
		bool Navmesh::bakeQuery()
		{
			m_data->query = dtAllocNavMeshQuery();
			
			if (!m_data->query)
			{
				log::err("Karhu") << "Failed to allocate query for navmesh";
				return false;
			}
			
			// Second parameter is maximum number of nodes.
			dtStatus status;
			status = m_data->query->init(m_data->navmesh, 2048);
			
			if (dtStatusFailed(status))
			{
				log::err("Karhu") << "Failed to initialise path query interface for navmesh";
				return false;
			}
			
			return true;
		}
		
		std::vector<mth::Vec3f> Navmesh::path
		(
			mth::Vec3f const &from,
			mth::Vec3f const &to,
			Area       const  filter
		) const
		{
			if (!valid())
				return {};
		
			/// @todo: Do something about these magic numbers. Base them on the actual number of polys/verts in the mesh?
			constexpr int const
				countPolysMax{256},
				countVertsMax{512};
			
			m_data->filter.setIncludeFlags(filter);
			m_data->filter.setExcludeFlags(Areas::invalid);
			
			dtStatus status;
			dtPolyRef polyFrom, polyTo, polysPath[countPolysMax];
			float const posFrom[]{from.x, from.y, from.z}, posTo[]{to.x, to.y, to.z};
			float nearestFrom[3], nearestTo[3], vertsPath[countVertsMax * 3];
			int countPolys{0}, countVerts{0};
			
			status = m_data->query->findNearestPoly(posFrom, m_data->extents, &m_data->filter, &polyFrom, nearestFrom);
			
			if ((status & DT_FAILURE) || (status & DT_STATUS_DETAIL_MASK))
			{
				log::err("Karhu") << "Failed to find polygon at path start for navmesh";
				return {};
			}
			
			status = m_data->query->findNearestPoly(posTo, m_data->extents, &m_data->filter, &polyTo, nearestTo);
			
			if ((status & DT_FAILURE) || (status & DT_STATUS_DETAIL_MASK))
			{
				log::err("Karhu") << "Failed to find polygon at path end for navmesh";
				return {};
			}
			
			status = m_data->query->findPath(polyFrom, polyTo, nearestFrom, nearestTo, &m_data->filter, polysPath, &countPolys, countPolysMax);
			
			if ((status & DT_FAILURE) || (status & DT_STATUS_DETAIL_MASK) || 0 == countPolys)
			{
				log::err("Karhu") << "Failed to find path polygons for navmesh";
				return {};
			}
			
			status = m_data->query->findStraightPath(nearestFrom, nearestTo, polysPath, countPolys, vertsPath, nullptr, nullptr, &countVerts, countVertsMax);
	
			if ((status & DT_FAILURE) || (status & DT_STATUS_DETAIL_MASK) || 0 == countVerts)
			{
				log::err("Karhu") << "Failed to find straight path for navmesh";
				return {};
			}
			
			std::vector<mth::Vec3f> r;
			
			for (std::size_t i{0}; i < static_cast<std::size_t>(countVerts); ++ i)
			{
				r.emplace_back
				(
					vertsPath[i * 3 + 0],
					vertsPath[i * 3 + 1],
					vertsPath[i * 3 + 2]
				);
			}
			
			return r;
		}
		
		void Navmesh::clean()
		{
			m_valid = false;
			
			if (m_data->navmesh)
			{
				dtFreeNavMesh(m_data->navmesh);
				m_data->navmesh = nullptr;
			}
			
			if (m_data->query)
			{
				dtFreeNavMeshQuery(m_data->query);
				m_data->query = nullptr;
			}
			
			cleanRC();
		}
		
		void Navmesh::cleanRC()
		{
			if (m_data->solid)
			{
				rcFreeHeightField(m_data->solid);
				m_data->solid = nullptr;
			}
		
			if (m_data->compact)
			{
				rcFreeCompactHeightfield(m_data->compact);
				m_data->compact = nullptr;
			}
		
			if (m_data->contours)
			{
				rcFreeContourSet(m_data->contours);
				m_data->contours = nullptr;
			}
		
			if (m_data->polymesh)
			{
				rcFreePolyMesh(m_data->polymesh);
				m_data->polymesh = nullptr;
			}
		
			if (m_data->detailmesh)
			{
				rcFreePolyMeshDetail(m_data->detailmesh);
				m_data->detailmesh = nullptr;
			}
		}
		
		Status Navmesh::performLoadInAdditionToMetadata
		(
			const conv::JSON::Obj *const /*metadata*/,
			const char *filepathRelativeToResourcepath,
			const char */*suffix*/,
			conv::AdapterFilesystemRead &filesystem
		)
		{
			if (valid())
				clean();
			
			m_data->context.enableLog(true);
			
			// For path queries.
			/// @todo: Diameter should be loaded from the file too.
			m_data->extents[0] =
			m_data->extents[1] =
			m_data->extents[2] = limitDiameter;
		
			/// @todo: Attempt to load navmesh using readBytes crashes at memcpy of tile data and I need to get on with other stuff for now, but do fix this eventually so that it works on every system... Pasting fstream solution for now.
			
			/// @todo: Extra super temporary, this is also why we want the adapter...
			std::string const path
			{
				app::App::instance()->fio().basepathResources()
				+ '/'
				+ filepathRelativeToResourcepath
			};
			
			std::ifstream fi{path, std::ios::binary};
			
			if (fi.is_open())
			{
				HeaderNavmesh headerNavmesh;
				
				fi.read(reinterpret_cast<char *>(&headerNavmesh), sizeof(headerNavmesh));
				
				if
				(
					static_cast<std::uint8_t>(DT_NAVMESH_VERSION) == headerNavmesh.version &&
					static_cast<std::uint8_t>(DT_NAVMESH_MAGIC)   == headerNavmesh.magic
				)
				{
					m_data->navmesh = dtAllocNavMesh();
					m_data->navmesh->init(&headerNavmesh.params);
					
					for (std::uint8_t i{0}; i < headerNavmesh.tilecount; ++ i)
					{
						HeaderTile
							headerTile;
						
						fi.read(reinterpret_cast<char *>(&headerTile), sizeof(headerTile));
						
						auto const
							size(static_cast<std::size_t>(headerTile.size));
						
						auto data(static_cast<unsigned char *>(dtAlloc(size, DT_ALLOC_PERM)));
						
						karhuASSERT(data);
						
						std::memset(data, 0, size);
						
						fi.read(reinterpret_cast<char *>(data), static_cast<std::streamsize>(size));
						
						m_data->navmesh->addTile
						(
							data,
							static_cast<int>(size),
							DT_TILE_FREE_DATA,
							0,
							nullptr
						);
					}
				}
			}
			else
			{
				std::stringstream err;
				err
					<< "Failed to load navmesh from '"
					<< path
					<< "': "
					<< "could not open file";
			
				return {false, err.str()};
			}
		/*
			auto const bytes(filesystem.readBytesFromResource(filepathRelativeToResourcepath));
			
			if (!bytes.success)
			{
				std::stringstream err;
				err
					<< "Failed to load navmesh from '"
					<< filepathRelativeToResourcepath
					<< "': "
					<< bytes.error;
			
				return {false, err.str()};
			}
			log::msg("DEBUG")<<"navmesh load " << bytes.value.size << " bytes";
			Byte const *cursor{bytes.value.data.get()};
			
			HeaderNavmesh headerNavmesh;
			
			std::memcpy(&headerNavmesh, cursor, sizeof(headerNavmesh));
			cursor += sizeof(headerNavmesh);
log::msg("DEBUG")<<"navmesh memcpyd header, tilecnt = " << (int)headerNavmesh.tilecount
<< ", param maxtil "<<(int)headerNavmesh.params.maxTiles;
			if
			(
				static_cast<std::uint8_t>(DT_NAVMESH_VERSION) == headerNavmesh.version &&
				static_cast<std::uint8_t>(DT_NAVMESH_MAGIC)   == headerNavmesh.magic
			)
			{
				m_data->navmesh = dtAllocNavMesh();
				m_data->navmesh->init(&headerNavmesh.params);
				
				for (std::uint8_t i{0}; i < headerNavmesh.tilecount; ++ i)
				{
					HeaderTile
						headerTile;
					
					std::memcpy(&headerTile, cursor, sizeof(headerTile));
					cursor += sizeof(headerTile);
					
					auto const
						size(static_cast<std::size_t>(headerTile.size));
					
					auto data(static_cast<unsigned char *>(dtAlloc(size, DT_ALLOC_PERM)));
					
					karhuASSERT(data)
					
					std::memcpy(&data, cursor, size);
					cursor += size;
					
					m_data->navmesh->addTile
					(
						data,
						static_cast<int>(size),
						DT_TILE_FREE_DATA,
						0,
						nullptr
					);
				}
			}
			*/
			if (!bakeQuery())
			{
				std::stringstream err;
				err
					<< "Failed to load navmesh from '"
					<< filepathRelativeToResourcepath
					<< "': "
					<< "failed to create query object";
				
				return {false, err.str()};
			}
			
			m_valid = true;
/*
			/// @todo: Temporary crowd test, should not go here.
			if (!m_data->crowd.init(10, limitDiameter, m_data->navmesh))
				log::err("DEBUG")<<"crowd init fail";
			else
			{
				log::msg("DEBUG")<<"crowd init good";
				
				for (float i{-1.0f}; i <= 1.0f; ++ i)
				{
					dtCrowdAgentParams params;
					std::memset(&params, 0, sizeof(params));
					
					params.radius = limitDiameter * 0.5f + 0.1f;
					params.height = 2.0f;
					params.maxAcceleration = 8.0f;
					params.maxSpeed = 1.5f;
					params.collisionQueryRange = params.radius * 12.0f;
					params.pathOptimizationRange = params.radius * 30.0f;
					params.updateFlags |= DT_CROWD_ANTICIPATE_TURNS;
//					params.updateFlags |= DT_CROWD_OPTIMIZE_VIS;
	//				params.updateFlags |= DT_CROWD_OPTIMIZE_TOPO;
					params.updateFlags |= DT_CROWD_OBSTACLE_AVOIDANCE;
//					params.updateFlags |= DT_CROWD_SEPARATION;
	//				params.obstacleAvoidanceType = ;
//					params.separationWeight = 0.25f;
					
					float pos[]{i * 10.0f, 7.85105f, 40.0f - ((0 == i) ? 10.0f : 0.0f)};
					
					int id = m_data->crowd.addAgent(pos, &params);
					if (-1 == id)
						log::err("DEBUG")<<"crowd add agent fail";
					else
					{
						log::msg("DEBUG")<<"crowd added agent " << id;
						s_agents.emplace_back(id);
					}
				}
				
				for (std::size_t i{0}; i < s_agents.size(); ++ i)
				{
					auto agent = m_data->crowd.getAgent(s_agents[i]);
					float target[3];
					rcVcopy(target, agent->npos);
					target[0] *= -1.0f;
					target[2] += ((i == 1) ? 15.0f : 0.0f);
					log::msg("DEBUG") << "agent " << i << " ("
					<< agent->npos[0] << ", "
					<< agent->npos[1] << ", "
					<< agent->npos[2] << ") -> ("
					<< target[0] << ", "
					<< target[1] << ", "
					<< target[2] << ")";
					
					dtPolyRef polyref;
					float nearestFrom[3];
					dtStatus status = m_data->query->findNearestPoly(target, m_data->extents, &m_data->filter, &polyref, nearestFrom);
					
					if (dtStatusFailed(status))
						log::err("DEBUG")<<"agent query fail";
					else
					{
						log::msg("DEBUG")<<"agent query good";
						
						if (!m_data->crowd.requestMoveTarget(s_agents[i], polyref, target))
							log::err("DEBUG")<<"agent req move fail";
						else
							log::msg("DEBUG")<<"agent req move good";
					}
				}
			}
			*/
			return {true};
		}
		
		/// @todo: Temporary navmesh crowd test
		void Navmesh::update(float /*dt*/)
		{
			//m_data->crowd.update(dt, nullptr);
		}
		
		/// @todo: Probably safe to not use the FS adapter here since this should not be done at game runtime anyway, only in editor.
		Status Navmesh::performSaveInAdditionToMetadata
		(
			conv::JSON::Obj *const /*metadata*/,
			char const *filepathRelativeToResourcepath,
			char const */*suffix*/,
			conv::AdapterFilesystemWrite &/*filesystem*/
		)
		{
			/// @todo: Extra super temporary, this is also why we want the adapter...
			std::string const path
			{
				app::App::instance()->fio().basepathResources()
				+ '/'
				+ filepathRelativeToResourcepath
			};
			
			std::ofstream fo{path, std::ios::trunc | std::ios::binary};
			
			if (fo.is_open())
			{
				dtNavMesh const *const mesh{m_data->navmesh};
				
				auto const forEachTile([mesh](auto const callback)
				{
					for (int i{0}; i < mesh->getMaxTiles(); ++ i)
					{
						dtMeshTile const *const tile{mesh->getTile(i)};
						
						if (!tile || !tile->header || !tile->dataSize)
							continue;
						
						callback(tile);
					}
				});
				
				HeaderNavmesh headerNavmesh;
				headerNavmesh.tilecount = 0;
				headerNavmesh.version   = static_cast<std::uint8_t>(DT_NAVMESH_VERSION);
				headerNavmesh.magic     = static_cast<std::uint8_t>(DT_NAVMESH_MAGIC);
				
				std::memcpy(&headerNavmesh.params, mesh->getParams(), sizeof(headerNavmesh.params));
				
				forEachTile([&headerNavmesh](auto const)
				{
					++ headerNavmesh.tilecount;
				});
				
				fo.write(reinterpret_cast<char const *>(&headerNavmesh), sizeof(headerNavmesh));
				
				forEachTile([mesh, &fo](auto const tile)
				{
					HeaderTile headerTile;
					
					headerTile.ref  = mesh->getTileRef(tile);
					headerTile.size = static_cast<std::uint64_t>(tile->dataSize);
					
					fo.write(reinterpret_cast<char const *>(&headerTile), sizeof(headerTile));
					fo.write(reinterpret_cast<char const *>(tile->data), tile->dataSize);
				});
				
				return {true};
			}
			
			std::stringstream err;
			err
				<< "Failed to save navmesh to '"
				<< path
				<< "': unable to open file for writing";
			
			return {false, err.str()};
		}
		
		Navmesh::~Navmesh()
		{
			clean();
		}
	}
}
