/**
 * @author		Ava Skoog
 * @date		2020-10-12
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/nav/components/Navigable.hpp>
#include <karhu/app/edt/dropdown.inl>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace nav
	{
		void Navigable::area(nav::Area const v)
		{
			/// @todo: What should happen if a navigable area is changed at runtime? Force navmesh rebuild? Or nothing?
//			if (v != m_area)
			m_area = v;
		}
	
		void Navigable::serialise(conv::Serialiser &ser) const
		{
			ser << karhuINn("area", m_area);
		}
		
		void Navigable::deserialise(conv::Serialiser const &ser)
		{
			ser >> karhuOUTn("area", m_area);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Navigable::editorEdit(app::App &, edt::Editor &)
		{
			constexpr std::pair<char const *, nav::Area> areas[]
			{
				{"Invalid", nav::Areas::invalid},
				{"Walk",    nav::Areas::walk},
				{"Door",    nav::Areas::door},
				{"Swim",    nav::Areas::swim},
				{"Jump",    nav::Areas::jump},
				{"Climb",   nav::Areas::climb}
				/// @todo: Custom navmesh areas?
			};
			
			if (edt::dropdown("Area", m_area, std::begin(areas), std::end(areas)))
				return true;
			
			return false;
		}
		#endif
	}
}
