/**
 * @author		Ava Skoog
 * @date		2020-10-12
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_NAV_NAVIGABLE_H_
	#define KARHU_NAV_NAVIGABLE_H_

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/app/nav/common.hpp>
	#include <karhu/app/edt/support.hpp>

	namespace karhu
	{
		namespace nav
		{
			class Navigable : public ecs::Component
			{
				public:
					void area(nav::Area const);
					nav::Area area() const noexcept { return m_area; }
				
				public:
					using ecs::Component::Component;
				
					void serialise(conv::Serialiser &) const;
					void deserialise(conv::Serialiser const &);
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
                    void editorEvent(app::App &, edt::Editor &, edt::Identifier const /*caller*/, edt::Event const &) override {}
                    bool editorEdit(app::App &, edt::Editor &) override;
                    #endif
				
				private:
					nav::Area m_area{nav::Areas::walk};
			};
		}
	}
#endif
