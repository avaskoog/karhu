constexpr char const *scriptmagic{R"(
#include <initializer_list>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-attributes"

template<template<typename> class T, typename U> U &_this_(T<U> *);
template<typename T> T &_this_(T *);
template<template<typename> class T, typename U> U const &_this_(T<U> const *);
template<typename T> T const &_this_(T const *);

template<typename T, typename U> T cast(U);

struct null
{
	template<typename T> operator T() const;
	template<typename T> friend bool operator==(T, null);
	template<typename T> friend bool operator==(null, T);
	template<typename T> friend bool operator!=(T, null);
	template<typename T> friend bool operator!=(null, T);
} null;

struct ref
{
	ref();
	template<typename T> ref(T);
};

using int8 = signed char;
using int16 = signed short;
using int32 = int;
using int64 = long;
using uint8 = unsigned char;
using uint16 = unsigned short;
using uint32 = unsigned int;
using uint = unsigned int;
using uint64 = unsigned long;

#define in [[annotate("in")]]
#define out [[annotate("out")]]
#define inout [[annotate("inout")]]
#define private [[annotate("private")]]
#define protected [[annotate("protected")]]
#define external
#define shared
#define abstract
#define override
#define property
#define final
#define is ==
#define interface struct
#define class struct
#define enum enum class
#define funcdef typedef
#define function []
#define mixin template<typename>
#define this (_this_(this))
)"};
