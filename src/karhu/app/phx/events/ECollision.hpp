/**
 * @author		Ava Skoog
 * @date		2019-01-10
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_PHYSIC_ECOLLISION_H_
	#define KARHU_PHYSIC_ECOLLISION_H_

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
		namespace phx
		{
			class ECollision : public ecs::Event
			{
				public:
					enum class Type : std::uint8_t
					{
						begin,
						end,
						stay
					};
				
					struct Contact
					{
						mth::Vec3f point, pointOther;
						mth::Scalf distance;
						/// @todo: Lifetime?
					};
				
				public:
					ECollision
					(
						const ecs::IDEvent         &identifier,
						const ecs::IDEntity        &entity,
						const ecs::IDTypeComponent &typeComponent,
						const ecs::IDComponent     &component,
						const Type                  type,
						const ecs::IDEntity        &entityOther,
						const ecs::IDComponent     &componentOther,
						const ecs::IDComponent     &colliderA,
						const ecs::IDComponent     &colliderB,
						const ecs::IDTypeComponent &typeComponentA,
						const ecs::IDTypeComponent &typeComponentB,
						const std::vector<Contact> &contacts
					)
					:
					ecs::Event    {identifier, entity, typeComponent, component},
					type          {type},
					entityOther   {entityOther},
					componentOther{componentOther},
					colliderA     {colliderA},
					colliderB     {colliderB},
					typeComponentA{typeComponentA},
					typeComponentB{typeComponentB},
					contacts      {contacts}
					{
					}
				
				public:
					Type                 type;
					ecs::IDEntity        entityOther;
					ecs::IDComponent     componentOther, colliderA, colliderB;
					ecs::IDTypeComponent typeComponentA, typeComponentB;
					std::vector<Contact> contacts;
			};
		}
	}
#endif
