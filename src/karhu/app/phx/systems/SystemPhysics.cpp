/**
 * @author		Ava Skoog
 * @date		2019-01-10
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Physics layers!
/// @todo: Bullet does SIMD stuff which might have to be turned off for MinGW. BT_USE_SSE?

#include <karhu/app/phx/systems/SystemPhysics.hpp>
#include <karhu/app/phx/components/ColliderBox.hpp>
#include <karhu/app/phx/components/ColliderSphere.hpp>
#include <karhu/app/phx/components/ColliderCapsule.hpp>
#include <karhu/app/phx/components/ColliderCylinder.hpp>
#include <karhu/app/phx/components/ColliderMesh.hpp>
#include <karhu/app/phx/events/ECollision.hpp>
#include <karhu/app/gfx/components/Transform.hpp>
#include <karhu/core/PoolPersistent.hpp>

#include <karhu/app/edt/support.hpp>

#include <karhu/app/lib/bullet/btBulletDynamicsCommon.h>

#include <set>

/// @todo: Remove this when done testing.
#include <karhu/app/lib/im3d/im3d.h>
#include <karhu/app/lib/im3d/im3d_math.h>
#include <karhu/app/App.hpp>

namespace
{
	using namespace karhu;
	using namespace karhu::phx;
	
	static void logErrorCollider
	(
		const ecs::IDEntity    &entity,
		const ecs::IDComponent &component
	)
	{
		log::err("Karhu")
			<< "Could not assign collider "
			<< static_cast<int>(component)
			<< " to entity "
			<< static_cast<int>(entity)
			<< ": body component missing "
			<< "(make sure it comes before any collider component)";
	}
	
	static btRigidBody::btRigidBodyConstructionInfo constructionInfoBodyDefault
	(
		btCompoundShape &colliders
	)
	{
		btRigidBody::btRigidBodyConstructionInfo r
		{
			0.0f,
			nullptr,
			&colliders,
			/// @todo: Figure out what to set inertia to.
			// Inertia.
			{0.0f, 0.0f, 0.0f}
		};
		
		r.m_friction = 0.0f;
		
		return r;
	}
}

namespace karhu
{
	namespace phx
	{
		struct SystemPhysics::Data
		{
			struct DataBody
			{
				ecs::IDEntity    entity;
				ecs::IDComponent component;
				bool             modified{true};
				btCompoundShape  colliders{};
				btRigidBody      rigidbody{constructionInfoBodyDefault(colliders)};
				
				DataBody(DataBody &&) = delete;
				DataBody(const DataBody &) = delete;
				DataBody &operator=(DataBody &&) = delete;
				DataBody &operator=(const DataBody &) = delete;
			};
			
			struct DataCollider
			{
				ecs::IDTypeComponent componenttype;
				ecs::IDEntity        entity;
				ecs::IDComponent     component;
				
				enum class Type : std::uint8_t
				{
					box,
					sphere,
					capsule,
					cylinder,
					cone,
					mesh,
					hull
				} type;
				
				struct Meshdata
				{
					std::unique_ptr<btTriangleIndexVertexArray> tris;
					
					/// @todo: btBvhTriangleMeshShape is only for static objects so will have to do something else for dynamic ones.
					std::unique_ptr<btBvhTriangleMeshShape>  mesh;
					
					std::vector<int> indices;
					std::vector<btScalar> vertices;
				};
				
				union
				{
					btBoxShape        box;
					btSphereShape     sphere;
					btCapsuleShape    capsule;
					btCylinderShape   cylinder;
					btConeShape       cone;
					Meshdata          mesh;
					btConvexHullShape hull;
				};
				
				DataCollider
				(
					const ecs::IDTypeComponent &componenttype,
					const ecs::IDEntity        &entity,
					const ecs::IDComponent     &component,
					const Type                  type
				)
				:
				componenttype{componenttype},
				entity       {entity},
				component    {component},
				type         {type}
				{
				}
				
				DataCollider(DataCollider &&) = delete;
				DataCollider(const DataCollider &) = delete;
				DataCollider &operator=(DataCollider &&) = delete;
				DataCollider &operator=(const DataCollider &) = delete;
				
				~DataCollider()
				{
					switch (type)
					{
						case Type::box:      box     .~btBoxShape();        break;
						case Type::sphere:   sphere  .~btSphereShape();     break;
						case Type::capsule:  capsule .~btCapsuleShape();    break;
						case Type::cylinder: cylinder.~btCylinderShape();   break;
						case Type::cone:     cone    .~btConeShape();       break;
						case Type::mesh:     mesh    .~Meshdata();          break;
						case Type::hull:     hull    .~btConvexHullShape(); break;
					}
				}
			};
			
			/// @todo: Do some testing and figure out how large to actually make the body and collider memory pool blocks.
			PoolPersistent<ecs::IDComponent, DataBody,     64> bodies;
			PoolPersistent<ecs::IDComponent, DataCollider, 64> colliders;
			
			struct Contactinfo
			{
				bool stay;
				const DataBody *bodyA, *bodyB;
				const DataCollider *collA, *collB;
				std::vector<ECollision::Contact> contacts;
			};
			
			using Colliderpair = std::pair<ecs::IDComponent, ecs::IDComponent>;
			using Manifolds    = std::map<Colliderpair, Contactinfo>;
			
			Manifolds manifolds;
			
			btDbvtBroadphase                    bulletInterfaceBroadphase;
			btDefaultCollisionConfiguration     bulletConfigColl;
			btCollisionDispatcher               bulletDispatcherColl{&bulletConfigColl};
			btSequentialImpulseConstraintSolver bulletSolver;
			
			btDiscreteDynamicsWorld bulletWorld
			{
				&bulletDispatcherColl,
				&bulletInterfaceBroadphase,
				&bulletSolver,
				&bulletConfigColl
			};
		};
		
		SystemPhysics::SystemPhysics()
		:
		m_data{std::make_unique<SystemPhysics::Data>()}
		{
			c_instance = this;
			
			/// @todo: Make physics gravity customisable.
			m_data->bulletWorld.setGravity(btVector3{0.0f, -9.82f, 0.0f});
			
			// Enable collisions on the default layer by default.
			layermask(layer(0), layer(0), true);
		}
		
		void SystemPhysics::listenerBodyCreated(app::App &app, const ecs::EComponentCreated &e, const float dt, const float dtFixed, const float timestep, void *data)
		{
			auto s(reinterpret_cast<SystemPhysics *>(data));
			auto c(app.ecs().componentInEntity<Body>(e.component(), e.entity()));
			
			auto body(s->m_data->bodies.create
			(
				e.component(),
				e.entity(),
				e.component()
			));
		
			body->rigidbody.setUserPointer(body);
		}
		
		void SystemPhysics::listenerBodyDestroyed(app::App &, const ecs::EComponentDestroyed &e, const float dt, const float dtFixed, const float timestep, void *data)
		{
			auto s   (reinterpret_cast<SystemPhysics *>(data));
			auto body(s->m_data->bodies.get(e.component()));
			
			s->m_data->bulletWorld.removeRigidBody(&body->rigidbody);
			
			s->m_data->bodies.destroy(e.component());
		}
		
		void SystemPhysics::listenerColliderBoxCreated(app::App &app, const ecs::EComponentCreated &e, const float dt, const float dtFixed, const float timestep, void *data)
		{
			auto s(reinterpret_cast<SystemPhysics *>(data));
			auto c(app.ecs().componentInEntity<ColliderBox>(e.component(), e.entity()));
			
			// Create the collider.
			
			auto collider(s->m_data->colliders.create
			(
				e.component(),
				app.ecs().IDOfTypeComponent<ColliderBox>(),
				e.entity(),
				e.component(),
				Data::DataCollider::Type::box
			));
			
			// Bullet uses half sizes but we do not.
			new(&collider->box) btBoxShape
			{{
				c->size().x * 0.5f,
				c->size().y * 0.5f,
				c->size().z * 0.5f
			}};
			
			collider->box.setUserPointer(collider);
			
			// Add it to the body.
			
			/// @todo: Should we add ECS function to just get ID of component in entity so that it does not actually have to be retrieved from pool as well?
			const auto b(app.ecs().componentInEntity<Body>(e.entity()));
		
			if (!b)
			{
				logErrorCollider(e.entity(), e.component());
				return;
			}
		
				  auto body(s->m_data->bodies.get(b->identifier()));
			const auto p   (c->offset() - c->origin());
			
			btTransform t;
			t.setIdentity();
			t.setOrigin({p.x, p.y, p.z});
			t.setRotation({c->rotation().x, c->rotation().y, c->rotation().z, c->rotation().w});
			
			body->colliders.addChildShape(t, &collider->box);
		}
		
		void SystemPhysics::listenerColliderBoxDestroyed(app::App &app, const ecs::EComponentDestroyed &e, const float dt, const float dtFixed, const float timestep, void *data)
		{
			auto s(reinterpret_cast<SystemPhysics *>(data));
			
			if (auto b = app.ecs().componentInEntity<Body>(e.entity()))
			{
				auto body(s->m_data->bodies.get(b->identifier()));
				auto c   (s->m_data->colliders.get(e.component()));
				
				body->colliders.removeChildShape(&c->box);
			}
			
			s->m_data->colliders.destroy(e.component());
		}
		
		void SystemPhysics::listenerColliderSphereCreated(app::App &app, const ecs::EComponentCreated &e, const float dt, const float dtFixed, const float timestep, void *data)
		{
			auto s(reinterpret_cast<SystemPhysics *>(data));
			auto c(app.ecs().componentInEntity<ColliderSphere>(e.component(), e.entity()));
			
			// Create the collider.
			
			auto collider(s->m_data->colliders.create
			(
				e.component(),
				app.ecs().IDOfTypeComponent<ColliderSphere>(),
				e.entity(),
				e.component(),
				Data::DataCollider::Type::sphere
			));
			
			// Bullet uses radius but we use diameter.
			new(&collider->sphere) btSphereShape
			{
				c->diameter() * 0.5f,
			};
			
			collider->sphere.setUserPointer(collider);
			
			// Add it to the body.
			
			/// @todo: Should we add ECS function to just get ID of component in entity so that it does not actually have to be retrieved from pool as well?
			const auto b(app.ecs().componentInEntity<Body>(e.entity()));
		
			if (!b)
			{
				logErrorCollider(e.entity(), e.component());
				return;
			}
		
				  auto body(s->m_data->bodies.get(b->identifier()));
			const auto p   (c->offset() - c->origin());
		
			btTransform t;
			t.setIdentity();
			t.setOrigin({p.x, p.y, p.z});
			t.setRotation({c->rotation().x, c->rotation().y, c->rotation().z, c->rotation().w});
			
			body->colliders.addChildShape(t, &collider->sphere);
		}
		
		void SystemPhysics::listenerColliderSphereDestroyed(app::App &app, const ecs::EComponentDestroyed &e, const float dt, const float dtFixed, const float timestep, void *data)
		{
			auto s(reinterpret_cast<SystemPhysics *>(data));
			
			if (auto b = app.ecs().componentInEntity<Body>(e.entity()))
			{
				auto body(s->m_data->bodies.get(b->identifier()));
				auto c   (s->m_data->colliders.get(e.component()));
				
				body->colliders.removeChildShape(&c->sphere);
			}
			
			s->m_data->colliders.destroy(e.component());
		}
		
		void SystemPhysics::listenerColliderCapsuleCreated(app::App &app, const ecs::EComponentCreated &e, const float dt, const float dtFixed, const float timestep, void *data)
		{
			auto s(reinterpret_cast<SystemPhysics *>(data));
			auto c(app.ecs().componentInEntity<ColliderCapsule>(e.component(), e.entity()));
			
			// Create the collider.
			
			auto collider(s->m_data->colliders.create
			(
				e.component(),
				app.ecs().IDOfTypeComponent<ColliderCapsule>(),
				e.entity(),
				e.component(),
				Data::DataCollider::Type::capsule
			));
			
			// Bullet uses radius but we use diameter.
			new(&collider->capsule) btCapsuleShape
			{
				c->diameter() * 0.5f,
				c->height()
			};
			
			collider->capsule.setUserPointer(collider);
			
			// Add it to the body.
			
			/// @todo: Should we add ECS function to just get ID of component in entity so that it does not actually have to be retrieved from pool as well?
			const auto b(app.ecs().componentInEntity<Body>(e.entity()));
		
			if (!b)
			{
				logErrorCollider(e.entity(), e.component());
				return;
			}
		
				  auto body(s->m_data->bodies.get(b->identifier()));
			const auto p   (c->offset() - c->origin());
		
			btTransform t;
			t.setIdentity();
			t.setOrigin({p.x, p.y, p.z});
			t.setRotation({c->rotation().x, c->rotation().y, c->rotation().z, c->rotation().w});
			
			body->colliders.addChildShape(t, &collider->capsule);
		}
		
		void SystemPhysics::listenerColliderCapsuleDestroyed(app::App &app, const ecs::EComponentDestroyed &e, const float dt, const float dtFixed, const float timestep, void *data)
		{
			auto s(reinterpret_cast<SystemPhysics *>(data));
			
			if (auto b = app.ecs().componentInEntity<Body>(e.entity()))
			{
				auto body(s->m_data->bodies.get(b->identifier()));
				auto c   (s->m_data->colliders.get(e.component()));
				
				body->colliders.removeChildShape(&c->capsule);
			}
			
			s->m_data->colliders.destroy(e.component());
		}
		
		void SystemPhysics::listenerColliderCylinderCreated(app::App &app, const ecs::EComponentCreated &e, const float dt, const float dtFixed, const float timestep, void *data)
		{
			auto s(reinterpret_cast<SystemPhysics *>(data));
			auto c(app.ecs().componentInEntity<ColliderCylinder>(e.component(), e.entity()));
			
			// Create the collider.
			
			auto collider(s->m_data->colliders.create
			(
				e.component(),
				app.ecs().IDOfTypeComponent<ColliderCylinder>(),
				e.entity(),
				e.component(),
				Data::DataCollider::Type::cylinder
			));
			
			// Bullet uses radius but we use diameter.
			new(&collider->cylinder) btCylinderShape
			{{
				c->diameter() * 0.5f,
				c->height()   * 0.5f,
				c->diameter() * 0.5f
			}};
			
			collider->cylinder.setUserPointer(collider);
			
			// Add it to the body.
			
			/// @todo: Should we add ECS function to just get ID of component in entity so that it does not actually have to be retrieved from pool as well?
			const auto b(app.ecs().componentInEntity<Body>(e.entity()));
		
			if (!b)
			{
				logErrorCollider(e.entity(), e.component());
				return;
			}
		
				  auto body(s->m_data->bodies.get(b->identifier()));
			const auto p   (c->offset() - c->origin());
		
			btTransform t;
			t.setIdentity();
			t.setOrigin({p.x, p.y, p.z});
			t.setRotation({c->rotation().x, c->rotation().y, c->rotation().z, c->rotation().w});
			
			body->colliders.addChildShape(t, &collider->cylinder);
		}
		
		void SystemPhysics::listenerColliderCylinderDestroyed(app::App &app, const ecs::EComponentDestroyed &e, const float dt, const float dtFixed, const float timestep, void *data)
		{
			auto s(reinterpret_cast<SystemPhysics *>(data));
			
			if (auto b = app.ecs().componentInEntity<Body>(e.entity()))
			{
				auto body(s->m_data->bodies.get(b->identifier()));
				auto c   (s->m_data->colliders.get(e.component()));
				
				body->colliders.removeChildShape(&c->cylinder);
			}
			
			s->m_data->colliders.destroy(e.component());
		}
		
		void SystemPhysics::listenerColliderMeshCreated(app::App &app, const ecs::EComponentCreated &e, const float dt, const float dtFixed, const float timestep, void *data)
		{
			auto s(reinterpret_cast<SystemPhysics *>(data));
			auto c(app.ecs().componentInEntity<ColliderMesh>(e.component(), e.entity()));
			
			/// @todo: Validate?
			const auto mesh(app.res().get<res::Mesh>(c->mesh()));
			
			/// @todo: Validate?
			const auto geometry(mesh->geometryAtLOD(0));
			
			// Create the collider.
			
			auto collider(s->m_data->colliders.create
			(
				e.component(),
				app.ecs().IDOfTypeComponent<ColliderMesh>(),
				e.entity(),
				e.component(),
				Data::DataCollider::Type::mesh
			));
			
			new(&collider->mesh) Data::DataCollider::Meshdata;
			
			collider->mesh.indices.resize(geometry->indices.size());
			collider->mesh.indices.shrink_to_fit();
			
			for (std::size_t i{0}; i < geometry->indices.size(); ++ i)
				collider->mesh.indices[i] = geometry->indices[i];
			
			collider->mesh.vertices.resize(geometry->positions.size() * 3);
			collider->mesh.vertices.shrink_to_fit();
			
			for (std::size_t i{0}; i < geometry->positions.size(); ++ i)
			{
				collider->mesh.vertices[i * 3 + 0] = geometry->positions[i].x;
				collider->mesh.vertices[i * 3 + 1] = geometry->positions[i].y;
				collider->mesh.vertices[i * 3 + 2] = geometry->positions[i].z;
			}
			
			collider->mesh.tris = std::make_unique<btTriangleIndexVertexArray>
			(
				static_cast<int>(collider->mesh.indices.size() / 3),
				&collider->mesh.indices[0],
				3 * sizeof(int),
				static_cast<int>(collider->mesh.vertices.size() / 3),
				&collider->mesh.vertices[0],
				3 * sizeof(btScalar)
			);
			
			collider->mesh.mesh = std::make_unique<btBvhTriangleMeshShape>
			(
				collider->mesh.tris.get(),
				true
			);
			
			collider->mesh.mesh->setUserPointer(collider);
			
			// Add it to the body.
			
			/// @todo: Should we add ECS function to just get ID of component in entity so that it does not actually have to be retrieved from pool as well?
			const auto b(app.ecs().componentInEntity<Body>(e.entity()));
		
			if (!b)
			{
				logErrorCollider(e.entity(), e.component());
				return;
			}
		
				  auto body(s->m_data->bodies.get(b->identifier()));
			const auto p   (c->offset() - c->origin());
		
			btTransform t;
			t.setIdentity();
			t.setOrigin({p.x, p.y, p.z});
			t.setRotation({c->rotation().x, c->rotation().y, c->rotation().z, c->rotation().w});
			
			body->colliders.addChildShape(t, collider->mesh.mesh.get());
		}
		
		void SystemPhysics::listenerColliderMeshDestroyed(app::App &app, const ecs::EComponentDestroyed &e, const float dt, const float dtFixed, const float timestep, void *data)
		{
			auto s(reinterpret_cast<SystemPhysics *>(data));
			
			if (auto b = app.ecs().componentInEntity<Body>(e.entity()))
			{
				auto body(s->m_data->bodies.get(b->identifier()));
				auto c   (s->m_data->colliders.get(e.component()));
				
				body->colliders.removeChildShape(c->mesh.mesh.get());
			}
			
			s->m_data->colliders.destroy(e.component());
		}
		
		void SystemPhysics::performUpdateComponents
		(
			      app::App        &app,
				  ecs::Pool<Body> &components,
			const float            dt,
			const float            dtFixed,
			const float            timestep
		)
		{
			/// @todo: Add modified flags to colliders and update Bullet colliders.
			/// @todo: Remove this when done testing.
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);
				
				if (!c->activeInHierarchy())
					continue;
				
				auto body     (m_data->bodies.get(c->identifier()));
				auto transform(app.ecs().componentInEntity<gfx::Transform>(c->entity()));
				
				if (app.win().top() && app.inp().keyHeld(inp::Key::r))
				{
					for (std::size_t i{0}; i < body->colliders.getNumChildShapes(); ++ i)
					{
						Im3d::PushDrawState();
						{
							mth::Transformf t, tp;
							
							{
								btScalar floats[16];
								body->rigidbody.getWorldTransform().getOpenGLMatrix(floats);
								
								mth::decompose
								(
									mth::matrix4x4(floats),
									&tp.position,
									&tp.rotation,
									&tp.scale
								);
								
								tp.rotation = mth::inverse(tp.rotation);
							}
							
							{
								btScalar floats[16];
								body->colliders.getChildTransform(i).getOpenGLMatrix(floats);
								
								mth::decompose
								(
									mth::matrix4x4(floats),
									&t.position,
									&t.rotation,
									&t.scale
								);
								
								t.rotation = mth::inverse(t.rotation);
							}
							
							const mth::Mat4f model{tp.matrix() * t.matrix()};
							const Im3d::Mat4 M{mth::matrixIm3D(model)};
							
							Im3d::PushMatrix(M);
							{
								auto data(reinterpret_cast<const Data::DataCollider *>(body->colliders.getChildShape(i)->getUserPointer()));
								
								Im3d::SetColor(0.0f, 0.0f, 1.0f, 0.75f);
								
								switch (data->type)
								{
									case Data::DataCollider::Type::box:
									{
										const btVector3
											min{data->box.getHalfExtentsWithMargin() * -1.0f},
											max{data->box.getHalfExtentsWithMargin()};
										
										Im3d::DrawAlignedBox
										(
											{min.x(), min.y(), min.z()},
											{max.x(), max.y(), max.z()}
										);
										
										break;
									}
									
									case Data::DataCollider::Type::sphere:
									{
										Im3d::DrawSphere({0.0f, 0.0f, 0.0f}, data->sphere.getRadius());
										break;
									}
									
									case Data::DataCollider::Type::capsule:
									{
										Im3d::DrawCapsule
										(
											{0.0f, data->capsule.getHalfHeight(), 0.0f},
											{0.0f, -data->capsule.getHalfHeight(), 0.0f},
											data->capsule.getRadius()
										);
										
										break;
									}
									
									case Data::DataCollider::Type::cylinder:
									{
										Im3d::DrawCylinder
										(
											{0.0f, data->cylinder.getHalfExtentsWithMargin().y(), 0.0f},
											{0.0f, -data->cylinder.getHalfExtentsWithMargin().y(), 0.0f},
											data->cylinder.getRadius()
										);
										
										break;
									}
									
									case Data::DataCollider::Type::mesh:
									{
										const auto &arr (data->mesh.tris->getIndexedMeshArray());
										const auto  vers(reinterpret_cast<const btScalar *>(arr[0].m_vertexBase));
										const auto  inds(reinterpret_cast<const int *>(arr[0].m_triangleIndexBase));
										
										for (int i{0}; i < (arr[0].m_numTriangles * 3); i += 3)
										{
											const int *ia{inds + i + 0}, *ib{inds + i + 1}, *ic{inds + i + 2};
											const btScalar *va{vers + *ia * 3}, *vb{vers + *ib * 3}, *vc{vers + *ic * 3};
											
											const Im3d::Vec3
												a
												{
													*(va + 0),
													*(va + 1),
													*(va + 2)
												},
												b
												{
													*(vb + 0),
													*(vb + 1),
													*(vb + 2)
												},
												c
												{
													*(vc + 0),
													*(vc + 1),
													*(vc + 2)
												};
											
											Im3d::DrawLine(a, b, 6.0f, {1.0f, 0.0f, 0.0f, 0.15f});
											Im3d::DrawLine(b, c, 6.0f, {1.0f, 0.0f, 0.0f, 0.15f});
											Im3d::DrawLine(c, a, 6.0f, {1.0f, 0.0f, 0.0f, 0.15f});
										}
										
										break;
									}
									
									default:
										break;
								}
							}
							Im3d::PopMatrix();
						}
						Im3d::PopDrawState();
					}
				}
			}
			
			if (m_paused)
				return;
			
			Data::Manifolds manifolds;
			
			// We start by updating the physics engine.
			
			float timeAccumulated{dtFixed};
			
			while (timeAccumulated >= timestep)
			{
				#ifdef KARHU_EDITOR
				if (app::ModeApp::server != app.mode())
				#endif
					m_data->bulletWorld.stepSimulation(timestep);
				
				timeAccumulated -= timestep;
				
				btDispatcher *const dispatcher{m_data->bulletWorld.getDispatcher()};
				const int count{dispatcher->getNumManifolds()};
				
				for (int i{0}; i < count; ++ i)
				{
					btPersistentManifold *const manifold{dispatcher->getManifoldByIndexInternal(i)};
					
					auto objectA(dynamic_cast<const btRigidBody *>(manifold->getBody0()));
					auto objectB(dynamic_cast<const btRigidBody *>(manifold->getBody1()));
					
					auto bodyA(reinterpret_cast<const Data::DataBody *>(objectA->getUserPointer()));
					auto bodyB(reinterpret_cast<const Data::DataBody *>(objectB->getUserPointer()));
					
					auto shapeA(dynamic_cast<const btCompoundShape *>(objectA->getCollisionShape()));
					auto shapeB(dynamic_cast<const btCompoundShape *>(objectB->getCollisionShape()));
					
					auto collisiontype{ECollision::Type::stay};
					
					using Indexpair = std::pair<int, int>;
					using List      = std::vector<const btManifoldPoint *>;
					
					std::map<Indexpair, List> contactsByIndices;
					
					for (int j{0}; j < manifold->getNumContacts(); ++ j)
					{
						const btManifoldPoint &contact{manifold->getContactPoint(j)};
						contactsByIndices[{contact.m_index0, contact.m_index1}].emplace_back(&contact);
					}
					
					for (auto &contactByIndex : contactsByIndices)
					{
						const Indexpair &indices{contactByIndex.first};
							  List      &points {contactByIndex.second};
						
						/// @todo: Quick fix because children are invalid for btBvhTriangleMeshShape; fix!
						if (indices.first >= shapeA->getNumChildShapes() || indices.second >= shapeB->getNumChildShapes())
							continue;
						
						auto collA(reinterpret_cast<const Data::DataCollider *>
						(
							shapeA->getChildShape(indices.first)->getUserPointer()
						));
					
						auto collB(reinterpret_cast<const Data::DataCollider *>
						(
							shapeB->getChildShape(indices.second)->getUserPointer()
						));
					
						std::vector<ECollision::Contact> contacts;
						
						for (const auto &p : points)
						{
							ECollision::Contact contact;
							
							contact.point =
							{
								p->getPositionWorldOnA().x(),
								p->getPositionWorldOnA().y(),
								p->getPositionWorldOnA().z()
							};
							
							contact.pointOther =
							{
								p->getPositionWorldOnB().x(),
								p->getPositionWorldOnB().y(),
								p->getPositionWorldOnB().z()
							};
							
							contact.distance = p->getDistance();
							
							contacts.emplace_back(std::move(contact));
						}
						
						const Data::Colliderpair pair{collA->component, collB->component};
						auto it(m_data->manifolds.find(pair));
						const bool stay{(it != m_data->manifolds.end())};
						
						manifolds[pair] = Data::Contactinfo
						{
							stay,
							bodyA,
							bodyB,
							collA,
							collB,
							std::move(contacts)
						};
					}
				}
				
				for (auto &m : manifolds)
				{
					auto &info(m.second);
					
					app.ecs().queueEvent<ECollision>
					(
						info.bodyA->entity,
						app.ecs().IDOfTypeComponent<Body>(),
						info.bodyA->component,
						((info.stay) ? ECollision::Type::stay : ECollision::Type::begin),
						info.bodyB->entity,
						info.bodyB->component,
						info.collA->component,
						info.collB->component,
						info.collA->componenttype,
						info.collB->componenttype,
						info.contacts
					);
				}
		
				for (auto it(m_data->manifolds.begin()); it != m_data->manifolds.end();)
				{
					const auto jt(manifolds.find(it->first));
					if (jt == manifolds.end())
					{
						auto &info(it->second);
						
						app.ecs().queueEvent<ECollision>
						(
							info.bodyA->entity,
							app.ecs().IDOfTypeComponent<Body>(),
							info.bodyA->component,
							ECollision::Type::end,
							info.bodyB->entity,
							info.bodyB->component,
							info.collA->component,
							info.collB->component,
							info.collA->componenttype,
							info.collB->componenttype,
							std::vector<ECollision::Contact>{}
						);
						
						it = m_data->manifolds.erase(it);
					}
					else
						++ it;
				}
			
				m_data->manifolds = std::move(manifolds);
			}
			
			// The actual component loop.
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);
				
				if (!c->activeInHierarchy())
					continue;
				
				auto body     (m_data->bodies.get(c->identifier()));
				auto transform(app.ecs().componentInEntity<gfx::Transform>(c->entity()));
				
				const auto &g(transform->global());
				
				// Update scale.
				/// @todo: Can we make it so we don't have to check physics scale every frame?
				{
					btVector3 const
						prev{body->colliders.getLocalScaling()},
						curr{g.scale.x, g.scale.y, g.scale.z};
					
					if (prev != curr)
					{
						body->colliders.setLocalScaling(curr);
						/// @todo: bulletWorld.updateSingleAabb() crashes so figure it out.
						//m_data->bulletWorld.updateSingleAabb(&body->rigidbody);
					}
				}

				// If the body is modified, we update the body from the transform.
				if (body->modified || c->m_modified)
				{
					body->modified = false;
					c->m_modified = false;
					
					m_data->bulletWorld.removeRigidBody(&body->rigidbody);
					
					btTransform t;
					t.setFromOpenGLMatrix(&g.matrix()[0][0]);
					body->rigidbody.setWorldTransform(t);
					
					btVector3 inertia;
					const btScalar mass{((c->kinematic()) ? std::max(0.1f, c->mass()) : 0.0f)};
					body->colliders.calculateLocalInertia(mass, inertia);
					body->rigidbody.setMassProps(mass, inertia);
					body->rigidbody.updateInertiaTensor();
					
					body->rigidbody.clearForces();
					body->rigidbody.setLinearVelocity({0.0f, 0.0f, 0.0f});
					body->rigidbody.setAngularVelocity({0.0f, 0.0f, 0.0f});
					
					body->rigidbody.setLinearFactor
					({
						((c->positionlock().x) ? 0.0f : 1.0f),
						((c->positionlock().y) ? 0.0f : 1.0f),
						((c->positionlock().z) ? 0.0f : 1.0f)
					});
					
					body->rigidbody.setAngularFactor
					({
						((c->rotationlock().x) ? 0.0f : 1.0f),
						((c->rotationlock().y) ? 0.0f : 1.0f),
						((c->rotationlock().z) ? 0.0f : 1.0f)
					});
					
					//body->rigidbody.setCenterOfMassTransform(o);
					
					m_data->bulletWorld.addRigidBody
					(
						&body->rigidbody,
						static_cast<int>(c->layer()),
						static_cast<int>(layermask(c->layer()))
					);
				}
				
				// If the body is not modified, we update the transform from the body.

				auto l(transform->local());

				btScalar m[16];
				body->rigidbody.getWorldTransform().getOpenGLMatrix(m);
				mth::Mat4f mat{mth::matrix4x4(m)};
				mth::decompose(mat, &l.position, &l.rotation, nullptr);
				l.rotation = mth::inverse(l.rotation);
				
				transform->local(std::move(l));
				//l.rotation = mth::inverse(l.rotation);
			}
		}
		
		void *SystemPhysics::body(const ecs::IDComponent &ID)
		{
			if (auto data = m_data->bodies.get(ID))
				return &data->rigidbody;
			
			return nullptr;
		}
		
		void *SystemPhysics::world()
		{
			return &m_data->bulletWorld;
		}
		
		void SystemPhysics::layermask(const Layer a, const Layers b, const bool set)
		{
			auto it(m_layermasks.find(a));
			
			if (it == m_layermasks.end())
				m_layermasks.emplace(a, ((set) ? b : 0));
			else if (set)
				it->second |= b;
			else
				it->second &= ~b;
			
			if (a != b)
			{
				auto jt(m_layermasks.find(b));
			
				if (jt == m_layermasks.end())
					m_layermasks.emplace(b, ((set) ? a : 0));
				else if (set)
					jt->second |= a;
				else
					jt->second &= ~a;
			}
		}
		
		Layers SystemPhysics::layermask(const Layer layer) const
		{
			const auto it(m_layermasks.find(layer));
			
			if (it != m_layermasks.end())
				return it->second;
			
			return 0;
		}
		
		void SystemPhysics::debugDrawBody
		(
			app::App &app,
			ecs::IDEntity const entity,
			gfx::ColRGBAf const &col
		)
		{
			auto const bodyEntity(app.ecs().componentInEntity<Body>(entity));
			
			if (!bodyEntity)
				return;
			
			Im3d::Color const colIm3D{col.r, col.g, col.b, col.a};
			constexpr float const thickness{4.0f};
			
			Im3d::PushSize(thickness);
			
			auto body     (m_data->bodies.get(bodyEntity->identifier()));
			auto transform(app.ecs().componentInEntity<gfx::Transform>(entity));
		
			for (std::size_t i{0}; i < body->colliders.getNumChildShapes(); ++ i)
			{
				Im3d::PushDrawState();
				{
					mth::Transformf t, tp;
					
					{
						btScalar floats[16];
						body->rigidbody.getWorldTransform().getOpenGLMatrix(floats);
						
						mth::decompose
						(
							mth::matrix4x4(floats),
							&tp.position,
							&tp.rotation,
							&tp.scale
						);
						
						tp.rotation = mth::inverse(tp.rotation);
					}
					
					{
						btScalar floats[16];
						body->colliders.getChildTransform(i).getOpenGLMatrix(floats);
						
						mth::decompose
						(
							mth::matrix4x4(floats),
							&t.position,
							&t.rotation,
							&t.scale
						);
						
						t.rotation = mth::inverse(t.rotation);
					}
					
					const mth::Mat4f model{tp.matrix() * t.matrix()};
					const Im3d::Mat4 M{mth::matrixIm3D(model)};
					
					Im3d::PushMatrix(M);
					{
						auto data(reinterpret_cast<const Data::DataCollider *>(body->colliders.getChildShape(i)->getUserPointer()));
						
						Im3d::SetColor(colIm3D);
						
						switch (data->type)
						{
							case Data::DataCollider::Type::box:
							{
								const btVector3
									min{data->box.getHalfExtentsWithMargin() * -1.0f},
									max{data->box.getHalfExtentsWithMargin()};
								
								Im3d::DrawAlignedBox
								(
									{min.x(), min.y(), min.z()},
									{max.x(), max.y(), max.z()}
								);
								
								break;
							}
							
							case Data::DataCollider::Type::sphere:
							{
								Im3d::DrawSphere({0.0f, 0.0f, 0.0f}, data->sphere.getRadius());
								break;
							}
							
							case Data::DataCollider::Type::capsule:
							{
								Im3d::DrawCapsule
								(
									{0.0f, data->capsule.getHalfHeight(), 0.0f},
									{0.0f, -data->capsule.getHalfHeight(), 0.0f},
									data->capsule.getRadius()
								);
								
								break;
							}
								
							case Data::DataCollider::Type::cylinder:
							{
								Im3d::DrawCylinder
								(
									{0.0f, data->cylinder.getHalfExtentsWithMargin().y(), 0.0f},
									{0.0f, -data->cylinder.getHalfExtentsWithMargin().y(), 0.0f},
									data->cylinder.getRadius()
								);
								
								break;
							}
							
							case Data::DataCollider::Type::mesh:
							{
								const auto &arr (data->mesh.tris->getIndexedMeshArray());
								const auto  vers(reinterpret_cast<const btScalar *>(arr[0].m_vertexBase));
								const auto  inds(reinterpret_cast<const int *>(arr[0].m_triangleIndexBase));
								
								for (int i{0}; i < (arr[0].m_numTriangles * 3); i += 3)
								{
									const int *ia{inds + i + 0}, *ib{inds + i + 1}, *ic{inds + i + 2};
									const btScalar *va{vers + *ia * 3}, *vb{vers + *ib * 3}, *vc{vers + *ic * 3};
									
									const Im3d::Vec3
										a
										{
											*(va + 0),
											*(va + 1),
											*(va + 2)
										},
										b
										{
											*(vb + 0),
											*(vb + 1),
											*(vb + 2)
										},
										c
										{
											*(vc + 0),
											*(vc + 1),
											*(vc + 2)
										};
									
									Im3d::DrawLine(a, b, thickness, colIm3D);
									Im3d::DrawLine(b, c, thickness, colIm3D);
									Im3d::DrawLine(c, a, thickness, colIm3D);
								}
								
								break;
							}
							
							default:
								break;
						}
					}
					Im3d::PopMatrix();
				}
				Im3d::PopDrawState();
			}
			
			Im3d::PopSize();
		}
		
		SystemPhysics::~SystemPhysics() {}
		
		SystemPhysics *SystemPhysics::c_instance;
	}
}
