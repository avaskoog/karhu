/**
 * @author		Ava Skoog
 * @date		2019-01-10
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_PHYSICS_SYSTEM_PHYSICS_H_
	#define KARHU_PHYSICS_SYSTEM_PHYSICS_H_

	#include <karhu/app/ecs/System.hpp>
	#include <karhu/app/phx/components/Body.hpp>
	#include <karhu/app/phx/common.hpp>

	namespace karhu
	{
		namespace phx
		{
			class SystemPhysics : public ecs::System<Body>
			{
				public:
					static void listenerBodyCreated              (app::App &, const ecs::EComponentCreated   &, const float, const float, const float, void *);
					static void listenerBodyDestroyed            (app::App &, const ecs::EComponentDestroyed &, const float, const float, const float, void *);
					static void listenerColliderBoxCreated       (app::App &, const ecs::EComponentCreated   &, const float, const float, const float, void *);
					static void listenerColliderBoxDestroyed     (app::App &, const ecs::EComponentDestroyed &, const float, const float, const float, void *);
					static void listenerColliderSphereCreated    (app::App &, const ecs::EComponentCreated   &, const float, const float, const float, void *);
					static void listenerColliderSphereDestroyed  (app::App &, const ecs::EComponentDestroyed &, const float, const float, const float, void *);
					static void listenerColliderCapsuleCreated   (app::App &, const ecs::EComponentCreated   &, const float, const float, const float, void *);
					static void listenerColliderCapsuleDestroyed (app::App &, const ecs::EComponentDestroyed &, const float, const float, const float, void *);
					static void listenerColliderCylinderCreated  (app::App &, const ecs::EComponentCreated   &, const float, const float, const float, void *);
					static void listenerColliderCylinderDestroyed(app::App &, const ecs::EComponentDestroyed &, const float, const float, const float, void *);
					static void listenerColliderMeshCreated      (app::App &, const ecs::EComponentCreated   &, const float, const float, const float, void *);
					static void listenerColliderMeshDestroyed    (app::App &, const ecs::EComponentDestroyed &, const float, const float, const float, void *);

				public:
					SystemPhysics();
					~SystemPhysics();

				protected:
					void performUpdateComponents
					(
						      app::App        &app,
						      ecs::Pool<Body> &components,
						const float            dt,
						const float            dtFixed,
						const float            timestep
					) override;
				
				private:
					static SystemPhysics *instance() noexcept { return c_instance; }
				
				private:
					void *world();
					void *body(const ecs::IDComponent &);
				
					void layermask(const Layer, const Layer, const bool);
					Layers layermask(const Layer) const;
				
					/// @todo: Quick fix to draw mesh in editor, clean up later
					void debugDrawBody(app::App &app, ecs::IDEntity const entity, gfx::ColRGBAf const &);
				
				private:
					static SystemPhysics *c_instance;
				
				private:
					struct Data;
					std::unique_ptr<Data> m_data;
					std::map<Layer, Layers> m_layermasks;
					bool m_paused{false};
				
				/// @todo: Ugly spaghetti. Work it out.
				friend class SubsystemPhysics;
			};
		}
	}
#endif
