/**
 * @author		Ava Skoog
 * @date		2019-01-17
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/phx/SubsystemPhysics.hpp>
#include <karhu/app/phx/systems/SystemPhysics.hpp>
#include <karhu/app/App.hpp>
#include <karhu/app/ecs/common.hpp>
#include <karhu/app/gfx/components/Transform.hpp>

#include <karhu/app/lib/bullet/btBulletDynamicsCommon.h>

namespace karhu
{
	namespace phx
	{
		void SubsystemPhysics::paused(bool const set)
		{
			SystemPhysics::instance()->m_paused = set;
		}
		
		bool SubsystemPhysics::paused() const
		{
			return SystemPhysics::instance()->m_paused;
		}
		
		void SubsystemPhysics::layermask(Layer const a, Layer const b, bool const set)
		{
			SystemPhysics::instance()->layermask(a, b, set);
		}
		
		Layers SubsystemPhysics::layermask(Layer const layer) const
		{
			return SystemPhysics::instance()->layermask(layer);
		}
		
		void SubsystemPhysics::velocityLinear(ecs::IDComponent const &ID, mth::Vec3f const &v)
		{
			auto s(SystemPhysics::instance());
			
			if (auto b = reinterpret_cast<btRigidBody *>(s->body(ID)))
			{
				b->setLinearVelocity({v.x, v.y, v.z});
				b->activate(true);
			}
		}
		
		mth::Vec3f SubsystemPhysics::velocityLinear(ecs::IDComponent const &ID)
		{
			auto s(SystemPhysics::instance());
			
			if (auto b = reinterpret_cast<btRigidBody *>(s->body(ID)))
			{
				const btVector3 &v{b->getLinearVelocity()};
				return {v.x(), v.y(), v.z()};
			}
			
			return {};
		}
		
		void SubsystemPhysics::position(ecs::IDComponent const &ID, mth::Vec3f const &v)
		{
			auto s(SystemPhysics::instance());
			
			if (auto b = reinterpret_cast<btRigidBody *>(s->body(ID)))
				b->getWorldTransform().setOrigin({v.x, v.y, v.z});
		}
		
		void SubsystemPhysics::rotation(ecs::IDComponent const &ID, mth::Quatf const &v)
		{
			auto s(SystemPhysics::instance());
			
			if (auto b = reinterpret_cast<btRigidBody *>(s->body(ID)))
				b->getWorldTransform().setRotation({v.x, v.y, v.z, v.w});
		}
		
		ResultCast SubsystemPhysics::castLine
		(
			mth::Vec3f const &from,
			mth::Vec3f const &to,
			Layers const mask
		)
		{
			ResultCast r;
			
			auto const world(reinterpret_cast<const btDynamicsWorld *>
			(
				SystemPhysics::instance()->world()
			));
			
			btVector3
				a{from.x, from.y, from.z},
				b{to  .x, to  .y, to  .z};
			
			// Bullet crashes if the positions are
			// identical, so this is a safeguard.
			constexpr btScalar epsilon{0.00001f};
			if (std::abs(a.length() - b.length()) <= epsilon)
				b += {epsilon, epsilon, epsilon};
			
			btCollisionWorld::ClosestRayResultCallback cb{a, b};
			
			if (mask > 0)
			{
				cb.m_collisionFilterGroup = -1;
				cb.m_collisionFilterMask = static_cast<int>(mask);
			}
			
			world->rayTest(a, b, cb);
			
			if ((r.hit = cb.hasHit()))
			{
				/// @todo: Add the rest of the linecast data.
				r.point =
				{
					cb.m_hitPointWorld.x(),
					cb.m_hitPointWorld.y(),
					cb.m_hitPointWorld.z()
				};
				
				r.normal =
				{
					cb.m_hitNormalWorld.x(),
					cb.m_hitNormalWorld.y(),
					cb.m_hitNormalWorld.z()
				};
			}
			
			return r;
		}
		/*
		ResultCast SubsystemPhysics::castSphere
		(
			const mth::Vec3f &from,
			const mth::Vec3f &to,
			const mth::Scalf  &diameter
		)
		{
			ResultCast r;
			
			auto const world(reinterpret_cast<const btDynamicsWorld *>
			(
				SystemPhysics::instance()->world()
			));
			
			btVector3
				a{from.x, from.y, from.z},
				b{to  .x, to  .y, to  .z};
			
			// Bullet crashes if the positions are
			// identical, so this is a safeguard.
			constexpr btScalar epsilon{0.00001f};
			if (std::abs(a.length() - b.length()) <= epsilon)
				b += {epsilon, epsilon, epsilon};
			
			btCollisionWorld::ClosestConvexResultCallback cb{a, b};
			const btSphereShape shape{diameter * 0.5f};
			
			btTransform ta, tb;
			ta.setIdentity();
			tb.setIdentity();
			ta.setOrigin(a);
			tb.setOrigin(b);
			
			world->convexSweepTest(&shape, ta, tb, cb);
			
			if ((r.hit = cb.hasHit()))
			{
				/// @todo: Add the rest of the spherecast data.
			}
			
			return r;
		}*/
		
		void SubsystemPhysics::debugDrawBody
		(
			app::App &app,
			ecs::IDEntity const entity,
			gfx::ColRGBAf const &col
		)
		{
			return SystemPhysics::instance()->debugDrawBody(app, entity, col);
		}
		
		void SubsystemPhysics::logVersions()
		{
			const std::string v{std::to_string(btGetVersion())};
			log::msg("Karhu") << "Bullet " << v[0] << '.' << v.substr(1, v.length() - 1);	
		}
	}
}
