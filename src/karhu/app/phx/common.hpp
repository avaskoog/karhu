/**
 * @author		Ava Skoog
 * @date		2019-01-07
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_PHYSICS_COMMON_H_
	#define KARHU_PHYSICS_COMMON_H_

	#include <cstdint>
	#include <cstddef>

	namespace karhu
	{
		namespace phx
		{
			// Layers are specified as bitflags so
			// that multiple can be specified at once.
			using Layer  = std::uint16_t;
			using Layers = Layer;
			
			struct Defaultlayers
			{
				enum : Layer
				{
					none     = 0,
					standard = 1,
					all      = std::numeric_limits<Layer>::max()
				};
			};
			
			/**
			 * Returns a valid bitflag for the layer at the specified index.
			 *
			 * @param index The index of the layer, starting from zero.
			 *
			 * @return A valid bitflag for a single layer.
			 */
			inline constexpr Layer layer(const std::size_t &index) noexcept
			{
				return static_cast<Layer>((Defaultlayers::standard + 1) << index);
			}
		}
	}
#endif
