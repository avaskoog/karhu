/**
 * @author		Ava Skoog
 * @date		2019-01-10
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/phx/components/ColliderCylinder.hpp>
#include <karhu/data/serialise.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/ecs/World.hpp>
#include <karhu/app/gfx/components/Transform.hpp>
#include <karhu/app/App.hpp>
#include <karhu/app/karhuimgui.hpp>
#endif

namespace karhu
{
	namespace phx
	{
		void ColliderCylinder::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("origin",   m_origin)
				<< karhuINn("offset",   m_offset)
				<< karhuINn("diameter", m_diameter)
				<< karhuINn("height",   m_height)
				<< karhuINn("rotation", m_rotation);
		}

		void ColliderCylinder::deserialise(conv::Serialiser const &ser)
		{
			ser
				>> karhuOUTn("origin",   m_origin)
				>> karhuOUTn("offset",   m_offset)
				>> karhuOUTn("diameter", m_diameter)
				>> karhuOUTn("height",   m_height)
				>> karhuOUTn("rotation", m_rotation);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool ColliderCylinder::editorEdit(app::App &app, edt::Editor &editor)
		{
			bool r{false};
			
			ImGui::DragFloat3("Origin", &m_origin.x);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			ImGui::DragFloat3("Offset", &m_offset.x);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			ImGui::DragFloat("Diameter", &m_diameter);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			ImGui::DragFloat("Height", &m_height);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			/// @todo: Collider rotation editor.
			
			/// @todo: Take rotation, and transform rotation, into account; probably want to be able to push editor matrices.
			if (auto const transform = app.ecs().componentInEntity<gfx::Transform>(entity()))
			{
				mth::Vec3f const
					halfsize{0.0f, m_height * 0.5f, 0.0f},
					pos     {transform->global().position - m_origin + m_offset},
					start   {pos - halfsize},
					end     {pos + halfsize};
				
				editor.pushColour({0.0f, 1.0f, 1.0f, 0.75f});
				editor.pushThickness(6.0f);
				editor.cylinderInViewport(start, end, m_diameter);
				editor.popThickness();
				editor.popColour();
			}
			
			return r;
		}
		#endif
	}
}
