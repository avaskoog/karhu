/**
 * @author		Ava Skoog
 * @date		2019-02-25
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/phx/components/ColliderMesh.hpp>
#include <karhu/data/serialise.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/ecs/World.hpp>
#include <karhu/app/gfx/components/Transform.hpp>
#include <karhu/app/App.hpp>
#include <karhu/res/Bank.hpp>
#include <karhu/app/karhuimgui.hpp>
#endif

namespace karhu
{
	namespace phx
	{
		void ColliderMesh::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("mesh",     m_mesh)
				<< karhuINn("origin",   m_origin)
				<< karhuINn("offset",   m_offset)
				<< karhuINn("rotation", m_rotation);
		}

		void ColliderMesh::deserialise(conv::Serialiser const &ser)
		{
			ser
				>> karhuOUTn("mesh",     m_mesh)
				>> karhuOUTn("origin",   m_origin)
				>> karhuOUTn("offset",   m_offset)
				>> karhuOUTn("rotation", m_rotation);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool ColliderMesh::editorEdit(app::App &app, edt::Editor &editor)
		{
			bool r{false};
			
			if (editor.pickerResource("_phx-coll-mesh-mesh", "Mesh", m_mesh, app.res().IDOfType<res::Mesh>()))
				r = true;
			
			ImGui::DragFloat3("Origin", &m_origin.x);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			ImGui::DragFloat3("Offset", &m_offset.x);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			/// @todo: Collider rotation editor.
			
			/// @todo: Take rotation, and transform rotation, into account; probably want to be able to push editor matrices.
			/// @todo: Draw mesh collider in editor viewport.
			
			auto const
				mesh(app.res().get<res::Mesh>(m_mesh));
			
			if (mesh && mesh->valid())
			{
				auto const
					transform(app.ecs().componentInEntity<gfx::Transform>(entity()));
				
				if (transform)
				{
					auto const
						geom(mesh->geometryAtLOD(0));
					
					if (geom && geom->indices.size() > 1)
					{
						Im3d::PushDrawState();
						Im3d::PushMatrix(mth::matrixIm3D(transform->global().matrix()));
						
						mth::Vec3f const
							pos{m_offset - m_origin};
						
						for (std::size_t i{0}; i < geom->indices.size(); i += 3)
						{
							for (std::size_t j{0}; j < 3; ++ j)
							{
								gfx::Index const
									curr{geom->indices[i + j]},
									next{geom->indices[i + ((2 == j) ? 0 : (j + 1))]};
								
								mth::Vec3f const
									&a{geom->positions[curr]},
									&b{geom->positions[next]};
								
								Im3d::DrawLine
								(
									{pos.x + a.x, pos.y + a.y, pos.z + a.z},
									{pos.x + b.x, pos.y + b.y, pos.z + b.z},
									6.0f,
									{0.0f, 1.0f, 1.0f, 0.75f}
								);
							}
						}
						
						Im3d::PopMatrix();
						Im3d::PopDrawState();
					}
				}
			}
			
			return r;
		}
		#endif
	}
}
