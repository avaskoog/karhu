/**
 * @author		Ava Skoog
 * @date		2019-01-10
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/phx/components/ColliderBox.hpp>
#include <karhu/data/serialise.hpp>

#include <sstream>

#if KARHU_EDITOR_SERVER_SUPPORTED
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/ecs/World.hpp>
#include <karhu/app/gfx/components/Transform.hpp>
#include <karhu/app/App.hpp>
#include <karhu/app/karhuimgui.hpp>
#endif

namespace karhu
{
	namespace phx
	{
		void ColliderBox::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("origin",   m_origin)
				<< karhuINn("offset",   m_offset)
				<< karhuINn("size",     m_size)
				<< karhuINn("rotation", m_rotation);
		}

		void ColliderBox::deserialise(conv::Serialiser const &ser)
		{
			ser
				>> karhuOUTn("origin",   m_origin)
				>> karhuOUTn("offset",   m_offset)
				>> karhuOUTn("size",     m_size)
				>> karhuOUTn("rotation", m_rotation);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool ColliderBox::editorEdit(app::App &app, edt::Editor &editor)
		{
			bool r{false};
			
			ImGui::DragFloat3("Origin", &m_origin.x);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			ImGui::DragFloat3("Offset", &m_offset.x);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			ImGui::DragFloat3("Size", &m_size.x);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			/// @todo: Collider rotation editor.
			
			/// @todo: Take rotation, and transform rotation, into account; probably want to be able to push editor matrices.
			if (auto const transform = app.ecs().componentInEntity<gfx::Transform>(entity()))
			{
				mth::Vec3f
					min{transform->global().position - m_origin + m_offset},
					max{min};
				
				min -= 0.5f * m_size;
				max += 0.5f * m_size;
				
				editor.pushColour({0.0f, 1.0f, 1.0f, 0.75f});
				editor.pushThickness(6.0f);
				editor.boxInViewport(min, max);
				editor.popThickness();
				editor.popColour();
			}
			
			return r;
		}
		#endif
	}
}
