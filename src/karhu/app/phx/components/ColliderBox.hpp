/**
 * @author		Ava Skoog
 * @date		2019-01-10
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_PHYSICS_COLLIDER_BOX_H_
	#define KARHU_PHYSICS_COLLIDER_BOX_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
		namespace phx
		{
			class ColliderBox : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
					void serialise(conv::Serialiser &) const;
					void deserialise(conv::Serialiser const &);
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
                    void editorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) override {}
                    bool editorEdit(app::App &, edt::Editor &) override;
                    #endif
				
				public:
					void origin  (mth::Vec3f const &v) { m_origin   = v; }
					void offset  (mth::Vec3f const &v) { m_offset   = v; }
					void size    (mth::Vec3f const &v) { m_size     = v; }
					void rotation(mth::Quatf const &v) { m_rotation = v; }
				
					mth::Vec3f const &origin()   const noexcept { return m_origin; }
					mth::Vec3f const &offset()   const noexcept { return m_offset; }
					mth::Vec3f const &size()     const noexcept { return m_size; }
					mth::Quatf const &rotation() const noexcept { return m_rotation; }
				
				private:
					mth::Vec3f
						m_origin{0.0f, 0.0f, 0.0f},
						m_offset{0.0f, 0.0f, 0.0f},
						m_size  {1.0f, 1.0f, 1.0f};
				
					mth::Quatf m_rotation{1.0f, 0.0f, 0.0f, 0.0f};
			};
		}
	}
#endif
