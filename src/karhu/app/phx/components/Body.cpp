/**
 * @author		Ava Skoog
 * @date		2019-01-10
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/phx/components/Body.hpp>
#include <karhu/data/serialise.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/karhuimgui.hpp>
#include <karhu/app/edt/dropdown.inl>
#endif

namespace karhu
{
	namespace phx
	{
		void Body::layer(Layer const layer)
		{
			if (layer != m_layer)
			{
				m_layer    = layer;
				m_modified = true;
			}
		}
		
		void Body::kinematic(bool const set)
		{
			if (m_kinematic != set)
			{
				m_kinematic = set;
				m_modified  = true;
			}
		}
		
		void Body::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("layer",        m_layer)
				<< karhuINn("kinematic",    m_kinematic)
				<< karhuINn("mass",         m_mass)
				<< karhuINn("positionlock", m_positionlock)
				<< karhuINn("rotationlock", m_rotationlock);
		}

		void Body::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUTn("layer",        m_layer)
				>> karhuOUTn("kinematic",    m_kinematic)
				>> karhuOUTn("mass",         m_mass)
				>> karhuOUTn("positionlock", m_positionlock)
				>> karhuOUTn("rotationlock", m_rotationlock);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Body::editorEdit(app::App &app, edt::Editor &)
		{
			bool r{false};
			
			if (edt::dropdownLayerPhysics(app, m_layer))
				r = true;
			
			if (ImGui::Checkbox("Kinematic", &m_kinematic))
				r = true;
			
			ImGui::DragFloat("Mass", &m_mass);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			ImGui::TextUnformatted("Lock position");
			
			ImGui::PushID("_phx-body-pl");
			
			if (ImGui::Checkbox("X", &m_positionlock.x))
				r = true;
			
			ImGui::SameLine();
			
			if (ImGui::Checkbox("Y", &m_positionlock.y))
				r = true;
			
			ImGui::SameLine();
			
			if (ImGui::Checkbox("Z", &m_positionlock.z))
				r = true;
			
			ImGui::PopID();
			
			ImGui::TextUnformatted("Lock rotation");
			
			ImGui::PushID("_phx-body-rl");
			
			if (ImGui::Checkbox("X", &m_rotationlock.x))
				r = true;
			
			ImGui::SameLine();
			
			if (ImGui::Checkbox("Y", &m_rotationlock.y))
				r = true;
			
			ImGui::SameLine();
			
			if (ImGui::Checkbox("Z", &m_rotationlock.z))
				r = true;
			
			ImGui::PopID();
			
			if (r)
				m_modified = true;
			
			return r;
		}
		#endif
	}
}
