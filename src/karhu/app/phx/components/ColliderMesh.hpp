/**
 * @author		Ava Skoog
 * @date		2019-02-25
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_PHYSICS_COLLIDER_MESH_H_
	#define KARHU_PHYSICS_COLLIDER_MESH_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/res/Bank.hpp>
	#include <karhu/res/Mesh.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
		namespace phx
		{
			class ColliderMesh : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
					void serialise(conv::Serialiser &) const;
					void deserialise(conv::Serialiser const &);
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
                    void editorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) override {}
                    bool editorEdit(app::App &, edt::Editor &) override;
                    #endif
				
				public:
					res::IDResource const &mesh()     const noexcept { return m_mesh; }
					mth::Vec3f      const &origin()   const noexcept { return m_origin; }
					mth::Vec3f      const &offset()   const noexcept { return m_offset; }
					mth::Quatf      const &rotation() const noexcept { return m_rotation; }
				
				private:
					res::IDResource m_mesh{0};
				
					mth::Vec3f
						m_origin{0.0f, 0.0f, 0.0f},
						m_offset{0.0f, 0.0f, 0.0f};
				
					mth::Quatf m_rotation{1.0f, 0.0f, 0.0f, 0.0f};
			};
		}
	}
#endif
