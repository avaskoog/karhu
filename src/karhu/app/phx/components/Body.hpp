/**
 * @author		Ava Skoog
 * @date		2019-01-10
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_PHYSICS_BODY_H_
	#define KARHU_PHYSICS_BODY_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/phx/common.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
		namespace phx
		{
			class Body : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);

					#if KARHU_EDITOR_SERVER_SUPPORTED
                    void editorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) override {}
                    bool editorEdit(app::App &, edt::Editor &) override;
                    #endif
				
				public:
					void layer(Layer const layer);
					Layer layer() const noexcept { return m_layer; }
				
					void kinematic(bool const set);
					bool kinematic() const noexcept { return m_kinematic; }
				
					mth::Scalf mass() const noexcept { return m_mass; }
				
					mth::Vec3<bool> const &positionlock() const noexcept { return m_positionlock; }
					mth::Vec3<bool> const &rotationlock() const noexcept { return m_rotationlock; }
				
				private:
					Layer
						m_layer{phx::layer(0)};
				
					bool
						m_kinematic{true};
				
					mth::Scalf
						m_mass{1.0f};
				
					mth::Vec3<bool>
						m_positionlock{false, false, false},
						m_rotationlock{false, false, false};
				
				private:
					bool m_modified{false};
				
				friend class SystemPhysics;
			};
		}
	}
#endif
