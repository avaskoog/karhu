/**
 * @author		Ava Skoog
 * @date		2019-01-17
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_PHYSICS_SUBSYSTEM_PHYSICS_H_
	#define KARHU_PHYSICS_SUBSYSTEM_PHYSICS_H_

	#include <karhu/app/Subsystem.hpp>
	#include <karhu/app/Bitflagbank.hpp>
	#include <karhu/app/phx/components/Body.hpp>
	#include <karhu/app/phx/common.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/maths.hpp>
	#include <karhu/conv/graphics.hpp>

	#include <map>
	#include <string>

	namespace karhu
	{
		namespace phx
		{
			class ColliderMesh;
			
			struct ResultCast
			{
				bool       hit  {false};
				mth::Vec3f point;
				mth::Vec3f normal;
			};
			
			class Layerbank : public app::Bitflagbank<Layer, Defaultlayers::standard + 1, Defaultlayers::none>
			{
				public:
					char const *label()      override { return "layers"; }
					char const *descriptor() override { return "physics layer"; }
			};
			
			class SubsystemPhysics : public subsystem::Subsystem
			{
				public:
					void paused(bool const);
					bool paused() const;
				
					Layerbank &layers() noexcept { return m_layers; }
					Layerbank const &layers() const noexcept { return m_layers; }
				
					void layermask(Layer const, Layer const, const bool);
					Layers layermask(Layer const) const;
				
					// @todo: Replace all the functions that take a component with setters on the physics body components etc and add a dirty flag instead.
				
					void velocityLinear(ecs::IDComponent const &, mth::Vec3f const &);
					mth::Vec3f velocityLinear(ecs::IDComponent const &);
				
					void position(ecs::IDComponent const &, mth::Vec3f const &);
					void rotation(ecs::IDComponent const &, mth::Quatf const &);
				
					/// @todo: Add cast masking options and whether to get all or only nearest.
					ResultCast castLine
					(
						mth::Vec3f const &from,
						mth::Vec3f const &to,
						Layers const mask = 0
					);
				
					/// @todo: Add cast masking options and whether to get all or only nearest.
					/*ResultCast castSphere
					(
						const mth::Vec3f &from,
						const mth::Vec3f &to,
						const mth::Scalf  &diameter
					);*/
				
				public:
					/// @todo: Quick fix to draw mesh in editor, clean up later
					void debugDrawBody(app::App &app, ecs::IDEntity const, gfx::ColRGBAf const &);

				public:
					void logVersions() override;
				
				private:
					Layerbank m_layers;
			};
		}
	}
#endif
