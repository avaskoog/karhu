/**
 * @author		Ava Skoog
 * @date		2021-06-11
 * @copyright   2017-2023 Ava Skoog
 */

/*
 * Contains functions for exporting script info for outside tools.
 * The bound API can be converted to C++ to edit AngelScript with
 * C++ tooling, e.g. using the clangproxy script in KARHU_HOME.
 */

#ifndef KARHU_APP_SCRIPTUTIL_H_
	#define KARHU_APP_SCRIPTUTIL_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/core/macros.hpp>
	#include <ostream>

	class asIScriptEngine;

	namespace karhu
	{		
		namespace app
		{
			class App;
			
			namespace scriptutil
			{
				enum class FlagsOutput
				{
					none,
					includeMagic
				};
				
				karhuBITFLAGGIFY(FlagsOutput)
				
				bool outputMagicCPP(std::ostream &outStream);
				bool outputBoundAPIAsCPP(asIScriptEngine const &engine, std::ostream &outStream, FlagsOutput const = FlagsOutput::none);
				
				void refreshScript(App &, res::IDResource const);
			}
		}
	}
#endif
