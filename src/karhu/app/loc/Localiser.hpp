/**
 * @author		Ava Skoog
 * @date		2023-01-18
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_LOCALISER_H_
	#define KARHU_APP_LOCALISER_H_

	#include <karhu/data/serialise.hpp>
	#include <karhu/conv/serialise.hpp>

	#include <map>
	#include <vector>
	#include <string>
	#include <memory>
	#include <cstdint>

	namespace karhu
	{
		namespace app
		{
			class App;
		}
		
		namespace loc
		{
			struct Form
			{
				std::string
					key;
			};
			
			using String   = std::string;
			using Integer  = std::uint64_t;
			using Floating = double;
			
			enum class TypeToken
			{
				none,
				string,
				input,
				condition
				// TODO: …
			};
			
			class Token
			{
				// TODO: Finish this
				public:
					std::string
						key;
				
					TypeToken
						type;
				
					std::vector<Token>
						values;
			};
			
			class Format
			{
				public:
					// TODO: Only need the original format to start with so will not actually be parsing and filling in any tokens but might not want the original format stored in practice later.
					std::string
						format;
				
					std::vector<Token>
						tokens;
			};
			
			enum class TypeArg
			{
				none,
				form,
				string,
				integer,
				floating
			};
			
			class Arg
			{
				public:
					Arg() = default;
					Arg(Form);
					Arg(String);
					Arg(Integer);
					Arg(Floating);
					Arg(Arg const &);
					Arg &operator=(Arg const &);
					~Arg();
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
				
				private:
					void deinit();
				
				private:
					TypeArg
						m_type{TypeArg::none};
				
					union
					{
						Form
							m_form;
						
						String
							m_string;
						
						Integer
							m_integer;
						
						Floating
							m_floating;
					};
			};
			
			using Args = std::map<std::string, Arg>;
			
			class Localisation
			{
				public:
					Localisation() = default;
					Localisation(std::string const &key, Args const &args = {}, std::string const &locale = {});
				
					void key(std::string);
					std::string const &key() const noexcept { return m_key; }
				
					void args(Args);
					Args const &args() const noexcept { return m_args; }
				
					void locale(std::string);
					std::string const &locale() const noexcept { return m_locale; }
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
				
				private:
					std::string
						m_key,
						m_locale;
				
					Args
						m_args;
			};
			
			class Localiser
			{
				// velja forinnstilt språk
				// få streng etter streng-ID på valt språk
				
				// kunna signalera at språk er endra for å oppdatera alt som er synleg på skjermen no?
				// kanskje det skal vera komponent, slik at ting med «lokalisert»-komponent får signal
				// eller rett og slett at det er som ein ressurstype, eller begge?
				// dvs GUI-tekstkomponenten har alltid verdi som kan peika på lokaliseringsverdi (men som kan rundtgåast)
				// men kanskje det er akkurat det, at Locref er typen i komponenten,
				// _og_ kan nyttast på andre stader, td om ein vil lagra i fil eller w/e
				
				// kan rett og slett vera shared_ptr no når eg veit at det er goa greier
				
				// TODO: null = aktiv lokal, tom streng = alle lokalar?
				
				public:
					Localiser(app::App &, std::string const &locale = {});
					void clearModules();
				
					// TODO: Argument startRange og endRange for å lasta inn berre del av viss CSV?
					bool loadFromCSVInMemory(std::string const &CSV, char const *const locale = nullptr, std::string const &module = {});
					bool unloadModule(std::string const &module, char const *const locale = nullptr);
				
					bool set(std::string const &key, std::string const &format, char const *const locale = nullptr);
					std::string get(std::string const &key, Args const &args, char const *const locale = nullptr);
					std::string get(Localisation const &localisation);
				
					void locale(std::string code);
					std::string const &locale() const noexcept { return m_locale; }
					std::vector<std::string> const &locales() const noexcept { return m_locales; }
				
				private:
					char const *localeResolved(char const *const) const noexcept;
				
				private:
					using Formats         = std::map<std::string, Format>;
					using FormatsByLocale = std::map<std::string, Formats>;
					using KeysInModule    = std::vector<std::string>;
					using Modules         = std::map<std::string, KeysInModule>;
				
					app::App
						&m_app;
				
					std::string
						m_locale{"en_GB"};
				
					std::vector<std::string>
						m_locales;
				
					FormatsByLocale
						m_formatsByLocale;
				
					Modules
						m_modules;
			};
		}
	}
#endif
