/**
 * @author		Ava Skoog
 * @date		2023-01-20
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_LOC_ELOCALISATION_H_
	#define KARHU_LOC_ELOCALISATION_H_

	#include <karhu/app/ecs/common.hpp>

	namespace karhu
	{
		namespace loc
		{
			class ELocalisation : public ecs::Event
			{
				public:
					ELocalisation(const ecs::IDEvent &identifier)
					:
					ecs::Event{identifier, 0, 0, 0}
					{
					}
				
				public:
			};
		}
	}
#endif
