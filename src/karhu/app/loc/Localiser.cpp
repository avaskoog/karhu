/**
 * @author		Ava Skoog
 * @date		2023-01-18
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/loc/Localiser.hpp>
#include <karhu/core/log.hpp>

/*
Idé for kategoriar:
Har jo allereie syntaks som %[Suomi, Suomessa]
Og ting som %{0;1,2} osfr.
Så kanskje kan ha %[0,4,1;Stefan] for å leggja Stefan i fleire kategoriar…
Eller t.o.m. %[MASC;Stefan] for å gjera det lettare å lesa.
Og så for ei omsetning kan ein basera på kategori, td.
%[%{MASC;0} er liten][%{FEM;0} er lita]
elns for å velja basert på forma som er mata inn, men betre syntaks…
Men korleis med fleire ting i same setning?
Ein/Ei/Eit liten/lita/lite %{0} seier at at %{1} er liten/lita/lite.
Så det er betre å ha vilkår rundt kvart ord:
%[0=MASC:Ein;FEM:Ei;NEUT:Eit] %[0=MASC:liten;FEM:lita;NEUT:lite] %{0} seier at %{1} er %[1=…
Trur det funkar bra. Ok, treng ikkje å gjera noko ekstra her då.
Kan vera bra om kvart element i ei liste kan ha sin eigen kategori òg,
t.d. om ord endrar gruppe mellom eintal og fleirtal og sånt.
%[braccio:MASC,braccia:FEM] eller %[Björn, Björn, Birni, Bjarnar]:MASC eller Hund:MASC
sånn at :-syntaksen konsekvent kan brukast for alt…

Går dette an å kombinera med det andre gamle nummersystemet for å td velja preposisjon for stad?

       | no_NN                           | is
IT_WAS | Det var %(0#PLACE_ON:på;i) %{0} | Það var %(0#PLACE_ON:á;í) %{0#DAT}

Kombinert med

		| no_NN           | is
NORWAY  | Noreg@PLACE_IN  | %[Noregur@NOM;Noreg@ACC;Noregi@DAT;Noregs@GEN]@PLACE_IN
ICELAND | Island@PLACE_ON | %[Ísland@NOM@ACC;Íslandi@DAT;Íslands@GEN]@PLACE_ON

Og så endå eit steg vanskelegare med preposisjonar som styrer ulike fall:

Það var %(0@PLACE_ON:á;@PLACE_IN:í;við) %(0@PLACE_ON@PLACE_IN:%{0@DAT};%{0@ACC})

Það var á Íslandi  (@PLACE_ON, @DAT)
Það var í Noregi   (@PLACE_IN, @DAT)
Það var við sjóinn (@ACC)

Okei, dette blir bra… Og nummersyntaksen?

Du har #{0:ven;vener}

Og for resten ordinale nummer hadde vore bra, dobbel ## elns? #ORD{}?
Betre å gjera det generisk slik at ein kan laga så mange reglar ein vil, med tagg?
Dvs. #NUM, #ORD, #ALPHANUM, #ALPHAORD osfr.

Den #ORD{0;dagen} i Blåfjell

Kan i sin tur gjerast endå ålmennare, slik at ein kan huka inn logikk òg for streng osfr?

Min %EIGEN{0} logikk

Kan vera at argumenta går inn i gjenkallet òg istf at funksjonen berre returnerer nummer?
Betre å returnera streng, som t.o.m. kan operera på verdiane om nødvendig?

Då trengst ikkje nokon hardkoda kjennskap om rekkjefylgd og sånt heller, kan berre gjera:

callbackNUM(val, args[]) -> str
{
	// tal før namnord
	return val . ' ' . args[0];
}

callbackNUM(val, args[]) -> str
{
	// namnord før tal
	return args[0] . ' ' . val;
}

Hadde ein kjent seg ekstra fænçy hadde det gått an å kopla
til skript osfr. men det gjer eg i neste motor isf…
Eller det er kanskje best å ordna for så vidt,
slik at inkje må kompilerast om for å leggja til nye omsetningar i framtida.

Går det an å unngå gjenteken XML?
For den saks skyld, FORM-er rett i formateringa?

Det var <col=%{COL_HINT}>…</col>

Kan òg vera variablar frå bank som kan setjast ved køyring istf fila

Det var <col=${COL_HINT}>…</col>

Og for resten heil formatering?

$<HINT>…</HINT>

elns, for å få heile taggen frå bank ved køyring,
og då kan ein berre gjenta den same variabelen i omsetninga,
så slepp me gjera det supervanskeleg med indekserte XML-taggar,
og sleppa forvirringa med vanlege indeks…

Kanskje ikkje treng syntaks for taggar sånn heller då,
sidan taggar ikkje faktisk er eit konsept lokaliseringa
kjenner til, det er berre noko eg personleg brukar med kirja…
SÅÅÅ det kan vera funksjonssyntaks det òg, eigentleg?
Dvs %HINT{…} -> '<col=red>' . val . '</col>'
Einaste problem med det er kanskje at ting plutseleg vert rekursive,
kanskje vil unngå det, men no er tanken lagra her i kvart fall.

Eit anna spørsmål, sidan det allereie er gått over mest til nøklar
over indeks, skal eg gjera det med siste gjenverande òg?

IT_WAS | Det var %(LOC@PLACE_ON:på;i) %{LOC} | Það var %(LOC@PLACE_ON:á;í) %{LOC@DAT}

Så er det tydeleg i koden som set argumenta ke som er ke,
localiser.get("IT_WAS", {{"LOC", "ICELAND"_form}})
*/

namespace karhu
{
	namespace loc
	{
		Arg::Arg(Form value)
		:
		m_type{TypeArg::form}
		{
			new(&m_form) Form{std::move(value)};
		}

		Arg::Arg(String value)
		:
		m_type{TypeArg::string}
		{
			new(&m_string) String{std::move(value)};
		}

		Arg::Arg(Integer value)
		:
		m_type{TypeArg::integer}
		{
			m_integer = value;
		}

		Arg::Arg(Floating value)
		:
		m_type{TypeArg::floating}
		{
			m_floating = value;
		}
		
		Arg::Arg(Arg const &o)
		{
			operator=(o);
		}
		
		Arg &Arg::operator=(Arg const &o)
		{
			deinit();
			
			switch ((m_type = o.m_type))
			{
				case TypeArg::form:
					new(&m_form) Form{o.m_form};
					break;
					
				case TypeArg::string:
					new(&m_string) String{o.m_string};
					break;
					
				case TypeArg::integer:
					m_integer = o.m_integer;
					break;
					
				case TypeArg::floating:
					m_floating = o.m_floating;
					break;
					
				case TypeArg::none:
					break;
			}
			
			return *this;
		}

		Arg::~Arg()
		{
			deinit();
		}

		void Arg::serialise(conv::Serialiser &) const
		{
			// TODO: Serialise loc::Arg
		}
		
		void Arg::deserialise(const conv::Serialiser &)
		{
			// TODO: Deserialise loc::Arg
		}
		
		void Arg::deinit()
		{
			switch (m_type)
			{
				case TypeArg::form:
					m_form.~Form();
					break;
					
				case TypeArg::string:
					m_string.~String();
					break;
					
				case TypeArg::integer:
				case TypeArg::floating:
				case TypeArg::none:
					break;
			}
		}
		
		Localisation::Localisation
		(
			std::string const &key,
			Args const &args,
			std::string const &locale
		)
		:
		m_key   {key},
		m_locale{locale},
		m_args  {args}
		{
		}
		
		void Localisation::key(std::string key)
		{
			m_key = std::move(key);
		}
		
		void Localisation::args(Args args)
		{
			m_args = std::move(args);
		}
		
		void Localisation::locale(std::string code)
		{
			m_locale = std::move(code);
		}

		void Localisation::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("key",    m_key)
				<< karhuINn("locale", m_locale);
			// TODO: Serialise m_args
		}
		
		void Localisation::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUTn("key",    m_key)
				>> karhuOUTn("locale", m_locale);
			// TODO: Deserialise m_args
		}
		
		Localiser::Localiser(app::App &app, std::string const &locale)
		:
		m_app{app}
		{
			if (!locale.empty())
				m_locale = locale;
		}
		
		void Localiser::clearModules()
		{
			m_formatsByLocale.clear();
			m_modules.clear();
			m_locales.clear();
		}
		
		// TODO: Be able to load a set of locales at once.
		// TODO: Be able to load all locales at once if locale is empty string (not null).
		// TODO: Add function that only does the reading of the first line to detect locales without loading any? Or is that unnecessary since even a settings screen would want to load default labels?
		bool Localiser::loadFromCSVInMemory
		(
			std::string const &      CSV,
			char        const *const locale,
			std::string const &      module
		)
		{
			if (CSV.empty())
			{
				log::err() << "loccsv err 1\n"; // TODO: Slett
				return false;
			}
			
			// linje for linje (rad)
			// komma for komma (selle/kolonne)
			// linje startar med " om , eller " er inni
			// om " er inni er det dobbel ""
			
			// fyrste linja reservert for lokal
			// fyrste kolonnen reservert for nøkkel
			// fyrste selle alltid tom og unytta
			
			// TODO: Error-check. Match line count to first line's.
			auto const
				getColumnsInLineNext
				(
					[](char const *&c, std::vector<std::string> &outColumn) -> bool
					{
						if (!c || !*c)
							return false;
						
						outColumn.clear();
						
						std::string
							string;
						
						bool
							isEscaped{false};
						
						for (; *c; ++ c)
						{
							if (',' == *c && !isEscaped)
							{
								outColumn.emplace_back(string);
								string.clear();
							}
							else if ('\n' == *c || '\r' == *c)
							{
								if ('\r' == *c && '\n' == *(c + 1))
									++ c;
								else if (!isEscaped)
								{
									++ c;
									outColumn.emplace_back(string);
									break;
								}
								else
									// We never want \r internally.
									string += '\n';
							}
							else if ('"' == *c)
							{
								if (!isEscaped)
									isEscaped = true;
								else if ('"' == *(c + 1))
								{
									string += *c;
									++ c;
								}
								else
									isEscaped = false;
							}
							else
								string += *c;
						}
						
						if (!*c)
							outColumn.emplace_back(string);
						
						return true;
					}
				);
			
			std::vector<std::string>
				lineFirst,
				outColumn;
			
			char const
				*inoutCursor{CSV.c_str()};
			
			if (!getColumnsInLineNext(inoutCursor, lineFirst))
			{
				log::err() << "loccsv err 2"; // TODO: Slett
				return false;
			}
			
			if (1 == lineFirst.size())
			{
				log::err() << "loccsv err 3"; // TODO: Slett
				return false;
			}
			
//			{
//				std::stringstream ss;
//				ss << "CSV line 1 size " << lineFirst.size() << ":\n";
//				for (auto const &v : lineFirst)
//					ss << '\t' << v << '\n';
//				log::msg("Karhu") << ss.str();
//			}
			
			std::string const
				code(localeResolved(locale));
			
//			log::msg("Karhu") << "CSV locale code: " << code;
			
			std::size_t
				column{0};
			
			// First cell always empty to have one column of keys.
			for (std::size_t i{1}; i < lineFirst.size(); ++ i)
			{
				m_locales.emplace_back(lineFirst[i]);
				
				if (code == lineFirst[i])
					column = i;
			}
			
			if (0 == column)
			{
				log::err() << "loccsv err 4"; // TODO: Slett
				return false;
			}
			
			while (getColumnsInLineNext(inoutCursor, outColumn))
			{
//				std::stringstream ss;
//				ss << "CSVKOLONNE:";
//				for (auto const &v : outColumn) ss << ' ' << v;
//				log::msg("Karhu") << ss.str();
				
				// TODO: Fine for this to be a soft, ignored error? Probably not since it's a CSV syntax error.
				if (column >= outColumn.size())
					continue;
				
				std::string const
					&key{outColumn[0]};
				
				m_formatsByLocale[code][key] = {outColumn[column]}; // TODO: Parse and pass in tokens.
				m_modules[module].emplace_back(key);
			}
			
			// TODO: Remove localisation CSV load debug log when done testing.
			{
//				std::stringstream ss;

//				ss
				log::msg("Karhu")
					<< "Loaded CSV for locale "
					<< code
					<< " into module '"
					<< module
					<< "' with "
					<< m_formatsByLocale[code].size()
					<< " key(s)";
				
				log::msg("Karhu")
					<< "Detected "
					<< m_locales.size()
					<< " locales"
					<< ((m_locales.empty()) ? "" : ":");
				
				for (auto const &code : m_locales)
					log::msg("Karhu") << '\t' << code;

//				for (auto const &f : m_formatsByLocale[code])
//					ss
//						<< '\t'
//						<< f.first
//						<< ' '
//						<< f.second.format
//						<< '\n';

//				log::msg("Karhu") << ss.str();
			}
			
			return true;
		}
		
		bool Localiser::unloadModule
		(
			std::string const &      module,
			char        const *const locale
		)
		{
			// TODO: Unload from all modules if locale is empty string (not null).
			
			auto const
				itFormats(m_formatsByLocale.find(localeResolved(locale)));
			
			if (itFormats == m_formatsByLocale.end())
				return false;
			
			auto const
				itModule(m_modules.find(module));
			
			if (itModule == m_modules.end())
				return false;
			
			for (auto const &key : itModule->second)
			{
				auto const jt(itFormats->second.find(key));
				itFormats->second.erase(jt);
			}
			
			m_modules.erase(itModule);
			
			return true;
		}
		
		bool Localiser::set
		(
			std::string const &      key,
			std::string const &      format,
			char        const *const locale
		)
		{
			// TODO: Implement setting localisation
			return false;
		}
		
		std::string Localiser::get
		(
			std::string const &      key,
			Args        const &      /*args*/,
			char        const *const locale
		)
		{
			auto const
				itFormats(m_formatsByLocale.find(localeResolved(locale)));
			
			if (itFormats == m_formatsByLocale.end())
				return {};
			
			auto const
				it(itFormats->second.find(key));
			
			if (it != itFormats->second.end())
				return it->second.format; // TODO: Actually parse format etc. later
			
			return {};
		}
		
		std::string Localiser::get(Localisation const &localisation)
		{
			char const
				*const code
				{
					(localisation.locale().empty())
					?
						locale().c_str()
					:
						localisation.locale().c_str()
				};
		
			return get
			(
				localisation.key(),
				localisation.args(),
				code
			);
		}
		
		void Localiser::locale(std::string code)
		{
			if (code.empty() || code == m_locale)
				return;
			
			m_locale = code;
		}
		
		char const *Localiser::localeResolved(char const *const code) const noexcept
		{
			return (code) ? code : locale().c_str();
		}
	}
}
