/**
 * @author		Ava Skoog
 * @date		2019-05-27
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_SCRIPTMANAGER_H_
	#define KARHU_APP_SCRIPTMANAGER_H_

	#include <karhu/app/scripting.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/res/Bank.hpp>
	#include <karhu/core/result.hpp>

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/lib/angelscript/include/angelscript.h>
	#include <karhu/app/lib/angelscript/add_on/scriptbuilder/scriptbuilder.h>
	#include <karhu/app/lib/angelscript/add_on/autowrapper/aswrappedcall.h>

	#include <functional>
	#include <map>
	#include <queue>
	#include <vector>
	#include <string>
	#include <type_traits>

	#ifdef AS_MAX_PORTABILITY
		#define karhuAS_PORTABLE
		#define karhuAS_FUNCTION(name)                         WRAP_FN(name)
		#define karhuAS_FUNCTIONPR(name, params, ret)          WRAP_FN_PR(name, params, ret)
		#define karhuAS_FUNCTION_OBJLAST(name)                 WRAP_OBJ_LAST(name)
		#define karhuAS_FUNCTIONPR_OBJLAST(name, params, ret)  WRAP_OBJ_LAST_PR(name, params, ret)
		#define karhuAS_FUNCTION_OBJFIRST(name)                WRAP_OBJ_FIRST(name)
		#define karhuAS_FUNCTIONPR_OBJFIRST(name, params, ret) WRAP_OBJ_FIRST_PR(name, params, ret)
		#define karhuAS_METHOD(type, name)                     WRAP_MFN(type, name)
		#define karhuAS_METHODPR(type, name, params, ret)      WRAP_MFN_PR(type, name, params, ret)
		#define karhuAS_CALL_THISCALL                          asCALL_GENERIC
		#define karhuAS_CALL_CDECL                             asCALL_GENERIC
		#define karhuAS_CALL_CDECL_OBJLAST                     asCALL_GENERIC
		#define karhuAS_CALL_CDECL_OBJFIRST                    asCALL_GENERIC
	#else
		#define karhuAS_FUNCTION(name)                         asFUNCTION(name)
		#define karhuAS_FUNCTIONPR(name, params, ret)          asFUNCTIONPR(name, params, ret)
		#define karhuAS_FUNCTION_OBJLAST(name)                 asFUNCTION(name)
		#define karhuAS_FUNCTIONPR_OBJLAST(name, params, ret)  asFUNCTIONPR(name, params, ret)
		#define karhuAS_FUNCTION_OBJFIRST(name)                asFUNCTION(name)
		#define karhuAS_FUNCTIONPR_OBJFIRST(name, params, ret) asFUNCTIONPR(name, params, ret)
		#define karhuAS_METHOD(type, name)                     asMETHOD(type, name)
		#define karhuAS_METHODPR(type, name, params, ret)      asMETHODPR(type, name, params, ret)
		#define karhuAS_CALL_THISCALL                          asCALL_THISCALL
		#define karhuAS_CALL_CDECL                             asCALL_CDECL
		#define karhuAS_CALL_CDECL_OBJLAST                     asCALL_CDECL_OBJLAST
		#define karhuAS_CALL_CDECL_OBJFIRST                    asCALL_CDECL_OBJFIRST
	#endif

	#define karhuAS_METHODPTRb(type, base, func) (::karhu::app::Scriptmanager::Method<type, base, decltype(&base::func), &base::func>{#func})
	#define karhuAS_METHODPTR(type, func) karhuAS_METHODPTRb(type, type, func)
	#define karhuAS_METHODPTRbrp(type, base, func, ret, par) (::karhu::app::Scriptmanager::Method<type, base, ret (base::*)par, &base::func>{#func})
	#define karhuAS_METHODPTRrp(type, func, ret, par) karhuAS_METHODPTRbrp(type, type, func, ret, par)

	/// @todo: Move these over to App.

	#define karhuAS_REGISTER_ENUM(apphandle, name) \
		(apphandle).scr().engine().RegisterEnum(#name);

	#define karhuAS_REGISTER_ENUMVAL(apphandle, name, val) \
		(apphandle).scr().engine().RegisterEnumValue(#name, #val, static_cast<int>(name::val));

	#define karhuAS_REGISTER_TYPEDEF(apphandle, type) \
		(apphandle).scr().engine().RegisterTypedef(#type, ::karhu::AS::primname<type>());

	#define karhuAS_REGISTER_FUNCDEF(apphandle, asdecl) \
		(apphandle).scr().engine().RegisterFuncdef(asdecl);

	#define karhuAS_REGISTER_INTERFACE(apphandle, type) \
		(apphandle).scr().engine().RegisterInterface(type);

	#define karhuAS_REGISTER_REFTYPE(apphandle, type) \
		{ \
			const int ID_type{(apphandle).scr().engine().RegisterObjectType(#type, 0, asOBJ_REF | asOBJ_NOCOUNT)}; \
			if (ID_type >= 0) \
				::karhu::AS::IDForType<type>(ID_type); \
		}

	#define karhuAS_REGISTER_RCTYPE(apphandle, type) \
		{ \
			const int ID_type{(apphandle).scr().engine().RegisterObjectType(#type, 0, asOBJ_REF)}; \
			if (ID_type >= 0) \
			{ \
				::karhu::AS::IDForType<type>(ID_type); \
				(apphandle).scr().engine().RegisterObjectBehaviour \
				( \
					#type, \
					asBEHAVE_ADDREF, \
					"void f()", \
					karhuAS_METHOD(::karhu::AS::RC<type>, f_karhuAS_add), \
					karhuAS_CALL_THISCALL \
				); \
				(apphandle).scr().engine().RegisterObjectBehaviour \
				( \
					#type, \
					asBEHAVE_RELEASE, \
					"void f()", \
					karhuAS_METHOD(::karhu::AS::RC<type>, f_karhuAS_release), \
					karhuAS_CALL_THISCALL \
				); \
			} \
		}

	#define karhuAS_REGISTER_VALTYPE(apphandle, type) \
		{ \
			const int ID_type{(apphandle).scr().engine().RegisterObjectType(#type, sizeof(type), asOBJ_VALUE | asGetTypeTraits<type>())}; \
			if (ID_type >= 0) \
				::karhu::AS::IDForType<type>(ID_type); \
		}

	#define karhuAS_REGISTER_VALTYPEf(apphandle, type) \
		{ \
			const int ID_type{(apphandle).scr().engine().RegisterObjectType(#type, sizeof(type), asOBJ_VALUE | asOBJ_APP_CLASS_ALLFLOATS | asGetTypeTraits<type>())}; \
			if (ID_type >= 0) \
				::karhu::AS::IDForType<type>(ID_type); \
		}

	#define karhuAS_REGISTER_VALTYPEi(apphandle, type) \
		{ \
			const int ID_type{(apphandle).scr().engine().RegisterObjectType(#type, sizeof(type), asOBJ_VALUE | asOBJ_APP_CLASS_ALLINTS | asGetTypeTraits<type>())}; \
			if (ID_type >= 0) \
				::karhu::AS::IDForType<type>(ID_type); \
		}

	#define karhuAS_REGISTER_PODTYPE(apphandle, type) \
		{ \
			const int ID_type{(apphandle).scr().engine().RegisterObjectType(#type, sizeof(type), asOBJ_VALUE | asOBJ_POD | asGetTypeTraits<type>())}; \
			if (ID_type >= 0) \
				::karhu::AS::IDForType<type>(ID_type); \
		}

	#define karhuAS_REGISTER_PODTYPEf(apphandle, type) \
		{ \
			const int ID_type{(apphandle).scr().engine().RegisterObjectType(#type, sizeof(type), asOBJ_VALUE | asOBJ_POD | asOBJ_APP_CLASS_ALLFLOATS | asGetTypeTraits<type>())}; \
			if (ID_type >= 0) \
				::karhu::AS::IDForType<type>(ID_type); \
		}

	#define karhuAS_REGISTER_PODTYPEi(apphandle, type) \
		{ \
			const int ID_type{(apphandle).scr().engine().RegisterObjectType(#type, sizeof(type), asOBJ_VALUE | asOBJ_POD | asOBJ_APP_CLASS_ALLINTS | asGetTypeTraits<type>())}; \
			if (ID_type >= 0) \
				::karhu::AS::IDForType<type>(ID_type); \
		}

	#define karhuAS_REGISTER_INHERITANCE(apphandle, basetype, childtype) \
		{ \
			(apphandle).scr().engine().RegisterObjectMethod(#basetype, #childtype "@ opCast()", karhuAS_FUNCTION((::karhu::AS::refcast<basetype, childtype>)), karhuAS_CALL_CDECL_OBJLAST); \
			(apphandle).scr().engine().RegisterObjectMethod(#childtype, #basetype "@ opImplCast()", karhuAS_FUNCTION((::karhu::AS::refcast<childtype, basetype>)), karhuAS_CALL_CDECL_OBJLAST); \
			(apphandle).scr().engine().RegisterObjectMethod(#basetype, "const " #childtype "@ opCast() const", karhuAS_FUNCTION((::karhu::AS::refcast<basetype, childtype>)), karhuAS_CALL_CDECL_OBJLAST); \
			(apphandle).scr().engine().RegisterObjectMethod(#childtype, "const " #basetype "@ opImplCast() const", karhuAS_FUNCTION((::karhu::AS::refcast<childtype, basetype>)), karhuAS_CALL_CDECL_OBJLAST); \
		}

	#define karhuAS_REGISTER_CTOR(apphandle, type) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_CONSTRUCT, \
			"void f()", \
			karhuAS_FUNCTION_OBJFIRST(::karhu::AS::constructor<type>), \
			karhuAS_CALL_CDECL_OBJFIRST \
		);

	#define karhuAS_REGISTER_FACTORY(apphandle, type) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_FACTORY, \
			#type " @f()", \
			karhuAS_FUNCTION(::karhu::AS::factory<::karhu::AS::RC<type>>), \
			karhuAS_CALL_CDECL \
		);

	#define karhuAS_REGISTER_CTORa(apphandle, type, atype0) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_CONSTRUCT, \
			"void f(" #atype0 ")", \
			karhuAS_FUNCTION_OBJFIRST((::karhu::AS::constructor<type, atype0>)), \
			karhuAS_CALL_CDECL_OBJFIRST \
		);

	#define karhuAS_REGISTER_FACTORYa(apphandle, type, atype) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_FACTORY, \
			#type " @f(" #atype ")", \
			karhuAS_FUNCTION((::karhu::AS::factory<::karhu::AS::RC<type>, atype>)), \
			karhuAS_CALL_CDECL \
		);

	#define karhuAS_REGISTER_CTORad(apphandle, type, atype, dtype) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_CONSTRUCT, \
			"void f(" dtype ")", \
			karhuAS_FUNCTION_OBJFIRST((::karhu::AS::constructor<type, atype>)), \
			karhuAS_CALL_CDECL_OBJFIRST \
		);

	#define karhuAS_REGISTER_FACTORYad(apphandle, type, atype, dtype) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_FACTORY, \
			#type " @f(" dtype ")", \
			karhuAS_FUNCTION((::karhu::AS::factory<::karhu::AS::RC<type>, atype>)), \
			karhuAS_CALL_CDECL \
		);

	#define karhuAS_REGISTER_CTORaa(apphandle, type, atype0, atype1) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_CONSTRUCT, \
			"void f(" #atype0 ", " #atype1 ")", \
			karhuAS_FUNCTION_OBJFIRST((::karhu::AS::constructor<type, atype0, atype1>)), \
			karhuAS_CALL_CDECL_OBJFIRST \
		);

	#define karhuAS_REGISTER_FACTORYaa(apphandle, type, atype0, atype1) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_FACTORY, \
			#type " @f(" #atype0 ", " #atype1 ")", \
			karhuAS_FUNCTION((::karhu::AS::factory<::karhu::AS::RC<type>, atype0, atype1>)), \
			karhuAS_CALL_CDECL \
		);

	#define karhuAS_REGISTER_CTORaad(apphandle, type, atype0, atype1, dtypes) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_CONSTRUCT, \
			"void f(" dtypes ")", \
			karhuAS_FUNCTION_OBJFIRST((::karhu::AS::constructor<type, atype0, atype1>)), \
			karhuAS_CALL_CDECL_OBJFIRST \
		);

	#define karhuAS_REGISTER_FACTORYaad(apphandle, type, atype0, atype1, dtypes) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_FACTORY, \
			#type " @f(" dtypes ")", \
			karhuAS_FUNCTION((::karhu::AS::factory<::karhu::AS::RC<type>, atype0, atype1>)), \
			karhuAS_CALL_CDECL \
		);

	#define karhuAS_REGISTER_CTORaaa(apphandle, type, atype0, atype1, atype2) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_CONSTRUCT, \
			"void f(" #atype0 ", " #atype1 ", " #atype2 ")", \
			karhuAS_FUNCTION_OBJFIRST((::karhu::AS::constructor<type, atype0, atype1, atype2>)), \
			karhuAS_CALL_CDECL_OBJFIRST \
		);

	#define karhuAS_REGISTER_FACTORYaaa(apphandle, type, atype0, atype1, atype2) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_FACTORY, \
			#type " @f(" #atype0 ", " #atype1 ", " #atype2 ")", \
			karhuAS_FUNCTION((::karhu::AS::factory<::karhu::AS::RC<type>, atype0, atype1, atype2>)), \
			karhuAS_CALL_CDECL \
		);

	#define karhuAS_REGISTER_CTORaaad(apphandle, type, atype0, atype1, atype2, dtypes) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_CONSTRUCT, \
			"void f(" dtypes ")", \
			karhuAS_FUNCTION_OBJFIRST((::karhu::AS::constructor<type, atype0, atype1, atype2>)), \
			karhuAS_CALL_CDECL_OBJFIRST \
		);

	#define karhuAS_REGISTER_FACTORYaaad(apphandle, type, atype0, atype1, atype2, dtypes) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_FACTORY, \
			#type " @f(" dtypes ")", \
			karhuAS_FUNCTION((::karhu::AS::factory<::karhu::AS::RC<type>, atype0, atype1, atype2>)), \
			karhuAS_CALL_CDECL \
		);

	#define karhuAS_REGISTER_COPYCTOR(apphandle, type) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_CONSTRUCT, \
			"void f(const " #type " &in)", \
			karhuAS_FUNCTION_OBJFIRST(::karhu::AS::copyconstructor<type>), \
			karhuAS_CALL_CDECL_OBJFIRST \
		);

	#define karhuAS_REGISTER_ASSIGNref(apphandle, type) \
		(apphandle).scr().engine().RegisterObjectMethod \
		( \
			#type, \
			#type " @opAssign(const " #type " &in)", \
			karhuAS_METHODPR(type , operator=, (const type &), type &), \
			karhuAS_CALL_THISCALL \
		);

	#define karhuAS_REGISTER_ASSIGNval(apphandle, type) \
		(apphandle).scr().engine().RegisterObjectMethod \
		( \
			#type, \
			#type " &opAssign(const " #type " &in)", \
			karhuAS_METHODPR(type , operator=, (const type &), type &), \
			karhuAS_CALL_THISCALL \
		);

	#define karhuAS_REGISTER_DTOR(apphandle, type) \
		(apphandle).scr().engine().RegisterObjectBehaviour \
		( \
			#type, \
			asBEHAVE_DESTRUCT, \
			"void f()", \
			karhuAS_FUNCTION_OBJLAST(::karhu::AS::destructor<type>), \
			karhuAS_CALL_CDECL_OBJLAST \
		);

	#define karhuAS_REGISTER_FUNCTIONd(apphandle, name, asdecl) \
		(apphandle).scr().engine().RegisterGlobalFunction \
		( \
			asdecl, \
			karhuAS_FUNCTION(name), \
			karhuAS_CALL_CDECL \
		);

	#define karhuAS_REGISTER_FUNCTIONrp(apphandle, name, ret, params) \
		(apphandle).scr().engine().RegisterGlobalFunction \
		( \
			#ret " " #name #params, \
			karhuAS_FUNCTIONPR(name, params, ret), \
			karhuAS_CALL_CDECL \
		);

	#define karhuAS_REGISTER_FUNCTIONrpd(apphandle, name, ret, params, asdecl) \
		(apphandle).scr().engine().RegisterGlobalFunction \
		( \
			asdecl, \
			karhuAS_FUNCTIONPR(name, params, ret), \
			karhuAS_CALL_CDECL \
		);

	#define karhuAS_REGISTER_METHODi(apphandle, type, asdecl) \
		(apphandle).scr().engine().RegisterInterfaceMethod \
		( \
			type, \
			asdecl \
		);

	#define karhuAS_REGISTER_METHODd(apphandle, type, methodname, asdecl) \
		(apphandle).scr().engine().RegisterObjectMethod \
		( \
			#type, \
			asdecl, \
			karhuAS_METHOD(type , methodname), \
			karhuAS_CALL_THISCALL \
		);

	#define karhuAS_REGISTER_METHODrp(apphandle, type, methodname, ret, params) \
		(apphandle).scr().engine().RegisterObjectMethod \
		( \
			#type, \
			#ret " " #methodname #params, \
			karhuAS_METHODPR(type , methodname , params, ret), \
			karhuAS_CALL_THISCALL \
		);

	#define karhuAS_REGISTER_METHODrpd(apphandle, type, methodname, ret, params, asdecl) \
		(apphandle).scr().engine().RegisterObjectMethod \
		( \
			#type, \
			asdecl, \
			karhuAS_METHODPR(type, methodname, params, ret), \
			karhuAS_CALL_THISCALL \
		);

	#define karhuAS_REGISTER_METHODrpp(apphandle, type, methodname, ret, params, ptype) \
		(apphandle).scr().engine().RegisterObjectMethod \
		( \
			#type, \
			#ret " " #methodname #params, \
			karhuAS_METHODPR(ptype, methodname, params, ret), \
			karhuAS_CALL_THISCALL \
		);

	#define karhuAS_REGISTER_METHODrppd(apphandle, type, methodname, ret, params, ptype, asdecl) \
		(apphandle).scr().engine().RegisterObjectMethod \
		( \
			#type, \
			asdecl, \
			karhuAS_METHODPR(ptype, methodname, params, ret), \
			karhuAS_CALL_THISCALL \
		);

	/// @todo: Fix up the karhuAS_REGISTER_METHOD*b to specify basetype manually instead of ugly BaseAS alias hackery…

	#define karhuAS_REGISTER_METHODrpb(apphandle, type, methodname, ret, params) \
		(apphandle).scr().engine().RegisterObjectMethod \
		( \
			#type, \
			#ret " " #methodname #params, \
			karhuAS_METHODPR(type::BaseAS, methodname, params, ret), \
			karhuAS_CALL_THISCALL \
		);

	#define karhuAS_REGISTER_METHODrpbd(apphandle, type, methodname, ret, params, asdecl) \
		(apphandle).scr().engine().RegisterObjectMethod \
		( \
			#type, \
			asdecl, \
			karhuAS_METHODPR(type::BaseAS, methodname, params, ret), \
			karhuAS_CALL_THISCALL \
		);

	#define karhuAS_REGISTER_MEMBER(apphandle, type, name, mtype) \
		(apphandle).scr().engine().RegisterObjectProperty \
		( \
			#type, \
			mtype " " #name, \
			asOFFSET(type, name) \
		);
		
	namespace karhu
	{
		namespace app
		{
			class Scriptmanager
			{
				public:
					struct PropWithAttribs
					{
						int IDType, IDSubtype;
						bool reference;
						asUINT index;
						std::string name;
						std::vector<std::string> attribs;
					};
				
				public:
					struct Typedata
					{
						enum class Category : std::uint8_t
						{
							none,
							value,
							reference,
							component,
							resource
						} category;
						
						union
						{
							ecs::IDTypeComponent typeComponent;
							res::IDTypeResource  typeResource;
						};
						
						std::function<void(conv::Serialiser &, char const *, void *)> fSerialise;
						std::function<void(conv::Serialiser const &, char const *, void *)> fDeserialise;
						std::function<void(conv::Serialiser &, void *)> fSerialiseDirectly;
						std::function<void(conv::Serialiser const &, void *)> fDeserialiseDirectly;
					};
				
				public:
					template<typename T, typename TBase, typename TFunc, TFunc>
					struct RefFuncBase;

					template<typename T, typename TBase, typename TRet, typename... TArgs, TRet (TBase::*func)(TArgs...)>
					struct RefFuncBase<T, TBase, TRet (TBase::*)(TArgs...), func>
					{
						static TRet call(Reference &ref, TArgs &&... args)
						{
							return (static_cast<T *>(ref.getVoidPointer())->*func)(std::forward<TArgs>(args)...);
						}
					};

					template<typename T, typename TBase, typename TRet, typename... TArgs, TRet (TBase::*func)(TArgs...) const>
					struct RefFuncBase<T, TBase, TRet (TBase::*)(TArgs...) const, func>
					{
						static TRet call(Reference const &ref, TArgs &&... args)
						{
							return (static_cast<T *>(ref.getVoidPointer())->*func)(std::forward<TArgs>(args)...);
						}
					};
				
					template<typename T, typename TFunc, TFunc func>
					using RefFunc = RefFuncBase<T, T, TFunc, func>;
				
					template<typename... Ts>
					std::enable_if_t<(0 == sizeof...(Ts)), bool>
					fillOutTypeAS(std::string &, bool const, bool const) { return true; }
				
					template<typename T, typename... Ts>
					bool fillOutTypeAS(std::string &inString, bool const isParam, bool const isReturn)
					{
						using TVal  = std::remove_pointer_t<std::remove_reference_t<T>>;
						using TPure = std::decay_t<std::remove_pointer_t<T>>;
						
						bool const
							isPrim{AS::isPrimitive<TPure>()},
							isVoid{std::is_same<void, TPure>{}};
						
						bool
							isReftype{false};
						
						if (isPrim && !isVoid && std::is_pointer<T>{})
							return false;
						
						if (!std::is_pointer<T>{} && std::is_const<std::remove_reference_t<T>>{})
							inString += "const ";
						
						if (isVoid)
							inString += (isReturn) ? "void" : "ref";
						else if (isPrim)
							inString += AS::primname<TPure>();
						else
						{
							int const
								ID{AS::IDForType<TPure>()};
							
							asITypeInfo *const
									info{m_engine->GetTypeInfoById(ID)};
							
							if (!info)
								return false;
							
							isReftype = static_cast<bool>(info->GetFlags() & asOBJ_REF);
							
							if (info->GetNamespace())
							{
								inString += info->GetNamespace();
								inString += "::";
							}
							
							inString += info->GetName();
						}
						
						if
						(
							std::is_pointer<T>{} ||
							(
								std::is_reference<T>{} &&
								isReturn               &&
								isReftype
							)
						)
						{
							inString += " @";
						
							if (!isReturn && std::is_const<T>{})
								inString += "const ";
						}
						else if (std::is_reference<T>{})
						{
							inString += " &";
							
							if (isParam && std::is_const<TVal>{})
								inString += "in ";
						}
						
						if (0 != sizeof...(Ts))
							inString += ", ";
						
						return fillOutTypeAS<Ts...>(inString, isParam, isReturn);
					}
				
					template<typename TRet, typename T, typename... TArgs>
					bool fillOutDeclAS(char const *const nameAS, bool const isConst, std::string &inString)
					{
						if (!fillOutTypeAS<TRet>(inString, false, true))
							return false;
						
						inString += " ";
						inString += nameAS;
						inString += "(";
						
						if (!fillOutTypeAS<TArgs...>(inString, true, false))
							return false;
						
						inString += ")";
						
						if (isConst)
							inString += " const";
						
						return true;
					}
				
					template<typename> struct Memberfunc;
				
					template<typename TRet, typename T, typename... TArgs>
					struct Memberfunc<TRet (T::*)(TArgs...)>
					{
						using TClass = T;
						using TReturn = TRet;
						
						static bool fillOutDeclAS(Scriptmanager &sm, char const *const nameAS, std::string &inString)
						{
							return sm.template fillOutDeclAS<TRet, T, TArgs...>(nameAS, false, inString);
						}
					};
				
					template<typename TRet, typename T, typename... TArgs>
					struct Memberfunc<TRet (T::*)(TArgs...) const>
					{
						using TClass = T;
						using TReturn = TRet;
						
						static bool fillOutDeclAS(Scriptmanager &sm, char const *const nameAS, std::string &inString)
						{
							return sm.template fillOutDeclAS<TRet, T, TArgs...>(nameAS, true, inString);
						}
					};
				
					template<typename T, typename TBase, typename TFunc, TFunc func>
					bool registerMethod(char const *const typeAS, char const *const declAS)
					{
						#ifdef AS_MAX_PORTABILITY
						auto ptr(asFunctionPtr(::gw::Wrapper<TFunc>::template f<func>));
						#else
						auto ptr(asSMethodPtr<sizeof(TFunc)>::Convert((TFunc)(func)));
						#endif
						
						if (m_engine->RegisterObjectMethod(typeAS, declAS, ptr, karhuAS_CALL_THISCALL) < 0)
							return false;
						
						if (std::is_base_of<ecs::Component, T>{})
						{
							using namespace std::string_literals;
							
							std::string const
								reftypeAS{"ecs::Ref<"s + typeAS + ">"};
							
							if (m_engine->RegisterObjectMethod
							(
								reftypeAS.c_str(),
								declAS,
								karhuAS_FUNCTION_OBJFIRST((RefFuncBase<T, TBase, TFunc, func>::call)),
								karhuAS_CALL_CDECL_OBJFIRST
							) < 0)
								return false;
						}
						
						return true;
					}
				
					template<typename T, typename TFunc, TFunc func>
					bool registerMethod(char const *const typeAS, char const *const declAS)
					{
						return registerMethod<T, T, TFunc, func>(typeAS, declAS);
					}
				
					template<typename TFunc, TFunc func>
					bool registerMethod(char const *const typeAS, char const *const declAS)
					{
						using T = typename Memberfunc<TFunc>::TClass;
						return registerMethod<T, T, TFunc, func>(typeAS, declAS);
					}
				
					template<typename T, typename TBase, typename TFunc, TFunc func>
					bool registerMethod(char const *const nameAS)
					{
						std::string
							typeAS,
							declAS;
						
						if (!fillOutTypeAS<T>(typeAS, false, false))
							return false;
						
						if (!Memberfunc<TFunc>::fillOutDeclAS(*this, nameAS, declAS))
						{
							log::err("Karhu")
								<< "Failed to generate script signature for '"
								<< nameAS
								<< '\''
								<< ((0 != declAS.length()) ? ("(trail: " + declAS + ")") : "");
							
							return false;
						}
						
						log::dbg("Karhu")
							<< "Generated script signature for '"
							<< nameAS
							<< "':";
						log::dbg("Karhu")
							<< declAS;
						
						return registerMethod<T, TBase, TFunc, func>(typeAS.c_str(), declAS.c_str());
					}
				
					template<typename T, typename TFunc, TFunc func>
					bool registerMethod(char const *const nameAS)
					{
						return registerMethod<T, T, TFunc, func>(nameAS);
					}
				
					template<typename TFunc, TFunc func>
					bool registerMethod(char const *const nameAS)
					{
						using T = typename Memberfunc<TFunc>::TClass;
						return registerMethod<T, T, TFunc, func>(nameAS);
					}
				
					template<typename T, typename TBase, typename TFunc, TFunc func>
					struct Method
					{
						char const *name;
					};
				
					template<typename T, typename TBase, typename TFunc, TFunc func>
					bool registerMethod(Method<T, TBase, TFunc, func> const &method)
					{
						return registerMethod<T, TBase, TFunc, func>(method.name);
					};
				
					template<typename T, typename TBase, typename TFunc, TFunc func>
					bool registerMethod(Method<T, TBase, TFunc, func> const &, char const *const declAS)
					{
						std::string
							typeAS;
						
						if (!fillOutTypeAS<T>(typeAS, false, false))
							return false;
						
						return registerMethod<T, TBase, TFunc, func>(typeAS.c_str(), declAS);
					};
				
					template<typename T, typename TProp, TProp (T::*prop)>
					static TProp refPropGet(Reference const &ref)
					{
						return static_cast<T *>(ref.getVoidPointer())->*prop;
					}

					template<typename T, typename TProp, TProp (T::*prop)>
					static void refPropSet(Reference &ref, TProp const &value)
					{
						static_cast<T *>(ref.getVoidPointer())->*prop = value;
					}
				
					template<typename T, typename TBase, typename TProp, TProp (TBase::*prop)>
					bool registerMember(char const *const typeAS, char const *const typePropAS, char const *const nameAS)
					{
						// Based on the offset code in the AS codebase itself, supposedly more portable.
						int const
							offset{(int)(std::size_t)(&(reinterpret_cast<TBase *>(100000)->*prop)) - 100000};
						
						std::string const
							declAS{std::string{typePropAS} + " " + nameAS};
					
						if (m_engine->RegisterObjectProperty(typeAS, declAS.c_str(), offset) < 0)
							return false;
						
						if (std::is_base_of<ecs::Component, T>{})
						{
							using namespace std::string_literals;
							
							std::string const
								reftypeAS{"ecs::Ref<"s + typeAS + ">"};
							
							std::string
								refdeclGetAS{typePropAS},
								refdeclSetAS{"void"};
							
							refdeclGetAS += " get_"s + nameAS + "() const property";
							refdeclSetAS += " set_"s + nameAS + "(" + typePropAS + ") property";
							
							log::dbg("Karhu")
								<< "Generated script getter and setter for ecs::Ref<"
								<< typeAS
								<< "> prop '"
								<< nameAS
								<< "' ("
								<< typePropAS
								<< "):";
							log::dbg("Karhu")
								<< refdeclGetAS;
							log::dbg("Karhu")
								<< refdeclSetAS;
							
							if (m_engine->RegisterObjectMethod
							(
								reftypeAS.c_str(),
								refdeclGetAS.c_str(),
								karhuAS_FUNCTION_OBJFIRST((&refPropGet<T, TProp, prop>)),
								karhuAS_CALL_CDECL_OBJFIRST
							) < 0)
								return false;
							
							if (m_engine->RegisterObjectMethod
							(
								reftypeAS.c_str(),
								refdeclSetAS.c_str(),
								karhuAS_FUNCTION_OBJFIRST((&refPropSet<T, TProp, prop>)),
								karhuAS_CALL_CDECL_OBJFIRST
							) < 0)
								return false;
						}
						
						return true;
					}
				
				public:
					Scriptmanager(App &);
					~Scriptmanager();
				
					App &app() const noexcept { return m_app; }
					void logVersion();
					bool init();
					bool initResources();
					bool initECS();
				
					asIScriptEngine &engine() noexcept { return *m_engine; }
				
					/// @todo: Possible we could use some clever RAII ref instead to guarantee safe return but since this is such a low-level barely used part of the engine it might be safe anyway.
					asIScriptContext &borrowContext();
					void returnContext(asIScriptContext &);
				
					void serialise(asIScriptObject &, conv::Serialiser &, bool const onlyMarked = true);
					void deserialise(asIScriptObject &, conv::Serialiser const &, bool const onlyMarked = true);
				
					Result<asIScriptModule *> loadModule
					(
						const char        *name,
						const char        *code,
						const std::size_t &length
					);
				
					void unloadModule(char const *const name);
				
					Status loadModuleThrowaway
					(
						char        const * const name,
						char        const * const code,
						std::size_t const         length,
						asIScriptEngine   *&      outEngine,
						asIScriptModule   *&      outModule
					);
				
					void unloadEngineThrowaway(asIScriptEngine &);
				
					std::vector<std::string>     const *attribsForType(int const ID) const;
					std::vector<PropWithAttribs> const *propsWithAttribsForType(int const ID) const;
				
					Status loadDerivedTypeWithFactoryAndOptionalMethodsFromModule
					(
						asIScriptModule &module,
						const char *parentname,
						asITypeInfo *&outType,
						const char *parametersFactory,
						asIScriptFunction *&outFactory,
						const std::map<const char *, asIScriptFunction *&> &outFuncsByDecl
					);
				
					bool loadOptionalMethodsOfDerivedTypeFromModule
					(
						asIScriptModule &module,
						const char *parentname,
						const std::map<const char *, asIScriptFunction *&> &outFuncsByDecl
					);
				
					template<typename T>
					bool registerTypeResource
					(
						const res::IDTypeResource &type,
						std::string name
					);
				
					template<typename T>
					bool registerTypeComponent
					(
						const ecs::IDTypeComponent &type,
						std::string name
					);
				
					template<typename T>
					bool registerTypeIScriptable
					(
						const ecs::IDTypeComponent &type,
						std::string name
					);
				
					template<typename T>
					bool registerTypeEvent
					(
						const ecs::IDTypeEvent &event,
						std::string name
					);
				
					template<typename T>
					ecs::IDTypeComponent IDTypeIScriptable() const noexcept
					{
						return getOrSetIDForIScriptable<T>();
					}
				
					struct DataTypeIScriptable
					{
						std::string name;
					};
				
					auto const &dataPerTypeIScriptable() const noexcept
					{
						return m_dataPerTypeIScriptable;
					}
				
					std::string nameTypeIScriptableByID(ecs::IDTypeComponent const ID) const noexcept
					{
						auto const it(m_dataPerTypeIScriptable.find(ID));
						
						if (it != m_dataPerTypeIScriptable.end())
							return it->second.name;
						
						return {};
					}
				
					template<typename T>
					std::string nameTypeIScriptable() const noexcept
					{
						return nameTypeIScriptableByID(IDTypeIScriptable<T>());
					}
				
				private:
					static std::pair<std::string, std::string> splitSpaceAndName(std::string);
				
					template<typename T>
					static ecs::IDTypeComponent getOrSetIDForIScriptable(ecs::IDTypeComponent ID = 0) noexcept
					{
						static ecs::IDTypeComponent value{0};
						
						if (ID != 0)
							value = ID;
						
						return value;
					}
				
				private:
					void callbackMessage(const asSMessageInfo &msg);
				
					void serialise(asIScriptObject &, PropWithAttribs const &, conv::Serialiser &, bool const onlyMarked = true);
					void serialise(AS::Vector &, conv::JSON::Arr &, int const IDSubtype, bool const onlyMarked = true);
					void deserialise(asIScriptObject &, PropWithAttribs const &, conv::Serialiser const &, bool const onlyMarked = true);
					void deserialise(AS::Vector &, conv::JSON::Arr const &, int const IDSubtype, bool const onlyMarked = true);
					bool fillPropWithAttribsForType(PropWithAttribs &inProp, asITypeInfo const &, asUINT const index);
				
				public:
					#if KARHU_EDITOR_SERVER_SUPPORTED
					bool edit(asIScriptObject &, PropWithAttribs const &);
					#endif
				
				private:
					App
						&m_app;
				
					asIScriptEngine
						*m_engine;
				
					std::queue<asIScriptContext *>
						m_contexts;
				
					CScriptBuilder
						m_scriptbuilder;
				
					std::map<std::string, asIScriptModule *>
						m_modules;
				
					std::map<int, std::vector<PropWithAttribs>>
						m_propsWithAttribsByType;
				
					std::map<int, std::vector<std::string>>
						m_typesWithAttribs;
				public: //! @todo: Make private again when done with new script reg stuff.
					std::map<int, std::unique_ptr<Typedata>>
						m_typedata; /// @todo: Use persistent pool instead?
				private:
					//! @todo: Found out we probably don't need these but instead can use engine->GetStringFactoryReturnTypeId() and GetDefaultArrayTypeId()
					int
						m_IDTypeString,
						m_IDTypeVector;
				
					std::map<ecs::IDTypeComponent, DataTypeIScriptable>
						m_dataPerTypeIScriptable;
			};
			
			template<typename T>
			bool Scriptmanager::registerTypeResource
			(
				const res::IDTypeResource &type,
				std::string name
			)
			{
				std::string space;
				
				if (0 == name.length())
				{
					log::err("Karhu") << "Cannot register resource type: not registered in bank";
					return false;
				}
				
				/// @todo: This would look great with C++17 syntax in the future...
				const auto split(splitSpaceAndName(name));
				space = split.first;
				name  = split.second;
				
				m_engine->SetDefaultNamespace(space.c_str());
				
				const int ID{m_engine->RegisterObjectType
				(
					name.c_str(),
					0,
					asOBJ_REF | asOBJ_NOCOUNT
				)};
				
				if (ID < 0)
					return false;
				else
				{
					AS::IDForType<T>(ID);
					
					auto info(m_engine->GetTypeInfoById(ID));
					auto td  (std::make_unique<Typedata>());
					
					td->category = Typedata::Category::resource;
					td->typeResource = type;
					
					info->SetUserData(td.get());
					
					m_typedata.emplace(ID, std::move(td));
				}
				
				if
				(
					m_engine->RegisterObjectMethod
					(
						name.c_str(),
						"res::IDResource identifier() const",
						karhuAS_METHOD(T, identifier),
						karhuAS_CALL_THISCALL
					) < 0
				)
					return false;
				
				return true;
			}
			
			template<typename T>
			bool Scriptmanager::registerTypeComponent
			(
				const ecs::IDTypeComponent &type,
				std::string name
			)
			{
				std::string const
					nameFull{name};
				
				std::string
					space;
				
				if (0 == name.length())
				{
					log::err("Karhu") << "Cannot register component type: not registered in ECS";
					return false;
				}
				
				/// @todo: This would look great with C++17 syntax in the future...
				const auto split(splitSpaceAndName(name));
				space = split.first;
				name  = split.second;
				
				m_engine->SetDefaultNamespace(space.c_str());
				
				const int ID{m_engine->RegisterObjectType
				(
					name.c_str(),
					0,
					asOBJ_REF | asOBJ_NOCOUNT
				)};
				
				if (ID < 0)
				{
					m_engine->SetDefaultNamespace("");
					return false;
				}
				else if (!std::is_same<T, ecs::Component>{})
				{
					AS::IDForType<T>(ID);
					
					auto info(m_engine->GetTypeInfoById(ID));
					auto td  (std::make_unique<Typedata>());
					
					td->category = Typedata::Category::component;
					td->typeComponent = type;
					
					info->SetUserData(td.get());
					
					m_typedata.emplace(ID, std::move(td));
				}
				
				// Regular component methods.
				
				if
				(
					m_engine->RegisterObjectMethod
					(
						name.c_str(),
						"ecs::IDComponent identifier() const",
						karhuAS_METHOD(T, identifier),
						karhuAS_CALL_THISCALL
					) < 0
				)
				{
					m_engine->SetDefaultNamespace("");
					return false;
				}
				
				if
				(
					m_engine->RegisterObjectMethod
					(
						name.c_str(),
						"ecs::IDEntity entity() const",
						karhuAS_METHOD(T, entity),
						karhuAS_CALL_THISCALL
					) < 0
				)
				{
					m_engine->SetDefaultNamespace("");
					return false;
				}
				
				if
				(
					m_engine->RegisterObjectMethod
					(
						name.c_str(),
						"bool activeSelf() const",
						karhuAS_METHOD(T, activeSelf),
						karhuAS_CALL_THISCALL
					) < 0
				)
				{
					m_engine->SetDefaultNamespace("");
					return false;
				}
				
				if
				(
					m_engine->RegisterObjectMethod
					(
						name.c_str(),
						"bool activeInHierarchy() const",
						karhuAS_METHOD(T, activeInHierarchy),
						karhuAS_CALL_THISCALL
					) < 0
				)
				{
					m_engine->SetDefaultNamespace("");
					return false;
				}
				
				// Polymorphism.
				
				if (!std::is_same<T, ecs::Component>{})
				{
					m_engine->RegisterObjectMethod(nameFull.c_str(), "ecs::Component @opImplCast()", karhuAS_FUNCTION((AS::refcast<T, ecs::Component>)), karhuAS_CALL_CDECL_OBJLAST);
					m_engine->RegisterObjectMethod(nameFull.c_str(), "const ecs::Component @opImplCast() const", karhuAS_FUNCTION((AS::refcast<T, ecs::Component>)), karhuAS_CALL_CDECL_OBJLAST);
					
					std::string const
						cast{nameFull + "@ opCast()"},
						castConst{"const " + nameFull + "@ opCast() const"};
					
					m_engine->SetDefaultNamespace("ecs");
					
					m_engine->RegisterObjectMethod("Component", cast.c_str(), karhuAS_FUNCTION((AS::refcast<ecs::Component, T>)), karhuAS_CALL_CDECL_OBJLAST);
					m_engine->RegisterObjectMethod("Component", castConst.c_str(), karhuAS_FUNCTION((AS::refcast<ecs::Component, T>)), karhuAS_CALL_CDECL_OBJLAST);
					
					// Ref<T> support.
					
					std::string const
						nameRef        {"Ref<" + nameFull + ">"},
						declCopyctorRef{"void f(const " + nameRef + " &in)"},
						declAssignRef  {nameRef + " &opAssign(const " + nameRef + " &in)"};
					
					if
					(
						m_engine->RegisterObjectType
						(
							nameRef.c_str(),
							sizeof(Reference),
							asOBJ_VALUE | asGetTypeTraits<Reference>()
						) < 0
					)
					{
						m_engine->SetDefaultNamespace("");
						return false;
					}
					
					if
					(
						m_engine->RegisterObjectBehaviour
						(
							nameRef.c_str(),
							asBEHAVE_CONSTRUCT,
							"void f()",
							karhuAS_FUNCTION_OBJFIRST(AS::constructor<Reference>),
							karhuAS_CALL_CDECL_OBJFIRST
						) < 0
					)
					{
						m_engine->SetDefaultNamespace("");
						return false;
					}
					
					if
					(
						m_engine->RegisterObjectBehaviour
						(
							nameRef.c_str(),
							asBEHAVE_CONSTRUCT,
							declCopyctorRef.c_str(),
							karhuAS_FUNCTION_OBJFIRST(AS::copyconstructor<Reference>),
							karhuAS_CALL_CDECL_OBJFIRST
						) < 0
					)
					{
						m_engine->SetDefaultNamespace("");
						return false;
					}
					
					if
					(
						m_engine->RegisterObjectMethod
						(
							nameRef.c_str(),
							declAssignRef.c_str(),
							karhuAS_FUNCTION_OBJFIRST(AS::opAssign<Reference>),
							karhuAS_CALL_CDECL_OBJFIRST
						) < 0
					)
					{
						m_engine->SetDefaultNamespace("");
						return false;
					}
				}
				
				m_engine->SetDefaultNamespace("");
				return true;
			}
			
			template<typename T>
			bool Scriptmanager::registerTypeIScriptable
			(
				const ecs::IDTypeComponent &type,
				std::string name
			)
			{
				std::string space;
				
				if (0 == name.length())
				{
					log::err("Karhu") << "Cannot register scriptable interface type: must have a name";
					return false;
				}
				
				/// @todo: This would look great with C++17 syntax in the future...
				const auto split(splitSpaceAndName(name));
				space = split.first;
				name  = split.second;
				
				m_engine->SetDefaultNamespace(space.c_str());
				
				const int ID{m_engine->RegisterObjectType
				(
					name.c_str(),
					0,
					asOBJ_REF | asOBJ_NOCOUNT
				)};
				
				if (ID < 0)
					return false;
				
				if
				(
					m_engine->RegisterObjectMethod
					(
						name.c_str(),
						"ecs::IDComponent component() const",
						karhuAS_METHOD(T, component),
						karhuAS_CALL_THISCALL
					) < 0
				)
					return false;
				
				if
				(
					m_engine->RegisterObjectMethod
					(
						name.c_str(),
						"ecs::IDEntity entity() const",
						karhuAS_METHOD(T, entity),
						karhuAS_CALL_THISCALL
					) < 0
				)
					return false;
				
				getOrSetIDForIScriptable<T>(type);
				
				m_dataPerTypeIScriptable.emplace
				(
					type,
					DataTypeIScriptable
					{
						name
					}
				);
				
				return true;
			}
			
			template<typename T>
			bool Scriptmanager::registerTypeEvent
			(
				const ecs::IDTypeEvent &type,
				std::string name
			)
			{
				std::string space;
				
				if (0 == name.length())
				{
					log::err("Karhu") << "Cannot register event type: not registered in ECS";
					return false;
				}
				
				/// @todo: This would look great with C++17 syntax in the future...
				const auto split(splitSpaceAndName(name));
				space = split.first;
				name  = split.second;
				
				m_engine->SetDefaultNamespace(space.c_str());
				
				const int ID{m_engine->RegisterObjectType
				(
					name.c_str(),
					0,
					asOBJ_REF | asOBJ_NOCOUNT
				)};
				
				if (ID < 0)
					return false;
				else
				{
					AS::IDForType<T>(ID);
					
					auto info(m_engine->GetTypeInfoById(ID));
					auto td  (std::make_unique<Typedata>());
					
					td->category = Typedata::Category::component;
					td->typeComponent = type;
					
					info->SetUserData(td.get());
					
					m_typedata.emplace(ID, std::move(td));
				}
				
				if
				(
					m_engine->RegisterObjectMethod
					(
						name.c_str(),
						"ecs::IDEntity entity() const",
						karhuAS_METHOD(T, entity),
						karhuAS_CALL_THISCALL
					) < 0
				)
					return false;
				
				if
				(
					m_engine->RegisterObjectMethod
					(
						name.c_str(),
						"ecs::IDTypeComponent typeComponent() const",
						karhuAS_METHOD(T, typeComponent),
						karhuAS_CALL_THISCALL
					) < 0
				)
					return false;
				
				if
				(
					m_engine->RegisterObjectMethod
					(
						name.c_str(),
						"ecs::IDComponent component() const",
						karhuAS_METHOD(T, component),
						karhuAS_CALL_THISCALL
					) < 0
				)
					return false;
				
				return true;
			}
		}
	}
#endif
