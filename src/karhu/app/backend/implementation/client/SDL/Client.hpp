/**
 * @author		Ava Skoog
 * @date		2017-07-10
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Figure out the best way to build this only in debug.
#ifndef KARHU_TOOL_NETWORK_CLIENT_H_
	#define KARHU_TOOL_NETWORK_CLIENT_H_

	#include <karhu/network/Interface.hpp>
	#include <karhu/core/string.hpp>
	#include <karhu/core/platform.hpp>
	#include <karhu/core/log.hpp>
	#include <karhu/core/debugging.hpp>

	/// @todo: Try to get SDL out of this header.
	#include <SDL2/SDL.h>
	#include <karhu/app/backend/implementation/client/SDL/lib/SDL2_net/SDL_net.h>

	#include <atomic>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace SDL
				{
					template<typename TChar, std::size_t SizeBuffer>
					class MessengerClient : public karhu::network::Messenger<TChar, SizeBuffer>
					{
						public:
							using TMessenger = karhu::network::Messenger<TChar, SizeBuffer>;
							using TMessage   = typename TMessenger::TMessage;
							using TBuffer    = typename TMessage::Buffer;

						public:
							using TMessenger::Messenger;

						protected:
							bool performRead(TBuffer &) override;
							bool performSend(const TBuffer &) override;
					};

					/**
					 * This is a basic client implementation for the tools.
					 * It is meant to be derived in order to implement any
					 * actual specific client functionality for a particular
					 * tool.
					 */
					template<typename TChar, std::size_t SizeBuffer>
					class Client
					:
					public karhu::network::Interface<MessengerClient<TChar, SizeBuffer>, karhu::network::TypeInterface::client>
					{
						public:
							using TMessenger = MessengerClient<TChar, SizeBuffer>;
							using TMessage   = typename TMessenger::TMessage;
							using TBuffer    = typename TMessage::Buffer;

						public:
							Client() : karhu::network::Interface<TMessenger, karhu::network::TypeInterface::client>{this} {}
							~Client();

						protected:
							bool performConnect(const Platform, const std::uint16_t, const std::string &) override;
							bool performConnectAsync(const Platform, const std::uint16_t, const std::string &) override;
							void performDisconnect() override;
							void onDisconnect() override;
							void onUpdate() override {}
						
						private:
							void clean();
						
						private:
							SDLNet_SocketSet m_socketset{nullptr};
							TCPsocket        m_socket   {nullptr};
							IPaddress        m_IP;
						
							bool m_initialised{false};
							std::atomic<bool> m_connected{false};
						
						friend class MessengerClient<TChar, SizeBuffer>;
					};

					template<typename TChar, std::size_t SizeBuffer>
					bool MessengerClient<TChar, SizeBuffer>::performRead(TBuffer &b)
					{
						auto d(reinterpret_cast<Client<TChar, SizeBuffer> *>(TMessenger::data()));
						auto socketset(d->m_socketset);
						auto socket(d->m_socket);

						if (!socket) return false;

						if (SDLNet_CheckSockets(socketset, 0) <= 0)
							return false;
						else
						{
							auto response(SDLNet_SocketReady(socket));
							if (response == 0)
								return false;
							else
							{
								auto bytecount(SDLNet_TCP_Recv(socket, b.data(), static_cast<int>(b.size() * sizeof(TChar))));

								if (bytecount <= 0)
									return false;
							}
						}

						return true;
					}
					
					template<typename TChar, std::size_t SizeBuffer>
					bool MessengerClient<TChar, SizeBuffer>::performSend(const TBuffer &b)
					{
						auto d(reinterpret_cast<Client<TChar, SizeBuffer> *>(TMessenger::data()));
						auto &socket(d->m_socket);
						
						if (!socket) return false;

						auto bytecount(SDLNet_TCP_Send(socket, b.data(), static_cast<int>(b.size() * sizeof(TChar))));

						if (bytecount <= 0)
							return false;

						return true;
					}
					
					template<typename TChar, std::size_t SizeBuffer>
					Client<TChar, SizeBuffer>::~Client()
					{
						clean();
						
						if (m_initialised)
						{
							SDLNet_Quit();
							m_initialised = false;
						}
					}
					
					template<typename TChar, std::size_t SizeBuffer>
					bool Client<TChar, SizeBuffer>::performConnect
					(
						const Platform,
						const std::uint16_t  port,
						const std::string   &host
					)
					{
						if (!m_initialised && SDLNet_Init() == -1)
						{
							log::err("Karhu") << "Failed to initialise SDL net: " << SDLNet_GetError();
							return false;
						}
						else
						{
							m_initialised = true;

							log::msg("Karhu") << "Successfully initialised SDL net";
						
							m_socketset = SDLNet_AllocSocketSet(1);
							if (!m_socketset)
							{
								log::err("Karhu") << "Failed to allocate socket set (1): " << SDLNet_GetError();
								return false;
							}
							else
							{
								log::msg("Karhu") << "Successfully allocated socket set (1)";
								
								log::msg("Karhu") << "Connecting to " << host << ':' << port << "...";
								if (SDLNet_ResolveHost(&m_IP, host.c_str(), static_cast<Uint16>(port)) == -1)
								{
									log::err("Karhu") << "Failed to resolve host: " << SDLNet_GetError();
									return false;
								}
								else
									log::msg("Karhu") << "Successfully resolved host";
								
								if (!SDLNet_ResolveIP(&m_IP))
								{
									log::err("Karhu") << "Failed to resolve host IP: " << SDLNet_GetError();
									return false;
								}
								else
									log::msg("Karhu") << "Successfully resolved host IP";
								
								m_socket = SDLNet_TCP_Open(&m_IP);
								
								if (!m_socket)
								{
									log::err("Karhu") << "Failed to open socket: " << SDLNet_GetError();
									return false;
								}
								else
								{
									log::msg("Karhu") << "Successfully opened socket";
									
									if (SDLNet_TCP_AddSocket(m_socketset, m_socket) == -1)
									{
										log::err("Karhu") << "Failed to add socket: " << SDLNet_GetError();
										return false;
									}
									else
										log::msg("Karhu") << "Successfully added socket";
								}
							}
						}

						m_connected = true;
						
						return true;
					}
					
					template<typename TChar, std::size_t SizeBuffer>
					bool Client<TChar, SizeBuffer>::performConnectAsync
					(
						const Platform,
						const std::uint16_t,
						const std::string &
					)
					{
						karhuASSERT(false) // Async client not supported!
						return false;
					}
					
					template<typename TChar, std::size_t SizeBuffer>
					void Client<TChar, SizeBuffer>::performDisconnect()
					{
						clean();
					}
					
					template<typename TChar, std::size_t SizeBuffer>
					void Client<TChar, SizeBuffer>::onDisconnect()
					{
						clean();
					}
					
					template<typename TChar, std::size_t SizeBuffer>
					void Client<TChar, SizeBuffer>::clean()
					{
						if (m_socket)
						{
							SDLNet_TCP_Close(m_socket);
							m_socket = nullptr;
						}
						
						if (m_socketset)
						{
							SDLNet_FreeSocketSet(m_socketset);
							m_socketset = nullptr;
						}
					}
				}
			}
		}
	}
#endif
