/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_IMPLEMENTATION_BACKEND_H_
	#define KARHU_APP_BACKEND_IMPLEMENTATION_BACKEND_H_

	#include <karhu/app/backend/Backend.hpp>
	#include <karhu/app/backend/implementation/window/SDL/SubsystemWindow.hpp>
	#include <karhu/app/backend/implementation/input/SDL/SubsystemInput.hpp>
	#include <karhu/app/backend/implementation/file/SDL/SubsystemFile.hpp>
	#include <karhu/app/backend/implementation/graphics/GL/SubsystemGraphics.hpp>
	#include <karhu/app/backend/implementation/audio/SDL/SubsystemAudio.hpp>
	#include <karhu/app/backend/implementation/module/SDL/SubsystemModule.hpp>
	#include <karhu/app/backend/implementation/client/SDL/Client.hpp>
	#include <karhu/core/platform.hpp>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				class Backend : public app::BackendWith
				<
					SDL::SubsystemWindow,
					SDL::SubsystemInput,
					SDL::SubsystemFile,

					gfx::ListSubsystemsGraphics
					<
						GL::SubsystemGraphics
					>,

					snd::ListSubsystemsAudio
					<
						SDL::SubsystemAudio
					>
				
					#ifdef KARHU_EDITOR
					,
					SDL::SubsystemModule,
					SDL::Client
					#endif
				>
				{
					public:
						Backend();
						~Backend();

					protected:
						void performLogVersions() override;
						bool performInit() override;
						void performStartLoop() override;
						bool performBeginFrame(app::FuncImGuiEvent) override;
						void performEndFrame() override;

					private:
						#ifdef KARHU_PLATFORM_WEB
							static void wrapperLoop() { if (c_instance) c_instance->update(); }
						#endif

					private:
						#ifdef KARHU_PLATFORM_WEB
							static Backend *c_instance;
						#endif
				};
			}
		}
	}
#endif
