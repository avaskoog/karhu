/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_INPUT_SDL_SUBSYSTEM_INPUT_H_
	#define KARHU_APP_BACKEND_INPUT_SDL_SUBSYSTEM_INPUT_H_

	#include <karhu/app/inp/SubsystemInput.hpp>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace SDL
				{
					class SubsystemInput : public inp::SubsystemInput
					{
						public:
							void logVersions() override {}
							
						protected:
							bool performInit() override;
							void performDeinit() override;
						
							void performBeginFrame(win::Window &) override;
							void performEndFrame(win::Window &) override;
						
							void mouseCentred(const bool set) override;
							void mouseVisible(const bool set) override;
						
							std::unique_ptr<inp::Controller> performCreateController(void *data) override;
							void performDestroyController(void *) override {}
						
						private:
							bool m_cursorVisible{true};
							bool m_mouseLast{false};
					};
				}
			}
		}
	}
#endif
