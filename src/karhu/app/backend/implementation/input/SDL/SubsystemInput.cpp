/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/backend/implementation/input/SDL/SubsystemInput.hpp>
#include <karhu/app/win/Window.hpp>
#include <karhu/app/App.hpp>
#include <karhu/core/platform.hpp>
#include <SDL2/SDL.h>
#include <cmath>

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace SDL
			{
				class Controller : public inp::Controller
				{
					public:					
						void *handle() const override { return m_data.gamepad; }

					private:
						struct Data
						{
							SDL_JoystickID      ID;
							SDL_Joystick       *joystick{nullptr};
							SDL_GameController *gamepad {nullptr};
						} m_data;

					friend class SubsystemInput;
				};

				bool SubsystemInput::performInit()
				{
					if (0 != SDL_InitSubSystem(SDL_INIT_GAMECONTROLLER))
					{
						log::err("Karhu") << "Error initialising joystick subsystem: " << SDL_GetError();
						return false;
					}
					
					if
					(
						SDL_GameControllerAddMapping
						(
							"4f5559412047616d6520436f6e74726f,OUYA Game Controller,a:b0,b:b3,dpdown:b9,dpleft:b10,dpright:b11,dpup:b8,guide:b14,leftshoulder:b4,leftstick:b6,lefttrigger:a2,leftx:a0,lefty:a1,rightshoulder:b5,rightstick:b7,righttrigger:b13,rightx:a3,righty:a4,x:b1,y:b2,platform:Android"
						) < 0
					)
						log::err("Karhu") << "Failed to add mapping for OUYA Game Controller: " << SDL_GetError();
					else
						log::msg("Karhu") << "Successfully added mapping for OUYA Game Controller";
					
					return true;
				}
				
				void SubsystemInput::performDeinit()
				{
					SDL_QuitSubSystem(SDL_INIT_GAMECONTROLLER);
				}
				
				void SubsystemInput::performBeginFrame(win::Window &w)
				{
					int x, y;
					
					const bool mouse(SDL_GetMouseState(&x, &y) & SDL_BUTTON(SDL_BUTTON_LEFT));
					
					registerMouse
					(
						static_cast<std::int32_t>(std::round(static_cast<float>(x) * w.scaleWidth())),
						static_cast<std::int32_t>(std::round(static_cast<float>(y) * w.scaleHeight()))
					);
					
					/// @todo: This solution doesn't work for fixing mouse delta on touch screens. Figure it out.
					#if defined(KARHU_PLATFORM_IOS) || defined(KARHU_PLATFORM_ANDROID)
					if (!m_mouseLast && mouse)
						registerMouseDelta(0, 0);
					else
					{
					#endif
						SDL_GetRelativeMouseState(&x, &y);
						
						registerMouseDelta
						(
							static_cast<std::int32_t>(std::round(static_cast<float>(x) * w.scaleWidth())),
							static_cast<std::int32_t>(std::round(static_cast<float>(y) * w.scaleHeight()))
						);
					#if defined(KARHU_PLATFORM_IOS) || defined(KARHU_PLATFORM_ANDROID)
					}
					#endif
					
					m_mouseLast = mouse;
					
					SDL_ShowCursor((m_cursorVisible) ? SDL_ENABLE : SDL_DISABLE);
				}
				
				void SubsystemInput::performEndFrame(win::Window &)
				{
				}
				
				void SubsystemInput::mouseCentred(const bool set)
				{
					// This will only cause trouble on devices with a touch
					// screen instead of a mouse so disable entirely there.
					/// @todo: Add Switch.
					#if !defined(KARHU_PLATFORM_IOS) && !defined(KARHU_PLATFORM_ANDROID)
					SDL_SetRelativeMouseMode((set) ? SDL_TRUE : SDL_FALSE);
					#endif
				}
				
				void SubsystemInput::mouseVisible(const bool set)
				{
					m_cursorVisible = set;
					SDL_ShowCursor((set) ? SDL_ENABLE : SDL_DISABLE);
				}
				
				std::unique_ptr<inp::Controller> SubsystemInput::performCreateController(void *data)
				{
					std::unique_ptr<inp::Controller> result{std::make_unique<Controller>()};
					auto controller(dynamic_cast<Controller *>(result.get()));
					const auto ID(*reinterpret_cast<Sint32 *>(data));
					
					if (SDL_IsGameController(ID))
					{
						if (!(controller->m_data.gamepad = SDL_GameControllerOpen(ID)))
						{
							log::err("Karhu") << "Error opening game controller: " << SDL_GetError();
							return {};
						}
						
						controller->m_data.joystick = SDL_GameControllerGetJoystick(controller->m_data.gamepad);
						controller->m_name = SDL_GameControllerNameForIndex(ID);
						
						log::msg("Karhu") << "Opened game controller with identifier " << ID << ": " << controller->name();
					}
					else
					{
//						return {}; /// @todo: Make non-gamepad joy devices work?
						if (!(controller->m_data.joystick = SDL_JoystickOpen(ID)))
						{
							log::err("Karhu") << "Error opening joystick: " << SDL_GetError();
							return {};
						}
						
						controller->m_name = SDL_JoystickNameForIndex(ID);
						
						char buf[64];
						SDL_JoystickGetGUIDString(SDL_JoystickGetGUID(controller->m_data.joystick), buf, 64);
						
						log::msg("Karhu") << "Opened joystick at ID " << ID << ": " << controller->name() << " (GUID " << buf << ')';
						
						return {};
					}
					
					controller->m_data.ID = SDL_JoystickInstanceID(controller->m_data.joystick);
					
					log::msg("Karhu") << "Axis/stick count for joystick/gamepad with identifier " << ID << ": " << SDL_JoystickNumAxes(controller->m_data.joystick);
					
					/// @todo: open haptic/rumble
					
					return result;
				}
			}
		}
	}
}
