/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/App.hpp>
#include <karhu/app/backend/implementation/graphics/GL/SubsystemGraphics.hpp>
#include <karhu/app/backend/implementation/graphics/GL/Renderer.hpp>
#include <karhu/app/backend/implementation/graphics/GL/Mesh.hpp>
#include <karhu/app/backend/implementation/graphics/GL/Texture.hpp>
#include <karhu/app/backend/implementation/graphics/GL/Rendertarget.hpp>
#include <karhu/app/backend/implementation/graphics/GL/Shader.hpp>
#include <karhu/app/backend/implementation/graphics/GL/setup.hpp>
#include <karhu/res/Bank.hpp>
#include <karhu/core/platform.hpp>

#include <karhu/app/lib/im3d/im3d.h>
#include <karhu/app/lib/im3d/im3d_math.h>

// Prints OpenGL integer info.
#define karhuPRINT_INFO_OPENGLi(name) \
	{ \
		GLint v{-1}; \
		glGetIntegerv(name, &v); \
		log::msg("Karhu") << "OpenGL: " #name ": " << v; \
	}

// All of the ImGui code below has been borrowed from the
// SDL/GL example; apologies if it is a bit unrefined.
/// @todo: Fix all these warnings.
/// @todo: Fix the formatting up a bit.
namespace
{
	// Data

	static GLuint       g_FontTexture = 0;
	static int          g_ShaderHandle = 0, g_VertHandle = 0, g_FragHandle = 0;
	static int          g_AttribLocationTex = 0, g_AttribLocationProjMtx = 0;
	static int          g_AttribLocationPosition = 0, g_AttribLocationUV = 0, g_AttribLocationColor = 0;
	static unsigned int g_VboHandle = 0, g_VaoHandle = 0, g_ElementsHandle = 0;

	// This is the main rendering function that you have to implement and provide to ImGui (via setting up 'RenderDrawListsFn' in the ImGuiIO structure)
	// If text or lines are blurry when integrating ImGui in your engine:
	// - in your Render function, try translating your projection matrix by (0.5f,0.5f) or (0.375f,0.375f)
	static void imguiRenderDrawLists(ImDrawData* draw_data)
	{
		glGetError();
		
		// Avoid rendering when minimized, scale coordinates for retina displays (screen coordinates != framebuffer coordinates)
		ImGuiIO& io = ImGui::GetIO();
		int fb_width = (int)(io.DisplaySize.x * io.DisplayFramebufferScale.x);
		int fb_height = (int)(io.DisplaySize.y * io.DisplayFramebufferScale.y);
		if (fb_width == 0 || fb_height == 0)
			return;
		draw_data->ScaleClipRects(io.DisplayFramebufferScale);

		// Backup GL state
		GLint last_program; glGetIntegerv(GL_CURRENT_PROGRAM, &last_program);
		GLint last_texture; glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
		GLint last_active_texture; glGetIntegerv(GL_ACTIVE_TEXTURE, &last_active_texture);
		GLint last_array_buffer; glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
		GLint last_element_array_buffer; glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &last_element_array_buffer);
		GLint last_vertex_array; glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);
		
//		#ifndef KARHU_GLES
//			GLint last_blend_src; glGetIntegerv(GL_BLEND_SRC, &last_blend_src);
//			GLint last_blend_dst; glGetIntegerv(GL_BLEND_DST, &last_blend_dst);
//		#else
			GLint last_blend_src_rgb; glGetIntegerv(GL_BLEND_SRC_RGB, &last_blend_src_rgb);
			GLint last_blend_dst_rgb; glGetIntegerv(GL_BLEND_DST_RGB, &last_blend_dst_rgb);
			GLint last_blend_src_alpha; glGetIntegerv(GL_BLEND_SRC_ALPHA, &last_blend_src_alpha);
			GLint last_blend_dst_alpha; glGetIntegerv(GL_BLEND_DST_ALPHA, &last_blend_dst_alpha);
//		#endif
		
		GLint last_blend_equation_rgb; glGetIntegerv(GL_BLEND_EQUATION_RGB, &last_blend_equation_rgb);
		GLint last_blend_equation_alpha; glGetIntegerv(GL_BLEND_EQUATION_ALPHA, &last_blend_equation_alpha);
		GLint last_viewport[4]; glGetIntegerv(GL_VIEWPORT, last_viewport);
		GLint last_scissor_box[4]; glGetIntegerv(GL_SCISSOR_BOX, last_scissor_box); 
		GLboolean last_enable_blend = glIsEnabled(GL_BLEND);
		GLboolean last_enable_cull_face = glIsEnabled(GL_CULL_FACE);
		GLboolean last_enable_depth_test = glIsEnabled(GL_DEPTH_TEST);
		GLboolean last_enable_scissor_test = glIsEnabled(GL_SCISSOR_TEST);

		// Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled
		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_SCISSOR_TEST);
		glActiveTexture(GL_TEXTURE0);

		// Setup orthographic projection matrix
		glViewport(0, 0, (GLsizei)fb_width, (GLsizei)fb_height);
		const float ortho_projection[4][4] =
		{
			{ 2.0f/io.DisplaySize.x, 0.0f,                   0.0f, 0.0f },
			{ 0.0f,                  2.0f/-io.DisplaySize.y, 0.0f, 0.0f },
			{ 0.0f,                  0.0f,                  -1.0f, 0.0f },
			{-1.0f,                  1.0f,                   0.0f, 1.0f },
		};
		glUseProgram(g_ShaderHandle);
		glUniform1i(g_AttribLocationTex, 0);
		glUniformMatrix4fv(g_AttribLocationProjMtx, 1, GL_FALSE, &ortho_projection[0][0]);
		glBindVertexArray(g_VaoHandle);
		
		glBindFramebuffer(GL_FRAMEBUFFER, karhu::app::implementation::GL::Renderer::framebufferDefault());
		glBindRenderbuffer(GL_RENDERBUFFER, karhu::app::implementation::GL::Renderer::renderbufferDefault());

		for (int n = 0; n < draw_data->CmdListsCount; n++)
		{
			const ImDrawList* cmd_list = draw_data->CmdLists[n];
			const ImDrawIdx* idx_buffer_offset = 0;

			glBindBuffer(GL_ARRAY_BUFFER, g_VboHandle);
			glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)cmd_list->VtxBuffer.Size * sizeof(ImDrawVert), (GLvoid*)cmd_list->VtxBuffer.Data, GL_STREAM_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_ElementsHandle);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx), (GLvoid*)cmd_list->IdxBuffer.Data, GL_STREAM_DRAW);

			for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
			{
				const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
				if (pcmd->UserCallback)
				{
					pcmd->UserCallback(cmd_list, pcmd);
				}
				else
				{
					glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
					glScissor((int)pcmd->ClipRect.x, (int)(fb_height - pcmd->ClipRect.w), (int)(pcmd->ClipRect.z - pcmd->ClipRect.x), (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
					glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, idx_buffer_offset);
				}
				idx_buffer_offset += pcmd->ElemCount;
			}
		}

		// Restore modified GL state
		glUseProgram(last_program);
		glActiveTexture(last_active_texture);
		glBindTexture(GL_TEXTURE_2D, last_texture);
		glBindVertexArray(last_vertex_array);
		glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, last_element_array_buffer);
		glBlendEquationSeparate(last_blend_equation_rgb, last_blend_equation_alpha);
		
//		#ifndef KARHU_GLES
//			glBlendFunc(last_blend_src, last_blend_dst);
//		#else
			glBlendFuncSeparate(last_blend_src_rgb, last_blend_dst_rgb, last_blend_src_alpha, last_blend_dst_alpha);
//		#endif
		
		if (last_enable_blend) glEnable(GL_BLEND); else glDisable(GL_BLEND);
		if (last_enable_cull_face) glEnable(GL_CULL_FACE); else glDisable(GL_CULL_FACE);
		if (last_enable_depth_test) glEnable(GL_DEPTH_TEST); else glDisable(GL_DEPTH_TEST);
		if (last_enable_scissor_test) glEnable(GL_SCISSOR_TEST); else glDisable(GL_SCISSOR_TEST);
		glViewport(last_viewport[0], last_viewport[1], (GLsizei)last_viewport[2], (GLsizei)last_viewport[3]);
		glScissor(last_scissor_box[0], last_scissor_box[1], (GLsizei)last_scissor_box[2], (GLsizei)last_scissor_box[3]);

	}

	static void imguiCreateFontsTexture()
	{
		// Build texture atlas
		ImGuiIO& io = ImGui::GetIO();
		unsigned char* pixels;
		int width, height;
		io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);   // Load as RGBA 32-bits for OpenGL3 demo because it is more likely to be compatible with user's existing shader.

		// Upload texture to graphics system
		GLint last_texture;
		glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
		glGenTextures(1, &g_FontTexture);
		glBindTexture(GL_TEXTURE_2D, g_FontTexture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

		// Store our identifier
		io.Fonts->TexID = (void *)(intptr_t)g_FontTexture;

		// Restore state
		glBindTexture(GL_TEXTURE_2D, last_texture);
	}
	
	using namespace karhu;
	using namespace karhu::app::implementation::GL;

	static void logShader(GLuint ID, const char *type)
	{
		if (glIsShader(ID))
		{
			GLint length{0};
			GLint max{0};
			
			glGetShaderiv(ID, GL_INFO_LOG_LENGTH, &max);
			
			if (max > 0)
			{
				auto log(std::make_unique<char[]>(static_cast<std::size_t>(max)));
				glGetShaderInfoLog(ID, max, &length, log.get());
				
				if (length > 0)
					log::err("Karhu") << "Failed to compile " << type << " shader: " << log.get();
			}
		}
		else
			log::err("Karhu") << "Failed to compile " << type << " shader: not a shader";
	}

	static bool imguiCreateDeviceObjects()
	{
		if (g_FontTexture) return true;

		// Backup GL state
		GLint last_texture, last_array_buffer, last_vertex_array;
		glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
		glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
		glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);

		const GLchar *vertex_shader =
			#ifdef KARHU_GLES
			"#version 300 es\n"
			#else
			"#version 330 core\n"
			#endif
			"precision highp float;\n"
			"precision highp int;\n"
			"uniform mat4 ProjMtx;\n"
			"in vec2 Position;\n"
			"in vec2 UV;\n"
			"in vec4 Color;\n"
			"out vec2 Frag_UV;\n"
			"out vec4 Frag_Color;\n"
			"void main()\n"
			"{\n"
			"	Frag_UV = UV;\n"
			"	Frag_Color = Color;\n"
			"	gl_Position = ProjMtx * vec4(Position.xy,0,1);\n"
			"}\n";

		const GLchar* fragment_shader =
			#ifdef KARHU_GLES
			"#version 300 es\n"
			#else
			"#version 330 core\n"
			#endif
			"precision highp float;\n"
			"precision highp int;\n"
			"uniform sampler2D Texture;\n"
			"in vec2 Frag_UV;\n"
			"in vec4 Frag_Color;\n"
			"out vec4 Out_Color;\n"
			"void main()\n"
			"{\n"
			"	Out_Color = Frag_Color * texture( Texture, Frag_UV.st);\n"
			"}\n";

		g_ShaderHandle = glCreateProgram();
		g_VertHandle = glCreateShader(GL_VERTEX_SHADER);
		g_FragHandle = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(g_VertHandle, 1, &vertex_shader, 0);
		glShaderSource(g_FragHandle, 1, &fragment_shader, 0);
		
		GLuint ret = 0;
		{
		glCompileShader(g_VertHandle);
		GLint compileStatus = GL_FALSE;
		glGetShaderiv(g_VertHandle, GL_COMPILE_STATUS, &compileStatus);
		if (compileStatus == GL_FALSE)
			logShader(g_VertHandle, "vert");
		}
		{
		glCompileShader(g_FragHandle);
		GLint compileStatus = GL_FALSE;
		glGetShaderiv(g_FragHandle, GL_COMPILE_STATUS, &compileStatus);
		if (compileStatus == GL_FALSE)
			logShader(g_FragHandle, "frag");
		}
		
		glAttachShader(g_ShaderHandle, g_VertHandle);
		glAttachShader(g_ShaderHandle, g_FragHandle);
		glLinkProgram(g_ShaderHandle);

		g_AttribLocationTex = glGetUniformLocation(g_ShaderHandle, "Texture");
		g_AttribLocationProjMtx = glGetUniformLocation(g_ShaderHandle, "ProjMtx");
		g_AttribLocationPosition = glGetAttribLocation(g_ShaderHandle, "Position");
		g_AttribLocationUV = glGetAttribLocation(g_ShaderHandle, "UV");
		g_AttribLocationColor = glGetAttribLocation(g_ShaderHandle, "Color");

		glGenBuffers(1, &g_VboHandle);
		glGenBuffers(1, &g_ElementsHandle);

		glGenVertexArrays(1, &g_VaoHandle);
		glBindVertexArray(g_VaoHandle);
		glBindBuffer(GL_ARRAY_BUFFER, g_VboHandle);
		glEnableVertexAttribArray(g_AttribLocationPosition);
		glEnableVertexAttribArray(g_AttribLocationUV);
		glEnableVertexAttribArray(g_AttribLocationColor);

	#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))
		glVertexAttribPointer(g_AttribLocationPosition, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, pos));
		glVertexAttribPointer(g_AttribLocationUV, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, uv));
		glVertexAttribPointer(g_AttribLocationColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, col));
	#undef OFFSETOF

		imguiCreateFontsTexture();

		// Restore modified GL state
		glBindTexture(GL_TEXTURE_2D, last_texture);
		glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
		glBindVertexArray(last_vertex_array);

		return true;
	}

	static void imguiInvalidateDeviceObjects()
	{
		if (g_VaoHandle) glDeleteVertexArrays(1, &g_VaoHandle);
		if (g_VboHandle) glDeleteBuffers(1, &g_VboHandle);
		if (g_ElementsHandle) glDeleteBuffers(1, &g_ElementsHandle);
		g_VaoHandle = g_VboHandle = g_ElementsHandle = 0;

		if (g_ShaderHandle && g_VertHandle) glDetachShader(g_ShaderHandle, g_VertHandle);
		if (g_VertHandle) glDeleteShader(g_VertHandle);
		g_VertHandle = 0;

		if (g_ShaderHandle && g_FragHandle) glDetachShader(g_ShaderHandle, g_FragHandle);
		if (g_FragHandle) glDeleteShader(g_FragHandle);
		g_FragHandle = 0;

		if (g_ShaderHandle) glDeleteProgram(g_ShaderHandle);
		g_ShaderHandle = 0;

		if (g_FontTexture)
		{
			glDeleteTextures(1, &g_FontTexture);
			ImGui::GetIO().Fonts->TexID = 0;
			g_FontTexture = 0;
		}
	}

	// Im3d setup.
	
	static constexpr char const *Im3D_GLSL
{R"(
#if !defined(POINTS) && !defined(LINES) && !defined(TRIANGLES)
	#error No primitive type defined
#endif
#if !defined(VERTEX_SHADER) && !defined(FRAGMENT_SHADER)
	#error No shader stage defined
#endif

#if defined(KARHU_GLES)
	#define MOD_NOPERSPECTIVE
	#define MOD_SMOOTH
#else
	#define MOD_NOPERSPECTIVE noperspective
	#define MOD_SMOOTH smooth
#endif

#define kAntialiasing 2.0
precision highp float;
precision highp int;
#ifdef VERTEX_SHADER
/*	This vertex shader fetches Im3d vertex data manually from a uniform buffer (uVertexData). It assumes that the bound vertex buffer contains 4 vertices as follows:

	 -1,1       1,1
	   2 ------- 3
	   | \       |
	   |   \     |
	   |     \   |
	   |       \ |
	   0 ------- 1
	 -1,-1      1,-1
 
	Both the vertex ID and the vertex position are used for point/line expansion. This vertex buffer is valid for both triangle strip rendering (points/lines), and
	triangle render using vertices 0,1,2.
 
	See im3d_opengl31.cpp for more details.
*/

	struct VertexData
	{
		vec4 m_positionSize;
		uint m_color;
	};
	uniform VertexDataBlock
	{
		VertexData uVertexData
		[
			#if defined(KARHU_GLES)
			16384
			#else
			(64 * 1024)
			#endif
			/ 32 // 32 is the aligned size of VertexData
		];
	};

	uniform mat4 uViewProjMatrix;
	uniform vec2 uViewport;
	
	in vec4 aPosition;
	
	#if   defined(POINTS)
		MOD_NOPERSPECTIVE out vec2 vUv;
	#elif defined(LINES)
		MOD_NOPERSPECTIVE out float vEdgeDistance;
	#endif
	MOD_NOPERSPECTIVE out float vSize;
	MOD_SMOOTH out vec4  vColor;
	
	vec4 UintToRgba(uint _u)
	{
		vec4 ret = vec4(0.0);
		ret.r = float((_u & 0xff000000u) >> 24u) / 255.0;
		ret.g = float((_u & 0x00ff0000u) >> 16u) / 255.0;
		ret.b = float((_u & 0x0000ff00u) >> 8u)  / 255.0;
		ret.a = float((_u & 0x000000ffu) >> 0u)  / 255.0;
		return ret;
	}
	
	void main()
	{
		#if   defined(POINTS)
			int vid = gl_InstanceID;
		
			vSize = max(uVertexData[vid].m_positionSize.w, kAntialiasing);
			vColor = UintToRgba(uVertexData[vid].m_color);
			vColor.a *= smoothstep(0.0, 1.0, vSize / kAntialiasing);
		
			gl_Position = uViewProjMatrix * vec4(uVertexData[vid].m_positionSize.xyz, 1.0);
			vec2 scale = 1.0 / uViewport * vSize;
			gl_Position.xy += aPosition.xy * scale * gl_Position.w;
			vUv = aPosition.xy * 0.5 + 0.5;
		
		#elif defined(LINES)
			int vid0  = gl_InstanceID * 2; // line start
			int vid1  = vid0 + 1; // line end
			int vid   = (gl_VertexID % 2 == 0) ? vid0 : vid1; // data for this vertex
		
			vColor = UintToRgba(uVertexData[vid].m_color);
			vSize = uVertexData[vid].m_positionSize.w;
			vColor.a *= smoothstep(0.0, 1.0, vSize / kAntialiasing);
			vSize = max(vSize, kAntialiasing);
			vEdgeDistance = vSize * aPosition.y;
		
			vec4 pos0  = uViewProjMatrix * vec4(uVertexData[vid0].m_positionSize.xyz, 1.0);
			vec4 pos1  = uViewProjMatrix * vec4(uVertexData[vid1].m_positionSize.xyz, 1.0);
			vec2 dir = (pos0.xy / pos0.w) - (pos1.xy / pos1.w);
			dir = normalize(vec2(dir.x, dir.y * uViewport.y / uViewport.x)); // correct for aspect ratio
			vec2 tng = vec2(-dir.y, dir.x) * vSize / uViewport;
		
			gl_Position = (gl_VertexID % 2 == 0) ? pos0 : pos1;
			gl_Position.xy += tng * aPosition.y * gl_Position.w;
		
		#elif defined(TRIANGLES)
			int vid = gl_InstanceID * 3 + gl_VertexID;
			
			vColor = UintToRgba(uVertexData[vid].m_color);
			gl_Position = uViewProjMatrix * vec4(uVertexData[vid].m_positionSize.xyz, 1.0);
		
		#endif
	}
#endif

#ifdef FRAGMENT_SHADER
	#if   defined(POINTS)
		MOD_NOPERSPECTIVE in vec2 vUv;
	#elif defined(LINES)
		MOD_NOPERSPECTIVE in float vEdgeDistance;
	#endif
	MOD_NOPERSPECTIVE in float vSize;
	MOD_SMOOTH in vec4  vColor;
	
	out vec4 fResult;
	
	void main()
	{
		fResult = vColor;
		#if   defined(LINES)
			float d = abs(vEdgeDistance) / vSize;
			d = smoothstep(1.0, 1.0 - (kAntialiasing / vSize), d);
			fResult.a *= d;
		#elif defined(POINTS)
			float d = length(vUv - vec2(0.5));
			d = smoothstep(0.5, 0.5 - (kAntialiasing / vSize), d);
			fResult.a *= d;
		#endif
	}
#endif
)"};
	
	using namespace Im3d;
	
	static GLuint g_Im3dVertexArray = 0;
	static GLuint g_Im3dVertexBuffer = 0;
	static GLuint g_Im3dUniformBuffer = 0;
	static GLuint g_Im3dShaderPoints = 0;
	static GLuint g_Im3dShaderLines = 0;
	static GLuint g_Im3dShaderTriangles = 0;
	
	static void Append(const char* _str, Vector<char>& _out_)
	{
		while (*_str) {
			_out_.push_back(*_str);
			++_str;
		}
	}
	
	static void AppendLine(const char* _str, Vector<char>& _out_)
	{
		Append(_str, _out_);
		_out_.push_back('\n');
	}
	
	static bool LoadShader(const char* _code, const std::size_t &_len, const char* _defines, Vector<char>& _out_)
	{
		if (_defines) {
			while (*_defines != '\0') {
				Append("#define ", _out_);
				AppendLine(_defines, _out_);
				_defines = strchr(_defines, 0);
				++_defines;
			}
		}

		int srcbeg = _out_.size();
		_out_.resize(srcbeg + _len, '\0');
		std::memcpy(_out_.data() + srcbeg, _code, _len);
		_out_.push_back('\0');

		return true;
	}
	
	enum class Im3DTypeShader : std::uint8_t
	{
		points,
		lines,
		triangles
	};
	
	static GLuint LoadCompileShader
	(
		const GLenum         stage,
		const Im3DTypeShader type
	)
	{
		std::string code
		{
			#ifdef KARHU_GLES
			"#version 300 es\n"
			"#define MOD_NOPERSPECTIVE\n"
			"#define MOD_SMOOTH\n"
			#else
			"#version 330 core\n"
			"#define MOD_NOPERSPECTIVE noperspective\n"
			"#define MOD_SMOOTH smooth\n"
			#endif
		};
		
		if (GL_VERTEX_SHADER == stage)
			code += "#define VERTEX_SHADER\n";
		else if (GL_FRAGMENT_SHADER)
			code += "#define FRAGMENT_SHADER\n";
		
		switch (type)
		{
			case Im3DTypeShader::points:
				code += "#define POINTS\n";
				break;
			
			case Im3DTypeShader::lines:
				code += "#define LINES\n";
				break;
			
			case Im3DTypeShader::triangles:
				code += "#define TRIANGLES\n";
				break;
		}
		
		code +=
		R"(
			#define kAntialiasing 2.0
			
			precision highp float;
			precision highp int;
		 
			#ifdef VERTEX_SHADER
				struct VertexData
				{
					vec4 m_positionSize;
					uint m_color;
				};
		
				uniform VertexDataBlock
				{
					VertexData uVertexData
					[
		)"
						#ifdef KARHU_GLES
						"16384"
						#else
						"(64 * 1024)"
						#endif
		R"(
						/ 32 // 32 is the aligned size of VertexData
					];
				};

				uniform mat4 uViewProjMatrix;
				uniform vec2 uViewport;
		
				in vec4 aPosition;
		
				#if defined(POINTS)
					MOD_NOPERSPECTIVE out vec2 vUv;
				#elif defined(LINES)
					MOD_NOPERSPECTIVE out float vEdgeDistance;
				#endif
				MOD_NOPERSPECTIVE out float vSize;
				MOD_SMOOTH out vec4  vColor;
		
				vec4 UintToRgba(uint _u)
				{
					vec4 ret = vec4(0.0);
					ret.r = float((_u & 0xff000000u) >> 24u) / 255.0;
					ret.g = float((_u & 0x00ff0000u) >> 16u) / 255.0;
					ret.b = float((_u & 0x0000ff00u) >> 8u)  / 255.0;
					ret.a = float((_u & 0x000000ffu) >> 0u)  / 255.0;
					return ret;
				}
		
				void main()
				{
					#if   defined(POINTS)
						int vid = gl_InstanceID;
					
						vSize = max(uVertexData[vid].m_positionSize.w, kAntialiasing);
						vColor = UintToRgba(uVertexData[vid].m_color);
						vColor.a *= smoothstep(0.0, 1.0, vSize / kAntialiasing);
					
						gl_Position = uViewProjMatrix * vec4(uVertexData[vid].m_positionSize.xyz, 1.0);
						vec2 scale = 1.0 / uViewport * vSize;
						gl_Position.xy += aPosition.xy * scale * gl_Position.w;
						vUv = aPosition.xy * 0.5 + 0.5;
					
					#elif defined(LINES)
						int vid0  = gl_InstanceID * 2; // line start
						int vid1  = vid0 + 1; // line end
						int vid   = (gl_VertexID % 2 == 0) ? vid0 : vid1; // data for this vertex
					
						vColor = UintToRgba(uVertexData[vid].m_color);
						vSize = uVertexData[vid].m_positionSize.w;
						vColor.a *= smoothstep(0.0, 1.0, vSize / kAntialiasing);
						vSize = max(vSize, kAntialiasing);
						vEdgeDistance = vSize * aPosition.y;
					
						vec4 pos0  = uViewProjMatrix * vec4(uVertexData[vid0].m_positionSize.xyz, 1.0);
						vec4 pos1  = uViewProjMatrix * vec4(uVertexData[vid1].m_positionSize.xyz, 1.0);
						vec2 dir = (pos0.xy / pos0.w) - (pos1.xy / pos1.w);
						dir = normalize(vec2(dir.x, dir.y * uViewport.y / uViewport.x)); // correct for aspect ratio
						vec2 tng = vec2(-dir.y, dir.x) * vSize / uViewport;
					
						gl_Position = (gl_VertexID % 2 == 0) ? pos0 : pos1;
						gl_Position.xy += tng * aPosition.y * gl_Position.w;
					
					#elif defined(TRIANGLES)
						int vid = gl_InstanceID * 3 + gl_VertexID;
					
						vColor = UintToRgba(uVertexData[vid].m_color);
						gl_Position = uViewProjMatrix * vec4(uVertexData[vid].m_positionSize.xyz, 1.0);
					
					#endif
				}
			#endif

			#ifdef FRAGMENT_SHADER
				#if   defined(POINTS)
					MOD_NOPERSPECTIVE in vec2 vUv;
				#elif defined(LINES)
					MOD_NOPERSPECTIVE in float vEdgeDistance;
				#endif
				MOD_NOPERSPECTIVE in float vSize;
				MOD_SMOOTH in vec4  vColor;
		
				out vec4 fResult;
		
				void main()
				{
					fResult = vColor;
					#if   defined(LINES)
						float d = abs(vEdgeDistance) / vSize;
						d = smoothstep(1.0, 1.0 - (kAntialiasing / vSize), d);
						fResult.a *= d;
					#elif defined(POINTS)
						float d = length(vUv - vec2(0.5));
						d = smoothstep(0.5, 0.5 - (kAntialiasing / vSize), d);
						fResult.a *= d;
					#endif
				}
			#endif
		)";
		
		GLuint ret = 0;
		ret = glCreateShader(stage);
		
		const char *pc{code.c_str()};

		glShaderSource(ret, 1, &pc, 0);
	
		glCompileShader(ret);
		GLint compileStatus = GL_FALSE;
		glGetShaderiv(ret, GL_COMPILE_STATUS, &compileStatus);
		if (compileStatus == GL_FALSE) {
			GLint len;
			glGetShaderiv(ret, GL_INFO_LOG_LENGTH, &len);
			char* log = new GLchar[len];
			glGetShaderInfoLog(ret, len, 0, log);
			
			karhu::log::err("Im3d")
				<< "Failed to compile "
				<< ((GL_FRAGMENT_SHADER == stage) ? "fragment" : "vertex")
				<< ' '
				<< ((Im3DTypeShader::points == type) ? "point" : ((Im3DTypeShader::lines == type) ? "line" : "triangle"))
				<< " shader program: "
				<< log;
			
			delete[] log;
	
			glDeleteShader(ret);
			
			return 0;
		}
		
		return ret;
	}
	
	static bool LinkShaderProgram(GLuint _handle, const Im3DTypeShader type)
	{
		glLinkProgram(_handle);
		GLint linkStatus = GL_FALSE;
		glGetProgramiv(_handle, GL_LINK_STATUS, &linkStatus);
		if (linkStatus == GL_FALSE) {
			GLint len;
			glGetProgramiv(_handle, GL_INFO_LOG_LENGTH, &len);
			GLchar* log = new GLchar[len];
			glGetProgramInfoLog(_handle, len, 0, log);
			
			karhu::log::err("Im3d")
				<< "Failed to link "
				<< ' '
				<< ((Im3DTypeShader::points == type) ? "point" : ((Im3DTypeShader::lines == type) ? "line" : "triangle"))
				<< " shader program: "
				<< log;
			
			delete[] log;
	
			return false;
		}
		return true;
	}
	
	static bool im3d_Init()
	{
		const std::size_t _len{std::strlen(Im3D_GLSL)};
		
		{
			GLuint vs = LoadCompileShader(GL_VERTEX_SHADER,   Im3DTypeShader::points);
			GLuint fs = LoadCompileShader(GL_FRAGMENT_SHADER, Im3DTypeShader::points);
			if (vs && fs) {
			
				g_Im3dShaderPoints = glCreateProgram();
				glAttachShader(g_Im3dShaderPoints, vs);
				glAttachShader(g_Im3dShaderPoints, fs);
				bool ret = LinkShaderProgram(g_Im3dShaderPoints, Im3DTypeShader::points);
				glDeleteShader(vs);
				glDeleteShader(fs);
				if (!ret) {
					return false;
				}
			} else {
				return false;
			}
			GLuint blockIndex;
			blockIndex = glGetUniformBlockIndex(g_Im3dShaderPoints, "VertexDataBlock");
			glUniformBlockBinding(g_Im3dShaderPoints, blockIndex, 0);
		}
		{
			GLuint vs = LoadCompileShader(GL_VERTEX_SHADER,   Im3DTypeShader::lines);
			GLuint fs = LoadCompileShader(GL_FRAGMENT_SHADER, Im3DTypeShader::lines);
			if (vs && fs) {
				g_Im3dShaderLines = glCreateProgram();
				glAttachShader(g_Im3dShaderLines, vs);
				glAttachShader(g_Im3dShaderLines, fs);
				bool ret = LinkShaderProgram(g_Im3dShaderLines, Im3DTypeShader::lines);
				glDeleteShader(vs);
				glDeleteShader(fs);
				if (!ret) {
					return false;
				}
			} else {
				return false;
			}
			GLuint blockIndex;
			blockIndex = glGetUniformBlockIndex(g_Im3dShaderLines, "VertexDataBlock");
			glUniformBlockBinding(g_Im3dShaderLines, blockIndex, 0);
		}
		{
			GLuint vs = LoadCompileShader(GL_VERTEX_SHADER,   Im3DTypeShader::triangles);
			GLuint fs = LoadCompileShader(GL_FRAGMENT_SHADER, Im3DTypeShader::triangles);
			if (vs && fs) {
				g_Im3dShaderTriangles = glCreateProgram();
				glAttachShader(g_Im3dShaderTriangles, vs);
				glAttachShader(g_Im3dShaderTriangles, fs);
				bool ret = LinkShaderProgram(g_Im3dShaderTriangles, Im3DTypeShader::triangles);
				glDeleteShader(vs);
				glDeleteShader(fs);
				if (!ret) {
					return false;
				}
			} else {
				return false;
			}
			GLuint blockIndex;
			blockIndex = glGetUniformBlockIndex(g_Im3dShaderTriangles, "VertexDataBlock");
			glUniformBlockBinding(g_Im3dShaderTriangles, blockIndex, 0);
		}

	 // in this example we're using a static buffer as the vertex source with a uniform buffer to provide
	 // the shader with the Im3d vertex data
		Im3d::Vec4 vertexData[] = {
			Im3d::Vec4(-1.0f, -1.0f, 0.0f, 1.0f),
			Im3d::Vec4( 1.0f, -1.0f, 0.0f, 1.0f),
			Im3d::Vec4(-1.0f,  1.0f, 0.0f, 1.0f),
			Im3d::Vec4( 1.0f,  1.0f, 0.0f, 1.0f)
		};
		glGenBuffers(1, &g_Im3dVertexBuffer);
		glGenVertexArrays(1, &g_Im3dVertexArray);
		glBindVertexArray(g_Im3dVertexArray);
		glBindBuffer(GL_ARRAY_BUFFER, g_Im3dVertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), (GLvoid*)vertexData, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Im3d::Vec4), (GLvoid*)0);
		glBindVertexArray(0);

		glGenBuffers(1, &g_Im3dUniformBuffer);

		return true;
	}
	
	static void Im3d_Shutdown()
	{
		glDeleteVertexArrays(1, &g_Im3dVertexArray);
		glDeleteBuffers(1, &g_Im3dUniformBuffer);
		glDeleteBuffers(1, &g_Im3dVertexBuffer);
		glDeleteProgram(g_Im3dShaderPoints);
		glDeleteProgram(g_Im3dShaderLines);
		glDeleteProgram(g_Im3dShaderTriangles);
	}
	
	static Mat4 Im3d_VP;
	
	static void Im3d_NewFrame
	(
		const float &dt,
		const float &w,
		const float &h,
		const bool ortho,
		const Vec3 &campos,
		const Vec3 &camdir,
		const float &FOVRad,
		const Mat4 &world,
		const Mat4 &proj,
		const Mat4 &viewproj,
		const Vec2 &cursorpos,
		const bool mouseDownLeft,
		const bool keyDownCtrl,
		const bool keyDownL,
		const bool keyDownT,
		const bool keyDownR,
		const bool keyDownS
	)
	{
		Im3d_VP = viewproj;
		
		AppData& ad = GetAppData();
		
		ad.m_deltaTime     = dt;
		ad.m_viewportSize  = Vec2(w, h);
		ad.m_viewOrigin    = campos; // for VR use the head position
		ad.m_viewDirection = camdir;
		ad.m_worldUp       = Vec3(0.0f, 1.0f, 0.0f); // used internally for generating orthonormal bases
		ad.m_projOrtho     = ortho;

	 // m_projScaleY controls how gizmos are scaled in world space to maintain a constant screen height
		ad.m_projScaleY   = ortho
			? 2.0f / proj(1, 1) // use far plane height for an ortho projection
			: tanf(FOVRad * 0.5f) * 2.0f // or vertical fov for a perspective projection
			;

	 // World space cursor ray from mouse position; for VR this might be the position/orientation of the HMD or a tracked controller.
		Vec2 cursorPos = cursorpos;
		cursorPos = (cursorPos / ad.m_viewportSize) * 2.0f - Vec2(1.0f, 1.0f);
		cursorPos.y = -cursorPos.y; // window origin is top-left, ndc is bottom-left
		Vec3 rayOrigin, rayDirection;
		if (ortho) {
			rayOrigin.x  = cursorPos.x / proj(0, 0);
			rayOrigin.y  = cursorPos.y / proj(1, 1);
			rayOrigin.z  = 0.0f;
			rayOrigin    = world * Vec4(rayOrigin, 1.0f);
			rayDirection = world * Vec4(0.0f, 0.0f, -1.0f, 0.0f);

		} else {
			rayOrigin = ad.m_viewOrigin;
			rayDirection.x  = cursorPos.x / proj(0, 0);
			rayDirection.y  = cursorPos.y / proj(1, 1);
			rayDirection.z  = -1.0f;
			rayDirection    = world * Vec4(Normalize(rayDirection), 0.0f);
		}
		ad.m_cursorRayOrigin = rayOrigin;
		ad.m_cursorRayDirection = rayDirection;

	 // Set cull frustum planes. This is only required if IM3D_CULL_GIZMOS or IM3D_CULL_PRIMTIIVES is enable via im3d_config.h, or if any of the IsVisible() functions are called.
		ad.setCullFrustum(viewproj, true);

	 // Fill the key state array; using GetAsyncKeyState here but this could equally well be done via the window proc.
	 // All key states have an equivalent (and more descriptive) 'Action_' enum.
		ad.m_keyDown[Im3d::Mouse_Left/*Im3d::Action_Select*/] = mouseDownLeft;

	 // The following key states control which gizmo to use for the generic Gizmo() function. Here using the left ctrl key as an additional predicate.
		bool ctrlDown = keyDownCtrl;
		ad.m_keyDown[Im3d::Key_L/*Action_GizmoLocal*/]       = ctrlDown && keyDownL;
		ad.m_keyDown[Im3d::Key_T/*Action_GizmoTranslation*/] = ctrlDown && keyDownT;
		ad.m_keyDown[Im3d::Key_R/*Action_GizmoRotation*/]    = ctrlDown && keyDownR;
		ad.m_keyDown[Im3d::Key_S/*Action_GizmoScale*/]       = ctrlDown && keyDownS;

	 // Enable gizmo snapping by setting the translation/rotation/scale increments to be > 0
		ad.m_snapTranslation = ctrlDown ? 0.1f : 0.0f;
		ad.m_snapRotation    = ctrlDown ? Im3d::Radians(30.0f) : 0.0f;
		ad.m_snapScale       = ctrlDown ? 0.5f : 0.0f;

		Im3d::NewFrame();
	}
	
	static void Im3d_EndFrame(const int &w, const int &h)
	{
		Im3d::EndFrame();
		
		glClear(GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		glViewport(0, 0, (GLsizei)w, (GLsizei)h);
		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		
/// @todo: Commented out while testing Im3d and we want to draw it to an offscreen buffer.
//		glBindFramebuffer(GL_FRAMEBUFFER, karhu::app::implementation::GL::Renderer::framebufferDefault());
//		glBindRenderbuffer(GL_RENDERBUFFER, karhu::app::implementation::GL::Renderer::renderbufferDefault());

		for (U32 i = 0, n = Im3d::GetDrawListCount(); i < n; ++i) {
			auto& drawList = Im3d::GetDrawLists()[i];
		
			if (drawList.m_layerId == Im3d::MakeId("NamedLayer")) {
			 // The application may group primitives into layers, which can be used to change the draw state (e.g. enable depth testing, use a different shader)
			}
		
			GLenum prim;
			GLuint sh;
			int primVertexCount;
			switch (drawList.m_primType) {
				case Im3d::DrawPrimitive_Points:
					prim = GL_TRIANGLE_STRIP;
					primVertexCount = 1;
					sh = g_Im3dShaderPoints;
					glDisable(GL_CULL_FACE); // points are view-aligned
					break;
				case Im3d::DrawPrimitive_Lines:
					prim = GL_TRIANGLE_STRIP;
					primVertexCount = 2;
					sh = g_Im3dShaderLines;
					glDisable(GL_CULL_FACE); // lines are view-aligned
					break;
				case Im3d::DrawPrimitive_Triangles:
					prim = GL_TRIANGLES;
					primVertexCount = 3;
					sh = g_Im3dShaderTriangles;
					//glEnable(GL_CULL_FACE); // culling valid for triangles, but optional
					break;
				default:
					return;
			};
		
			glBindVertexArray(g_Im3dVertexArray);
			glBindBuffer(GL_ARRAY_BUFFER, g_Im3dVertexBuffer);
		
			glUseProgram(sh);
			AppData& ad = GetAppData();
			
			glUniform2f(glGetUniformLocation(sh, "uViewport"), ad.m_viewportSize.x, ad.m_viewportSize.y);
			glUniformMatrix4fv(glGetUniformLocation(sh, "uViewProjMatrix"), 1, false, (const GLfloat*)Im3d_VP);
		
		 // Uniform buffers have a size limit; split the vertex data into several passes.
			const int kMaxBufferSize = 64 * 1024; // assuming 64kb here but the application should check the implementation limit
			const int kPrimsPerPass = kMaxBufferSize / (sizeof (Im3d::VertexData) * primVertexCount);
		
			int remainingPrimCount = drawList.m_vertexCount / primVertexCount;
			const Im3d::VertexData* vertexData = drawList.m_vertexData;

			while (remainingPrimCount > 0) {

				int passPrimCount = remainingPrimCount < kPrimsPerPass ? remainingPrimCount : kPrimsPerPass;
				int passVertexCount = passPrimCount * primVertexCount;
		
				glBindBuffer(GL_UNIFORM_BUFFER, g_Im3dUniformBuffer);
				glBufferData(GL_UNIFORM_BUFFER, (GLsizeiptr)passVertexCount * sizeof(Im3d::VertexData), (GLvoid*)vertexData, GL_DYNAMIC_DRAW);
		
			 // instanced draw call, 1 instance per prim
				glBindBufferBase(GL_UNIFORM_BUFFER, 0, g_Im3dUniformBuffer);
				glDrawArraysInstanced(prim, 0, prim == GL_TRIANGLES ? 3 : 4, passPrimCount); // for triangles just use the first 3 verts of the strip
		
				vertexData += passVertexCount;
				remainingPrimCount -= passPrimCount;
			}
		}
	}
}

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace GL
			{
				void SubsystemGraphics::logVersions()
				{
					if (const auto v = glGetString(GL_VERSION))
					{
						constexpr char const *prefix{"OpenGL "};
						
						const std::string vs{reinterpret_cast<const char *>(v)};
						const std::size_t lp{std::strlen(prefix)};
						
						if (vs.length() >= lp && vs.substr(0, lp) == prefix)
							log::msg("Karhu") << vs;
						else
							log::msg("Karhu") << prefix << vs;
					}
					else
						log::msg("Karhu") << "OpenGL N/A";
					/*
					karhuPRINT_INFO_OPENGLi(GL_MAX_VERTEX_ATTRIBS)
					karhuPRINT_INFO_OPENGLi(GL_MAX_TEXTURE_IMAGE_UNITS)
					karhuPRINT_INFO_OPENGLi(GL_MAX_COLOR_ATTACHMENTS)
					karhuPRINT_INFO_OPENGLi(GL_MAX_DRAW_BUFFERS)
					
					/// @todo: Figure out how to do this on web.
					#ifndef KARHU_PLATFORM_WEB
					{
						GLint v{-1};
						glGetFramebufferAttachmentParameteriv
						(
							GL_FRAMEBUFFER,
							GL_STENCIL,
							GL_FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE,
							&v
						);
						log::msg("Karhu") << "GL_FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE: " << v;
					}
					#endif*/
				}
				
				bool SubsystemGraphics::performInit(app::App &app)
				{
					app.res().registerDerivedType<res::Shader,  GL::Shader>();
					app.res().registerDerivedType<res::Texture, GL::Texture>();
					app.res().registerDerivedType<res::Mesh,    GL::Mesh>();
			
					// We need to initialise glew on non-mobile platforms.
					#ifndef KARHU_GLES
						const auto r(glewInit());
						if (GLEW_OK != r)
						{
							log::err("Karhu") << "Failed to initialise GLEW: " << glewGetErrorString(r);
							return false;
						}
					#endif
					
					/// @todo: Can/should this go here?
					/// @todo: Possible GL_SRC_ALPHA should be used when rendering directly to main framebuffer as in the old engine...
					glEnable(GL_BLEND);
					glBlendFuncSeparate
					(
						GL_SRC_ALPHA,
						GL_ONE_MINUS_SRC_ALPHA,
						GL_ONE,
						GL_ONE_MINUS_SRC_ALPHA
					);

					return true;
				}
				
				bool SubsystemGraphics::performDeinit(app::App &)
				{
					return true;
				}
				
				std::unique_ptr<gfx::Renderer> SubsystemGraphics::performCreateRenderer()
				{
					return std::make_unique<Renderer>();
				}
				
				std::unique_ptr<gfx::Mesh> SubsystemGraphics::performCreateMesh()
				{
					return std::make_unique<Mesh>();
				}
				
				std::unique_ptr<gfx::Rendertarget> SubsystemGraphics::performCreateRendertarget()
				{
					return std::make_unique<Rendertarget>();
				}
				
				std::unique_ptr<gfx::Shader> SubsystemGraphics::performCreateShader
				(
					const gfx::Shader::Type type
				)
				{
					return std::make_unique<Shader>(type);
				}

				std::unique_ptr<gfx::Texture> SubsystemGraphics::performCreateTexture
				(
					const gfx::Texture::Type       type,
					const gfx::Pixelbuffer::Format format
				)
				{
					return std::make_unique<Texture>(type, format);
				}
				
				app::FuncImGuiDevicesCreate SubsystemGraphics::funcImGuiDevicesCreate()
				{
					return imguiCreateDeviceObjects;
				}
				
				app::FuncImGuiDevicesDestroy SubsystemGraphics::funcImGuiDevicesDestroy()
				{
					return imguiInvalidateDeviceObjects;
				}
				
				app::FuncImGuiFontsCreate SubsystemGraphics::funcImGuiFontsCreate()
				{
					return imguiCreateFontsTexture;
				}
				
				app::FuncImGuiRender SubsystemGraphics::funcImGuiRender()
				{
					return imguiRenderDrawLists;
				}
				
				app::FuncIm3DInit SubsystemGraphics::funcIm3DInit()
				{
					return im3d_Init;
				}
				
				app::FuncIm3DNewFrame SubsystemGraphics::funcIm3DNewFrame()
				{
					return Im3d_NewFrame;
				}
				
				app::FuncIm3DEndFrame SubsystemGraphics::funcIm3DEndFrame()
				{
					return Im3d_EndFrame;
				}
				
				app::FuncIm3DShutdown SubsystemGraphics::funcIm3DShutdown()
				{
					return Im3d_Shutdown;
				}
			}
		}
	}
}
