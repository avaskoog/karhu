/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_GRAPHICS_GL_SETUP_H_
	#define KARHU_APP_BACKEND_GRAPHICS_GL_SETUP_H_

	#include <karhu/core/platform.hpp>

	#if defined(KARHU_PLATFORM_IOS) || defined(KARHU_PLATFORM_ANDROID) || defined(KARHU_PLATFORM_WEB)
		#define KARHU_GLES
	#endif

	#ifndef KARHU_GLES
		#include <GL/glew.h>
	#endif

	#ifdef KARHU_PLATFORM_ANDROID
		#include <GLES3/gl3.h>
		#include <GLES3/gl3ext.h>
	#endif

	#ifdef KARHU_PLATFORM_IOS
		#include <OpenGLES/ES3/gl.h>
		#include <OpenGLES/ES3/glext.h>
	#endif

	#ifdef KARHU_PLATFORM_WEB
		#include <GLES3/gl3.h>
	#endif

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace GL
				{
					static constexpr bool usingGLES() noexcept
					{
						namespace pf = platform;
						using     P  = Platform;

						return pf::is(P::iOS | P::android | P::web);
					}
				}
			}
		}
	}
#endif
