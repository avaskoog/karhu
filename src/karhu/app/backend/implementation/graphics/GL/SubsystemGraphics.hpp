/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_GRAPHICS_GL_SUBSYSTEM_GRAPHICS_H_
	#define KARHU_APP_BACKEND_GRAPHICS_GL_SUBSYSTEM_GRAPHICS_H_

	#include <karhu/app/gfx/SubsystemGraphics.hpp>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace GL
				{
					class SubsystemGraphics : public gfx::SubsystemGraphics
					{
						public:
							void logVersions() override;
							const char *name() const override { return "OpenGL"; }
						
						public:
							app::FuncImGuiDevicesCreate  funcImGuiDevicesCreate()  override;
							app::FuncImGuiDevicesDestroy funcImGuiDevicesDestroy() override;
							app::FuncImGuiFontsCreate    funcImGuiFontsCreate()    override;
							app::FuncImGuiRender         funcImGuiRender()         override;
						
							app::FuncIm3DInit     funcIm3DInit()     override;
							app::FuncIm3DNewFrame funcIm3DNewFrame() override;
							app::FuncIm3DEndFrame funcIm3DEndFrame() override;
							app::FuncIm3DShutdown funcIm3DShutdown() override;

						protected:
							bool performInit(app::App &)   override;
							bool performDeinit(app::App &) override;
						
							std::unique_ptr<gfx::Renderer>     performCreateRenderer()     override;
							std::unique_ptr<gfx::Mesh>         performCreateMesh()         override;
							std::unique_ptr<gfx::Rendertarget> performCreateRendertarget() override;
						
							std::unique_ptr<gfx::Shader> performCreateShader
							(
								const gfx::Shader::Type type
							) override;
						
							std::unique_ptr<gfx::Texture> performCreateTexture
							(
								const gfx::Texture::Type       type,
								const gfx::Pixelbuffer::Format format
							) override;
					};
				}
			}
		}
	}
#endif
