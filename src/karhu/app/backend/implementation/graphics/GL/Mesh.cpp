/**
 * @author		Ava Skoog
 * @date		2017-10-22
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/backend/implementation/graphics/GL/common.hpp>
#include <karhu/app/backend/implementation/graphics/GL/Mesh.hpp>
#include <karhu/app/backend/implementation/graphics/GL/setup.hpp>
#include <karhu/app/gfx/components/Transform.hpp>
#include <karhu/core/log.hpp>

#include <array>

namespace
{
	using namespace karhu;
	using namespace karhu::app::implementation::GL;

	/*
	 * Used to easily destroy a buffer.
	 *
	 * @param BO The ID of the buffer object.
	 */
	void destroyBuffer(std::uint32_t &BO)
	{
		if (0 != BO)
		{
			const auto ID(static_cast<GLuint>(BO));
			glDeleteBuffers(1, &ID);
			BO = 0;
		}
	}

	/*
	 * Used to easily set up a specific vertex attribute buffer.
	 *
	 * @param name  The name of the buffer for error messages.
	 * @param BO    The ID of the buffer object.
	 * @param index The attribute index.
	 * @param count The number of elements per attribute.
	 * @param size  The size in bytes of the buffer holding the data.
	 * @param data  The pointer to the data.
	 */
	/// @todo: Maybe we could automate this a bit more using some templates and/or macros?
	bool bakeBuffer
	(
		const std::string          &name,
			  std::uint32_t        &BO,
		const IndexAttributeVertex &index,
		const std::size_t          &count,
		const std::size_t          &size,
		const void                 *data
	)
	{
		clearErrorsGL();
		GLenum err;
		
		// Generate the buffer if it does not already exist.
		if (0 == BO)
		{
			GLuint ID{0};
			glGenBuffers(1, &ID);
			if (GL_NO_ERROR == (err = glGetError()))
				BO = ID;
			else
			{
				log::err("Karhu")
					<< "Failed to generate "
					<< name
					<< " for mesh: "
					<< errorToStringGL(err);
				return false;
			}
		}

		// Skip this if there is no initial data.
		if (size > 0 && data)
		{
			// Bind the BO.
			glBindBuffer(GL_ARRAY_BUFFER, BO);
			if (GL_NO_ERROR != (err = glGetError()))
			{
				log::err("Karhu")
					<< "Failed to bind "
					<< name
					<< ' '
					<< BO
					<< " for mesh: "
					<< errorToStringGL(err);
				return false;
			}

			// Set the data.
			glBufferData
			(
				GL_ARRAY_BUFFER,
				static_cast<GLsizeiptr>(size),
				data,
				GL_STATIC_DRAW//GL_DYNAMIC_DRAW /// @todo: need to check static/dynamic
			);
			if (GL_NO_ERROR != (err = glGetError()))
			{
				log::err("Karhu")
					<< "Failed to set "
					<< name
					<< ' '
					<< BO
					<< " data for mesh: "
					<< errorToStringGL(err);
				return false;
			}
			
			// Enable attribute.
			glEnableVertexAttribArray(static_cast<GLuint>(index));
			if (GL_NO_ERROR != (err = glGetError()))
			{
				log::err("Karhu")
					<< "Failed to enable vertex attribute "
					<< static_cast<int>(index)
					<< " for mesh: "
					<< errorToStringGL(err);
				return false;
			}

			// Point the data to the attribute.
			/// @todo: Figure out how to use glVertexAttribIPointer in WebGL 2.0...
//			if (!integer)
				glVertexAttribPointer
				(
					static_cast<GLuint>(index),
					static_cast<GLint>(count),
					GL_FLOAT,
					GL_FALSE,
					0,
					nullptr
				);
//			else
//				glVertexAttribIPointer(index, static_cast<GLint>(count), GL_UNSIGNED_SHORT, 0, nullptr);

			if (GL_NO_ERROR != (err = glGetError()))
			{
				log::err("Karhu")
					<< "Failed to set vertex attribute pointer "
					<< static_cast<int>(index)
					<< " for mesh: "
					<< errorToStringGL(err);
				return false;
			}
		}
		
		// Done.
//		glDisableVertexAttribArray(static_cast<GLuint>(index));
		
		return true;
	}
	
	/*
	 * Used to easily enable individual vertex arrays.
	 *
	 * @param attrAll      List of bitflags containing the attributes to check.
	 * @param attrToEnable Attribute to enable if it is found in the list.
	 * @param buf          The vector containing the attribute data.
	 * @param BO           The index of the buffer object to enable.
	 */
	template<class TBuffer>
	void enableAttribute
	(
		const gfx::Geometry::Attributes attrAll,
		const gfx::Geometry::Attribute &attrToEnable,
		const TBuffer &buf,
		const IndexAttributeVertex &BO
	)
	{
		if ((attrToEnable & attrAll) == attrToEnable && buf.size() > 0)
			glEnableVertexAttribArray(static_cast<GLuint>(BO));
	}
}

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace GL
			{
				bool Mesh::performBake()
				{
					// Get the buffer because we will be using it a bunch.
					const auto &b(buffer());

					// Flush the error buffer.
					glGetError();

					// Errors along the way will be stored here.
					GLenum err;
					
					/*
					 * The VAO contains all the buffer objects.
					 */

					// If VAO has not yet been generated, do so.
					if (0 == m_VAO)
					{
						GLuint ID{0};
						glGenVertexArrays(1, &ID);
						if (GL_NO_ERROR == (err = glGetError()))
							m_VAO = ID;
						else
						{
							log::err("Karhu")
								<< "Failed to generate VAO for mesh: "
								<< errorToStringGL(err);
							return false;
						}
					}
					
					// Bind the VAO.
					glBindVertexArray(m_VAO);
					if (GL_NO_ERROR != (err = glGetError()))
					{
						log::err("Karhu")
							<< "Failed to bind VAO "
							<< m_VAO
							<< " for mesh: "
							<< errorToStringGL(err);
						return false;
					}
					
					// The bufMatsModelInstanced contains the model matrix for each instance during instanced rendering
					// and is only filled in when necessary (when bound for multiple instances drawn).
					// We need to initialise with a dummy pointer or else glVertexAttribPointer() will fail.
					{
						mth::Mat4f dummy{};
						
						// The function used to bake buffers here is a hidden global at the top of this file.
						if (!bakeBuffer
						(
							"instanced model matrix buffer",
							m_bufMatsModelInstanced,
							IndexAttributeVertex::matricesModelInstanced,
							4,
							sizeof(mth::Mat4f), &dummy[0][0]
						))
							return false;
					}
					
					// A matrix is 4x4 but attributes can only be 4 so we need to use 4 attributes.
					for (GLuint i{0}; i < 4; ++ i)
					{
						const auto index(static_cast<GLuint>(IndexAttributeVertex::matricesModelInstanced) + i);
						glVertexAttribPointer
						(
							index,
							4,
							GL_FLOAT,
							GL_FALSE,
							sizeof(mth::Mat4f),
							reinterpret_cast<const GLvoid *>(sizeof(GLfloat) * i * 4)
						);
						if (GL_NO_ERROR != (err = glGetError()))
						{
							log::err("Karhu")
								<< "Failed to set vertex attribute pointer "
								<< index
								<< " for mesh: "
								<< errorToStringGL(err);
							return false;
						}
						
						// Make it instanced.
						glVertexAttribDivisor(index, 1);
					}
					
					// The function used to bake buffers here is a hidden global at the top of this file.
					
					// Vertex position attribute.
					if (b.geometry.positions.size() > 0)
						if (!bakeBuffer
						(
							"vertex position attribute buffer",
							m_bufPositions,
							IndexAttributeVertex::position,
							3,
							sizeof(mth::Vec3f) * b.geometry.positions.size(),
							&b.geometry.positions[0]
						))
							return false;
					
					// Vertex normal attribute.
					if (b.geometry.normals.size() > 0)
						if (!bakeBuffer
						(
							"vertex normal attribute buffer",
							m_bufNormals,
							IndexAttributeVertex::normal,
							3,
							sizeof(mth::Vec3f) * b.geometry.normals.size(),
							&b.geometry.normals[0]
						))
							return false;
					
					// Vertex texture coördinate (UV) attribute.
					if (b.geometry.UVs.size() > 0)
						if (!bakeBuffer
						(
							"vertex UV attribute buffer",
							m_bufUVs,
							IndexAttributeVertex::UV,
							2,
							sizeof(mth::Vec2f) * b.geometry.UVs.size(),
							&b.geometry.UVs[0]
						))
							return false;
					
					// Vertex colour attribute.
					if (b.geometry.colours.size() > 0)
						if (!bakeBuffer
						(
							"vertex colour attribute buffer",
							m_bufColours,
							IndexAttributeVertex::colour,
							4,
							sizeof(gfx::ColRGBAf) * b.geometry.colours.size(),
							&b.geometry.colours[0]
						))
							return false;
					
					// Vertex jointset attribute.
					if (b.geometry.jointsets0.size() > 0)
						if (!bakeBuffer
						(
							"vertex jointset 0 attribute buffer",
							m_bufJointsets0,
							IndexAttributeVertex::jointset0,
							4,
							sizeof(gfx::Jointset) * b.geometry.jointsets0.size(),
							&b.geometry.jointsets0[0]
						))
							return false;
					
					// Vertex weightset attribute.
					if (b.geometry.weightsets0.size() > 0)
						if (!bakeBuffer
						(
							"vertex weightset 0 attribute buffer",
							m_bufWeightsets0,
							IndexAttributeVertex::weightset0,
							4,
							sizeof(gfx::Weightset) * b.geometry.weightsets0.size(),
							&b.geometry.weightsets0[0]
						))
							return false;
					
					// Vertex jointset attribute.
					if (b.geometry.jointsets1.size() > 0)
						if (!bakeBuffer
						(
							"vertex jointset 1 attribute buffer",
							m_bufJointsets1,
							IndexAttributeVertex::jointset1,
							4,
							sizeof(gfx::Jointset) * b.geometry.jointsets1.size(),
							&b.geometry.jointsets1[0]
						))
							return false;
					
					// Vertex weightset attribute.
					if (b.geometry.weightsets1.size() > 0)
						if (!bakeBuffer
						(
							"vertex weightset 1 attribute buffer",
							m_bufWeightsets1,
							IndexAttributeVertex::weightset1,
							4,
							sizeof(gfx::Weightset) * b.geometry.weightsets1.size(),
							&b.geometry.weightsets1[0]
						))
							return false;
					
					// Vertex jointset attribute.
					if (b.geometry.jointsets2.size() > 0)
						if (!bakeBuffer
						(
							"vertex jointset 2 attribute buffer",
							m_bufJointsets2,
							IndexAttributeVertex::jointset2,
							4,
							sizeof(gfx::Jointset) * b.geometry.jointsets2.size(),
							&b.geometry.jointsets2[0]
						))
							return false;
					
					// Vertex weightset attribute.
					if (b.geometry.weightsets2.size() > 0)
						if (!bakeBuffer
						(
							"vertex weightset 2 attribute buffer",
							m_bufWeightsets2,
							IndexAttributeVertex::weightset2,
							4,
							sizeof(gfx::Weightset) * b.geometry.weightsets2.size(),
							&b.geometry.weightsets2[0]
						))
							return false;
					
					// Tangent attribute.
					if (b.geometry.tangents.size() > 0)
						if (!bakeBuffer
						(
							"vertex tangent attribute buffer",
							m_bufTangents,
							IndexAttributeVertex::tangent,
							4,
							sizeof(mth::Vec4f) * b.geometry.tangents.size(),
							&b.geometry.tangents[0]
						))
							return false;
					
					// Custom attribute.
					if (b.geometry.custom.size() > 0)
						if (!bakeBuffer
						(
							"vertex custom attribute buffer",
							m_bufCustom,
							IndexAttributeVertex::custom,
							4,
							sizeof(mth::Vec4f) * b.geometry.custom.size(),
							&b.geometry.custom[0]
						))
							return false;
					
					/*
					 * Index buffer.
					 */
					
					if (b.geometry.indices.size() > 0)
					{
						// If buffer has not yet been generated, do so.
						if (0 == m_bufIndices)
						{
							GLuint ID{0};
							glGenBuffers(1, &ID);
							if (GL_NO_ERROR == (err = glGetError()))
								m_bufIndices = ID;
							else
							{
								log::err("Karhu")
									<< "Failed to generate index buffer for mesh: "
									<< errorToStringGL(err);
								return false;
							}
						}
						
						// Bind the buffer.
						glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufIndices);
						if (GL_NO_ERROR != (err = glGetError()))
						{
							log::err("Karhu")
								<< "Failed to bind indexbuffer "
								<< m_bufIndices
								<< " for mesh: "
								<< errorToStringGL(err);
							return false;
						}
						
						// Set the index data.
						glBufferData
						(
							GL_ELEMENT_ARRAY_BUFFER,
							static_cast<GLsizei>(sizeof(gfx::Index) * b.geometry.indices.size()),
							&b.geometry.indices[0],
							GL_STATIC_DRAW//GL_DYNAMIC_DRAW /// @todo: need to check static/dynamic
						);
						if (GL_NO_ERROR != (err = glGetError()))
						{
							log::err("Karhu")
								<< "Failed to set index buffer data for mesh: "
								<< errorToStringGL(err);
							return false;
						}
					}

					return true;
				}
				
				void Mesh::performReset()
				{
					clean();
				}
				
				bool Mesh::performBindForSingle
				(
					const gfx::Geometry::Attributes attributes
				)
				{
					// Bind the VAO.
					if (!bindVAO()) return false;
					
					// Enable the vertex attributes.
					//enableAttributes(attributes);

					return true;
				}
				
				bool Mesh::performBindForMultiple
				(
					const std::vector<gfx::Instance> &instances,
					const gfx::Geometry::Attributes attributes
				)
				{
					if (!bindVAO()) return false;

					// Enable the vertex attributes.

					for (GLuint i{0}; i < 4; ++ i)
					{
						const auto index(static_cast<GLuint>(IndexAttributeVertex::matricesModelInstanced) + i);
						glEnableVertexAttribArray(index);
					}

					enableAttributes(attributes);
					
					clearErrorsGL();
					GLenum err;
					
					// Bind the bufMatsModelInstanced so that we can fill it in.
					glBindBuffer(GL_ARRAY_BUFFER, m_bufMatsModelInstanced);
					if (GL_NO_ERROR != (err = glGetError()))
					{
						log::err("Karhu")
							<< "Failed to bind model matrix buffer "
							<< m_bufMatsModelInstanced
							<< " for mesh: "
							<< errorToStringGL(err);
						return false;
					}
					
					// Collect all the matrices in an array.
					std::vector<mth::Mat4f> mats{instances.size()};
					{
						std::size_t i{0};
						for (const auto &c : instances)
						{
							mats[i] = c.transform;
							++ i;
						}
					}

					// Pass the data to the buffer.
					glBufferData
					(
						GL_ARRAY_BUFFER,
						static_cast<GLsizeiptr>(sizeof(mth::Mat4f) * instances.size()),
						&mats[0],
						GL_DYNAMIC_DRAW /// @todo: need to check static/dynamic
					);
					if (GL_NO_ERROR != (err = glGetError()))
					{
						log::err("Karhu")
							<< "Failed to set model matrix buffer "
							<< m_bufMatsModelInstanced
							<< " buffer data for mesh: "
							<< errorToStringGL(err);
						return false;
					}

					return true;
				}
				
				bool Mesh::bindVAO()
				{
					clearErrorsGL();
					GLenum err;

					glBindVertexArray(m_VAO);
					if (GL_NO_ERROR != (err = glGetError()))
					{
						log::err("Karhu")
							<< "Failed to bind VAO "
							<< m_VAO
							<< " for mesh: "
							<< errorToStringGL(err);
						return false;
					}
					
					return true;
				}
				
				void Mesh::enableAttributes(const gfx::Geometry::Attributes attributes)
				{/*
					using A = gfx::Geometry::Attribute;

					const auto &b(buffer());
					
					// The function used here is a hidden global at the top of this file.
					enableAttribute(attributes, A::position,   b.geometry.positions,   IndexAttributeVertex::position);
					enableAttribute(attributes, A::normal,     b.geometry.normals,     IndexAttributeVertex::normal);
					enableAttribute(attributes, A::UV,         b.geometry.UVs,         IndexAttributeVertex::UV);
					enableAttribute(attributes, A::colour,     b.geometry.colours,     IndexAttributeVertex::colour);
					enableAttribute(attributes, A::jointset0,  b.geometry.jointsets0,  IndexAttributeVertex::jointset0);
					enableAttribute(attributes, A::weightset0, b.geometry.weightsets0, IndexAttributeVertex::weightset0);
					enableAttribute(attributes, A::jointset1,  b.geometry.jointsets1,  IndexAttributeVertex::jointset1);
					enableAttribute(attributes, A::weightset1, b.geometry.weightsets1, IndexAttributeVertex::weightset1);
					enableAttribute(attributes, A::jointset2,  b.geometry.jointsets2,  IndexAttributeVertex::jointset2);
					enableAttribute(attributes, A::weightset2, b.geometry.weightsets2, IndexAttributeVertex::weightset2);
					enableAttribute(attributes, A::tangent,  b.geometry.tangents,  IndexAttributeVertex::tangent);
					enableAttribute(attributes, A::custom, b.geometry.custom, IndexAttributeVertex::custom);
			*/	}
				
				void Mesh::clean()
				{
					if (0 != m_VAO)
					{
						glDeleteVertexArrays(1, &m_VAO);
						m_VAO = 0;
					}
					
					destroyBuffer(m_bufPositions);
					destroyBuffer(m_bufNormals);
					destroyBuffer(m_bufUVs);
					destroyBuffer(m_bufJointsets0);
					destroyBuffer(m_bufWeightsets0);
					destroyBuffer(m_bufJointsets1);
					destroyBuffer(m_bufWeightsets1);
					destroyBuffer(m_bufJointsets2);
					destroyBuffer(m_bufWeightsets2);
					destroyBuffer(m_bufTangents);
					destroyBuffer(m_bufCustom);
					destroyBuffer(m_bufColours);
					
					destroyBuffer(m_bufIndices);
				}

				Mesh::~Mesh()
				{
					clean();
				}
			}
		}
	}
}
