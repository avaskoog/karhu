/**
 * @author		Ava Skoog
 * @date		2017-10-22
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_GRAPHICS_GL_MESH_H_
	#define KARHU_APP_BACKEND_GRAPHICS_GL_MESH_H_

	#include <karhu/app/gfx/resources/Mesh.hpp>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace GL
				{
					class Mesh : public gfx::Mesh
					{
						public:
							~Mesh();

						protected:
							bool performBake() override;
							void performReset() override;

							bool performBindForSingle(const gfx::Geometry::Attributes) override;

							bool performBindForMultiple
							(
								const std::vector<gfx::Instance> &,
								const gfx::Geometry::Attributes attributes
							) override;
						
						private:
							bool bindVAO();
							void enableAttributes(const gfx::Geometry::Attributes );
							void clean();

						private:
							std::uint32_t
								m_VAO                  {0},
								m_bufMatsModelInstanced{0},
								m_bufPositions         {0},
								m_bufNormals           {0},
								m_bufUVs               {0},
								m_bufColours           {0},
								m_bufJointsets0        {0},
								m_bufWeightsets0       {0},
								m_bufJointsets1        {0},
								m_bufWeightsets1       {0},
								m_bufJointsets2        {0},
								m_bufWeightsets2       {0},
								m_bufTangents          {0},
								m_bufCustom            {0},
								m_bufIndices           {0};
					};
				}
			}
		}
	}
#endif
