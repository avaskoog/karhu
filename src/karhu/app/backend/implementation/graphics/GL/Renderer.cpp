/**
 * @author		Ava Skoog
 * @date		2017-10-22
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: There is too much stuff in here. The general layout of this needs to be moved into the platform-agnostic base interface so that all different graphics backends can implement stuff without forgetting to do anything important.

#include <karhu/app/backend/implementation/graphics/GL/setup.hpp>
#include <karhu/app/backend/implementation/graphics/GL/common.hpp>
#include <karhu/app/backend/implementation/graphics/GL/Renderer.hpp>
#include <karhu/app/gfx/components/Camera.hpp>
#include <karhu/app/gfx/resources/Mesh.hpp>
#include <karhu/app/gfx/resources/Shader.hpp>
#include <karhu/app/gfx/resources/Texture.hpp>
#include <karhu/app/gfx/resources/Material.hpp>
#include <karhu/core/log.hpp>

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace GL
			{
				Renderer::Renderer()
				{
					// The default framebuffer might not be 0.
					{
						GLint v{-1};
						glGetIntegerv(GL_FRAMEBUFFER_BINDING, &v);
						if (v >= 0)
						{
							log::dbg("Karhu") << "Default OpenGL framebuffer has ID " << v;
							c_FBDefault = static_cast<std::uint32_t>(v);
						}
					}
					
					// The default renderbuffer might not be 0.
					{
						GLint v{-1};
						glGetIntegerv(GL_RENDERBUFFER_BINDING, &v);
						if (v >= 0)
						{
							log::dbg("Karhu") << "Default OpenGL renderbuffer has ID " << v;
							c_RBDefault = static_cast<std::uint32_t>(v);
						}
					}
					
					// Generate built in default texture.
					{
						GLuint ID;
						glGenTextures(1, &ID);
						m_texDefault = static_cast<std::uint32_t>(ID);
						glBindTexture(GL_TEXTURE_2D, ID);
						std::uint8_t pixels[]{0xff, 0xff, 0xff, 0xff};
						glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, &pixels);
						glBindTexture(GL_TEXTURE_2D, 0);
					}
					
					// Set up the default fullscreen quad shader.
					{
						constexpr auto vert
						{R"(
							layout(location = 0) in vec4 _vpos;

							void main()
							{
								gl_Position = _vpos;
							}
						)"};
							
						// The fragment shader draws the texture.
						/// @todo: Get uniform names from constants.
						constexpr auto frag
						{R"(
							uniform sampler2D _texMain, _texComposite;
							uniform vec2 _sizeViewport;
							uniform bool _karhuGL_useComposite;

							layout(location = 0) out vec4 col;
							
							void main()
							{
								col = texture(_texMain, gl_FragCoord.xy / _sizeViewport.xy);
								
								/// @todo: Should composite be a separate shader entirely?
								if (_karhuGL_useComposite)
								{
									vec4 colComposite = texture(_texComposite, gl_FragCoord.xy / _sizeViewport.xy);
									col.rgb = col.rgb * (1.0 - colComposite.a) + colComposite.rgb * colComposite.a;
									col.a = col.a + colComposite.a * (1.0 - col.a);
								}
							}
						)"};

						// Apply everything.
						m_shaderQuadFullscreen.addSourceGLSL(Shader::Mode::single, Shader::TypeGLSL::vert, vert);
						m_shaderQuadFullscreen.addSourceGLSL(Shader::Mode::single, Shader::TypeGLSL::frag, frag);

						// Try and bake it.
						if (!m_shaderQuadFullscreen.bake())
						{
							log::err("Karhu") << "Failed to generate default fullscreen quad shader for renderer";
							return;
						}
						
						m_uniformUseComposite = glGetUniformLocation
						(
							m_shaderQuadFullscreen.programGLSL(res::Shader::Mode::single),
							"_karhuGL_useComposite"
						);
						
						if (m_uniformUseComposite < 0)
						{
							log::err("Karhu") << "Failed to generate default composite quad shader for renderer";
							return;
						}
					}
					
					// Set up the default one by one quad shader.
					{
						constexpr auto vert
						{R"(
							/// @todo: Can we leverage the normal generation code to get the same locations without code reduplication? Or do sprites need to have their shader set up manually, just like meshes?
							layout(location = 0) in vec4 _vpos;
							/// @todo: Also UV?
							
							uniform mat4 _view;
							uniform mat4 _proj;
							uniform mat4 _model;
							
							uniform vec4 _tint;
							uniform vec2 _karhuGL_UVs[2];
							
							out vec4 _fcol;
							out vec2 _fUV;

							void main()
							{
								gl_Position = _proj * _view * _model * _vpos;
								_fcol = _tint;
								_fUV.x = _karhuGL_UVs[int(round(_vpos.x))].x;
								_fUV.y = _karhuGL_UVs[int(round(_vpos.y))].y;
							}
						)"};
						
						constexpr auto vertMultiple
						{R"(
							/// @todo: Can we leverage the normal generation code to get the same locations without code reduplication? Or do sprites need to have their shader set up manually, just like meshes?
							layout(location = 0) in vec4 _vpos;
							layout(location = 1) in mat4 _model;
							/// @todo: Also UV?
							
							uniform mat4 _view;
							uniform mat4 _proj;
							
							uniform vec4 _tint;
							uniform vec2 _karhuGL_UVs[2];
							
							out vec4 _fcol;
							out vec2 _fUV;

							void main()
							{
								gl_Position = _proj * _view * _model * _vpos;
								_fcol = _tint;
								_fUV.x = _karhuGL_UVs[int(round(_vpos.x))].x;
								_fUV.y = _karhuGL_UVs[int(round(_vpos.y))].y;
							}
						)"};
							
						// The fragment shader draws the texture.
						/// @todo: Get uniform names from constants.
						/// @todo: This needs to be moved into user-controlled shader since it needs access to custom outputs.
						constexpr auto frag
						{R"(
							in vec4 _fcol;
							in vec2 _fUV;
							
							uniform sampler2D _texMain;

							layout(location = 0) out vec4 _col;
							layout(location = 1) out vec4 _colNormal;
							layout(location = 2) out vec4 _colLight; // r = skymask, g = lightmask/emission, b = specular
							layout(location = 3) out vec4 _colMask;
							layout(location = 4) out vec4 _texDepth2;
							
							const vec3 bitEnc = vec3(1.,255.,65025.);
							vec3 EncodeFloatRGB (float v) {
								vec3 enc = bitEnc * v;
								enc = fract(enc);
								enc -= enc.yzz * vec2(1./255., 0.).xxy;
								return enc;
							}
							
							void main()
							{
								/// @todo: This is problematic, but will be fixed when sprites are no longer drawn with built-in shaders...
								_col = texture(_texMain, vec2(_fUV.x, 1.0 - _fUV.y)) * _fcol;
								_colNormal = vec4(0.0, 0.0, 0.0, _col.a);
								_colLight = vec4(0.0, 0.0, 0.0, _col.a);
								_colMask = vec4(1.0, 1.0, 1.0, _col.a);
								_texDepth2 = vec4(EncodeFloatRGB(gl_FragCoord.z), _col.a);
							}
						)"};

						// Apply everything.
						m_shaderQuadOneByOne.addSourceGLSL(Shader::Mode::single, Shader::TypeGLSL::vert, vert);
						m_shaderQuadOneByOne.addSourceGLSL(Shader::Mode::single, Shader::TypeGLSL::frag, frag);
						m_shaderQuadOneByOne.addSourceGLSL(Shader::Mode::multiple, Shader::TypeGLSL::vert, vertMultiple);
						m_shaderQuadOneByOne.addSourceGLSL(Shader::Mode::multiple, Shader::TypeGLSL::frag, frag);

						// Try and bake it.
						if (!m_shaderQuadOneByOne.bake())
						{
							log::err("Karhu") << "Failed to generate default one by one quad shader for renderer";
							return;
						}
						
						m_uniformUV0 = glGetUniformLocation
						(
							m_shaderQuadOneByOne.programGLSL(res::Shader::Mode::single),
							"_karhuGL_UVs[0]"
						);
						
						m_uniformUV1 = glGetUniformLocation
						(
							m_shaderQuadOneByOne.programGLSL(res::Shader::Mode::single),
							"_karhuGL_UVs[1]"
						);
						
						if
						(
							m_uniformUV0  < 0 ||
							m_uniformUV1  < 0
						)
						{
							log::err("Karhu") << "Failed to generate default one by one quad shader for renderer";
							return;
						}
						
						m_uniformUV0Multiple = glGetUniformLocation
						(
							m_shaderQuadOneByOne.programGLSL(res::Shader::Mode::multiple),
							"_karhuGL_UVs[0]"
						);
						
						m_uniformUV1Multiple = glGetUniformLocation
						(
							m_shaderQuadOneByOne.programGLSL(res::Shader::Mode::multiple),
							"_karhuGL_UVs[1]"
						);
						
						if
						(
							m_uniformUV0Multiple  < 0 ||
							m_uniformUV1Multiple  < 0
						)
						{
							log::err("Karhu") << "Failed to generate default one by one quad multiple shader for renderer";
							return;
						}
					}
				}
				
				void Renderer::performClear(const gfx::ColRGBAf &colour)
				{
					/// @todo: Should this accept arguments?
					glClearColor(colour.r, colour.g, colour.b, colour.a);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
				}
				
				void Renderer::performSetDepthmask(const bool set) const
				{
					glDepthMask((set) ? GL_TRUE : GL_FALSE);
				}
				
				void Renderer::performSetConditionDepth
				(
					const gfx::ConditionDepth condition
				) const
				{
					using C = gfx::ConditionDepth;
					
					switch (condition)
					{
						case C::always: glEnable (GL_DEPTH_TEST); break;
						case C::never:  glDisable(GL_DEPTH_TEST); break;
					}
				}
				
				void Renderer::performSetCulling(const gfx::Culling culling) const
				{
					switch (culling)
					{
						case gfx::Culling::none:
							glDisable(GL_CULL_FACE);
							break;
					
						case gfx::Culling::back:
							glEnable(GL_CULL_FACE);
							glCullFace(GL_BACK);
							break;

						case gfx::Culling::front:
							glEnable(GL_CULL_FACE);
							glCullFace(GL_FRONT);
							break;

						case gfx::Culling::both:
							glEnable(GL_CULL_FACE);
							glCullFace(GL_FRONT_AND_BACK);
							break;
					}
				}
				
				void Renderer::performResetTexturesamplers() const
				{
					for (GLenum i{GL_TEXTURE0}; i < GL_TEXTURE16; ++ i)
					{
						glActiveTexture(i);
						glBindTexture
						(
							GL_TEXTURE_2D,
							static_cast<GLuint>(m_texDefault)
						);
					}
				}
					
				void Renderer::performDrawQuadOneByOne
				(
					const float UVs[4]
				) const
				{
					glUniform2f(m_uniformUV0, UVs[0], UVs[1]);
					glUniform2f(m_uniformUV1, UVs[2], UVs[3]);
					
					/// @todo: Needed?
					glEnable(GL_BLEND);
					glBlendFuncSeparate
					(
						GL_SRC_ALPHA,
						GL_ONE_MINUS_SRC_ALPHA,
						GL_ONE,
						GL_ONE_MINUS_SRC_ALPHA
					);
					
					m_quadrendererOneByOne.render();
				}
					
				void Renderer::performDrawQuadsOneByOne
				(
					const std::vector<gfx::Instance> &instances,
					const float UVs[4]
				) const
				{
					glUniform2f(m_uniformUV0Multiple, UVs[0], UVs[1]);
					glUniform2f(m_uniformUV1Multiple, UVs[2], UVs[3]);
					
					/// @todo: Needed?
					glEnable(GL_BLEND);
					glBlendFuncSeparate
					(
						GL_SRC_ALPHA,
						GL_ONE_MINUS_SRC_ALPHA,
						GL_ONE,
						GL_ONE_MINUS_SRC_ALPHA
					);
					
					m_quadrendererOneByOne.renderMultiple(instances);
				}
				
				void Renderer::performDrawSingleWithoutIndices
				(
					const res::Mesh::Buffer &buffer
				) const
				{
					glDrawArrays
					(
						GL_TRIANGLES,
						0,
						static_cast<GLsizei>(buffer.geometry.positions.size())
					);
				}
			
				void Renderer::performDrawSingleWithIndices
				(
					const res::Mesh::Buffer        &,
					const res::Mesh::Buffer::Range &range
				) const
				{
					glDrawElements
					(
						GL_TRIANGLES,
						static_cast<GLsizei>(range.count),
						GL_UNSIGNED_INT,
						reinterpret_cast<GLvoid *>(range.offset * sizeof(gfx::Index))
					);
				}
			
				void Renderer::performDrawMultipleWithoutIndices
				(
					const res::Mesh::Buffer &buffer,
					const std::size_t       &count
				) const
				{
					glDrawArraysInstanced
					(
						GL_TRIANGLES,
						0,
						static_cast<GLsizei>(buffer.geometry.positions.size()),
						static_cast<GLsizei>(count)
					);
				}
			
				void Renderer::performDrawMultipleWithIndices
				(
					const res::Mesh::Buffer        &,
					const res::Mesh::Buffer::Range &range,
					const std::size_t              &count
				) const
				{
					glDrawElementsInstanced
					(
						GL_TRIANGLES,
						static_cast<GLsizei>(range.count),
						GL_UNSIGNED_INT,
						reinterpret_cast<GLvoid *>(range.offset * sizeof(gfx::Index)),
						static_cast<GLsizei>(count)
					);
				}

				void Renderer::performRenderQuadFullscreen
				(
					const gfx::Pass &pass,
					const gfx::ContainerInputs &inputs,
					const std::uint32_t &widthWindow,
					const std::uint32_t &heightWindow,
					const float &scaleWindowX,
					const float &scaleWindowY,
					res::Material &material,
					res::Shader *const shader
				) const
				{
					// Use the default shader if none was provided.
					const res::Shader *const s
					{
						(shader) ? shader : &m_shaderQuadFullscreen
					};
					
					s->bindForSingle
					(
						pass,
						gfx::Defaultlayers::standard,
						&material,
						widthWindow,
						heightWindow,
						scaleWindowX,
						scaleWindowY
					);
					
					s->bindInputsForSingle(inputs);
					
					s->bindInputsForSingle(material.inputs);
					
					if (!shader)
						glUniform1i(m_uniformUseComposite, 0);
					
					// Render the quad!
					m_quadrendererFullscreen.render();
				}

				void Renderer::performCompositeQuadsFullscreen
				(
					const gfx::Pass &pass,
					const gfx::ContainerInputs &inputs,
					const std::uint32_t &widthWindow,
					const std::uint32_t &heightWindow,
					const float &scaleWindowX,
					const float &scaleWindowY,
					res::Material &material,
					res::Shader *const shader
				) const
				{
					// Use the default shader if none was provided.
					const res::Shader *const s
					{
						(shader) ? shader : &m_shaderQuadFullscreen
					};
					
					s->bindForSingle
					(
						pass,
						gfx::Defaultlayers::standard,
						&material,
						widthWindow,
						heightWindow,
						scaleWindowX,
						scaleWindowY
					);
					
					s->bindInputsForSingle(inputs);
					
					s->bindInputsForSingle(material.inputs);
					
					if (!shader)
						glUniform1i(m_uniformUseComposite, 1);
					
					// Render the quad!
					m_quadrendererFullscreen.render();
				}
				
				bool Renderer::performBindCamera(gfx::Camera &camera)
				{
					// Set viewport to match the buffer size.
					glViewport
					(
						0,
						0,
						static_cast<GLsizei>(camera.viewport().x),
						static_cast<GLsizei>(camera.viewport().y)
					);
					
					return true;
				}
				
				void Renderer::performClearCamera(gfx::Camera &camera)
				{
					glClearColor
					(
						camera.clearcolour().r,
						camera.clearcolour().g,
						camera.clearcolour().b,
						camera.clearcolour().a
					);
					
					glStencilMask(0xff);
					glClearStencil(static_cast<GLint>(camera.clearstencil()));

					using CF = gfx::Clearflag;

					if (CF::none != camera.clearflags())
					{
						GLenum f{0};

						if (static_cast<bool>(CF::colour & camera.clearflags()))
							f |= GL_COLOR_BUFFER_BIT;
						
						if (static_cast<bool>(CF::depth & camera.clearflags()))
							f |= GL_DEPTH_BUFFER_BIT;
						
						if (static_cast<bool>(CF::stencil & camera.clearflags()))
							f |= GL_STENCIL_BUFFER_BIT;

						glClear(f);
					}
				}
				
				Renderer::Quadrenderer::Quadrenderer(const Mode mode)
				:
				m_mode{mode}
				{
					clearErrorsGL();
					GLenum err;
					
					// Generate the buffer.
					{
						GLuint ID{0};
						glGenVertexArrays(1, &ID);
						if (0 == (m_VAO = ID))
						{
							log::err("Karhu") << "Failed to generate VAO for fullscreen triangle";
							return;
						}
					}

					glGetError();
					glBindVertexArray(m_VAO);
					if (GL_NO_ERROR != (err = glGetError()))
					{
						log::err("Karhu") << "Failed to bind VAO " << m_VAO << " for fullscreen triangle" << errorToStringGL(err);
						return;
					}
					
					{
						GLuint ID{0};
						glGenBuffers(1, &ID);
						if (0 == (m_VBO = ID))
						{
							log::err("Karhu") << "Failed to generate VBO for fullscreen triangle";
							return;
						}
					}
					
					glGetError();
					glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
					if (GL_NO_ERROR != (err = glGetError()))
					{
						log::err("Karhu") << "Failed to bind VBO " << m_VBO << " for fullscreen triangle: " << errorToStringGL(err);
						return;
					}

					// Triangle data.
					switch (mode)
					{
						case Mode::fullscreen:
						{
							const GLfloat vertices[]
							{
								-1.0f, -1.0f,
								 3.0f, -1.0f,
								-1.0f,  3.0f
							};
							
							glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices[0], GL_STATIC_DRAW);
							if (GL_NO_ERROR != (err = glGetError()))
							{
								log::err("Karhu") << "Failed to set buffer data for fullscreen triangle: " << errorToStringGL(err);
								return;
							}
							
							break;
						}
						
						case Mode::oneByOne:
						{
							const GLfloat vertices[]
							{
								0.0f, 0.0f,
								1.0f, 0.0f,
								0.0f, 1.0f,
								0.0f, 1.0f,
								1.0f, 0.0f,
								1.0f, 1.0f
							};
							
							glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices[0], GL_DYNAMIC_DRAW); /// @todo: Or static draw?
							if (GL_NO_ERROR != (err = glGetError()))
							{
								log::err("Karhu") << "Failed to set buffer data for one by one triangle: " << errorToStringGL(err);
								return;
							}
							
							break;
						}
					}
					
					// Enable the attribute for position.
					const auto ip(static_cast<GLuint>(0));
					glEnableVertexAttribArray(ip);
					glGetError();
					glVertexAttribPointer(ip, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
					if (GL_NO_ERROR != (err = glGetError()))
					{
						log::err("Karhu") << "Failed to set vertex attribute pointer 0 for fullscreen triangle: " << errorToStringGL(err);
						return;
					}
					
					if (Mode::oneByOne == mode)
					{
						{
							GLuint ID{0};
							glGenBuffers(1, &ID);
							if (0 == (m_MBO = ID))
							{
								log::err("Karhu") << "Failed to generate MBO for fullscreen triangle";
								return;
							}
						}
						glGetError();
						glBindBuffer(GL_ARRAY_BUFFER, m_MBO);
						if (GL_NO_ERROR != (err = glGetError()))
						{
							log::err("Karhu") << "Failed to bind MBO " << m_MBO << " for fullscreen triangle: " << errorToStringGL(err);
							return;
						}
						
						// The bufMatsModelInstanced contains the model matrix for each instance during instanced rendering
						// and is only filled in when necessary (when bound for multiple instances drawn).
						// We need to initialise with a dummy pointer or else glVertexAttribPointer() will fail.
						
						mth::Mat4f dummy{};
						
						glBufferData
						(
							GL_ARRAY_BUFFER,
							static_cast<GLsizeiptr>(sizeof(mth::Mat4f)),
							&dummy[0][0],
							GL_DYNAMIC_DRAW//GL_STATIC_DRAW /// @todo: need to check static/dynamic
						);
						
						if (GL_NO_ERROR != (err = glGetError()))
						{
							log::err("Karhu") << "Failed to set matrix buffer data for one by one triangle: " << errorToStringGL(err);
							return;
						}
						
						// A matrix is 4x4 but attributes can only be 4 so we need to use 4 attributes.
						for (GLuint i{0}; i < 4; ++ i)
						{
							const auto index(static_cast<GLuint>(1) + i);
							glEnableVertexAttribArray(index);
							glVertexAttribPointer
							(
								index,
								4,
								GL_FLOAT,
								GL_FALSE,
								sizeof(mth::Mat4f),
								reinterpret_cast<const GLvoid *>(sizeof(GLfloat) * i * 4)
							);
							
							if (GL_NO_ERROR != (err = glGetError()))
							{
								log::err("Karhu")
									<< "Failed to set matrix buffer attribute pointer "
									<< i
									<< " for one by one triangle: " << errorToStringGL(err);
								
								return;
							}
						
							// Make it instanced.
							glVertexAttribDivisor(index, 1);
						}
					}

					glBindVertexArray(0);
				}
				
				bool Renderer::Quadrenderer::render() const
				{
					// Draw the fullscreen triangle.
					glBindVertexArray(m_VAO);
					
					switch (m_mode)
					{
						case Mode::fullscreen: glDrawArrays(GL_TRIANGLES, 0, 3); break;
						case Mode::oneByOne:   glDrawArrays(GL_TRIANGLES, 0, 6); break;
					}
					
					glBindVertexArray(0);

					return true;
				}
				
				bool Renderer::Quadrenderer::renderMultiple(const std::vector<gfx::Instance> &instances) const
				{
					// Draw the fullscreen triangle.
					glBindVertexArray(m_VAO);
					
					glBindBuffer(GL_ARRAY_BUFFER, m_MBO);
					
					// Collect all the matrices in an array.
					std::vector<mth::Mat4f> mats{instances.size()};
					{
						std::size_t i{0};
						for (const auto &c : instances)
						{
							mats[i] = c.transform;
							++ i;
						}
					}

					// Pass the data to the buffer.
					glBufferData
					(
						GL_ARRAY_BUFFER,
						static_cast<GLsizeiptr>(sizeof(mth::Mat4f) * instances.size()),
						&mats[0],
						GL_DYNAMIC_DRAW /// @todo: need to check static/dynamic
					);
					
					switch (m_mode)
					{
						case Mode::fullscreen: break;
						case Mode::oneByOne:
							glDrawArraysInstanced
							(
								GL_TRIANGLES,
								0,
								6,
								static_cast<GLsizei>(instances.size())
							);
							break;
					}
					
					glBindVertexArray(0);

					return true;
				}

				Renderer::Quadrenderer::~Quadrenderer()
				{
					if (0 != m_VAO)
					{
						const GLuint ID(m_VAO);
						glDeleteVertexArrays(1, &ID);
					}
					
					if (0 != m_VBO)
					{
						const GLuint ID(m_VBO);
						glDeleteBuffers(1, &ID);
					}
				}

				std::uint32_t Renderer::c_FBDefault{0}, Renderer::c_RBDefault{0};
			}
		}
	}
}
