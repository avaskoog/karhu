/**
 * @author		Ava Skoog
 * @date		2017-10-22
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/backend/implementation/graphics/GL/Shader.hpp>
#include <karhu/app/backend/implementation/graphics/GL/setup.hpp>
#include <karhu/app/backend/implementation/graphics/GL/common.hpp>
#include <karhu/app/gfx/components/Camera.hpp>
#include <karhu/app/gfx/components/Transform.hpp>
#include <karhu/app/gfx/resources/Material.hpp>
#include <karhu/app/gfx/Rendertarget.hpp>
#include <karhu/core/log.hpp>

#ifdef KARHU_EDITOR
#include <karhu/app/App.hpp>
#include <karhu/app/gfx/SubsystemGraphics.hpp>
#include <karhu/app/gfx/Pipeline.hpp>
#include <regex>
#endif

/// @todo: Might crash if shader compilation fails; fix.

namespace
{
	inline std::size_t djb2(char const *str)
    {
        std::size_t hash = 5381;
        int c;

        while ((c = *str++))
            hash = ((hash << 5) + hash) + static_cast<std::size_t>(c); /* hash * 33 + c */

        return hash;
    }
	
	using namespace karhu;
	using namespace karhu::app::implementation::GL;

	static void logShader(GLuint ID, const char *type)
	{
		if (glIsShader(ID))
		{
			GLint length{0};
			GLint max{0};
			
			glGetShaderiv(ID, GL_INFO_LOG_LENGTH, &max);
			
			if (max > 0)
			{
				auto log(std::make_unique<char[]>(static_cast<std::size_t>(max)));
				glGetShaderInfoLog(ID, max, &length, log.get());
				
				if (length > 0)
					log::err("Karhu") << "Failed to compile " << type << " shader: " << log.get();
			}
		}
		else
			log::err("Karhu") << "Failed to compile " << type << " shader: not a shader";
	}

	static void logProgram(GLuint ID)
	{
		if (glIsProgram(ID))
		{
			GLint length{0};
			GLint max{0};
			
			glGetProgramiv(ID, GL_INFO_LOG_LENGTH, &max);

			if (max > 0)
			{
				auto log(std::make_unique<char[]>(static_cast<std::size_t>(max)));
				glGetProgramInfoLog(ID, max, &length, log.get());
			
				if (length > 0)
					log::err("Karhu") << "Failed to link program: " << log.get();
			}
		}
		else
			log::err("Karhu") << "Failed to link program: not a program";
	}
	
	static bool checkErrorUniformSet(const char *name)
	{
		GLenum err;
		if (GL_NO_ERROR != (err = glGetError()))
		{
			log::err("Karhu") << "Failed to set program uniform '" << name << "': " << errorToStringGL(err);
			return false;
		}
		
		return true;
	}
	
	static bool checkErrorUniformFind(const GLint &loc, const char *name)
	{
		if (loc < 0)
		{
//			log::wrn("Karhu") << "Failed to find program uniform '" << name << '\'';
			return false;
		}
		
		return true;
	}
}

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace GL
			{
				void Shader::addSourceGLSL
				(
					const Mode mode,
					const TypeGLSL type,
					const char *code
				)
				{
					addSourceGLSL
					(
						mode,
						type,
						code,
						true // Sources added directly as GLSL need to be prepared at runtime.
					);
				}
				
				bool Shader::supportsFormat(const Format &format) const
				{
					#ifdef KARHU_GLES
					if (Format::GLES == format)
					#else
					if (Format::GL == format)
					#endif
						return true;
					
					return false;
				}
				
				bool Shader::performAddSources(const std::vector<DataSource> &sources)
				{
					// Go through each source.
					for (const auto &source : sources)
					{
						// Check which type it is based on the suffix.
						TypeGLSL type;
						{
							if (source.name.length() < 5)
							{
								log::err("Karhu")
									<< "Error loading shader source '"
									<< source.name
									<< "': invalid filename; needs a valid GLSL type suffix (.vert or .frag)."; /// @todo: Update this if .geom support gets added.
								return false;
							}
							
							// All valid suffixes are five characters long (.vert/.geom).
							const std::string suffix{source.name.substr
							(
								source.name.length() - 5,
								5
							)};

							if (suffix == suffixForTypeGLSL(TypeGLSL::vert))
								type = TypeGLSL::vert;
							else if (suffix == suffixForTypeGLSL(TypeGLSL::frag))
								type = TypeGLSL::frag;
							else
							{
								log::err("Karhu")
									<< "Error loading shader source '"
									<< source.name
									<< "': invalid filename; needs a valid GLSL type suffix (.vert or .frag).";
								return false;
							}
						}

						// Try to add the source.
						addSourceGLSL
						(
							source.mode,
							type,
							source.code.c_str(),
							false // Code added this way is always prepared.
						);
					}

					return true;
				}

				bool Shader::performBake()
				{
					if (!bakeForVersion(m_single))   return false;
					if (!bakeForVersion(m_multiple)) return false;
					
					return true;
				}
				
				void Shader::performReset()
				{
					m_single.clean();
					m_multiple.clean();
				}

				bool Shader::performBindForSingle
				(
					const gfx::Pass &pass,
					const gfx::Layers &layers,
					const res::Material *const material,
					const std::uint32_t &widthWindow,
					const std::uint32_t &heightWindow,
					const float &scaleWindowX,
					const float &scaleWindowY
				) const
				{
					return bindBasics
					(
						pass,
						layers,
						material,
						m_single,
						widthWindow,
						heightWindow,
						scaleWindowX,
						scaleWindowY
					);
				}
				
				bool Shader::performBindForMultiple
				(
					const gfx::Pass &pass,
					const gfx::Layers &layers,
					const res::Material *const material,
					const std::uint32_t &widthWindow,
					const std::uint32_t &heightWindow,
					const float &scaleWindowX,
					const float &scaleWindowY
				) const
				{
					return bindBasics
					(
						pass,
						layers,
						material,
						m_multiple,
						widthWindow,
						heightWindow,
						scaleWindowX,
						scaleWindowY
					);
				}
				
				bool Shader::performBindInputsForSingle(const gfx::ContainerInputs &inputs) const
				{
					return bindInputsForVersion(m_single, inputs);
				}
				
				bool Shader::performBindInputsForMultiple(const gfx::ContainerInputs &inputs) const
				{
					return bindInputsForVersion(m_multiple, inputs);
				}
				
				bool Shader::performUpdateDataForInstanceSingle(const gfx::Instance &instance) const
				{
					const Version &version{m_single};
					
					clearErrorsGL();
					
					// Pass the model matrix.
					/// @todo: Deal with transform's origin, although not in here...
					glUniformMatrix4fv(version.uniformModel, 1, GL_FALSE, &instance.transform[0][0]);
					if (!checkErrorUniformSet(uniform::nameMatrixModel)) return false;

					return true;
				}

				bool Shader::performUpdateDataForInstancesMultiple(const std::vector<gfx::Instance> &) const
				{
					/// @todo: Add any data for multiple instances when/if it becomes necessary.
					return true;
				}
				
				bool Shader::bakeForVersion(Version &version)
				{
					using namespace std::literals::string_literals;
					
					clearErrorsGL();
					GLenum err;
					
					// Try to create the program.
					version.prog = glCreateProgram();
					if (GL_NO_ERROR != (err = glGetError()))
					{
						log::err("Karhu") << "Failed to create program: " << errorToStringGL(err);
						return false;
					}

					for (auto &shader : version.shaders)
					{
						// Store this for use in error messages.
						char const *const
							suffix(suffixForTypeGLSL(shader.type));
						
						// Create the correct kind of shader handle.
						switch (shader.type)
						{
							case TypeGLSL::vert: shader.ID = glCreateShader(GL_VERTEX_SHADER);   break;
							case TypeGLSL::frag: shader.ID = glCreateShader(GL_FRAGMENT_SHADER); break;
						}

						if (GL_NO_ERROR != (err = glGetError()))
						{
							log::err("Karhu") << "Failed to create " << suffix << " shader: " << errorToStringGL(err);
							version.clean();
							return false;
						}

						// Try to add the code.
						{
							char const
								*src{shader.code.c_str()};
							
							/// @todo: Quick hack to get editor mouse picking via ID output texture working with all shaders; hopefully we can find a cleaner and not platform specific solution later, but the whole graphics backend setup needs a rewrite anyway.
							#ifdef KARHU_EDITOR
							std::string srcForEditor;
							
							if
							(
								Type::regular == type() &&
								(TypeGLSL::frag == shader.type || Mode::multiple == version.type)
							)
							{
								/// @todo: Get app singleton out of shader bake, and existence.
								auto
									&app(*app::App::instance());
								
								if (app::ModeApp::server == app.mode())
								{
									auto const
										&pipeline(app.gfx().pipeline());
									
									for (std::size_t i{0}; i < pipeline.countSteps() - 1; ++ i)
									{
										auto &step(*pipeline.step(i));
										
										if (gfx::Pipeline::Step::Type::groups == step.step.type)
										{
											std::regex
												pattern{"void(\\s+)main(\\s*)\\((\\s*)\\)(\\s*)\\{"};
											
											std::string
												replacement{"\n"};
											
											constexpr char const
												*uniformInstance{"__karhuIDInstance"};
											
											if (TypeGLSL::frag == shader.type)
											{
												replacement += "layout(location = ";
												replacement += std::to_string(step.step.buffers.size() - 1);
												replacement += ") out int ";
												replacement += step.step.buffers.back().name;
												
												if (Mode::single == version.type)
													replacement += ";\nuniform int _entity;\n";
												else
												{
													/// @todo: If the max for instanced entities is going to be hard-coded to 512 it needs to also be held to that limit in the workloads.
													replacement += ";\nuniform int _entity[512];\n";
													replacement += "flat in int ";
													replacement += uniformInstance;
													replacement += ";\n";
												}
											}
											else
											{
												replacement += "flat out int ";
												replacement += uniformInstance;
												replacement += ";\n";
											}
											
											replacement += "\nvoid$1main$2($3)$4{\n\t";
											
											if (TypeGLSL::frag == shader.type)
											{
												replacement += step.step.buffers.back().name;
												
												if (Mode::single == version.type)
													replacement += " = _entity;\n";
												else
												{
													replacement += " = _entity[";
													replacement += uniformInstance;
													replacement += "];\n";
												}
											}
											else
											{
												replacement += uniformInstance;
												replacement += " = gl_InstanceID;\n";
											}
											
											std::regex_replace
											(
												std::back_inserter(srcForEditor),
												shader.code.begin(),
												shader.code.end(),
												pattern,
												replacement
											);
											
											src = srcForEditor.c_str();
											
											break;
										}
									}
								}
							}
							#endif
							
							glShaderSource(shader.ID, 1, &src, nullptr);
							if (GL_NO_ERROR != (err = glGetError()))
							{
								log::err("Karhu") << "Failed to set " << suffix << " shader source: " << errorToStringGL(err);
								version.clean();
								return false;
							}
						}
						
						// Try to compile the code.
						{
							GLint r;
							glCompileShader(shader.ID);
							glGetShaderiv(shader.ID, GL_COMPILE_STATUS, &r);
							if (GL_TRUE != r)
							{
								logShader(shader.ID, suffix);
								version.clean();
								return false;
							}
							glGetError();
						}

						// Try to attach the shader to the program.
						glAttachShader(version.prog, shader.ID);
						if (GL_NO_ERROR != (err = glGetError()))
						{
							log::err("Karhu")
								<< "Failed to attach " << suffix << " shader "
								<< shader.ID
								<< " to program "
								<< version.prog
								<< ": "
								<< errorToStringGL(err);
							version.clean();
							return false;
						}
					}
					
					// Try to link the program.
					{
						GLint r;
						glLinkProgram(version.prog);
						glGetProgramiv(version.prog, GL_LINK_STATUS, &r);
						
						if (GL_TRUE != r)
						{
							logProgram(version.prog);
							version.clean();
							return false;
						}
					}
					
					// Find the uniforms expected of the shader.
					/// @todo: Perhaps not expected of all shaders, is there a better way to do this?

					version.uniformView = glGetUniformLocation(version.prog, uniform::nameMatrixView);
					checkErrorUniformFind(version.uniformView, uniform::nameMatrixView);
					
					version.uniformProj = glGetUniformLocation(version.prog, uniform::nameMatrixProjection);
					checkErrorUniformFind(version.uniformProj, uniform::nameMatrixProjection);
					
					// No error checking for this one because it is okay if it does not exist.
					version.uniformAnimate = glGetUniformLocation(version.prog, uniform::nameFlagAnimate);

					if (Mode::single == version.type)
					{
						version.uniformModel = glGetUniformLocation(version.prog, uniform::nameMatrixModel);
						checkErrorUniformFind(version.uniformModel, uniform::nameMatrixModel);
					}

					return true;
				}
				
				bool Shader::bindBasics
				(
					const gfx::Pass &pass,
					const gfx::Layers &/*layers*/, /// @todo: Are layers needed in Shader::bindBasics()?
					const res::Material *const material,
					const Version &version,
					const std::uint32_t &widthWindow,
					const std::uint32_t &heightWindow,
					const float &scaleWindowX,
					const float &scaleWindowY
				) const
				{
					clearErrorsGL();
					GLenum err;

					// Activate the shader program.
					glUseProgram(version.prog);
					if (GL_NO_ERROR != (err = glGetError()))
					{
						log::err("Karhu") << "Failed to set program for use: " << errorToStringGL(err);
						return false;
					}
					
					// Pass in built-in values.
					/// @todo: These should not be hardcoded here but received somehow.
					{
						const auto loc(getUniformLocation(version, uniform::nameSizeViewport));
						
						if (loc >= 0)
						{
							float w{0.0f}, h{0.0f};
							
							if (pass.camera && pass.camera->target())
							{
								w = static_cast<float>(pass.camera->target()->width());
								h = static_cast<float>(pass.camera->target()->height());
							}
							
							if (0.0f == w || 0.0f == h)
							{
								w = static_cast<float>(widthWindow)  * scaleWindowX;
								h = static_cast<float>(heightWindow) * scaleWindowY;
							}
								glUniform2f
								(
									loc,
									static_cast<GLfloat>(w),
									static_cast<GLfloat>(h)
								); /// @todo: Multiply by scale?
						}
					}
					
					thread_local std::string s;

					// Set up type-dependent defaults.
					// These need to be set before the custom uniforms,
					// as they may overwrite some of the defaults.
					switch (type())
					{
						case Type::regular:
						{
							// Set the matrices from the camera.
							if (pass.camera)
							{
								glUniformMatrix4fv(version.uniformProj, 1, GL_FALSE, &pass.camera->matrixProjection()[0][0]);
								if (GL_NO_ERROR != (err = glGetError()))
								{
									log::err("Karhu") << "Failed to set program uniform '_proj': " << errorToStringGL(err);
									return false;
								}

								const mth::Mat4f view{pass.camera->matrixView(*pass.transformCamera)};
								
								glUniformMatrix4fv(version.uniformView, 1, GL_FALSE, &view[0][0]);
								if (GL_NO_ERROR != (err = glGetError()))
								{
									log::err("Karhu") << "Failed to set program uniform '_view': " << errorToStringGL(err);
									return false;
								}
							}
							
							// Reset the animation flag.
							if (version.uniformAnimate >= 0)
								glUniform1i(version.uniformAnimate, 0);
							
							break;
						}
						
						case Type::texture:
						{
							// Bind the textures to be used as outputs.
							if (material && material->output)
							{
								// Try to find the uniforms to pass in the dimensions.
								for (std::size_t i{0}; i < material->output->textures().size(); ++ i)
								{
									const auto &t(material->output->textures()[i]);

									s = uniform::prefixSizeTarget;
									s += std::to_string(i);
									const auto loc(getUniformLocation(version, s));
									
									// We do not fail completely on error since these
									// uniforms might not be of interest to the user.
									if (loc < 0)
										continue;

									// Pass the size in as a vec2.
									glUniform2f
									(
										loc,
										static_cast<GLfloat>(t->width()),
										static_cast<GLfloat>(t->height())
									);
								}

								// Bind the textures themselves.
								if (!material->output->bind
								(
									widthWindow,
									heightWindow,
									scaleWindowX,
									scaleWindowY
								))
									return false;
							}
							
							break;
						}
					}
					
					m_indexTexCurr = 0;
					m_textureslots.clear();
					
					if (material)
					{
						// Enable the correct culling.
						{
							using Cull = gfx::Culling;

							switch (material->culling)
							{
								case Cull::none:
									glDisable(GL_CULL_FACE);
									break;
									
								case Cull::back:
									glEnable(GL_CULL_FACE);
									glCullFace(GL_BACK);
									break;

								case Cull::front:
									glEnable(GL_CULL_FACE);
									glCullFace(GL_FRONT);
									break;

								case Cull::both:
									glEnable(GL_CULL_FACE);
									glCullFace(GL_FRONT_AND_BACK);
									break;
							}
						}

						// Time to check what buffers to interact with and how.
						{
							using Buffer = gfx::Buffertarget;

							// Figure out the colour buffer behaviour.
							if (static_cast<bool>(Buffer::colour & material->buffertargets))
								glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
							else
								glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

							// Figure out the depth buffer behaviour.
							{
								using Depth = gfx::ConditionDepth;

								if (static_cast<bool>(Buffer::depth & material->buffertargets))
									glDepthMask(GL_TRUE);
								else
									glDepthMask(GL_FALSE);

								switch (material->conditionDepth)
								{
									case Depth::always: glEnable (GL_DEPTH_TEST); break;
									case Depth::never:  glDisable(GL_DEPTH_TEST); break;
								}
							}

							// Figure out the stencil buffer behaviour.
							if (static_cast<bool>(Buffer::stencil & material->buffertargets))
							{
								glEnable(GL_STENCIL_TEST);
								glStencilMask(static_cast<GLuint>(material->stencil.mask));
								
								// Figure out what stencil function/comparison to set.
								// This is also where we set reference and mask values.
								{
									using Cond = gfx::ConditionStencil;
									
									GLenum f{GL_NEVER};
									switch (material->stencil.condition)
									{
										case Cond::never:          f = GL_NEVER;    break;
										case Cond::always:         f = GL_ALWAYS;   break;
										case Cond::equal:          f = GL_EQUAL;    break;
										case Cond::unequal:        f = GL_NOTEQUAL; break;
										case Cond::less:           f = GL_LESS;     break;
										case Cond::lessOrEqual:    f = GL_LEQUAL;   break;
										case Cond::greater:        f = GL_GREATER;  break;
										case Cond::greaterOrEqual: f = GL_GEQUAL;   break;
									}

									glStencilFunc
									(
										f,
										static_cast<GLint>(material->stencil.ref),
										0xff
									);
								}
								
								{
									using Op = gfx::OperationStencil;
									
									GLenum f{GL_KEEP};
									switch (material->stencil.operation)
									{
										case Op::keep:             f = GL_KEEP;      break;
										case Op::replace:          f = GL_REPLACE;   break;
										case Op::invert:           f = GL_INVERT;    break;
										case Op::zero:             f = GL_ZERO;      break;
										case Op::increment:        f = GL_INCR;      break;
										case Op::incrementAndWrap: f = GL_INCR_WRAP; break;
										case Op::decrement:        f = GL_DECR;      break;
										case Op::decrementAndWrap: f = GL_DECR_WRAP; break;
									}

									glStencilOp(f, f, f); /// @todo: Maybe allow the user to control all of these.
								}
							}
							else
								glDisable(GL_STENCIL_TEST);
						}
					}
					else
					{
						glEnable(GL_CULL_FACE);
						glCullFace(GL_BACK);
						glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
						glDepthMask(GL_TRUE);
						glEnable(GL_DEPTH_TEST);
						glDisable(GL_STENCIL_TEST);
					}
					
					return true;
				}
				
				bool Shader::bindInputsForVersion
				(
					const Version &version,
					const gfx::ContainerInputs &inputs
				) const
				{
					/// @todo: Cache uniforms?
					
					thread_local std::string s;
					
					for (auto &v : inputs.values())
					{
						using T = gfx::TypeInput;

						// Try to find the uniforms.
						const auto loc(getUniformLocation(version, v.first));

						// We do not fail completely on error since these
						// uniforms might not be of interest to the user.
						if (loc < 0)
							continue;

						// Figure out what kind of data to pass in.
						switch (v.second.type)
						{
							case T::boolean:
								glUniform1i(loc, (v.second.boolean) ? 1 : 0);
								break;
								
							case T::integer:
								glUniform1i(loc, static_cast<GLint>(v.second.integer));
								break;

							case T::floating:
								glUniform1f(loc, static_cast<GLfloat>(v.second.floating));
								break;
							
							case T::vector2i:
								glUniform2i
								(
									loc,
									static_cast<GLint>(v.second.vector2i.x),
									static_cast<GLint>(v.second.vector2i.y)
								);
								break;
							
							case T::vector3i:
								glUniform3i
								(
									loc,
									static_cast<GLint>(v.second.vector3i.x),
									static_cast<GLint>(v.second.vector3i.y),
									static_cast<GLint>(v.second.vector3i.z)
								);
								break;
							
							case T::vector4i:
								glUniform4i
								(
									loc,
									static_cast<GLint>(v.second.vector4i.x),
									static_cast<GLint>(v.second.vector4i.y),
									static_cast<GLint>(v.second.vector4i.z),
									static_cast<GLint>(v.second.vector4i.w)
								);
								break;
							
							case T::vector2f:
								glUniform2f
								(
									loc,
									static_cast<GLfloat>(v.second.vector2f.x),
									static_cast<GLfloat>(v.second.vector2f.y)
								);
								break;
							
							case T::vector3f:
								glUniform3f
								(
									loc,
									static_cast<GLfloat>(v.second.vector3f.x),
									static_cast<GLfloat>(v.second.vector3f.y),
									static_cast<GLfloat>(v.second.vector3f.z)
								);
								break;
							
							case T::vector4f:
								glUniform4f
								(
									loc,
									static_cast<GLfloat>(v.second.vector4f.x),
									static_cast<GLfloat>(v.second.vector4f.y),
									static_cast<GLfloat>(v.second.vector4f.z),
									static_cast<GLfloat>(v.second.vector4f.w)
								);
								break;

							case T::matrix2:
								glUniformMatrix2fv
								(
									loc,
									1,
									GL_FALSE,
									&v.second.matrix2[0][0]
								);
								break;

							case T::matrix3:
								glUniformMatrix3fv
								(
									loc,
									1,
									GL_FALSE,
									&v.second.matrix3[0][0]
								);
								break;

							case T::matrix4:
								glUniformMatrix4fv
								(
									loc,
									1,
									GL_FALSE,
									&v.second.matrix4[0][0]
								);
								break;

							case T::integerv:
								glUniform1iv
								(
									loc,
									static_cast<GLsizei>(v.second.integerv.count),
									v.second.integerv.data
								);
								break;

							case T::floatingv:
								glUniform1fv
								(
									loc,
									static_cast<GLsizei>(v.second.floatingv.count),
									v.second.floatingv.data
								);
								break;

							case T::vector2iv:
								glUniform2iv
								(
									loc,
									static_cast<GLsizei>(v.second.vector2iv.count),
									&v.second.vector2iv.data[0].x
								);
								break;

							case T::vector3iv:
								glUniform3iv
								(
									loc,
									static_cast<GLsizei>(v.second.vector3iv.count),
									&v.second.vector3iv.data[0].x
								);
								break;

							case T::vector4iv:
								glUniform4iv
								(
									loc,
									static_cast<GLsizei>(v.second.vector4iv.count),
									&v.second.vector4iv.data[0].x
								);
								break;

							case T::vector2fv:
								glUniform2fv
								(
									loc,
									static_cast<GLsizei>(v.second.vector2fv.count),
									&v.second.vector2fv.data[0].x
								);
								break;

							case T::vector3fv:
								glUniform3fv
								(
									loc,
									static_cast<GLsizei>(v.second.vector3fv.count),
									&v.second.vector3fv.data[0].x
								);
								break;

							case T::vector4fv:
								glUniform4fv
								(
									loc,
									static_cast<GLsizei>(v.second.vector4fv.count),
									&v.second.vector4fv.data[0].x
								);
								break;

							case T::matrix2v:
								glUniformMatrix2fv
								(
									loc,
									static_cast<GLsizei>(v.second.matrix2v.count),
									GL_FALSE,
									&v.second.matrix2v.data[0][0][0]
								);
								break;

							case T::matrix3v:
								glUniformMatrix3fv
								(
									loc,
									static_cast<GLsizei>(v.second.matrix3v.count),
									GL_FALSE,
									&v.second.matrix3v.data[0][0][0]
								);
								break;

							case T::matrix4v:
								glUniformMatrix4fv
								(
									loc,
									static_cast<GLsizei>(v.second.matrix4v.count),
									GL_FALSE,
									&v.second.matrix4v.data[0][0][0]
								);
								break;
								
							case T::texture:
							{
								#if defined(KARHU_EDITOR) || defined(DEBUG)
									if (!v.second.texture.get() || !v.second.texture.get()->valid())
										break;
								#endif
									
								std::size_t
									index;
								
								const auto it(m_textureslots.find(v.first));
								
								if (it != m_textureslots.end())
									index = it->second;
								else
								{
									index = m_indexTexCurr;
									m_textureslots.emplace(v.first, index);
									++ m_indexTexCurr;
								}
								
								// Set the sampler2D uniform.
								glActiveTexture(GL_TEXTURE0 + static_cast<GLenum>(index)); /// @todo: Enforce/check limit?
								glBindTexture
								(
									GL_TEXTURE_2D,
									*reinterpret_cast<const std::uint32_t *>(v.second.texture.get()->handle())
								);
 								glUniform1i(loc, static_cast<GLint>(index));
								
								// Set the size if we can find it.
								s = uniform::prefixSizeTexture;
								s += v.first;
								const auto locSize(getUniformLocation(version, s));

								if (locSize >= 0)
									glUniform2f
									(
										locSize,
										static_cast<GLfloat>(v.second.texture.get()->width()),
										static_cast<GLfloat>(v.second.texture.get()->height())
									);

								break;
							}
							
							case T::invalid:
								break;
						}
					}
					
					return true;
				}
				
				std::int32_t Shader::getUniformLocation(Version const &version, std::string const &name) const
				{
					std::size_t const hs{djb2(name.c_str())};
					
					for (std::size_t i{0}, size = version.m_uniforms.size(); i < size; ++ i)
					{
						if (hs == version.m_uniforms[i].first)
							return version.m_uniforms[i].second;
					}
					
//					if (it != version.m_uniforms.end())
//						return it->second;
					
					// Try to find the uniform.
					GLint loc = glGetUniformLocation(version.prog, name.c_str());
					
					/*if (loc < 0)
					{
					log::msg("DEBUG")<<"LOC "<<name<<" < 0";
//						log::dbg("Karhu")
//							<< "Failed to find uniform '"
//							<< name
//							<< "' in program "
//							<< version.prog
//							<< '!';
						return -1;
					}*/
					
					const auto
						r(static_cast<std::int32_t>(loc));
					
					version.m_uniforms.emplace_back(hs, r);
					
					return r;
				}
				
				void Shader::addSourceGLSL
				(
					const Mode mode,
					const TypeGLSL type,
					const char *code,
					const bool prepare
				)
				{
					Version &version{(Mode::single == mode) ? m_single : m_multiple};
					
					const Format format
					{
						#ifdef KARHU_GLES
							Format::GLES
						#else
							Format::GL
						#endif
					};
					
					version.shaders.emplace_back(ShaderGLSL
					{
						type,
						// Header needs to be added to unprepared shaders.
						(
							(prepare)
							?
								addHeaderToGLSL(format, mode, type, code)
							:
								code
						)
					});
				}

				void Shader::Version::clean()
				{
					m_uniforms.clear();
				
					for (auto &shader : shaders)
					{
						if (0 != shader.ID)
							glDeleteShader(shader.ID);
						
						shader.ID = 0;
					}
					
					shaders.clear();
					
					if (0 != prog)
						glDeleteProgram(prog);
					
					prog = 0;
					
					uniformView    =
					uniformProj    =
					uniformModel   =
					uniformAnimate = -1;
				}
			}
		}
	}
}
