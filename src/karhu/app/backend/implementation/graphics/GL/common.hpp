/**
 * @author		Ava Skoog
 * @date		2017-11-07
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_GRAPHICS_GL_COMMON_H_
	#define KARHU_APP_BACKEND_GRAPHICS_GL_COMMON_H_

	#include <karhu/app/backend/implementation/graphics/GL/setup.hpp>
	#include <karhu/conv/graphics.hpp>
	#include <karhu/core/log.hpp>

	#include <sstream>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace GL
				{
					/**
					 * Specifies the index to use for which vertex attribute.
					 * Meant simply to be cast to integer to extract the value.
					 * Can be used when generating shader to set correct locations.
					 */
					enum class IndexAttributeVertex : std::size_t
					{
						matricesModelInstanced = 0,
						position               = 4,
						normal                 = 5,
						UV                     = 6,
						colour                 = 7,
						jointset0              = 8,
						weightset0             = 9,
						jointset1              = 10,
						weightset1             = 11,
						jointset2              = 12,
						weightset2             = 13,
						tangent                = 14,
						custom                 = 15
					};

					// Names of default uniforms.
					namespace uniform
					{
						static constexpr auto
							nameMatrixProjection ("_proj"),
							nameMatrixView       ("_view"),
							nameMatrixModel      ("_model"),
							nameFlagAnimate      ("_animate"),
							nameArrayMatrixJoints("_joints"),
							nameSizeViewport     ("_sizeViewport"),
							nameTextureMain      ("_texMain"),
							prefixUseTexture     ("_useTex_"),
							prefixSizeTexture    ("_sizeTex_"),
							prefixSizeTarget     ("_sizeTarget_");
					}

					inline constexpr auto errorToStringGL(GLenum err) noexcept
					{
						#define karhuERRCASE(error) case error: return #error;
						
						switch (err)
						{
							karhuERRCASE(GL_INVALID_ENUM)
							karhuERRCASE(GL_INVALID_VALUE)
							karhuERRCASE(GL_INVALID_OPERATION)
							karhuERRCASE(GL_INVALID_FRAMEBUFFER_OPERATION)
							karhuERRCASE(GL_OUT_OF_MEMORY)
							
							#ifndef KARHU_GLES
								karhuERRCASE(GL_STACK_UNDERFLOW)
								karhuERRCASE(GL_STACK_OVERFLOW)
							#endif
							
							default: return "unknown";
						}
						
						#undef karhuERRCASE
					}
					
					inline void clearErrorsGL()
					{
						while (GL_NO_ERROR != glGetError());
					}
				}
			}
		}
	}
#endif
