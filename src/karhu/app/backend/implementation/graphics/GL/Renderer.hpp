/**
 * @author		Ava Skoog
 * @date		2017-10-22
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_GRAPHICS_GL_RENDERER_H_
	#define KARHU_APP_BACKEND_GRAPHICS_GL_RENDERER_H_

	#include <karhu/app/gfx/Renderer.hpp>
	#include <karhu/app/backend/implementation/graphics/GL/Shader.hpp>
	#include <karhu/app/backend/implementation/graphics/GL/Texture.hpp>
	#include <karhu/app/backend/implementation/graphics/GL/Rendertarget.hpp>
	#include <karhu/app/gfx/resources/Material.hpp>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace GL
				{
					class Renderer : public gfx::Renderer
					{
						private:
							class Quadrenderer /// @todo: Rename.
							{
								public:
									enum class Mode : std::uint8_t
									{
										fullscreen,
										oneByOne
									};
								
								public:
									Quadrenderer(const Mode mode);

									bool render() const;
									bool renderMultiple(const std::vector<gfx::Instance> &instances) const;

									~Quadrenderer();

								private:
									Mode m_mode;
									std::uint32_t m_VAO{0}, m_MBO{0}, m_VBO{0};
							};

						public:
							static std::uint32_t &framebufferDefault() noexcept { return c_FBDefault; }
							static std::uint32_t &renderbufferDefault() noexcept { return c_RBDefault; }
						
						public:
							Renderer();

						protected:
							const res::Shader &shaderForQuadOneByOne() const override
							{
								return m_shaderQuadOneByOne;
							}
						
							void performClear(const gfx::ColRGBAf &) override;
							void performSetDepthmask(const bool) const override;
							void performSetConditionDepth(const gfx::ConditionDepth) const override;
							void performSetCulling(const gfx::Culling) const override;
						
							void performResetTexturesamplers() const override;
						
							void performDrawQuadOneByOne
							(
								const float UVs[4]
							) const override;
						
							void performDrawQuadsOneByOne
							(
								const std::vector<gfx::Instance> &instances,
								const float UVs[4]
							) const override;
						
							void performDrawSingleWithoutIndices
							(
								const res::Mesh::Buffer &buffer
							) const override;
						
							void performDrawSingleWithIndices
							(
								const res::Mesh::Buffer        &buffer,
								const res::Mesh::Buffer::Range &range
							) const override;
						
							void performDrawMultipleWithoutIndices
							(
								const res::Mesh::Buffer &buffer,
								const std::size_t       &count
							) const override;
						
							void performDrawMultipleWithIndices
							(
								const res::Mesh::Buffer        &buffer,
								const res::Mesh::Buffer::Range &range,
								const std::size_t              &count
							) const override;
						
							void performRenderQuadFullscreen
							(
								const gfx::Pass &pass,
								const gfx::ContainerInputs &inputs,
								const std::uint32_t &widthWindow,
								const std::uint32_t &heightWindow,
								const float &scaleWindowX,
								const float &scaleWindowY,
								res::Material &material,
								res::Shader *const shader
							) const override;
						
							void performCompositeQuadsFullscreen
							(
								const gfx::Pass &pass,
								const gfx::ContainerInputs &inputs,
								const std::uint32_t &widthWindow,
								const std::uint32_t &heightWindow,
								const float &scaleWindowX,
								const float &scaleWindowY,
								res::Material &material,
								res::Shader *const shader
							) const override;
						
							bool performBindCamera(gfx::Camera &camera) override;
							void performClearCamera(gfx::Camera &camera) override;
					
						private:
							static std::uint32_t c_FBDefault, c_RBDefault;

						private:
							/// @todo: Put all this stuff in the quad renderer struct.
						
							std::int32_t
								m_uniformUseComposite{-1},
								m_uniformUV0{-1},
								m_uniformUV1{-1},
								m_uniformUV0Multiple{-1},
								m_uniformUV1Multiple{-1};
						
							std::uint32_t m_texDefault{0};
						
							Shader
								m_shaderQuadFullscreen,
								m_shaderQuadOneByOne;
						
							Quadrenderer
								m_quadrendererFullscreen{Quadrenderer::Mode::fullscreen},
								m_quadrendererOneByOne  {Quadrenderer::Mode::oneByOne};
					};
				}
			}
		}
	}
#endif
