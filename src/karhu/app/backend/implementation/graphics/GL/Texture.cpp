/**
 * @author		Ava Skoog
 * @date		2017-11-01
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/backend/implementation/graphics/GL/Texture.hpp>
#include <karhu/app/backend/implementation/graphics/GL/Renderer.hpp>
#include <karhu/app/backend/implementation/graphics/GL/setup.hpp>
#include <karhu/app/backend/implementation/graphics/GL/common.hpp>
#include <karhu/app/backend/implementation/graphics/GL/setup.hpp>
#include <karhu/core/log.hpp>
#include <karhu/core/platform.hpp>

namespace
{
	using namespace karhu;
	
	inline GLenum formatToGLEnum(const gfx::Pixelbuffer::Format &format)
	{
		switch (format)
		{
			case gfx::Pixelbuffer::Format::BW:   return GL_RED;
			case gfx::Pixelbuffer::Format::BWA:  return GL_RG;
			case gfx::Pixelbuffer::Format::RGB:  return GL_RGB;
			case gfx::Pixelbuffer::Format::RGBA: return GL_RGBA;
		}
		
		return GL_RGB;
	}
}

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace GL
			{				
				bool Texture::performCopy(gfx::Pixelbuffer &buffer) const
				{
					GLenum const
						mode{formatToGLEnum(format())};
					
					buffer.reserve(width(), height(), format());
					
					if
					(
						Type::colour == type()
						#ifndef KARHU_GLES
						|| Type::data == type()
						#endif
					)
					{
						// We can use this more elegant method on non-GLES.
						#ifndef KARHU_GLES
							glBindTexture(GL_TEXTURE_2D, static_cast<GLuint>(m_ID));
						
							glGetError();
						
							glGetTexImage
							(
								GL_TEXTURE_2D,
								0,
								((Type::data == type()) ? GL_RED_INTEGER : mode),
								((Type::data == type()) ? GL_UNSIGNED_INT : GL_UNSIGNED_BYTE),
								buffer.components.get()
							);
						
							GLenum const
								r{glGetError()};
						
							glBindTexture(GL_TEXTURE_2D, static_cast<GLuint>(Renderer::renderbufferDefault()));
						
							return (GL_NO_ERROR == r);
						// GLES lacks this function, so it gets messier but still doable.
						#else
							GLuint FBO; /// @todo: Cache a global FBO for all textures?
						
							glGetError();
							glGenFramebuffers(1, &FBO);
						
							if (GL_NO_ERROR != glGetError())
								return false;
						
							glBindFramebuffer(GL_FRAMEBUFFER, FBO);
						
							if (GL_NO_ERROR != glGetError())
								return false;
						
							glFramebufferTexture2D
							(
								GL_FRAMEBUFFER,
								GL_COLOR_ATTACHMENT0,
								GL_TEXTURE_2D,
								static_cast<GLuint>(m_ID),
								0
							);
						
							GLenum r{glGetError()};
						
							if (GL_NO_ERROR == r)
							{
								glReadPixels
								(
									0,
									0,
									static_cast<GLsizei>(width()),
									static_cast<GLsizei>(height()),
									mode,
									GL_UNSIGNED_BYTE,
									buffer.components.get()
								);
								
								r = glGetError();
							}

							glBindFramebuffer
							(
								GL_FRAMEBUFFER,
								static_cast<GLuint>(Renderer::framebufferDefault())
							);
						
							glDeleteFramebuffers(1, &FBO);
						
							return (GL_NO_ERROR == r);
						#endif
					}
					else
						/// @todo: Support copying depth and stencil textures too.
						log::err("Karhu") << "Copying non-colour (or data on GLES) textures is currently not supported";
					
					return false;
				}
				
				bool Texture::performBake()
				{
					clearErrorsGL();
					GLenum err;
					
					// Regenerate texture if necessary.
					if (0 == m_ID)
						createBasedOnType();

					// Validate the ID.
					if (0 == m_ID)
					{
						err = glGetError();
						log::err("Karhu") << "Failed to generate texture: " << errorToStringGL(err);
						return false;
					}
					
					// Try to bind the texture.
					bindBasedOnType(m_ID);
					
					if (GL_NO_ERROR != (err = glGetError()))
					{
						log::err("Karhu") << "Failed to bind texture " << m_ID << ": " << errorToStringGL(err);
						return false;
					}
					
					bool const
						integral{(Type::data == type())};

					// Insert the correct data.
					switch (type())
					{
						// Transfer the pixel data to the OpenGL texture if this is a colour texture.
						case Type::colour:
						case Type::data:
						{
							GLenum const
								modeInternal{(integral) ? GL_RGBA32I      : formatToGLEnum(format())},
								mode        {(integral) ? GL_RGBA_INTEGER : modeInternal};
							
							glTexImage2D
							(
								GL_TEXTURE_2D,
								0,
								static_cast<GLint>(modeInternal),
								static_cast<GLsizei>(width()),
								static_cast<GLsizei>(height()),
								0,
								mode,
								((integral) ? GL_UNSIGNED_INT : GL_UNSIGNED_BYTE),
								pixels()
							);
							
							if (GL_NO_ERROR != (err = glGetError()))
							{
								log::err("Karhu")
									<< "Failed to set texture "
									<< m_ID
									<< " pixels: "
									<< errorToStringGL(err);

								unbindBasedOnType();
								
								return false;
							}
							
							break;
						}
						
						// Set up the depth buffer storage if this is a depth texture.
						case Type::depth:
						case Type::shadowdepth:
						{
							glTexImage2D
							(
								GL_TEXTURE_2D,
								0,
								GL_DEPTH_COMPONENT24,
								static_cast<GLsizei>(width()),
								static_cast<GLsizei>(height()),
								0,
								GL_DEPTH_COMPONENT,
								GL_UNSIGNED_INT,
								nullptr
							);
							
							if (GL_NO_ERROR != (err = glGetError()))
							{
								log::err("Karhu")
									<< "Failed to set depth buffer "
									<< m_ID
									<< " storage: "
									<< errorToStringGL(err);
							
								unbindBasedOnType();
							
								return false;
							}
							
							if (Type::shadowdepth == type())
							{
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
							}

							break;
						}

						// Set up the depth buffer storage if this is a depth texture.
						case Type::stencil:
						{
							glTexImage2D
							(
								GL_TEXTURE_2D,
								0,
								GL_DEPTH24_STENCIL8,
								static_cast<GLsizei>(width()),
								static_cast<GLsizei>(height()),
								0,
								GL_DEPTH_STENCIL,
								GL_UNSIGNED_INT_24_8,
								nullptr
							);
							if (GL_NO_ERROR != (err = glGetError()))
							{
								log::err("Karhu")
									<< "Failed to set stencil buffer "
									<< m_ID
									<< " storage: "
									<< errorToStringGL(err);
								
								unbindBasedOnType();
								
								return false;
							}

							break;
						}
					}

					/// @todo: Mipmaps?
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
					
					// Filtering.
					{
						GLint f;
						
						switch (filter())
						{
							case ModeFilter::nearest: f = GL_NEAREST; break;
							case ModeFilter::linear:  f = GL_LINEAR;  break;
						}
						
						if (integral)
							f = GL_NEAREST;
						
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, f);
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, f);
					}
					
					// Wrapping.
					if (Type::stencil != type())
					{
						// Set up wrapping behaviour.
						switch (wrap())
						{
							case ModeWrap::repeat:
							{
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
								break;
							}
							
							case ModeWrap::clamp:
							{
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
								break;
							}
						}
					}

					// Unbind the texture.
					unbindBasedOnType();

					return true;
				}
				
				void Texture::createBasedOnType()
				{
					GLuint ID{0};
					glGenTextures(1, &ID);
					m_ID = ID;
				}
				
				void Texture::destroyBasedOnType()
				{
					const GLuint ID(m_ID);
					glDeleteTextures(1, &ID);
					m_ID = 0;
				}

				void Texture::bindBasedOnType(std::uint32_t const ID)
				{
					glBindTexture(GL_TEXTURE_2D, static_cast<GLuint>(ID));
				}

				void Texture::unbindBasedOnType()
				{
					/// @todo: Or 0 for regular texture?
					bindBasedOnType(Renderer::renderbufferDefault());
				}
				
				Texture::~Texture()
				{
					if (0 != m_ID)
						destroyBasedOnType();
				}
			}
		}
	}
}
