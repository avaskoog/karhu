/**
 * @author		Ava Skoog
 * @date		2017-10-22
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_GRAPHICS_GL_SHADER_H_
	#define KARHU_APP_BACKEND_GRAPHICS_GL_SHADER_H_

	#include <karhu/app/gfx/resources/Shader.hpp>

	#include <string>
	#include <unordered_map>
	#include <cstdint>

	namespace karhu
	{
		class HashedString
		{
			public:
				HashedString(std::string string)
				:
				m_hash  {std::hash<std::string>{}(string)},
				m_string{std::move(string)}
				{
				}
			
				HashedString(HashedString const &o)
				:
				m_hash  {o.m_hash},
				m_string{o.m_string}
				{
				}
			
				HashedString(HashedString &&o)
				:
				m_hash  {o.m_hash},
				m_string{std::move(o.m_string)}
				{
				}
			
				HashedString &operator=(HashedString const &o)
				{
					m_hash   = o.m_hash;
					m_string = o.m_string;
					return *this;
				}
			
				HashedString &operator=(HashedString &&o)
				{
					m_hash   = o.m_hash;
					m_string = std::move(o.m_string);
					return *this;
				}
			
				std::size_t        hash()   const noexcept { return m_hash; }
				std::string const &string() const noexcept { return m_string; }
			
			private:
				std::size_t m_hash;
				std::string m_string;
		};
		
		struct CompareHashedString
		{
			bool operator()(HashedString const &a, HashedString const &b) const
			{
				if (a.hash() < b.hash())
					return true;
				
				if (a.hash() > b.hash())
					return false;
				
				return (a.string() < b.string());
			}
		};
		
		namespace app
		{
			namespace implementation
			{
				namespace GL
				{
					class Shader : public gfx::Shader
					{
						private:
							struct ShaderGLSL
							{
								TypeGLSL      type;
								std::string   code;
								std::uint32_t ID{0};
							};

							struct Version
							{
								const Mode type{}; /// @todo: Should probably rename this 'mode' for less confusion...
								std::vector<ShaderGLSL> shaders{};
								std::uint32_t prog{0};
								
								/// @todo: Get rid of these and use normal caching.
								std::int32_t
									uniformView   {-1},
									uniformProj   {-1},
									uniformModel  {-1},
									uniformAnimate{-1};
								
								mutable std::vector<std::pair<std::size_t, std::int32_t>> m_uniforms;
								
								void clean();
								~Version() { clean(); }
							};
						
						public:
							using gfx::Shader::Shader;

							/**
							 * Can be used to push GLSL code directly to the GL shader implementation if necessary.
							 *
							 * The code should exclude the version and other header information such as precision
							 * settings as these will be automatically generated depending on the target platform.
							 *
							 * @param typeVersion Whether the shader is for single or multiple rendering.
							 * @param typeGLSL    The GLSL shader type.
							 * @param codeGLSL    The GLSL shader code.
							 */
							void addSourceGLSL(const Mode typeVersion, const TypeGLSL typeGLSL, const char *codeGLSL);
						
							std::uint32_t programGLSL(const Mode typeVersion) const
							{
								switch (typeVersion)
								{
									case Mode::single:   return m_single.prog;
									case Mode::multiple: return m_multiple.prog;
									case Mode::both:     return 0;
								}
							}
						
						public:
							bool supportsFormat(const Format &format) const override;

						protected:
							bool performAddSources(const std::vector<DataSource> &) override;
							bool performBake() override;
							void performReset() override;

							bool performBindForSingle
							(
								const gfx::Pass &,
								const gfx::Layers &layers,
								const res::Material *const,
								const std::uint32_t &widthWindow,
								const std::uint32_t &heightWindow,
								const float &scaleWindowX,
								const float &scaleWindowY
							) const override;
						
							bool performBindForMultiple
							(
								const gfx::Pass &,
								const gfx::Layers &layers,
								const res::Material *const,
								const std::uint32_t &widthWindow,
								const std::uint32_t &heightWindow,
								const float &scaleWindowX,
								const float &scaleWindowY
							) const override;
						
							bool performBindInputsForSingle
							(
								const gfx::ContainerInputs &inputs
							) const override;
						
							bool performBindInputsForMultiple
							(
								const gfx::ContainerInputs &inputs
							) const override;
						
							bool performUpdateDataForInstanceSingle(const gfx::Instance &) const override;
							bool performUpdateDataForInstancesMultiple(const std::vector<gfx::Instance> &) const override;
						
						private:
							bool bakeForVersion(Version &);
						
							bool bindBasics
							(
								const gfx::Pass &,
								const gfx::Layers &layers,
								const res::Material *const,
								const Version &,
								const std::uint32_t &widthWindow,
								const std::uint32_t &heightWindow,
								const float &scaleWindowX,
								const float &scaleWindowY
							) const;
						
							bool bindInputsForVersion
							(
								const Version &,
								const gfx::ContainerInputs &
							) const;
						
							std::int32_t getUniformLocation(const Version &version, const std::string &name) const;

							void addSourceGLSL(const Mode typeVersion, const TypeGLSL typeGLSL, const char *codeGLSL, const bool shouldBake);
						
						private:
							Version m_single{Mode::single}, m_multiple{Mode::multiple};
						
							/// @todo: This stuff should probably be handled by the base renderer/shader class/es and not here...
							mutable std::size_t m_indexTexCurr;
							mutable std::map<HashedString, std::size_t, CompareHashedString> m_textureslots;
					};
				}
			}
		}
	}
#endif
