/**
 * @author		Ava Skoog
 * @date		2017-11-01
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_GRAPHICS_GL_TEXTURE_H_
	#define KARHU_APP_BACKEND_GRAPHICS_GL_TEXTURE_H_

	#include <karhu/app/gfx/resources/Texture.hpp>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace GL
				{
					class Texture : public gfx::Texture
					{
						public:
							using gfx::Texture::Texture;

							void const *handle() const noexcept override { return &m_ID; }

							~Texture();

						protected:
							virtual bool performCopy(gfx::Pixelbuffer &) const override;
							virtual bool performBake() override;
						
						private:
							void createBasedOnType();
							void destroyBasedOnType();
							void bindBasedOnType(std::uint32_t const ID);
							void unbindBasedOnType();
						
						private:
							std::uint32_t m_ID{0};
					};
				}
			}
		}
	}
#endif
