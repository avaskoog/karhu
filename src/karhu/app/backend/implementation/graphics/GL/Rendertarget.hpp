/**
 * @author		Ava Skoog
 * @date		2017-11-02
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_GRAPHICS_GL_RENDERTARGET_H_
	#define KARHU_APP_BACKEND_GRAPHICS_GL_RENDERTARGET_H_

	#include <karhu/app/gfx/Rendertarget.hpp>

	#include <cstdint>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace GL
				{
					class Rendertarget : public gfx::Rendertarget
					{
						public:
							~Rendertarget();
						
						protected:
							virtual bool performBake()   override;
							virtual bool performBind()   override;
							virtual bool performUnbind() override;
						
							virtual void performClearTextureColour(std::size_t const index, gfx::ColRGBAf const &colour) override;
							virtual void performClearTextureData(std::size_t const index, std::int32_t const data) override;
						
						private:
							bool validateFramebuffer();
							void bindDefault();

						private:
							std::uint32_t m_ID{0};
					};
				}
			}
		}
	}
#endif
