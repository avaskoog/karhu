/**
 * @author		Ava Skoog
 * @date		2017-11-02
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Should I delete FBO and textures first before resizing them? Read there might be issues some places... Look into it.

#include <karhu/app/backend/implementation/graphics/GL/Rendertarget.hpp>
#include <karhu/app/backend/implementation/graphics/GL/Renderer.hpp>
#include <karhu/app/backend/implementation/graphics/GL/setup.hpp>
#include <karhu/app/backend/implementation/graphics/GL/common.hpp>
#include <karhu/core/platform.hpp>
#include <karhu/core/log.hpp>

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace GL
			{
				bool Rendertarget::performBake()
				{
					clearErrorsGL();
					GLenum err;

					// Regenerate if necessary.
					if (0 == m_ID)
					{
						GLuint ID{0};
						glGenFramebuffers(1, &ID);
						if (0 == (m_ID = ID))
						{
							err = glGetError();
							
							log::err("Karhu")
								<< "Failed to generate rendertarget framebuffer: "
								<< errorToStringGL(err);
							
							return false;
						}
					}

					// Try to bind.
					glBindFramebuffer(GL_FRAMEBUFFER, m_ID);
					if (GL_NO_ERROR != (err = glGetError()))
					{
						log::err("Karhu")
							<< "Failed to bind rendertarget framebuffer "
							<< m_ID
							<< ": "
							<< errorToStringGL(err);
						
						return false;
					}

					// Try to attach the depth texture.
					if (depth())
					{
						const auto ID(*reinterpret_cast<const std::uint32_t *>(depth()->handle()));
						
						if (0 != ID)
						{/*
							glFramebufferRenderbuffer
							(
								GL_FRAMEBUFFER,
								GL_DEPTH_ATTACHMENT,
								GL_RENDERBUFFER,
								ID
							);*/
							// The 2D suffix is neccessary for GLES.
							glFramebufferTexture2D
							(
								GL_FRAMEBUFFER,
								GL_DEPTH_ATTACHMENT,
								GL_TEXTURE_2D, // 2D suffix requires this additional argument.
								ID,
								0
							);
							if (GL_NO_ERROR != (err = glGetError()))
							{
								log::err("Karhu")
									<< "Failed to set rendertarget framebuffer "
									<< m_ID
									<< " depthbuffer to "
									<< ID
									<< ": "
									<< errorToStringGL(err);
								
								bindDefault();
								
								return false;
							}
						}
					}

					// Try to attach the stencil texture.
					if (stencil())
					{
						const auto ID(*reinterpret_cast<const std::uint32_t *>(stencil()->handle()));
						
						if (0 != ID)
						{/*
							glFramebufferRenderbuffer
							(
								GL_FRAMEBUFFER,
								GL_DEPTH_STENCIL_ATTACHMENT,
								GL_RENDERBUFFER,
								ID
							);*/
							// The 2D suffix is neccessary for GLES.
							glFramebufferTexture2D
							(
								GL_FRAMEBUFFER,
								GL_DEPTH_STENCIL_ATTACHMENT,
								GL_TEXTURE_2D, // 2D suffix requires this additional argument.
								ID,
								0
							);
							if (GL_NO_ERROR != (err = glGetError()))
							{
								log::err("Karhu")
									<< "Failed to set rendertarget framebuffer "
									<< m_ID
									<< " stencilbuffer to "
									<< ID
									<< ": "
									<< errorToStringGL(err);
								
								bindDefault();
								
								return false;
							}
						}
					}

					// Try to attach the colour textures.
					std::vector<GLenum> buffers(textures().size());
					for (std::size_t i{0}; i < textures().size(); ++ i)
					{
						auto t(textures()[i]);

						const auto ID   (*reinterpret_cast<const std::uint32_t *>(t->handle()));
						const auto index(static_cast<GLenum>(GL_COLOR_ATTACHMENT0 + i));
						
						buffers[i] = index;

						if (0 != ID)
						{
							// The 2D suffix is neccessary for GLES.
							glFramebufferTexture2D
							(
								GL_FRAMEBUFFER,
								index,
								GL_TEXTURE_2D, // 2D suffix requires this additional argument.
								ID,
								0
							);
							
							if (GL_NO_ERROR != (err = glGetError()))
							{
								log::err("Karhu")
									<< "Failed to set rendertarget framebuffer "
									<< m_ID
									<< " texture to "
									<< ID
									<< " at index "
									<< static_cast<int>(i)
									<< ": "
									<< errorToStringGL(err);
								
								bindDefault();
								
								return false;
							}
						}
					}

					// Set up the draw buffers.
					
					glDrawBuffers(static_cast<GLsizei>(buffers.size()), &buffers[0]);
					
					if (GL_NO_ERROR != (err = glGetError()))
					{
						log::err("Karhu")
							<< "Failed to set rendertarget framebuffer "
							<< m_ID
							<< " drawbuffer: "
							<< errorToStringGL(err);
						
						bindDefault();
						
						return false;
					}

					if (!validateFramebuffer()) return false;
					
					// Print some information.
					/// @todo: Figure out how to do this on web.
					#ifndef KARHU_PLATFORM_WEB
//					{
//						GLint v{-1};
//						glGetFramebufferAttachmentParameteriv
//						(
//							GL_FRAMEBUFFER,
//							GL_DEPTH_STENCIL_ATTACHMENT,
//							GL_FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE,
//							&v
//						);
//
//						log::msg("Karhu")
//							<< "GL_FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE for FBO "
//							<< m_ID
//							<< ": "
//							<< v;
//					}
					#endif

					bindDefault();

					return true;
				}

				bool Rendertarget::performBind()
				{
					clearErrorsGL();
					GLenum err;

					// Try to bind.
					glBindFramebuffer(GL_FRAMEBUFFER, m_ID);
					if (GL_NO_ERROR != (err = glGetError()))
					{
						log::err("Karhu") << "Failed to bind rendertarget framebuffer " << m_ID;
						return false;
					}

					return true;
				}
				
				bool Rendertarget::performUnbind()
				{
					bindDefault();
					return true;
				}
				
				void Rendertarget::performClearTextureColour(std::size_t const index, gfx::ColRGBAf const &colour)
				{
					if (textures().size() > index)
					{
						auto const
							texture(textures()[index]);
						
						if
						(
							texture          &&
							texture->valid() &&
							Texture::Type::colour == texture->type()
						)
						{
							glClearBufferfv
							(
								GL_COLOR,
								static_cast<GLint>(index),
								const_cast<GLfloat *>(&colour.r)
							);
						}
					}
				}
				
				void Rendertarget::performClearTextureData(std::size_t const index, std::int32_t const data)
				{
					if (textures().size() > index)
					{
						auto const
							texture(textures()[index]);
						
						if
						(
							texture          &&
							texture->valid() &&
							Texture::Type::data == texture->type()
						)
						{
							auto const
								value(static_cast<GLint>(data));
							
							glClearBufferiv
							(
								GL_COLOR,
								static_cast<GLint>(index),
								&value
							);
						}
					}
				}
				
				bool Rendertarget::validateFramebuffer()
				{
					// Validate.
					const auto status(glCheckFramebufferStatus(GL_FRAMEBUFFER));
					if (status != GL_FRAMEBUFFER_COMPLETE)
					{
						log::err("Karhu")
							<< "Rendertarget framebuffer "
							<< m_ID
							<< " status is incomplete! Status: "
							<< status;
//							<< ", GL_FRAMEBUFFER_UNDEFINED = " << GL_FRAMEBUFFER_UNDEFINED
//							<< ", GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT = " << GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT
//							<< ", GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT = " << GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT
//							<< ", GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER = " << GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER
//							<< ", GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER = " << GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER
//							<< ", GL_FRAMEBUFFER_UNSUPPORTED = " << GL_FRAMEBUFFER_UNSUPPORTED
//							<< ", GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE = " << GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE
//							<< ", GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE = " << GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE
//							<< ", GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS = " << GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS;
						
						bindDefault();
						
						return false;
					}
					
					return true;
				}
				
				void Rendertarget::bindDefault()
				{
					glBindFramebuffer(GL_FRAMEBUFFER, Renderer::framebufferDefault());
				}
				
				Rendertarget::~Rendertarget()
				{
					if (0 != m_ID)
					{
						const GLuint ID(m_ID);
						glDeleteFramebuffers(1, &ID);
					}
				}
			}
		}
	}
}
