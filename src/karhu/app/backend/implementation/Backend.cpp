/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/backend/implementation/Backend.hpp>
#include <karhu/app/backend/implementation/window/SDL/setup.hpp>
#include <karhu/core/log.hpp>

namespace
{
	using namespace karhu;

	static Uint32 getIDWindow(win::Window &window)
	{
		return static_cast<Uint32>
		(
			SDL_GetWindowID
			(
				reinterpret_cast<SDL_Window *>(window.handle())
			)
		);
	}
	
	/// @todo: Feels bad that all this input related code and indeed the event polling for input should be here and not in the input subsystem so maybe try to fix that up in the future...
	
	static constexpr inp::Mouse mouseFromSDL(const Uint8 &value) noexcept
	{
		switch (value)
		{
			case SDL_BUTTON_LEFT:   return inp::Mouse::left;
			case SDL_BUTTON_RIGHT:  return inp::Mouse::right;
			case SDL_BUTTON_MIDDLE: return inp::Mouse::middle;

			default: return inp::Mouse::none;
		}
	}
	
	static constexpr inp::Key keyFromSDL(const SDL_Keycode &value) noexcept
	{
		switch (value)
		{
			case SDLK_LEFT:  return inp::Key::left;
			case SDLK_RIGHT: return inp::Key::right;
			case SDLK_UP:    return inp::Key::up;
			case SDLK_DOWN:  return inp::Key::down;
			
			case SDLK_0: return inp::Key::zero;
			case SDLK_1: return inp::Key::one;
			case SDLK_2: return inp::Key::two;
			case SDLK_3: return inp::Key::three;
			case SDLK_4: return inp::Key::four;
			case SDLK_5: return inp::Key::five;
			case SDLK_6: return inp::Key::six;
			case SDLK_7: return inp::Key::seven;
			case SDLK_8: return inp::Key::eight;
			case SDLK_9: return inp::Key::nine;
			
			case SDLK_a: return inp::Key::a;
			case SDLK_b: return inp::Key::b;
			case SDLK_c: return inp::Key::c;
			case SDLK_d: return inp::Key::d;
			case SDLK_e: return inp::Key::e;
			case SDLK_f: return inp::Key::f;
			case SDLK_g: return inp::Key::g;
			case SDLK_h: return inp::Key::h;
			case SDLK_i: return inp::Key::i;
			case SDLK_j: return inp::Key::j;
			case SDLK_k: return inp::Key::k;
			case SDLK_l: return inp::Key::l;
			case SDLK_m: return inp::Key::m;
			case SDLK_n: return inp::Key::n;
			case SDLK_o: return inp::Key::o;
			case SDLK_p: return inp::Key::p;
			case SDLK_q: return inp::Key::q;
			case SDLK_r: return inp::Key::r;
			case SDLK_s: return inp::Key::s;
			case SDLK_t: return inp::Key::t;
			case SDLK_u: return inp::Key::u;
			case SDLK_v: return inp::Key::v;
			case SDLK_w: return inp::Key::w;
			case SDLK_x: return inp::Key::x;
			case SDLK_y: return inp::Key::y;
			case SDLK_z: return inp::Key::z;

			case SDLK_SPACE:     return inp::Key::space;
			case SDLK_BACKSPACE: return inp::Key::backspace;
			case SDLK_RETURN:    return inp::Key::enter;
			case SDLK_ESCAPE:    return inp::Key::escape;

			case SDLK_KP_0:        return inp::Key::numZero;
			case SDLK_KP_1:        return inp::Key::numOne;
			case SDLK_KP_2:        return inp::Key::numTwo;
			case SDLK_KP_3:        return inp::Key::numThree;
			case SDLK_KP_4:        return inp::Key::numFour;
			case SDLK_KP_5:        return inp::Key::numFive;
			case SDLK_KP_6:        return inp::Key::numSix;
			case SDLK_KP_7:        return inp::Key::numSeven;
			case SDLK_KP_8:        return inp::Key::numEight;
			case SDLK_KP_9:        return inp::Key::numNine;
			case SDLK_KP_ENTER:    return inp::Key::numEnter;
			case SDLK_KP_EQUALS:   return inp::Key::numEquals;
			case SDLK_KP_PLUS:     return inp::Key::numAdd;
			case SDLK_KP_MINUS:    return inp::Key::numSubtract;
			case SDLK_KP_MULTIPLY: return inp::Key::numMultiply;
			case SDLK_KP_DIVIDE:   return inp::Key::numDivide;
			case SDLK_KP_DECIMAL:  return inp::Key::numDecimal;
			
			case SDLK_F1:  return inp::Key::f1;
			case SDLK_F2:  return inp::Key::f2;
			case SDLK_F3:  return inp::Key::f3;
			case SDLK_F4:  return inp::Key::f4;
			case SDLK_F5:  return inp::Key::f5;
			case SDLK_F6:  return inp::Key::f6;
			case SDLK_F7:  return inp::Key::f7;
			case SDLK_F8:  return inp::Key::f8;
			case SDLK_F9:  return inp::Key::f9;
			case SDLK_F10: return inp::Key::f10;
			case SDLK_F11: return inp::Key::f11;
			case SDLK_F12: return inp::Key::f12;
			
			case SDLK_LSHIFT: return inp::Key::shiftLeft;
			case SDLK_RSHIFT: return inp::Key::shiftRight;
			
			case SDLK_LALT: return inp::Key::altLeft;
			case SDLK_RALT: return inp::Key::altRight;
			
			case SDLK_LCTRL: return inp::Key::controlLeft;
			case SDLK_RCTRL: return inp::Key::controlRight;
			
			case SDLK_LGUI: return inp::Key::metaLeft;
			case SDLK_RGUI: return inp::Key::metaRight;
			
			default: return inp::Key::none;
		}
	}
	
	static constexpr inp::Button buttonFromSDL(const Uint8 &value) noexcept
	{
		switch (value)
		{
			case SDL_CONTROLLER_BUTTON_DPAD_LEFT:  return inp::Button::left;
			case SDL_CONTROLLER_BUTTON_DPAD_RIGHT: return inp::Button::right;
			case SDL_CONTROLLER_BUTTON_DPAD_UP:    return inp::Button::up;
			case SDL_CONTROLLER_BUTTON_DPAD_DOWN:  return inp::Button::down;
			
			case SDL_CONTROLLER_BUTTON_A: return inp::Button::a;
			case SDL_CONTROLLER_BUTTON_B: return inp::Button::b;
			case SDL_CONTROLLER_BUTTON_X: return inp::Button::x;
			case SDL_CONTROLLER_BUTTON_Y: return inp::Button::y;
			
			case SDL_CONTROLLER_BUTTON_BACK:  return inp::Button::select;
			case SDL_CONTROLLER_BUTTON_START: return inp::Button::start;
			case SDL_CONTROLLER_BUTTON_GUIDE: return inp::Button::guide;
			
			case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:  return inp::Button::shoulderLeft;
			case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER: return inp::Button::shoulderRight;
			
			case SDL_CONTROLLER_BUTTON_LEFTSTICK:  return inp::Button::stickLeft;
			case SDL_CONTROLLER_BUTTON_RIGHTSTICK: return inp::Button::stickRight;

			default: return inp::Button::none;
		}
	}
	
	static constexpr inp::Axis axisFromSDL(const Uint8 &value) noexcept
	{
		switch (value)
		{
			case SDL_CONTROLLER_AXIS_LEFTX:        return inp::Axis::xLeft;
			case SDL_CONTROLLER_AXIS_LEFTY:        return inp::Axis::yLeft;
			case SDL_CONTROLLER_AXIS_RIGHTX:       return inp::Axis::xRight;
			case SDL_CONTROLLER_AXIS_RIGHTY:       return inp::Axis::yRight;
			case SDL_CONTROLLER_AXIS_TRIGGERLEFT:  return inp::Axis::triggerLeft;
			case SDL_CONTROLLER_AXIS_TRIGGERRIGHT: return inp::Axis::triggerRight;
			
			default: return inp::Axis::none;
		}
	}
}

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			Backend::Backend()
			{
				#ifdef KARHU_PLATFORM_WEB
					c_instance = this;
				#endif
			}
			
			void Backend::performLogVersions()
			{
				SDL_version v;
				SDL_GetVersion(&v);
				log::msg("Karhu")
					<< "SDL "
					<< static_cast<int>(v.major)
					<< '.'
					<< static_cast<int>(v.minor)
					<< '.'
					<< static_cast<int>(v.patch);
			}

			bool Backend::performInit()
			{
				if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) /// @todo: Maybe SDL_INIT_AUDIO should be in InitSubsystem().
				{
					log::err("Karhu") << "Failed to initialise SDL: " << SDL_GetError();
					return false;
				}

				// Set up glew on the non-mobile platforms.
				#if defined(KARHU_PLATFORM_MAC) || defined(KARHU_PLATFORM_LINUX) || defined(KARHU_PLATFORM_WINDOWS)
					glewExperimental = GL_TRUE;
				#endif

				return true;
			}

			void Backend::performStartLoop()
			{
				#ifdef KARHU_PLATFORM_WEB
					emscripten_set_main_loop(wrapperLoop, 0, true);
				#else
					while (windows().open())
					{
						update();
						SDL_Delay(1); /// @todo: Figure out how to actually deal with framerate.
					}
				#endif
			}
			
			bool Backend::performBeginFrame(app::FuncImGuiEvent imguiEvent)
			{
				if (!windows().top())
					return false;
				
				bool r{false};
				
				Uint32
					flags{0};

				SDL_Event e;
				while (windows().open() && SDL_PollEvent(&e))
				{
					if (imguiEvent)
						imguiEvent(&e);

					switch (e.type)
					{
						case SDL_MOUSEMOTION:
						case SDL_MOUSEWHEEL:
							r = true;
							break;
							
						case SDL_WINDOWEVENT:
						{
							// Find the affected window.
							win::Window *w{nullptr};
							std::size_t i{0};
							for (; i < windows().count(); ++ i)
							{
								if (e.window.windowID == getIDWindow(*windows().get(i)))
								{
									w = windows().get(i);
									break;
								}
							}
							
							// Make sure we got a window.
							if (!w) break;

							// Figure out the exact event.
							switch (e.window.event)
							{
								case SDL_WINDOWEVENT_CLOSE:
								{
									// If the window closed we need to get rid of the wrapper.
									windows().close(i);
									break;
								}
								
								case SDL_WINDOWEVENT_RESIZED:
								{
									r = true;
									
									#ifdef KARHU_EDITOR
									shouldRepaintViewportEditor(true);
									#endif
									
									w->notifyChangeSize
									(
										static_cast<std::uint32_t>(e.window.data1),
										static_cast<std::uint32_t>(e.window.data2)
									);
									break;
								}
								
								default:
									break;
							}

							break;
						}

						case SDL_KEYDOWN:
						{
							r = true;
					
							const inp::Key value{keyFromSDL(e.key.keysym.sym)};
							
							if (inp::Key::none != value)
							{
								if (SDLK_LSHIFT == e.key.keysym.sym || SDLK_RSHIFT == e.key.keysym.sym)
									input().registerKeyDown(inp::Key::shift);
								else if (SDLK_LALT == e.key.keysym.sym || SDLK_RALT == e.key.keysym.sym)
									input().registerKeyDown(inp::Key::alt);
								else if (SDLK_LCTRL == e.key.keysym.sym || SDLK_RCTRL == e.key.keysym.sym)
									input().registerKeyDown(inp::Key::control);
								else if (SDLK_LGUI == e.key.keysym.sym || SDLK_RGUI == e.key.keysym.sym)
									input().registerKeyDown(inp::Key::meta);
								
								input().registerKeyDown(value);
							}

							break;
						}

						case SDL_KEYUP:
						{
							r = true;
					
							const inp::Key value{keyFromSDL(e.key.keysym.sym)};
							
							if (inp::Key::none != value)
							{
								if (SDLK_LSHIFT == e.key.keysym.sym || SDLK_RSHIFT == e.key.keysym.sym)
									input().registerKeyUp(inp::Key::shift);
								else if (SDLK_LALT == e.key.keysym.sym || SDLK_RALT == e.key.keysym.sym)
									input().registerKeyUp(inp::Key::alt);
								else if (SDLK_LCTRL == e.key.keysym.sym || SDLK_RCTRL == e.key.keysym.sym)
									input().registerKeyUp(inp::Key::control);
								else if (SDLK_LGUI == e.key.keysym.sym || SDLK_RGUI == e.key.keysym.sym)
									input().registerKeyUp(inp::Key::meta);

								input().registerKeyUp(value);
							}

							break;
						}
						
						case SDL_MOUSEBUTTONDOWN:
						{
							r = true;
					
							const inp::Mouse value{mouseFromSDL(e.button.button)};
							
							if (inp::Mouse::none != value)
								input().registerMouseDown(value);
							
							break;
						}
						
						case SDL_MOUSEBUTTONUP:
						{
							r = true;
					
							const inp::Mouse value{mouseFromSDL(e.button.button)};
							
							if (inp::Mouse::none != value)
								input().registerMouseUp(value);
							
							break;
						}
						
						case SDL_JOYDEVICEADDED:
						{
							r = true;
					
							log::msg("Karhu") << "Joy device detected";
							
							auto &&c(input().createController(&e.jdevice.which));
							
							input().registerController
							(
								static_cast<inp::IDController>(e.jdevice.which),
								std::move(c)
							);
							
							break;
						}

						case SDL_JOYDEVICEREMOVED:
						{
							r = true;
					
							input().destroyController(&e.jdevice.which);
							
							input().deregisterController
							(
								static_cast<inp::IDController>(e.jdevice.which)
							);
							
							break;
						}
						
						case SDL_CONTROLLERAXISMOTION:
						{
							r = true;
					
							for (auto &controller : input().controllers())
							{
								auto c(reinterpret_cast<SDL_GameController *>(controller.second->handle()));
								auto j(SDL_GameControllerGetJoystick(c));
								const auto ID(SDL_JoystickInstanceID(j));

								if (ID == e.caxis.which)
								{
									// SDL provides large integral values but we want floats between -1.0 and 1.0.
									const mth::Scalf value{mth::clamp
									(
										(mth::Scalf)e.caxis.value /
										(mth::Scalf)std::numeric_limits<Sint16>::max(),
										-1.0f,
										1.0f
									)};
									
//									log::msg("DEBUG") << "AXIS (" << controller.first << " : "
//									<< static_cast<int>(axisFromSDL(e.caxis.axis)) << ") = " << e.caxis.value << " / " << std::numeric_limits<Sint16>::max() << " = " << value;
									
									input().registerAxis
									(
										controller.first,
										axisFromSDL(e.caxis.axis),
									 	value
									);
									
									// Triggers are only provided as axes in SDL,
									// but Karhu wants them as regular buttons too.
									
									constexpr mth::Scalf epsilon{0.000001f};
									
									if (SDL_CONTROLLER_AXIS_TRIGGERLEFT == e.caxis.axis)
									{
										if (std::abs(value) > epsilon && !input().buttonHeld(controller.first, inp::Button::triggerLeft))
											input().registerButtonDown(controller.first, inp::Button::triggerLeft);
										else if (std::abs(value) < epsilon && input().buttonHeld(controller.first, inp::Button::triggerLeft))
											input().registerButtonUp(controller.first, inp::Button::triggerLeft);
									}
									else if (SDL_CONTROLLER_AXIS_TRIGGERRIGHT == e.caxis.axis)
									{
										if (std::abs(value) > epsilon && !input().buttonHeld(controller.first, inp::Button::triggerRight))
											input().registerButtonDown(controller.first, inp::Button::triggerRight);
										else if (std::abs(value) < epsilon && input().buttonHeld(controller.first, inp::Button::triggerRight))
											input().registerButtonUp(controller.first, inp::Button::triggerRight);
									}
									
									break;
								}
							}
							
							
							break;
						}

						case SDL_CONTROLLERBUTTONDOWN:
						{
							r = true;
					
							for (auto &controller : input().controllers())
							{
								auto c(reinterpret_cast<SDL_GameController *>(controller.second->handle()));
								auto j(SDL_GameControllerGetJoystick(c));
								const auto ID(SDL_JoystickInstanceID(j));

								if (ID == e.cbutton.which)
								{
									input().registerButtonDown(controller.first, buttonFromSDL(e.cbutton.button));
									break;
								}
							}
							
							break;
						}
						
						case SDL_CONTROLLERBUTTONUP:
						{
							r = true;
					
							for (auto &controller : input().controllers())
							{
								auto c(reinterpret_cast<SDL_GameController *>(controller.second->handle()));
								
								if (!c) continue;
								
								auto j(SDL_GameControllerGetJoystick(c));
								const auto ID(SDL_JoystickInstanceID(j));

								if (ID == e.cbutton.which)
								{
									input().registerButtonUp(controller.first, buttonFromSDL(e.cbutton.button));
									break;
								}
							}
							
							break;
						}
						
						case SDL_QUIT:
						{
							windows().close();
							break;
						}
					}
				}
				
				#ifdef KARHU_EDITOR
				if (r)
					shouldRepaintEditor(true);
				#endif
				
				return r;
			}

			void Backend::performEndFrame()
			{
			}
			
			Backend::~Backend()
			{
				#ifdef KARHU_PLATFORM_WEB
					c_instance = nullptr;
				#endif
				
				input().deinit();
				
				SDL_Quit();
			}
			
			#ifdef KARHU_PLATFORM_WEB
				Backend *Backend::c_instance{nullptr};
			#endif
		}
	}
}
