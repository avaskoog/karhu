/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/backend/implementation/window/SDL/SubsystemWindow.hpp>
#include <karhu/app/backend/implementation/window/SDL/Window.hpp>
#include <karhu/app/backend/implementation/window/SDL/Context.hpp>

#include <SDL2/SDL.h>

#ifdef _WIN32
	#include <SDL2/SDL_syswm.h>
#endif

#ifdef KARHU_PLATFORM_IOS
	#import <UIKit/UIKit.h>
#endif

// All of the ImGui code below has been borrowed from the
// SDL/GL example; apologies if it is a bit unrefined.
/// @todo: Fix the formatting up a bit.
namespace
{
	static double g_Time{0.0};
	static bool   g_MousePressed[3]{false, false, false};
	static float  g_MouseWheel{0.0f};

	static const char* imguiGetClipboardText(void *)
	{
		return SDL_GetClipboardText();
	}

	static void imguiSetClipboardText(void *, char const *text)
	{
		SDL_SetClipboardText(text);
	}

	static bool imguiProcessEvent(void *event)
	{
		ImGuiIO& io = ImGui::GetIO();
		
		auto e(reinterpret_cast<SDL_Event *>(event));
		
		switch (e->type)
		{
			case SDL_MOUSEWHEEL:
			{
				if (e->wheel.y > 0)
					g_MouseWheel = 1;
				if (e->wheel.y < 0)
					g_MouseWheel = -1;
				
				return true;
			}
			
			case SDL_MOUSEBUTTONDOWN:
			{
				if (e->button.button == SDL_BUTTON_LEFT)   g_MousePressed[0] = true;
				if (e->button.button == SDL_BUTTON_RIGHT)  g_MousePressed[1] = true;
				if (e->button.button == SDL_BUTTON_MIDDLE) g_MousePressed[2] = true;
				
				return true;
			}
			
			case SDL_TEXTINPUT:
			{
				io.AddInputCharactersUTF8(e->text.text);
				return true;
			}
			
			case SDL_KEYDOWN:
			case SDL_KEYUP:
			{
				int key = e->key.keysym.sym & ~SDLK_SCANCODE_MASK;
				
				io.KeysDown[key] = (e->type == SDL_KEYDOWN);
				io.KeyShift      = ((SDL_GetModState() & KMOD_SHIFT) != 0);
				io.KeyCtrl       = ((SDL_GetModState() & KMOD_CTRL)  != 0);
				io.KeyAlt        = ((SDL_GetModState() & KMOD_ALT)   != 0);
				io.KeySuper      = ((SDL_GetModState() & KMOD_GUI)   != 0);
				
				return true;
			}
		}
		
		return false;
	}

	static bool imguiInit(void *win)
	{
		auto window(reinterpret_cast<SDL_Window *>(win));

		ImGuiIO& IO = ImGui::GetIO();
		
		// Keyboard mapping.
		// ImGui will use these indices to peek into the IO.KeyDown[] array.
		IO.KeyMap[ImGuiKey_Tab] = SDLK_TAB;
		IO.KeyMap[ImGuiKey_LeftArrow] = SDL_SCANCODE_LEFT;
		IO.KeyMap[ImGuiKey_RightArrow] = SDL_SCANCODE_RIGHT;
		IO.KeyMap[ImGuiKey_UpArrow] = SDL_SCANCODE_UP;
		IO.KeyMap[ImGuiKey_DownArrow] = SDL_SCANCODE_DOWN;
		IO.KeyMap[ImGuiKey_PageUp] = SDL_SCANCODE_PAGEUP;
		IO.KeyMap[ImGuiKey_PageDown] = SDL_SCANCODE_PAGEDOWN;
		IO.KeyMap[ImGuiKey_Home] = SDL_SCANCODE_HOME;
		IO.KeyMap[ImGuiKey_End] = SDL_SCANCODE_END;
		IO.KeyMap[ImGuiKey_Delete] = SDLK_DELETE;
		IO.KeyMap[ImGuiKey_Backspace] = SDLK_BACKSPACE;
		IO.KeyMap[ImGuiKey_Enter] = SDLK_RETURN;
		IO.KeyMap[ImGuiKey_Escape] = SDLK_ESCAPE;
		IO.KeyMap[ImGuiKey_A] = SDLK_a;
		IO.KeyMap[ImGuiKey_C] = SDLK_c;
		IO.KeyMap[ImGuiKey_V] = SDLK_v;
		IO.KeyMap[ImGuiKey_X] = SDLK_x;
		IO.KeyMap[ImGuiKey_Y] = SDLK_y;
		IO.KeyMap[ImGuiKey_Z] = SDLK_z;
		
		return true;
	}

	static void imguiShutdown()
	{
	}

	static void imguiNewFrame(void *win)
	{
		auto window(reinterpret_cast<SDL_Window *>(win));

		ImGuiIO& io = ImGui::GetIO();

		// Setup display size (every frame to accommodate for window resizing)
		int w, h;
		int display_w, display_h;
		SDL_GetWindowSize(window, &w, &h);
		SDL_GL_GetDrawableSize(window, &display_w, &display_h);
		
		// These lines were added by Ava to make it render properly on high DPI iOS.
		const float scl_w((float)display_w / (float)w), scl_h((float)display_h / (float)h);
		
		w = display_w;
		h = display_h;
		
		io.DisplaySize = ImVec2
		{
			(float)w / scl_w,
			(float)h / scl_h
		};
		
		io.DisplayFramebufferScale = ImVec2((float)scl_w, (float)scl_h);

		// Setup time step
		Uint32	time = SDL_GetTicks();
		double current_time = time / 1000.0;
		io.DeltaTime = g_Time > 0.0 ? (float)(current_time - g_Time) : (float)(1.0f / 60.0f);
		g_Time = current_time;

		// Setup inputs
		// (we already got mouse wheel, keyboard keys & characters from SDL_PollEvent())
		int mx, my;
		Uint32 mouseMask = SDL_GetMouseState(&mx, &my);
		
//		if (SDL_GetWindowFlags(window) & SDL_WINDOW_MOUSE_FOCUS)
			io.MousePos = ImVec2((float)mx, (float)my);   // Mouse position, in pixels (set to -1,-1 if no mouse / on another screen, etc.)
			// Multiplication by scl_w/_h added by Ava to make it work properly on high DPI iOS.
//			io.MousePos = ImVec2((float)mx * (float)scl_w, (float)my * (float)scl_h);   // Mouse position, in pixels (set to -1,-1 if no mouse / on another screen, etc.)
//		else
//			io.MousePos = ImVec2(-1, -1);
		
		io.MouseDown[0] = g_MousePressed[0] || (mouseMask & SDL_BUTTON(SDL_BUTTON_LEFT)) != 0;		// If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
		io.MouseDown[1] = g_MousePressed[1] || (mouseMask & SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0;
		io.MouseDown[2] = g_MousePressed[2] || (mouseMask & SDL_BUTTON(SDL_BUTTON_MIDDLE)) != 0;
		g_MousePressed[0] = g_MousePressed[1] = g_MousePressed[2] = false;

		io.MouseWheel = g_MouseWheel;
		g_MouseWheel = 0.0f;
		
		// Added by Ava to track mouse outside window when dragging.
		SDL_CaptureMouse
		(
			(
				ImGui::IsMouseDragging(ImGuiMouseButton_Left)   ||
				ImGui::IsMouseDragging(ImGuiMouseButton_Middle) ||
				ImGui::IsMouseDragging(ImGuiMouseButton_Right)
			)
			?
				SDL_TRUE
			:
				SDL_FALSE
		);

		// Hide OS mouse cursor if ImGui is drawing it
		SDL_ShowCursor(io.MouseDrawCursor ? 0 : 1);
	}
}

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace SDL
			{
				std::unique_ptr<win::Window> SubsystemWindow::performCreateWindow()
				{
					return std::make_unique<Window>();
				}
				
				std::unique_ptr<win::Context> SubsystemWindow::performCreateContext()
				{
					return std::make_unique<Context>();
				}

				void SubsystemWindow::performBeginFrame()
				{
				}

				void SubsystemWindow::performEndFrame()
				{
				}
				
				app::FuncImGuiInit SubsystemWindow::funcImGuiInit()
				{
					return imguiInit;
				}
				
				app::FuncImGuiFrame SubsystemWindow::funcImGuiFrame()
				{
					return imguiNewFrame;
				}
				
				app::FuncImGuiEvent SubsystemWindow::funcImGuiEvent()
				{
					return imguiProcessEvent;
				}
				
				app::FuncImGuiClipboardGet SubsystemWindow::funcImGuiClipboardGet()
				{
					return imguiGetClipboardText;
				}
				
				app::FuncImGuiClipboardSet SubsystemWindow::funcImGuiClipboardSet()
				{
					return imguiSetClipboardText;
				}
				
				app::FuncImGuiDeinit SubsystemWindow::funcImGuiDeinit()
				{
					return imguiShutdown;
				}
			}
		}
	}
}
