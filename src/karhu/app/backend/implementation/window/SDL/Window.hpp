/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_WINDOW_SDL_WINDOW_H_
	#define KARHU_APP_WINDOW_SDL_WINDOW_H_

	#include <karhu/app/win/Window.hpp>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace SDL
				{
					class Window : public win::Window
					{
						public:
							Window();

							void *handle() const noexcept override;
						
							~Window();
							
						protected:
							bool performInit
							(
								std::string   const &caption,
								std::uint32_t const  width,
								std::uint32_t const  height,
								win::Flags    const  flags
							) override;
						
							void performBeginFrame() override;
							void performEndFrame() override;
						
							void onChangeSize(std::uint32_t const, std::uint32_t const) override;

						private:
							// We do not want to expose SDL in the header and force users
							// of the library to have to set up include directories for SDL,
							// so the data is defined in the implementation file instead.
							struct Data;
							std::unique_ptr<Data> m_data;
					};
				}
			}
		}
	}
#endif
