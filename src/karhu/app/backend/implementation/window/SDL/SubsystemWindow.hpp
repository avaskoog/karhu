/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_SUBSYSTEMS_WINDOW_SDL_SUBSYSTEM_WINDOW_H_
	#define KARHU_APP_BACKEND_SUBSYSTEMS_WINDOW_SDL_SUBSYSTEM_WINDOW_H_

	#include <karhu/app/backend/implementation/window/SDL/Window.hpp>
	#include <karhu/app/backend/implementation/window/SDL/Context.hpp>
	#include <karhu/app/backend/implementation/graphics/GL/SubsystemGraphics.hpp>
	#include <karhu/app/win/SubsystemWindow.hpp>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace SDL
				{
					class SubsystemWindow : public win::SubsystemWindow
					{
						public:
							app::FuncImGuiInit         funcImGuiInit()         override;
							app::FuncImGuiFrame        funcImGuiFrame()        override;
							app::FuncImGuiEvent        funcImGuiEvent()        override;
							app::FuncImGuiClipboardGet funcImGuiClipboardGet() override;
							app::FuncImGuiClipboardSet funcImGuiClipboardSet() override;
							app::FuncImGuiDeinit       funcImGuiDeinit()       override;

						public:
							void logVersions() override {}
						
						protected:
							std::unique_ptr<win::Window>  performCreateWindow()  override;
							std::unique_ptr<win::Context> performCreateContext() override;

							void performBeginFrame() override;
							void performEndFrame()   override;
					};
				}
			}
		}
	}
#endif
