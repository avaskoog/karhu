/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_WINDOW_SDL_SETUP_H_
	#define KARHU_APP_BACKEND_WINDOW_SDL_SETUP_H_

	#include <karhu/core/platform.hpp>

	#if !defined(KARHU_PLATFORM_ANDROID) && !defined(KARHU_PLATFORM_IOS) && !defined(KARHU_PLATFORM_WEB)
		#include <GL/glew.h>
	#endif

	#include <SDL2/SDL.h>

	#ifndef KARHU_PLATFORM_ANDROID
		#include <SDL2/SDL_opengl.h>
	#endif

	#ifdef KARHU_PLATFORM_WEB
		#include <emscripten/emscripten.h>
	#endif

	#ifdef KARHU_PLATFORM_IOS
		#import <UIKit/UIKit.h>
	#endif
#endif
