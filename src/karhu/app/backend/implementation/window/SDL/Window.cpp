/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/backend/implementation/window/SDL/Window.hpp>
#include <karhu/app/backend/implementation/window/SDL/Context.hpp>
#include <karhu/app/backend/implementation/graphics/GL/SubsystemGraphics.hpp>
#include <karhu/app/backend/implementation/window/SDL/setup.hpp>
#include <karhu/core/log.hpp>
#include <karhu/core/platform.hpp>

#include <karhu/app/App.hpp>
#include <karhu/app/edt/support.hpp>

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace SDL
			{
				/// @todo: Call SDL_GL_SetCurrent() to set the context to the active window before rendering to it.

				struct Window::Data
				{
					SDL_Window *handle{nullptr};
				};
				
				Window::Window()
				:
				m_data{std::make_unique<Data>()}
				{
				}
				
				void *Window::handle() const noexcept
				{
					return m_data->handle;
				}

				bool Window::performInit
				(
					std::string   const &caption,
					std::uint32_t const  width,
					std::uint32_t const  height,
					win::Flags    const  flags
				)
				{
					// We only support OpenGL for now.
					if (static_cast<bool>(flags & win::Flags::graphicsOpenGL))
					{
						// Different GL versions depending on platform.
						
						if (0 > SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24))
							log::err("Karhu") << "Failed to set OpenGL depth size to 24: " << SDL_GetError();
						
						#if defined(KARHU_PLATFORM_IOS) || defined(KARHU_PLATFORM_ANDROID) || defined(KARHU_PLATFORM_WEB)
							if (0 > SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES))
							{
								log::err("Karhu") << "Failed to set OpenGL context profile mask to ES: " << SDL_GetError();
								return false;
							}
						#else
							if (0 > SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE))
							{
								log::err("Karhu") << "Failed to set OpenGL context profile mask to core: " << SDL_GetError();
								return false;
							}
						#endif
						
						{
							// Android (or possibly SDL <= 2.0.4) is weird and setting 3.0
							// doesn't work, but setting 2.0 does, and still creates a 3.0
							// context if available, so leave this as is.
							#if defined(KARHU_PLATFORM_WEB) || defined(KARHU_PLATFORM_ANDROID)
								constexpr int major{2}, minor{0};
							#elif defined(KARHU_PLATFORM_IOS)
								constexpr int major{3}, minor{0};
							#else
								constexpr int major{3}, minor{2};
							#endif
							
							// For Wine.
							SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);

							if (0 > SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major))
							{
								log::err("Karhu") << "Failed to set OpenGL major version to " << major << ": " << SDL_GetError();
								return false;
							}
							
							if (0 > SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minor))
							{
								log::err("Karhu") << "Failed to set OpenGL minor version to " << minor << ": " << SDL_GetError();
								return false;
							}
						}

						{
							constexpr int bits{8};
							if (0 > SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, bits))
							{
								log::err("Karhu") << "Failed to set OpenGL stencil buffer bits to " << bits << ": " << SDL_GetError();
								return false;
							}
						}
						
						// We need to do some system-dependent figuring out of size.
						{
							#ifdef KARHU_PLATFORM_IOS
								CGRect screenRect = [[UIScreen mainScreen] bounds];
							
								m_width  = static_cast<std::uint32_t>(screenRect.size.width  * [UIScreen mainScreen].scale);
								m_height = static_cast<std::uint32_t>(screenRect.size.height * [UIScreen mainScreen].scale);
							
								log::msg("Karhu") << "Got viewport size " << m_width << 'x' << m_height << " with scale " << [UIScreen mainScreen].scale << '.';
							
								scale
								(
									static_cast<float>([UIScreen mainScreen].scale),
									static_cast<float>([UIScreen mainScreen].scale)
								);
							#elif defined(KARHU_PLATFORM_ANDROID)
								SDL_Rect rect;
								SDL_GetDisplayBounds(0, &rect);
							
								m_width  = static_cast<std::uint32_t>(rect.w);
								m_height = static_cast<std::uint32_t>(rect.h);
							
								log::msg("Karhu") << "Got viewport size " << m_width << 'x' << m_height << '.';
							
								/// @todo: Android screen scale?
							#else
								m_width  = width;
								m_height = height;
							#endif
						}
						
						Uint32
							flagsSDL{SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN};
						
						auto const
							highDPI(static_cast<bool>(flags & win::Flags::highDPI));
						
						if (highDPI)
							flagsSDL |= SDL_WINDOW_ALLOW_HIGHDPI;
						
						#if defined(KARHU_PLATFORM_IOS) || defined(KARHU_PLATFORM_ANDROID)
							flagsSDL |= SDL_WINDOW_FULLSCREEN | SDL_WINDOW_BORDERLESS;
						#elif !defined(KARHU_PLATFORM_WEB)
						 	/// @todo: Figure out if resize and fullscreen make sense on web.
						
							if (static_cast<bool>(flags & win::Flags::resizable))
								flagsSDL |= SDL_WINDOW_RESIZABLE;
						
							if (static_cast<bool>(flags & win::Flags::fullscreen))
								flagsSDL |= SDL_WINDOW_FULLSCREEN;
							else if (static_cast<bool>(flags & win::Flags::fillscreen))
								flagsSDL |= SDL_WINDOW_MAXIMIZED;
						#endif

						m_data->handle = SDL_CreateWindow
						(
							caption.c_str(),
						 
							SDL_WINDOWPOS_CENTERED,
							SDL_WINDOWPOS_CENTERED,
						 
							static_cast<int>(m_width),
							static_cast<int>(m_height),
						 
							flagsSDL
						);
						
						/// @note: If we want macOS high DPI support in the future, the scale seems to be [[NSScreen mainScreen] backingScaleFactor] but what we have here might work?
						#if defined(KARHU_PLATFORM_MAC) || defined(KARHU_PLATFORM_LINUX) || defined(KARHU_PLATFORM_WINDOWS)
						if (highDPI)
						{
							int
								w, h, dw, dh;
		
							SDL_GetWindowSize     (m_data->handle, &w, &h);
							SDL_GL_GetDrawableSize(m_data->handle, &dw, &dh);
							
							log::msg("Karhu") << "Window size: " << w << 'x' << h;
							log::msg("Karhu") << "Drawable size: " << dw << 'x' << dh;
							
							float const
								sw((float)dw / (float)w),
								sh((float)dh / (float)h);
							
							scale(sw, sh);
						}
						#endif
						
						if (!m_data->handle)
						{
							log::err("Karhu") << "Failed to create window: " << SDL_GetError();
							return false;
						}
						
						// Get the final size since after the window creation it might differ.
						/*{
							int w, h;
							SDL_GetWindowSize(m_data->handle, &w, &h);
							log::msg("Karhu") << "Final window size: " << w << 'x' << h;
							m_width = static_cast<std::uint32_t>(w);
							m_height = static_cast<std::uint32_t>(h);
						}
						
						{
							int w, h;
							SDL_GL_GetDrawableSize(m_data->handle, &w, &h);
							log::msg("Karhu") << "Drawable size: " << w << 'x' << h;
						}*/
					}

					return true;
				}
				
				void Window::performBeginFrame()
				{
					if (context())
						SDL_GL_MakeCurrent
						(
							m_data->handle,
							reinterpret_cast<SDL_GLContext>(context()->handle())
						);
				}
				
				void Window::performEndFrame()
				{
					// Apparently we have to set the framebuffer back to 0 on Mac.
					// See: https://wiki.libsdl.org/SDL_GL_SwapWindow
					#ifdef KARHU_PLATFORM_MAC
						/// @todo: Should we have GL code directly here though..?
						glBindFramebuffer(GL_FRAMEBUFFER, 0); // or Renderer::framebufferDefault()?
					#endif
					
					SDL_GL_SwapWindow(m_data->handle);
				}

				void Window::onChangeSize(std::uint32_t const, std::uint32_t const )
				{
					/// @todo: Update anything that needs updating when window size changes.
				}
				
				Window::~Window()
				{
					if (m_data->handle)
						SDL_DestroyWindow(m_data->handle);
				}
			}
		}
	}
}
