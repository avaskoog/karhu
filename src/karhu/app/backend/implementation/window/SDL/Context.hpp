/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_WINDOW_SDL_CONTEXT_H_
	#define KARHU_APP_WINDOW_SDL_CONTEXT_H_

	#include <karhu/app/win/Context.hpp>

	#include <memory>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace SDL
				{
					class Context : public win::Context
					{
						public:
							Context();

							void *handle() const override;

							~Context();

						protected:
							bool performInit(const win::Window &window) override;
						
						private:
							static Context *c_current;
						
						private:
							// We do not want to expose SDL in the header and force users
							// of the library to have to set up include directories for SDL,
							// so the data is defined in the implementation file instead.
							struct Data;
							std::unique_ptr<Data> m_data;
					};
				}
			}
		}
	}
#endif
