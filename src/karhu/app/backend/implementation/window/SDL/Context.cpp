/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/backend/implementation/window/SDL/Context.hpp>
#include <karhu/app/backend/implementation/window/SDL/setup.hpp>
#include <karhu/app/win/Window.hpp>
#include <karhu/core/log.hpp>
#include <karhu/core/platform.hpp>

#ifdef KARHU_PLATFORM_WEB
	#include <emscripten/html5.h>
#endif

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace SDL
			{
				struct Context::Data
				{
					SDL_GLContext handle{nullptr};
				};

				Context::Context()
				:
				m_data{std::make_unique<Data>()}
				{
				}
				
				void *Context::handle() const
				{
					return m_data->handle;
				}

				bool Context::performInit(const win::Window &window)
				{
					if (!static_cast<bool>(win::Flags::graphicsOpenGL & window.flags()))
					{
						log::err("Karhu") << "Failed to create context: the only supported graphics option is 'GL'";
						return false;
					}
					
					// There can only be one underlying context at a time.
					/// @todo: Do we have to deal with contexts disappearing in weird order, or reassign? This probably always ends up in the main window, which should always be the last to go, so it SHOULD be fine like this.
					if (c_current)
					{
						m_data->handle = c_current->m_data->handle;
						return true;
					}

					#ifndef KARHU_PLATFORM_WEB
						m_data->handle = SDL_GL_CreateContext(reinterpret_cast<SDL_Window *>(window.handle()));
					// On web we need to set a couple of things up.
					#else
					{
						EmscriptenWebGLContextAttributes as;
						
						emscripten_webgl_init_context_attributes(&as);
						
						// Function above prepares for 1.0, so we change to 2.0.
						as.majorVersion = 2;
						as.minorVersion = 0;
						
						// We want a stencil buffer.
						as.stencil = true;
						
						// We do not want a transparent HTML canvas.
						as.alpha = false;
						
						// We do not want anti-aliasing since this is not used on non-web platforms.
						as.antialias = false;
						
						// We create the context using Emscripten instead of SDL.
						EMSCRIPTEN_WEBGL_CONTEXT_HANDLE ch{emscripten_webgl_create_context(0, &as)};
						
						if (ch > 0 && EMSCRIPTEN_RESULT_SUCCESS == emscripten_webgl_make_context_current(ch))
							m_data->handle = reinterpret_cast<void *>(ch);
						else
							m_data->handle = nullptr;
					}
					#endif
					
					if (!m_data->handle)
					{
						log::err("Karhu") << "Failed to create OpenGL context: " << SDL_GetError();
						return false;
					}

					// Try to set vsync.
					/// @todo: Vsync settings should be loaded from app.
					#ifndef KARHU_GLES
						if (SDL_GL_SetSwapInterval(0) == 0)
							log::msg("Karhu") << "Disabled vsync";
						else if (SDL_GL_SetSwapInterval(-1) == 0)
							log::msg("Karhu") << "Enabled adaptive vsync";
						else if (SDL_GL_SetSwapInterval(1) == 0)
							log::msg("Karhu") << "Enabled vsync";
						else
							log::msg("Karhu") << "Failed to enable vsync";
							
						log::msg("DEBUG") << "Window swap interval: " << SDL_GL_GetSwapInterval();
					#endif
					
					int ds;
					SDL_GL_GetAttribute(SDL_GL_DEPTH_SIZE, &ds);
					
					log::dbg("Karhu") << "Initialised SDL OpenGL context with depth size " << ds;

					return true;
				}
				
				Context::~Context()
				{
					if (this == c_current)
					{
						c_current = nullptr;
						if (m_data->handle) SDL_GL_DeleteContext(m_data->handle);
					}
				}

				Context *Context::c_current{nullptr};
			}
		}
	}
}

#undef karhuPRINT_INFO_OPENGLi
