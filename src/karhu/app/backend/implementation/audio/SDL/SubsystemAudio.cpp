/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/backend/implementation/audio/SDL/SubsystemAudio.hpp>
#include <karhu/core/log.hpp>

#include <karhu/app/backend/implementation/audio/SDL/lib/soloud/soloud.h>

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace SDL
			{
				void SubsystemAudio::logVersions()
				{
					// SoLoud does not provide a useful version string, so we have to hard-code it.
					/// @todo: Update version string if SoLoud gets updated.
					log::msg("Karhu") << "SoLoud 2018.11.19";
				}
				
				bool SubsystemAudio::performInit(app::App &)
				{
					return true;
				}
				
				bool SubsystemAudio::performDeinit(app::App &)
				{
					return true;
				}
			}
		}
	}
}
