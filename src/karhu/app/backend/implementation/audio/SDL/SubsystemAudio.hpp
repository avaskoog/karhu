/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_AUDIO_SDL_SUBSYSTEM_AUDIO_H_
	#define KARHU_APP_BACKEND_AUDIO_SDL_SUBSYSTEM_AUDIO_H_

	#include <karhu/app/snd/SubsystemAudio.hpp>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace SDL
				{
					class SubsystemAudio : public snd::SubsystemAudio
					{
						public:
							void logVersions() override;
							const char *name() const override { return "SDL"; }
						
						protected:
							bool performInit(app::App &) override;
							bool performDeinit(app::App &) override;
					};
				}
			}
		}
	}
#endif
