/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_FILE_SDL_SUBSYSTEM_FILE_H_
	#define KARHU_APP_BACKEND_FILE_SDL_SUBSYSTEM_FILE_H_

	#include <karhu/app/fio/SubsystemFile.hpp>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace SDL
				{
					class SubsystemFile : public fio::SubsystemFile
					{
						protected:
							std::string pathForResources() const override;
							std::string pathForPreferences(const char *ext, const char *org, const char *app) const override;
						
							Nullable<std::string>  performReadStringFromResource(const char *filepath) override;
							Nullable<Buffer<Byte>> performReadBytesFromResource (const char *filepath) override;
					};
				}
			}
		}
	}
#endif
