/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/backend/implementation/file/SDL/SubsystemFile.hpp>
#include <karhu/core/platform.hpp>
#include <karhu/core/log.hpp>

#include <SDL2/SDL.h>
#include <SDL2/SDL_rwops.h>

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace SDL
			{
				std::string SubsystemFile::pathForResources() const
				{
					#ifdef KARHU_PLATFORM_ANDROID
						return "";
					#else
						auto p(SDL_GetBasePath());
						std::string r{p};
						SDL_free(p);
						return r;
					#endif
				}

				std::string SubsystemFile::pathForPreferences(const char *ext, const char *org, const char *proj) const
				{
					// Assemble the combined extension and organisation string that SDL wants.
					std::string full{ext};
					full += '.';
					full += org;
					//SDL_AndroidGetInternalStoragePath();
					
					auto p(SDL_GetPrefPath(full.c_str(), proj));
					std::string r{p};
					SDL_free(p);
					return r;
				}
				
				Nullable<std::string> SubsystemFile::performReadStringFromResource(const char *filepath)
				{
					if (SDL_RWops *file = SDL_RWFromFile(filepath, "r"))
					{
						const auto size(static_cast<std::size_t>(SDL_RWsize(file)));

						if (size > 0)
						{
							std::unique_ptr<char[]> buffer{new char[static_cast<std::size_t>(size + 1)]};
							const std::size_t result{SDL_RWread(file, buffer.get(), 1, size)};
							
							SDL_RWclose(file);
							
							if (result == size)
							{
								buffer[static_cast<std::size_t>(size)] = '\0';
								return {true, buffer.get()};
							}
							else
								log::err("Karhu") << "Error reading file to string '" << filepath << "': " << SDL_GetError();
						}
					}
					else
						log::err("Karhu") << "Error opening file for reading to string '" << filepath << "': " << SDL_GetError();
					
					return {};
				}
				
				Nullable<Buffer<Byte>> SubsystemFile::performReadBytesFromResource(const char *filepath)
				{
					if (SDL_RWops *file = SDL_RWFromFile(filepath, "rb"))
					{
						const auto size(static_cast<std::size_t>(SDL_RWsize(file)));

						if (size > 0)
						{
							auto buffer(std::make_unique<Byte[]>(size));
							const std::size_t result{SDL_RWread(file, buffer.get(), 1, size)};
							
							SDL_RWclose(file);
							
							if (result == size)
							{
								return {true, Buffer<Byte>{std::move(buffer), size}};
							}
							else
								log::err("Karhu") << "Error reading file to bytes '" << filepath << "': " << SDL_GetError();
						}
					}
					else
						log::err("Karhu") << "Error opening file for reading to bytes '" << filepath << "': " << SDL_GetError();
					
					return {};
				}
			}
		}
	}
}
