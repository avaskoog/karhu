/**
 * @author		Ava Skoog
 * @date		2017-09-18
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/backend/implementation/module/SDL/Library.hpp>
#include <karhu/app/module/Pool.hpp>
#include <karhu/core/log.hpp>

#include <SDL2/SDL_loadso.h>

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace SDL
			{
				bool Library::performLoad(const std::string &path)
				{
					m_object = SDL_LoadObject(path.c_str()); /// @todo: Fix cross-platform path?
					
					if (!m_object)
					{
						log::err("Karhu")
							<< "Loading dynamic library '" << path
							<< "' failed: " << SDL_GetError();
						return false;
					}
					
					return true;
				}
				
				void Library::performUnload()
				{
					if (m_object)
					{
						SDL_UnloadObject(m_object);
						m_object = nullptr;
					}
				}

				void *Library::performGetFunction(const std::string &function)
				{
					if (!m_object) return nullptr;
					
					if (auto f = SDL_LoadFunction(m_object, function.c_str()))
						return f;
					else
					{
						log::err("Karhu")
							<< "Loading dynamic library function '" << function
							<< "' failed: " << SDL_GetError();
						return nullptr;
					}
				}
				
				Library::~Library()
				{
					if (m_object) SDL_UnloadObject(m_object);
				}
			}
		}
	}
}
