/**
 * @author		Ava Skoog
 * @date		2017-09-18
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_SUBSYSTEMS_MODULE_SDL_SUBSYSTEM_LIBRARY_H_
	#define KARHU_APP_BACKEND_SUBSYSTEMS_MODULE_SDL_SUBSYSTEM_LIBRARY_H_

	#include <karhu/app/module/Library.hpp>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace SDL
				{
					class Library : public module::Library
					{
						public:
							~Library();

						protected:
							bool performLoad(const std::string &path) override;
							void performUnload() override;
							void *performGetFunction(const std::string &function) override;

						private:
							void *m_object{nullptr};
					};
				}
			}
		}
	}
#endif
