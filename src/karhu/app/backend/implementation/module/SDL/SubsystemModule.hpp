/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_MODULE_SDL_SUBSYSTEM_MODULE_H_
	#define KARHU_APP_BACKEND_MODULE_SDL_SUBSYSTEM_MODULE_H_

	#include <karhu/app/module/SubsystemModule.hpp>

	namespace karhu
	{
		namespace app
		{
			namespace implementation
			{
				namespace SDL
				{
					class SubsystemModule : public module::SubsystemModule
					{
						protected:
							std::unique_ptr<module::Library> performCreateLibrary() override;
					};
				}
			}
		}
	}
#endif
