/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/backend/implementation/module/SDL/SubsystemModule.hpp>
#include <karhu/app/backend/implementation/module/SDL/Library.hpp>

namespace karhu
{
	namespace app
	{
		namespace implementation
		{
			namespace SDL
			{
				std::unique_ptr<module::Library> SubsystemModule::performCreateLibrary()
				{
					return std::make_unique<Library>();
				}
			}
		}
	}
}
