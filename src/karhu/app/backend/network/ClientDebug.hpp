/**
 * @author		Ava Skoog
 * @date		2017-07-13
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_NETWORK_CLIENT_DEBUG_H_
	#define KARHU_APP_NETWORK_CLIENT_DEBUG_H_

	#include <karhu/network/Interface.hpp>
	#include <karhu/core/log.hpp>

	#include <iostream>

	namespace karhu
	{
		namespace app
		{
			namespace network
			{
				/**
				 * This is the client that gets compiled into the debug build
				 * of a Karhu application so that the debug server and other
				 * tools such as an editor can talk to it and receive its logs.
				 */
				template<class TClient>
				class ClientDebug : public TClient
				{	
					public:
						using TString = std::basic_string<conv::Char>;
						using TMessage = typename TClient::TMessage;
						using CallbackOnReceive = std::function<bool(const TMessage &message)>;

					private:
						class Logger : public log::Implementation
						{
							public:
								Logger(ClientDebug &parent) : m_parent{parent} {}

								bool log
								(
									const log::Level   level,
									const std::string &tag,
									const std::string &message
								) override;

							private:
								ClientDebug &m_parent;
						};
					
					public:
						ClientDebug();
						void attachLogger();
						void callbackOnReceive(const CallbackOnReceive &callback) { m_callbackOnReceive = callback; }
						~ClientDebug();

					protected:
						bool updateAfterMessages() override;
						bool receive(const TMessage &message) override;
					
					private:
						Logger m_logger;
						CallbackOnReceive m_callbackOnReceive;
					
					friend class Logger;
				};

				template<class TClient>
				bool ClientDebug<TClient>::Logger::log
				(
					const log::Level level,
					const std::string &tag,
					const std::string &message
				)
				{
					m_parent.messenger().send({karhu::network::Protocol::serialiseLog
					(
						level,
						tag,
						message
					)});
					
					return true;
				}

				template<class TClient>
				ClientDebug<TClient>::ClientDebug()
				:
				m_logger{*this}
				{
				}
				
				template<class TClient>
				void ClientDebug<TClient>::attachLogger()
				{
					log::Logger::implementation(&m_logger);
				}
				
				template<class TClient>
				bool ClientDebug<TClient>::receive(const TMessage &message)
				{
					if (m_callbackOnReceive)
						return m_callbackOnReceive(message);

					return true;
				}
				
				template<class TClient>
				bool ClientDebug<TClient>::updateAfterMessages()
				{
					return true;
				}
				
				template<class TClient>
				ClientDebug<TClient>::~ClientDebug()
				{
					log::Logger::implementation(nullptr);
					
					if (karhu::network::State::connected == TClient::state())
						TClient::messenger().send("disconnect");
				}
			}
		}
	}
#endif
