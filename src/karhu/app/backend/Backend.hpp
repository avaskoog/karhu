/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Use a deltatimer from core instead for the deltatime in the backend.
#ifndef KARHU_APP_BACKEND_H_
	#define KARHU_APP_BACKEND_H_

	#ifndef KARHU_BACKEND_PATH_HEADER
		#define KARHU_BACKEND_PATH_HEADER <karhu/app/backend/implementation/Backend.hpp>
	#endif

	#ifdef KARHU_EDITOR
		#include <karhu/app/backend/network/ClientDebug.hpp>
		#include <karhu/network/Interface.hpp>
		#include <karhu/network/Protocol.hpp>
		#include <karhu/app/module/SubsystemModule.hpp>
	#endif

	#include <karhu/app/win/SubsystemWindow.hpp>
	#include <karhu/app/gfx/SubsystemGraphics.hpp>
	#include <karhu/app/inp/SubsystemInput.hpp>
	#include <karhu/app/fio/SubsystemFile.hpp>
	#include <karhu/app/phx/SubsystemPhysics.hpp>
	#include <karhu/app/snd/SubsystemAudio.hpp>
	#include <karhu/core/Deltatimer.hpp>
	#include <karhu/core/Nullable.hpp>
	#include <karhu/core/platform.hpp>
	#include <karhu/core/log.hpp>
	#include <karhu/core/meta.hpp>

	#ifdef KARHU_EDITOR
			#endif

	#include <type_traits>
	#include <memory>
	#include <algorithm>
	#include <chrono>
	#include <functional>

	namespace karhu
	{
		namespace gfx
		{
			class Transform;
			class Camera;
		}
		
		namespace app
		{
			class Backend
			{
				public:
					using Argument  = std::pair<std::string, std::vector<std::string>>;
					using Arguments = std::vector<Argument>;

				public:
					void logVersions();
					Nullable<Arguments> separateArgs(const std::size_t &argc, char *argv[]);
					const Argument *findArg(const Arguments &, const char *) const;
					bool init(App &, int argc, char *argv[], Arguments args);
					bool initImGui(win::Window &window);
					void deinitImGui();
					void renderImGui();
					bool initIm3D(win::Window &window);
					void deinitIm3D();
					void loop();
					void update();
				
					#ifdef KARHU_EDITOR
					bool shouldRepaintEditor() const noexcept;
					void shouldRepaintEditor(bool const set) noexcept;
					bool shouldRepaintViewportEditor() const noexcept;
					void shouldRepaintViewportEditor(bool const set) noexcept;
					#endif

					/**
					 * Returns whether the backend has been successfully initialised.
					 *
					 * @return Whether valid.
					 */
					bool valid() const noexcept { return m_initialised; }
				
					/**
					 * Tries to use the graphics subsystem at the specified index.
					 * If the switch is successful, the new system needs to be manually initialised.
					 *
					 * @return Whether the switch was successfully made.
					 */
					bool trySetGraphics(const std::size_t &index) { return performTrySetGraphics(index); }
				
					/**
					 * Tries to use the audio subsystem at the specified index.
					 * If the switch is successful, the new system needs to be manually initialised.
					 *
					 * @return Whether the switch was successfully made.
					 */
					bool trySetAudio(const std::size_t &index) { return performTrySetAudio(index); }
				
					static constexpr float timestep() noexcept { return 1.0f / 60.0f; }
					const float &deltatime() const noexcept { return m_deltatime; }
					const float &deltatimeFixed() const noexcept { return m_deltatimeFixed; }
				
					virtual ~Backend() {}
				
				public:				
					/**
					 * Returns the current graphics subsystem.
					 *
					 * @return A pointer to current graphics subsystem if there is one, or null.
					 */
					virtual gfx::SubsystemGraphics *graphics() noexcept = 0;
				
					/**
					 * Returns the current graphics subsystem.
					 *
					 * @return A pointer to current graphics subsystem if there is one, or null.
					 */
					virtual const gfx::SubsystemGraphics *graphics() const noexcept = 0;
				
					/**
					 * Returns the current audio subsystem.
					 *
					 * @return A pointer to current audio subsystem if there is one, or null.
					 */
					virtual snd::SubsystemAudio *audio() noexcept = 0;
				
					/**
					 * Returns the current audio subsystem.
					 *
					 * @return A pointer to current audio subsystem if there is one, or null.
					 */
					virtual const snd::SubsystemAudio *audio() const noexcept = 0;

					/**
					 * Returns the window subsystem.
					 *
					 * @return The window subsystem.
					 */
					virtual win::SubsystemWindow &windows() noexcept = 0;
				
					/**
					 * Returns the window subsystem.
					 *
					 * @return The window subsystem.
					 */
					virtual const win::SubsystemWindow &windows() const noexcept = 0;
				
					/**
					 * Returns the input subsystem.
					 *
					 * @return The input subsystem.
					 */
					virtual inp::SubsystemInput &input() noexcept = 0;
				
					/**
					 * Returns the input subsystem.
					 *
					 * @return The input subsystem.
					 */
					virtual const inp::SubsystemInput &input() const noexcept = 0;

					/**
					 * Returns the file subsystem.
					 *
					 * @return The file subsystem.
					 */
					virtual fio::SubsystemFile &files() noexcept = 0;

					/**
					 * Returns the file subsystem.
					 *
					 * @return The file subsystem.
					 */
					virtual const fio::SubsystemFile &files() const noexcept = 0;
				
					/**
					 * Returns the physics subsystem.
					 *
					 * @return The physics subsystem.
					 */
					virtual phx::SubsystemPhysics &physics() noexcept = 0;
				
					/**
					 * Returns the physics subsystem.
					 *
					 * @return The physics subsystem.
					 */
					virtual const phx::SubsystemPhysics &physics() const noexcept = 0;

					#ifdef KARHU_EDITOR
						/**
						 * Returns the module subsystem.
						 *
						 * @return The module subsystem.
						 */
						virtual module::SubsystemModule &modules() noexcept = 0;

						/**
						 * Returns the module subsystem.
						 *
						 * @return The module subsystem.
						 */
						virtual const module::SubsystemModule &modules() const noexcept = 0;
				
						virtual void message(const std::string &) = 0;

						/// @todo: Figure out how to return the client since it's a template class...
//						virtual Client &client() noexcept = 0;
//						virtual const Client &client() const noexcept = 0;
					#endif
				
				protected:
					virtual void performLogVersions() = 0;
				
					/**
					 * Performs any necessary initialisation for the backend.
					 *
					 * @return Whether initialisation was successful.
					 */
					virtual bool performInit() = 0;

					/**
					 * Called at the beginning of each call to update().
					 */
					virtual bool performBeginFrame(app::FuncImGuiEvent) = 0;

					/**
					 * Called at the end of each call to update().
					 */
					virtual void performEndFrame() = 0;
				
					virtual bool performTrySetGraphics(const std::size_t &index) = 0;
					virtual bool performTrySetAudio(const std::size_t &index) = 0;

					/**
					 * Can be overridden to start the loop and call update() manually
					 * in case the default implementation does not work on one or more
					 * of the implementation's target platforms.
					 */
					virtual void performStartLoop()
					{
						while (windows().open()) update();
					}
					
					/**
					 * Updates the backend each frame.
					 *
					 * This needs to be called manually if a derived backend overrides
					 * the virtual method startLoop() in case the default implementation
					 * does not work on one or more of the implementation's target platforms.
					 */
					virtual void performUpdate() = 0;

					#ifdef KARHU_EDITOR
						virtual bool performConnectClient(const Arguments &args) = 0;
						virtual bool performInitModules(const Arguments &args) = 0;
					#endif
				
				public:
					void updateIm3D(gfx::Transform const *const, gfx::Camera const *const, float const w, float const h);
				
				/// @todo: Unprotecting while figuring out Im3D. Temporary.
				//protected:
					void beginFrameImGui(win::Window &window);
					void endFrameImGui();
					void beginFrameIm3D(win::Window &);
					void endFrameIm3D(win::Window &);
					void performEndFrameIm3D(win::Window &);
				
					void showImGui(bool const set) { m_showImGui = set; }
					bool showImGui() const noexcept { return m_showImGui; }
				
				public:
					#ifdef KARHU_EDITOR
						std::function
						<
							void
							(
								App &,
								karhu::network::Protocol::TypeMessage const &,
								conv::JSON::Val const &
							)
						> callbackMessageFromServer;
					#endif

				protected:
					Arguments m_args;
					App *m_app{nullptr};
				
					bool
						m_initialised{false},
						m_initialisedImGui{false},
						m_initialisedIm3D{false},
						m_showImGui
						{
							#if defined(KARHU_EDITOR) || defined(DEBUG)
							true
							#else
							false
							#endif
						};
				
					Deltatimer m_dtIm3D;
					gfx::Transform const *m_transformIm3D{nullptr};
					gfx::Camera const *m_cameraIm3D{nullptr};
					float m_widthIm3D, m_heightIm3D;
				
					/// @todo: Replace with deltatimer.
					std::chrono::time_point<std::chrono::system_clock> m_timeLast;
					float m_deltatime{0.0f}, m_deltatimeFixed{0.0f}, m_timeAccumulated{0.0f};
				
					#ifdef KARHU_EDITOR
					int m_framesToRepaintEditor, m_framesToRepaintViewportEditor;
					#endif
			};

			/// @todo: In the future maybe support networking beyond the client, which is going to necessitate quite a bit of a rewrite, with support for multiple clients and so on...
			/// @todo: Rewrite this description.
			/**
			 * Brings together and manages all subsystems to provide a working backend.
			 */
			template
			<
				class TSubsystemWindow,
				class TSubsystemInput,
				class TSubsystemFile,
				class TListSubsystemsGraphics,
				class TListSubsystemsAudio

				#ifdef KARHU_EDITOR
					,
					class TSubsystemModule,
					template<typename, std::size_t> class TClientBase
				#endif
			>
			class BackendWith : public Backend
			{
				private:
					#ifdef KARHU_EDITOR
						using TClient = TClientBase<conv::Char, karhu::network::protocol::buffersize()>;
					#endif

				public:
					// Set up some shorthands.
					using Windows  = TSubsystemWindow;
					using Input    = TSubsystemInput;
					using Files    = TSubsystemFile;
					using Physics  = phx::SubsystemPhysics;
					using Graphics = TListSubsystemsGraphics;
					using Audio    = TListSubsystemsAudio;
				
					#ifdef KARHU_EDITOR
						using Modules = TSubsystemModule;
						using Client  = network::ClientDebug<TClient>;
					#endif
				
				public:
					gfx::SubsystemGraphics *graphics() noexcept override { return m_graphics.get(); }
					const gfx::SubsystemGraphics *graphics() const noexcept override { return m_graphics.get(); }

					snd::SubsystemAudio *audio() noexcept override { return m_audio.get(); }
					const snd::SubsystemAudio *audio() const noexcept override { return m_audio.get(); }

					win::SubsystemWindow &windows() noexcept override { return m_windows; }
					const win::SubsystemWindow &windows() const noexcept override { return m_windows; }

					inp::SubsystemInput &input() noexcept override { return m_input; }
					const inp::SubsystemInput &input() const noexcept override { return m_input; }

					fio::SubsystemFile &files() noexcept override { return m_files; }
					const fio::SubsystemFile &files() const noexcept override { return m_files; }
				
					phx::SubsystemPhysics &physics() noexcept override { return m_physics; }
					const phx::SubsystemPhysics &physics() const noexcept override { return m_physics; }

					#ifdef KARHU_EDITOR
						module::SubsystemModule &modules() noexcept override { return m_modules; }
						const module::SubsystemModule &modules() const noexcept override { return m_modules; }
				
						void message(const std::string &text) override
						{
							if (karhu::network::State::connected == m_client.state())
								m_client.messenger().send(text);
						}

						/// @todo: Figure out how to return the client since it's a template class...
//						Client &client() noexcept override { return m_client; }
//						const Client &client() const noexcept override { return m_client; }
					#endif
				
					~BackendWith()
					{
						m_files.deinit();
					}

				private:
					/// @todo: Queue this?
					/// @todo: Do something about code reduplication for other dynamic subsystems (maybe more are added in the future)? Maybe just a macro?
					bool performTrySetGraphics(const std::size_t &index) override
					{
						if (m_graphics && index == m_indexGraphics) return true;
						
						m_indexGraphics = index;

						if (m_factoriesGraphics.size() > index)
						{
							// Create the new one.
							auto p(m_factoriesGraphics[index]());
							if (!p)
							{
								log::err("Karhu") << "Error setting graphics subsystem: construction failed";
								return false;
							}

							// Check if we need to get rid of a current one.
							if (m_graphics)
							{
								m_graphics->deinit(*m_app);
								/// @todo: Move over data to new system and such.
								/// @todo: Recreate windows and such.
							}

							// Store the new one instead.
							m_graphics = std::move(p);

							return true;
						}
						else
						{
							log::err("Karhu")
								<< "Error setting graphics subsystem: invalid index "
								<< index << " (of " << m_factoriesGraphics.size() << ")";
							return false;
						}
					}

					/// @todo: Queue this?
					/// @todo: Do something about code reduplication for other dynamic subsystems (maybe more are added in the future)? Maybe just a macro?
					bool performTrySetAudio(const std::size_t &index) override
					{
						if (m_audio && index == m_indexAudio) return true;
						
						m_indexAudio = index;

						if (m_factoriesAudio.size() > index)
						{
							// Create the new one.
							auto p(m_factoriesAudio[index]());
							if (!p)
							{
								log::err("Karhu") << "Error setting audio subsystem: construction failed";
								return false;
							}

							// Check if we need to get rid of a current one.
							if (m_audio)
							{
								m_audio->deinit(*m_app);
								/// @todo: Move over data to new system and such.
								/// @todo: Recreate windows and such (if necessary for audio).
							}

							// Store the new one instead.
							m_audio = std::move(p);

							return true;
						}
						else
						{
							log::err("Karhu")
								<< "Error setting audio subsystem: invalid index "
								<< index << " (of " << m_factoriesAudio.size() << ")";
							return false;
						}
					}

					void performUpdate() override
					{
						/// @todo: Separate events for separate windows? Or remove multiple window support alltogether?
						input().beginFrame(*windows().top());
						
						#ifdef KARHU_EDITOR
						if (!performBeginFrame(windows().funcImGuiEvent()))
						{
							auto const &IO(ImGui::GetIO());
							
							if
							(
//								IO.WantTextInput                                ||
//								ImGui::GetHoveredID() != m_IDHoveredLastImGui   ||
								ImGui::IsMouseDragging(ImGuiMouseButton_Left)   ||
								ImGui::IsMouseDragging(ImGuiMouseButton_Middle) ||
								ImGui::IsMouseDragging(ImGuiMouseButton_Right)
							)
								shouldRepaintEditor(true);
						}
						
						m_IDHoveredLastImGui = ImGui::GetHoveredID();
						#else
						performBeginFrame(windows().funcImGuiEvent());
						#endif
						
						#ifdef KARHU_EDITOR
							m_client.update();
						#endif
						
						for (std::size_t i{0}; i < m_windows.count(); ++ i)
						{
							/// @todo: Begin frame and end frame for ImGui probably should not live here but be used manually?
							auto w(m_windows.get(i));
							
							m_windows.beginFrame(*w); /// @todo: Check if the window is closed and stop everything... Maybe just return a bool here.
							
							beginFrameImGui(*w);
							
							beginFrameIm3D(*w);
							
							if (w->callbackUpdate)
								w->callbackUpdate
								(
									deltatime(),
									deltatimeFixed(),
									timestep()
								);
							
							/// @todo: Temporary while figuring out Im3d.
							performEndFrameIm3D(*w);
							
							#ifdef KARHU_EDITOR
							if (shouldRepaintEditor())
							#endif
							if (m_showImGui)
								renderImGui();
							
							endFrameImGui();
							
							#ifdef KARHU_EDITOR
							//if (shouldRepaintEditor())
							#endif
							m_windows.endFrame(*w);
						}

						performEndFrame();
						input().endFrame(*windows().top());
						
						#ifdef KARHU_EDITOR
						if (shouldRepaintEditor())
							-- m_framesToRepaintEditor;
						
						if (shouldRepaintViewportEditor())
							-- m_framesToRepaintViewportEditor;
						#endif
					}
				
				private:
					#ifdef KARHU_EDITOR
						/*
						 * Tries to connect to the debug client if the arguments specify a host and a port.
						 */
						bool performConnectClient(const Arguments &args) override
						{
							std::string host, port;
							
							{
								const auto it(std::find_if(args.begin(), args.end(), [](const auto &v)
								{
									return (v.first == "host");
								}));
								if (it != args.end() && it->second.size() > 0)
								{
									host = it->second[0];
								}
							}
							{
								const auto it(std::find_if(args.begin(), args.end(), [](const auto &v)
								{
									return (v.first == "port");
								}));
								if (it != args.end() && it->second.size() > 0)
								{
									port = it->second[0];
								}
							}
							
							// Check if host and port arguments were provided.
							if (host.length() > 0 && port.length() > 0)
							{
								// Convert the port from string to number.
								std::stringstream ps;
								ps << port;
								std::uint16_t p;
								ps >> p;

								// Concatenate the final address to display.
								ps.str("");
								ps << host << ':' << static_cast<int>(karhu::network::protocol::client::port(Platform::current, p));
								
								log::msg("Karhu") << "Connecting to debug server at " << ps.str() << "...";

								// Try to connect!
								// Failure to connect will not shut down as we may still want to keep running.
								if (!m_client.connect(Platform::current, p, host))
								{
									log::err("Karhu") << "Failed to connect to debug server";
									return false;
								}
								else
								{
									m_client.attachLogger();
									log::msg("Karhu") << "Successfully connected to debug server";
									m_client.messenger().send("connect");
									
									m_client.callbackOnReceive([this](const typename TClient::TMessage &message)
									{
										return receive(message);
									});
								}
							}
							else
							{
								log::err("Karhu") << "Failed to connect to server";
								return true;
							}
							
							return true;
						}

						bool performInitModules(const Arguments &args) override
						{
							const auto it(std::find_if(args.begin(), args.end(), [](const auto &v)
							{
								return (v.first == "modules");
							}));
							if (it != args.end() && it->second.size() > 0)
							{
								// If the number is odd something is wrong.
								if (it->second.size() % 2 != 0)
								{
									log::err("Karhu") << "Not all module names passed through the command line arguments are paired with a path";
									return false;
								}
								
								// Go through the pairs of module name and path.
								for (std::size_t i{0}; i < it->second.size(); i += 2)
								{
									auto pool(m_modules.createPool(it->second[i]));
									if (!pool) return false;

									if (m_modules.updatePoolFromLibrary(*pool, it->second[i + 1]))
										log::msg("Karhu") << "Successfully loaded dynamic module '" << it->second[i] << "'";
									else
										log::err("Karhu") << "Failed to load dynamic module '" << it->second[i] << "'";
								}
							}
							
							return true;
						}

						bool receive(const typename TClient::TMessage &message)
						{
							using P = karhu::network::Protocol;
							using T = P::TypeMessage;

							const auto text(message.text());

							//log::msg("server") << '\'' << text << '\'';

							if (auto v = P::deserialise(text))
							{
								if (m_app && callbackMessageFromServer)
									callbackMessageFromServer(*m_app, v->type, v->data);

								switch (v->type)
								{
									case T::module:
									{
										log::msg("Karhu") << "(Re)loading module...";
										if (v->data.array() && v->data.array()->count() == 2)
										{
											const auto name(v->data.array()->get(0));
											const auto file(v->data.array()->get(1));
											
											if (name->string() && file->string())
											{
												for (auto &pool : m_modules.pools())
												{
													if (pool.second && pool.first == *name->string())
													{
														if (m_modules.updatePoolFromLibrary(*pool.second, *file->string()))
															log::msg("Karhu") << "Successfully loaded dynamic module '" << *name->string() << "'";
													}
												}
											}
										}
										
										break;
									}

									default:
										break;
								}
							}
							else if (text == "disconnect")
							{
								windows().close();
								return false;
							}
							else if (m_app && callbackMessageFromServer)
								callbackMessageFromServer(*m_app, T::string, text);
							
							return true;
						}
					#endif

				private:
					// Only one each of these.
					Windows m_windows;
					Input   m_input;
					Files   m_files;
					Physics m_physics;

					// Client is only used for debugging.
					#ifdef KARHU_EDITOR
					Modules m_modules;
					Client  m_client;
				
					ImGuiID m_IDHoveredLastImGui{0};
					#endif

					// Multiple possible for these.
					// Construct the default one so that there is always one in use.
					/// @todo: Read from settings.
					std::unique_ptr<gfx::SubsystemGraphics> m_graphics;
					std::unique_ptr<snd::SubsystemAudio>    m_audio;

					// Factories will be generated.
					typename TListSubsystemsGraphics::Factories m_factoriesGraphics{TListSubsystemsGraphics::generateFactories()};
					typename TListSubsystemsAudio   ::Factories m_factoriesAudio   {TListSubsystemsAudio   ::generateFactories()};

					// Which one is being used now.
					std::size_t m_indexGraphics{0}, m_indexAudio{0};
			};
		}
	}
#endif
