/**
 * @author		Ava Skoog
 * @date		2019-11-09
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/backend/Backend.hpp>
#include <karhu/app/gfx/components/Transform.hpp>
#include <karhu/app/gfx/components/Camera.hpp>
#include <karhu/app/App.hpp>

#include <karhu/app/lib/imguizmo/ImGuizmo.h>

namespace karhu
{
	namespace app
	{
		void Backend::logVersions()
		{
			if (!m_initialised)
				return;
			
			performLogVersions();
			
			windows().logVersions();
			input().logVersions();
			
			if (graphics())
				graphics()->logVersions();
			
			if (audio())
				audio()->logVersions();
			
			physics().logVersions();
			files().logVersions();
		}
		
		Nullable<Backend::Arguments> Backend::separateArgs
		(
			const std::size_t &argc,
			char *argv[]
		)
		{
			Arguments r;
			
			if (argc > 1)
			{
				for (std::size_t i{1}; i < argc; ++ i)
				{
					std::string arg{argv[i]};
					
					// Used for escaping when hyphens are problematic; ignore.
					if (arg[0] == '\\')
						arg = arg.substr(1);

					// Is it a key?
					if (arg[0] == '-')
					{
						std::pair<std::string, std::vector<std::string>> curr;
						curr.first = arg.substr(1);
						
						++ i;

						for (; i < argc; ++ i)
						{
							arg = argv[i];
							
							// Used for escaping when hyphens are problematic; ignore.
							if (arg[0] == '\\')
								arg = arg.substr(1);

							// Break on next key.
							if (arg[0] == '-')
							{
								-- i;
								break;
							}
							// Add regular values.
							else
								curr.second.emplace_back(std::move(arg));
						}
						
						// Add the current key with values.
						r.emplace_back(std::move(curr));
					}
					else if (i == 1)
					{
						log::err("Karhu") << "First command-line argument has to be a key (indicated by one hyphen)";
						return {};
					}
				}
			}
			/*
			log::msg("debug") << "---BEG ARGS---";
			for (auto &it : r)
			{
				for (auto &jt : it.second)
					log::msg("debug") << "-" << it.first << ": " << jt;
			}
			log::msg("debug") << "---END ARGS---";
			*/
			return {true, std::move(r)};
		}
		
		const Backend::Argument *Backend::findArg(const Arguments &args, const char *name) const
		{
			const auto it(std::find_if
			(
				args.begin(),
				args.end(),
				[&name](const auto &v)
				{
					return (v.first == name);
				}
			));
			
			if (it != args.end())
				return &*it;
			
			return nullptr;
		}
		
		/**
		 * Intialises the backend.
		 * This has to be called before starting the loop.
		 *
		 * @param app  The parent application instance.
		 * @param argc Passed on from main function.
		 * @param argv Passed on from main function.
		 * @param args The program arguments, extracted using separateArgs().
		 *
		 * @return Whether initalisation was successful.
		 */
		bool Backend::init(App &app, int argc, char *argv[], Arguments args)
		{
			if (m_initialised)
				return true;
			
			m_args = args;
			m_app  = &app;
			
			#ifdef KARHU_EDITOR
			shouldRepaintViewportEditor(true);
			#endif
			
			std::string basepath;
		
			const Argument *const argProjdir(findArg(args, "projdir"));
			
			if (argProjdir && argProjdir->second.size() > 0)
				basepath = argProjdir->second[0];
			
			if (!performInit())
				return false;

			if (!input().init())
				return false;
			
			if (!files().init(argc, argv, basepath))
				return false;

			#ifdef KARHU_EDITOR
			if (findArg(args, "client") && !findArg(args, "offline"))
				if (!performConnectClient(m_args))
					return false;
		
			if (!performInitModules(m_args))
				return false;
			#endif

			if (!trySetGraphics(0))
				return false;
			
			if (!trySetAudio(0))
				return false;
			
			m_dtIm3D.reset();

			return (m_initialised = true);
		}
		
		bool Backend::initImGui(win::Window &window)
		{
			/// @todo: Unset initialisedImGui if graphics subsystem changes.
			if (m_initialisedImGui || !window.handle() || !graphics())
				return false;

			auto init   (windows().funcImGuiInit());
			auto clipget(windows().funcImGuiClipboardGet());
			auto clipset(windows().funcImGuiClipboardSet());
			auto render (graphics()->funcImGuiRender());
			
			if (!init || !render)
				return false;
			
			ImGui::CreateContext();
			
			auto &io(ImGui::GetIO());
			
			// Alternatively you can set this to NULL and
			// call ImGui::GetDrawData() after ImGui::Render()
			// to get the same ImDrawData pointer.
			io.RenderDrawListsFn  = render;
			io.GetClipboardTextFn = clipget;
			io.SetClipboardTextFn = clipset;
			io.ClipboardUserData  = nullptr;
			
			// A bit too little by default.
			io.MouseDoubleClickTime = 0.5f;
			
			ImVec4* colors = ImGui::GetStyle().Colors;
			colors[ImGuiCol_Text]                   = ImVec4(0.94f, 0.94f, 0.94f, 1.00f);
			colors[ImGuiCol_TextDisabled]           = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
			colors[ImGuiCol_WindowBg]               = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_ChildBg]                = ImVec4(0.02f, 0.02f, 0.02f, 1.00f);
			colors[ImGuiCol_PopupBg]                = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_Border]                 = ImVec4(0.71f, 0.68f, 0.74f, 0.18f);
			colors[ImGuiCol_BorderShadow]           = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
			colors[ImGuiCol_FrameBg]                = ImVec4(0.38f, 0.38f, 0.38f, 0.46f);
			colors[ImGuiCol_FrameBgHovered]         = ImVec4(0.49f, 0.48f, 0.48f, 0.46f);
			colors[ImGuiCol_FrameBgActive]          = ImVec4(0.59f, 0.59f, 0.59f, 0.46f);
			colors[ImGuiCol_TitleBg]                = ImVec4(0.13f, 0.09f, 0.20f, 1.00f);
			colors[ImGuiCol_TitleBgActive]          = ImVec4(0.30f, 0.20f, 0.49f, 1.00f);
			colors[ImGuiCol_TitleBgCollapsed]       = ImVec4(0.13f, 0.09f, 0.20f, 1.00f);
			colors[ImGuiCol_MenuBarBg]              = ImVec4(0.13f, 0.13f, 0.13f, 1.00f);
			colors[ImGuiCol_ScrollbarBg]            = ImVec4(0.02f, 0.02f, 0.02f, 0.53f);
			colors[ImGuiCol_ScrollbarGrab]          = ImVec4(0.31f, 0.31f, 0.31f, 1.00f);
			colors[ImGuiCol_ScrollbarGrabHovered]   = ImVec4(0.41f, 0.41f, 0.41f, 1.00f);
			colors[ImGuiCol_ScrollbarGrabActive]    = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
			colors[ImGuiCol_CheckMark]              = ImVec4(0.68f, 0.52f, 0.98f, 1.00f);
			colors[ImGuiCol_SliderGrab]             = ImVec4(0.68f, 0.52f, 0.98f, 1.00f);
			colors[ImGuiCol_SliderGrabActive]       = ImVec4(0.85f, 0.77f, 1.00f, 1.00f);
			colors[ImGuiCol_Button]                 = ImVec4(0.45f, 0.31f, 0.72f, 1.00f);
			colors[ImGuiCol_ButtonHovered]          = ImVec4(0.62f, 0.50f, 0.83f, 1.00f);
			colors[ImGuiCol_ButtonActive]           = ImVec4(0.69f, 0.61f, 0.85f, 1.00f);
			colors[ImGuiCol_Header]                 = ImVec4(0.38f, 0.25f, 0.62f, 1.00f);
			colors[ImGuiCol_HeaderHovered]          = ImVec4(0.45f, 0.31f, 0.72f, 1.00f);
			colors[ImGuiCol_HeaderActive]           = ImVec4(0.40f, 0.27f, 0.66f, 1.00f);
			colors[ImGuiCol_Separator]              = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
			colors[ImGuiCol_SeparatorHovered]       = ImVec4(0.45f, 0.31f, 0.72f, 1.00f);
			colors[ImGuiCol_SeparatorActive]        = ImVec4(0.69f, 0.61f, 0.85f, 1.00f);
			colors[ImGuiCol_ResizeGrip]             = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
			colors[ImGuiCol_ResizeGripHovered]      = ImVec4(0.45f, 0.31f, 0.72f, 1.00f);
			colors[ImGuiCol_ResizeGripActive]       = ImVec4(0.69f, 0.61f, 0.85f, 1.00f);
			colors[ImGuiCol_Tab]                    = ImVec4(0.27f, 0.19f, 0.42f, 1.00f);
			colors[ImGuiCol_TabHovered]             = ImVec4(0.51f, 0.40f, 0.72f, 1.00f);
			colors[ImGuiCol_TabActive]              = ImVec4(0.45f, 0.31f, 0.72f, 1.00f);
			colors[ImGuiCol_TabUnfocused]           = ImVec4(0.27f, 0.19f, 0.42f, 0.63f);
			colors[ImGuiCol_TabUnfocusedActive]     = ImVec4(0.45f, 0.31f, 0.72f, 0.63f);
			colors[ImGuiCol_DockingPreview]         = ImVec4(0.79f, 0.68f, 0.98f, 0.68f);
			colors[ImGuiCol_DockingEmptyBg]         = ImVec4(0.10f, 0.10f, 0.10f, 1.00f);
			colors[ImGuiCol_PlotLines]              = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
			colors[ImGuiCol_PlotLinesHovered]       = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
			colors[ImGuiCol_PlotHistogram]          = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
			colors[ImGuiCol_PlotHistogramHovered]   = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
			colors[ImGuiCol_TextSelectedBg]         = ImVec4(0.76f, 0.76f, 0.76f, 0.46f);
			colors[ImGuiCol_DragDropTarget]         = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
			colors[ImGuiCol_NavHighlight]           = ImVec4(0.45f, 0.31f, 0.72f, 1.00f);
			colors[ImGuiCol_NavWindowingHighlight]  = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
			colors[ImGuiCol_NavWindowingDimBg]      = ImVec4(0.80f, 0.80f, 0.80f, 0.20f);
			colors[ImGuiCol_ModalWindowDimBg]       = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);

			ImGui::GetStyle().TabBorderSize    =
			ImGui::GetStyle().ChildBorderSize  =
			ImGui::GetStyle().FrameBorderSize  =
			ImGui::GetStyle().PopupBorderSize  =
			ImGui::GetStyle().WindowBorderSize = 1.0f;
			
			ImGui::GetStyle().TabRounding       =
			ImGui::GetStyle().GrabRounding      =
			ImGui::GetStyle().ChildRounding     =
			ImGui::GetStyle().FrameRounding     =
			ImGui::GetStyle().PopupRounding     =
			ImGui::GetStyle().WindowRounding    =
			ImGui::GetStyle().ScrollbarRounding = 1.0f;
			
			return (m_initialisedImGui = init(window.handle()));
		}
		
		void Backend::deinitImGui() /// @todo: Why is this not used? Why does funcImGuiDevicesDestroy() crash if used?
		{
			if (!m_initialisedImGui) return;
			
			if (auto devices = graphics()->funcImGuiDevicesDestroy())
				devices();
			
			if (auto deinit = windows().funcImGuiDeinit())
				deinit();
			
			ImGui::DestroyContext();
			//ImGui::Shutdown();
			
			m_initialisedImGui = false;
		}
		
		void Backend::renderImGui()
		{
			if (!m_initialisedImGui)
				return;
			
			#ifdef KARHU_EDITOR
			if
			(
				app::ModeApp::server == m_app->mode() &&
				!shouldRepaintEditor()                &&
				!shouldRepaintViewportEditor()
			)
				return;
			#endif
			
			ImGui::Render();
		}
		
		bool Backend::initIm3D(win::Window &window)
		{
			if (m_initialisedIm3D || !window.handle() || !graphics())
				return false;
			
			auto init(graphics()->funcIm3DInit());
			
			if (!init)
				return false;
			
			return (m_initialisedIm3D = init());
		}

		void Backend::deinitIm3D()
		{
			if (!m_initialisedIm3D)
				return;
			
			if (auto deinit = graphics()->funcIm3DShutdown())
				deinit();
		}
		
		/**
		 * Starts the main loop and keeps running until shutdown.
		 */
		void Backend::loop()
		{
			if (!valid())
			{
				log::err("Karhu") << "Failed to start main loop: backend not initialised";
				return;
			}

			m_timeLast = std::chrono::system_clock::now();
			return performStartLoop();
		}

		void Backend::update()
		{
			/// @todo: Is this editor ifdef for backend deltatime necessary now that only select systems will be updated in editor mode?
			#ifdef KARHU_EDITOR
			//if (m_app->client())
			{
			#endif
				using namespace std::chrono;
				
				// Calculate the new deltatime.
				auto now{system_clock::now()};
				auto const diff(static_cast<float>(duration_cast<milliseconds>(now - m_timeLast).count()));
				
				m_deltatime = (diff / 1000.0f);
				
				//m_deltatime = mth::min(m_deltatime, timestep() * 4.0f); /// @todo: Some kind of safety measure for low FPS… But it might break things more if things lag too much? Think about it…
				
				m_deltatimeFixed   = 0.0f;
				m_timeAccumulated += m_deltatime;
				
				while (m_timeAccumulated >= timestep())
				{
					m_timeAccumulated -= timestep();
					m_deltatimeFixed  += timestep();
				}
				
				m_timeLast = now;
			#ifdef KARHU_EDITOR
			}
			//else if (shouldRepaintEditor())
			if (app::ModeApp::server == m_app->mode() && shouldRepaintEditor())
				graphics()->renderer()->clear();
			#endif

			performUpdate();
		}
		
		#ifdef KARHU_EDITOR
		bool Backend::shouldRepaintEditor() const noexcept
		{
			if (app::ModeApp::client == m_app->mode())
				return true;
			
			return (m_framesToRepaintEditor > 0);
		}

		void Backend::shouldRepaintEditor(bool const set) noexcept
		{
			m_framesToRepaintEditor = (set) ? 5 : 0;
		}

		bool Backend::shouldRepaintViewportEditor() const noexcept
		{
			if (app::ModeApp::client == m_app->mode())
				return true;
			
			return (m_framesToRepaintViewportEditor > 0);
		}

		void Backend::shouldRepaintViewportEditor(bool const set) noexcept
		{
			if (set)
				shouldRepaintEditor(true);
				
			m_framesToRepaintViewportEditor = (set) ? 2 : 0;
		}
		#endif
		
		void Backend::beginFrameImGui(win::Window &window)
		{
			if (!m_initialisedImGui)
				return;

			if (auto devices = graphics()->funcImGuiDevicesCreate())
				devices();
			
			if (auto frame = windows().funcImGuiFrame())
				frame(window.handle());

			ImGui::NewFrame();
			ImGuizmo::BeginFrame();
		}

		void Backend::endFrameImGui()
		{
			if (!m_initialisedImGui)
				return;
			
			ImGui::EndFrame();
		}
		
		void Backend::updateIm3D(gfx::Transform const *const t, gfx::Camera const *const c, float const w, float const h)
		{
			m_transformIm3D = t;
			m_cameraIm3D    = c;
			m_widthIm3D     = w;
			m_heightIm3D    = h;
		}
		
		void Backend::beginFrameIm3D(win::Window &window)
		{
			if (!m_initialisedImGui || !m_transformIm3D || !m_cameraIm3D)
				return;
			
			auto newFrame = graphics()->funcIm3DNewFrame();
			
			if (!newFrame)
				return;
			
			m_dtIm3D.tick();
			
			/// @todo: Do we need this?
			/*
			if (const auto window = app.win().top())
			{
				camera->updateProjection
				(
					window->width(),
					window->height(),
					window->scaleWidth(),
					window->scaleHeight()
				);
			 
				camera->recalculateProjection();
			}
			*/
			
			const mth::Mat4f
				V {m_cameraIm3D->matrixView(*m_transformIm3D)},
				P {m_cameraIm3D->matrixProjection()},
				W {m_transformIm3D->global().matrix()},
				VP{P * V};
		
			const auto dir{mth::normalise(mth::Vec3f
			{
				 V[2][0],
				 V[2][1],
				-V[2][2]
			})};
		
			const Im3d::Mat4
				WIm3D {mth::matrixIm3D(W)},
				PIm3D {mth::matrixIm3D(P)},
				VPIm3D{mth::matrixIm3D(VP)};
			
			newFrame
			(
				m_dtIm3D.normal(),
				m_widthIm3D,
				m_heightIm3D,
				false,
				{
					m_transformIm3D->global().position.x,
					m_transformIm3D->global().position.y,
					m_transformIm3D->global().position.z
				},
				{dir.x, dir.y, dir.z},
				m_cameraIm3D->projection().FOV,
				WIm3D,
				PIm3D,
				VPIm3D,
				{
					static_cast<float>(input().mouseX()),
					static_cast<float>(input().mouseY())
				},
				ImGui::IsMouseDown(0),
				ImGui::GetIO().KeyCtrl,
				false,//ImGui::IsKeyPressed(),
				ImGui::IsKeyPressed(90), // Match ImGuizmo
				ImGui::IsKeyPressed(69), // Match ImGuizmo
				ImGui::IsKeyPressed(82)  // Match ImGuizmo
			);
		}

		void Backend::endFrameIm3D(win::Window &window)
		{
			if (!m_initialisedImGui || !m_transformIm3D || !m_cameraIm3D)
				return;
		
			if (auto endFrame = graphics()->funcIm3DEndFrame())
				endFrame
				(
					static_cast<int>(m_widthIm3D),
					static_cast<int>(m_heightIm3D)
				);
		}
		
		void Backend::performEndFrameIm3D(win::Window &window)
		{
			#if KARHU_EDITOR_SERVER_SUPPORTED
			if (app::ModeApp::client == m_app->mode())
			#endif
			endFrameIm3D(window);
		}
	}
}
