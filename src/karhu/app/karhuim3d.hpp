/**
 * @author		Ava Skoog
 * @date		2019-01-11
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_IM3D_H_
	#define KARHU_APP_IM3D_H_

	#include <karhu/app/lib/im3d/im3d.h>
	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
		namespace app
		{
			using FuncIm3DInit = bool (*)();
			using FuncIm3DShutdown = void (*)();
			using FuncIm3DNewFrame = void (*)
			(
				const float &dt,
				const float &w,
				const float &h,
				const bool ortho,
				const Im3d::Vec3 &campos,
				const Im3d::Vec3 &camdir,
				const float &FOVRad,
				const Im3d::Mat4 &world,
				const Im3d::Mat4 &proj,
				const Im3d::Mat4 &viewproj,
				const Im3d::Vec2 &cursorpos,
				const bool mouseDownLeft,
				const bool keyDownCtrl,
				const bool keyDownL,
				const bool keyDownT,
				const bool keyDownR,
				const bool keyDownS
			);
			using FuncIm3DEndFrame = void (*)
			(
				const int &w,
				const int &h
			);
		}
		
		namespace mth
		{
			inline Im3d::Mat4 matrixIm3D(const mth::Mat4f &m)
			{
				Im3d::Mat4 r;
				
				for (int i{0}; i < 16; ++ i)
					r[i] = *(&(m[0][0]) + i);
				
				return r;
			}
		}
	}
#endif
