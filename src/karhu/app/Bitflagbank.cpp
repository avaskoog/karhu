/**
 * @author		Ava Skoog
 * @date		2020-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/Bitflagbank.hpp>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace app
	{
		void BitflagbankBase::serialiseElements
		(
			conv::Serialiser               &      ser,
			char                     const *const name,
			std::vector<std::string> const &      elements
		) const
		{
			ser << karhuINn(name, elements);
		}
		
		void BitflagbankBase::deserialiseElements
		(
			conv::Serialiser         const &      ser,
			char                     const *const name,
			std::vector<std::string>       &      elements
		)
		{
			ser >> karhuOUTn(name, elements);
		}
	}
}
