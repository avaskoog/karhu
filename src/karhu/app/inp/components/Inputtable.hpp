/**
 * @author		Ava Skoog
 * @date		2019-01-07
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_INPUT_INPUTTABLE_H_
	#define KARHU_INPUT_INPUTTABLE_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/inp/common.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
		namespace inp
		{
			class Inputtable : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
                    void editorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) override {}
                    bool editorEdit(app::App &, edt::Editor &) override;
                    #endif
				
				public:
					Layer    layer   {Defaultlayers::custom};
					Players  players {player(0)};
					Priority priority{0};
					Actions  actions;
			};
		}
	}
#endif
