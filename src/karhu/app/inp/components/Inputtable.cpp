/**
 * @author		Ava Skoog
 * @date		2019-01-07
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/inp/components/Inputtable.hpp>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace inp
	{
		void Inputtable::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(layer)
				<< karhuIN(players)
				<< karhuIN(priority)
				<< karhuIN(actions);
		}

		void Inputtable::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(layer)
				>> karhuOUT(players)
				>> karhuOUT(priority)
				>> karhuOUT(actions);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Inputtable::editorEdit(app::App &, edt::Editor &)
		{
			return false;
		}
		#endif
	}
}
