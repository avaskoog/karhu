/**
 * @author		Ava Skoog
 * @date		2019-01-09
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_INPUT_ECONNECTION_H_
	#define KARHU_INPUT_ECONNECTION_H_

	#include <karhu/app/inp/common.hpp>
	#include <karhu/app/ecs/common.hpp>

	#include <string>

	namespace karhu
	{
		namespace inp
		{
			class EConnection : public ecs::Event
			{
				public:
					enum class Type : std::uint8_t
					{
						connected,
						disconnected
					};
				
				public:
					EConnection
					(
						const ecs::IDEvent &identifier,
						const Type         &type,
						const IDController &ID,
						const char         *name,
						const Player       &player
					)
					:
					ecs::Event{identifier, 0, 0, 0},
					type      {type},
					ID        {ID},
					name      {name},
					player    {player}
					{
					}
				
				public:
					Type         type;
					IDController ID;
					std::string  name;
					Player       player;
			};
		}
	}
#endif
