/**
 * @author		Ava Skoog
 * @date		2019-01-08
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_INPUT_EINPUT_H_
	#define KARHU_INPUT_EINPUT_H_

	#include <karhu/app/inp/common.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
		namespace inp
		{
			class EInput : public ecs::Event
			{
				public:
					EInput
					(
						const ecs::IDEvent         &identifier,
						const ecs::IDEntity        &entity,
						const ecs::IDTypeComponent &typeComponent,
						const ecs::IDComponent     &component,
						const Action               &action,
						const Player               &player,
						const Type                 &type,
						const mth::Scalf       &analogue,
						const mth::Vec2f         &direction
					)
					:
					ecs::Event{identifier, entity, typeComponent, component},
					action    {action},
					player    {player},
					type      {type}
					{
						switch (type)
						{
							case Type::press:
							case Type::hold:
							case Type::release:
								break;
							
							case Type::analogue:  this->analogue  = analogue;  break;
							case Type::direction: this->direction = direction; break;
						}
					}
				
				public:
					Action action;
					Player player;
					Type   type;
				
					union
					{
						mth::Scalf analogue;
						mth::Vec2f   direction;
					};
			};
		}
	}
#endif
