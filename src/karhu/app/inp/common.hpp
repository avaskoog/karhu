/**
 * @author		Ava Skoog
 * @date		2019-01-07
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_INPUT_COMMON_H_
	#define KARHU_INPUT_COMMON_H_

	#include <karhu/conv/input.hpp>
	#include <karhu/conv/maths.hpp>

	#include <string>
	#include <map>
	#include <array>
	#include <algorithm>
	#include <cstdint>
	#include <type_traits>

	namespace karhu
	{
		namespace inp
		{
			enum class Mouse : Code
			{
				none = -1,
				
				left,
				right,
				middle,
				
				count
			};
			
			/// @todo: Fill in the rest.
			enum class Key : Code
			{
				none = -1,
				
				left  = 0,
				right = 1,
				up    = 2,
				down  = 3,
				
				zero  = '0', // 48
				one   = '1', // 49
				two   = '2', // 50
				three = '3', // 51
				four  = '4', // 52
				five  = '5', // 53
				six   = '6', // 54
				seven = '7', // 55
				eight = '8', // 56
				nine  = '9', // 57
				
				a = 'a', // 97
				b = 'b', // 98
				c = 'c', // 99
				d = 'd', // 100
				e = 'e', // 101
				f = 'f', // 102
				g = 'g', // 103
				h = 'h', // 104
				i = 'i', // 105
				j = 'j', // 106
				k = 'k', // 107
				l = 'l', // 108
				m = 'm', // 109
				n = 'n', // 110
				o = 'o', // 111
				p = 'p', // 112
				q = 'q', // 113
				r = 'r', // 114
				s = 's', // 115
				t = 't', // 116
				u = 'u', // 117
				v = 'v', // 118
				w = 'w', // 119
				x = 'x', // 120
				y = 'y', // 121
				z = 'z', // 122
				
				space     = ' ',    // 32
				backspace = '\b',   // 8
				enter     = '\r',   // 13
				escape    = '\033', // 27
				
				// The rest are not mapped to ASCII.
				
				numZero = 10000,
				numOne,
				numTwo,
				numThree,
				numFour,
				numFive,
				numSix,
				numSeven,
				numEight,
				numNine,
				numEnter,
				numEquals,
				numAdd,
				numSubtract,
				numMultiply,
				numDivide,
				numDecimal,

				f1,
				f2,
				f3,
				f4,
				f5,
				f6,
				f7,
				f8,
				f9,
				f10,
				f11,
				f12,

				shift,
				shiftLeft,
				shiftRight,

				alt,
				altLeft,
				altRight,

				control,
				controlLeft,
				controlRight,

				meta,
				metaLeft,
				metaRight,

				count
			};

			/// @todo: Fill in the rest.
			enum class Button : Code
			{
				none = -1,

				left,
				right,
				up,
				down,
				
				a,
				b,
				x,
				y,
				
				select,
				start,
				guide,
				
				shoulderLeft,
				shoulderRight,
				
				triggerLeft,
				triggerRight,
				
				stickLeft,
				stickRight,
				
				count
			};
			
			enum class Axis : Code
			{
				none = -1,
				
				xLeft,
				yLeft,
				xRight,
				yRight,
				
				triggerLeft,
				triggerRight,
				
				count
			};
			
			using IDController = std::int32_t;
			
			class Controller
			{
				public:
					Controller()
					{
						std::fill(m_buttons    .begin(), m_buttons    .end(), false);
						std::fill(m_buttonsLast.begin(), m_buttonsLast.end(), false);
						std::fill(m_axes       .begin(), m_axes       .end(), 0.0f);
						std::fill(m_axesLast   .begin(), m_axesLast   .end(), 0.0f);
					}
				
					const IDController &identifier() const noexcept { return m_ID; }
					const std::string &name() const noexcept { return m_name; }
				
					virtual void *handle() const = 0;
				
					virtual ~Controller() {}

				protected:
					IDController m_ID;
					std::string  m_name;
				
					std::array<bool, static_cast<std::size_t>(Button::count)>
						m_buttons,
						m_buttonsLast;
				
					std::array<mth::Scalf, static_cast<std::size_t>(Axis::count)>
						m_axes,
						m_axesLast;
				
				friend class SubsystemInput;
			};
		}
	}
#endif
