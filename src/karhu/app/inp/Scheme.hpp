/**
 * @author		Ava Skoog
 * @date		2019-01-07
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_INPUT_SCHEME_H_
	#define KARHU_INPUT_SCHEME_H_

	#include <karhu/res/Inputmap.hpp>

	namespace karhu
	{
		namespace app
		{
			class App;
		}
		
		namespace inp
		{
			class Scheme
			{
				public:
					using Actions = std::vector<res::Inputmap::Action>;
					using Setups  = std::vector<res::Inputmap::Setup>;
				
				public:
					Scheme() = default;
					Scheme(Scheme &&) = default;
					Scheme(const Scheme &) = delete;
					Scheme &operator=(const Scheme &) = delete;
					Scheme &operator=(Scheme &&) = default;
				
					void map(app::App &, const res::IDResource &);
					const res::IDResource &map() const { return m_map; }
				
					const Actions &actions() const noexcept { return m_actions; }
					const Setups  &setups () const noexcept { return m_setups; }
				
				private:
					res::IDResource m_map{0};
				
					Actions m_actions;
					Setups  m_setups;
			};
		}
	}
#endif
