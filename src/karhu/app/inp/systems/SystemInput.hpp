/**
 * @author		Ava Skoog
 * @date		2019-01-07
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_INPUT_SYSTEM_INPUT_H_
	#define KARHU_INPUT_SYSTEM_INPUT_H_

	#include <karhu/app/ecs/System.hpp>
	#include <karhu/app/inp/components/Inputtable.hpp>

	#include <map>
	#include <functional>

	namespace karhu
	{
		namespace inp
		{
			class SystemInput : public ecs::System<Inputtable>
			{
				public:
					SystemInput();
				
					void layerEnabled(const Layer &, const bool set);
					bool layerEnabled(const Layer &) const;
					void layerSolo(const Layer &);
					
				protected:
					void performUpdateComponents
					(
						      app::App              &app,
						      ecs::Pool<Inputtable> &components,
						const float                 dt,
						const float                 dtFixed,
						const float                 timestep
					) override;
				
				private:
					static SystemInput *c_instance;
				
				private:
					// We are using maps for the inputtables and layers
					// so we do not need to sort any of them manually.
					
					struct DataLayer
					{						
						std::multimap<Priority, Inputtable *> inputtables;
					};
				
					std::map<Layer, DataLayer, std::greater<Layer>> m_layers;
					std::vector<Layer> m_layersDisabled;
					Layer m_layerSolo{Defaultlayers::none};
				
				/// @todo: Ugly spaghetti. Work it out.
				friend class SubsystemInput;
			};
		}
	}
#endif
