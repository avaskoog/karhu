/**
 * @author		Ava Skoog
 * @date		2019-01-07
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Add a way to make a direction override another for example when holding left arrow and moving over to the right arrow before even releasing the left one, making the latest button win out instead of them cancelling each other out, and make this optional.

#include <karhu/app/inp/systems/SystemInput.hpp>
#include <karhu/app/inp/events/EInput.hpp>
#include <karhu/app/inp/events/EConnection.hpp>
#include <karhu/app/inp/SubsystemInput.hpp>

#include <algorithm>
#include <unordered_map>

namespace
{
	using namespace karhu;
	using namespace karhu::inp;
	
	struct DataEvent
	{
		bool           happened;
		std::size_t    indexScheme;
		mth::Scalf scalar;
		mth::Vec2f   vector;
	};
	
	static constexpr float epsilon() noexcept
	{
		return 0.000001f;
	}
	
	// Helper to check whether an input action was carried out.
	static bool actionGlobalHappened
	(
		const res::Inputmap::Action &action,
		const Scheme                &scheme,
			  DataEvent             &event,
		const bool                   hasController,
		const IDController          &controller,
		      SubsystemInput        &input
	)
	{
		bool r{false};
		
		if (Method::controller == action.method && !hasController)
			return false;
		
		for (Code const code : action.codes)
		{
			switch (action.type)
			{
				case Type::press:
				{
					switch (action.method)
					{
						case Method::mouse:      r = input.mousePressed (            static_cast<Mouse >(code)); break;
						case Method::keyboard:   r = input.keyPressed   (            static_cast<Key   >(code)); break;
						case Method::controller: r = input.buttonPressed(controller, static_cast<Button>(code)); break;
						case Method::touch:      break; /// @todo: Implement touch input method.
						case Method::simulated:  break; /// @todo: Implement simulated input method.
					}
					
					break;
				}
				
				case Type::hold:
				{
					switch (action.method)
					{
						case Method::mouse:      r = input.mouseHeld (            static_cast<Mouse >(code)); break;
						case Method::keyboard:   r = input.keyHeld   (            static_cast<Key   >(code)); break;
						case Method::controller: r = input.buttonHeld(controller, static_cast<Button>(code)); break;
						case Method::touch:      break; /// @todo: Implement touch input method.
						case Method::simulated:  break; /// @todo: Implement simulated input method.
					}
					
					break;
				}
				
				case Type::release:
				{
					switch (action.method)
					{
						case Method::mouse:      r = input.mouseReleased (            static_cast<Mouse >(code)); break;
						case Method::keyboard:   r = input.keyReleased   (            static_cast<Key   >(code)); break;
						case Method::controller: r = input.buttonReleased(controller, static_cast<Button>(code)); break;
						case Method::touch:      break; /// @todo: Implement touch input method.
						case Method::simulated:  break; /// @todo: Implement simulated input method.
					}
					
					break;
				}
				
				case Type::analogue:
				{
					switch (action.method)
					{
						case Method::mouse:      break; /// @todo: Do mice with analogue buttons exist and can SDL deal with them?
						case Method::keyboard:   break; /// @todo: Can SDL deal with analogue keyboards?
						
						case Method::controller:
						{
							event.scalar = input.axis(controller, static_cast<Axis>(code));
							
							if (std::abs(event.scalar) < action.deadzone)
								event.scalar = 0.0f;
							
							if (std::abs(event.scalar) > epsilon())
								// No need to check the rest of the codes if we have a match.
								return true;
							
							break;
						}
						
						case Method::touch:      break; /// @todo: Implement touch input method.
						case Method::simulated:  break; /// @todo: Implement simulated input method.
					}
				}
				
				case Type::direction:
				{
					switch (action.method)
					{
						/// @todo: Allow directional axes to be filtered out so that a directional input is only horizontal or vertical.
						// Mouse, keyboard and controller will be handled mostly similarly.
						case Method::mouse:
						case Method::keyboard:
						case Method::controller:
						{
							const auto  index (static_cast<std::size_t>(code));
							const auto &setups(scheme.setups());
							
							if (index >= setups.size())
								break;
							
							const auto &setup   (setups[index]);
							const auto  subcodes(setup.codes);
							
							event.vector.x = event.vector.y = 0.0f;
							
							float fx{1.0f}, fy{1.0f};
							
							switch (action.invert)
							{
								case Invert::none:                        break;
								case Invert::horizontal: fx      = -1.0f; break;
								case Invert::vertical:   fy      = -1.0f; break;
								case Invert::both:       fx = fy = -1.0f; break;
							}
							
							// This is wehere we get into the differences.
							if (Method::mouse == action.method)
							{
								enum Direction : std::size_t
								{
									hor,
									ver
								};
								
								// Lack of codes means check all the time.
								bool set[]
								{
									(0 == subcodes[hor].size()),
									(0 == subcodes[ver].size())
								};
								
								for (std::size_t i{hor}; i <= ver; ++ i)
								{
									for (Code const subcode : subcodes[i])
									{
										if (input.mouseHeld(static_cast<Mouse>(subcode)))
										{
											set[i] = true;
											break;
										}
									}
								}
								
								/// @todo: Maybe need to divide these values by something and take screen size and window scale into account?
								
								if (set[hor])
									event.vector.x += static_cast<float>(input.mouseXDelta()) * fx * action.sensitivity;
								
								if (set[ver])
									event.vector.y += static_cast<float>(input.mouseYDelta()) * fy * action.sensitivity;
							}
							else if (Method::keyboard == action.method)
							{
								enum Direction : std::size_t
								{
									left,
									right,
									up,
									down
								};
								
								for (std::size_t i{left}; i <= down; ++ i)
								{
									for (Code const subcode : subcodes[i])
									{
										if (input.keyHeld(static_cast<Key>(subcode)))
										{
											switch (i)
											{
												case left:  event.vector.x -= fx * action.sensitivity; break;
												case right: event.vector.x += fx * action.sensitivity; break;
												case up:    event.vector.y -= fy * action.sensitivity; break;
												case down:  event.vector.y += fy * action.sensitivity; break;
											}
											
											break;
										}
									}
								}
							}
							// Controller joystick.
							else if (res::Inputmap::Setup::Type::axes == setup.type)
							{
								enum Direction : std::size_t
								{
									hor,
									ver
								};
								
								bool set[]{false, false};
								
								for (std::size_t i{hor}; i <= ver; ++ i)
								{
									for (Code const subcode : subcodes[i])
									{
										if (set[i])
											break;
										
										const auto a(input.axis(controller, static_cast<Axis>(subcode)));
										
										switch (i)
										{
											case hor:
												set[i] = (std::abs((event.vector.x = fx * a * action.sensitivity)) > epsilon());
												break;
												
											case ver:
												set[i] = (std::abs((event.vector.y = fy * a * action.sensitivity)) > epsilon());
												break;
										}
									}
								}
							}
							// Controller buttons.
							else
							{
								enum Direction : std::size_t
								{
									left,
									right,
									up,
									down
								};
								
								for (std::size_t i{left}; i <= down; ++ i)
								{
									for (Code const subcode : subcodes[i])
									{
										if (input.buttonHeld(controller, static_cast<Button>(subcode)))
										{
											switch (i)
											{
												case left:  event.vector.x -= fx * action.sensitivity; break;
												case right: event.vector.x += fx * action.sensitivity; break;
												case up:    event.vector.y -= fy * action.sensitivity; break;
												case down:  event.vector.y += fy * action.sensitivity; break;
											}
											
											break;
										}
									}
								}
							}
							
							if (std::abs(event.vector.x) < action.deadzone)
								event.vector.x = 0.0f;
							
							if (std::abs(event.vector.y) < action.deadzone)
								event.vector.y = 0.0f;

								  mth::Scalf length   {mth::length(event.vector)};
							const mth::Scalf lengthOld{length};
							
							event.vector = mth::normalise(event.vector) * std::min(length, 1.0f);
							
							length = mth::length(event.vector);
							length = mth::max(0.0f, length - action.deadzone);
							
							if (action.deadzone < 1.0f)
								length /= (1.0f - action.deadzone);
							
							length = mth::clamp(length, 0.0f, 1.0f);
							event.vector = mth::normalise(event.vector) * length;
							
							if (!action.normalise)
								event.vector *= lengthOld;

							if (mth::length(event.vector) > epsilon())
								return true;
							
							break;
						}
						
						case Method::touch:     break; /// @todo: Implement touch input method.
						case Method::simulated: break; /// @todo: Implement simulated input method.
					}
				}
			}
			
			if (r)
				return true;
		}
		
		return false;
	}
}

namespace karhu
{
	namespace inp
	{
		SystemInput::SystemInput()
		{
			c_instance = this;
		}
		
		/**
		 * Sets whether an input layer should be enabled.
		 * Inputtables on a disabled input layer will not receive events.
		 * Does nothing if the layer does not exist.
		 *
		 * @param layer The layer to enable or disable.
		 * @param set   Whether to set the layer enabled.
		 */
		void SystemInput::layerEnabled(const Layer &layer, const bool set)
		{
			if (layer <= Defaultlayers::none)
				return;
			
			auto it(std::find
			(
				m_layersDisabled.begin(),
				m_layersDisabled.end(),
				layer
			));
			
			if (set && it != m_layersDisabled.end())
				m_layersDisabled.erase(it);
			else if (!set && it == m_layersDisabled.end())
				m_layersDisabled.emplace_back(layer);
		}
		
		/**
		 * Returns whether an input layer should be enabled.
		 * Inputtables on a disabled input layer will not receive events.
		 * Returns false if the layer does not exist.
		 *
		 * @param layer The layer to enable or disable.
		 *
		 * @return Whether the layer is enabled.
		 */
		bool SystemInput::layerEnabled(const Layer &layer) const
		{
			if (m_layerSolo > Defaultlayers::none && m_layerSolo != layer)
				return false;
			
			const auto it(std::find
			(
				m_layersDisabled.begin(),
				m_layersDisabled.end(),
				layer
			));
			
			return (it == m_layersDisabled.end());
		}
		
		void SystemInput::layerSolo(const Layer &layer)
		{
			m_layerSolo = layer;
		}
		
		void SystemInput::performUpdateComponents
		(
			      app::App              &app,
				  ecs::Pool<Inputtable> &components,
			const float                 dt,
			const float                 dtFixed,
			const float                 timestep
		)
		{
			auto &input(app.inp());
			
			// Check if controllers are disconnected or connected.
			
			for (IDController const ID : input.controllersDisconnected())
			{
				Player player{0};
				
				for (Players p{1}, ps{input.playersEnabled()}; ps; p <<= 1, ps >>= 1)
				{
					if (!(1 & ps))
						continue;
				
					if (auto IDForPlayer = input.controllerForPlayer(p))
					{
						if (ID == *IDForPlayer)
						{
							input.removeControllerForPlayer(p);
							player = p;
							break;
						}
					}
				}
				
				app.ecs().emitEvent<EConnection>
				(
					dt,
					dtFixed,
					timestep,
					EConnection::Type::disconnected,
					ID,
					"",
					player
				);
			}
			
			for (IDController const ID : input.controllersConnected())
			{
				auto c(input.controller(ID));
				
				if (!c)
					continue;
				
				app.ecs().emitEvent<EConnection>
				(
					dt,
					dtFixed,
					timestep,
					EConnection::Type::connected,
					ID,
					c->name().c_str(),
					0
				);
			}
			
			// Go through every active player.
			
			if (0 == input.playersEnabled())
				return;
			
			std::map<Player, std::vector<DataEvent>> actionHappened;
			
			for (Players p{1}, ps{input.playersEnabled()}; ps; p <<= 1, ps >>= 1)
			{
				if (!(1 & ps))
					continue;
				
				const auto &schemes(input.schemesForPlayer(p));
				
				if (0 == schemes.size())
					continue;
				
				actionHappened.emplace(p, std::vector<DataEvent>(schemes[0].actions().size()));
				
				const auto controller(input.controllerForPlayer(p));
				
				for (std::size_t i{0}; i < schemes.size(); ++ i)
				{
					const auto &scheme (schemes[i]);
					const auto &actions(scheme.actions());
					
					if (0 == actions.size())
						continue;
					
					for (std::size_t j{0}; j < actions.size(); ++ j)
					{
						const auto it(actionHappened.find(p));
						if (it != actionHappened.end())
							if (it->second[j].happened)
								continue;
					
						auto &action(actions[j]);
						
						switch (action.scope)
						{
							// Global scope is simpler, so handle separately.
							case Scope::global:
							{
								DataEvent &e(actionHappened[p][j]);
								
								e.happened = actionGlobalHappened
								(
									action,
									scheme,
									e,
									controller,
									((controller) ? *controller : 0),
									input
								);
								
								if (e.happened)
									e.indexScheme = i;

								break;
							}
							
							// Local scope involes raycasting.
							case Scope::local:
								/// @todo: Implement local input scope.
								break;
						}
					}
				}
			}
			
			// Time to construct our ordered list of inputtable anew each frame.
			
			m_layers.clear();
			
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);
				
				if (!c->activeInHierarchy())
					continue;
				
				auto it(m_layers.find(c->layer));
				
				if (it == m_layers.end())
					it = m_layers.emplace(c->layer, DataLayer{}).first;
				
				it->second.inputtables.emplace(c->priority, c);
			}
			
			// This will keep track of what actions have already
			// been dispatched at a higher priority than current.
			std::unordered_map<Action, std::unordered_map<Player, Priority>> dispatched;
			
			// Helper to check if the action was already dispatched.
			auto didActionGetDispatchedAtHigherPriority
			(
				[&dispatched]
				(
					const Action   &action,
					const Player   &player,
					const Priority &priority
				) -> bool
				{
					const auto it(dispatched.find(action));
					
					if (it == dispatched.end())
						return false;
					
					const auto jt(it->second.find(player));
					
					if (jt == it->second.end())
						return false;
					
					return (jt->second < priority);
				}
			);
			
			// Helper to store information about an action being dispatched.
			auto updateActionDispatchedAtPriority
			(
				[&dispatched]
				(
					const Action   &action,
					const Player   &player,
					const Priority &priority
				)
				{
					auto it(dispatched.find(action));
					
					if (it == dispatched.end())
						it = dispatched.emplace(action, std::unordered_map<Player, Priority>{}).first;
					
					auto jt(it->second.find(player));
					
					if (jt == it->second.end())
						it->second.emplace(player, priority);
					else if (priority > jt->second)
						jt->second = priority;
				}
			);
			
			// This is where we go through the layers to dispatch events.
			for (auto const &layer : m_layers)
			{
				// Ignore disabled layers.
				if (!layerEnabled(layer.first))
					continue;
				
				for (const auto &inputtable : layer.second.inputtables)
				{
					// Finally we get to the actual listener.
					Inputtable *const c{inputtable.second};
					
					// Go through every player to check.
					for (Players p{1}, ps{input.playersEnabled()}; ps; p <<= 1, ps >>= 1)
					{
						if (!(1 & ps))
							continue;
						
						const auto it(actionHappened.find(p));
						if (it == actionHappened.end())
							continue;
						
						const auto &schemes(input.schemesForPlayer(p));
						
						if (0 == schemes.size())
							continue;
						
						for (std::size_t i{0}; i < schemes[0].actions().size(); ++ i)
						{
							const Action a{action(i)};
							
							if (!(a & c->actions))
								continue;
							
							const DataEvent &e(it->second[i]);
							
							// Check if the action in question did happen.
							if (!e.happened)
								continue;
							
							auto &actions(schemes[it->second[i].indexScheme].actions());
							
							// Move on if a higher priority listener got to this action first.
							/// @todo: Allow for per-component passthrough setting?
							if (didActionGetDispatchedAtHigherPriority(a, p, c->priority))
								continue;
							
							// Finally time to dispatch the event.
							// We are not using the ECS system for this, since we only want to
							// inform the one inputtable found to listen for this exact action.
							app.ecs().emitEvent<EInput>
							(
								dt,
								dtFixed,
								timestep,
								c->entity(),
								app.ecs().IDOfTypeComponent<Inputtable>(),
								c->identifier(),
								action(i),
								p,
								actions[i].type,
								e.scalar,
								e.vector
							);
							
							updateActionDispatchedAtPriority(a, p, c->priority);
						}
					}
				}
			}
		}
		
		SystemInput *SystemInput::c_instance;
	}
}
