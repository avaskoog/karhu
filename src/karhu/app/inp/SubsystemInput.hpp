/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Load mappings on startup.
// https://github.com/gabomdq/SDL_GameControllerDB
// https://wiki.libsdl.org/SDL_GameControllerAddMappingsFromRW
// https://wiki.libsdl.org/SDL_RWFromMem

#ifndef KARHU_INPUT_SUBSYSTEM_INPUT_H_
	#define KARHU_INPUT_SUBSYSTEM_INPUT_H_

	#include <karhu/app/Subsystem.hpp>
	#include <karhu/app/Bitflagbank.hpp>
	#include <karhu/app/inp/common.hpp>
	#include <karhu/app/inp/Scheme.hpp>
	#include <karhu/core/log.hpp>
	#include <karhu/conv/maths.hpp>

	#include <array>
	#include <memory>
	#include <map>
	#include <cstdint>

	namespace karhu
	{
		namespace win
        {
			class Window;
		};

		namespace inp
		{
			class Layerbank : public app::Bitflagbank<Layer, Defaultlayers::custom, Defaultlayers::none>
			{
				public:
					char const *label()      override { return "normal"; }
					char const *descriptor() override { return "normal input layer"; }
			};
			
			class LayerbankGUI : public app::Bitflagbank<Layer, Defaultlayers::GUI + 1, Defaultlayers::none>
			{
				public:
					char const *label()      override { return "GUI"; }
					char const *descriptor() override { return "GUI input layer"; }
			};
			
			class Actionbank : public app::Bitflagbank<Action, action(0), 0>
			{
				public:
					char const *label()      override { return "actions"; }
					char const *descriptor() override { return "input action"; }
			};
			
			class SubsystemInput : public subsystem::Subsystem
			{
				public:
					bool init();
					void deinit();
					
					void beginFrame(win::Window &);
					void endFrame(win::Window &);
				
					Layerbank &layers() noexcept { return m_layers; }
					Layerbank const &layers() const noexcept { return m_layers; }
				
					LayerbankGUI &layersGUI() noexcept { return m_layersGUI; }
					LayerbankGUI const &layersGUI() const noexcept { return m_layersGUI; }
				
					Actionbank &actions() noexcept { return m_actions; }
					Actionbank const &actions() const noexcept { return m_actions; }
				
					void layerEnabled(const Layer &, const bool set);
					bool layerEnabled(const Layer &) const;
					void layerSolo(const Layer &);
				
					std::vector<Scheme> &schemesForPlayer(const Player &player);
				
					std::unique_ptr<Controller> createController(void *data)
					{
						return performCreateController(data);
					}
				
					void controllerForPlayer(const Player &, const IDController &);
					void removeControllerForPlayer(const Player &);
					Nullable<IDController> controllerForPlayer(const Player &) const;
				
					void playersEnabled(const Players &, const bool &set);
					const Players &playersEnabled() const noexcept { return m_playersEnabled; }
				
					void destroyController(void *data)
					{
						return performDestroyController(data);
					}
				
					const std::map<IDController, std::unique_ptr<Controller>> &controllers() const noexcept
					{
						return m_controllers;
					}
				
					const Controller *controller(const IDController &ID) const
					{
						const auto it(m_controllers.find(ID));
						
						if (it != m_controllers.end())
							return it->second.get();
						
						return nullptr;
					}
				
					/**
					 * Returns a list of identifiers of controllers connected since last frame.
					 *
					 * @return A vector containing all recently connected controller identifiers.
					 */
					const std::vector<IDController> &controllersConnected() const noexcept
					{
						return m_controllersConnected;
					}
				
					/**
					 * Returns a list of identifiers of controllers disconnected since last frame.
					 *
					 * @return A vector containing all recently disconnected controller identifiers.
					 */
					const std::vector<IDController> &controllersDisconnected() const noexcept
					{
						return m_controllersDisconnected;
					}
				
					const std::int32_t &mouseX() const { return m_mouseX; }
					const std::int32_t &mouseY() const { return m_mouseY; }
					const std::int32_t &mouseXDelta() const { return m_mouseXDelta; }
					const std::int32_t &mouseYDelta() const { return m_mouseYDelta; }
				
					virtual void mouseCentred(const bool set) = 0;
					virtual void mouseVisible(const bool set) = 0;

					bool mousePressed(const Mouse &value) const
					{
						return
						(
							!m_mouseLast[static_cast<std::size_t>(value)] &&
							 m_mouse    [static_cast<std::size_t>(value)]
						);
					}

					bool mouseHeld(const Mouse &value) const
					{
						return m_mouse[static_cast<std::size_t>(value)];
					}
				
					bool mouseReleased(const Mouse &value) const
					{
						return
						(
							 m_mouseLast[static_cast<std::size_t>(value)] &&
							!m_mouse    [static_cast<std::size_t>(value)]
						);
					}
				
					bool keyPressed(const Key &value) const
					{
						return
						(
							!m_keysLast[static_cast<std::size_t>(value)] &&
							 m_keys    [static_cast<std::size_t>(value)]
						);
					}

					bool keyHeld(const Key &value) const
					{
						return m_keys[static_cast<std::size_t>(value)];
					}
				
					bool keyReleased(const Key &value) const
					{
						return
						(
							 m_keysLast[static_cast<std::size_t>(value)] &&
							!m_keys    [static_cast<std::size_t>(value)]
						);
					}
				
					bool buttonPressed(const IDController &ID, const Button &value) const
					{
						const auto it(m_controllers.find(ID));
						if (it != m_controllers.end())
							return
							(
								!it->second->m_buttonsLast[static_cast<std::size_t>(value)] &&
								 it->second->m_buttons    [static_cast<std::size_t>(value)]
							);
						
						return false;
					}

					bool buttonHeld(const IDController &ID, const Button &value) const
					{
						const auto it(m_controllers.find(ID));
						if (it != m_controllers.end())
							return it->second->m_buttons[static_cast<std::size_t>(value)];
						
						return false;
					}
				
					bool buttonReleased(const IDController &ID, const Button &value) const
					{
						const auto it(m_controllers.find(ID));
						if (it != m_controllers.end())
							return
							(
								 it->second->m_buttonsLast[static_cast<std::size_t>(value)] &&
								!it->second->m_buttons    [static_cast<std::size_t>(value)]
							);
						
						return false;
					}
				
					mth::Scalf axis(const IDController &ID, const Axis &axis) const
					{
						const auto it(m_controllers.find(ID));
						if (it != m_controllers.end())
							return it->second->m_axes[static_cast<std::size_t>(axis)];
						
						return 0.0f;
					}
				
					void registerMouse(const std::int32_t &x, const std::int32_t &y)
					{
						m_mouseX = x;
						m_mouseY = y;
					}
				
					void registerMouseDelta(const std::int32_t &x, const std::int32_t &y)
					{
						m_mouseXDelta = x;
						m_mouseYDelta = y;
					}

					void registerMouseDown(const Mouse &value)
					{
						m_mouse[static_cast<std::size_t>(value)] = true;
					}
					
					void registerMouseUp(const Mouse &value)
					{
						m_mouse[static_cast<std::size_t>(value)] = false;
					}
					
					void registerKeyDown(const Key &value)
					{
						m_keys[static_cast<std::size_t>(value)] = true;
					}
					
					void registerKeyUp(const Key &value)
					{
						m_keys[static_cast<std::size_t>(value)] = false;
					}
				
					void registerController
					(
						const IDController &,
						std::unique_ptr<Controller> &&
					);
				
					void deregisterController(const IDController &ID);

					void registerButtonDown(const IDController &ID, const Button &value)
					{
						auto it(m_controllers.find(ID));
						if (it != m_controllers.end())
							it->second->m_buttons[static_cast<std::size_t>(value)] = true;
					}
					
					void registerButtonUp(const IDController &ID, const Button &value)
					{
						auto it(m_controllers.find(ID));
						if (it != m_controllers.end())
							it->second->m_buttons[static_cast<std::size_t>(value)] = false;
					}
				
					void registerAxis(const IDController &ID, const Axis &axis, const mth::Scalf &value)
					{
						auto it(m_controllers.find(ID));
						if (it != m_controllers.end())
							it->second->m_axes[static_cast<std::size_t>(axis)] = value;
					}

					~SubsystemInput();

				protected:
					virtual bool performInit()   = 0;
					virtual void performDeinit() = 0;
				
					virtual void performBeginFrame(win::Window &) = 0;
					virtual void performEndFrame  (win::Window &) = 0;
				
					virtual std::unique_ptr<Controller> performCreateController(void *data) = 0;
					virtual void performDestroyController(void *data) = 0;
				
				private:
					std::int32_t
						m_mouseX     {0},
						m_mouseY     {0},
						m_mouseXDelta{0},
						m_mouseYDelta{0};
				
					std::array<bool, static_cast<std::size_t>(Mouse::count)>
						m_mouse,
						m_mouseLast;
				
					std::array<bool, static_cast<std::size_t>(Key::count)>
						m_keys,
						m_keysLast;
				
					std::map<IDController, std::unique_ptr<Controller>>
						m_controllers;
				
					std::vector<IDController>
						m_controllersConnected,
						m_controllersDisconnected;
				
					std::map<Player, std::vector<Scheme>>
						m_schemesPerPlayer;
				
					std::map<Player, IDController>
						m_controllersPerPlayer;
				
					Players
						m_playersEnabled{player(0)};
				
					Layerbank
						m_layers;
				
					LayerbankGUI
						m_layersGUI;
				
					Actionbank
						m_actions;
			};
		}
	}
#endif
