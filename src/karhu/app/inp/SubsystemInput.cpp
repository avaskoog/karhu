/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/inp/SubsystemInput.hpp>
#include <karhu/app/inp/systems/SystemInput.hpp>
#include <karhu/core/platform.hpp>

namespace karhu
{
	namespace inp
	{
		bool SubsystemInput::init()
		{
			std::fill(m_mouse    .begin(), m_mouse    .end(), false);
			std::fill(m_mouseLast.begin(), m_mouseLast.end(), false);
			std::fill(m_keys     .begin(), m_keys     .end(), false);
			std::fill(m_keysLast .begin(), m_keysLast .end(), false);
			
			return performInit();
		}
		
		void SubsystemInput::deinit()
		{
			performDeinit();
		}
		
		void SubsystemInput::layerEnabled(const Layer &layer, const bool set)
		{
			SystemInput::c_instance->layerEnabled(layer, set);
		}
		
		bool SubsystemInput::layerEnabled(const Layer &layer) const
		{
			return SystemInput::c_instance->layerEnabled(layer);
		}
		
		void SubsystemInput::layerSolo(const Layer &layer)
		{
			return SystemInput::c_instance->layerSolo(layer);
		}
		
		void SubsystemInput::beginFrame(win::Window &w)
		{
			m_mouseLast  = m_mouse;
			m_keysLast   = m_keys;
			
			for (auto &it : m_controllers)
			{
				it.second->m_buttonsLast = it.second->m_buttons;
				it.second->m_axesLast    = it.second->m_axes;
			}
			
			performBeginFrame(w);
		}
		
		void SubsystemInput::endFrame(win::Window &w)
		{
			performEndFrame(w);
			
			m_controllersConnected.clear();
			m_controllersDisconnected.clear();
		}
		
		void SubsystemInput::playersEnabled(const Players &players, const bool &set)
		{
			if (set)
				m_playersEnabled |= players;
			else
				m_playersEnabled &= ~players;
		}
		
		/**
		 * Returns the schemes for the specified player,
		 * creating the list if one does not already exist.
		 * Schemes can then be addde and maps to them,
		 * but do not hold on to the reference.
		 *
		 * @param player The bitflag for the single player to create or get the list of schemes for.
		 *
		 * @return The schemes for the player specified.
		 */
		std::vector<Scheme> &SubsystemInput::schemesForPlayer(const Player &player)
		{
			auto it(m_schemesPerPlayer.find(player));
			
			if (it != m_schemesPerPlayer.end())
				return it->second;
			
			return m_schemesPerPlayer.emplace(player, std::vector<Scheme>{}).first->second;
		}
		
		void SubsystemInput::controllerForPlayer(const Player &p, const IDController &ID)
		{
			auto it(m_controllersPerPlayer.find(p));
			
			if (it == m_controllersPerPlayer.end())
				m_controllersPerPlayer.emplace(p, ID);
			else
				it->second = ID;
		}
		
		void SubsystemInput::removeControllerForPlayer(const Player &p)
		{
			auto it(m_controllersPerPlayer.find(p));
			
			if (it != m_controllersPerPlayer.end())
				m_controllersPerPlayer.erase(it);
		}
		
		Nullable<IDController> SubsystemInput::controllerForPlayer(const Player &p) const
		{
			const auto it(m_controllersPerPlayer.find(p));
			
			if (it == m_controllersPerPlayer.end())
				return {false};
			
			return {true, it->second};
		}
		
		void SubsystemInput::registerController
		(
			const IDController &ID,
			std::unique_ptr<Controller> &&c
		)
		{
			if (!c)
				return;
			
			c->m_ID = ID;
			
			log::msg("Karhu") << "Controller connected with identifier " << ID;
			
			m_controllers.emplace(ID, std::move(c));
			m_controllersConnected.emplace_back(ID);
		}
		
		void SubsystemInput::deregisterController(const IDController &ID)
		{
			auto it(m_controllers.find(ID));
			if (it != m_controllers.end())
			{
				log::msg("Karhu") << "Controller disconnected with identifier " << ID;
				m_controllers.erase(it);
				m_controllersDisconnected.emplace_back(ID);
			}
		}

		SubsystemInput::~SubsystemInput() {}
	}
}
