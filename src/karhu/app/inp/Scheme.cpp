/**
 * @author		Ava Skoog
 * @date		2019-01-07
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/inp/Scheme.hpp>
#include <karhu/app/App.hpp>
#include <karhu/res/Bank.hpp>

namespace karhu
{
	namespace inp
	{
		void Scheme::map(app::App &app, const res::IDResource &ID)
		{
			auto m(app.res().get<res::Inputmap>(ID));
			
			if (ID == m_map)
			{
				log::err("Karhu")
					<< "Failed to create scheme from input map "
					<< static_cast<int>(ID);
				return;
			}
			
			m_actions.clear();
			m_map = ID;
			
			// Make a copy of all the actions so that they
			// can be remapped without messing with the asset.
			if (m_map)
			{
				m_actions = m->actions;
				m_setups  = m->setups;
			}
		}
	}
}
