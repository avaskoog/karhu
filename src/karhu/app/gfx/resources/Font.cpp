/**
 * @author		Ava Skoog
 * @date		2018-11-28
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/resources/Font.hpp>
#include <karhu/core/log.hpp>

#include <karhu/app/lib/kirja/kirja.hpp>

namespace
{
	static kirja::Fontbank  fontbank;
	static kirja::Imagebank imagebank;
	/// @todo: Colourbank and others (Tokenbank?), once added to Kirja.
}

namespace karhu
{
	namespace gfx
	{
		Nullable<gfx::Pixelbuffer> renderTextToBitmap
		(
			const char               *string,
			const Textrendersettings &settings
		)
		{
			if (!settings.font)
				return {false};
			
			kirja::Rendersettings s;
			
			s.fontbank  = &fontbank;
			s.imagebank = &imagebank;
			/// @todo: Add additional banks if they come.
			s.font      =  settings.font->face();
			s.colour    =
			{
				static_cast<kirja::Byte>(mth::round(settings.colour.r * 255.0f)),
				static_cast<kirja::Byte>(mth::round(settings.colour.g * 255.0f)),
				static_cast<kirja::Byte>(mth::round(settings.colour.b * 255.0f)),
				static_cast<kirja::Byte>(mth::round(settings.colour.a * 255.0f))
			};
			/// @todo: Fill in the rest of the settings.
			
			auto bm(kirja::bitmapFromInput(string, s));
			
			if (!bm.success)
			{
				log::err("Karhu")
					<< "Failed to render bitmap from string with font: "
					<< bm.error;
				
				return {false};
			}
			
			gfx::Pixelbuffer r;
			
			r.reserve
			(
				static_cast<std::uint32_t>(bm.value.width),
				static_cast<std::uint32_t>(bm.value.height),
				gfx::Pixelbuffer::Format::RGBA
			);
			
			/// @todo: Can we safely memmove instead?
			std::memcpy(r.components.get(), bm.value.buffer.get(), bm.value.size());
			
			return {true, std::move(r)};
		}
		
		struct Font::Memberdata
		{
			// Need to be pointers for the addresses to remain
			// stable for Kirja in case the resource gets moved.
			std::unique_ptr<kirja::Font> fonts[4];
			
			// To keep track of the entry in the fontbank.
			std::string bankname;
		};
		
		Font::Font()
		:
		m_data{std::make_unique<Memberdata>()}
		{
		}
		
		const char *Font::face() const noexcept
		{
			return m_data->bankname.c_str();
		}
	
		// Loads and compiles the actual shader code.
		Status Font::performLoadInAdditionToMetadata
		(
			const conv::JSON::Obj *const metadata,
			const char *filepathRelativeToResourcepath,
			const char *,
			conv::AdapterFilesystemRead &filesystem
		)
		{
			using namespace std::literals::string_literals;
			
			m_data->bankname = filepathRelativeToResourcepath;
			
			if (metadata)
			{
				if (const auto weights = metadata->getObject("weights"))
				{
					for (const auto &weight : *weights)
					{
						kirja::Fontweight w;
						
						if      (weight.first == "regular")
							w = kirja::Fontweight::regular;
						else if (weight.first == "bold")
							w = kirja::Fontweight::bold;
						else if (weight.first == "italic")
							w = kirja::Fontweight::italic;
						else if (weight.first == "boldItalic")
							w = kirja::Fontweight::boldItalic;
						else
						{
							log::wrn("Karhu")
								<< "Invalid font weight '"
								<< weight.first
								<< "' for font '"
								<< filepathRelativeToResourcepath
								<< '\'';

							continue;
						}
						
						const auto f(weight.second.string());
						
						if (!f)
							return
							{
								false,
								"Invalid font weight address for weight '"s
								+ weight.first
								+ "' for font '"
								+ filepathRelativeToResourcepath +
								'\''
							};
						
						const auto contents(filesystem.readBytesFromResource(f->c_str()));

						if (!contents.success)
							return {false, std::move(contents.error)};

						auto r(kirja::fontFromMemory
						(
							reinterpret_cast<kirja::Byte *>(contents.value.data.get()),
							contents.value.size
						));
						
						if (!r.success)
							return {false, std::move(r.error)};

						// Create the actual font.
						auto &font(m_data->fonts[static_cast<std::size_t>(w)]);
						font = std::make_unique<kirja::Font>(std::move(r.value));
						
						// Add the entry to the bank.
						fontbank.add
						(
							filepathRelativeToResourcepath,
							font.get(),
							w
						);
					}
				}
				else
					return
					{
						false,
						"Missing object 'weights' in metadata for font '"s +
						filepathRelativeToResourcepath +
						'\''
					};
			}
			else
				return
				{
					false,
					"Invalid metadata for font '"s +
					filepathRelativeToResourcepath +
					'\''
				};
			
			if (!m_data->fonts[static_cast<std::size_t>(kirja::Fontweight::regular)])
				return
				{
					false,
					"No regular weight loaded for font '"s +
					filepathRelativeToResourcepath +
					'\''
				};
			
			return {true};
		}
		
		Font::~Font()
		{
			fontbank.remove(face());
		}
	}
}
