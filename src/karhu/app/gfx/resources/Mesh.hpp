/**
 * @author		Ava Skoog
 * @date		2017-10-22
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: If meshes are to be serialisable in the future (e.g. generating a resource after importing a glTF file), add de/serialisation methods here.

#ifndef KARHU_GRAPHICS_MESH_H_
	#define KARHU_GRAPHICS_MESH_H_

	#include <karhu/res/Mesh.hpp>

	#include <vector>

	namespace karhu
	{
		namespace gfx
		{
			class Mesh : public res::Mesh
			{
				protected:
					Status performInit
					(
						std::vector<std::string> const &extraPathsRelativeToSourcePath
					) override;
				
					Status performLoadInAdditionToMetadata
					(
						const conv::JSON::Obj *const metadata,
						const char *filepathRelativeToResourcepath,
						const char *suffix,
						conv::AdapterFilesystemRead &filesystem
					) override;
				
				private:
					std::string m_filepath;
			};
		}
	}
#endif
