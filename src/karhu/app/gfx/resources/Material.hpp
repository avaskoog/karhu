/**
 * @author		Ava Skoog
 * @date		2017-11-01
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Cache uniforms.

#ifndef KARHU_GRAPHICS_MATERIAL_H_
	#define KARHU_GRAPHICS_MATERIAL_H_

	#include <karhu/res/Material.hpp>

	namespace karhu
	{
		namespace gfx
		{
			/// @todo: Create a default shader so that we can at least see things if they do not have one?

			class Material : public res::Material
			{
				/// @todo: Anything left to add here..?
			};
		}
	}
#endif
