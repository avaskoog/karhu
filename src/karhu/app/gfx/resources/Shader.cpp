/**
 * @author		Ava Skoog
 * @date		2018-11-28
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/resources/Shader.hpp>

namespace karhu
{
	namespace gfx
	{
		// Loads and compiles the actual shader code.
		Status Shader::performLoadInAdditionToMetadata
		(
			const conv::JSON::Obj *const metadata,
			const char *filepathRelativeToResourcepath,
			const char *,
			conv::AdapterFilesystemRead &filesystem
		)
		{
			using namespace std::literals::string_literals;
			
			std::string
				path{filepathRelativeToResourcepath},
				pathDir;
			
			for (auto i{static_cast<std::ptrdiff_t>(path.length()) - 1}; i >= 0; -- i)
			{
				auto const index(static_cast<std::size_t>(i));
				
				if ('/' == path[index])
				{
					pathDir = path.substr(0, index + 1);
					break;
				}
			}
			
			// Set up a default type in case the metadata is missing one.
			auto type(Type::regular);
			
			if (metadata)
			{
				if (const auto v = metadata->getInt("type"))
					type = static_cast<Type>(*v);
				
				const auto builds(metadata->getArray("builds"));
				
				if (!builds || 0 == builds->count())
					return
					{
						false,
						(
							"No valid builds found in shader '"s +
							filepathRelativeToResourcepath +
							'\''
						)
					};
				
				std::vector<DataSource> dataSources;
				
				for (const auto &build : *builds)
				{
					const auto o(build.object());
					
					if (!o)
						continue; /// @todo: Error message? Warning?
					
					const auto sources(o->getArray("sources"));
					
					if (!sources || 0 == sources->count())
						return
						{
							false,
							(
								"Sources missing for shader '"s +
								filepathRelativeToResourcepath +
								'\''
							)
						};
					
					Format format;
					
					if (const auto f = o->getInt("format"))
						format = static_cast<Format>(*f);
					else
						return
						{
							false,
							(
								"No format found in build for shader '"s +
								filepathRelativeToResourcepath +
								'\''
							)
						};
					
					if (!supportsFormat(format))
						continue;
					
					for (const auto &source : *sources)
					{
						const auto o(source.object());
						
						if (!o)
							continue; /// @todo: Error message? Warning?
						
						DataSource ds;
						
						if (const auto m = o->getInt("mode"))
							ds.mode = static_cast<Mode>(*m);
						else
							return
							{
								false,
								(
									"Mode missing for build in shader '"s +
									filepathRelativeToResourcepath +
									'\''
								)
							};
						
						if (const auto f = o->getString("file"))
						{
							std::string const
								path{pathDir + *f};
							
							const auto s(filesystem.readStringFromResource(path.c_str()));
							
							if (!s.success)
								return
								{
									false,
									(
										"Failed to load built shader source for shader '"s +
										filepathRelativeToResourcepath +
										"': " +
										s.error
									)
								};
							
							ds.name = *f;
							ds.code = std::move(s.value);
						}
						else
							return
							{
								false,
								(
									"File path missing for build in shader '"s +
									filepathRelativeToResourcepath +
									'\''
								)
							};
						
						dataSources.emplace_back(std::move(ds));
					}
				}
				
				if (0 == dataSources.size())
					return
					{
						false,
						(
							"No sources found in shader '"s +
							filepathRelativeToResourcepath +
							'\''
						)
					};
				
				if (!addSources(dataSources))
					return
					{
						false,
						(
							"Could not add sources to shader '"s +
							filepathRelativeToResourcepath +
							'\''
						)
					};
				
				if (!bake())
					return
					{
						false,
						(
							"Failed to bake shader '"s +
							filepathRelativeToResourcepath +
							'\''
						)
					};
				
				return {true};
			}
			
			return
			{
				false,
				(
					"Invalid metadata for shader '"s +
					filepathRelativeToResourcepath +
					'\''
				)
			};
		}
	}
}
