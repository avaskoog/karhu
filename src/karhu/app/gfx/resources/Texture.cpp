/**
 * @author		Ava Skoog
 * @date		2018-11-26
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/resources/Texture.hpp>
#include <karhu/app/gfx/utilities.hpp>

namespace karhu
{
	namespace gfx
	{
		Status Texture::performInit
		(
			std::vector<std::string> const &extraPathsRelativeToSourcePath
		)
		{
			if (extraPathsRelativeToSourcePath.size() > 1)
				return {false, "A mesh resource takes only one source"};
			
			if (!extraPathsRelativeToSourcePath.empty())
				m_filepath = extraPathsRelativeToSourcePath[0];
			
			return {true};
		}
		
		// Loads the actual texture image data.
		Status Texture::performLoadInAdditionToMetadata
		(
			const conv::JSON::Obj *const,
			const char *filepathRelativeToResourcepath,
			const char *,
			conv::AdapterFilesystemRead &filesystem
		)
		{
			using namespace std::literals::string_literals;
			
			char const *const path
			{
				(!m_filepath.empty())
				?
					m_filepath.c_str()
				:
					filepathRelativeToResourcepath
			};
			
			const auto contents(filesystem.readBytesFromResource(path));
			
			if (!contents.success)
				return {false, std::move(contents.error)};
			
			auto buffer(loadPixelsFromFileInMemory(contents.value.bufferview()));
			
			if (!buffer.components)
				return
				{
					false,
					"Invalid texture file '"s +
					filepathRelativeToResourcepath +
					'\''
				};
			
			setFromBuffer(std::move(buffer));
			
			if (!bake())
				return {false, "Bake failed"};
			
			return {true};
		}
	}
}
