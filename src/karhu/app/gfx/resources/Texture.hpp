/**
 * @author		Ava Skoog
 * @date		2017-11-01
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_TEXTURE_H_
	#define KARHU_GRAPHICS_TEXTURE_H_

	#include <karhu/res/Texture.hpp>
	#include <karhu/core/log.hpp>

	#include <cstdint>
	#include <memory>
	#include <type_traits>

	namespace karhu
	{
		namespace gfx
		{
			class Texture : public res::Texture
			{
				public:
					/**
					 * Constructs a texture with the specified settings.
					 *
					 * @param type   The texture type; defaults to a normal colour texture if omitted.
					 * @param format The format (channel count); defaults to RGB if omitted; ignored if the type is not colour.
					 */
					Texture
					(
						const Type type = Type::colour,
						const Pixelbuffer::Format format = Pixelbuffer::Format::RGB
					)
					:
					res::Texture{type, format}
					{
					}
				
				protected:
					Status performInit
					(
						std::vector<std::string> const &extraPathsRelativeToSourcePath
					) override;
				
					Status performLoadInAdditionToMetadata
					(
						const conv::JSON::Obj *const metadata,
						const char *filepathRelativeToResourcepath,
						const char *suffix,
						conv::AdapterFilesystemRead &filesystem
					) override;
				
				private:
					std::string m_filepath;
			};
		}
	}
#endif
