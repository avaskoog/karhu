/**
 * @author		Ava Skoog
 * @date		2018-12-27
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/resources/Mesh.hpp>
#include <karhu/app/gfx/utilities.hpp>
#include <karhu/core/log.hpp>

namespace karhu
{
	namespace gfx
	{
		Status Rig::performInit
		(
			std::vector<std::string> const &extraPathsRelativeToSourcePath
		)
		{
			if (extraPathsRelativeToSourcePath.size() > 1)
				return {false, "A rig resource takes only one source"};
			
			if (!extraPathsRelativeToSourcePath.empty())
				m_filepath = extraPathsRelativeToSourcePath[0];
			
			return {true};
		}
		
		Status Rig::performLoadInAdditionToMetadata
		(
			const conv::JSON::Obj *const metadata,
			const char *filepathRelativeToResourcepath,
			const char *,
			conv::AdapterFilesystemRead &filesystem
		)
		{
			reset();
			
			if (!metadata)
				return {false, "Missing rig metadata"};
			
			char const *const path
			{
				(!m_filepath.empty())
				?
					m_filepath.c_str()
				:
					filepathRelativeToResourcepath
			};
			
			auto s(filesystem.readStringFromResource(path));
			
			if (!s.success)
				return {false, std::move(s.error)};
			
			const auto name(metadata->getString("name"));
			
			if (!name)
				return {false, "Missing name in rig metadata file"};
			
			auto rs(loadRigByNameFromGLTFInMemory
			(
				name->c_str(),
				s.value.c_str(),
				s.value.length())
			);
			
			if (!rs)
				return {false, "Failed to load rig from glTF"};
			
			/// @todo: Fix this so it doesn't set identifier manually which should not be possible anyway, or at least make identifier a protected member instead to get rid of setIdentifier().
			const auto ID(identifier());
			*this = std::move(*rs);
			setIdentifier(ID);
			
			Status
				status{bake()};
			
			if (!status.success)
				return status;
			
//			for (std::size_t i{0}; i < roots.size(); ++ i)
//				if (roots[i])
//					log::msg("DEBUG") << "root " << i << ": " << roots[i]->name;
			
			return {true};
		}
	}
}
