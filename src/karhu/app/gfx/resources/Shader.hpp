/**
 * @author		Ava Skoog
 * @date		2017-10-22
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_SHADER_H_
	#define KARHU_GRAPHICS_SHADER_H_

	#include <karhu/res/Shader.hpp>
	#include <karhu/app/gfx/components/Camera.hpp>
	#include <karhu/app/gfx/Workload.hpp>

	#include <cstdint>

	namespace karhu
	{
		namespace gfx
		{
			class Shader : public res::Shader
			{
				public:
					Shader(const Type type = Type::regular)
					:
					res::Shader{type}
					{
					}
				
				protected:
					Status performLoadInAdditionToMetadata
					(
						const conv::JSON::Obj *const metadata,
						const char *filepathRelativeToResourcepath,
						const char *suffix,
						conv::AdapterFilesystemRead &filesystem
					) override;
			};
		}
	}
#endif
