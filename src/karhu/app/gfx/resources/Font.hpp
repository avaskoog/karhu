/**
 * @author		Ava Skoog
 * @date		2019-05-20
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_FONT_H_
	#define KARHU_GRAPHICS_FONT_H_

	#include <karhu/res/Font.hpp>

	namespace karhu
	{
		namespace gfx
		{
			struct Textrendersettings
			{
				res::Font *font{nullptr}; /// @todo: Build a default font into Karhu?
				ColRGBAf colour{1.0f, 1.0f, 1.0f, 1.0f};
				/// @todo: Fill in...
			};
			
			/**
			 * Tries to render text to a bitmap using the settings provided.
			 *
			 * @param string   The text to render, including XML formatting.
			 * @param settings The settings for rendering.
			 *
			 * @return A nullable struct containing the rendered bitmap on success.
			 */
			Nullable<Pixelbuffer> renderTextToBitmap
			(
				const char               *string,
				const Textrendersettings &settings
			);
			
			class Font : public res::Font
			{
				private:
					struct Memberdata;
				
				public:
					Font();
				
					const char *face() const noexcept override;
				
					~Font();
				
				protected:
					Status performLoadInAdditionToMetadata
					(
						const conv::JSON::Obj *const metadata,
						const char *filepathRelativeToResourcepath,
						const char *suffix,
						conv::AdapterFilesystemRead &filesystem
					) override;
				
				private:
					std::unique_ptr<Memberdata> m_data;
			};
		}
	}
#endif
