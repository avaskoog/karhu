/**
 * @author		Ava Skoog
 * @date		2017-11-04
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: If rigs are to be serialisable in the future (e.g. generating a resource after importing a glTF file), add de/serialisation methods here.
/// @todo: Limit number of joints to match limit in shader?

#ifndef KARHU_GRAPHICS_RIG_H_
	#define KARHU_GRAPHICS_RIG_H_

	#include <karhu/res/Rig.hpp>

	namespace karhu
	{
		namespace gfx
		{
			class Rig : public res::Rig
			{
					Status performInit
					(
						std::vector<std::string> const &extraPathsRelativeToSourcePath
					) override;
				
					Status performLoadInAdditionToMetadata
					(
						const conv::JSON::Obj *const metadata,
						const char *filepathRelativeToResourcepath,
						const char *suffix,
						conv::AdapterFilesystemRead &filesystem
					) override;
					
					/// @todo: Implement these if necessary.
					void performSerialise(conv::Serialiser &) const override {}
					void performDeserialise(const conv::Serialiser &) override {}
				
				private:
					std::string m_filepath;
			};
		}
	}
#endif
