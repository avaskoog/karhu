/**
 * @author		Ava Skoog
 * @date		2018-12-27
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/resources/Mesh.hpp>
#include <karhu/app/gfx/utilities.hpp>
#include <karhu/core/log.hpp>

namespace karhu
{
	namespace gfx
	{
		Status Animation::performInit
		(
			std::vector<std::string> const &extraPathsRelativeToSourcePath
		)
		{
			if (extraPathsRelativeToSourcePath.size() > 1)
				return {false, "An animation resource takes only one source"};
			
			if (!extraPathsRelativeToSourcePath.empty())
				m_filepath = extraPathsRelativeToSourcePath[0];
			
			return {true};
		}
		
		Status Animation::performLoadInAdditionToMetadata
		(
			const conv::JSON::Obj *const metadata,
			const char *filepathRelativeToResourcepath,
			const char *,
			conv::AdapterFilesystemRead &filesystem
		)
		{
			reset();
			
			if (!metadata)
				return {false, "Missing animation metadata"};
			
			char const *const path
			{
				(!m_filepath.empty())
				?
					m_filepath.c_str()
				:
					filepathRelativeToResourcepath
			};
			
			auto s(filesystem.readStringFromResource(path));
			
			if (!s.success)
				return {false, std::move(s.error)};
			
			const auto name(metadata->getString("name"));
			
			if (!name)
				return {false, "Missing name in animation metadata file"};
			
			auto as(loadAnimationByNameFromGLTFInMemory
			(
				name->c_str(),
				s.value.c_str(),
				s.value.length())
			);
			
			if (!as)
				return {false, "Failed to load animation from glTF"};
			
			/// @todo: Fix this so it doesn't set identifier manually which should not be possible anyway, or at least make identifier a protected member instead to get rid of setIdentifier().
			const auto ID(identifier());
			*this = std::move(*as);
			setIdentifier(ID);
			
			if (!bake())
				return {false, "Bake failed"};
			
			return {true};
		}
	}
}
