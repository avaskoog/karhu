/**
 * @author		Ava Skoog
 * @date		2018-12-27
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/resources/Mesh.hpp>
#include <karhu/app/gfx/utilities.hpp>
#include <karhu/core/log.hpp>

namespace karhu
{
	namespace gfx
	{
		Status Mesh::performInit
		(
			std::vector<std::string> const &extraPathsRelativeToSourcePath
		)
		{
			if (extraPathsRelativeToSourcePath.size() > 1)
				return {false, "A mesh resource takes only one source"};
			
			if (!extraPathsRelativeToSourcePath.empty())
				m_filepath = extraPathsRelativeToSourcePath[0];
			
			return {true};
		}
		
		Status Mesh::performLoadInAdditionToMetadata
		(
			const conv::JSON::Obj *const metadata,
			const char *filepathRelativeToResourcepath,
			const char *,
			conv::AdapterFilesystemRead &filesystem
		)
		{
			reset();
			
			if (!metadata)
				return {false, "Missing mesh metadata"};
			
			char const *const path
			{
				(!m_filepath.empty())
				?
					m_filepath.c_str()
				:
					filepathRelativeToResourcepath
			};
			
			auto s(filesystem.readStringFromResource(path));
			
			if (!s.success)
				return {false, std::move(s.error)};
			
			auto gs(loadGeometryFromGLTFInMemory(s.value.c_str(), s.value.length()));
			
			if (!gs)
				return {false, "Failed to load geometry from glTF"};
			
			auto &geometry(createGeometryAtNextLOD());
			geometry = std::move(*gs);
			
			if (!bake())
				return {false, "Bake failed"};
			
			return {true};
		}
	}
}
