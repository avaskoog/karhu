/**
 * @author		Ava Skoog
 * @date		2017-11-04
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: If animations are to be serialisable in the future (e.g. generating a resource after importing a glTF file), add de/serialisation methods here.

#ifndef KARHU_GRAPHICS_ANIMATION_H_
	#define KARHU_GRAPHICS_ANIMATION_H_

	#include <karhu/res/Animation.hpp>

	namespace karhu
	{
		namespace gfx
		{
			class Animation : public res::Animation
			{
				protected:
					Status performInit
					(
						std::vector<std::string> const &extraPathsRelativeToSourcePath
					) override;
				
					Status performLoadInAdditionToMetadata
					(
						const conv::JSON::Obj *const metadata,
						const char *filepathRelativeToResourcepath,
						const char *suffix,
						conv::AdapterFilesystemRead &filesystem
					) override;
				
					/// @todo: Implement these if necessary.
					void performSerialise(conv::Serialiser &) const override {}
					void performDeserialise(const conv::Serialiser &) override {}
				
				private:
					std::string m_filepath;
			};
		}
	}
#endif
