/**
 * @author		Ava Skoog
 * @date		2019-03-13
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/SubsystemGraphics.hpp>
#include <karhu/app/gfx/components/Renderdata2D.hpp>
#include <karhu/app/gfx/components/Renderdata3D.hpp>
#include <karhu/app/gfx/components/Particlesystem.hpp>

namespace karhu
{
	namespace gfx
	{
		void SubsystemGraphics::modeRendersize(ModeRendersize const mode)
		{
			m_modeRendersize = mode;
		}
		
		void SubsystemGraphics::rendersizeCustom
		(
			mth::Scalu const width,
			mth::Scalu const height
		)
		{
			m_rendersizeCustom.x = width;
			m_rendersizeCustom.y = height;
		}
		
		void SubsystemGraphics::rendersizeCustom(mth::Vec2u const &size)
		{
			m_rendersizeCustom = size;
		}
		
		void SubsystemGraphics::renderWithPipelineToCameraFromECS
		(
			app::App &app,
			Pipeline &pipeline,
			Camera &camera,
			ecs::World &world,
			const float dt,
			const float dtFixed,
			const float timestep
		)
		{
			if (!m_renderer || !camera.target() || !camera.target()->valid())
				return;
			
			Workload workload;
			
			if (auto t = world.componentInEntity<Transform>(camera.entity()))
			{
				Pass pass;
				pass.camera = &camera;
				pass.transformCamera = t;
				workload.passes.emplace_back(std::move(pass));
			}
			
			/// @todo: This all feels very prone to errors if there are more things that get added that need to be added separately to a workload and we forget to update here (writing this comment after, in fact, having forgotten for a long time to add 2D renderables and particlesystems).
			
			for (auto &components : world.componentpoolsInActiveScenes<Renderdata2D>())
				workload.addFromECS
				(
					app,
					world,
					components,
					dt,
					dtFixed,
					timestep
				);
			
			for (auto &components : world.componentpoolsInActiveScenes<Renderdata3D>())
				workload.addFromECS
				(
					app,
					world,
					components,
					dt,
					dtFixed,
					timestep
				);
			
			for (auto &components : world.componentpoolsInActiveScenes<Particlesystem>())
				workload.addFromECS
				(
					app,
					world,
					components,
					dt,
					dtFixed,
					timestep
				);
			
			workload.sort();
			
			pipeline.render
			(
				app,
				*m_renderer,
				workload,
				m_inputs,
				camera.target()->width(),
				camera.target()->height(),
				1.0f,
				1.0f
			);
		}
	}
}
