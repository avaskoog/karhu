/**
 * @author		Ava Skoog
 * @date		2017-11-03
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/Rendertarget.hpp>

namespace karhu
{
	namespace gfx
	{
		bool Rendertarget::bake()
		{
			m_baked = false;

			for (auto &t : m_texturesColour)
				t->reserveNull(m_width, m_height);

			if (m_textureDepth)
				m_textureDepth->reserveNull(m_width, m_height);

			if (m_textureStencil)
				m_textureStencil->reserveNull(m_width, m_height);

			// Try to bake the textures first.

			for (auto & t : m_texturesColour)
				if (!t->bake())
					return false;

			if (m_textureDepth && !m_textureDepth->bake())
				return false;

			if (m_textureStencil && !m_textureStencil->bake())
				return false;

			// Now try to bake the actual rendertarget.
			return (m_baked = performBake());
		}

		bool Rendertarget::bind
		(
			std::uint32_t const widthWindow,
			std::uint32_t const heightWindow,
			float         const scaleWindowX,
			float         const scaleWindowY
		)
		{
			if (!updateSize(widthWindow, heightWindow, scaleWindowX, scaleWindowY))
				return false;
			else if (!valid())
				return false;

			return performBind();
		}
		
		bool Rendertarget::updateSize
		(
			std::uint32_t const widthViewport,
			std::uint32_t const heightViewport,
			float         const factorX,
			float         const factorY
		)
		{
			if (!m_tiedToViewport)
				return true;
			
			// Check if we need to update the size based on the viewport size.

			auto const
				w(static_cast<std::uint32_t>(std::round(static_cast<float>(widthViewport)  * factorX))),
				h(static_cast<std::uint32_t>(std::round(static_cast<float>(heightViewport) * factorY)));

			if (w != m_width || h != m_height)
			{
				size(w, h);
				
				if (!bake())
					return false;
			}
			
			return true;
		}
		
		bool Rendertarget::texture(res::Texture &texture)
		{
			if
			(
				res::Texture::Type::colour != texture.type() &&
				res::Texture::Type::data   != texture.type()
			)
			{
				log::err("Karhu") << "Failed setting rendertarget colour texture: texture type is not 'colour' or 'data'";
				return false;
			}

			m_texturesColour.emplace_back(&texture);
			return true;
		}
		
		bool Rendertarget::depth(res::Texture &texture)
		{
			if
			(
				res::Texture::Type::depth       != texture.type() &&
				res::Texture::Type::shadowdepth != texture.type()
			)
			{
				log::err("Karhu") << "Failed setting rendertarget depth texture: texture type is not 'depth' or 'shadowdepth'";
				m_textureDepth = nullptr;
				return false;
			}

			m_textureDepth = &texture;
			return true;
		}
		
		bool Rendertarget::stencil(res::Texture &texture)
		{
			if (res::Texture::Type::stencil != texture.type())
			{
				log::err("Karhu") << "Failed setting rendertarget stencil texture: texture type is not 'stencil'";
				m_textureStencil = nullptr;
				return false;
			}

			m_textureStencil = &texture;
			return true;
		}
	}
}
