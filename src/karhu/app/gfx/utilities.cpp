/**
 * @author		Ava Skoog
 * @date		2017-11-01
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/utilities.hpp>
#include <karhu/core/log.hpp>

#define STBI_FAILURE_USERMSG
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define TINYGLTF_IMPLEMENTATION
#include <karhu/app/gfx/lib/tiny_gltf.h>
//#include <karhu/app/gfx/lib/stb_image.h> // tiny_gltf already loads stb_image

#include <cstring>

namespace
{
	using namespace karhu;
	using namespace karhu::gfx;
	
	static Pixelbuffer createBufferFromPixelsAndFree
	(
		stbi_uc *pixels,
		const int w,
		const int h,
		const int channels
	)
	{
		Pixelbuffer result;

		if (pixels)
		{
			result.reserve
			(
				static_cast<std::uint32_t>(w),
				static_cast<std::uint32_t>(h),
				static_cast<Pixelbuffer::Format>(channels)
			);
			
			std::memcpy
			(
				result.components.get(),
				pixels,
				result.size()
			);
			
			stbi_image_free(pixels);
		}
		else
		{
			log::err("Karhu")
				<< "Failed to load pixels from file in memory: "
				<< stbi_failure_reason();
				
			return result;
		}

		return result;
	}
	
	struct DataGLTF
	{
		tinygltf::Model model;
		const tinygltf::Primitive &primitive() const { return model.meshes[0].primitives[0]; }
	};
	
	Nullable<DataGLTF> loadGLTFAsString(const char *string, const std::size_t &length)
	{
		tinygltf::TinyGLTF loader;
		DataGLTF result;
		std::string err, warn;

		// Try to load.
		/// @todo: What about the last parameter?
		if (!loader.LoadASCIIFromString
		(
			&result.model,
			&err,
			&warn,
			string,
			static_cast<unsigned int>(length),
			"",
			tinygltf::REQUIRE_ALL
		))
		{
			log::err("Karhu") << "Failed to load glTF from string: " << err;
			return {};
		}
		
		if (0 != warn.length())
			log::err("Karhu") << "glTF warning: " << warn;
		
		// Find the mesh.
		/// @todo: Should we check more than one?
		if (0 == result.model.meshes.size())
		{
			log::err("Karhu")
				<< "Failed to load glTF! No mesh found.";
			return {};
		}
		
		// Find the first primitive.
		/// @todo: Should we check more than one?
		if (0 == result.model.meshes[0].primitives.size())
		{
			log::err("Karhu")
				<< "Failed to load glTF! No primitives in mesh found.";
			return {};
		}

		return {true, result};
	}
	
	Nullable<DataGLTF> loadGLTFAsFile(const char *file)
	{
		tinygltf::TinyGLTF loader;
		DataGLTF result;
		std::string err, warn;

		// Try to load.
		/// @todo: What about the last parameter?
		if (!loader.LoadASCIIFromFile
		(
			&result.model,
			&err,
			&warn,
			file,
			tinygltf::REQUIRE_ALL
		))
		{
			log::err("Karhu") << "Failed to load glTF from string: " << err;
			return {};
		}
		
		if (0 != warn.length())
			log::err("Karhu") << "glTF warning: " << warn;
		
		// Find the mesh.
		/// @todo: Should we check more than one?
		if (0 == result.model.meshes.size())
		{
			log::err("Karhu")
				<< "Failed to load glTF! No mesh found.";
			return {};
		}
		
		// Find the first primitive.
		/// @todo: Should we check more than one?
		if (0 == result.model.meshes[0].primitives.size())
		{
			log::err("Karhu")
				<< "Failed to load glTF! No primitives in mesh found.";
			return {};
		}

		return {true, result};
	}
}

namespace karhu
{
	namespace gfx
	{
		Nullable<Geometry> loadGeometryFromGLTFInMemory
		(
			const char *string,
			const std::size_t &length
		)
		{
			/// @todo: Update to check multiple models/primitives.
			
			Geometry r;
			
			auto const
				s(loadGLTFAsString(string, length));
			
			if (!s)
				return {false};
			
			auto const
				&data(*s);
			
			std::function<mth::Mat4f(tinygltf::Node const &, std::size_t const, bool const)>
				getTransformForNode;
			
			getTransformForNode = [&data, &getTransformForNode](auto node, auto index, auto recurse)
			{
				mth::Transformf transform;
				
				if (3 == node.translation.size())
				{
					transform.position.x = static_cast<float>(node.translation[0]);
					transform.position.y = static_cast<float>(node.translation[1]);
					transform.position.z = static_cast<float>(node.translation[2]);
				}
				
				if (3 == node.scale.size())
				{
					transform.scale.x = static_cast<float>(node.scale[0]);
					transform.scale.y = static_cast<float>(node.scale[1]);
					transform.scale.z = static_cast<float>(node.scale[2]);
				}
				
				if (4 == node.rotation.size())
				{
					transform.rotation.x = static_cast<float>(node.rotation[0]);
					transform.rotation.y = static_cast<float>(node.rotation[1]);
					transform.rotation.z = static_cast<float>(node.rotation[2]);
					transform.rotation.w = static_cast<float>(node.rotation[3]);
				}
				
				mth::Mat4f
					matrix{transform.matrix()};
				
				if (recurse)
				{
					for (std::size_t i{0}; i < data.model.nodes.size(); ++ i)
					{
						auto const
							&other(data.model.nodes[i]);
						
						auto const
							it(std::find(other.children.begin(), other.children.end(), index));
						
						if (it != other.children.end())
						{
							mth::Mat4f const
								matrixParent{getTransformForNode(other, i, true)};
							
							matrix = matrixParent * matrix;
						}
					}
				}
				
				return matrix;
			};
			
			for (std::size_t indexNode{0}; indexNode < data.model.nodes.size(); ++ indexNode)
			{
				auto const
					&node(data.model.nodes[indexNode]);
				
				if (0 > node.mesh)
					continue;
					
				auto const
					&mesh(data.model.meshes[static_cast<std::size_t>(node.mesh)]);
				
				bool const
					recurseMatrix{(0 == data.model.skins.size())};
				
				mth::Mat4f const
					matrix{getTransformForNode(node, indexNode, recurseMatrix)};
				
				for (auto const &primitive : mesh.primitives)
				{
					Geometry geometry;
					
					// Now get the indices.
					/// @todo: Deal with strides other than zero?
					{
						// Get the buffer information and data.
						/// @todo: Validate that the accessor and buffer view indices exist?
						const auto &accessor(data.model.accessors[static_cast<std::size_t>(primitive.indices)]);
						const auto &view    (data.model.bufferViews[static_cast<std::size_t>(accessor.bufferView)]);
						const auto &buf     (data.model.buffers[static_cast<std::size_t>(view.buffer)]);
						
						geometry.indices.resize(accessor.count);
						
						/// @todo: Create helper function to copy based on component type so we don't have to use a bunch of ifs.
						if (TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT == accessor.componentType)
						{
							std::vector<std::uint16_t> v(accessor.count);
							std::memcpy(v.data(), buf.data.data() + view.byteOffset, view.byteLength);
							
							for (std::size_t i{0}; i < accessor.count; ++ i)
								geometry.indices[i] = static_cast<Index>(v[i]);
						}
						else if (TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT == accessor.componentType)
						{
							std::vector<std::uint32_t> v(accessor.count);
							std::memcpy(v.data(), buf.data.data() + view.byteOffset, view.byteLength);
							
							for (std::size_t i{0}; i < accessor.count; ++ i)
								geometry.indices[i] = static_cast<Index>(v[i]);
						}
					}

					// Go through the attributes.
					/// @todo: Check all attriutes and not just position, normal and UV.
					for (const auto &attr : primitive.attributes)
					{
						// Get the buffer information and data.
						/// @todo: Deal with strides other than zero?
						/// @todo: Validate that the accessor and buffer view indices exist?
						const auto &accessor(data.model.accessors[static_cast<std::size_t>(attr.second)]);
						const auto &view    (data.model.bufferViews[static_cast<std::size_t>(accessor.bufferView)]);
						const auto &buf     (data.model.buffers[static_cast<std::size_t>(view.buffer)]);
						
						if (attr.first == "POSITION")
						{
							geometry.positions.resize(accessor.count);
							
							std::memcpy
							(
								geometry.positions.data(),
								buf.data.data() + view.byteOffset,
								view.byteLength
							);
							
							for (mth::Vec3f &p : geometry.positions)
								p = (matrix * mth::Vec4f{p, 1.0f}).xyz();
						}
						else if (attr.first == "NORMAL")
						{
							geometry.normals.resize(accessor.count);
							
							std::memcpy
							(
								geometry.normals.data(),
								buf.data.data() + view.byteOffset,
								view.byteLength
							);
							
							auto const
								nmatrix{mth::transpose(mth::inverse(mth::Mat3f{matrix}))};
							
							for (mth::Vec3f &n : geometry.normals)
								n = nmatrix * n;
						}
						else if (attr.first == "TANGENT")
						{
							geometry.tangents.resize(accessor.count);
							
							std::memcpy
							(
								geometry.tangents.data(),
								buf.data.data() + view.byteOffset,
								view.byteLength
							);
						}
						else if (attr.first == "COLOR_0")
						{
							std::size_t
								stride;
						
							if (TINYGLTF_COMPONENT_TYPE_FLOAT == accessor.componentType)
								stride = sizeof(float);
							else if (TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE == accessor.componentType)
								stride = sizeof(std::uint8_t);
							else // TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT
								stride = sizeof(std::uint16_t);
							
							std::size_t
								countComponents;
							
							if (TINYGLTF_TYPE_VEC3 == accessor.type)
								countComponents = 3;
							else // TINYGLTF_TYPE_VEC4
								countComponents = 4;
							
							geometry.colours.resize(accessor.count);
						
							unsigned char const
								*data{buf.data.data() + view.byteOffset};
							
							for (std::size_t i{0}; i < accessor.count * countComponents; i += countComponents)
							{
								ColRGBAf
									&col{geometry.colours[i / countComponents]};
								
								unsigned char const
									*dataOffset{data + i * stride};
								
								if (TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE == accessor.componentType)
								{
									col.r = (*(dataOffset + 0)) / 255;
									col.g = (*(dataOffset + 1)) / 255;
									col.b = (*(dataOffset + 2)) / 255;
									col.a = (4 == countComponents) ? ((*(dataOffset + 3)) / 255) : 1.0f;
								}
								else if (TINYGLTF_COMPONENT_TYPE_FLOAT == accessor.componentType)
								{
									auto
										*dataFloat{reinterpret_cast<float const *>(dataOffset)};
									
									col.r = *(dataFloat + 0);
									col.g = *(dataFloat + 1);
									col.b = *(dataFloat + 2);
									col.a = (4 == countComponents) ? *(dataFloat + 3) : 1.0f;
								}
								else // TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT
								{
									auto
										*dataShort{reinterpret_cast<std::uint16_t const *>(dataOffset)};
									
									col.r = (*(dataShort + 0)) / 255;
									col.g = (*(dataShort + 1)) / 255;
									col.b = (*(dataShort + 2)) / 255;
									col.a = (4 == countComponents) ? ((*(dataShort + 3)) / 255) : 1.0f;
								}
							}
						} /// @todo: COLOR_1 etc?
						else if (attr.first == "TEXCOORD_0") /// @todo: Ignore index? Check all indices?
						{
							/// @todo: Texcoords can also be unsigned byte/short but is the normalised which needs to be figured out and dealt with.
							
							geometry.UVs.resize(accessor.count);
							
							std::memcpy
							(
								geometry.UVs.data(),
								buf.data.data() + view.byteOffset,
								view.byteLength
							);
						}
						else
						{
							constexpr char const
								*strJoints {"JOINTS_"},
								*strWeights{"WEIGHTS_"};
							
							/// @todo: constexpr
							std::size_t const
								lenJoints {std::strlen(strJoints)},
								lenWeights{std::strlen(strWeights)};
							
							if (attr.first.length() > lenJoints && attr.first.substr(0, lenJoints) == strJoints)
							{
								std::size_t
									stride;
								
								if (TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE == accessor.componentType)
									stride = sizeof(std::uint8_t);
								else // TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT
									stride = sizeof(std::uint16_t);
								
								std::vector<Jointset>
									*jointsets;
								
								if (attr.first == "JOINTS_0")
									jointsets = &geometry.jointsets0;
								else if (attr.first == "JOINTS_1")
									jointsets = &geometry.jointsets1;
								else // JOINTS_2
									jointsets = &geometry.jointsets2;
								
								jointsets->resize(accessor.count);
								
								unsigned char const
									*data{buf.data.data() + view.byteOffset};
								
								for (std::size_t i{0}; i < accessor.count * 4; i += 4)
								{
									Jointset
										&set{(*jointsets)[i / 4]};
									
									unsigned char const
										*dataOffset{data + i * stride};
									
									if (TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE == accessor.componentType)
									{
										set.x = *(dataOffset + 0);
										set.y = *(dataOffset + 1);
										set.z = *(dataOffset + 2);
										set.w = *(dataOffset + 3);
									}
									else // TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT
									{
										auto
											*dataShort{reinterpret_cast<std::uint16_t const *>(dataOffset)};
										
										set.x = *(dataShort + 0);
										set.y = *(dataShort + 1);
										set.z = *(dataShort + 2);
										set.w = *(dataShort + 3);
									}
								}
							}
							else if (attr.first.length() > lenWeights && attr.first.substr(0, lenWeights) == strWeights)
							{
								/// @todo: Weights can also be unsigned byte/short but is the normalised which needs to be figured out and dealt with.
								
								std::vector<Weightset>
									*weightsets;
								
								if (attr.first == "WEIGHTS_0")
									weightsets = &geometry.weightsets0;
								else if (attr.first == "WEIGHTS_1")
									weightsets = &geometry.weightsets1;
								else // WEIGHTS_2
									weightsets = &geometry.weightsets2;
								
								weightsets->resize(accessor.count);
								
								std::memcpy
								(
									weightsets->data(),
									buf.data.data() + view.byteOffset,
									view.byteLength
								);
							}
						}
					}
					
					r.append(geometry);
				}
			}
			
			return {true, std::move(r)};
		}
		
		Nullable<Animation> loadAnimationByNameFromGLTFInMemory
		(
			const char *name,
			const char *string,
			const std::size_t &length
		)
		{
			Animation animation;
			
			const auto s(loadGLTFAsString(string, length));
			
			if (!s)
				return {false};
			
			const auto &data(*s);

			animation.duration = 0.0f;
			
			bool found{false};
			
			for (const auto &info : data.model.animations)
			{
				if (info.name != name)
					continue;
				
				found = true;
				
				for (const auto &infoChannel : info.channels)
				{
					Animation::Channel channel;
					
					// Validate that the node referenced exist so that we can get at its
					// name to use as the key later when inserting the channel into the map.
					if (static_cast<std::size_t>(infoChannel.target_node) >= data.model.nodes.size())
					{
						log::err("Karhu")
							<< "Failed to load glTF animation! "
							<< "Joint node expected at index "
							<< infoChannel.target_node
							<< " not found.";
						
						return {false};
					}

					// Figure out what property to animate.
					if (infoChannel.target_path == "translation")
						channel.property = Animation::Channel::Property::position;
					else if (infoChannel.target_path == "rotation")
						channel.property = Animation::Channel::Property::rotation;
					else if (infoChannel.target_path == "scale")
						channel.property = Animation::Channel::Property::scale;
					// else if weights?

					// Validate that the sampler referenced exists.
					const auto indexSampler(static_cast<std::size_t>(infoChannel.sampler));
					if (indexSampler >= info.samplers.size())
					{
						log::err("Karhu")
							<< "Failed to load glTF animation! "
							<< "Sampler expected at index "
							<< indexSampler
							<< " not found.";
						
						return {false};
					}

					const auto &sampler(info.samplers[indexSampler]);
					/// @todo: Also grab sampler interpolation later and put it into the channel data.

					{
						// Extract the sampler inputs (keyframe times in seconds).
						/// @todo: Validate that the accessor and buffer view indices exist?
						const auto &iaccessor(data.model.accessors[static_cast<std::size_t>(sampler.input)]);
						const auto &iview    (data.model.bufferViews[static_cast<std::size_t>(iaccessor.bufferView)]);
						const auto &ibuf     (data.model.buffers[static_cast<std::size_t>(iview.buffer)]);

						// Extract the sampler outputs (what values to set at the specified times).
						/// @todo: Validate that the accessor and buffer view indices exist?
						const auto &oaccessor(data.model.accessors[static_cast<std::size_t>(sampler.output)]);
						const auto &oview    (data.model.bufferViews[static_cast<std::size_t>(oaccessor.bufferView)]);
						const auto &obuf     (data.model.buffers[static_cast<std::size_t>(oview.buffer)]);

						// Validate that their sizes match up.
						if (iaccessor.count != oaccessor.count)
						{
							log::err("Karhu")
								<< "Failed to load glTF animation! Sampler "
								<< infoChannel.sampler
								<< " input count ("
								<< iaccessor.count
								<< ") does not match corresponding output count ("
								<< oaccessor.count
								<< ").";
							
							return {false};
						}
						
						if (0 == iaccessor.count)
							continue;

						// Get the inputs out of the buffer.
						std::vector<float> inputs(iaccessor.count);
						
						std::memcpy
						(
							inputs.data(),
							ibuf.data.data() + iview.byteOffset,
							iview.byteLength
						);
						
						// Output datatype depends on what property the channel animates.
						std::vector<mth::Vec3f> outputs3(0);
						std::vector<mth::Quatf> outputs4(0);

						/// @todo: Spec says it's always vec3 for pos etc but component type may vary so should probably check prop string rather than accessor type. https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#animations
						
						// Put the data into the correct vector.
						if (TINYGLTF_TYPE_VEC3 == oaccessor.type)
						{
							outputs3.resize(oaccessor.count);
							
							std::memcpy
							(
								outputs3.data(),
								obuf.data.data() + oview.byteOffset,
								oview.byteLength
							);
						}
						else if (TINYGLTF_TYPE_VEC4 == oaccessor.type)
						{
							outputs4.resize(oaccessor.count);
							
							std::memcpy
							(
								outputs4.data(),
								obuf.data.data() + oview.byteOffset,
								oview.byteLength
							);
						}
						
						// Go through all the inputs and outputs and create keyframes.
						for (std::size_t i{0}; i < iaccessor.count; ++ i)
						{
							Animation::Channel::Keyframe keyframe;
							
							// Set the timestamp.
							// Subtract the time of the first frame in case it is not zero.
							keyframe.time = inputs[i] - inputs[0];
							
							// Make sure the animation's duration corresponds to the latest timestamp.
							if (keyframe.time > animation.duration)
								animation.duration = keyframe.time;

							// Set the value at the timestamp.
							switch (channel.property)
							{
								case Animation::Channel::Property::position:
									keyframe.position = outputs3[i];
									break;
									
								case Animation::Channel::Property::scale:
									keyframe.scale = outputs3[i];
									break;
									
								case Animation::Channel::Property::rotation:
									keyframe.rotation = outputs4[i];
									break;
							}
							
							channel.keyframes.emplace_back(std::move(keyframe));
						}
					}

					{
						const auto &name(data.model.nodes[static_cast<std::size_t>(infoChannel.target_node)].name);

						// Create a channelset for the joint if it does not exist.
						
						auto it(animation.channelsPerJoint.find(name));
						
						if (it == animation.channelsPerJoint.end())
							it = animation.channelsPerJoint.emplace(name, std::vector<Animation::Channel>{}).first;

						// Insert the new channel.
						it->second.emplace_back(std::move(channel));
					}
				}
				
				break;
			}
			
			return {found, std::move(animation)};
		}
		
		Nullable<Rig> loadRigByNameFromGLTFInMemory
		(
			const char *name,
			const char *string,
			const std::size_t &length
		)
		{
			Rig rig;
			
			const auto s(loadGLTFAsString(string, length));
			
			if (!s)
				return {false};
			
			const auto &data(*s);
			
			bool found{false};
			
			const auto &nodes(data.model.nodes);
			
			// Validate the skins.
			if (0 == data.model.skins.size())
			{
				log::err("Karhu") << "Failed to load glTF rig! No skins found.";
				return {false};
			}

			// Validate the nodes.
			if (0 == data.model.nodes.size())
			{
				log::err("Karhu") << "Failed to load glTF rig! No nodes found.";
				return {false};
			}
			
			for (const auto &skin : data.model.skins)
			{
				// Find the skeleton node.
				/// @todo: This got weird after updating to Blender 2.8 and not getting the skeleton property anymore and I'm no longer sure what's correct; supporting both for now. Seems skeleton node is not supposed to be used anymore? https://github.com/KhronosGroup/glTF/issues/1270
				
				int indexJoint{0};
				tinygltf::Node const *skeleton{nullptr};
				
				if (skin.skeleton >= 0)
				{
					if (static_cast<std::size_t>(skin.skeleton) >= nodes.size())
					{
						log::err("Karhu")
							<< "Failed to load glTF rig! "
							<< "Skeleton node expected at index "
							<< skin.skeleton
							<< " not found.";
						
						return {false};
					}

					// Set up the rig.
					skeleton = &nodes[static_cast<std::size_t>(skin.skeleton)];
				
					if (skeleton->name != name)
						continue;
				}
				else
				{
					if (skin.name != name || 0 == skin.name.length())
						continue;
					
					indexJoint = 0;
					for (auto const &node : nodes)
					{
						if (node.name == skin.name)
						{
							skeleton = &node;
							break;
						}
						
						++ indexJoint;
					}
				}
				
				if (!skeleton)
					continue;
				
				found = true;

				if (skeleton->translation.size() >= 3)
				{
					rig.transform.position.x = static_cast<float>(skeleton->translation[0]);
					rig.transform.position.y = static_cast<float>(skeleton->translation[1]);
					rig.transform.position.z = static_cast<float>(skeleton->translation[2]);
				}
				
				if (skeleton->scale.size() >= 3)
				{
					rig.transform.scale.x = static_cast<float>(skeleton->scale[0]);
					rig.transform.scale.y = static_cast<float>(skeleton->scale[1]);
					rig.transform.scale.z = static_cast<float>(skeleton->scale[2]);
				}
				
				if (skeleton->rotation.size() >= 4)
				{
					rig.transform.rotation.x = static_cast<float>(skeleton->rotation[0]);
					rig.transform.rotation.y = static_cast<float>(skeleton->rotation[1]);
					rig.transform.rotation.z = static_cast<float>(skeleton->rotation[2]);
					rig.transform.rotation.w = static_cast<float>(skeleton->rotation[3]);
				}

				// Find the inverse bind matrices
				std::vector<mth::Mat4f> matrices(0);
				{
					// Get the buffer information and data.
					/// @todo: Deal with strides other than zero?
					/// @todo: Validate that the accessor and buffer view indices exist?
					const auto &accessor(data.model.accessors[static_cast<std::size_t>(skin.inverseBindMatrices)]);
					const auto &view    (data.model.bufferViews[static_cast<std::size_t>(accessor.bufferView)]);
					const auto &buf     (data.model.buffers[static_cast<std::size_t>(view.buffer)]);
					
					// Make sure the number matches the number of joints.
					if (skin.joints.size() != accessor.count)
					{
						log::err("Karhu")
							<< "Failed to load glTF rig! Joint count ("
							<< skin.joints.size()
							<< ") does not match inverse bind matrix count ("
							<< accessor.count
							<< ").";
						
						return {false};
					}
					
					// Pull the matrices into a vector.
					matrices.resize(accessor.count);
					
					std::memcpy
					(
						matrices.data(),
						buf.data.data() + view.byteOffset,
						view.byteLength
					);
				}

				// Finds and validates the existence of the joint node at the specified index.
				auto getJoint([&nodes](const std::size_t &index) -> const tinygltf::Node *
				{
					if (index >= nodes.size())
					{
						log::err("Karhu")
							<< "Failed to load glTF rig! "
							<< "Joint node expected at index "
							<< index
							<< " not found.";
						
						return nullptr;
					}
					
					return &nodes[index];
				});

				auto getIndexOfNode([&skin](const std::size_t &index) -> int
				{
					for (std::size_t i{0}; i < skin.joints.size(); ++ i)
						if (index == static_cast<std::size_t>(skin.joints[i]))
							return static_cast<int>(i);

					return -1;
				});

				// Need to predeclare for recursion.
				std::function<bool
				(
					const tinygltf::Node &,
					const std::size_t &,
					std::unique_ptr<Rig::Child> &
				)> setUpChild;
				
				setUpChild = [&setUpChild, &getJoint, &getIndexOfNode, &rig, &matrices]
				(
					const tinygltf::Node &node,
					const std::size_t &indexNode,
					std::unique_ptr<Rig::Child> &child
				) -> bool
				{
					child = std::make_unique<Rig::Child>();

					child->name = node.name;
					
					const auto index(getIndexOfNode(indexNode));
					if (index < 0)
					{
						log::err("Karhu")
							<< "Failed to load glTF rig! "
							<< "Joint node expected at index "
							<< indexNode
							<< " not found.";
						
						return false;
					}
					
					child->ID                = static_cast<std::size_t>(index);
					child->matrixBindInverse = matrices[child->ID];

					// Keep track of how many joints the rig has.
					++ rig.countJoints;

					// We cannot just memcpy this data because the types are incompatible.

					if (node.translation.size() >= 3)
					{
						child->transform.position.x = static_cast<float>(node.translation[0]);
						child->transform.position.y = static_cast<float>(node.translation[1]);
						child->transform.position.z = static_cast<float>(node.translation[2]);
					}

					if (node.scale.size() >= 3)
					{
						child->transform.scale.x = static_cast<float>(node.scale[0]);
						child->transform.scale.y = static_cast<float>(node.scale[1]);
						child->transform.scale.z = static_cast<float>(node.scale[2]);
					}

					if (node.rotation.size() >= 4)
					{
						child->transform.rotation.x = static_cast<float>(node.rotation[0]);
						child->transform.rotation.y = static_cast<float>(node.rotation[1]);
						child->transform.rotation.z = static_cast<float>(node.rotation[2]);
						child->transform.rotation.w = static_cast<float>(node.rotation[3]);
					}

					for (const auto &next : node.children)
					{
						const auto joint(getJoint(static_cast<std::size_t>(next)));
						
						if (!joint)
							return false;

						std::unique_ptr<Rig::Child> grandchild;
						setUpChild(*joint, static_cast<std::size_t>(next), grandchild);
						child->children.emplace_back(std::move(grandchild));
					}
					
					return true;
				};
				
				// Validate the number of inverse bind matrices.
				if (static_cast<std::size_t>(skin.inverseBindMatrices) >= data.model.accessors.size())
				{
					log::err("Karhu")
						<< "Failed to load glTF rig! "
						<< "Inverse bind matrices expected at index "
						<< skin.inverseBindMatrices
						<< " not found.";
					
					return {false};
				}

				// Find the root joints.
				/// @todo: This got weird after updating to Blender 2.8 and not getting the skeleton property anymore and I'm no longer sure what's correct; supporting both for now. Seems skeleton node is not supposed to be used anymore? https://github.com/KhronosGroup/glTF/issues/1270
				std::unique_ptr<Rig::Child> root;
				
				if (skin.skeleton < 0)
				{
					root = std::make_unique<Rig::Child>();

					root->matrixBindInverse = mth::inverse(rig.transform.matrix());
					root->ID                = static_cast<std::size_t>(indexJoint);
					root->name              = skeleton->name;
				}
				
				for (std::size_t i{0}; i < skeleton->children.size(); ++ i)
				{
					indexJoint = skeleton->children[i];
					
					const auto it(std::find
					(
						skin.joints.begin(),
						skin.joints.end(),
						indexJoint
					));
					
					if (it == skin.joints.end())
						continue;

					const auto joint(getJoint(static_cast<std::size_t>(indexJoint)));
					
					if (!joint)
						return {false};

					std::unique_ptr<Rig::Child> child;
					
					if (!setUpChild(*joint, static_cast<std::size_t>(indexJoint), child))
						return {false};
					
					if (root)
						root->children.emplace_back(std::move(child));
					else
						rig.roots.emplace_back(std::move(child));
				}
				
				if (root)
					rig.roots.emplace_back(std::move(root));
			
				break;
			}
			
			return {found, std::move(rig)};
		}
		
		Pixelbuffer loadPixelsFromFileAtPath(const char *path)
		{
			int w, h, channels;
			auto pixels(stbi_load(path, &w, &h, &channels, 0));
			return createBufferFromPixelsAndFree(pixels, w, h, channels);
		}
		
		Pixelbuffer loadPixelsFromFileInMemory(const Bufferview<Byte> &bytes)
		{
			int w, h, channels;
			
			auto pixels(stbi_load_from_memory
			(
				reinterpret_cast<unsigned char *>(bytes.data),
				static_cast<int>(bytes.size),
				&w,
				&h,
				&channels,
				0
			));
			
			return createBufferFromPixelsAndFree(pixels, w, h, channels);
		}
	}
}
