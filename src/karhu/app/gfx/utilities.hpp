/**
 * @author		Ava Skoog
 * @date		2017-11-01
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Add some functions to get 3D primitives for testing and so on.

#ifndef KARHU_GRAPHICS_UTILITIES_H_
	#define KARHU_GRAPHICS_UTILITIES_H_

	#include <karhu/app/gfx/resources/Animation.hpp>
	#include <karhu/app/gfx/resources/Rig.hpp>
	#include <karhu/conv/graphics.hpp>
	#include <karhu/core/Buffer.hpp>
	#include <karhu/core/Byte.hpp>
	#include <karhu/core/Nullable.hpp>

	#include <string>
	#include <vector>

	namespace karhu
	{
		namespace gfx
		{
			Nullable<Geometry> loadGeometryFromGLTFInMemory(const char *string, const std::size_t &length);
			Nullable<Animation> loadAnimationByNameFromGLTFInMemory(const char *name, const char *string, const std::size_t &length);
			Nullable<Rig> loadRigByNameFromGLTFInMemory(const char *name, const char *string, const std::size_t &length);

			/**
			 * Tries to load pixels from an image file at the specified path.
			 *
			 * @param path The image file path.
			 *
			 * @return A buffer containing a pointer to valid pixels on success or null on failure.
			 */
			Pixelbuffer loadPixelsFromFileAtPath(const char *path);
			
			/**
			 * Tries to load pixels from an image file in memory into a buffer.
			 *
			 * @param bytes A struct containing the image file data and the byte count.
			 *
			 * @return A buffer containing a pointer to valid pixels on success or null on failure.
			 */
			Pixelbuffer loadPixelsFromFileInMemory(const Bufferview<Byte> &bytes);
		}
	}
#endif
