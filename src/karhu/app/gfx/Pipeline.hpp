/**
 * @author		Ava Skoog
 * @date		2017-11-09
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_PIPELINE_H_
	#define KARHU_GRAPHICS_PIPELINE_H_

	#include <karhu/app/gfx/Renderer.hpp>
	#include <karhu/app/gfx/Workload.hpp>
	#include <karhu/app/gfx/Rendertarget.hpp>
	#include <karhu/app/gfx/components/Transform.hpp>
	#include <karhu/app/gfx/components/Camera.hpp>
	#include <karhu/res/Material.hpp>
	#include <karhu/res/Shader.hpp>
	#include <karhu/res/Texture.hpp>
	#include <karhu/core/debugging.hpp>

	#include <vector>
	#include <utility>
	#include <cstddef>

	namespace karhu
	{
		namespace edt
		{
			class EditorViewport;
		}
		
		namespace gfx
		{
			class SubsystemGraphics;
			
			class Pipeline
			{
				public:
					// What colour buffers to use?
					enum class TypeBuffer
					{
						R,
						RG,
						RGB,
						RGBA,
						data
					};
				
					enum class ModeClear
					{
						camera,
						colour
					};
				
					struct Buffer
					{
						std::string name;
						TypeBuffer type;
						ModeClear clear;
						
						union
						{
							gfx::ColRGBAf clearcolour;
							std::int32_t  cleardata;
						};
						
						Buffer(std::string const name, TypeBuffer const type)
						:
						name{std::move(name)},
						type{type}
						{
							switch (type)
							{
								case TypeBuffer::R:
								case TypeBuffer::RG:
								case TypeBuffer::RGB:
								case TypeBuffer::RGBA:
									clear       = ModeClear::colour;
									clearcolour = {1.0f, 1.0f, 1.0f, 1.0f};
									break;
									
								case TypeBuffer::data:
									cleardata = 0;
									break;
							}
						}
						
						Buffer
						(
							std::string   const name,
							TypeBuffer    const type,
							ModeClear           clear,
							gfx::ColRGBAf const clearcolour = {1.0f, 1.0f, 1.0f, 1.0f}
						)
						:
						name       {std::move(name)},
						type       {type},
						clear      {clear},
						clearcolour{std::move(clearcolour)}
						{
							karhuASSERT((type != TypeBuffer::data))
						}
						
						Buffer
						(
							std::string  const name,
							TypeBuffer   const type,
							std::int32_t const cleardata
						)
						:
						name     {std::move(name)},
						type     {type},
						cleardata{std::move(cleardata)}
						{
							karhuASSERT((type == TypeBuffer::data))
						}
						
						Buffer() {}
						Buffer(Buffer const &) = default;
						Buffer(Buffer &&) = default;
						Buffer &operator=(Buffer const &) = default;
						Buffer &operator=(Buffer &&) = default;
						~Buffer() = default;
						
						void serialise(conv::Serialiser &) const;
						void deserialise(conv::Serialiser const &);
					};
				
					/**
					 * Template for a step in the pipeline.
					 *
					 * Every render pass will go through the full list of steps.
					 * The steps contains named enumerations and structures so
					 * that it can be constructed without confusing values, e.g.:
					 *
					 * Step
					 * {
					 *     Type::groups,
					 *     Extrabuffer::depth,
					 *     Textures{2}
					 * }
					 */
					struct Step
					{
						bool active{true};
						
						// Render custom groups of geometry
						// or render to a fullscreen quad?
						enum class Type
						{
							groups, // For rendering the groups specified.
							quad,   // For rendering a fullscreen quad.
							lights,
							shadows
						} type{Type::groups};

						// What non-colour buffers to use?
						enum class Extrabuffer
						{
							none,
							depth,
							stencil,
							shadowdepth
						} extrabuffer{Extrabuffer::none};
						
						std::string nameExtrabuffer;
						std::vector<Buffer> buffers;
						res::IDResource material{0};
						
						// Only used for shadow steps.
						float
							scaleShadowmaps {1.0f},
							marginShadowmaps{-50.0f},
							cutoffShadowmaps{1.0f},
							biasShadowmaps  {0.002f};
						
						std::size_t
							indexShadowmaps{0};
						
						void serialise(conv::Serialiser &) const;
						void deserialise(conv::Serialiser const &);
					};
				
					struct DataStep
					{
						// The information about the step.
						Step step;
						ContainerInputs inputs;

						// The data to generate for the step.
						std::unique_ptr<Rendertarget> rendertarget;
						std::unique_ptr<res::Texture> extrabuffer;
						std::vector<std::unique_ptr<res::Texture>> textures;
						
						void serialise(conv::Serialiser &) const;
						void deserialise(conv::Serialiser const &);
					};
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(conv::Serialiser const &);

				public:
					void steps(std::vector<Step> const &);
					Rendertarget *rendertargetForStep(std::size_t const index) const;
					std::size_t countSteps() const noexcept;
					DataStep *step(std::size_t const index);
					DataStep const *step(std::size_t const index) const;

					/**
					 * Validates all of the steps and tries to generate their data.
					 * Can only be called successfully once.
					 *
					 * @param graphics The graphics backend to generate the data.
					 *
					 * @return Whether validation and generation were successful.
					 */
					bool bake(SubsystemGraphics &graphics);
				
					/**
					 * Returns whether the pipeline has been successfully baked.
					 *
					 * @return Whether the pipeline is valid for use.
					 */
					bool valid() const noexcept { return m_baked; }
				
					void render
					(
						app::App              &app,
						Renderer              &renderer,
						Workload              &workload,
						ContainerInputs const &inputs,
						std::uint32_t   const  widthWindow,
						std::uint32_t   const  heightWindow,
						float           const  scaleWindowX,
						float           const  scaleWindowY
					);

					virtual ~Pipeline() = default;

				private:
					bool bindStepOutput
					(
						Camera              &,
						Rendertarget        &,
						std::uint32_t const  widthWindow,
						std::uint32_t const  heightWindow,
						float         const  scaleWindowX,
						float         const  scaleWindowY
					);

					bool updateStepInput(DataStep &, res::Material &);
					bool updateStepForLights(Pass const &, res::Material &);
					bool updateStepForShadows(DataStep &step, Pass &pass, mth::Scalu const size);
					bool unbindStep(Rendertarget &);
					float cutoffShadowmaps(std::size_t const) const;
				
				public:
					std::uint32_t
						sizeShadowmapMin{512u},
						sizeShadowmapMax{12288u};
				
				private:
					bool
						m_baked{false};
				
					std::vector<DataStep>
						m_steps;
				
					struct StepSpecial
					{
						DataStep      data;
						res::Material material;
					} m_stepFinal;
				
					using Frustumpoints = std::array<mth::Vec4f, 8>;
				
					struct DataStepShadow
					{
						std::size_t
							indexStep;
						
						Camera
							camera{0, 0, 0};
						
						Transform
							transform{0, 0, 0};
						
						Frustumpoints
							frustum;
					};
				
					std::vector<DataStepShadow>
						m_datasStepShadow;
			};
		}
	}
#endif
