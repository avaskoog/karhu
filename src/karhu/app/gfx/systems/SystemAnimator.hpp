/**
 * @author		Ava Skoog
 * @date		2019-06-06
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_SYSTEM_ANIMATOR_H_
	#define KARHU_GRAPHICS_SYSTEM_ANIMATOR_H_

	#include <karhu/app/ecs/System.hpp>
	#include <karhu/app/gfx/components/Animator.hpp>
	#include <karhu/res/Rig.hpp>

	namespace karhu
	{
		namespace res
		{
			class Animation;
			class Animationclips;
		}
		
		namespace gfx
		{
			class SystemAnimator : public ecs::System<Animator>
			{
				public:
					static void listenerAnimatorCreated
					(
							  app::App               &,
						const ecs::EComponentCreated &e,
						const float,
						const float,
						const float,
							  void                   *
					);
				
				protected:
					void performUpdateComponents
					(
						      app::App            &app,
						      ecs::Pool<Animator> &components,
						const float                dt,
						const float                dtFixed,
						const float                timestep
					) override;
			
				private:
					static bool refreshAnimator
					(
						app::App                       &,
						Animator                       &,
						res::Animation           const **outClip       = nullptr,
						res::Animationcontroller const **outController = nullptr,
						res::Animationclips      const **outClips      = nullptr
					);
				
					static bool resetClip
					(
						      Animator       &,
						const res::Animation &
					);
				
					static bool resetController
					(
						      app::App                 &,
						      Animator                 &,
						const res::Animationcontroller &,
					 	const res::Animationclips      &
					);
					
					static void updateClip
					(
							  Animator                &,
						const res::Rig                &,
						const res::Animation          &,
							  std::vector<mth::Mat4f> &
					);
				
					static void updateController
					(
						      app::App                 &,
							  Animator                 &,
						const res::Rig                 &,
						const res::Animationcontroller &,
						const res::Animationclips      &,
							  std::vector<mth::Mat4f>  &
					);
				
					static void resetClipstate
					(
							  Animator            &,
							  Animator::Clipstate &,
						const res::Animation      &
					);
				
					static void resetClipstates
					(
						      app::App                 &,
						      Animator                 &,
						const res::Animationcontroller &,
						const res::Animationclips      &,
						const Animator::Index
					);
				
					static bool updateSingle
					(
							  Animator                 &,
						const res::Rig                 &,
							  Animator::Clipstate      &,
						const res::Animation           &,
						const bool                      ,
							  std::vector<mth::Mat4f>  &
					);
				
					static bool updateBlendtree
					(
						      app::App                 &,
							  Animator                 &,
						const res::Rig                 &,
						const res::Animationcontroller &,
						const res::Animationclips      &,
						const Animator::Index           ,
							  std::vector<mth::Mat4f>  &
					);
				
					static bool updateSingleOrBlendtree
					(
						      app::App                 &,
							  Animator                 &,
						const res::Rig                 &,
						const res::Animationcontroller &,
						const res::Animationclips      &,
						const Animator::Index           ,
							  std::vector<mth::Mat4f>  &
					);
				
					static void updateJoints
					(
						const res::Rig::Child          &,
						const mth::Mat4f               &,
						const res::Animation           &,
						const float                     ,
							  std::vector<mth::Mat4f>  &
					);
				
					static bool compare
					(
						const Animator                 &,
						const Animator::Value          &,
						const Animator::Comparand      &,
						const Animator::Operation      &,
						const Animator::Behaviour      &,
						const res::Animationcontroller &,
						const bool
					);
				
				friend class Animator;
			};
		}
	}
#endif
