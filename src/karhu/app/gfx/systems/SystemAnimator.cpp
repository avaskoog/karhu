/**
 * @author		Ava Skoog
 * @date		2019-06-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/systems/SystemAnimator.hpp>
#include <karhu/res/Bank.hpp>
#include <karhu/res/Animation.hpp>
#include <karhu/res/Animationcontroller.hpp>
#include <karhu/res/Animationclips.hpp>
#include <karhu/conv/maths.hpp>

namespace karhu
{
	namespace gfx
	{
		void SystemAnimator::listenerAnimatorCreated
		(
			      app::App               &app,
			const ecs::EComponentCreated &e,
			const float,
			const float,
			const float,
			      void                   *
		)
		{
			auto anim(app.ecs().componentInEntity<Animator>(e.component(), e.entity()));
			
			refreshAnimator(app, *anim);
			
			if (anim && anim->m_autoplay)
				anim->start();
		}
		
		void SystemAnimator::performUpdateComponents
		(
			      app::App            &app,
				  ecs::Pool<Animator> &components,
			const float                dt,
			const float,
			const float
		)
		{
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);
				
				c->m_valid = true;
				
				if (!c->activeInHierarchy())
				{
					c->m_valid = false;
					continue;
				}
				
				res::Animation const
					*clip{nullptr};
				
				res::Animationcontroller const
					*controller{nullptr};
				
				res::Animationclips const
					*clips{nullptr};
				
				if (!refreshAnimator(app, *c, &clip, &controller, &clips))
					continue;
				
				const auto rig(app.res().get<res::Rig>(c->m_rig));
				
				if (!rig)
				{
					c->m_valid = false;
					continue;
				}
				
				if (c->m_transformsPerJointByID.size() != rig->countJoints)
					c->m_transformsPerJointByID.resize(rig->countJoints);
				
				std::fill
				(
					c->m_transformsPerJointByID.begin(),
					c->m_transformsPerJointByID.end(),
					mth::Mat4f{1.0f}
				);
				
				if (c->playing())
					c->tick(dt);
				
				switch (c->m_mode)
				{
					case ModeAnimator::clip:
						updateClip
						(
							*c,
							*rig,
							*clip,
							c->m_transformsPerJointByID
						);
						break;
					
					case ModeAnimator::controller:
						updateController
						(
							app,
							*c,
							*rig,
							*controller,
							*clips,
							c->m_transformsPerJointByID
						);
						break;
				}
				
				c->m_tick = 0.0f;
			}
		}
		
		bool SystemAnimator::refreshAnimator
		(
			app::App                       & app,
			Animator                       & c,
			res::Animation           const **outClip,
			res::Animationcontroller const **outController,
			res::Animationclips      const **outClips
		)
		{
			res::Animation const
				*clip{nullptr};
			
			res::Animationcontroller const
				*controller{nullptr};
			
			res::Animationclips const
				*clips{nullptr};
		
			switch (c.m_mode)
			{
				case ModeAnimator::clip:
				{
					if (!(clip = app.res().get<res::Animation>(c.m_clip)))
					{
						c.m_valid = false;
						return false;
					}
					
					break;
				}
				
				case ModeAnimator::controller:
				{
					controller = app.res().get<res::Animationcontroller>(c.m_controller);
					clips      = app.res().get<res::Animationclips>     (c.m_clips);
					
					if (!controller || !clips)
					{
						c.m_valid = false;
						return false;
					}
					
					break;
				}
			}
		
			if (c.m_modified)
			{
				switch (c.m_mode)
				{
					case ModeAnimator::clip:
						c.m_modified = !resetClip(c, *clip);
						break;
					
					case ModeAnimator::controller:
						c.m_modified = !resetController(app, c, *controller, *clips);
						break;
				}
			}
		
			// Animator is not valid.
			if (c.m_modified)
			{
				c.m_valid = false;
				return false;
			}
			
			if (outClip)
				*outClip = clip;
			
			if (outController)
				*outController = controller;
			
			if (outClips)
				*outClips = clips;
			
			return true;
		}
		
		bool SystemAnimator::resetClip
		(
				  Animator       &c,
			const res::Animation &clip
		)
		{
			resetClipstate(c, c.m_clipstate, clip);
			return true;
		}
		
		bool SystemAnimator::resetController
		(
			      app::App                 &app,
				  Animator                 &c,
			const res::Animationcontroller &controller,
			const res::Animationclips      &clips
		)
		{
			if
			(
				0 == controller.names.size() ||
				0 == controller.clips.size() ||
				0 == clips.clips.size()
			)
				return false;
			
			if (controller.names.size() != clips.clips.size())
				return false;
			
			/// @todo: Or should we just clamp it and move on?
			if (controller.entry >= static_cast<res::Animationcontroller::Index>(controller.clips.size()))
				return false;
			
			c.m_controllerstate.blending      = false;
			c.m_controllerstate.indexClipCurr = controller.entry;
			
			// We need one clipstate for every clip.
			
			c.m_controllerstate.clipstates.resize(controller.clips.size());
			
			std::fill
			(
				c.m_controllerstate.clipstates.begin(),
				c.m_controllerstate.clipstates.end(),
				Animator::Clipstate{}
			);
			
			// Copy over the variables in their default states.
			
			c.m_controllerstate.vars.clear();
			
			if (0 != controller.vars.size())
			{
				for (std::size_t i{0}; i < controller.vars.size(); ++ i)
				{
					using T = Animator::Value::Type;
					
					const auto &v(controller.vars[i]);
					Animator::Value copy;
					
					switch ((copy.type = v.type))
					{
						case T::boolean:  copy.boolean  = v.boolean;  break;
						case T::integer:  copy.integer  = v.integer;  break;
						case T::floating: copy.floating = v.floating; break;
					}
					
					c.m_controllerstate.vars.emplace_back(std::move(copy));
					
					c.m_controllerstate.indicesVars.emplace
					(
						v.name,
						static_cast<Animator::Index>(i)
					);
				}
			}
			
			// Copy over the triggers in their default states.
			
			c.m_controllerstate.triggers.clear();
			
			if (0 != controller.triggers.size())
			{
				c.m_controllerstate.triggers.resize(controller.triggers.size());
				c.m_controllerstate.triggers.shrink_to_fit();
				
				std::fill
				(
					c.m_controllerstate.triggers.begin(),
					c.m_controllerstate.triggers.end(),
					false
				);
				
				for (std::size_t i{0}; i < controller.triggers.size(); ++ i)
					c.m_controllerstate.indicesTriggers.emplace
					(
						controller.triggers[i].name,
						static_cast<Animator::Index>(i)
					);
			}
			
			c.m_controllerstate.controller = app.res().ref<res::Animationcontroller>(c.m_controller);
			
			resetClipstates
			(
				app,
				c,
				controller,
				clips,
				c.m_controllerstate.indexClipCurr
			);
			
			return true;
		}
		
		void SystemAnimator::updateClip
		(
				  Animator                &c,
			const res::Rig                &rig,
			const res::Animation          &clip,
			      std::vector<mth::Mat4f> &outMatrices
		)
		{
			updateSingle
			(
				c,
				rig,
				c.m_clipstate,
				clip,
				c.m_loop,
				outMatrices
			);
		}
		
		void SystemAnimator::updateController
		(
			      app::App                 &app,
				  Animator                 &c,
			const res::Rig                 &rig,
			const res::Animationcontroller &controller,
			const res::Animationclips      &clips,
			      std::vector<mth::Mat4f>  &outMatrices
		)
		{
			bool done{false};
		
			if (c.m_controllerstate.blending)
			{
				const std::size_t count{outMatrices.size()};
				auto &blend(c.m_controllerstate.blend);
				
				/// @todo: Figure out if this is the best way to deal with backwards blending too.
				blend.time += mth::abs(c.m_tick);
				done        = (blend.time >= blend.duration);
				blend.time  = mth::clamp(blend.time, 0.0f, blend.duration);
				
				std::vector<mth::Mat4f>
					matricesA(count),
					matricesB(count);
				
				updateSingleOrBlendtree
				(
					app,
					c,
					rig,
					controller,
					clips,
					blend.src,
					matricesA
				);
				
				updateSingleOrBlendtree
				(
					app,
					c,
					rig,
					controller,
					clips,
					blend.dst,
					matricesB
				);
				
				const float progress{blend.time / blend.duration};
				
				for (std::size_t i{0}; i < count; ++ i)
					outMatrices[i] =
					(
						matricesA[i] * (1.0f - progress) +
						matricesB[i] * progress
					);
				
				if (done)
					c.m_controllerstate.blending = false;
			}
			else
				done = !updateSingleOrBlendtree
				(
					app,
					c,
					rig,
					controller,
					clips,
					c.m_controllerstate.indexClipCurr,
					outMatrices
				);

			for (const auto &transition : controller.transitions)
			{
				/// @todo: Should we create a lookup table instead of looping through all transitions? There probably won't be THAT many…
				if (transition.src >= 0 && transition.src != c.m_controllerstate.indexClipCurr)
					continue;
				
				if (transition.dst == c.m_controllerstate.indexClipCurr)
					continue;
				
				if (compare
				(
					c,
					c.m_controllerstate.vars[static_cast<std::size_t>(transition.var)], /// @todo: Work with triggers too.
					transition.comp,
					transition.op,
					transition.behaviour,
					controller,
					done
				))
				{
					if (Animator::Behaviour::blend == transition.behaviour)
					{
						const Animator::Index srcOld{c.m_controllerstate.indexClipCurr};
						
						c.m_controllerstate.blending = true;
						c.m_controllerstate.indexClipCurr = transition.dst;
						
						auto &blend(c.m_controllerstate.blend);
						
						blend.time     = 0.0f;
						blend.duration = transition.blendtime;
						blend.src      = srcOld;
						blend.dst      = transition.dst;
						
						resetClipstates(app, c, controller, clips, blend.src);
						resetClipstates(app, c, controller, clips, blend.dst);
					}
					else
					{
						c.m_controllerstate.blending      = false;
						c.m_controllerstate.indexClipCurr = transition.dst;
						
						resetClipstates(app, c, controller, clips, transition.dst);
					}
				}
			}
			
			// Need to reset triggers every frame.
			std::fill
			(
				c.m_controllerstate.triggers.begin(),
				c.m_controllerstate.triggers.end(),
				false
			);
		}
		
		void SystemAnimator::resetClipstate
		(
			      Animator            &c,
				  Animator::Clipstate &state,
			const res::Animation      &clip
		)
		{
			state.time = (c.m_speed >= 0.0f) ? 0.0f : clip.duration;
		}
		
		void SystemAnimator::resetClipstates
		(
			      app::App                 &app,
			      Animator                 &c,
			const res::Animationcontroller &controller,
			const res::Animationclips      &clips,
			const Animator::Index           index
		)
		{
			using T = res::Animationcontroller::Clip::Type;
			
			const auto i(static_cast<std::size_t>(index));
			
			switch (controller.clips[i].type)
			{
				case T::single:
					resetClipstate
					(
						c,
						c.m_controllerstate.clipstates[i],
						/// @todo: This lags a bunch if it has to load the animation for the first time, so do something clever with preloading etc?
						*app.res().get<res::Animation>
						(
							clips.clips
							[
								static_cast<std::size_t>
								(
									controller.clips[i].names[0]
								)
							]
						)
					);
					break;
				
				case T::blendtree:
					for (const auto &name : controller.clips[i].names)
						resetClipstates(app, c, controller, clips, name);
					
					break;
			}
		}
		
		bool SystemAnimator::updateSingle
		(
			      Animator                 &c,
			const res::Rig                 &rig,
			      Animator::Clipstate      &state,
			const res::Animation           &clip,
			const bool                      loop,
			      std::vector<mth::Mat4f>  &outMatrices
		)
		{
			bool r{true};
			
			state.time += c.m_tick;
			
			if (loop)
				state.time = mth::mod(state.time, clip.duration);
			else
			{
				if
				(
					(c.m_speed >= 0.0f && state.time >= clip.duration) ||
					(c.m_speed <  0.0f && state.time <= 0.0f)
				)
				{
					c.m_clipstate.time = mth::clamp
					(
						c.m_clipstate.time,
						0.0f,
						clip.duration
					);
					
					c.m_playing = false;
					
					r = false;
				}
			}
			
			for (const auto &root : rig.roots)
				if (root)
					updateJoints
					(
						*root,
						rig.transform.matrix(),
						clip,
						state.time,
						outMatrices
					);
			
			return r;
		}
		
		bool SystemAnimator::updateBlendtree
		(
		          app::App                 &app,
			      Animator                 &c,
			const res::Rig                 &rig,
			const res::Animationcontroller &controller,
			const res::Animationclips      &clips,
			const Animator::Index           index,
			      std::vector<mth::Mat4f>  &outMatrices
		)
		{
			bool r{false};
			
			const auto &clip(controller.clips[static_cast<std::size_t>(index)]);
			
			const std::size_t count{outMatrices.size()};
			
			std::vector<std::vector<mth::Mat4f>> matricesPerClip
			(
				clip.names.size(),
				std::vector<mth::Mat4f>(count)
			);

			for (std::size_t i{0}; i < matricesPerClip.size(); ++ i)
				// This way if a single call returns true, the final result is true.
				r |= updateSingleOrBlendtree
				(
					app,
					c,
					rig,
					controller,
					clips,
					clip.names[i],
					matricesPerClip[i]
				);
			
			// If there is just one clip, we can return its matrices as they are.
			if (1 == matricesPerClip.size())
				outMatrices = std::move(matricesPerClip[0]);
			// Otherwise we need to calculate the blend.
			else
			{
				// Figure out what two nodes to blend between.
				
				const float
					countf        {static_cast<float>(matricesPerClip.size() - 1)},
					value         {mth::clamp(c.m_controllerstate.vars[static_cast<std::size_t>(clip.var)].floating, 0.0f, 1.0f)},
					progressScaled{value * countf};
				
				const std::size_t
					indexA{static_cast<std::size_t>
					(
						mth::min
						(
							countf - 1.0f,
							mth::floor(progressScaled)
						)
					)},
					indexB{indexA + 1};
				
				// Figure out the blend weights.
				const float
					progress{progressScaled - static_cast<float>(indexA)},
					percentA{1.0f - progress},
					percentB{progress};
				
				// Calculate the final matrices.
				for (std::size_t i{0}; i < count; ++ i)
					outMatrices[i] =
					(
						matricesPerClip[indexA][i] * percentA +
						matricesPerClip[indexB][i] * percentB
					);
			}
			
			return r;
		}
		
		bool SystemAnimator::updateSingleOrBlendtree
		(
		          app::App                 &app,
			      Animator                 &c,
			const res::Rig                 &rig,
			const res::Animationcontroller &controller,
			const res::Animationclips      &clips,
			const Animator::Index           index,
			      std::vector<mth::Mat4f>  &outMatrices
		)
		{
			using T = res::Animationcontroller::Clip::Type;
			
			const auto i(static_cast<std::size_t>(index));
			
			switch (controller.clips[i].type)
			{
				case T::single:
				{
					const auto subindex(static_cast<std::size_t>
					(
						controller.clips[i].names[0]
					));
					
					return updateSingle
					(
						c,
						rig,
						c.m_controllerstate.clipstates[i],
						*app.res().get<res::Animation>(clips.clips[subindex]),
						controller.clips[i].loop,
						outMatrices
					);
				}
					
				case T::blendtree:
					return updateBlendtree
					(
						app,
						c,
						rig,
						controller,
						clips,
						index,
						outMatrices
					);
			}
		}
		
		void SystemAnimator::updateJoints
		(
			const res::Rig::Child         &joint,
			const mth::Mat4f              &matrixParent,
			const res::Animation          &clip,
			const float                    time,
				  std::vector<mth::Mat4f> &outMatrices
		)
		{
			// Find the corresponding animation target.
			const std::vector<res::Animation::Channel> *channels{nullptr};
			{
				const auto it(clip.channelsPerJoint.find(joint.name));
				if (it != clip.channelsPerJoint.end())
					channels = &it->second;
			}
			
			// This will hold the final interpolated values per property.
			mth::Transformf transform{joint.transform};
			
			// If there is no target we still want to keep recursing, so do not exit function.
			if (channels)
			{
				// Go through the channels which animate a transformation property each.
				for (const auto &channel : *channels)
				{
					// If there is only one keyframe, we can just grab that value.
					if (1 == channel.keyframes.size())
					{
						const auto &keyframe(channel.keyframes[0]);

						switch (channel.property)
						{
							case res::Animation::Channel::Property::position:
								transform.position = keyframe.position;
								break;

							case res::Animation::Channel::Property::scale:
								transform.scale = keyframe.scale;
								break;

							case res::Animation::Channel::Property::rotation:
								transform.rotation = keyframe.rotation;
								break;
						}
					}
					else
					{
						// Find the closest two keyframes.
						for (std::size_t i{0}; i < channel.keyframes.size() - 1; ++ i)
						{
							/// @todo: If i == count - 1, should we set b to frame 0?
							const auto &a(channel.keyframes[i + 0]);
							const auto &b(channel.keyframes[i + 1]);
							
							if (a.time <= time && b.time >= time)
							{
								// Calculate the progress between the two frames.
								const float
									position{time - a.time},
									duration{b.time - a.time},
									progress{position / duration};

								// Interpolate linearly between the values according to progress.
								/// @todo: If/when different interpolation types get supported, update here, in which case the above progress calculation could be moved into a function that returns the correct progress according to interpolation method.
								switch (channel.property)
								{
									case res::Animation::Channel::Property::position:
										transform.position = mth::lerp(a.position, b.position, progress);
										break;

									case res::Animation::Channel::Property::scale:
										transform.scale = mth::lerp(a.scale, b.scale, progress);
										break;

									case res::Animation::Channel::Property::rotation:
										transform.rotation = mth::slerp(a.rotation, b.rotation, progress);
										break;
								}

								break;
							}
						}
					}
				}
			}
			
			// Now we can calcute the matrix for this joint.
			mth::Mat4f matrix{matrixParent * transform.matrix()};

			for (const auto &child : joint.children)
				if (child)
					updateJoints(*child, matrix, clip, time, outMatrices);

			matrix *= joint.matrixBindInverse;

			// Store the matrix in the list!
			if (joint.ID < outMatrices.size()) /// @todo: Unnecessary check?
				outMatrices[joint.ID] = std::move(matrix);
		}
		
		bool SystemAnimator::compare
		(
			const Animator                 &c,
			const Animator::Value          &val,
			const Animator::Comparand      &comp,
			const Animator::Operation      &op,
			const Animator::Behaviour      &behaviour,
			const res::Animationcontroller &controller,
			const bool                      done
		)
		{
			using Op = Animator::Operation;
			using CT = Animator::Comparand::Type;
			using VT = Animator::Value::Type;
			
			bool const
				f{(Animator::Behaviour::finish != behaviour || done)};
			
			auto const
				i(static_cast<std::size_t>(comp.index));
			
			auto const
				execute([&val, &comp, &controller, &i](auto const &op) -> bool
				{
					Animator::Value const
						*compval{nullptr};
					
					switch (comp.type)
					{
						case CT::value: compval = &comp.value;           break;
						case CT::index: compval = &controller.consts[i]; break;
					}
					
					if (val.type != compval->type)
						return false;
					
					switch (val.type)
					{
						case VT::boolean:  return op(val.boolean,  compval->boolean);
						case VT::integer:  return op(val.integer,  compval->integer);
						case VT::floating: return op(val.floating, compval->floating);
					}
				});
			
			auto const
				&triggers(c.m_controllerstate.triggers);
			
			switch (op)
			{
				case Op::equal:          return f && execute(std::equal_to     <>{});
				case Op::unequal:        return f && execute(std::not_equal_to <>{});
				case Op::less:           return f && execute(std::less         <>{});
				case Op::lessOrEqual:    return f && execute(std::less_equal   <>{});
				case Op::greater:        return f && execute(std::greater      <>{});
				case Op::greaterOrEqual: return f && execute(std::greater_equal<>{});
				case Op::trigger:        return f && triggers[i];
				case Op::always:         return f;
			}
		}
	}
}
