/**
 * @author		Ava Skoog
 * @date		2019-03-13
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_SYSTEM_PASSES_H_
	#define KARHU_GRAPHICS_SYSTEM_PASSES_H_

	#include <karhu/app/ecs/System.hpp>
	#include <karhu/app/gfx/components/Camera.hpp>
	#include <karhu/app/gfx/Workload.hpp>

	namespace karhu
	{
		namespace gfx
		{
			class SystemPasses : public ecs::System<Camera>
			{
				public:
					SystemPasses(Workload &workload)
					:
					m_workload{workload}
					{
					}
				
				protected:
					void performUpdateComponents
					(
						      app::App          &app,
							  ecs::Pool<Camera> &components,
						const float             dt,
						const float             dtFixed,
						const float             timestep
					) override;
				
				private:
					Workload &m_workload;
			};
		}
	}
#endif
