/**
 * @author		Ava Skoog
 * @date		2023-01-16
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_SYSTEM_BONEBINDER_H_
	#define KARHU_GRAPHICS_SYSTEM_BONEBINDER_H_

	#include <karhu/app/ecs/System.hpp>
	#include <karhu/app/gfx/Workload.hpp>
	#include <karhu/app/gfx/components/Bonebinder.hpp>

	namespace karhu
	{
		namespace gfx
		{
			class SystemBonebinder : public ecs::System<Bonebinder>
			{
				protected:
					void performUpdateComponents
					(
						      app::App              &app,
							  ecs::Pool<Bonebinder> &components,
						const float                  dt,
						const float                  dtFixed,
						const float                  timestep
					) override;
			};
		}
	}
#endif
