/**
 * @author		Ava Skoog
 * @date		2018-10-21
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_SYSTEM_RENDER_H_
	#define KARHU_GRAPHICS_SYSTEM_RENDER_H_

	#include <karhu/app/ecs/System.hpp>
	#include <karhu/app/gfx/components/Renderdata3D.hpp>
	#include <karhu/app/gfx/Workload.hpp>

	namespace karhu
	{
		namespace win
        {
			class Window;
		}
		
		namespace gfx
		{
			class Renderer;
			class Pipeline;
			
			class SystemRender : public ecs::SystemWithoutComponents
			{
				public:
					SystemRender
					(
						win::Window     &window,
						Renderer        &renderer,
					 	Pipeline        &pipeline,
					 	Workload        &workload,
					 	ContainerInputs &inputs
					);

				protected:
					void performUpdateNoComponents
					(
						app::App       &app,
						float    const  dt,
						float    const  dtFixed,
						float    const  timestep
					) override;
				
				private:
					win::Window     &m_window;
					Renderer        &m_renderer;
					Pipeline        &m_pipeline;
					Workload        &m_workload;
					ContainerInputs &m_inputs;
			};
		}
	}
#endif
