/**
 * @author		Ava Skoog
 * @date		2019-03-13
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/systems/SystemPasses.hpp>

namespace karhu
{
	namespace gfx
	{
		void SystemPasses::performUpdateComponents
		(
			      app::App          &app,
				  ecs::Pool<Camera> &components,
			const float,
			const float,
			const float
		)
		{
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);
				
				if (!c->activeInHierarchy())
					continue;
				
				#ifdef KARHU_EDITOR
				/// @todo: Quick hack for editor cameras. Can we do better?
				if (app::ModeApp::server == app.mode())
				{
					const auto &name(app.ecs().entityName(c->entity()));
					
					if (name != "_cam-editor" && name != "_cam-gui")
						continue;
				}
				#endif
				
				if (auto t = app.ecs().componentInEntity<Transform>(c->entity()))
				{
					Pass pass;
					pass.camera = c;
					pass.transformCamera = t;
					m_workload.passes.emplace_back(std::move(pass));
				}
			}
		}
	}
}
