/**
 * @author		Ava Skoog
 * @date		2019-09-12
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_SYSTEM_PARTICLESYSTEM_H_
	#define KARHU_GRAPHICS_SYSTEM_PARTICLESYSTEM_H_

	#include <karhu/app/ecs/System.hpp>
	#include <karhu/app/gfx/Workload.hpp>
	#include <karhu/app/gfx/components/Particlesystem.hpp>

	namespace karhu
	{
		namespace gfx
		{
			class SystemParticlesystem : public ecs::System<Particlesystem>
			{
				public:
					static void listenerParticlesystemActive
					(
							  app::App              &,
						const ecs::EComponentActive &e,
						const float,
						const float,
						const float,
							  void                  *
					);
				
				public:
					SystemParticlesystem(Workload &workload)
					:
					m_workload{workload}
					{
					}
				
				protected:
					void performUpdateComponents
					(
						      app::App                  &app,
							  ecs::Pool<Particlesystem> &components,
						const float                      dt,
						const float                      dtFixed,
						const float                      timestep
					) override;
				
				private:
					Workload &m_workload;
			};
		}
	}
#endif
