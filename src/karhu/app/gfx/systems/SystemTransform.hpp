/**
 * @author		Ava Skoog
 * @date		2018-10-21
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_SYSTEM_TRANSFORM_H_
	#define KARHU_GRAPHICS_SYSTEM_TRANSFORM_H_

	#include <karhu/app/ecs/System.hpp>
	#include <karhu/app/gfx/components/Transform.hpp>

	namespace karhu
	{
		namespace gfx
		{
			class SystemTransform : public ecs::System<Transform>
			{
				protected:
					void performUpdateComponents
					(
						      app::App             &app,
						      ecs::Pool<Transform> &components,
						const float                 dt,
						const float                 dtFixed,
						const float                 timestep
					) override;
			};
		}
	}
#endif
