/**
 * @author		Ava Skoog
 * @date		2019-03-13
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_SYSTEM_WORKLOAD_3D_H_
	#define KARHU_GRAPHICS_SYSTEM_WORKLOAD_3D_H_

	#include <karhu/app/ecs/System.hpp>
	#include <karhu/app/gfx/components/Renderdata3D.hpp>
	#include <karhu/app/gfx/Workload.hpp>

	namespace karhu
	{
		namespace gfx
		{
			class SystemWorkload3D : public ecs::System<Renderdata3D>
			{
				public:
					SystemWorkload3D(Workload &workload)
					:
					m_workload{workload}
					{
					}
				
				protected:
					void performUpdateComponents
					(
						      app::App                &app,
						      ecs::Pool<Renderdata3D> &components,
						const float                   dt,
						const float                   dtFixed,
						const float                   timestep
					) override;
				
				private:
					Workload &m_workload;
			};
		}
	}
#endif
