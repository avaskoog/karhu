/**
 * @author		Ava Skoog
 * @date		2018-10-21
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/systems/SystemTransform.hpp>
#include <karhu/conv/maths.hpp>
/*
/// @todo: Temporary
#include <karhu/app/lib/im3d/im3d.h>
#include <karhu/app/lib/im3d/im3d_math.h>
*/
namespace karhu
{
	namespace gfx
	{
		void SystemTransform::performUpdateComponents
		(
			      app::App             &app,
				  ecs::Pool<Transform> &components,
			const float,
			const float,
			const float
		)
		{
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				/// @todo: Break if component inactive?
			
				auto c(components.data + i);
				
				/// @todo: Is this even necessary now that updating a transform calls refresh anyway?
				c->refresh(app.ecs());
				/*
				if (app::App::instance()->win().top())
				{
				Im3d::PushMatrix();
				Im3d::PushDrawState();
				{
					const auto z(c->global().forward()), x(c->global().right()), y(c->global().up());
					const Im3d::Vec3 p{c->global().position.x, c->global().position.y, c->global().position.z};
					
					Im3d::DrawLine
					(
						p,
						p + Im3d::Vec3{x.x, x.y, x.z},
						10.0f,
						{1.0f, 0.0f, 0.0f, 1.0f}
					);
					Im3d::DrawLine
					(
						p,
						p + Im3d::Vec3{y.x, y.y, y.z},
						10.0f,
						{0.0f, 1.0f, 0.0f, 1.0f}
					);
					Im3d::DrawLine
					(
						p,
						p + Im3d::Vec3{z.x, z.y, z.z},
						10.0f,
						{0.0f, 0.0f, 1.0f, 1.0f}
					);
				}
				Im3d::PopDrawState();
				Im3d::PopMatrix();
				}*/
				/*
				mth::Mat4f
					TS{c->m_local.matrix()},
					R {mth::rotationmatrix(c->m_local.rotation)};
				
				world.forEachParentOfEntity
				(
					c->entity(),
					[&TS, &R, &world](const ecs::IDEntity &ID)
					{
						if (auto parent = world.componentInEntity<Transform>(ID))
						{
							const auto t(parent->m_local);
							
							const auto tp(mth::translationmatrix(t.position));
							const auto rp(mth::rotationmatrix   (t.rotation));
							const auto sp(mth::scalematrix      (t.scale));

							mth::Mat4f tf, T, S;
							mth::Vec3f   sf;
							
							{
								mth::Vec3f p, s;
								
								mth::decompose(TS, &p, nullptr, &s);
								
								const mth::Mat4f
									tc{mth::translationmatrix(p)},
									sc{mth::scalematrix      (s)};
							
								tf = tp * mth::inverse(rp) * sp * tc;
								sf = t.scale * s;
							}
							
							T = mth::translationmatrix(mth::position(tf));
							S = mth::scalematrix      (sf);
							
							TS = T * S;
							R  = R * rp;
						}

						return true;
					}
				);
	
				mth::decompose(TS, &c->m_global.position, nullptr, &c->m_global.scale);
				c->m_global.rotation = mth::rotation(R);*/
			}
		}
	}
}
