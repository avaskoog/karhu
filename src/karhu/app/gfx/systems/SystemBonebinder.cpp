/**
 * @author		Ava Skoog
 * @date		2023-01-16
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/systems/SystemBonebinder.hpp>
#include <karhu/app/gfx/components/Transform.hpp>
#include <karhu/app/gfx/components/Animator.hpp>
#include <karhu/res/Rig.hpp>

namespace karhu
{
	namespace gfx
	{
		void SystemBonebinder::performUpdateComponents
		(
			      app::App              &app,
			      ecs::Pool<Bonebinder> &components,
			const float                  /*dt*/,
			const float                  /*dtFixed*/,
			const float                  /*timestep*/
		)
		{
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);
				
				if (!c->activeInHierarchy())
					continue;
				
				auto
					tf(app.ecs().componentInEntity<Transform>(c->entity()));
				
				ecs::IDEntity const
					IDParent(app.ecs().entityParentFirst(c->entity()));
				
				if (IDParent == c->entity())
					continue;
				
				auto
					anim(app.ecs().componentInEntity<Animator>(IDParent));
				
				if (!tf || !anim)
					continue;
				
				auto const
					&matrices(anim->matrices());
				
				if (c->IDJoint() >= matrices.size())
					continue;
				
				auto
					rig(app.res().get<res::Rig>(anim->rig()));
				
				if (!rig)
					continue;
				
				if (c->IDJoint() >= rig->childrenInOrder.size())
					continue;
				
				auto
					bone(rig->childrenInOrder[c->IDJoint()]);
				
				if (!bone)
					continue;
				
				mth::Mat4f const
					matLocal{matrices[c->IDJoint()]},
					matWorld{matLocal * mth::inverse(bone->matrixBindInverse)};
				
				mth::Transformf
					local;
				
				local.position = mth::position(matWorld);
				local.scale    = mth::scale(matLocal);
				local.rotation = mth::conjugate(mth::rotation(matLocal));
//
//				mth::decompose
//				(
//					matrices[c->IDJoint()] * mth::inverse(bone->matrixBindInverse),
//					local
//				);
//
//				local.rotation = mth::inverse(local.rotation);
				
				tf->local(std::move(local));
			}
		}
	}
}
