/**
 * @author		Ava Skoog
 * @date		2019-03-13
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/systems/SystemWorkload3D.hpp>

namespace karhu
{
	namespace gfx
	{
		void SystemWorkload3D::performUpdateComponents
		(
			      app::App                &app,
				  ecs::Pool<Renderdata3D> &components,
			const float                   dt,
			const float                   dtFixed,
			const float                   timestep
		)
		{
			m_workload.addFromECS
			(
				app,
				app.ecs(),
				components,
				dt,
				dtFixed,
				timestep
			);
		}
	}
}
