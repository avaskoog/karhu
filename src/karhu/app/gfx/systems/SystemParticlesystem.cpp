/**
 * @author		Ava Skoog
 * @date		2019-09-12
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/systems/SystemParticlesystem.hpp>
#include <karhu/app/gfx/components/Renderdata2D.hpp>
#include <karhu/app/gfx/components/Renderdata3D.hpp>

namespace karhu
{
	namespace gfx
	{
		void SystemParticlesystem::listenerParticlesystemActive
		(
			      app::App              &app,
			const ecs::EComponentActive &e,
			const float,
			const float,
			const float,
			      void                  *
		)
		{
			/// @todo: When we can get component by parent type, just get plain Renderdata.
			
			auto rd2D(app.ecs().componentInEntity<Renderdata2D>(e.entity()));
			auto rd3D(app.ecs().componentInEntity<Renderdata3D>(e.entity()));
			
			if (rd2D)
				app.ecs().componentActiveInEntity(rd2D->identifier(), e.entity(), false);
			
			if (rd3D)
				app.ecs().componentActiveInEntity(rd3D->identifier(), e.entity(), false);
		}
		
		void SystemParticlesystem::performUpdateComponents
		(
			      app::App                  &app,
			      ecs::Pool<Particlesystem> &components,
			const float                      dt,
			const float                      dtFixed,
			const float                      timestep
		)
		{
			m_workload.addFromECS
			(
				app,
				app.ecs(),
				components,
				dt,
				dtFixed,
				timestep
			);
		}
	}
}
