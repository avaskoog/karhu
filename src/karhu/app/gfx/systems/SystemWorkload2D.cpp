/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/systems/SystemWorkload2D.hpp>

namespace karhu
{
	namespace gfx
	{
		void SystemWorkload2D::performUpdateComponents
		(
			      app::App                &app,
			      ecs::Pool<Renderdata2D> &components,
			const float                   dt,
			const float                   dtFixed,
			const float                   timestep
		)
		{
			m_workload.addFromECS
			(
				app,
				app.ecs(),
				components,
				dt,
				dtFixed,
				timestep
			);
		}
	}
}
