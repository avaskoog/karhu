/**
 * @author		Ava Skoog
 * @date		2018-10-21
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/systems/SystemRender.hpp>
#include <karhu/app/gfx/components/Camera.hpp>
#include <karhu/app/gfx/components/Animator.hpp>
#include <karhu/app/gfx/Renderer.hpp>
#include <karhu/app/gfx/Pipeline.hpp>
#include <karhu/app/win/Window.hpp>

namespace karhu
{
	namespace gfx
	{
		SystemRender::SystemRender
		(
			win::Window     &window,
			Renderer        &renderer,
			Pipeline        &pipeline,
			Workload        &workload,
			ContainerInputs &inputs
		)
		:
		m_window  {window},
		m_renderer{renderer},
		m_pipeline{pipeline},
		m_workload{workload},
		m_inputs  {inputs}
		{
		}
		
		void SystemRender::performUpdateNoComponents
		(
			app::App &app,
			float const,
			float const,
			float const
		)
		{
			m_workload.sort();
			
			mth::Vec2u const
				size{app.rendersize()};
			
			mth::Vec2f const
				scale{app.renderscale()};
			
			m_pipeline.render
			(
				app,
				m_renderer,
				m_workload,
				m_inputs,
				size.x,
				size.y,
				scale.x,
				scale.y
			);
			
			m_workload.clear();
		}
	}
}
