/**
 * @author		Ava Skoog
 * @date		2017-11-16
 * @copyright	2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/Workload.hpp>
#include <karhu/app/gfx/components/Camera.hpp>
#include <karhu/app/gfx/resources/Material.hpp>
#include <karhu/app/gfx/components/Animator.hpp>
#include <karhu/res/Animation.hpp>

#include <algorithm>
#include <cmath>

/// @todo: Remove this when done testing with it.
#include <karhu/app/App.hpp>
#include <karhu/app/ecs/World.hpp>
#include <karhu/core/log.hpp>
#include <sstream>

namespace
{
	using namespace karhu;
	using namespace karhu::gfx;
	
	constexpr float epsilon{0.001f};
	
	std::int8_t compareAnimations
	(
		const Group &a,
		const Group &b
	)
	{
		return 0; /// @todo: Better to always split on animations for now since otherwise we get issues with things that have the same animation but different clips etc...
		
		/// @todo: This whole thing is pretty much broken right now and needs to be fixed by 1) not comparing memory addresses and 2) accessing current animation differently since it has changed after introducing animation controllers and blendtrees.
		/*
		if (a.mesh.animator && b.mesh.animator)
		{
			res::Animation
				*const A{a.mesh.animator->animation.get()},
				*const B{b.mesh.animator->animation.get()};

			if (A == B)
			{
				if (mth::abs(a.mesh.animator->time() - b.mesh.animator->time()) > epsilon)
				{
					if (a.mesh.animator->time() < b.mesh.animator->time())
						return -1;
					else
						return 1;
				}
			}
			else if (A < B)
				return -1;
			else if (A > B)
				return 1;
		}
		else if (a.mesh.animator < b.mesh.animator)
			return -1;
		else if (a.mesh.animator > b.mesh.animator)
			return 1;
		
		return 0;*/
	}
	
	bool sees(app::App &app, Renderdata *c, Pass &pass)
	{
		Layers layers{pass.camera->layers()};
		bool r; /// @todo: Do also frustum/occlusion culling here.

		// GUI camera can only see the GUI layer.
		if (ModeCamera::GUI == pass.camera->mode())
			layers = Defaultlayers::GUI;
		// Regular camera can see everything except GUI layer.
		else
			layers &= ~Defaultlayers::GUI;
		
		if (0 == pass.camera->layers())
		{
			if (ModeCamera::GUI == pass.camera->mode())
				r = (c->layers() & Defaultlayers::GUI);
			else
				r = (c->layers() != Defaultlayers::GUI);
		}
		else
			r = (c->layers() & layers);
		
		if (!r)
			return false;
		
		return r;
	}
	
	void pushInstanceForRenderdata2D
	(
		Group &group,
		Pass &pass,
		Renderdata2D *const c,
		mth::Mat4f transform
	)
	{
		/// @todo: Try to manipulate sprite matrix directly instead of de- and recomposing.
		
		mth::Quatf r;
		mth::Vec3f p, s;
		mth::decompose(transform, &p, &r, &s);
		
		if (c->billboard())
			r = pass.transformCamera->global().eyerotation();

		s.x *= c->size().x;
		s.y *= c->size().y;
		
		mth::Transformf tmp;
		tmp.position = std::move(p);
		tmp.rotation = std::move(r);
		tmp.scale    = std::move(s);
		
		transform = tmp.matrix();
		
		group.instances.emplace_back(Instance
		{
			static_cast<std::int32_t>(c->entity()),
			std::move(transform),
			&c->inputs()
		});
	}
	
	void pushInstanceForRenderdata3D
	(
		Group &group,
		Renderdata3D *const c,
		mth::Mat4f transform
	)
	{
		group.instances.emplace_back(Instance
		{
			static_cast<std::int32_t>(c->entity()),
			std::move(transform),
			&c->inputs()
		});
	}
	
	void createGroupsForRenderdata2D
	(
		app::App &app,
		Workload &workload,
		Renderdata2D *const c,
		const std::vector<Instance> *const instances
	)
	{
		// We will add this renderable to each matching pass.
		for (auto &pass : workload.passes)
		{
			if (sees(app, c, pass))
			{
				Group group;

				res::Texture *const texture{app.res().get<res::Texture>(c->texture())};

				group.mode           = Rendermode::sprite;
				group.dynamic        = true;
				group.instanced      = c->instanced();
				group.layers         = c->layers();
				group.modeShadows    = c->modeShadows();
				group.sprite.texture = (texture && texture->valid()) ? texture : nullptr;
				group.sprite.tint    = &c->tint();
				group.sprite.UVs[0]  = c->UVBottomLeft().x;
				group.sprite.UVs[1]  = c->UVBottomLeft().y;
				group.sprite.UVs[2]  = c->UVTopRight().x;
				group.sprite.UVs[3]  = c->UVTopRight().y;

				/// @todo: Set mode (dynamic/static, probably based on manual setting).

				if (!instances || instances->empty())
				{
					auto t(app.ecs().componentInEntity<Transform>(c->entity()));
					pushInstanceForRenderdata2D(group, pass, c, t->matrixGlobal());
				}
				else
					for (const Instance &instance : *instances)
						pushInstanceForRenderdata2D(group, pass, c, instance.transform);
				
				pass.groups.emplace_back(std::move(group));
			}
		}
	}
	
	void createGroupsForRenderdata3D
	(
		app::App &app,
		Workload &workload,
		Renderdata3D *const c,
		const std::vector<Instance> *const instances
	)
	{
		res::Mesh *const mesh{app.res().get<res::Mesh>(c->mesh())};
		
		if (!mesh || !mesh->valid())
			return;

		res::Material *const material{app.res().get<res::Material>(c->material())};

		if (!material)
			return;

		res::Shader *const shader{app.res().get<res::Shader>(material->shader)};

		if (!shader || !shader->valid())
			return;

		// We will add this renderable to each matching pass.
		for (auto &pass : workload.passes)
		{
			if (sees(app, c, pass))
			{
				Group group;

				group.mode          = Rendermode::mesh;
				group.dynamic       = true;
				group.instanced     = c->instanced();
				group.layers        = c->layers();
				group.modeShadows   = c->modeShadows();
				group.mesh.material = material;
				group.mesh.shader   = shader;
				group.mesh.mesh     = mesh;
				group.mesh.LOD      = 0;

				/// @todo: Set mode (dynamic/static, probably based on manual setting).
				/// @todo: Frustum cull things behind camera.
				/// @todo: Figure out where and how batching should be chosen (manually set?).

				// Find any animator.
				group.mesh.animator = app.ecs().componentInEntity<Animator>(c->entity());
				
				if (!instances || instances->empty())
				{
					auto t(app.ecs().componentInEntity<Transform>(c->entity()));
					mth::Mat4f transform{t->matrixGlobal()};
					
					// Calculate LOD based on distance from camera and LOD settings.
					
					// Distance between entity and camera.
					const float dist{mth::distance
					(
						/// @todo: We might want to implement some custom functions or do something else so that we can use linalg functions like this without having to cast things...
						t->global().position,
						pass.transformCamera->global().position
					)};

					// Find the closest matching LOD.
					for (std::size_t i{0}; i < mesh->countLevelsLOD(); ++ i)
						if (dist > mesh->distanceAtLOD(i))
							group.mesh.LOD = i;
					
					pushInstanceForRenderdata3D(group, c, transform);
				}
				else
					for (const Instance &instance : *instances)
						pushInstanceForRenderdata3D(group, c, instance.transform);
				
				pass.groups.emplace_back(std::move(group));
			}
		}
	}
}

namespace karhu
{
	namespace gfx
	{
		void Pass::sort()
		{
			if (groups.size() <= 1)
				return;
			
			mth::Vec3f const
				&posCam{transformCamera->global().position};
			
			std::sort
			(
				groups.begin(),
				groups.end(),
				[&posCam](const Group &a, const Group &b) -> bool
				{
					// Meshes before sprites.
					if (a.mode < b.mode)
						return true;
					else if (a.mode > b.mode)
						return false;
					
					/// @todo: Sort sprites by parent-child hierarchy rather than alpha, texture and such since their order is important. For now the hotfix is not to sort sprites at all.
					if (a.mode == Rendermode::sprite)
						return false;
					
					// Transparent after opaque and other queueing.
					switch (a.mode)
					{
						case Rendermode::mesh:
						{
							if (a.mesh.material->queue < b.mesh.material->queue)
								return true;
							else if (a.mesh.material->queue > b.mesh.material->queue)
								return false;
							
							bool const
								depthA{static_cast<bool>(Buffertargets::depth & a.mesh.material->buffertargets)},
								depthB{static_cast<bool>(Buffertargets::depth & b.mesh.material->buffertargets)};
							
							if (depthA && !depthB)
								return true;
							else if (!depthA && depthB)
								return false;
							
							if (!depthA && !depthB)
							{
								// All groups have a single instance at this point.
								mth::Vec3f const
									posA{mth::position(a.instances.back().transform)},
									posB{mth::position(b.instances.back().transform)};
								
								mth::Scalf const
									distA{mth::distance(posA, posCam)},
									distB{mth::distance(posB, posCam)};
								
								// We want most distant first so the comparison is opposite
								// to most in this sorting algorithm; this is not a typo.
								if (distA > distB)
									return true;
								else if (distA < distB)
									return false;
							}
							
							break;
						}
						
						case Rendermode::sprite:
						{
							/// @todo: Remove this when fixing proper sprite sorting.
//							if (a.sprite.tint->a > b.sprite.tint->a)
//								return true;
//							else if (a.sprite.tint->a < b.sprite.tint->a)
//								return false;
							
							break;
						}
					}
					
					// Shadow mode.
					if (a.modeShadows < b.modeShadows)
						return true;
					else if (a.modeShadows > b.modeShadows)
						return false;

					// Order by static or dynamic mode.
					if (a.dynamic < b.dynamic)
						return true;
					else if (a.dynamic > b.dynamic)
						return false;

					// Order by instanced or individual mode.
					if (a.instanced < b.instanced)
						return true;
					else if (a.instanced > b.instanced)
						return false;
					
					// Mode-specific considerations.
					switch (a.mode)
					{
						case Rendermode::mesh:
						{
							if (a.mesh.material->identifier() < b.mesh.material->identifier())
								return true;
							else if (a.mesh.material->identifier() > b.mesh.material->identifier())
								return false;
							
							if (a.mesh.mesh->identifier() < b.mesh.mesh->identifier())
								return true;
							else if (a.mesh.mesh->identifier() > b.mesh.mesh->identifier())
								return false;
							
							if (a.mesh.LOD < b.mesh.LOD)
								return true;
							else if (a.mesh.LOD > b.mesh.LOD)
								return false;
							
							const std::int8_t anim{compareAnimations(a, b)};
							
							if (anim < 0)
								return true;
							else if (anim > 0)
								return false;
							
							break;
						}
						
						case Rendermode::sprite:
						{
							/// @todo: Remove this when fixing proper sprite sorting.
//							if (a.sprite.texture && !b.sprite.texture)
//								return true;
//							else if (!a.sprite.texture && b.sprite.texture)
//								return false;
//
//							if (a.sprite.texture && b.sprite.texture)
//							{
//								if (a.sprite.texture->identifier() < b.sprite.texture->identifier())
//									return true;
//								else if (a.sprite.texture->identifier() > b.sprite.texture->identifier())
//									return false;
//							}
							
							break;
						}
					}
					
					// Order by layers.
					if (a.layers < b.layers)
						return true;
					else if (a.layers > b.layers)
						return false;
					
					return false;
				}
			);
			
			// Now that everything has been sorted it is time to move
			// instances into one group per set of settings instead.
			
			std::vector<Group> groupsNew;
			
			auto createGroupFrom([&groupsNew](const Group &source)
			{
				Group group;
			
				group.mode        = source.mode;
				group.dynamic     = source.dynamic;
				group.instanced   = source.instanced;
				group.layers      = source.layers;
				group.modeShadows = source.modeShadows;
				
				switch (source.mode)
				{
					case Rendermode::mesh:   group.mesh   = source.mesh;   break;
					case Rendermode::sprite: group.sprite = source.sprite; break;
				}
				
				groupsNew.emplace_back(std::move(group));
			});
			
			auto insertInstancesFrom([&groupsNew](const Group &source)
			{
				groupsNew.back().instances.insert
				(
					groupsNew.back().instances.end(),
					source.instances.begin(),
					source.instances.end()
				);
			});
			
			createGroupFrom(groups[0]);
			insertInstancesFrom(groups[0]);
			
			for (std::size_t i{1}; i < groups.size(); ++ i)
			{
				auto
					&curr(groups[i]),
					&prev(groups[i - 1]);
				
				bool same;

				if
				(
					Rendermode::sprite == prev.mode || // Sprites are never grouped.
					curr.mode          != prev.mode
				)
					same = false;
				else
				{
					switch (curr.mode)
					{
						case Rendermode::mesh:
							same =
							(
								curr.mesh.mesh     == prev.mesh.mesh                &&
								curr.mesh.material == prev.mesh.material            &&
								curr.mesh.LOD      == prev.mesh.LOD                 &&
								0                  == compareAnimations(curr, prev)
							);
							break;
							
						case Rendermode::sprite:
							same =
							(
								curr.sprite.texture == prev.sprite.texture                    &&
							 	mth::abs(curr.sprite.tint->r - prev.sprite.tint->r) < epsilon &&
							 	mth::abs(curr.sprite.tint->g - prev.sprite.tint->g) < epsilon &&
							 	mth::abs(curr.sprite.tint->b - prev.sprite.tint->b) < epsilon &&
							 	mth::abs(curr.sprite.tint->a - prev.sprite.tint->a) < epsilon
							);
							break;
					}
				}
				
				if
				(
					curr.dynamic     != prev.dynamic     ||
					curr.instanced   != prev.instanced   ||
					curr.layers      != prev.layers      ||
					curr.modeShadows != prev.modeShadows ||
					!same
				)
					createGroupFrom(curr);
				
				insertInstancesFrom(curr);
			}
			
			// Replace all the old groups with the new one.
			groups = std::move(groupsNew);
		}
		
		void Workload::addFromECS
		(
			app::App &app,
			ecs::World &,
			ecs::Pool<Renderdata2D> &components,
			const float,
			const float,
			const float
		)
		{
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);
				
				if (!c->activeInHierarchy())
					continue;
				
				createGroupsForRenderdata2D(app, *this, c, nullptr);
			}
		}
		
		void Workload::addFromECS
		(
			app::App &app,
			ecs::World &,
			ecs::Pool<Renderdata3D> &components,
			const float,
			const float,
			const float
		)
		{
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);
				
				if (!c->activeInHierarchy())
					continue;
				
				createGroupsForRenderdata3D(app, *this, c, nullptr);
			}
		}
		
		void Workload::addFromECS
		(
			app::App &app,
			ecs::World &world,
			ecs::Pool<Particlesystem> &components,
			const float,
			const float,
			const float
		)
		{
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);
				
				if (!c->activeInHierarchy())
					continue;
				
				std::vector<Instance> instances(c->instances().size());
				
				for (auto const &instance : c->instances())
					instances.emplace_back(Instance{0, instance.transform.matrix()});
				
				if (SpaceParticlesystem::relative == c->space())
				{
					auto t(world.componentInEntity<Transform>(c->entity()));
					
					for (Instance &instance : instances)
					{
						/// @todo: Actually transform properly with parent transform according to rotation and all.
						instance.transform = mth::translate(instance.transform, t->global().position);
					}
				}
				
				if (auto rd = app.ecs().componentInEntity<Renderdata2D>(c->entity()))
					createGroupsForRenderdata2D(app, *this, rd, &instances);
				else if (auto rd = app.ecs().componentInEntity<Renderdata3D>(c->entity()))
					createGroupsForRenderdata3D(app, *this, rd, &instances);
			}
		}

		void Workload::clear()
		{
			passes.clear();
			cameraGUI = nullptr;
		}

		void Workload::sort()
		{
			if (0 == passes.size())
				return;
			
			if (passes.size() > 1)
			{
				// Camera order determines the order of the passes.
				std::sort(passes.begin(), passes.end(), [this](const auto &a, const auto &b)
				{
					/// @todo: Throw passes without valid cameras away entirely.
					if (!a.camera || !b.camera)
						return false;
					
					// GUI camera always first.
					if (ModeCamera::GUI == a.camera->mode())
					{
						/// @todo: Figure out how to deal with preventing more than one GUI camera because this way doesn't work when there are more than one normal camera per pass.
//						if (cameraGUI)
//							log::err("Karhu") << "There is more than one GUI camera";
						
						cameraGUI = a.camera;
						
						return true;
					}
					else if (ModeCamera::GUI == b.camera->mode())
					{
						/// @todo: Figure out how to deal with preventing more than one GUI camera because this way doesn't work when there are more than one normal camera per pass.
//						if (cameraGUI)
//							log::err("Karhu") << "There is more than one GUI camera";
						
						cameraGUI = b.camera;
						
						return false;
					}
					
					return (a.camera->order() <= b.camera->order());
				});
			}
			else if (ModeCamera::GUI == passes[0].camera->mode())
				cameraGUI = passes[0].camera;
			
			for (auto &pass : passes)
				pass.sort();
		}
		
		/// @todo: Remove this when done testing with it.
		void Workload::dump()
		{
			std::stringstream ss;

			for (std::size_t i{0}; i < passes.size(); ++ i)
			{
				const auto &pass(passes[i]);
				
				ss.str("");
				ss << "PASS #" << i << ": camera " << pass.camera;
				ss << " (" << app::App::instance()->ecs().entityName(pass.camera->entity()) << ")";
				if (pass.camera) ss << ", order = " << pass.camera->order();
				if (pass.camera) ss << ", mode = " << ((ModeCamera::GUI == pass.camera->mode()) ? "GUI" : "normal");
				
				log::msg("DEBUG") << ss.str();
				
				for (std::size_t j{0}; j < pass.groups.size(); ++ j)
				{
					const auto &group(pass.groups[j]);
					
					log::msg("DEBUG") << "\tGROUP #" << j << " (" <<
					((Rendermode::mesh == group.mode) ? "mesh" : "sprite")
					<< ", " <<
					((group.instanced) ? "" : "not " )
					<< "instanced"
					<< "):";
					
					log::msg("DEBUG") << "\t\tinstances: " << group.instances.size();
//					for (const auto &instance : group.instances)
//						log::msg("DEBUG") << "\t\t\t " << instance.name;
					
					log::msg("DEBUG") << "\t\tlayers: " << group.layers;
					log::msg("DEBUG") << "\t\tshadowmode: " << static_cast<int>(group.modeShadows);
					
					switch (group.mode)
					{
						case Rendermode::mesh:
						{
							log::msg("DEBUG") << "\t\tmaterial: " << group.mesh.material->identifier();
							
							log::msg("DEBUG") << "\t\tmaterial queue: " << group.mesh.material->queue;
							
							log::msg("DEBUG") << "\t\tLOD: " << group.mesh.LOD;
							
							log::msg("DEBUG") << "\t\tmesh: " << group.mesh.mesh->identifier();
							
							if (!group.mesh.animator)
								log::msg("DEBUG") << "\t\tanim: none";
							else
								log::msg("DEBUG") << "\t\tanim: " << group.mesh.animator->identifier();// << ", animation = " << group.mesh.animator->animation;
							
							break;
						}
						
						case Rendermode::sprite:
						{
							if (!group.sprite.texture)
								log::msg("DEBUG") << "\t\ttexture: none";
							else
								log::msg("DEBUG") << "\t\ttexture: " << group.sprite.texture->identifier();
							
							log::msg("DEBUG")
								<< "\t\ttint: R "
								<< group.sprite.tint->r
								<< " G "
								<< group.sprite.tint->g
								<< " B "
								<< group.sprite.tint->b
								<< " A "
								<< group.sprite.tint->a;
							
							break;
						}
					}
				}
			}
		}
		
		void Workload::dump(std::stringstream &out)
		{
			std::stringstream ss;

			for (std::size_t i{0}; i < passes.size(); ++ i)
			{
				const auto &pass(passes[i]);
				
				ss.str("");
				ss << "PASS #" << i << ": camera " << pass.camera;
				ss << " (" << app::App::instance()->ecs().entityName(pass.camera->entity()) << ")";
				if (pass.camera) ss << ", order = " << pass.camera->order();
				if (pass.camera) ss << ", mode = " << ((ModeCamera::GUI == pass.camera->mode()) ? "GUI" : "normal");
				
				out << ss.str() << '\n';
				
				for (std::size_t j{0}; j < pass.groups.size(); ++ j)
				{
					const auto &group(pass.groups[j]);
					
					out << "\tGROUP #" << j << " (" <<
					((Rendermode::mesh == group.mode) ? "mesh" : "sprite")
					<< ", " <<
					((group.instanced) ? "" : "not " )
					<< "instanced"
					<< "):" << '\n';
					
					out << "\t\tinstances: " << group.instances.size() << '\n';
//					for (const auto &instance : group.instances)
//						out << "\t\t\t " << instance.name;
					
					out << "\t\tlayers: " << group.layers << '\n';
					out << "\t\tshadowmode: " << static_cast<int>(group.modeShadows) << '\n';
					
					switch (group.mode)
					{
						case Rendermode::mesh:
						{
							out << "\t\tmaterial: " << group.mesh.material->identifier() << '\n';
							
							out << "\t\tmaterial queue: " << group.mesh.material->queue << '\n';
							
							out << "\t\tLOD: " << group.mesh.LOD << '\n';
							
							out << "\t\tmesh: " << group.mesh.mesh->identifier() << '\n';
							
							if (!group.mesh.animator)
								out << "\t\tanim: none" << '\n';
							else
								out << "\t\tanim: " << group.mesh.animator->identifier() << '\n';// << ", animation = " << group.mesh.animator->animation;
							
							break;
						}
						
						case Rendermode::sprite:
						{
							if (!group.sprite.texture)
								out << "\t\ttexture: none" << '\n';
							else
								out << "\t\ttexture: " << group.sprite.texture->identifier() << '\n';
							
							out
								<< "\t\ttint: R "
								<< group.sprite.tint->r
								<< " G "
								<< group.sprite.tint->g
								<< " B "
								<< group.sprite.tint->b
								<< " A "
								<< group.sprite.tint->a
								<< '\n';
							
							break;
						}
					}
				}
			}
		}
	}
}
