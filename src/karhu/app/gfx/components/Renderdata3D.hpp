/**
 * @author		Ava Skoog
 * @date		2017-11-20
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_RENDERDATA_3D_H_
	#define KARHU_GRAPHICS_RENDERDATA_3D_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/gfx/components/Renderdata.hpp>
	#include <karhu/res/Resource.hpp>

	namespace karhu
	{
		namespace gfx
		{
			class Renderdata3D : public Renderdata
			{
				public:
					using Renderdata::Renderdata;
				
				public:
					//! @todo: Make Renderdata3D::mesh take/return Ref<Mesh>
					void mesh(res::IDResource const);
					res::IDResource mesh() const noexcept { return m_mesh; }
				
					//! @todo: Make Renderdata3D::material take/return Ref<Material>
					void material(res::IDResource const);
					res::IDResource material() const noexcept { return m_material; }
					
				protected:
					virtual void performSerialise(conv::Serialiser &) const override;
					virtual void performDeserialise(conv::Serialiser const &) override;
					virtual mth::Bounds3f const &performGetAABB() const noexcept override { return m_AABB; }
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
					virtual void performEditorEvent(app::App &, edt::Editor &, edt::Identifier const, edt::Event const &) override {}
					virtual bool performEditorEdit(app::App &, edt::Editor &) override;
					#endif

				private:
					res::IDResource
						m_mesh    {0},
						m_material{0};
				
				private:
					mth::Bounds3f
						m_AABB{};
			};
		}
	}
#endif
