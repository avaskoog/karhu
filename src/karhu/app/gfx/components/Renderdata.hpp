/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_RENDERDATA_H_
	#define KARHU_GRAPHICS_RENDERDATA_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/gfx/components/Transform.hpp>
	#include <karhu/conv/graphics.hpp>
	#include <karhu/app/ecs/common.hpp>

	namespace karhu
	{
		namespace gfx
		{
			enum class ModeShadows : std::uint32_t
			{
				none,
				normal,
				custom,
				normalOnlyShadow,
				customOnlyShadow
			};
			
			class Renderdata : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
				public:
					bool instanced() const noexcept { return m_instanced; }
					void instanced(bool const);
				
					Layers layers() const noexcept { return m_layers; }
					void layers(Layers const);
				
					ModeShadows modeShadows() const noexcept { return m_modeShadows; }
					void modeShadows(ModeShadows const);
				
					ContainerInputs &inputs() noexcept { return m_inputs; }
					ContainerInputs const &inputs() const noexcept { return m_inputs; }
				
					mth::Bounds3f const &AABB() const noexcept { return performGetAABB(); }
					mth::Bounds3f const AABB(mth::Transformf const &) const noexcept;

				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(conv::Serialiser const &);

					#if KARHU_EDITOR_SERVER_SUPPORTED
					void editorEvent(app::App &, edt::Editor &, edt::Identifier const /*caller*/, edt::Event const &) override;
					bool editorEdit(app::App &, edt::Editor &) override;
					#endif

				protected:
					virtual void performSerialise(conv::Serialiser &) const = 0;
					virtual void performDeserialise(conv::Serialiser const &) = 0;
					virtual mth::Bounds3f const &performGetAABB() const noexcept = 0;
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
					virtual void performEditorEvent(app::App &, edt::Editor &, edt::Identifier const /*caller*/, edt::Event const &) = 0;
					virtual bool performEditorEdit(app::App &, edt::Editor &) = 0;
					#endif

				private:
					bool
						m_instanced{false};
				
					Layers
						m_layers{gfx::Defaultlayers::standard};
				
					ModeShadows
						m_modeShadows{ModeShadows::normal};
				
					ContainerInputs
						m_inputs;
				
				protected:
					mth::Bounds3f m_AABB{};
			};
		}
	}
#endif
