/**
 * @author		Ava Skoog
 * @date		2017-10-22
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_CAMERA_H_
	#define KARHU_GRAPHICS_CAMERA_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/app/gfx/Frustum.hpp>
	#include <karhu/app/gfx/components/Transform.hpp>
	#include <karhu/res/Bank.hpp>
	#include <karhu/res/Material.hpp>
	#include <karhu/conv/graphics.hpp>
	#include <karhu/core/macros.hpp>

	#include <cstdint>
	#include <type_traits>

	namespace karhu
	{
		namespace gfx
		{
			class Rendertarget;
			
			enum class TypeProjection
			{
				perspective,
				orthographic
			};
			
			struct Projection
			{
				TypeProjection type{TypeProjection::perspective};
				mth::Radsf FOV{1.1f};
				float near{0.1f}, far{500.0f};

				void serialise(conv::Serialiser &) const;
				void deserialise(const conv::Serialiser &);
			};
	
			enum class ModeCamera
			{
				normal,
				GUI
			};
	
			enum class Clearflag
			{
				none    =  0,
				colour  =  1 << 0,
				depth   =  1 << 1,
				stencil =  1 << 2,
				all     = (1 << 3) - 1
			};
			
			karhuBITFLAGGIFY(Clearflag)
			
			using Clearflags = Clearflag;

			class Camera : public ecs::Component
			{
				public:/*
					struct Planes
					{
						struct Plane
						{
							mth::Vec3f corners[4], normal;
							mth::Scalf d;
						} left, right, top, bottom, near, far;
					};*/
				
				public:
					Camera
					(
						ecs::IDComponent     const identifier,
						ecs::IDTypeComponent const typeComponent,
						ecs::IDEntity        const entity
					)
					:
					ecs::Component{identifier, typeComponent, entity}
					{
						recalculateProjection();
					}
				
					void matrixProjection(mth::Mat4f const &);
					void matrixProjection(mth::Mat4f &&);

					const mth::Mat4f &matrixProjection() const
					{
						return m_matrixProjection;
					}

					const mth::Mat4f matrixView(const Transform &t) const;
					
					const mth::Vec2f positionInViewport
					(
						const mth::Vec3f &positionInWorld,
						const Transform  &transformCamera
					) const;
				
					bool updateProjection
					(
						std::uint32_t const,
						std::uint32_t const,
						float const,
						float const
					);
					
					bool recalculateProjection();
					
					Frustum frustum(const Transform &) const;
				
				public:
					ModeCamera mode() const noexcept { return m_mode; }
					void mode(ModeCamera const);
				
					Clearflags clearflags() const noexcept { return m_clearflags; }
					void clearflags(Clearflags const);
				
					ColRGBAf const &clearcolour() const noexcept { return m_clearcolour; }
					void clearcolour(ColRGBAf const &);
				
					std::uint8_t clearstencil() const noexcept { return m_clearstencil; }
					void clearstencil(std::uint8_t const);
				
					mth::Vec2u const &viewport() const noexcept { return m_viewport; }
					void viewport(mth::Vec2u const &);
				
					Projection const &projection() const noexcept { return m_projection; }
					void projection(Projection const &);
				
					Order order() const noexcept { return m_order; }
					void order(Order const);
				
					Layers layers() const noexcept { return m_layers; }
					void layers(Layers const);
				
					/// @todo: Camera rendertarget should be Ref<T> once that is in place.
					Rendertarget *target() const noexcept { return m_target; }
					void target(Rendertarget *const);
				
					/// @todo: Camera effect should be Ref<T> once that is in place.
					void effect(res::IDResource const);
					res::IDResource effect() const noexcept { return m_effect; }
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
					
					#if KARHU_EDITOR_SERVER_SUPPORTED
					void editorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) override {}
					bool editorEdit(app::App &, edt::Editor &) override;
					#endif

				public:
float orthosize{50.0f};/// @todo: Quick hack to get GUI back before redesigning pipelines with renderbuffers?

				private:
					ModeCamera mutable
						m_mode{ModeCamera::normal};
				
					Clearflags
						m_clearflags{Clearflags::all};
				
					ColRGBAf
						m_clearcolour{0.0f, 0.0f, 0.0f, 1.0f};
				
					std::uint8_t
						m_clearstencil{0};

					mth::Vec2u
						m_viewport{256u, 256u};
				
					Projection
						m_projection;
				
					Order
						m_order{0};
				
					Layers
						m_layers{0};
				
					Rendertarget
						*m_target{nullptr};
				
				private:
					res::IDResource
						m_effect{0};

				private:
					mth::Mat4f
						m_matrixProjection;
				
					Frustum
						m_frustum;
			};
		}
	}
#endif
