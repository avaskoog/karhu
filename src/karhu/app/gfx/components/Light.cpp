/**
 * @author		Ava Skoog
 * @date		2019-08-08
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/components/Light.hpp>
#include <karhu/data/serialise.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/karhuimgui.hpp>
#include <karhu/app/edt/dropdown.inl>
#endif

namespace karhu
{
	namespace gfx
	{
		void Light::diameter(const float v) noexcept
		{
			switch (m_type)
			{
				case TypeLight::directional:
					break;
					
				case TypeLight::point:
					m_data.point.diameter = v; break;
			}
		}
		
		float Light::diameter() const noexcept
		{
			switch (m_type)
			{
				case TypeLight::directional:
					return 0.0f;
					
				case TypeLight::point:
					return m_data.point.diameter;
			}
		}
		
		void Light::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("type",      m_type)
				<< karhuINn("colour",    m_colour)
				<< karhuINn("intensity", m_intensity);
			
			switch (m_type)
			{
				case TypeLight::directional:
					break;
				
				case TypeLight::point:
					ser << karhuINn("diameter", m_data.point.diameter);
					break;
			}
		}
		
		void Light::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUTn("type",      m_type)
				>> karhuOUTn("colour",    m_colour)
				>> karhuOUTn("intensity", m_intensity);
			
			switch (m_type)
			{
				case TypeLight::directional:
					break;
				
				case TypeLight::point:
				{
					ser >> karhuOUTn("diameter", m_data.point.diameter);
					break;
				}
			}
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Light::editorEdit(app::App &, edt::Editor &editor)
		{
			bool r{false};
			
			editor.header("General properties");
			
			// Type dropdown.
			
			constexpr std::pair<char const *, TypeLight>
				types[]
				{
					{"Directional", TypeLight::directional},
					{"Point",       TypeLight::point}
				};
			
			if (edt::dropdown("Type", m_type, std::begin(types), std::end(types)))
				r = true;
			
			// Colour picker.
			
			ImGui::ColorEdit3
			(
				"Colour",
				&m_colour.r
			);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			// Intensity slider.
			
			ImGui::DragFloat
			(
				"Intensity",
				&m_intensity,
				0.01f, // Speed.
				0.0f,  // Minimum.
				10.0f  // Maximum.
			);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			// Type-specific options.
			
			switch (m_type)
			{
				case TypeLight::directional:
					break;
				
				case TypeLight::point:
				{
					editor.header("Point light properties");
					
					ImGui::DragFloat
					(
						"Diameter",
						&m_data.point.diameter,
						0.01f, // Speed.
						0.01f, // Minimum.
						std::numeric_limits<float>::max()
					);
					
					if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
						r = true;
					
					break;
				}
			}
			
			return r;
		}
		#endif
	}
}
