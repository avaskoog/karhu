/**
 * @author		Ava Skoog
 * @date		2023-01-16
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_BONEBINDER_H_
	#define KARHU_GRAPHICS_BONEBINDER_H_

	#include <karhu/app/edt/support.hpp>
	#include <karhu/res/Rig.hpp>

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/conv/graphics.hpp>

	#include <cstdint>

	namespace karhu
	{
		namespace gfx
		{
			class Bonebinder : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
				public:
					void IDJoint(std::size_t const);
					std::size_t IDJoint() const { return m_IDJoint; }
				
					void offset(mth::Vec3f const &);
					mth::Vec3f const &offset() const { return m_offset; }
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
				
				private:
					std::size_t
						m_IDJoint{0};
				
					// Didn't plan ahead adequately for this sort of thing,
					// and need to get objects to follow bones as soon as possible,
					// so instead of using the transform, offset just has to be set.
					mth::Vec3f
						m_offset{};
				
				#if KARHU_EDITOR_SERVER_SUPPORTED
				public:
                    void editorEvent(app::App &, edt::Editor &, edt::Identifier const, edt::Event const &) override {}
                    bool editorEdit(app::App &, edt::Editor &) override;
                #endif
			};
		}
	}
#endif
