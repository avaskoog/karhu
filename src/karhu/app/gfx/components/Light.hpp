/**
 * @author		Ava Skoog
 * @date		2019-08-08
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_LIGHT_H_
	#define KARHU_GRAPHICS_LIGHT_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/conv/graphics.hpp>

	#include <cstdint>

	namespace karhu
	{
		namespace gfx
		{
			enum class TypeLight : std::int32_t
			{
				directional,
				point
			};
		
			/// @todo: Require transform.
			class Light : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
				public:
					void type(const TypeLight type) noexcept { m_type = type; }
					TypeLight type() const noexcept { return m_type; }
				
					void colour(const ColRGBf &v) noexcept { m_colour = v; }
					const ColRGBf &colour() const noexcept { return m_colour; }
				
					void intensity(const float v) noexcept { m_intensity = v; }
					float intensity() const noexcept { return m_intensity; }
				
					void diameter(const float) noexcept;
					float diameter() const noexcept;
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
				
				private:
					TypeLight m_type     {TypeLight::point};
					ColRGBf   m_colour   {1.0f, 1.0f, 1.0f};
					float     m_intensity{1.0};
				
					struct Point
					{
						float diameter{1.0f};
					};
				
					union Data
					{
						Point point;
						
						Data() { new(&point) Point; }
					} m_data;
				
				#if KARHU_EDITOR_SERVER_SUPPORTED
				public:
                    void editorEvent(app::App &, edt::Editor &, edt::Identifier const, edt::Event const &) override {}
                    bool editorEdit(app::App &, edt::Editor &) override;
                #endif
			};
		}
	}
#endif
