/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_RENDERDATA_2D_H_
	#define KARHU_GRAPHICS_RENDERDATA_2D_H_

	#include <karhu/app/edt/support.hpp>
	
	#include <karhu/app/gfx/components/Renderdata.hpp>
	#include <karhu/res/Resource.hpp>

	namespace karhu
	{
		namespace gfx
		{
			class Renderdata2D : public Renderdata
			{
				public:
					using Renderdata::Renderdata;

				public:
					void texture(res::IDResource const);
					res::IDResource texture() const noexcept { return m_texture; }
				
					void billboard(bool const);
					bool billboard() const noexcept { return m_billboard; }
				
					void tint(ColRGBAf const &);
					ColRGBAf const &tint() const noexcept { return m_tint; }
				
					void size(mth::Vec2f const);
					mth::Vec2f const &size() const noexcept { return m_size; }
				
					void UVBottomLeft(mth::Vec2f const);
					mth::Vec2f const &UVBottomLeft() const noexcept { return m_UVBottomLeft; }
				
					void UVTopRight(mth::Vec2f const);
					mth::Vec2f const &UVTopRight() const noexcept { return m_UVTopRight; }
				
				protected:
					virtual void performSerialise(conv::Serialiser &) const override;
					virtual void performDeserialise(conv::Serialiser const &) override;
				
					virtual mth::Bounds3f const &performGetAABB() const noexcept override
					{
						// Square root of 2 to get diagonal of 45 deg rotated 1x1 plane.
						constexpr mth::Scalf
							n{1.4142135623730951f};
						
						static mth::Bounds3f const
							r{{0.0f, 0.0f, 0.0f}, {n, n, n}};
						
						return r;
					}
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
					virtual void performEditorEvent(app::App &, edt::Editor &, edt::Identifier const /*caller*/, edt::Event const &) override {}
					virtual bool performEditorEdit(app::App &, edt::Editor &) override;
					#endif
				
				private:
					res::IDResource m_texture  {0};
					ColRGBAf        m_tint     {1.0f, 1.0f, 1.0f, 1.0f};
					bool            m_billboard{false};
				
					mth::Vec2f
						m_size        {1.0f, 1.0f},
						m_UVBottomLeft{0.0f, 0.0f},
						m_UVTopRight  {1.0f, 1.0f};
			};
		}
	}
#endif
