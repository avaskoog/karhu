/**
 * @author		Ava Skoog
 * @date		2017-11-03
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/components/Camera.hpp>
#include <karhu/app/gfx/resources/Material.hpp>
#include <karhu/app/gfx/Rendertarget.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/karhuimgui.hpp>
#include <karhu/app/edt/dropdown.inl>
#endif

namespace karhu
{
	namespace gfx
	{
		const mth::Mat4f Camera::matrixView(const Transform &t) const
		{
			// https://stackoverflow.com/a/23337476/5012939
			// Should probably be more like this:
			/*
			mth::Mat4f const
				T{mth::translationmatrix(t.global().position)},
				R{mth::rotationmatrix(t.global().rotation)};
			
			return R * mth::inverse(T);
			*/
			
			// But I janked this engine up early and I'm rewriting it for
			// the next game anyway, so let's just keep the weirdness and
			// finish the current game with what we have, working around it.
			
			/// @todo: Quick hack to get GUI back before redesigning pipelines with renderbuffers?
			// ^ Yeah, not happening, sweaty ;*
			if (ModeCamera::GUI == m_mode)
				return mth::inverse(t.global().matrix());
			else
				return mth::projection::look
				(
					t.global().position,
					t.global().position + t.global().forward(),
					mth::upf()
				);
		}

		const mth::Vec2f Camera::positionInViewport
		(
			const mth::Vec3f &positionInWorld,
			const Transform  &transformCamera
		) const
		{
			const auto p
			(
				matrixProjection() *
				matrixView(transformCamera) *
				mth::Vec4f{positionInWorld, 1.0f}
			);
			
			auto r(mth::Vec2f{p} / p.w);
			
			r += 1.0f;
			r /= 2.0f;
			r *= viewport();
			
			return r;
		}
		
		void Camera::matrixProjection(mth::Mat4f const &matrix)
		{
			m_matrixProjection = matrix;
		}
		
		void Camera::matrixProjection(mth::Mat4f &&matrix)
		{
			m_matrixProjection = std::move(matrix);
		}
		
		bool Camera::updateProjection
		(
			std::uint32_t const width,
			std::uint32_t const height,
			float const factorX,
			float const factorY
		)
		{
			// Check if the dimensions have changed.

			const auto
				w(static_cast<std::uint32_t>(std::round(static_cast<float>(width)  * factorX))),
				h(static_cast<std::uint32_t>(std::round(static_cast<float>(height) * factorY)));

			if (w != m_viewport.x || h != m_viewport.y)
			{
				viewport({w, h});
				return recalculateProjection();
			}
			
			return true;
		}
		
		bool Camera::recalculateProjection()
		{
			if (0 == viewport().x || 0 == viewport().y)
			{
				log::err("Karhu") << "Cannot calculate projection for camera with viewport with zero width or height";
				return false;
			}
			
			m_frustum.tangent = mth::tan(projection().FOV * 0.5f);
			m_frustum.ratio   = static_cast<float>(viewport().x) / static_cast<float>(viewport().y);
			m_frustum.near    = projection().near;
			m_frustum.far     = projection().far;
			
			// Set up the correct matrix.
			switch (projection().type)
			{
				case TypeProjection::perspective:
				{
					m_matrixProjection = mth::projection::perspective
					(
						projection().FOV,
						m_frustum.ratio,
						projection().near,
						projection().far
					);
					
					break;
				}
				
				case TypeProjection::orthographic:
				{
/// @todo: Quick hack to get GUI back before redesigning pipelines with renderbuffers?
if (ModeCamera::GUI == mode())
	m_matrixProjection = mth::projection::orthographic
	(
		0.0f,                           // Left.
		static_cast<float>(viewport().x), // Right.
		0.0f,                           // Bottom,
		static_cast<float>(viewport().y), // Top.
	 
		projection().near,
		projection().far
	);
else
					/*m_matrixProjection = mth::projection::orthographic
					(
					 	/// @todo: Figure out if pixel perfect orthographic is needed.
					 
//						0.0f,                           // Left.
//						static_cast<float>(viewport.x), // Right.
//						0.0f,                           // Bottom,
//						static_cast<float>(viewport.y), // Top.
					 
						-orthosize,                   // Left.
						orthosize,                   // Right.
						-orthosize,                   // Bottom,
						orthosize, // Top.
					 
						projection().near,
						projection().far
					);*/
					
					break;
				}
			}
			/*
			using P = Frustum::Plane;
			

			m(3,1) + m(4,1),
				 m(3,2) + m(4,2),
				 m(3,3) + m(4,3),
				 m(3,4) + m(4,4));
	pl[FARP].setCoefficients(
				-m(3,1) + m(4,1),
				-m(3,2) + m(4,2),
				-m(3,3) + m(4,3),
				-m(3,4) + m(4,4));
	pl[BOTTOM].setCoefficients(
				 m(2,1) + m(4,1),
				 m(2,2) + m(4,2),
				 m(2,3) + m(4,3),
				 m(2,4) + m(4,4));
	pl[TOP].setCoefficients(
				-m(2,1) + m(4,1),
				-m(2,2) + m(4,2),
				-m(2,3) + m(4,3),
				-m(2,4) + m(4,4));
	pl[LEFT].setCoefficients(
				 m(1,1) + m(4,1),
				 m(1,2) + m(4,2),
				 m(1,3) + m(4,3),
				 m(1,4) + m(4,4));
	pl[RIGHT].setCoefficients(
				-m(1,1) + m(4,1),
				-m(1,2) + m(4,2),
				-m(1,3) + m(4,3),
				-m(1,4) + m(4,4)
			*/
			
			return true;
		}
		
		Frustum Camera::frustum(const Transform &t) const
		{
			/// @todo: There is glm::frustum, can we just use that instead?
			
			using P = Frustum::Plane;
			
			Frustum r{m_frustum};
			
			r.position = t.global().position;
			r.forward  = t.global().eyeforward();
			r.right    = t.global().eyeright();
			r.up       = t.global().eyeup();
			
			const mth::Mat4f m{m_matrixProjection * matrixView(t)};
			
			for (int i{0}; i < 4; ++ i) r.planes[P::left]  [i] = m[i][3] + m[i][0];
			for (int i{0}; i < 4; ++ i) r.planes[P::right] [i] = m[i][3] - m[i][0];
			for (int i{0}; i < 4; ++ i) r.planes[P::bottom][i] = m[i][3] + m[i][1];
			for (int i{0}; i < 4; ++ i) r.planes[P::top]   [i] = m[i][3] - m[i][1];
			for (int i{0}; i < 4; ++ i) r.planes[P::near]  [i] = m[i][3] + m[i][2];
			for (int i{0}; i < 4; ++ i) r.planes[P::far]   [i] = m[i][3] - m[i][2];
			
//			for (std::size_t i{0}; i < Frustum::Plane::count; ++ i)
//				r.planes[i] /= mth::length(mth::Vec3f{r.planes[i]});
			
			/*
			const mth::Scalf
				tangent       {2.0f * m_frustum.tangent},
				nearHeight    {tangent * projection.near},
				farHeight     {tangent * projection.far},
				nearWidth     {nearHeight * m_frustum.ratio},
				farWidth      {farHeight * m_frustum.ratio},
				nearHeightHalf{nearHeight * 0.5f},
				farHeightHalf {farHeight * 0.5f},
				nearWidthHalf {nearWidth * 0.5f},
				farWidthHalf  {farWidth * 0.5f};
			
			const mth::Vec3f
				nearCentre     {t.position - r.forward * projection.near},
				farCentre      {t.position - r.forward * projection.far},
				farTopLeft     {farCentre + r.up * farHeightHalf - r.right * farWidthHalf},
				farTopRight    {farCentre + r.up * farHeightHalf + r.right * farWidthHalf},
				farBottomLeft  {farCentre - r.up * farHeightHalf - r.right * farWidthHalf},
				farBottomRight {farCentre - r.up * farHeightHalf + r.right * farWidthHalf},
				nearTopLeft    {nearCentre + r.up * nearHeightHalf - r.right * nearWidthHalf},
				nearTopRight   {nearCentre + r.up * nearHeightHalf + r.right * nearWidthHalf},
				nearBottomLeft {nearCentre - r.up * nearHeightHalf - r.right * nearWidthHalf},
				nearBottomRight{nearCentre - r.up * nearHeightHalf + r.right * nearWidthHalf};
			
			using P = Frustum::Plane;
			
			r.planes[P::left].w   = -mth::dot(mth::Vec3f{r.planes[P::left]},   nearBottomLeft);
			r.planes[P::right].w  = -mth::dot(mth::Vec3f{r.planes[P::right]},  nearTopRight);
			r.planes[P::bottom].w = -mth::dot(mth::Vec3f{r.planes[P::bottom]}, nearBottomRight);
			r.planes[P::top].w    = -mth::dot(mth::Vec3f{r.planes[P::top]},    nearTopLeft);
			r.planes[P::near].w   = -mth::dot(mth::Vec3f{r.planes[P::near]},   nearTopRight);
			r.planes[P::far].w    = -mth::dot(mth::Vec3f{r.planes[P::far]},    farTopLeft);
			*/
			return r;
		}
		
		/*
		Camera::Planes Camera::planes(const mth::Transformf &t) const
		{
			enum
			{
				left,
				right,
				bottom,
				top,
				near,
				far
			};
			
			Planes result;
			
			// Update the clipping planes using the Hartmann/Gribbs method.
			// See: https://stackoverflow.com/questions/12836967/extracting-view-frustum-planes-hartmann-gribbs-method
			mth::Vec4f p[6];
			for (int i{0}; i < 4; ++ i) p[left]  [i] = m_matrixProjection[i][3] + m_matrixProjection[i][0];
			for (int i{0}; i < 4; ++ i) p[right] [i] = m_matrixProjection[i][3] - m_matrixProjection[i][0];
			for (int i{0}; i < 4; ++ i) p[bottom][i] = m_matrixProjection[i][3] + m_matrixProjection[i][1];
			for (int i{0}; i < 4; ++ i) p[top]   [i] = m_matrixProjection[i][3] - m_matrixProjection[i][1];
			for (int i{0}; i < 4; ++ i) p[near]  [i] = m_matrixProjection[i][3] + m_matrixProjection[i][2];
			for (int i{0}; i < 4; ++ i) p[far]   [i] = m_matrixProjection[i][3] - m_matrixProjection[i][2];

			const mth::Scalf
				tangent       {2.0f * mth::tan(projection.FOV / 2.0f)},
				nearHeight    {tangent * projection.near},
				farHeight     {tangent * projection.far},
				nearWidth     {nearHeight * m_ratio},
				farWidth      {farHeight * m_ratio},
				nearHeightHalf{nearHeight * 0.5f},
				farHeightHalf {farHeight * 0.5f},
				nearWidthHalf {nearWidth * 0.5f},
				farWidthHalf  {farWidth * 0.5f};
			
			const mth::Vec3f
				f              {t.forward()},
				r              {t.right()},
				u              {t.up()},
				nearCentre     {t.position - f * projection.near},
				farCentre      {t.position - f * projection.far},
				farTopLeft     {farCentre + u * farHeightHalf - r * farWidthHalf},
				farTopRight    {farCentre + u * farHeightHalf + r * farWidthHalf},
				farBottomLeft  {farCentre - u * farHeightHalf - r * farWidthHalf},
				farBottomRight {farCentre - u * farHeightHalf + r * farWidthHalf},
				nearTopLeft    {nearCentre + u * nearHeightHalf - r * nearWidthHalf},
				nearTopRight   {nearCentre + u * nearHeightHalf + r * nearWidthHalf},
				nearBottomLeft {nearCentre - u * nearHeightHalf - r * nearWidthHalf},
				nearBottomRight{nearCentre - u * nearHeightHalf + r * nearWidthHalf};
			
			result.near.corners[0] = nearBottomRight;
			result.near.corners[1] = nearBottomLeft;
			result.near.corners[2] = nearTopLeft;
			result.near.corners[3] = nearTopRight;
			result.near.normal     = {p[near].x, p[near].y, p[near].z};
			result.near.d          = p[near].w;
			
			result.far.corners[0] = farBottomRight;
			result.far.corners[1] = farBottomLeft;
			result.far.corners[2] = farTopLeft;
			result.far.corners[3] = farTopRight;
			result.far.normal     = {p[far].x, p[far].y, p[far].z};
			result.far.d          = p[far].w;
			
			result.left.corners[0] = farBottomLeft;
			result.left.corners[1] = farTopLeft;
			result.left.corners[2] = nearTopLeft;
			result.left.corners[3] = nearBottomLeft;
			result.left.normal     = {p[left].x, p[left].y, p[left].z};
			result.left.d          = p[left].w;
			
			result.right.corners[0] = nearBottomRight;
			result.right.corners[1] = nearTopRight;
			result.right.corners[2] = farTopRight;
			result.right.corners[3] = farBottomRight;
			result.right.normal     = {p[right].x, p[right].y, p[right].z};
			result.right.d          = p[right].w;
			
			result.bottom.corners[0] = nearBottomRight;
			result.bottom.corners[1] = farBottomRight;
			result.bottom.corners[2] = farBottomLeft;
			result.bottom.corners[3] = nearBottomLeft;
			result.bottom.normal     = {p[bottom].x, p[bottom].y, p[bottom].z};
			result.bottom.d          = p[bottom].w;
			
			result.top.corners[0] = nearTopLeft;
			result.top.corners[1] = farTopLeft;
			result.top.corners[2] = farTopRight;
			result.top.corners[3] = nearTopRight;
			result.top.normal     = {p[top].x, p[top].y, p[top].z};
			result.top.d          = p[top].w;
			
			return result;
		}
*/
		
		void Camera::mode(ModeCamera const v)
		{
			m_mode = v;
		}
		
		void Camera::clearflags(Clearflags const v)
		{
			m_clearflags = v;
		}
		
		void Camera::clearcolour(ColRGBAf const &v)
		{
			m_clearcolour = v;
		}
		
		void Camera::clearstencil(std::uint8_t const v)
		{
			m_clearstencil = v;
		}
		
		void Camera::viewport(mth::Vec2u const &v)
		{
			m_viewport = v;
		}
		
		void Camera::projection(Projection const &v)
		{
			m_projection = v;
		}
		
		void Camera::order(Order const v)
		{
			m_order = v;
		}
		
		void Camera::layers(Layers const v)
		{
			m_layers = v;
		}
		
		void Camera::target(Rendertarget *const v)
		{
			m_target = v;
		}
		
		void Camera::effect(res::IDResource const v)
		{
			m_effect = v;
		}

		void Camera::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("mode",         m_mode)
				<< karhuINn("clearflags",   m_clearflags)
				<< karhuINn("clearcolour",  m_clearcolour)
				<< karhuINn("clearstencil", m_clearstencil)
				<< karhuINn("viewport",     m_viewport)
				<< karhuINn("projection",   m_projection)
				<< karhuINn("order",        m_order)
				<< karhuINn("layers",       m_layers)
				<< karhuINn("effect",       m_effect);
				/// @todo: Target?
		}

		void Camera::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUTn("mode",         m_mode)
				>> karhuOUTn("clearflags",   m_clearflags)
				>> karhuOUTn("clearcolour",  m_clearcolour)
				>> karhuOUTn("clearstencil", m_clearstencil)
				>> karhuOUTn("viewport",     m_viewport)
				>> karhuOUTn("projection",   m_projection)
				>> karhuOUTn("order",        m_order)
				>> karhuOUTn("layers",       m_layers)
				>> karhuOUTn("effect",       m_effect);
				/// @todo: Target?
		}

		void Projection::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(type)
				<< karhuIN(FOV)
				<< karhuIN(near)
				<< karhuIN(far);
		}
		
		void Projection::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(type)
				>> karhuOUT(FOV)
				>> karhuOUT(near)
				>> karhuOUT(far);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Camera::editorEdit(app::App &app, edt::Editor &editor)
		{
			bool r{false};
			
			// Mode.
			
			constexpr std::pair<char const *, ModeCamera>
				modes[]
				{
					{"Normal", ModeCamera::normal},
					{"GUI",    ModeCamera::GUI}
				};
			
			if (edt::dropdown("Mode", m_mode, std::begin(modes), std::end(modes)))
				r = true;
			
			// Clearflags.
			
			constexpr std::pair<char const *, Clearflag>
				flags[]
				{
					{"Colour",  Clearflag::colour},
					{"Depth",   Clearflag::depth},
					{"Stencil", Clearflag::stencil}
				};
			
			if (edt::dropdown("Clearflags", m_clearflags, std::begin(flags), std::end(flags), true))
				r = true;
			
			// Clearcolour.
			
			ImGui::ColorEdit4("Clearcolour", &m_clearcolour.r);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			// Clearstencil.
			
			ImGui::InputScalar("Clearstencil", app::datatypeImGui<decltype(m_clearstencil)>(), &m_clearstencil);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			// Viewport dimensions.
			
			ImGui::InputScalarN("Viewport", app::datatypeImGui<decltype(m_viewport.x)>(), &m_viewport.x, 2);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			// Order.
			
			ImGui::InputScalar("Order", app::datatypeImGui<decltype(m_order)>(), &m_order);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			// Layers.
			
			if (edt::dropdownLayersGraphics(app, m_layers, false))
				r = true;
			
			// Effect shader.
			
			if (editor.pickerResource("cam-effect", "Effect", m_effect, app.res().IDOfType<res::Shader>()))
				r = true;
			
			// Projection.
			
			editor.header("Projection properties");
			
			// Type.
			
			constexpr std::pair<char const *, TypeProjection>
				types[]
				{
					{"Perspective",  TypeProjection::perspective},
					{"Orthographic", TypeProjection::orthographic}
				};
			
			if (edt::dropdown("Type", m_projection.type, std::begin(types), std::end(types)))
				r = true;
			
			// Field of view.
			/// @todo: Camera FOV editable with degrees instead of radians?
			
			ImGui::DragFloat("Field of view", &m_projection.FOV, 0.001f, mth::radians(10.0f), mth::radians(140.0f));
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			// Clip near.
			
			ImGui::DragFloat("Near clip", &m_projection.near, 0.0001f);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			// Clip far.
			
			ImGui::DragFloat("Far clip", &m_projection.far, 1.0f);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			return r;
		}
		#endif
	}
}
