/**
 * @author		Ava Skoog
 * @date		2017-10-22
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_TRANSFORM_H_
	#define KARHU_GRAPHICS_TRANSFORM_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/app/ecs/World.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
		namespace gfx
		{			
			class Transform : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
				public:
					void local(mth::Transformf const &);
					void local(mth::Transformf &&);
					const mth::Transformf &local() const noexcept { return m_local; }
				
					void global(mth::Transformf const &);
					void global(mth::Transformf &&);
					const mth::Transformf &global() const noexcept { return m_global; }
				
					mth::Mat4f const &matrixLocal() const noexcept { return m_matrixLocal; }
					mth::Mat4f const &matrixGlobal() const noexcept { return m_matrixGlobal; }
				
					void refresh(ecs::World &world);
				
					void serialise(conv::Serialiser &) const;
					void deserialise(conv::Serialiser const &);
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
					void editorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) override;
					bool editorEdit(app::App &, edt::Editor &) override;
				
					private:
						static struct Editor
						{
							mth::Transformf
								transform;
							
							mth::Vec4f
								prev;
							
							mth::Vec3f
								rotationEuler;
							
							edt::Transformation
								transformation;
							
							bool
								drag{false};
						} c_editor;
					#endif
				
				private:
					mth::Transformf m_local, m_global;
					mth::Mat4f m_matrixLocal, m_matrixGlobal;
			};
		}
	}
#endif
