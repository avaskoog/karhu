/**
 * @author		Ava Skoog
 * @date		2017-11-05
 * @copyright	2017-2023 Ava Skoog
 */

/// @todo: Add some way of animating/transitioning variables/blendtrees?

#include <karhu/app/gfx/components/Animator.hpp>
#include <karhu/app/gfx/systems/SystemAnimator.hpp>
#include <karhu/app/App.hpp>
#include <karhu/res/Bank.hpp>
#include <karhu/res/Animationcontroller.hpp>
#include <karhu/data/serialise.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
#include <karhu/res/Animationclips.hpp>
#include <karhu/res/Animation.hpp>
#include <karhu/res/Rig.hpp>
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/karhuimgui.hpp>
#include <karhu/app/edt/dropdown.inl>
#include <karhu/app/edt/util.hpp>

#include <karhu/tool/lib/IconFontCppHeaders/IconsForkAwesome.h>
#endif

#if KARHU_EDITOR_SERVER_SUPPORTED
namespace
{
	using namespace karhu;
	using namespace karhu::gfx;
	
	using Value = res::Animationcontroller::Value;
	using Clip  = res::Animationcontroller::Clip;
	using Index = res::Animationcontroller::Index;
	
	static ImVec4 const
		colTrue {0.5f, 1.0f,  0.5f,  1.0f},
		colFalse{0.5f, 0.25f, 0.25f, 1.0f};
	
	static std::string valueToString(Value const &value)
	{
		switch (value.type)
		{
			case Value::Type::boolean:
				return (value.boolean) ? ICON_FK_CHECK_SQUARE_O : ICON_FK_SQUARE_O;
			
			case Value::Type::integer:
				return std::to_string(value.integer);
				
			case Value::Type::floating:
				return std::to_string(value.floating);
		}
	}
	
	static ImVec4 valueToCol(Value const &value)
	{
		switch (value.type)
		{
			case Value::Type::boolean:
				return (value.boolean) ? colTrue : colFalse;
			
			case Value::Type::integer:
			case Value::Type::floating:
				return ImGui::GetStyle().Colors[ImGuiCol_Text];
		}
	}
	
	static void drawValVar
	(
		Value const &      val,
		bool  const        withName   = false,
		char  const *const tabs       = nullptr,
		char  const *const name       = nullptr,
		float              facCurrent = 1.0f
	)
	{
		char const *p0{""},*p1{""}, *p2{""}, *p3{""}, *p4{""};
		
		if (withName)
		{
			p0 = tabs;
			p1 = "[";
			p2 = name;
			p3 = ": ";
			p4 = "]";
		}
		
		switch (val.type)
		{
			case Value::Type::boolean:
			{
				if (val.boolean)
					ImGui::TextColored(colTrue,  "%s%s%s%s%s%s", p0, p1, p2, p3, ICON_FK_CHECK_SQUARE_O, p4);
				else
					ImGui::TextColored(colFalse, "%s%s%s%s%s%s", p0, p1, p2, p3, ICON_FK_SQUARE_O, p4);
				
				break;
			}
			
			case Value::Type::integer:
				ImGui::Text("%s%s%s%s%i%s", p0, p1, p2, p3, val.integer, p4);
				break;
			
			case Value::Type::floating:
			{
				ImVec4
					c{ImGui::GetStyle().Colors[ImGuiCol_Text]};
				
				c.w = 0.5f + 0.5f * facCurrent;
				
				ImGui::TextColored(c, "%s%s%s%s%f%s", p0, p1, p2, p3, val.floating, p4);
				break;
			}
		}
	}
	
	static void editValVar(std::string const &name, Value &val)
	{
		ImGui::PushID(name.c_str());
		
		switch (val.type)
		{
			case Value::Type::boolean:
			{
				ImGui::Checkbox("", &val.boolean);
				break;
			}
			
			case Value::Type::integer:
			{
				ImGui::InputInt("", &val.integer);
				break;
			}
			
			case Value::Type::floating:
			{
				ImGui::InputFloat("", &val.floating);
				
				if (val.floating < 0.0f)
					val.floating = 0.0f;
				else if (val.floating > 1.0f)
					val.floating = 1.0f;
				
				break;
			}
		}
		
		ImGui::PopID();
	}
	
	static void drawClip
	(
	 	std::vector<Value>              const &vars,
		res::Animationcontroller        const &controller,
		res::Animationcontroller::Index const  indexClip,
		int                             const  level      = 0,
		float                           const  facCurrent = 1.0f
	)
	{
		Clip const
			&clip{controller.clips[static_cast<std::size_t>(indexClip)]};
		
		std::string
			tabs;

		for (int i = 0; i < level; ++ i)
			tabs += '\t';
		
		if (Clip::Type::single == clip.type)
		{
			ImVec4
				c{ImGui::GetStyle().Colors[ImGuiCol_Text]};
			
			c.w = 0.5f + 0.5f * facCurrent;
			
			ImGui::TextColored
			(
				c,
				"%s[%i] %s",
				tabs.c_str(),
				static_cast<int>(indexClip),
				controller.names[static_cast<std::size_t>(clip.names[0])].c_str()
			);
		}
		else if (Clip::Type::blendtree == clip.type)
		{
			std::string const
				&nameVar{controller.vars[static_cast<std::size_t>(clip.var)].name};
			
			mth::Scalf
				valVar;
			
			if (Value::Type::floating != vars[static_cast<std::size_t>(clip.var)].type)
			{
				ImGui::TextColored(colFalse, "[NOT FLOAT: %s]", nameVar.c_str());
				valVar = 0.0f;
			}
			else
			{
				std::string const
					name{std::to_string(indexClip) + "] [" + nameVar};
				
				drawValVar
				(
					vars[static_cast<std::size_t>(clip.var)],
					true,
					tabs.c_str(),
					name.c_str(),
					facCurrent
				);
				
				valVar = vars[static_cast<std::size_t>(clip.var)].floating;
			}
			
			// Copied from the actual animator system;
			// would be nice to have these values gettable but.
		
			float const
				countf        {static_cast<mth::Scalf>(clip.names.size() - 1)},
				value         {mth::clamp(valVar, 0.0f, 1.0f)},
				progressScaled{value * countf};
		
			std::size_t const
				indexA{static_cast<std::size_t>
				(
					mth::min
					(
						countf - 1.0f,
						mth::floor(progressScaled)
					)
				)},
				indexB{indexA + 1};
		
			float const
				progress{mth::floor(progressScaled - static_cast<float>(indexA))},
				percentA{1.0f - progress},
				percentB{progress};
		
			for (std::size_t i{0}; i < clip.names.size(); ++ i)
			{
				Index const
					index{clip.names[i]};
				
				if (index == indexClip)
				{
					ImGui::TextColored
					(
						colFalse,
						"%sRECURSION: %i",
						tabs.c_str(),
						(int)index
					);
					continue;
				}
				
				float
					facChildCurrent{0.0f};
				
				if (i == indexA)
					facChildCurrent = percentA;
				else if (i == indexB)
					facChildCurrent = percentB;
				
				drawClip(vars, controller, index, level + 1, facChildCurrent * facCurrent);
			}
		}
	}
}
#endif

namespace karhu
{
	namespace gfx
	{
		void Animator::mode(ModeAnimator const mode)
		{
			if (mode != m_mode)
			{
				m_mode     = mode;
				m_modified = true;
			}
		}
		
		void Animator::loop(bool const set)
		{
			m_loop = set;
		}
		
		void Animator::autoplay(bool const set)
		{
			m_autoplay = set;
		}
		
		void Animator::clip(res::IDResource const ID)
		{
			if (ModeAnimator::clip == m_mode && ID != m_clip)
				m_modified = true;
			
			m_clip = ID;
		}
		
		void Animator::clips(res::IDResource const ID)
		{
			if (ModeAnimator::controller == m_mode && ID != m_clips)
				m_modified = true;
			
			m_clips = ID;
		}
		
		void Animator::controller(res::IDResource const ID)
		{
			if (ModeAnimator::controller == m_mode && ID != m_controller)
				m_modified = true;
			
			m_controller = ID;
		}
		
		void Animator::rig(const res::IDResource ID)
		{
			m_rig = ID;
		}
		
		void Animator::speed(float const value)
		{
			m_speed = value;
		}
		
		/**
		 * Starts playing the animator from its current time.
		 */
		void Animator::start()
		{
			m_playing = true;
		}
		
		/**
		 * Starts playing the animator from the beginning.
		 */
		void Animator::restart()
		{
			reset();
			m_playing = true;
		}
		
		/**
		 * Pauses the animator at its current time.
		 */
		void Animator::pause()
		{
			m_playing = false;
		}
		
		/**
		 * Resets the animator without changing its playback state.
		 */
		void Animator::reset()
		{
			/// @todo: Remove app singleton and inject handle into animator system.
			m_modified = true;
			SystemAnimator::refreshAnimator(*app::App::instance(), *this);
		}
		
		/**
		 * Pauses the animator and resets its time to the beginning.
		 */
		void Animator::stop()
		{
			reset();
			m_playing = false;
		}
		
		void Animator::tick(float const dt)
		{
			m_tick += dt * m_speed;
		}
		
		void Animator::variable(std::string const &name, std::string const &constant)
		{
			auto const
				it(m_controllerstate.indicesVars.find(name));
			
			if
			(
				it == m_controllerstate.indicesVars.end() ||
				it->second <= static_cast<Index>(m_controllerstate.vars.size())
			)
				return;
			
			auto
				&v(m_controllerstate.vars[static_cast<std::size_t>(it->second)]);
			
			auto const
				c(this->constant(constant));
			
			if (!c)
				return;
			
			if (v.type != c->type)
				return;
			
			switch (v.type)
			{
				case Value::Type::boolean:
					v.boolean = c->boolean;
					break;
					
				case Value::Type::integer:
					v.integer = c->integer;
					break;
					
				case Value::Type::floating:
					v.floating = c->floating;
					break;
			}
		}
		
		/**
		 * If the animator is in animation controller mode,
		 * sets the value of the boolean by the specified name.
		 *
		 * @param name  The name of the value to set in the animation controller.
		 * @param value The value to set.
		 */
		void Animator::boolean(std::string const &name, bool const value)
		{
			boolean(name.c_str(), value);
		}
		
		/**
		 * If the animator is in animation controller mode,
		 * sets the value of the boolean by the specified name.
		 *
		 * @param name  The name of the value to set in the animation controller.
		 * @param value The value to set.
		 */
		void Animator::boolean(char const *const name, bool const value)
		{
			if (ModeAnimator::controller != m_mode)
				return;
			
			auto const it(m_controllerstate.indicesVars.find(name));
			
			if (it == m_controllerstate.indicesVars.end())
				return;
			
			if (static_cast<std::size_t>(it->second) >= m_controllerstate.vars.size())
				return;
			
			auto &v(m_controllerstate.vars[static_cast<std::size_t>(it->second)]);
			
			if (Value::Type::boolean != v.type)
				return;
			
			v.boolean = value;
		}
		
		void Animator::boolean(std::string const &name, std::string const &constant)
		{
			auto
				c(this->constant(constant));
			
			if (!c)
				return;
			
			if (res::Animationcontroller::Value::Type::boolean != c->type)
				return;
			
			boolean(name, c->boolean);
		}
		
		/**
		 * Returns the value of the boolean by the specified name
		 * in the animation controller.
		 *
		 * @param name The name of the value to get in the animation controller.
		 *
		 * @return The value.
		 */
		bool Animator::boolean(std::string const &name) const
		{
			return boolean(name.c_str());
		}
		
		/**
		 * Returns the value of the boolean by the specified name
		 * in the animation controller.
		 *
		 * @param name The name of the value to get in the animation controller.
		 *
		 * @return The value.
		 */
		bool Animator::boolean(char const *const name) const
		{
			if (ModeAnimator::controller != m_mode)
				return false;
			
			auto const it(m_controllerstate.indicesVars.find(name));
			
			if (it == m_controllerstate.indicesVars.end())
				return false;
			
			if (static_cast<std::size_t>(it->second) >= m_controllerstate.vars.size())
				return false;
			
			auto const &v(m_controllerstate.vars[static_cast<std::size_t>(it->second)]);
			
			if (Value::Type::boolean != v.type)
				return false;
			
			return v.boolean;
		}
		
		/**
		 * If the animator is in animation controller mode,
		 * sets the value of the integer by the specified name.
		 *
		 * @param name  The name of the value to set in the animation controller.
		 * @param value The value to set.
		 */
		void Animator::integer(std::string const &name, std::int32_t const value)
		{
			integer(name.c_str(), value);
		}
		
		/**
		 * If the animator is in animation controller mode,
		 * sets the value of the integer by the specified name.
		 *
		 * @param name  The name of the value to set in the animation controller.
		 * @param value The value to set.
		 */
		void Animator::integer(char const *const name, std::int32_t const value)
		{
			if (ModeAnimator::controller != m_mode)
				return;
			
			auto const it(m_controllerstate.indicesVars.find(name));
			
			if (it == m_controllerstate.indicesVars.end())
				return;
			
			if (static_cast<std::size_t>(it->second) >= m_controllerstate.vars.size())
				return;
			
			auto &v(m_controllerstate.vars[static_cast<std::size_t>(it->second)]);
			
			if (Value::Type::integer != v.type)
				return;
			
			v.integer = value;
		}
		
		void Animator::integer(std::string const &name, std::string const &constant)
		{
			auto
				c(this->constant(constant));
			
			if (!c)
				return;
			
			if (res::Animationcontroller::Value::Type::integer != c->type)
				return;
			
			integer(name, c->integer);
		}
		
		/**
		 * Returns the value of the integer by the specified name
		 * in the animation controller.
		 *
		 * @param name The name of the value to get in the animation controller.
		 *
		 * @return The value.
		 */
		std::int32_t Animator::integer(std::string const &name) const
		{
			return integer(name.c_str());
		}
		
		/**
		 * Returns the value of the integer by the specified name
		 * in the animation controller.
		 *
		 * @param name The name of the value to get in the animation controller.
		 *
		 * @return The value.
		 */
		std::int32_t Animator::integer(char const *const name) const
		{
			if (ModeAnimator::controller != m_mode)
				return 0;
			
			auto const it(m_controllerstate.indicesVars.find(name));
			
			if (it == m_controllerstate.indicesVars.end())
				return 0;
			
			if (static_cast<std::size_t>(it->second) >= m_controllerstate.vars.size())
				return 0;
			
			auto const &v(m_controllerstate.vars[static_cast<std::size_t>(it->second)]);
			
			if (Value::Type::integer != v.type)
				return 0;
			
			return v.integer;
		}
		
		/**
		 * If the animator is in animation controller mode,
		 * sets the value of the float by the specified name.
		 *
		 * @param name  The name of the value to set in the animation controller.
		 * @param value The value to set.
		 */
		void Animator::floating(std::string const &name, float const value)
		{
			floating(name.c_str(), value);
		}
		
		/**
		 * If the animator is in animation controller mode,
		 * sets the value of the float by the specified name.
		 *
		 * @param name  The name of the value to set in the animation controller.
		 * @param value The value to set.
		 */
		void Animator::floating(char const *const name, float const value)
		{
			if (ModeAnimator::controller != m_mode)
				return;
			
			auto const it(m_controllerstate.indicesVars.find(name));
			
			if (it == m_controllerstate.indicesVars.end())
				return;
			
			if (static_cast<std::size_t>(it->second) >= m_controllerstate.vars.size())
				return;
			
			auto &v(m_controllerstate.vars[static_cast<std::size_t>(it->second)]);
			
			if (Value::Type::floating != v.type)
				return;
			
			v.floating = value;
		}
		
		void Animator::floating(std::string const &name, std::string const &constant)
		{
			auto
				c(this->constant(constant));
			
			if (!c)
				return;
			
			if (res::Animationcontroller::Value::Type::floating != c->type)
				return;
			
			floating(name, c->floating);
		}
		
		/**
		 * Returns the value of the boolean by the specified name
		 * in the animation controller.
		 *
		 * @param name The name of the value to get in the animation controller.
		 *
		 * @return The value.
		 */
		float Animator::floating(std::string const &name) const
		{
			return floating(name.c_str());
		}
		
		/**
		 * Returns the value of the boolean by the specified name
		 * in the animation controller.
		 *
		 * @param name The name of the value to get in the animation controller.
		 *
		 * @return The value.
		 */
		float Animator::floating(char const *const name) const
		{
			if (ModeAnimator::controller != m_mode)
				return 0.0f;
			
			auto const it(m_controllerstate.indicesVars.find(name));
			
			if (it == m_controllerstate.indicesVars.end())
				return 0.0f;
			
			if (it->second >= static_cast<Index>(m_controllerstate.vars.size()))
				return 0.0f;
			
			auto const &v(m_controllerstate.vars[static_cast<std::size_t>(it->second)]);
			
			if (Value::Type::floating != v.type)
				return 0.0f;
			
			return v.floating;
		}
		
		/**
		 * If the animator is in animation controller mode,
		 * activates the trigger by the specified name.
		 *
		 * @param name  The name of the trigger to activate in the animation controller.
		 */
		void Animator::trigger(std::string const &name)
		{
			trigger(name.c_str());
		}
		
		/**
		 * If the animator is in animation controller mode,
		 * activates the trigger by the specified name.
		 *
		 * @param name  The name of the trigger to activate in the animation controller.
		 */
		void Animator::trigger(char const *const name)
		{
			if (ModeAnimator::controller != m_mode)
				return;
			
			auto const it(m_controllerstate.indicesTriggers.find(name));
			
			if (it == m_controllerstate.indicesTriggers.end())
				return;
			
			if (static_cast<std::size_t>(it->second) >= m_controllerstate.triggers.size())
				return;
			
			m_controllerstate.triggers[static_cast<std::size_t>(it->second)] = true;
		}
		
		res::Animationcontroller::ValueWithName const *Animator::constant(std::string const &name) const
		{
			if (!m_controllerstate.controller)
				return nullptr;
			
			auto const
			it
			(
				std::find_if
				(
					m_controllerstate.controller->consts.begin(),
					m_controllerstate.controller->consts.end(),
					[&name](auto const &v) { return (v.name == name); }
				)
			);
		
			if (it == m_controllerstate.controller->consts.end())
				return nullptr;
			
			return &*it;
		}
		
		void Animator::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("mode",       m_mode)
				<< karhuINn("speed",      m_speed)
				<< karhuINn("autoplay",   m_autoplay)
				<< karhuINn("loop",       m_loop)
				<< karhuINn("controller", m_controller)
				<< karhuINn("clips",      m_clips)
				<< karhuINn("clip",       m_clip)
				<< karhuINn("rig",        m_rig);
		}

		void Animator::deserialise(conv::Serialiser const &ser)
		{
			ser
				>> karhuOUTn("mode",       m_mode)
				>> karhuOUTn("speed",      m_speed)
				>> karhuOUTn("autoplay",   m_autoplay)
				>> karhuOUTn("loop",       m_loop)
				>> karhuOUTn("controller", m_controller)
				>> karhuOUTn("clips",      m_clips)
				>> karhuOUTn("clip",       m_clip)
				>> karhuOUTn("rig",        m_rig);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
//		static void printNodeRig(int n, std::stringstream &s, res::Rig::Child const &root)
//		{
//			if (0 == n)
//				s << "[ROOT] ";
//
//			for (int i = 0; i < n; ++ i)
//				s << '>';
//
//			s << root.name << '\n';
//
//			for (const auto &child : root.children)
//				if (child)
//					printNodeRig(n + 1, s, *child);
//		}
		
		/// @todo: Animator editor playback.
		bool Animator::editorEdit(app::App &app, edt::Editor &editor)
		{
			static bool
				didInitVars  {false},
				didInitConsts{false},
				didInitTrigs {false},
				didInitNames {false};
			
			bool r{false};
			
			editor.header("General properties");
			
			// Mode.
			
			constexpr std::pair<char const *, ModeAnimator>
				modes[]
				{
					{"Clip",       ModeAnimator::clip},
					{"Controller", ModeAnimator::controller}
				};
			
			if (edt::dropdown("Mode", m_mode, std::begin(modes), std::end(modes)))
				r = true;
			
			// Loop.
			
			if (ImGui::Checkbox("Loop", &m_loop))
				r = true;
			
			// Autoplay.
			
			if (ImGui::Checkbox("Autoplay", &m_autoplay))
				r = true;
			
			// Speed.
			
			ImGui::DragFloat("Speed", &m_speed, 0.01f);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			// Rig.
			
			if (editor.pickerResource("anim-rig", "Rig", m_rig, app.res().IDOfType<res::Rig>()))
				r = true;
			
			switch (m_mode)
			{
				case ModeAnimator::clip:
				{
					editor.header("Clip properties");
					
					// Clip.
					
					if (editor.pickerResource("anim-clip", "Clip", m_clip, app.res().IDOfType<res::Animation>()))
						r = true;
					
					break;
				}
				
				case ModeAnimator::controller:
				{
					editor.header("Controller properties");
					
					// Controller.
					
					if (editor.pickerResource("anim-ctrl", "Controller", m_controller, app.res().IDOfType<res::Animationcontroller>()))
						r = true;
					
					// Clips.
					
					if (editor.pickerResource("anim-clips", "Clips", m_clips, app.res().IDOfType<res::Animationclips>()))
						r = true;
					
					break;
				}
			}
			
			if (r)
				m_modified = true;
			
			editor.header("Playback");
			
			bool const
				isValid{valid()};
			
			if (!isValid)
				editor.pushDisabled();
			else if (playing())
				app.backend().shouldRepaintViewportEditor(true);
			
			if (editor.button("anim-play", ((playing() ? ICON_FK_STOP : ICON_FK_PLAY))))
			{
				if (!playing())
					restart();
				else
					stop();
			}
			
			if (!isValid)
				editor.popDisabled();
			
			if (ModeAnimator::controller == m_mode && isValid)
			{
				auto
					clips(app.res().get<res::Animationclips>(m_clips));
				
				auto
					controller(app.res().get<res::Animationcontroller>(m_controller));
				
				// Should not be valid if not the case but no need to crash the editor
				// unnecessarily when we can just not draw this stuff in that case.
				if (clips && controller)
				{
					float
						widthGroup;
					
					editor.header("Ctrl. clip/blend");
					
					edt::beginGroupPanel();
					{
						if (!m_controllerstate.blending)
						{
							Clipstate const
								&state{m_controllerstate.clipstates[static_cast<std::size_t>(m_controllerstate.indexClipCurr)]};
							
							ImGui::Text("%f", state.time);
							
							drawClip(m_controllerstate.vars, *controller, m_controllerstate.indexClipCurr);
						}
						else
						{
							ImGui::Text("%f", m_controllerstate.blend.time);
							
							ImGui::Columns(2);
							drawClip(m_controllerstate.vars, *controller, m_controllerstate.blend.src);
							ImGui::NextColumn();
							drawClip(m_controllerstate.vars, *controller, m_controllerstate.blend.dst);
							ImGui::EndColumns();
						}
					}
					edt::endGroupPanel();
					
					editor.header("Ctrl. variables");
					
					edt::beginGroupPanel();
					{
						ImGui::Columns(2);
						
						if (didInitVars)
						{
							ImGui::SetColumnWidth(0, 80.0f);
							didInitVars = true;
						}
						
						for (ValueWithName const &val : controller->vars)
						{
							ImGui::AlignTextToFramePadding();
							editor.text(val.name);
						}
						
						ImGui::NextColumn();
						
						for (std::size_t i{0}; i < m_controllerstate.vars.size(); ++ i)
						{
							ImGui::AlignTextToFramePadding();
							editValVar(controller->vars[i].name, m_controllerstate.vars[i]);
						}
						
						ImGui::EndColumns();
					}
					edt::endGroupPanel();

					editor.header("Ctrl. triggers");
					
					edt::beginGroupPanel();
					{
						ImGui::Columns(2);
						
						if (didInitTrigs)
						{
							ImGui::SetColumnWidth(0, 80.0f);
							didInitTrigs = true;
						}
						
						for (std::size_t i{0}; i < m_controllerstate.triggers.size(); ++ i)
						{
							ImGui::AlignTextToFramePadding();
							editor.text(controller->triggers[i].name);
						}
						
						ImGui::NextColumn();
						
						for (std::size_t i{0}; i < m_controllerstate.triggers.size(); ++ i)
						{
							ImGui::AlignTextToFramePadding();
							ImGui::PushID(controller->triggers[i].name.c_str());
							ImGui::Checkbox("", &m_controllerstate.triggers[i]);
							ImGui::PopID();
						}
						
						ImGui::EndColumns();
					}
					edt::endGroupPanel();
					
					editor.header("Ctrl. constants");
					
					edt::beginGroupPanel();
					{
						ImGui::Columns(2);
						
						if (didInitConsts)
						{
							ImGui::SetColumnWidth(0, 80.0f);
							didInitConsts = true;
						}
						
						for (ValueWithName const &val : controller->consts)
						{
							ImGui::AlignTextToFramePadding();
							editor.text(val.name);
						}
						
						ImGui::NextColumn();
						
						for (ValueWithName const &val : controller->consts)
						{
							ImGui::AlignTextToFramePadding();
							drawValVar(val);
						}
						
						ImGui::EndColumns();
					}
					edt::endGroupPanel();
					
					editor.header("Ctrl. transitions");
					
					widthGroup = edt::beginGroupPanel();
					{
						for (std::size_t i{0}; i < controller->transitions.size(); ++ i)
						{
							Transition const
								&transition{controller->transitions[i]};
							
							Clip const
								*clipSrc,
								*clipDst;
							
							std::string
								nameSrc,
								nameDst,
								nameVar,
								nameVarComp,
								nameValComp;
							
							Index const
								sizeClips   {static_cast<Index>(controller->clips.size())},
								sizeVars    {static_cast<Index>(controller->vars.size())},
								sizeConsts  {static_cast<Index>(controller->consts.size())},
								sizeTriggers{static_cast<Index>(controller->triggers.size())};
							
							bool
								isInvalidClip   {false},
								isInvalidVar    {false},
								isInvalidVarComp{false};
							
							if (transition.src >= 0 && transition.src < sizeClips)
							{
								clipSrc = &controller->clips[static_cast<std::size_t>(transition.src)];
								nameSrc = std::to_string(transition.src);
							}
							else
							{
								clipSrc = nullptr;
								
								if (transition.src < 0)
									nameSrc = "[any]";
								else
								{
									nameSrc = "[INVALID " + std::to_string(transition.src) + ']';
									isInvalidClip = true;
								}
							}
							
							if (transition.dst >= 0 && transition.dst < sizeClips)
							{
								clipDst = &controller->clips[static_cast<std::size_t>(transition.dst)];
								nameDst = std::to_string(transition.dst);
							}
							else
							{
								clipDst = nullptr;
								nameSrc = "[INVALID " + std::to_string(transition.dst) + ']';
								isInvalidClip = true;
							}
							
							ImGui::TextColored
							(
								((isInvalidClip) ? colFalse : ImGui::GetStyle().Colors[ImGuiCol_Text]),
								"%s " ICON_FK_LONG_ARROW_RIGHT " %s",
								nameSrc.c_str(),
								nameDst.c_str()
							);
							
							char const
								*const opVar{[](Operation const op)
								{
									switch (op)
									{
										case Operation::equal:
											return " == ";
										case Operation::unequal:
											return " != ";
										case Operation::less:
											return " < ";
										case Operation::lessOrEqual:
											return " <= ";
										case Operation::greater:
											return " > ";
										case Operation::greaterOrEqual:
											return " >= ";
										case Operation::trigger:
											return " (trigger)";
										case Operation::always:
											return " (always)";
									}
								}(transition.op)};
							
							if (Operation::trigger == transition.op)
							{
								if (transition.var >= 0 && transition.var < sizeTriggers)
									nameVar = controller->triggers[static_cast<std::size_t>(transition.var)].name;
								else
								{
									nameVar = "[INVALID]";
									isInvalidVar = true;
								}
							}
							else
							{
								if (transition.var >= 0 && transition.var < sizeVars)
									nameVar = controller->vars[static_cast<std::size_t>(transition.var)].name;
								else
								{
									nameVar = "[INVALID]";
									isInvalidVar = true;
								}
								
								switch (transition.comp.type)
								{
									case Comparand::Type::value:
										nameValComp = valueToString(transition.comp.value);
										break;
									
									case Comparand::Type::index:
									{
										if
										(
											transition.comp.index >= 0 &&
											transition.comp.index < sizeConsts
										)
										{
											auto const
												index(static_cast<std::size_t>(transition.comp.index));
											
											nameVarComp = controller->consts[index].name;
											nameValComp = valueToString(controller->consts[index]);
										}
										else
										{
											nameVarComp = "[INVALID " + std::to_string(transition.comp.index) + ']';
											isInvalidVarComp = true;
										}
										
										break;
									}
								}
							}
							
							ImGui::TextColored
							(
								((isInvalidVar || isInvalidVarComp) ? colFalse : ImGui::GetStyle().Colors[ImGuiCol_Text]),
								"%s%s%s",
								nameVar.c_str(),
								opVar,
								nameValComp.c_str()
							);

							if ((i + 1) < controller->transitions.size())
								edt::lineGroupPanel(widthGroup);
						}
					}
					edt::endGroupPanel();
					
					editor.header("Ctrl. clips");
					
					widthGroup = edt::beginGroupPanel();
					{
						for (std::size_t i{0}; i < controller->clips.size(); ++ i)
						{
							drawClip(m_controllerstate.vars, *controller, static_cast<Index>(i));

							if ((i + 1) < controller->clips.size())
								edt::lineGroupPanel(widthGroup);
						}
					}
					edt::endGroupPanel();
					
					editor.header("Ctrl. clipnames");
					
					edt::beginGroupPanel();
					{
						ImGui::Columns(2);
						
						if (!didInitNames)
						{
							ImGui::SetColumnWidth(0, 80.0f);
							didInitNames = true;
						}
						
						for (std::string const &name : controller->names)
						{
							ImGui::AlignTextToFramePadding();
							editor.text(name);
						}
						
						ImGui::NextColumn();
						
						for (res::IDResource const IDClip : clips->clips)
							editor.nameResource(IDClip, app.res().IDOfType<res::Animationclips>());
						
						ImGui::EndColumns();
					}
					edt::endGroupPanel();
				}
			}

			return r;
		}
		
		/// @todo: Proper Animator editor eventually.
		/*
		bool Animator::editorEdit(app::App &app, edt::Editor &editor)
		{
			if (m_playing)
			{
				if (editor.button("anim-toggle", "Pause"))
					m_playing = false;
			}
			else
			{
				if (editor.button("anim-toggle", "Play"))
					m_playing = true;
			}
			
			editor.input("anim-tick", "Time", m_tick);
			editor.input("anim-speed", "Speed", m_speed);
			
			editor.line();
			
			if (m_valid)
				editor.text("VALID");
			else
				editor.text("INVALID");
			
			editor.line();
			
			std::stringstream s;
			
			s << "matrices: " << m_transformsPerJointByID.size();
			editor.text(s.str());
			
			editor.line();
			
			editor.input("anim-rig", "ID RIG", *reinterpret_cast<int *>(&m_rig));
			
			if (0 != m_rig)
			{
				auto const *const rig(app.res().get<res::Rig>(m_rig));
				
				if (rig)
				{
					s.str("");
					s << "joints: " << rig->countJoints;
					editor.text(s.str());
					s.str("");
					
					for (auto const &root : rig->roots)
						if (root)
							printNodeRig(0, s, *root);
					
					if (0 != s.str().length())
						editor.text(s.str());
				}
			}
		
			editor.line();
			
			editor.input("anim-clips", "ID CLIPS", *reinterpret_cast<int *>(&m_clips));
			
			if (0 != m_clips)
			{
				auto const *const clips(app.res().get<res::Animationclips>(m_clips));
				
				if (clips)
				{
					if (editor.button("anim-dump", "DEBUG DUMP CLIPS"))
					{
						for (auto const &ID : clips->clips)
						{
							auto const *const clip(app.res().get<res::Animation>(ID));
							
							printf("----\nANIM %i (%i channels):\n", ((int)ID), clip->channelsPerJoint.size());
							
							for (auto const &joint : clip->channelsPerJoint)
							{
								printf("\tJOINT %s (%i channels):\n", joint.first.c_str(), (int)joint.second.size());
								
								int i = 0;
								for (auto const &channel : joint.second)
								{
									std::string p;
									
									switch (channel.property)
									{
										case res::Animation::Channel::Property::position:
											p = "POS";
											break;
											
										case res::Animation::Channel::Property::rotation:
											p = "ROT";
											break;
											
										case res::Animation::Channel::Property::scale:
											p = "SCL";
											break;
									}
									
									printf
									(
										"\t\tCHAN #%i (%s, %i kfs):\n",
										(i ++),
										p.c_str(),
										(int)channel.keyframes.size()
									);
									
									int j = 0;
									std::stringstream ss;
									for (auto const &kf : channel.keyframes)
									{
										ss.str("");
										
										switch (channel.property)
										{
											case res::Animation::Channel::Property::position:
												ss << "x "  << kf.position.x;
												ss << " y " << kf.position.y;
												ss << " z " << kf.position.z;
												break;
												
											case res::Animation::Channel::Property::rotation:
												ss << "x "  << kf.rotation.x;
												ss << " y " << kf.rotation.y;
												ss << " z " << kf.rotation.z;
												ss << " w " << kf.rotation.w;
												break;
												
											case res::Animation::Channel::Property::scale:
												ss << "x "  << kf.scale.x;
												ss << " y " << kf.scale.y;
												ss << " z " << kf.scale.z;
												break;
										}
										
										printf("\t\t\tKF #%i (t %f, %s)\n", (j ++), kf.time, ss.str().c_str());
									}
								}
							}
						}
					}
					
					s.str("");
					
					s << clips->clips.size() << " clips";
					editor.text(s.str());
					
					s.str("");
					
					int i{0};
					for (auto const &ID : clips->clips)
					{
						s.str("");
						s << i << " (res " << ID << "): ";
						
						auto const *const clip(app.res().get<res::Animation>(ID));
						
						if (clip)
							s << "v " << ((clip->valid()) ? 'y' : 'n') << ", dur " << clip->duration;
						
						editor.text(s.str());
						
						++ i;
					}
				}
			}
			
			return true;
		}*/
		#endif
	}
}
