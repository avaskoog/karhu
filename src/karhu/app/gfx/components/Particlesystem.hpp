/**
 * @author		Ava Skoog
 * @date		2019-09-12
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_PARTICLESYSTEM_H_
	#define KARHU_GRAPHICS_PARTICLESYSTEM_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/conv/graphics.hpp>
	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
		namespace gfx
		{
			enum class SpaceParticlesystem
			{
				relative,
				absolute
			};
			
			struct Particle
			{
				mth::Transformf transform;
			};
			
			/// @todo: Require renderdata 2D OR 3D
			class Particlesystem : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
				public:
					void space(const SpaceParticlesystem space) { m_space = space; }
					SpaceParticlesystem space() const noexcept { return m_space; }
				
					void startcount(const std::uint32_t count) { m_startcount = count; }
					std::uint32_t startcount() const noexcept { return m_startcount; }
				
				public:
					void reserve(const std::uint32_t count) { m_instances.resize(count); }
					std::uint32_t count() const noexcept { return static_cast<std::uint32_t>(m_instances.size()); }
					Particle &instance(const std::uint32_t index) { return m_instances[index]; }
					const Particle &instance(const std::uint32_t index) const { return m_instances[index]; }
					const std::vector<Particle> &instances() const noexcept { return m_instances; }
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);

					#if KARHU_EDITOR_SERVER_SUPPORTED
                    void editorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) override {}
                    bool editorEdit(app::App &, edt::Editor &) override;
                    #endif
				
				private:
					SpaceParticlesystem m_space{SpaceParticlesystem::relative};
					std::uint32_t m_startcount{0};
				
				private:
					std::vector<Particle> m_instances;
			};
		}
	}
#endif
