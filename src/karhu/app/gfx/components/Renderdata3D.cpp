/**
 * @author		Ava Skoog
 * @date		2018-10-20
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/components/Renderdata3D.hpp>
#include <karhu/app/App.hpp>
#include <karhu/res/Bank.hpp>
#include <karhu/res/Mesh.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
#include <karhu/app/edt/Editor.hpp>
#endif

namespace
{
	using namespace karhu;
	using namespace karhu::gfx;
	
	static void recalculateAABB(mth::Bounds3f &inAABB, res::IDResource const mesh)
	{
		inAABB = {{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}};
	
		//! @todo: Won't need the app singleton when mesh is a Ref
		auto
			m(app::App::instance()->res().get<res::Mesh>(mesh));
	
		if (!m)
			return;
	
		if
		(
			!m                                         ||
			!m->valid()                                ||
			0 == m->countLevelsLOD()                   ||
			0 == m->geometryAtLOD(0)->positions.size()
		)
			return;
	
		for (std::size_t i{0}; i < m->geometryAtLOD(0)->positions.size(); ++ i)
		{
			const auto
				&p{m->geometryAtLOD(0)->positions[i]};
			
			if (p.x < inAABB.min.x) inAABB.min.x = p.x;
			if (p.y < inAABB.min.y) inAABB.min.y = p.y;
			if (p.z < inAABB.min.z) inAABB.min.z = p.z;
			
			if (p.x > inAABB.max.x) inAABB.max.x = p.x;
			if (p.y > inAABB.max.y) inAABB.max.y = p.y;
			if (p.z > inAABB.max.z) inAABB.max.z = p.z;
		}
	
		std::array<mth::Vec4f, 8> corners
		{{
			{inAABB.min.x, inAABB.min.y, inAABB.min.z, 1.0f},
			{inAABB.max.x, inAABB.min.y, inAABB.min.z, 1.0f},
			{inAABB.min.x, inAABB.max.y, inAABB.min.z, 1.0f},
			{inAABB.max.x, inAABB.max.y, inAABB.min.z, 1.0f},
			{inAABB.min.x, inAABB.min.y, inAABB.max.z, 1.0f},
			{inAABB.max.x, inAABB.min.y, inAABB.max.z, 1.0f},
			{inAABB.min.x, inAABB.max.y, inAABB.max.z, 1.0f},
			{inAABB.max.x, inAABB.max.y, inAABB.max.z, 1.0f}
		}};
		
		mth::Radsf const
			rads{mth::radians(45.0f)};
		
		mth::Quatf const
			rot{mth::euler(rads, rads, rads)};
		
		mth::Scalf
			max{0.0f};
	
		for (std::size_t i{0}; i < corners.size(); ++ i)
		{
			corners[i] = rot * corners[i];
			
			mth::Scalf const
				x{mth::abs(corners[i].x)},
				y{mth::abs(corners[i].y)},
				z{mth::abs(corners[i].z)};
			
			if (x > max) max = x;
			if (y > max) max = y;
			if (z > max) max = z;
			
			if (corners[i].x < inAABB.min.x) inAABB.min.x = corners[i].x;
			if (corners[i].y < inAABB.min.y) inAABB.min.y = corners[i].y;
			if (corners[i].z < inAABB.min.z) inAABB.min.z = corners[i].z;
			
			if (corners[i].x > inAABB.max.x) inAABB.max.x = corners[i].x;
			if (corners[i].y > inAABB.max.y) inAABB.max.y = corners[i].y;
			if (corners[i].z > inAABB.max.z) inAABB.max.z = corners[i].z;
		}
		
		// This way we have room for animations and rotations
		// without having to recalculate every frame.
		inAABB.min.x =
		inAABB.min.y =
		inAABB.min.z = -max;
		inAABB.max.x =
		inAABB.max.y =
		inAABB.max.z =  max;
	}
}

namespace karhu
{
	namespace gfx
	{
		void Renderdata3D::mesh(res::IDResource const ID)
		{
			m_mesh = ID;
			recalculateAABB(m_AABB, ID);
		}
		
		void Renderdata3D::material(res::IDResource const ID)
		{
			m_material = ID;
		}
		
		void Renderdata3D::performSerialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("mesh",     m_mesh)
				<< karhuINn("material", m_material);
		}

		void Renderdata3D::performDeserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUTn("mesh",     m_mesh)
				>> karhuOUTn("material", m_material);
			
			recalculateAABB(m_AABB, m_mesh);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Renderdata3D::performEditorEdit(app::App &app, edt::Editor &editor)
		{
			bool r{false};
			
			editor.header("3D properties");
			
			if (editor.pickerResource("rd3d-mesh", "Mesh", m_mesh, app.res().IDOfType<res::Mesh>()))
				r = true;
			
			if (editor.pickerResource("rd3d-mat", "Material", m_material, app.res().IDOfType<res::Material>()))
				r = true;
			
			auto m(app.res().get<res::Mesh>(m_mesh));
			auto g((m) ? m->geometryAtLOD(0) : nullptr);
			
			std::stringstream s;
			s
				<< ((g) ? g->positions.size() : 0)
				<< " vertices, "
				<< ((g) ? g->indices.size() : 0)
				<< " indices";
			
			editor.pushDisabled();
			editor.text(s.str().c_str());
			editor.popDisabled();
			
			return r;
		}
		#endif
	}
}
