/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/components/Renderdata2D.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
#include <karhu/app/App.hpp>
#include <karhu/res/Bank.hpp>
#include <karhu/res/Texture.hpp>
#endif

namespace karhu
{
	namespace gfx
	{
		void Renderdata2D::texture(res::IDResource const ID)
		{
			m_texture = ID;
		}
		
		void Renderdata2D::tint(ColRGBAf const &v)
		{
			m_tint = v;
		}
		
		void Renderdata2D::billboard(bool const set)
		{
			m_billboard = set;
		}
		
		void Renderdata2D::size(mth::Vec2f const v)
		{
			m_size = v;
		}
		
		void Renderdata2D::UVBottomLeft(mth::Vec2f const v)
		{
			m_UVBottomLeft = v;
		}
		
		void Renderdata2D::UVTopRight(mth::Vec2f const v)
		{
			m_UVTopRight = v;
		}
		
		void Renderdata2D::performSerialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("texture",      m_texture)
				<< karhuINn("tint",         m_tint)
				<< karhuINn("billboard",    m_billboard)
				<< karhuINn("size",         m_size)
				<< karhuINn("UVBottomLeft", m_UVBottomLeft)
				<< karhuINn("UVTopRight",   m_UVTopRight);
		}

		void Renderdata2D::performDeserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUTn("texture",      m_texture)
				>> karhuOUTn("tint",         m_tint)
				>> karhuOUTn("billboard",    m_billboard)
				>> karhuOUTn("size",         m_size)
				>> karhuOUTn("UVBottomLeft", m_UVBottomLeft)
				>> karhuOUTn("UVTopRight",   m_UVTopRight);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Renderdata2D::performEditorEdit(app::App &app, edt::Editor &editor)
		{
			bool r{false};
			
			editor.header("2D properties");
			
			// Billboard.
			
			if (ImGui::Checkbox("Billboard", &m_billboard))
				r = true;
			
			// Texture.
			
			if (editor.pickerResource("rd2d-tex", "Texture", m_texture, app.res().IDOfType<res::Texture>()))
				r = true;
			
			// Tint colour picker.
			
			ImGui::ColorEdit4
			(
				"Tint",
				&m_tint.r
			);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			// Size and UV's.
			
			auto const handleVec2([&r](char const *const name, mth::Vec2f &vec)
			{
				ImGui::DragFloat2(name, &vec.x, 0.01f);
				
				if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
					r = true;
			});
			
			handleVec2("Size",           m_size);
			handleVec2("UV bottom-left", m_UVBottomLeft);
			handleVec2("UV top-right",   m_UVTopRight);
			
			return r;
		}
		#endif
	}
}
