/**
 * @author		Ava Skoog
 * @date		2018-10-22
 * @copyright   2017-2023 Ava Skoog
 */

// https://stackoverflow.com/questions/37942389/correctly-transforming-a-node-relative-to-a-specified-space

#include <karhu/app/gfx/components/Transform.hpp>
#include <karhu/app/gfx/events/ETransform.hpp>
#include <karhu/app/App.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
	#include <karhu/app/edt/Editor.hpp>
	#include <karhu/app/edt/actions/AEntitiesTransformed.hpp>

	#include <karhu/app/karhuimgui.hpp>
#endif

namespace karhu
{
	namespace gfx
	{
		void Transform::local(mth::Transformf &&value)
		{
			m_local = std::move(value);
			
			auto &world(app::App::instance()->ecs()); /// @todo: Can we decouple this?
			
			refresh(world);
			
			world.forEachChildOfEntity(entity(), [&world](ecs::IDEntity const ID)
			{
				if (auto c = world.componentInEntity<Transform>(ID))
					c->refresh(world);
				
				return true;
			}, false);
		}
		
		void Transform::local(mth::Transformf const &value)
		{
			mth::Transformf copy{value};
			local(std::move(copy));
		}
		
		void Transform::global(mth::Transformf &&value)
		{
			auto &world(app::App::instance()->ecs()); /// @todo: Can we decouple this?
			
			ecs::IDEntity const
				IDParent{world.entityParentFirst(entity())};
			
			if (IDParent != entity())
			{
				auto const
					transformParent(world.componentInEntity<Transform>(IDParent));
				
				if (transformParent)
				{
					mth::Mat4f const
						matrix{mth::inverse(transformParent->global().matrix()) * value.matrix()};
					
					mth::decompose(matrix, &value.position, &value.rotation, &value.scale);
					value.rotation = mth::conjugate(value.rotation);
				}
			}
			
			local(std::move(value));
		}
		
		void Transform::global(mth::Transformf const &value)
		{
			mth::Transformf copy{value};
			global(std::move(copy));
		}
		
		void Transform::serialise(conv::Serialiser &ser) const
		{
			ser << karhuINn("local", m_local);
		}

		void Transform::deserialise(conv::Serialiser const &ser)
		{
			ser >> karhuOUTn("local", m_local);
		}
		
		void Transform::refresh(ecs::World &world)
		{
			mth::Mat4f
				TS{m_local.matrix()},
				R {mth::rotationmatrix(m_local.rotation)};

			world.forEachParentOfEntity
			(
				entity(),
				[&TS, &R, &world](ecs::IDEntity const ID)
				{
					if (auto parent = world.componentInEntity<Transform>(ID))
					{
						auto const
							t(parent->m_local);
						
						auto const
							tp(mth::translationmatrix(t.position));
						
						auto const
							rp(mth::rotationmatrix(t.rotation));
						
						auto const
							sp(mth::scalematrix(t.scale));

						mth::Mat4f tf, T, S;
						mth::Vec3f sf;
						
						{
							mth::Vec3f p, s;
							
							mth::decompose(TS, &p, nullptr, &s);
							
							mth::Mat4f const
								tc{mth::translationmatrix(p)},
								sc{mth::scalematrix      (s)};
						
							tf = tp * /*mth::inverse(*/rp/*)*/ * sp * tc;
							sf = t.scale * s;
						}
						
						T = mth::translationmatrix(mth::position(tf));
						S = mth::scalematrix      (sf);
						
						TS = T * S;
						//R  = R * rp;
						R  = rp * R;
					}

					return true;
				}
			);

			mth::decompose(TS, &m_global.position, nullptr, &m_global.scale);
			m_global.rotation = mth::conjugate(mth::rotation(R));
			
			m_matrixLocal = m_local.matrix();
			m_matrixGlobal = m_global.matrix();
			
			world.queueEvent<ETransform>(entity(), typeComponent(), identifier());
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		void Transform::editorEvent
		(
			      app::App        & ,
			      edt::Editor     & ,
			edt::Identifier const   ,
			edt::Event      const &e
		)
		{
			if
			(
				edt::TypeEvent::entitiesSelected    != e.type &&
				edt::TypeEvent::entitiesTransformed != e.type
			)
				return;
			
			c_editor.transform.position = m_global.position;
			c_editor.transform.origin   = m_global.origin;
			c_editor.transform.scale    = m_global.scale;
			c_editor.rotationEuler      = mth::seuler(m_global.rotation);
			
			/// @todo: Make it possible to set edit mode with degrees or radians or the quaternion directly.
			c_editor.rotationEuler.x = mth::degrees(c_editor.rotationEuler.x);
			c_editor.rotationEuler.y = mth::degrees(c_editor.rotationEuler.y);
			c_editor.rotationEuler.z = mth::degrees(c_editor.rotationEuler.z);
		}
		
		bool Transform::editorEdit
		(
			app::App    &app,
			edt::Editor &editor
		)
		{
			float const
				width{ImGui::GetContentRegionAvail().x - 60.0f}; /// @todo: Figure this hardcoded number out.
			
			const ImVec4
				colX{1.0f, 0.4f, 0.4f, 1.0f},
				colY{0.5f, 0.9f, 0.3f, 1.0f},
				colZ{0.4f, 0.5f, 1.0f, 1.0f};
			
			constexpr float
				speedPos{0.01f},
				speedOrg{0.01f},
				speedScl{0.01f},
				speedRot{0.5f};
			
			bool
				pos{false},
				org{false},
				scl{false},
				rot{false};
			
			bool const
				drag{c_editor.drag};
			
			c_editor.drag = false;
			
			ImGui::PushID(static_cast<int>(identifier()));

			ImGui::Text("Position");
			ImGui::BeginGroup();
				ImGui::PushMultiItemsWidths(3, width);
				ImGui::AlignTextToFramePadding();
				ImGui::TextColored(colX, "X");
				ImGui::SameLine();
				ImGui::DragFloat("##pos-x", &c_editor.transform.position.x, speedPos);
				c_editor.drag |= pos |= ImGui::IsItemActive();
				ImGui::PopItemWidth();
				ImGui::SameLine();
				ImGui::TextColored(colY, "Y");
				ImGui::SameLine();
				ImGui::DragFloat("##pos-y", &c_editor.transform.position.y, speedPos);
				c_editor.drag |= pos |= ImGui::IsItemActive();
				ImGui::PopItemWidth();
				ImGui::SameLine();
				ImGui::TextColored(colZ, "Z");
				ImGui::SameLine();
				ImGui::DragFloat("##pos-z", &c_editor.transform.position.z, speedPos);
				c_editor.drag |= pos |= ImGui::IsItemActive();
				ImGui::PopItemWidth();
			ImGui::EndGroup();
			
			ImGui::Text("Origin");
			ImGui::BeginGroup();
				ImGui::PushMultiItemsWidths(3, width);
				ImGui::AlignTextToFramePadding();
				ImGui::TextColored(colX, "X");
				ImGui::SameLine();
				ImGui::DragFloat("##org-x", &c_editor.transform.origin.x, speedOrg);
				c_editor.drag |= org |= ImGui::IsItemActive();
				ImGui::PopItemWidth();
				ImGui::SameLine();
				ImGui::TextColored(colY, "Y");
				ImGui::SameLine();
				ImGui::DragFloat("##org-y", &c_editor.transform.origin.y, speedOrg);
				c_editor.drag |= org |= ImGui::IsItemActive();
				ImGui::PopItemWidth();
				ImGui::SameLine();
				ImGui::TextColored(colZ, "Z");
				ImGui::SameLine();
				ImGui::DragFloat("##org-z", &c_editor.transform.origin.z, speedOrg);
				c_editor.drag |= org |= ImGui::IsItemActive();
				ImGui::PopItemWidth();
			ImGui::EndGroup();
			
			ImGui::Text("Scale");
			ImGui::BeginGroup();
				ImGui::PushMultiItemsWidths(3, width);
				ImGui::AlignTextToFramePadding();
				ImGui::TextColored(colX, "X");
				ImGui::SameLine();
				ImGui::DragFloat("##scl-x", &c_editor.transform.scale.x, speedScl);
				c_editor.drag |= scl |= ImGui::IsItemActive();
				ImGui::PopItemWidth();
				ImGui::SameLine();
				ImGui::TextColored(colY, "Y");
				ImGui::SameLine();
				ImGui::DragFloat("##scl-y", &c_editor.transform.scale.y, speedScl);
				c_editor.drag |= scl |= ImGui::IsItemActive();
				ImGui::PopItemWidth();
				ImGui::SameLine();
				ImGui::TextColored(colZ, "Z");
				ImGui::SameLine();
				ImGui::DragFloat("##scl-z", &c_editor.transform.scale.z, speedScl);
				c_editor.drag |= scl |= ImGui::IsItemActive();
				ImGui::PopItemWidth();
			ImGui::EndGroup();
			
			ImGui::Text("Rotation (degrees)");
			ImGui::BeginGroup();
				ImGui::PushMultiItemsWidths(3, width);
				ImGui::AlignTextToFramePadding();
				ImGui::TextColored(colX, "X");
				ImGui::SameLine();
				ImGui::DragFloat("##rot-x", &c_editor.rotationEuler.x, speedRot);
				c_editor.drag |= rot |= ImGui::IsItemActive();
				ImGui::PopItemWidth();
				ImGui::SameLine();
				ImGui::TextColored(colY, "Y");
				ImGui::SameLine();
				ImGui::DragFloat("##rot-y", &c_editor.rotationEuler.y, speedRot);
				c_editor.drag |= rot |= ImGui::IsItemActive();
				ImGui::PopItemWidth();
				ImGui::SameLine();
				ImGui::TextColored(colZ, "Z");
				ImGui::SameLine();
				ImGui::DragFloat("##rot-z", &c_editor.rotationEuler.z, speedRot);
				c_editor.drag |= rot |= ImGui::IsItemActive();
				ImGui::PopItemWidth();
			ImGui::EndGroup();
			
			ImGui::PopID();
			
			// Started dragging?
			if (!drag && c_editor.drag)
			{
				if      (pos)
				{
					c_editor.transformation = edt::Transformation::position;
					c_editor.prev           = mth::Vec4f{m_global.position, 0.0f};
				}
				else if (org)
				{
					c_editor.transformation = edt::Transformation::origin;
					c_editor.prev           = mth::Vec4f{m_global.origin, 0.0f};
				}
				else if (scl)
				{
					c_editor.transformation = edt::Transformation::scale;
					c_editor.prev           = mth::Vec4f{m_global.scale, 0.0f};
				}
				else if (rot)
				{
					c_editor.transformation = edt::Transformation::rotation;
					c_editor.prev           = reinterpret_cast<mth::Vec4f &>(m_global.rotation);
				}
			}
			
			if (drag)
			{
				mth::Vec4f r;
			
				switch (c_editor.transformation)
				{
					case edt::Transformation::position:
						r = mth::Vec4f{c_editor.transform.position, 0.0f};
						break;
			
					case edt::Transformation::origin:
						r = mth::Vec4f{c_editor.transform.origin, 0.0f};
						break;
				
					case edt::Transformation::scale:
						r = mth::Vec4f{c_editor.transform.scale, 0.0f};
						break;
				
					case edt::Transformation::rotation:
					{
						/// @todo: Make it possible to set edit mode with degrees or radians or the quaternion directly.
						
						c_editor.transform.rotation = mth::euler
						(
							mth::radians(c_editor.rotationEuler.x),
							mth::radians(c_editor.rotationEuler.y),
							mth::radians(c_editor.rotationEuler.z)
						);
						
						r = reinterpret_cast<mth::Vec4f &>(c_editor.transform.rotation);
						
						break;
					}
				}
				
				ecs::IDEntity const ID{entity()};
				
				editor.entitiesTransformation
				(
					c_editor.transformation,
					1,
					&ID,
					&r,
					false,
					static_cast<edt::Identifier>(identifier())
				);
			
				// Push to history if finished dragging.
				if (!c_editor.drag)
				{
					/// @todo: Deal with multiselect?
					
					std::vector<ecs::IDEntity>
						entities{entity()};
					
					std::vector<mth::Vec4f>
						curr{r},
						prev{c_editor.prev};
			
					editor.pushAction
					(
						static_cast<edt::Identifier>(identifier()),
						edt::AEntitiesTransformed
						{
							app,
							editor,
							c_editor.transformation,
							std::move(entities),
							std::move(curr),
							std::move(prev)
						}
					);
				}
			}
			
			return false;
		}
		
		Transform::Editor Transform::c_editor;
		#endif
	}
}
