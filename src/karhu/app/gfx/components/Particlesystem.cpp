/**
 * @author		Ava Skoog
 * @date		2019-09-12
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/components/Particlesystem.hpp>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace gfx
	{
		void Particlesystem::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("space",      m_space)
				<< karhuINn("startcount", m_startcount);
		}

		void Particlesystem::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUTn("space",      m_space)
				>> karhuOUTn("startcount", m_startcount);
			
			if (m_startcount > 0)
				reserve(m_startcount);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Particlesystem::editorEdit(app::App &, edt::Editor &)
		{
			return false;
		}
		#endif
	}
}
