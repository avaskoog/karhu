/**
 * @author		Ava Skoog
 * @date		2017-11-05
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Deal with multiple different animations/clips.

#ifndef KARHU_GRAPHICS_ANIMATOR_H_
	#define KARHU_GRAPHICS_ANIMATOR_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/res/Animationcontroller.hpp>
	#include <karhu/res/Ref.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/maths.hpp>

	#include <vector>
	#include <deque>
	#include <unordered_map>
	#include <cstdint>

	namespace karhu
	{
		namespace gfx
		{
			enum class ModeAnimator
			{
				clip,      // Play a single clip.
				controller // Use an animation controller.
			};
			
			class Animator : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
				public:
					bool valid() const noexcept { return m_valid; }
				
					void mode(const ModeAnimator);
					ModeAnimator mode() const noexcept { return m_mode; }
				
					void loop(bool const);
					bool loop() const noexcept { return m_loop; }
				
					void autoplay(bool const);
					bool autoplay() const noexcept { return m_autoplay; }
				
					void clip(res::IDResource const);
					res::IDResource clip() const noexcept { return m_clip; }
				
					void clips(res::IDResource const);
					res::IDResource clips() const noexcept { return m_clips; }
				
					void controller(res::IDResource const);
					res::IDResource controller() const noexcept { return m_controller; }
				
					void rig(const res::IDResource ID);
					res::IDResource rig() const noexcept { return m_rig; }
				
					void speed(float const);
					float speed() const noexcept { return m_speed; }
				
					void start();
					void restart();
					void pause();
					void reset();
					void stop();
					void tick(float const dt);
					bool playing() const noexcept { return m_playing; }
				
					void variable(std::string const &name, std::string const &constant);
				
					void boolean(std::string const &name, bool const value);
					void boolean(char const *const name, bool const value);
					void boolean(std::string const &name, std::string const &constant);
					bool boolean(std::string const &name) const;
					bool boolean(char const *const name) const;
				
					void integer(std::string const &name, std::int32_t const value);
					void integer(char const *const name, std::int32_t const value);
					void integer(std::string const &name, std::string const &constant);
					std::int32_t integer(std::string const &name) const;
					std::int32_t integer(char const *const name) const;
				
					void floating(std::string const &, float const value);
					void floating(char const *const name, float const value);
					void floating(std::string const &name, std::string const &constant);
					float floating(std::string const &) const;
					float floating(char const *const name) const;
				
					void trigger(std::string const &name);
					void trigger(char const *const name);
				
					res::Animationcontroller::ValueWithName const *constant(std::string const &name) const;
				
					std::vector<mth::Mat4f> const &matrices() const noexcept { return m_transformsPerJointByID; }
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(conv::Serialiser const &);

					#if KARHU_EDITOR_SERVER_SUPPORTED
                    void editorEvent(app::App &, edt::Editor &, edt::Identifier const /*caller*/, edt::Event const &) override {}
                    bool editorEdit(app::App &, edt::Editor &) override;
                    #endif
				
				private:
					using Index         = res::Animationcontroller::Index;
					using Value         = res::Animationcontroller::Value;
					using ValueWithName = res::Animationcontroller::ValueWithName;
					using Trigger       = res::Animationcontroller::Trigger;
					using Clip          = res::Animationcontroller::Clip;
					using Transition    = res::Animationcontroller::Transition;
					using Comparand     = res::Animationcontroller::Transition::Comparand;
					using Operation     = res::Animationcontroller::Transition::Operation;
					using Behaviour     = res::Animationcontroller::Transition::Behaviour;
				
				private:
					struct Clipstate
					{
						float time{0.0f};
					};
				
					struct Blend
					{
						Index src, dst;
						float duration, time;
					};
				
					struct Controllerstate
					{
						Index indexClipCurr;
						Blend blend;
						
						std::vector<Clipstate> clipstates;
						
						std::vector<Value> vars;
						std::deque<bool>  triggers; // To avoid vector<bool> weirdness.
						
						std::unordered_map<std::string, Index>
							indicesVars,
							indicesTriggers;
						
						res::Ref<res::Animationcontroller>
							controller;
						
						bool blending{false};
					};
				
				private:
					ModeAnimator
						m_mode{ModeAnimator::clip};
				
					float
						m_speed{1.0f};
				
					bool
						m_autoplay{true},
					    m_loop    {true};
				
					res::IDResource
						m_controller{0},
						m_clips     {0},
						m_clip      {0},
						m_rig       {0};
				
				private:
					bool
						m_modified{true},
						m_valid   {false},
					    m_playing {false};
				
					float
						m_tick{0.0f};
				
					Clipstate
						m_clipstate;
				
					Controllerstate
						m_controllerstate;
				
					std::vector<mth::Mat4f>
						m_transformsPerJointByID;
				
				friend class SystemAnimator;
			};
		}
	}
#endif
