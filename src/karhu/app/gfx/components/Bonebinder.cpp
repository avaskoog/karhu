/**
 * @author		Ava Skoog
 * @date		2023-01-16
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/components/Bonebinder.hpp>
#include <karhu/app/App.hpp>
#include <karhu/app/ecs/World.hpp>
#include <karhu/app/gfx/components/Animator.hpp>
#include <karhu/res/Rig.hpp>
#include <karhu/data/serialise.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/karhuimgui.hpp>
#include <karhu/app/edt/dropdown.inl>
#endif

namespace
{
	using namespace karhu;
	using namespace karhu::gfx;
	
	template<typename Iterator>
	inline char const *getStrDropdownBone(Iterator const &it)
	{
		return (*it)->name.c_str();
	}

	template<typename T, typename Iterator>
	inline T getValDropdownBone(Iterator const &it, Iterator const &/*begin*/)
	{
		return (*it)->ID;
	}
}

namespace karhu
{
	namespace gfx
	{
		void Bonebinder::IDJoint(std::size_t const ID)
		{
			m_IDJoint = ID;
		}
		
		void Bonebinder::offset(mth::Vec3f const &offset)
		{
			m_offset = offset;
		}
		
		void Bonebinder::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("IDJoint", m_IDJoint)
				<< karhuINn("offset",  m_offset);
		}
		
		void Bonebinder::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUTn("IDJoint", m_IDJoint)
				>> karhuOUTn("offset",  m_offset);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Bonebinder::editorEdit(app::App &app, edt::Editor &)
		{
			bool r{false};
			
			ecs::IDEntity const
			 	IDParent{app.ecs().entityParentFirst(entity())};
			
			if (IDParent == entity())
				return r;
			
			auto
				anim(app.ecs().componentInEntity<gfx::Animator>(IDParent));
			
			if (!anim)
				return r;
			
			auto
				rig(app.res().get<res::Rig>(anim->rig()));
			
			r |= edt::dropdown
			(
				"Bone",
				m_IDJoint,
				rig->childrenInOrder.begin(),
				rig->childrenInOrder.end(),
				false,
				edt::getItDropdownIndexed,
				getStrDropdownBone,
				getValDropdownBone
			);
			
			r |= ImGui::InputFloat3("Offset", &m_offset.x);
			
			return r;
		}
		#endif
	}
}
