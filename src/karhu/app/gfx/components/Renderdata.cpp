/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/components/Renderdata.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
#include <karhu/app/edt/Editor.hpp>
#include <karhu/app/App.hpp>
#include <karhu/app/gfx/SubsystemGraphics.hpp>
#include <karhu/app/karhuimgui.hpp>
#include <karhu/app/edt/dropdown.inl>
#include <karhu/app/edt/util.hpp>
#endif

namespace karhu
{
	namespace gfx
	{
		void Renderdata::instanced(bool const set)
		{
			m_instanced = set;
		}
		
		void Renderdata::layers(const Layers v)
		{
			m_layers = v;
		}
		
		void Renderdata::modeShadows(ModeShadows const v)
		{
			m_modeShadows = v;
		}
		
		mth::Bounds3f const Renderdata::AABB(mth::Transformf const &transform) const noexcept
		{
			mth::Bounds3f
				r{AABB()};
			
			// Rotation is taken into account when calculating local AABB.
			r.min *= transform.scale;
			r.max *= transform.scale;
			r.min += transform.position;
			r.max += transform.position;
			r.min -= transform.origin;
			r.max -= transform.origin;
			
			return r;
		}
		
		void Renderdata::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("instanced",   m_instanced)
				<< karhuINn("layers",      m_layers)
				<< karhuINn("modeShadows", m_modeShadows)
				<< karhuINn("inputs",      m_inputs);
			
			performSerialise(ser);
		}

		void Renderdata::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUTn("instanced",   m_instanced)
				>> karhuOUTn("layers",      m_layers)
				>> karhuOUTn("modeShadows", m_modeShadows)
				>> karhuOUTn("inputs",      m_inputs);
			
			performDeserialise(ser);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		void Renderdata::editorEvent(app::App &app, edt::Editor &editor, edt::Identifier const caller, edt::Event const &e)
		{
			performEditorEvent(app, editor, caller, e);
		}
		
		bool Renderdata::editorEdit(app::App &app, edt::Editor &editor)
		{
			bool r{false};
			
			editor.header("General properties");
			
			// Instancing.
			
			if (ImGui::Checkbox("Instanced", &m_instanced))
				r = true;
			
			// Layers.
			/// @todo: Add some way to create/remove graphics layers in the editor.
			
			if (edt::dropdownLayersGraphics(app, m_layers))
				r = true;
			
			// Shadow mode.
			
			constexpr std::pair<char const *, ModeShadows>
				modes[]
				{
					{"None",                 ModeShadows::none},
					{"Normal",               ModeShadows::normal},
					{"Custom",               ModeShadows::custom},
					{"Normal (only shadow)", ModeShadows::normalOnlyShadow},
					{"Custom (only shadow)", ModeShadows::customOnlyShadow}
				};
			
			if (edt::dropdown("Shadows", m_modeShadows, std::begin(modes), std::end(modes)))
				r = true;
			
			// Inputs.
			
			editor.header("Material inputs");
			
			if (edt::editInputsGraphics(editor, app, m_inputs))
				r = true;
			
			/// @todo: Renderdata inputs. Reuse also for materials etc.
			/*
			auto const nameOfTypeInput([](TypeInput const type) -> char const *
			{
				using Type = TypeInput;
				
				switch (type)
				{
					case Type::boolean:   return "boolean";
					case Type::integer:   return "integer";
					case Type::floating:  return "floating";
					case Type::vector2i:  return "vector2i";
					case Type::vector3i:  return "vector3i";
					case Type::vector4i:  return "vector4i";
					case Type::vector2f:  return "vector2f";
					case Type::vector3f:  return "vector3f";
					case Type::vector4f:  return "vector4f";
					case Type::matrix2:   return "matrix2";
					case Type::matrix3:   return "matrix3";
					case Type::matrix4:   return "matrix4";
					case Type::integerv:  return "integerv";
					case Type::floatingv: return "floatingv";
					case Type::vector2iv: return "vector2iv";
					case Type::vector3iv: return "vector3iv";
					case Type::vector4iv: return "vector4iv";
					case Type::vector2fv: return "vector2fv";
					case Type::vector3fv: return "vector3fv";
					case Type::vector4fv: return "vector4fv";
					case Type::matrix2v:  return "matrix2v";
					case Type::matrix3v:  return "matrix3v";
					case Type::matrix4v:  return "matrix4v";
					case Type::texture:   return "texture";
					case Type::invalid:   return "invalid";
				}
			});
			
			ImGui::Text("Inputs x %i", (int)inputs.values().size());
			
			for (auto const &value : inputs.values())
			{
				ImGui::Text("%s (%s)\n", value.first.c_str(), nameOfTypeInput(value.second.type));
			}
			*/
			if (performEditorEdit(app, editor))
				return true;
			
			return r;
		}
		#endif
	}
}
