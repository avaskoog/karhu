/**
 * @author		Ava Skoog
 * @date		2017-10-21
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_RENDERER_H_
	#define KARHU_APP_RENDERER_H_

	#include <karhu/app/gfx/Workload.hpp>
	#include <karhu/res/Material.hpp>
	#include <karhu/res/Shader.hpp>
	#include <karhu/res/Mesh.hpp>

	/// @todo: Probably create Renderer.cpp instead so that we can move implementation of bindCamera there.
	#include <karhu/app/gfx/components/Camera.hpp>
	#include <karhu/app/gfx/Rendertarget.hpp>

	namespace karhu
	{
		namespace ecs { class Octree; }
		
		namespace gfx
		{
			class Frustum;
			class Animator;
			
			/**
			 * Base class for a renderer to go through the specified passes.
			 *
			 * Everything is already supposed to be ordered for optimised
			 * drawing before getting passed to the renderer, so there is no
			 * need to do this manually, and things like material or mesh
			 * should only be updated at the beginning of each new group,
			 * before drawing all the instances.
			 *
			 * Each pass has its own camera to draw to. The camera may hold
			 * a buffer (or render texture) to draw to instead of the main one.
			 * Objects seen by the specified camera should already be filtered
			 * before the pass is sent to the renderer, so there is no need to
			 * do any of this manually either. Change the camera or buffer once
			 * at the beginning of each pass before iterating over its groups.
			 *
			 * Each group has a mode: static or dynamic draw. Static rendering
			 * should be prebatched where possible before getting passed to the
			 * renderer, merging geometry of meshes sharing the same material
			 * and settings. Dynamic draw groups can be instanced if there is
			 * more than one instance to draw.
			 *
			 * Derived implementations are only responsible for going through
			 * this presorted data and drawing everything in the most efficient
			 * way possible.
			 */
			class Renderer
			{
				public:
					void clear(const ColRGBAf &colour = {0.0f, 0.0f, 0.0f, 0.0f}) { performClear(colour); }
				
					void setConditionDepth(const ConditionDepth condition) const
					{
						return performSetConditionDepth(condition);
					}

					void renderGroups
					(
						Pass                 const &      pass,
						ecs::Octree          const *const octree,
						Frustum              const *const frustum,
						gfx::ContainerInputs const &      inputs,
						std::uint32_t        const        widthWindow,
						std::uint32_t        const        heightWindow,
						float                const        scaleWindowX,
						float                const        scaleWindowY,
						res::Material              *const materialOverride,
						bool                 const        shadow
					) const;
				
					void renderQuadFullscreen
					(
						const Pass &pass,
						const ContainerInputs &inputs,
						const std::uint32_t &widthWindow,
						const std::uint32_t &heightWindow,
						const float &scaleWindowX,
						const float &scaleWindowY,
						res::Material &material,
						res::Shader *const shader = nullptr
					) const
					{
						performRenderQuadFullscreen
						(
							pass,
							inputs,
							widthWindow,
							heightWindow,
							scaleWindowX,
							scaleWindowY,
							material,
							shader
						);
					}
				
					void compositeQuadsFullscreen
					(
						const Pass &pass,
						const ContainerInputs &inputs,
						const std::uint32_t &widthWindow,
						const std::uint32_t &heightWindow,
						const float &scaleWindowX,
						const float &scaleWindowY,
						res::Material &material,
						res::Shader *const shader = nullptr
					) const
					{
						performCompositeQuadsFullscreen
						(
							pass,
							inputs,
							widthWindow,
							heightWindow,
							scaleWindowX,
							scaleWindowY,
							material,
							shader
						);
					}

					bool refreshCamera
					(
						Camera &camera,
						const std::uint32_t &widthWindow,
						const std::uint32_t &heightWindow,
						const float &scaleWindowX,
						const float &scaleWindowY
					);

					bool bindCamera(Camera &camera) { return performBindCamera(camera); }
				
					/**
					 * Sets the shader in the material up for rendering
					 * an animated mesh using the matrices of the animator.
					 * This needs to be called manually by all renderer
					 * implementations before rendering an animated mesh.
					 *
					 * @param animator Pointer to the animator containing the matrices (null if animations should be disabled).
					 * @param mesh     The mesh to animate.
					 * @param LOD      The level of detail to render at.
					 * @param material The material with the shader to use.
					 *
					 * @return Whether binding was successful.
					 */
					bool bindAnimator
					(
						const Animator *const animator,
						const res::Mesh &mesh,
						const std::size_t &LOD,
						res::Material &material
					) const;

					void clearCamera(Camera &camera) { performClearCamera(camera); }

					/// @todo: renderParticles()

				protected:
					virtual const res::Shader &shaderForQuadOneByOne() const = 0;
				
					virtual void performClear(const ColRGBAf &) = 0; /// @todo: Add color params.
					virtual void performSetDepthmask(const bool) const = 0;
					virtual void performSetConditionDepth(const ConditionDepth) const = 0;
					virtual void performSetCulling(const Culling) const = 0;
				
					virtual void performResetTexturesamplers() const = 0;
				
					virtual void performDrawQuadOneByOne
					(
						const float UVs[4]
					) const = 0;
				
					virtual void performDrawQuadsOneByOne
					(
						const std::vector<gfx::Instance> &instances,
						const float UVs[4]
					) const = 0;
				
					virtual void performDrawSingleWithoutIndices
					(
						const res::Mesh::Buffer &buffer
					) const = 0;
				
					virtual void performDrawSingleWithIndices
					(
						const res::Mesh::Buffer        &buffer,
						const res::Mesh::Buffer::Range &range
					) const = 0;
				
					virtual void performDrawMultipleWithoutIndices
					(
						const res::Mesh::Buffer &buffer,
						const std::size_t       &count
					) const = 0;
				
					virtual void performDrawMultipleWithIndices
					(
						const res::Mesh::Buffer        &buffer,
						const res::Mesh::Buffer::Range &range,
						const std::size_t              &count
					) const = 0;
				
					virtual void performRenderQuadFullscreen
					(
						const Pass &pass,
						const ContainerInputs &inputs,
						const std::uint32_t &widthWindow,
						const std::uint32_t &heightWindow,
						const float &scaleWindowX,
						const float &scaleWindowY,
						res::Material &material,
						res::Shader *const shader
					) const = 0;
				
					virtual void performCompositeQuadsFullscreen
					(
						const Pass &pass,
						const ContainerInputs &inputs,
						const std::uint32_t &widthWindow,
						const std::uint32_t &heightWindow,
						const float &scaleWindowX,
						const float &scaleWindowY,
						res::Material &material,
						res::Shader *const shader
					) const = 0;
				
					virtual bool performBindCamera(Camera &camera) = 0;
					virtual void performClearCamera(Camera &camera) = 0;
			};
		}
	}
#endif
