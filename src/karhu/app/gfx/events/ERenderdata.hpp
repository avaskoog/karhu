/**
 * @author		Ava Skoog
 * @date		2021-07-20
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_EVENT_RENDERDATA_H_
	#define KARHU_GRAPHICS_EVENT_RENDERDATA_H_

	#include <karhu/app/ecs/common.hpp>

	namespace karhu
	{
		namespace gfx
		{
			class ERenderdata : public ecs::Event
			{
				public:
					using ecs::Event::Event;
			};
		}
	}
#endif
