/**
 * @author		Ava Skoog
 * @date		2017-11-09
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Cache buffers per camera so that they do not need to be resized every frame
/// @todo: Should we just be pingponging at most two textures instead of one for each step?
/// @todo: Probably need to do LOD a bit differently because that would have to be resorted a lot...

#include <karhu/app/gfx/Pipeline.hpp>
#include <karhu/app/gfx/Rendertarget.hpp>
#include <karhu/app/gfx/SubsystemGraphics.hpp>
#include <karhu/app/App.hpp>
#include <karhu/res/Bank.hpp>
#include <karhu/data/serialise.hpp>
#include <karhu/core/log.hpp>

/// @todo: Temporary.
#include <karhu/app/gfx/components/Light.hpp>
#include <karhu/app/edt/Editor.hpp>

//! @todo: Remove OpenGL code when done testing
#include <karhu/app/backend/implementation/graphics/GL/setup.hpp>

namespace karhu
{
	namespace gfx
	{
		void Pipeline::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN (sizeShadowmapMin)
				<< karhuIN (sizeShadowmapMax)
				<< karhuINn("steps", m_steps);
		}
		
		void Pipeline::deserialise(conv::Serialiser const &ser)
		{
			std::vector<Step> proxy;
			
			ser
				>> karhuOUT (sizeShadowmapMin)
				>> karhuOUT (sizeShadowmapMax)
				>> karhuOUTn("steps", proxy);
			
			/// @todo: Get rid of app singleton in pipeline deserialisation, and existence.
			auto
				&app(*app::App::instance());
			
			/// @todo: Or should this be in steps() or bake()?
			#ifdef KARHU_EDITOR
			if (app::ModeApp::server == app.mode())
			{
				for (auto &step : proxy)
				{
					if (Step::Type::groups == step.type)
					{
						step.buffers.emplace_back("__karhuTexID", TypeBuffer::data, 0);
						break;
					}
				}
			}
			#endif
			
			steps(proxy);
			bake(app.gfx());
		}
		
		void Pipeline::DataStep::serialise(conv::Serialiser &ser) const
		{
			step.serialise(ser);
		}
		
		void Pipeline::DataStep::deserialise(conv::Serialiser const &) {}
		
		void Pipeline::Step::serialise(conv::Serialiser &ser) const
		{
			std::vector<Buffer> proxyBuffers;
			
			for (Buffer const &buffer : buffers)
				if (buffer.name != "__karhuTexID")
					proxyBuffers.emplace_back(buffer);
			
			ser
				<< karhuIN (active)
				<< karhuIN (type)
				<< karhuIN (extrabuffer)
				<< karhuIN (nameExtrabuffer)
				<< karhuINn("buffers", proxyBuffers)
				<< karhuIN (material);
			
			if (Type::shadows == type)
				ser
					<< karhuIN(scaleShadowmaps)
					<< karhuIN(marginShadowmaps)
					<< karhuIN(cutoffShadowmaps)
					<< karhuIN(biasShadowmaps);
		}
		
		void Pipeline::Step::deserialise(conv::Serialiser const &ser)
		{
			ser
				>> karhuOUT(active)
				>> karhuOUT(type)
				>> karhuOUT(extrabuffer)
				>> karhuOUT(nameExtrabuffer)
				>> karhuOUT(buffers)
				>> karhuOUT(material);
			
			if (Type::shadows == type)
				ser
					>> karhuOUT(scaleShadowmaps)
					>> karhuOUT(marginShadowmaps)
					>> karhuOUT(cutoffShadowmaps)
					>> karhuOUT(biasShadowmaps);
		}
		
		void Pipeline::Buffer::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(name)
				<< karhuIN(type);
			
			switch (type)
			{
				case TypeBuffer::R:
				case TypeBuffer::RG:
				case TypeBuffer::RGB:
				case TypeBuffer::RGBA:
					ser
						<< karhuIN(clear)
						<< karhuIN(clearcolour);
					break;
				
				case TypeBuffer::data:
					ser << karhuIN(cleardata);
					break;
			}
		}
		
		void Pipeline::Buffer::deserialise(conv::Serialiser const &ser)
		{
			ser
				>> karhuOUT(name)
				>> karhuOUT(type);
			
			switch (type)
			{
				case TypeBuffer::R:
				case TypeBuffer::RG:
				case TypeBuffer::RGB:
				case TypeBuffer::RGBA:
				{
					ser >> karhuOUT(clear);
					
					if (ModeClear::colour == clear)
						ser >> karhuOUT(clearcolour);
					
					break;
				}
				
				case TypeBuffer::data:
					ser >> karhuOUT(cleardata);
					break;
			}
		}
		
		/**
		 * Initialises the pipeline with the steps specified.
		 * These cannot be modified later.
		 *
		 * @param steps The final list of steps to use.
		 */
		void Pipeline::steps(std::vector<Step> const &steps)
		{
			// At this point we just generate the list of
			// step data and copy the steps into it.
			// Bake to validate and generate buffers.

			m_steps.resize(steps.size());
			m_steps.shrink_to_fit();
			
			for (std::size_t i{0}; i < steps.size(); ++ i)
				m_steps[i].step = steps[i];
		}

		Rendertarget *Pipeline::rendertargetForStep(std::size_t const index) const
		{
			return (m_steps.size() > index) ? m_steps[index].rendertarget.get() : nullptr;
		}

		std::size_t Pipeline::countSteps() const noexcept
		{
			return m_steps.size() + 1; // Plus the final step.
		}
		
		Pipeline::DataStep *Pipeline::step(std::size_t const index)
		{
			if (index < m_steps.size())
				return &m_steps[index];
			else if (index == m_steps.size())
				return &m_stepFinal.data;
			
			return nullptr;
		}
		
		Pipeline::DataStep const *Pipeline::step(std::size_t const index) const
		{
			if (index < m_steps.size())
				return &m_steps[index];
			else if (index == m_steps.size())
				return &m_stepFinal.data;
			
			return nullptr;
		}
		
		bool Pipeline::bake(gfx::SubsystemGraphics &graphics)
		{
			// Can only bake once.
			if (m_baked)
				return true;

			m_baked = false;
			
			// This material needs only render a quad, so we can turn some things off.
			/// @todo: Should conditions/culling be defaults if creating texture shader and not need to be set manually here?
			m_stepFinal.material.conditionDepth = ConditionDepth::never;
			m_stepFinal.material.culling        = Culling::none;
			
			m_stepFinal.material.setIdentifier(std::numeric_limits<res::IDResource>::max());

			// We need to generate one special final step as well.
			// We will move it out of the list of regular steps again,
			// but put it there initially to have it go through the
			// same validation and generation as the other steps.

			// The final step is applied at the end.
			m_steps.emplace_back
			(
				DataStep
				{
					Step
					{
						true,
						Step::Type::quad,
						Step::Extrabuffer::none,
						"",
						{Buffer{"_texMain", TypeBuffer::RGBA, ModeClear::colour}}, /// @todo: Get uniform name from constant, i.e. move the list of constants out of GL submodule and into the core stuff here?
						m_stepFinal.material.identifier()
					}
				}
			);
			
			m_datasStepShadow.clear();
			
			for (std::size_t i{0}; i < m_steps.size(); ++ i)
			{
				auto
					&data(m_steps[i]);
				
				auto
					&step(data.step);
				
				if (Step::Type::shadows == step.type)
				{
					step.indexShadowmaps = m_datasStepShadow.size();
					
					DataStepShadow data;
					data.indexStep = i;
					data.camera.clearcolour({1.0f, 1.0f, 1.0f, 1.0f});
					Projection p(data.camera.projection());
					p.type = TypeProjection::orthographic;
					data.camera.projection(std::move(p));
					m_datasStepShadow.emplace_back(std::move(data));
				}
				
				res::Material const *material{nullptr};
				
				if (0 != data.step.material)
				{
					if (m_stepFinal.material.identifier() == data.step.material)
						material = &m_stepFinal.material;
					else
						/// @todo: Get rid of app singleton in pipeline bake.
						material = app::App::instance()->res().get<res::Material>(data.step.material);
				}

				// Generate the rendertarget.
				data.rendertarget = graphics.createRendertarget();

				if (!data.rendertarget)
					return false;

				// Generate the correct kind of extrabuffer.
				switch (step.extrabuffer)
				{
					case Step::Extrabuffer::none:
						break;
						
					case Step::Extrabuffer::depth:
					{
						data.extrabuffer = graphics.createTexture(Texture::Type::depth);
						data.rendertarget->depth(*data.extrabuffer);
						break;
					}
						
					case Step::Extrabuffer::stencil:
					{
						data.extrabuffer = graphics.createTexture(Texture::Type::stencil);
						data.rendertarget->stencil(*data.extrabuffer);
						break;
					}
						
					case Step::Extrabuffer::shadowdepth:
					{
						data.extrabuffer = graphics.createTexture(Texture::Type::shadowdepth);
						data.extrabuffer->filter(res::Texture::ModeFilter::linear);
						data.rendertarget->depth(*data.extrabuffer);
						break;
					}
				}
				
				if (Step::Extrabuffer::none != step.extrabuffer && !data.extrabuffer)
				{
					log::err("Karhu") << "Failed to generate extrabuffer for pipeline step " << i;
					return false;
				}
				
				// Generate the textures.
				
				data.textures.resize(step.buffers.size());
				data.textures.shrink_to_fit();
				
				for (std::size_t j{0}; j < step.buffers.size(); ++ j)
				{
					Pixelbuffer::Format f;
					
					switch (step.buffers[j].type)
					{
						case TypeBuffer::R:
							f = Pixelbuffer::Format::BW;
							break;
							
						case TypeBuffer::RG:
							f = Pixelbuffer::Format::BWA;
							break;
							
						case TypeBuffer::RGB:
							f = Pixelbuffer::Format::RGB;
							break;
							
						case TypeBuffer::RGBA:
						case TypeBuffer::data:
							f = Pixelbuffer::Format::RGBA;
							break;
					}
					
					Texture::Type type;
					
					switch (step.buffers[j].type)
					{
						case TypeBuffer::R:
						case TypeBuffer::RG:
						case TypeBuffer::RGB:
						case TypeBuffer::RGBA:
							type = Texture::Type::colour;
							break;
							
						case TypeBuffer::data:
							type = Texture::Type::data;
							break;
					}

					data.textures[j] = graphics.createTexture(type, f);
					data.textures[j]->filter(Texture::ModeFilter::linear);
					
					if (!data.textures[j])
					{
						log::err("Karhu") << "Failed to generate texture " << j << " for pipeline step " << i;
						return false;
					}
					
					data.rendertarget->texture(*data.textures[j]);
				}
				
				if (material)
					data.inputs = material->inputs;
				
				// Output an error if there is no material when there has to be.
				if (!material && (Step::Type::quad == step.type || Step::Type::lights == step.type))
				{
					log::err("Karhu") << "Missing material for pipeline step " << i << " of type 'quad'";
					return false;
				}
			}

			// Pop the special steps back out of the list in reverse order to insertion.
			m_stepFinal.data = std::move(m_steps.back());
			m_steps.pop_back();

			return (m_baked = true);
		}

		void Pipeline::render
		(
			app::App              &app,
			Renderer              &renderer,
			Workload              &workload,
			ContainerInputs const &inputs,
			std::uint32_t   const  widthWindow,
			std::uint32_t   const  heightWindow,
			float           const  scaleWindowX,
			float           const  scaleWindowY
		)
		{
			bool usedFramebufferDefault{false};
			
			#if KARHU_EDITOR_SERVER_SUPPORTED
			bool endedFrameIm3D{false}; /// @todo: Quick hack for Im3d. Temporary.
			#endif
			
			// Each pass will go through every step.
			for (auto &pass : workload.passes)
			{
				if (!pass.camera)
					continue;
				
				if (pass.camera->target())
				{
					auto const w(static_cast<std::uint32_t>(mth::round(widthWindow)));
					auto const h(static_cast<std::uint32_t>(mth::round(heightWindow)));
					
					if (w != pass.camera->target()->width() || h != pass.camera->target()->height())
					{
						pass.camera->target()->updateSize
						(
							widthWindow,
							heightWindow,
							scaleWindowX,
							scaleWindowY
						);
						
						pass.camera->updateProjection
						(
							widthWindow,
							heightWindow,
							scaleWindowX,
							scaleWindowY
						);
					}
				}
				
				float const
					sizeWindowMax(static_cast<float>(mth::max(widthWindow, heightWindow)));

				// Each step writes to its own buffer.
				for (std::size_t i{0}; i < m_steps.size(); ++ i)
				{
/// @todo: Quick hack to get GUI back before redesigning pipelines with renderbuffers.
if (ModeCamera::GUI == pass.camera->mode())
	if (i > 0)
		break;

					DataStep
						&curr{m_steps[i]};
					
					//! @todo: Quick hack, should not check if GUI
//					if (ModeCamera::GUI != pass.camera->mode())
					if (!curr.step.active)
						continue;
					
					res::Material
						*material{nullptr};

					if (0 != curr.step.material)
					{
						if (m_stepFinal.material.identifier() == curr.step.material)
							material = &m_stepFinal.material;
						else
							material = app.res().get<res::Material>(curr.step.material);
					}

					Camera *camera{pass.camera};
					Transform *transform{pass.transformCamera};

					bool const
						shadows{(Step::Type::shadows == curr.step.type)};
					
					mth::Scalu
						sizeShadows;

					if (shadows)
					{
						sizeShadows = mth::clamp
						(
							static_cast<mth::Scalu>(sizeWindowMax * curr.step.scaleShadowmaps),
							sizeShadowmapMin,
							sizeShadowmapMax
						);
						
						glEnable(GL_DEPTH_CLAMP); //! @todo: Remove OpenGL code when done testing
						
//					pass.camera=app::App::instance()->ecs().componentInEntity<Camera>(13);
//					pass.transformCamera=app::App::instance()->ecs().componentInEntity<Transform>(13);

						updateStepForShadows(curr, pass, sizeShadows);
						
						pass.camera = &m_datasStepShadow[curr.step.indexShadowmaps].camera;
						pass.transformCamera = &m_datasStepShadow[curr.step.indexShadowmaps].transform;
					}

					// Bind the buffer of the current step.
					bindStepOutput
					(
						*pass.camera,
						*curr.rendertarget,
						((shadows) ? sizeShadows : widthWindow),
						((shadows) ? sizeShadows : heightWindow),
						((shadows) ? 1.0f : scaleWindowX),
						((shadows) ? 1.0f : scaleWindowY)
					);
					
					// Bind and clear the camera.
					if (!shadows)
						if (!renderer.refreshCamera
						(
							*pass.camera,
							widthWindow,
							heightWindow,
							scaleWindowX,
							scaleWindowY
						))
							continue;
					
					if (!renderer.bindCamera(*pass.camera))
						continue;

/// @todo: Quick hack to get GUI back before redesigning pipelines with renderbuffers.
if (ModeCamera::GUI == pass.camera->mode())
	renderer.clear();
else {
					/*switch (curr.step.clear)
					{
						case Step::Clear::camera: renderer.clearCamera(*pass.camera); break;
						case Step::Clear::colour:*/ renderer.clear(); /*break;
					}*/

					for (std::size_t j{0}; j < curr.step.buffers.size(); ++ j)
					{
						auto
							&buffer(curr.step.buffers[j]);
						
						switch (buffer.type)
						{
							case TypeBuffer::R:
							case TypeBuffer::RG:
							case TypeBuffer::RGB:
							case TypeBuffer::RGBA:
							{
								switch (buffer.clear)
								{
									case ModeClear::colour:
										curr.rendertarget->clearTextureColour(j, buffer.clearcolour);
										break;
									
									case ModeClear::camera:
										curr.rendertarget->clearTextureColour(j, pass.camera->clearcolour());
										break;
								}
								
								break;
							}
							
							case TypeBuffer::data:
								curr.rendertarget->clearTextureData(j, buffer.cleardata);
								break;
						}
					}

} /// @todo: Quick hack to get GUI back before redesigning pipelines with renderbuffers.
					if (i > 0 && material)
					{
						material->inputs.clearInputs();
						
						for (std::size_t j{0}; j < i; ++ j)
							if (m_steps[j].step.active
							//! @todo: Quick hack, should not check if GUI
							//|| ModeCamera::GUI == pass.camera->mode()
							)
								updateStepInput(m_steps[j], *material);
						
						material->inputs.append(curr.inputs);
					}
					
					if (Step::Type::lights == curr.step.type && material)
						updateStepForLights(pass, *material);
					
if (material)
{
	if (transform)
	{
	material->inputs.input("_posCam", transform->global().position);
	material->inputs.input("_dirCam", transform->global().forward());
	}
	if (camera)
	{
	material->inputs.input("_near", camera->projection().near);
	material->inputs.input("_far", camera->projection().far);
	}
}

					// Check what to draw.
					switch (curr.step.type)
					{
						// Draw a list of 3D objects.
						case Step::Type::groups:
						case Step::Type::shadows:
						{
							// TODO: Uncomment when octree bugs are fixed.
							ecs::Octree const
								*const octree{nullptr};//app.octree()};
							
							Frustum const
								frustum{pass.camera->frustum(*pass.transformCamera)};
							
							renderer.renderGroups
							(
								pass,
								octree,
								&frustum,
								inputs,
								widthWindow,
								heightWindow,
								scaleWindowX,
								scaleWindowY,
								material,
								(Step::Type::shadows == m_steps[i].step.type)
							);
							break;
						}
						
						// Draw a fullscreen quad.
						case Step::Type::quad:
						case Step::Type::lights:
						{
							if (material)
							{
								mth::Mat4f const
									view {pass.camera->matrixView(*pass.transformCamera)},
									&proj{pass.camera->matrixProjection()};
								
								material->inputs.input("_view", view);
								material->inputs.input("_proj", proj);
								material->inputs.input("_viewInv", mth::inverse(view));
								material->inputs.input("_projInv", mth::inverse(proj));
							}
							
							renderer.renderQuadFullscreen
							(
								pass,
								inputs,
								widthWindow,
								heightWindow,
								scaleWindowX,
								scaleWindowY,
								*material,
								app.res().get<res::Shader>(material->shader) /// @todo: Cache outside loop?
							);
							break;
						}
					}
					
//					unbindStep(*curr.rendertarget);
					
					pass.camera = camera;
					pass.transformCamera = transform;
					
					glDisable(GL_DEPTH_CLAMP); //! @todo: Remove OpenGL code when done testing
				}

				// Bind that special final step.
				bindStepOutput
				(
					*pass.camera,
					*m_stepFinal.data.rendertarget,
					widthWindow,
					heightWindow,
					scaleWindowX,
					scaleWindowY
				);
/*
				renderer.clear();
				
				// Update inputs using the previous step.

				m_stepFinal.material.inputs.clearInputs();
				
/// @todo: Quick hack to get GUI back before redesigning pipelines with renderbuffers.
if (ModeCamera::GUI == pass.camera->mode())
	updateStepInput(m_steps[0], m_stepFinal.material);
else
				for (std::size_t j{0}; j < m_steps.size(); ++ j)
					updateStepInput(m_steps[j], m_stepFinal.material);
				
				// If the camera has an effect, use that shader.
				res::Material *const material{app.res().get<res::Material>(pass.camera->effect())};
				res::Shader *const shader{(material) ? app.res().get<res::Shader>(material->shader) : nullptr};
				
				if (material)
					m_stepFinal.material.inputs.append(material->inputs);
				
				// Render to the buffer of that special final step.
				renderer.renderQuadFullscreen
				(
					pass,
					inputs,
					widthWindow,
					heightWindow,
					scaleWindowX,
					scaleWindowY,
					m_stepFinal.material,
					shader
				);
				*/
				// Unbind that special final step.
				// Now that its buffer contains the output of the
				// final render with the effect applied, we can pass
				// that buffer's texture into the GUI composite step,
				// which is the true final one, straight to screen or
				// camera's own buffer if it has one.
				unbindStep(*m_stepFinal.data.rendertarget);
 
				// If the camera has a buffer, we draw to that. Otherwise to screen.
				if (pass.camera->target())
				{
					pass.camera->target()->bind
					(
						widthWindow,
						heightWindow,
						scaleWindowX,
						scaleWindowY
					);
				}
				else
					usedFramebufferDefault = true;
				
				renderer.clear();

				// This is where the render with effect applied gets passed to composite.
//				for (std::size_t j{0}; j < m_steps.size(); ++ j)
//					updateStepInput(m_steps[j], m_stepFinal.material);
 
				m_stepFinal.material.inputs.clearInputs();
/*				updateStepInput(m_stepFinal.data, m_stepFinal.material);*/
				for (std::size_t j{0}; j < m_steps.size(); ++ j)
				{
					if (m_steps[j].step.active
					//! @todo: Quick hack, should not check if GUI
					//|| ModeCamera::GUI == pass.camera->mode()
					)
					{
						updateStepInput(m_steps[j], m_stepFinal.material);
						
						//! @todo: Quick hack
						if (ModeCamera::GUI == pass.camera->mode() && 0 == j)
							break;
					}
				}
				
				std::uint32_t w, h;
				float sx, sy;
				if (pass.camera->target())
				{
					w = pass.camera->target()->width();
					h = pass.camera->target()->height();
					sx = sy = 1.0f;
				}
				else
				{
					w = widthWindow;
					h = heightWindow;
					sx = scaleWindowX;
					sy = scaleWindowY;
				}
				
				// Composite any GUI on top, unless this itself is the GUI camera.
				/// @todo: Possibly only do if there is no render target.
				/// @todo: Can probably remove all these checks now that a GUI camera is always provided built in...
				if
				(
					ModeCamera::GUI != pass.camera->mode()            &&
					workload.cameraGUI                                &&
					workload.cameraGUI->target()                      &&
					!workload.cameraGUI->target()->textures().empty()
				)
				{
					m_stepFinal.material.inputs.inputTexture
					(
						"_texComposite",
						*workload.cameraGUI->target()->textures()[0]
					);
					
					renderer.compositeQuadsFullscreen
					(
						pass,
						inputs,
						w,
						h,
						sx,
						sy,
						m_stepFinal.material,
						nullptr
					);
				}
				else
				{
					renderer.renderQuadFullscreen
					(
						pass,
						inputs,
						w,
						h,
						sx,
						sy,
						m_stepFinal.material,
						nullptr
					);
				}

				/// @todo: Temporary while figuring out Im3d.
				#if KARHU_EDITOR_SERVER_SUPPORTED
				if (ModeCamera::GUI != pass.camera->mode()){
				if (app::ModeApp::server == app.mode() && app.win().top() && !endedFrameIm3D)
				{
					app.backend().endFrameIm3D(*app.win().top());
					endedFrameIm3D = true;
				}}
				#endif

				if (pass.camera->target())
					pass.camera->target()->unbind();
			}
			
			if (!usedFramebufferDefault)
				renderer.clear();
		}
		
		bool Pipeline::bindStepOutput
		(
			Camera              &camera,
			Rendertarget        &target,
			std::uint32_t const  widthWindow,
			std::uint32_t const  heightWindow,
			float         const  scaleWindowX,
			float         const  scaleWindowY
		)
		{
			// Update the size of the buffer if it does not match the camera's.

			std::uint32_t w, h;

			if (camera.target())
			{
				w = camera.target()->width();
				h = camera.target()->height();
			}
			else
			{
				w = static_cast<std::uint32_t>(std::round(static_cast<float>(widthWindow) * scaleWindowX));
				h = static_cast<std::uint32_t>(std::round(static_cast<float>(heightWindow) * scaleWindowY));
			}
			
			if (!target.valid() || w != target.width() || h != target.height())
			{
				target.size(w, h);
				
				if (!target.bake())
					return false;
			}
			
			// Then bind the buffer for use.
			return target.bind
			(
				widthWindow,
				heightWindow,
				scaleWindowX,
				scaleWindowY
			);
		}

		bool Pipeline::updateStepInput(DataStep &data, res::Material &material)
		{
			/// @todo: Shouldn't these be called something else so that they don't clash with _depthMain/_texMain etc from regular shaders?
		
			// Pass any extra (depth or stencil) buffers into the material and shader.
			if (data.extrabuffer)
			{
				/// @todo: Get uniform name from constant, i.e. move the list of constants out of GL submodule and into the core stuff here?
				if (Step::Type::shadows == data.step.type)
					material.inputs.inputTexture
					(
						(
							data.step.nameExtrabuffer +
							"[" +
							std::to_string(data.step.indexShadowmaps) +
							"]"
						),
						*data.extrabuffer
					);
				else
					material.inputs.inputTexture(data.step.nameExtrabuffer, *data.extrabuffer);
			}
			
			// Pass all regular colour buffers on.
			for (std::size_t i{0}; i < data.textures.size(); ++ i)
				material.inputs.inputTexture(data.step.buffers[i].name, *data.textures[i]);
			
			return true;
		}
		
		bool Pipeline::updateStepForLights(const Pass &pass, res::Material &material)
		{
			thread_local std::string prefix;
			
			//! @todo: Take deactivated shadow steps into account when giving number to shader.
			if (pass.camera)
			{
				std::size_t
					i{0};
				
				std::size_t const
					c(m_datasStepShadow.size());
				
				for (; i < c; ++ i)
				{
					auto const
						&data(m_datasStepShadow[i]);
					
					auto const
						&step(m_steps[data.indexStep]);
					
					std::string const
						suffix{"[" + std::to_string(i) + "]"};
					
					std::uint32_t
						size{0};
					
					if (step.extrabuffer)
						size = step.extrabuffer->width();
					
					material.inputs.input("_viewShadows" + suffix, data.camera.matrixView(data.transform));
					material.inputs.input("_projShadows" + suffix, data.camera.matrixProjection());
					material.inputs.input("_biasShadows" + suffix, step.step.biasShadowmaps);
					material.inputs.input("_sizeShadows" + suffix, size);
				}
				
				for (i = 0; i < c; ++ i)
					material.inputs.input("_cutoffsShadows[" + std::to_string(i) + "]", cutoffShadowmaps(i + 1));
				
				material.inputs.input("_countShadowmaps", m_datasStepShadow.size());
			}
		
			/// @todo: Temporary. There should be a light system.
			{
				const auto lights(app::App::instance()->ecs().componentsInActiveScenes<Light>());
				
				int countDirectional{0}, countPoint{0};
				
				auto &ecs(app::App::instance()->ecs());
				
				for (const Light *const light : lights)
				{
					const auto tr(ecs.componentInEntity<Transform>(light->entity()));
			
					if (!tr || !light->activeInHierarchy())
						continue;
					
					const mth::Vec3f colour
					{
						light->colour().r,
						light->colour().g,
						light->colour().b
					};
					
					switch (light->type())
					{
						case TypeLight::directional:
						{
							if (countDirectional >= 4)
								break;
							
							prefix = "_lightsDirectional[";
							prefix += std::to_string(countDirectional);
							prefix += "].";
							
							material.inputs.input(prefix + "direction", tr->global().forward());
							material.inputs.input(prefix + "colour",    colour);
							material.inputs.input(prefix + "intensity", light->intensity());
							
							++ countDirectional;
							
							break;
						}

						case TypeLight::point:
						{
							if (countPoint >= 64)
								break;
							
							prefix = "_lightsPoint[";
							prefix += std::to_string(countPoint);
							prefix += "].";
						
							material.inputs.input(prefix + "position",  tr->global().position);
							material.inputs.input(prefix + "colour",    colour);
							material.inputs.input(prefix + "intensity", light->intensity());
							material.inputs.input(prefix + "radius",    light->diameter() * 0.5f);
							
							++ countPoint;
							
							break;
						}
					}
				}
				
				material.inputs.input("_countLightsDirectional", countDirectional);
				material.inputs.input("_countLightsPoint", countPoint);
			}
			
			return true;
		}
		
		bool Pipeline::updateStepForShadows
		(
			DataStep         &step,
			Pass             &pass,
			mth::Scalu const  size
		)
		{
			if (!pass.camera)
				return true;
			edt::Editor &editor{app::App::instance()->editor()};
			DataStepShadow
				&data{m_datasStepShadow[step.step.indexShadowmaps]};
			
			// Find the points of the frustum for each cascade.
			{
				std::size_t const
					indexCascade{step.step.indexShadowmaps};
				
				auto const
					planes([this, &pass](std::size_t const i) -> mth::Scalf
					{
						return
						(
							pass.camera->projection().near +
							(pass.camera->projection().far - pass.camera->projection().near) *
							cutoffShadowmaps(i)
						);
					});
				
				mth::Scalf const
					width {static_cast<mth::Scalf>(pass.camera->viewport().x)},
					height{static_cast<mth::Scalf>(pass.camera->viewport().y)},
					FOV   {pass.camera->projection().FOV * 0.5f},
					tanHor{mth::tan(FOV)},
					tanVer{mth::tan(FOV * (height / width))},
					xNear {planes(indexCascade + 0) * tanHor},
					xFar  {planes(indexCascade + 1) * tanHor},
					yNear {planes(indexCascade + 0) * tanVer},
					yFar  {planes(indexCascade + 1) * tanVer},
					plane {planes(indexCascade + 0)},
					planed{planes(indexCascade + 1)-plane},
					max   {mth::max(mth::max(planed * 0.5f, xFar), yFar) * 1.4142135623730951f},
					zf    {plane + planed * 0.5f - max};
				
//				data.frustum[0] = { xNear,  yNear, planes(indexCascade + 0), 1.0f};
//				data.frustum[1] = {-xNear,  yNear, planes(indexCascade + 0), 1.0f};
//				data.frustum[2] = {-xNear, -yNear, planes(indexCascade + 0), 1.0f};
//				data.frustum[3] = { xNear, -yNear, planes(indexCascade + 0), 1.0f};
//				data.frustum[4] = { xFar,   yFar,  planes(indexCascade + 1), 1.0f};
//				data.frustum[5] = {-xFar,   yFar,  planes(indexCascade + 1), 1.0f};
//				data.frustum[6] = {-xFar,  -yFar,  planes(indexCascade + 1), 1.0f};
//				data.frustum[7] = { xFar,  -yFar,  planes(indexCascade + 1), 1.0f};
				
				data.frustum[0] = { max,  max, zf, 1.0f};
				data.frustum[1] = {-max,  max, zf, 1.0f};
				data.frustum[2] = {-max, -max, zf, 1.0f};
				data.frustum[3] = { max, -max, zf, 1.0f};
				data.frustum[4] = { max,  max, zf + max, 1.0f};
				data.frustum[5] = {-max,  max, zf + max, 1.0f};
				data.frustum[6] = {-max, -max, zf + max, 1.0f};
				data.frustum[7] = { max, -max, zf + max, 1.0f};
				/*
				mth::Mat4f const
					transformCam{pass.transformCamera->matrixGlobal()};
				
				for (std::size_t i{0}; i < data.frustum.size(); ++ i)
					data.frustum[i] = transformCam * data.frustum[i];
				*/
				///
/*
				if (0 == step.step.indexShadowmaps)
					editor.pushColour({0.9f, 0.0f, 0.0f, 0.6f});
					else if (1 == step.step.indexShadowmaps)
					editor.pushColour({0.0f, 0.9f, 0.0f, 0.6f});
					else if (2 == step.step.indexShadowmaps)
					editor.pushColour({0.0f, 0.0f, 0.9f, 0.6f});
				editor.pushThickness(10.0f);
				for (int i = 0; i < 4; ++ i)
					editor.lineInViewport(data.frustum[i].xyz(), data.frustum[i + 4].xyz());
				editor.popThickness();
				editor.popColour();
*/
				///
			}
			
			constexpr auto
				calcBoundsFrustum([](Frustumpoints const &frustum) -> mth::Bounds3f
				{
					mth::Vec3f const
						xyz{frustum[0]};
					
					mth::Bounds3f
						bounds{xyz, xyz};
			
					for (std::size_t i{1}; i < frustum.size(); ++ i)
					{
						bounds.min.x = mth::min(bounds.min.x, frustum[i].x);
						bounds.max.x = mth::max(bounds.max.x, frustum[i].x);
						bounds.min.y = mth::min(bounds.min.y, frustum[i].y);
						bounds.max.y = mth::max(bounds.max.y, frustum[i].y);
						bounds.min.z = mth::min(bounds.min.z, frustum[i].z);
						bounds.max.z = mth::max(bounds.max.z, frustum[i].z);
					}
					/*
					mth::Vec3f const
						centre{bounds.centre()},
						size  {bounds.size()};
					
					mth::Scalf const
						max{mth::max(mth::max(size.x, size.y), size.z) * 0.5f};
					
					bounds.min = centre - max;
					bounds.max = centre + max;
					*/
					return bounds;
				});

mth::Bounds3f bounds{calcBoundsFrustum(data.frustum)};
mth::Vec3f const ssize{bounds.size() * 0.5f};
bounds.min = -ssize;
bounds.max = ssize;
mth::Mat4f const
		transformCam{pass.transformCamera->matrixGlobal()};
for (std::size_t i{0}; i < data.frustum.size(); ++ i)
	data.frustum[i] = transformCam * data.frustum[i];
	
			const auto
				lights(app::App::instance()->ecs().componentsInActiveScenes<Light>());
			
			for (const Light *const light : lights)
			{
				if (TypeLight::directional == light->type())
				{
					auto tr(app::App::instance()->ecs().componentInEntity<Transform>(light->entity()));
			
					if (!tr)
						continue;
					
					mth::Vec3f const
						centre
						{
							(
								data.frustum[0] +
								data.frustum[1] +
								data.frustum[2] +
								data.frustum[3] +
								data.frustum[4] +
								data.frustum[5] +
								data.frustum[6] +
								data.frustum[7]
							) / 8.0f
						};
					
					mth::Transformf const
						global{tr->global()};
					
					mth::Transformf t{};
					t.position = centre;
					t.rotation = global.rotation;
					data.transform.local(std::move(t));
					
					data.camera.clearflags(pass.camera->clearflags());
					data.camera.clearstencil(pass.camera->clearstencil());
					data.camera.layers(pass.camera->layers());
					
					///! @todo: Calc shadowcam size properly for cascades
					data.camera.viewport({size, size});

					// Cascade.
					
					mth::Mat4f const
						transformLight{data.camera.matrixView(data.transform)};
					
					Frustumpoints const
						frustum
						{
							transformLight * data.frustum[0],
							transformLight * data.frustum[1],
							transformLight * data.frustum[2],
							transformLight * data.frustum[3],
							transformLight * data.frustum[4],
							transformLight * data.frustum[5],
							transformLight * data.frustum[6],
							transformLight * data.frustum[7]
						};
/*
					mth::Bounds3f const
						bounds{calcBoundsFrustum(frustum)};
					
					///
					
					editor.pushThickness(8.0f);
					if (0 == step.step.indexShadowmaps)
					editor.pushColour({0.8f, 0.0f, 0.0f, 0.8f});
					else if (1 == step.step.indexShadowmaps)
					editor.pushColour({0.0f, 0.8f, 0.0f, 0.8f});
					else if (2 == step.step.indexShadowmaps)
					editor.pushColour({0.0f, 0.0f, 0.8f, 0.8f});
					mth::Vec3f const c{bounds.centre()}, hs{bounds.size() * 0.5f};
					mth::Mat4f const inv{mth::inverse(transformLight)};
					mth::Vec3f ps[8]
					{
						(inv * mth::Vec4f{c.x + hs.x, c.y + hs.y, c.z - hs.z, 1.0f}).xyz(),
						(inv * mth::Vec4f{c.x - hs.x, c.y + hs.y, c.z - hs.z, 1.0f}).xyz(),
						(inv * mth::Vec4f{c.x - hs.x, c.y - hs.y, c.z - hs.z, 1.0f}).xyz(),
						(inv * mth::Vec4f{c.x + hs.x, c.y - hs.y, c.z - hs.z, 1.0f}).xyz(),
						(inv * mth::Vec4f{c.x + hs.x, c.y + hs.y, c.z + hs.z, 1.0f}).xyz(),
						(inv * mth::Vec4f{c.x - hs.x, c.y + hs.y, c.z + hs.z, 1.0f}).xyz(),
						(inv * mth::Vec4f{c.x - hs.x, c.y - hs.y, c.z + hs.z, 1.0f}).xyz(),
						(inv * mth::Vec4f{c.x + hs.x, c.y - hs.y, c.z + hs.z, 1.0f}).xyz()
					};
					for (int i = 0; i < 4; ++ i)
					{
						editor.lineInViewport(ps[i], ps[i + 4]);
						editor.lineInViewport(ps[i], ps[(3 == i) ? 0 : (i + 1)]);
						editor.lineInViewport(ps[i + 4], ps[(3 == i) ? 4 : (i + 5)]);
					}
					editor.popColour();
					editor.popThickness();
					
					editor.pushThickness(15.0f);
					editor.pushColour({1.0f, 1.0f, 0.0f, 0.75f});
					mth::Vec3f const cp{pass.transformCamera->global().position};
					editor.lineInViewport(cp, cp + tr->global().forward() * 20.0f);
					editor.popColour();
					editor.pushColour({1.0f, 0.5f, 0.0f, 0.75f});
					editor.lineInViewport(cp, cp + tr->global().eyeforward() * 20.0f);
					editor.popColour();
					editor.popThickness();
*/
					/*
					if (0 == step.step.indexShadowmaps)
					editor.pushColour({0.8f, 0.0f, 0.0f, 0.8f});
					else if (1 == step.step.indexShadowmaps)
					editor.pushColour({0.0f, 0.8f, 0.0f, 0.8f});
					else if (2 == step.step.indexShadowmaps)
					editor.pushColour({0.0f, 0.0f, 0.8f, 0.8f});
					editor.pushThickness(10.0f);
					for (std::size_t i = 0; i < 4; ++ i)
						editor.lineInViewport(data.frustum[i + 4].xyz(), data.frustum[(3 == i) ? 4 : (i + 5)].xyz());
					editor.popThickness();
					editor.popColour();
					*/
					
					///
					
					/*
					mth::Vec3f const
						centre
						{
							(
								frustum[0] +
								frustum[1] +
								frustum[2] +
								frustum[3] +
								frustum[4] +
								frustum[5] +
								frustum[6] +
								frustum[7]
							) / 8.0f
						};
					*/
					/*
					mth::Vec3f const
						centre
						{
							(
								frustum[0] +
								frustum[1] +
								frustum[2] +
								frustum[3] +
								frustum[4] +
								frustum[5] +
								frustum[6] +
								frustum[7]
							) / 8.0f
						},
						size{bounds.size()};

					mth::Scalf const
						max{mth::max(size.x, mth::max(size.y, size.z)) * 0.5f};

					bounds = {centre - max, centre + max};
					*/
					{
					data.camera.matrixProjection
					(
						mth::projection::orthographic
						(
							bounds.min.x,
							bounds.max.x,
							bounds.min.y,
							bounds.max.y,
							bounds.min.z,
							bounds.max.z
						)
					);
					}
					/// @todo: Way to set main directional light other than just instance order?
					break;
				}
			}
			
			return true;
		}
		
		bool Pipeline::unbindStep(Rendertarget &target)
		{
			return target.unbind();
		}
		
		float Pipeline::cutoffShadowmaps(std::size_t const i) const
		{
			return (0 == i) ? 0.0f : m_steps[m_datasStepShadow[i - 1].indexStep].step.cutoffShadowmaps;
		}
	}
}
