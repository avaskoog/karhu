/**
 * @author		Ava Skoog
 * @date		2017-11-16
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GRAPHICS_WORKLOAD_H_
	#define KARHU_GRAPHICS_WORKLOAD_H_

	#include <karhu/conv/graphics.hpp>
	#include <karhu/res/Material.hpp>
	#include <karhu/res/Shader.hpp>
	#include <karhu/res/Mesh.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/app/ecs/World.hpp>
	#include <karhu/app/gfx/components/Renderdata3D.hpp>
	#include <karhu/app/gfx/components/Renderdata2D.hpp>
	#include <karhu/app/gfx/components/Particlesystem.hpp>
	#include <karhu/conv/maths.hpp>

	#include <vector>
	#include <cstdint>

	namespace karhu
	{
		namespace gfx
		{
			class Transform;
			class Camera;
			class Animator;

			struct Group
			{
				Rendermode    mode;
				bool          dynamic, instanced;
				Layers        layers;
				ModeShadows   modeShadows;

				struct Mesh
				{
					res::Material *material;
					res::Shader   *shader;
					res::Mesh     *mesh;
					Animator      *animator;
					std::size_t    LOD;
				};
				
				struct Sprite
				{
					res::Texture   *texture;
					ColRGBAf const *tint;
					float           UVs[4];
				};
				
				union
				{
					Mesh   mesh;
					Sprite sprite;
				};
				
				std::vector<Instance> instances;
			};

			/// @todo: Is pass separation necessary at all now? Or should the camera just go in the group too?
			struct Pass
			{
				Camera *camera{nullptr};
				Transform *transformCamera{nullptr};
				std::vector<Group> groups;
				
				void sort();
			};

			struct Workload
			{
				std::vector<Pass> passes;
				Camera *cameraGUI{nullptr};
				
				void addFromECS
				(
					app::App &app,
					ecs::World &world,
					ecs::Pool<Renderdata2D> &components,
					const float dt,
					const float dtFixed,
					const float timestep
				);
				
				/// @todo: Should probably be a separate function to decouple ECS and render stuff entirely?
				void addFromECS
				(
					app::App &app,
					ecs::World &world,
					ecs::Pool<Renderdata3D> &components,
					const float dt,
					const float dtFixed,
					const float timestep
				);
				
				void addFromECS
				(
					app::App &app,
					ecs::World &world,
					ecs::Pool<Particlesystem> &components,
					const float dt,
					const float dtFixed,
					const float timestep
				);

				void clear();
				void sort();
				void dump(); /// @todo: Remove this when done testing with it.
				void dump(std::stringstream &);
			};
		}
	}
#endif
