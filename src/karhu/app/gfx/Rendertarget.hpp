/**
 * @author		Ava Skoog
 * @date		2017-11-01
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: We cannot have depth and stencil separately but they have to be combined so do error checking and make the API clearer.

#ifndef KARHU_GRAPHICS_RENDERTARGET_H_
	#define KARHU_GRAPHICS_RENDERTARGET_H_

	#include <karhu/res/Texture.hpp>
	#include <karhu/core/log.hpp>

	#include <cstdint>
	#include <memory>

	namespace karhu
	{		
		namespace gfx
		{
			/**
			 * Abstract base class representing a rendertarget, which can
			 * be specified as a target for rendering to off-screen.
			 */
			class Rendertarget
			{
				public:
					/**
					 * Sets the size to update automatically depending on the
					 * size of the viewport that the program is rendering into.
					 * It will be scaled by the windows scale factors.
					 *
					 * @param set Whether to tie size to viewport.
					 */
					void tieSizeToViewport(bool const set)
					{
						m_tiedToViewport = set;
					}
				
					bool tieSizeToViewport() const noexcept
					{
						return m_tiedToViewport;
					}

					void size(std::uint32_t const width, std::uint32_t const height)
					{
						m_width  = width;
						m_height = height;
					}

					std::uint32_t width()  const noexcept { return m_width; }
					std::uint32_t height() const noexcept { return m_height; }
				
					bool updateSize
					(
						std::uint32_t const,
						std::uint32_t const,
						float const,
						float const
					);

					bool texture(res::Texture &);
					std::vector<res::Texture *> const &textures() noexcept { return m_texturesColour; }

					bool depth(res::Texture &);
					res::Texture *depth() noexcept { return m_textureDepth; }
					res::Texture const *depth() const noexcept { return m_textureDepth; }

					bool stencil(res::Texture &);
					res::Texture *stencil() noexcept { return m_textureStencil; }
					res::Texture const *stencil() const noexcept { return m_textureStencil; }
				
					bool bake();
				
					void clearTextureColour(std::size_t const index, gfx::ColRGBAf const &colour)
					{
						performClearTextureColour(index, colour);
					}
				
					void clearTextureData(std::size_t const index, std::int32_t const data)
					{
						performClearTextureData(index, data);
					}

					bool bind
					(
						std::uint32_t const widthWindow,
						std::uint32_t const heightWindow,
						float const         scaleWindowX,
						float const         scaleWindowY
					);

					bool unbind()
					{
						if (!valid()) return false;
						return performUnbind();
					}

					bool valid() const noexcept { return m_baked; }
				
					virtual ~Rendertarget() = default;

				protected:
					virtual bool performBake()   = 0;
					virtual bool performBind()   = 0;
					virtual bool performUnbind() = 0;
				
					virtual void performClearTextureColour(std::size_t const index, gfx::ColRGBAf const &colour) = 0;
					virtual void performClearTextureData(std::size_t const index, std::int32_t const data) = 0;

				private:
					std::vector<res::Texture *>
						m_texturesColour;
				
					res::Texture
						*m_textureDepth  {nullptr},
						*m_textureStencil{nullptr};
				
					std::uint32_t
						m_width{0},
						m_height{0};
				
					bool
						m_tiedToViewport{false},
						m_baked         {false};
			};
		}
	}
#endif
