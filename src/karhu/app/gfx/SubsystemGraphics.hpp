/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_SUBSYSTEM_GRAPHICS_H_
	#define KARHU_SUBSYSTEM_GRAPHICS_H_

	#include <karhu/app/Subsystem.hpp>
	#include <karhu/app/Bitflagbank.hpp>
	#include <karhu/app/gfx/Renderer.hpp>
	#include <karhu/app/gfx/Pipeline.hpp>
	#include <karhu/app/gfx/Rendertarget.hpp>
	#include <karhu/app/gfx/resources/Mesh.hpp>
	#include <karhu/app/gfx/resources/Texture.hpp>
	#include <karhu/app/gfx/resources/Shader.hpp>
	#include <karhu/app/gfx/components/Camera.hpp>

	#include <karhu/app/karhuimgui.hpp>
	#include <karhu/app/karhuim3d.hpp>

	#include <string>

	namespace karhu
	{
		namespace gfx
		{
			enum class ModeRendersize
			{
				window,
				custom
			};
			
			class Layerbank : public app::Bitflagbank<Layer, Defaultlayers::custom, Defaultlayers::none>
			{
				public:
					char const *label()      override { return "layers"; }
					char const *descriptor() override { return "graphics layer"; }
			};
			
			/**
			 * Base class for a graphics subsystem.
			 *
			 * Derived classes should provide an implementation to carry
			 * out all of the supported graphics operations and to handle
			 * the data and GPU communication (if any) necessary, and so on.
			 *
			 * Nothing should be done before initialisation is explicitly
			 * invoked by a call to init(), and so the constructor should
			 * not do anything. This is so that lightweight instances can
			 * be instantiated in order to fetch information about the
			 * subsystem, such as the name, from virtual method implementations.
			 *
			 * @see SubsystemDynamic for more virtual methods to implement.
			 */
			class SubsystemGraphics : public subsystem::SubsystemDynamic
			{
				public:
					Pipeline &pipeline() noexcept { return m_pipeline; }
					const Pipeline &pipeline() const noexcept { return m_pipeline; }
				
					Workload &workload() noexcept { return m_workload; }
					const Workload &workload() const noexcept { return m_workload; }
				
					ContainerInputs &inputs() noexcept { return m_inputs; }
					const ContainerInputs &inputs() const noexcept { return m_inputs; }
				
					Layerbank &layers() noexcept { return m_layers; }
					Layerbank const &layers() const noexcept { return m_layers; }
				
					void modeRendersize(ModeRendersize const);
					ModeRendersize modeRendersize() noexcept { return m_modeRendersize; }
					void rendersizeCustom(mth::Scalu const width, mth::Scalu const height);
					void rendersizeCustom(mth::Vec2u const &);
					mth::Vec2u const &rendersizeCustom() const noexcept { return m_rendersizeCustom; }
				
					/// @todo: Should probably be a separate function to decouple ECS and render stuff entirely?
					void renderWithPipelineToCameraFromECS
					(
						app::App &app,
						Pipeline &pipeline,
						Camera &camera,
						ecs::World &world,
						const float dt,
						const float dtFixed,
						const float timestep
					);
				
					/**
					 * Tries to initialise the renderer.
					 *
					 * @return Whether initialisation was successful.
					 */
					bool initRenderer()
					{
						m_renderer = performCreateRenderer();
						return m_renderer.get();
					}
				
					Renderer *renderer() const noexcept { return m_renderer.get(); }

					/**
					 * Tries to create a new mesh, returning a unique pointer to be taken over by the receiver.
					 *
					 * @return A pointer to the new mesh, if valid, or null.
					 */
					std::unique_ptr<Mesh> createMesh()
					{
						return performCreateMesh();
					}

					/**
					 * Tries to create a new rendertarget, returning a unique pointer to be taken over by the receiver.
					 *
					 * @return A pointer to the new rendertarget, if valid, or null.
					 */
					std::unique_ptr<Rendertarget> createRendertarget()
					{
						return performCreateRendertarget();
					}
				
					/**
					 * Tries to create a new shader, returning a unique pointer to be taken over by the receiver.
					 *
					 * @param type The shader type; defaults to regular if omitted.
					 *
					 * @return A pointer to the new shader, if valid, or null.
					 */
					std::unique_ptr<Shader> createShader
					(
						const Shader::Type type = Shader::Type::regular
					)
					{
						return performCreateShader(type);
					}
				
					/**
					 * Tries to create a new texture, returning a unique pointer to be taken over by the receiver.
					 *
					 * @param type The texture type; defaults to a regular colour texture if omitted.
					 *
					 * @return A pointer to the new texture, if valid, or null.
					 */
					std::unique_ptr<Texture> createTexture
					(
						const Texture::Type       type   = Texture::Type::colour,
						const Pixelbuffer::Format format = Pixelbuffer::Format::RGB
					)
					{
						return performCreateTexture(type, format);
					}
				
				public:
					virtual app::FuncImGuiDevicesCreate  funcImGuiDevicesCreate()  { return nullptr; }
					virtual app::FuncImGuiDevicesDestroy funcImGuiDevicesDestroy() { return nullptr; }
					virtual app::FuncImGuiFontsCreate    funcImGuiFontsCreate()    { return nullptr; }
					virtual app::FuncImGuiRender         funcImGuiRender()         { return nullptr; }
				
					virtual app::FuncIm3DInit     funcIm3DInit()     { return nullptr; }
					virtual app::FuncIm3DNewFrame funcIm3DNewFrame() { return nullptr; }
					virtual app::FuncIm3DEndFrame funcIm3DEndFrame() { return nullptr; }
					virtual app::FuncIm3DShutdown funcIm3DShutdown() { return nullptr; }
				
				protected:
					virtual std::unique_ptr<Renderer>     performCreateRenderer()     = 0;
					virtual std::unique_ptr<Mesh>         performCreateMesh()         = 0;
					virtual std::unique_ptr<Rendertarget> performCreateRendertarget() = 0;

					virtual std::unique_ptr<Shader> performCreateShader
					(
						const Shader::Type type
					) = 0;
				
					virtual std::unique_ptr<Texture> performCreateTexture
					(
						const Texture::Type       type,
						const Pixelbuffer::Format format
					) = 0;
				
				private:
					std::unique_ptr<Renderer>
						m_renderer;
				
					Workload
						m_workload;
				
					Pipeline
						m_pipeline;
				
					ContainerInputs
						m_inputs;
				
					Layerbank m_layers;
				
					ModeRendersize
						m_modeRendersize{ModeRendersize::window};
				
					mth::Vec2u
						m_rendersizeCustom;
			};

			/**
			 * Use this to provide a list of supported graphics subsystems
			 * to backends and window subsystems.
			 */
			template<class... Subsystems>
			struct ListSubsystemsGraphics : public subsystem::ListSubsystems<SubsystemGraphics, Subsystems...>
			{
			};
		}
	}
#endif
