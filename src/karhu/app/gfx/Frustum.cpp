/**
 * @author		Ava Skoog
 * @date		2021-07-20
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/Frustum.hpp>

namespace karhu
{
	namespace gfx
	{
		bool Frustum::contains(const mth::Vec3f &point) const
		{
			const mth::Vec3f v{point - position};
			
			mth::Scalf pcz, pcx, pcy, aux;
			
			pcz = mth::dot(v, -forward);
			
			if (pcz > far || pcz < near)
				return false;

			pcy = mth::dot(v, up);
			aux = pcz * tangent;
			
			if (pcy > aux || pcy < -aux)
				return false;

			pcx = mth::dot(v, right);
			aux = aux * ratio;
			
			if (pcx > aux || pcx < -aux)
				return false;

			return true;
		}

		bool Frustum::contains
		(
			const mth::Vec3f &point,
			const std::size_t &plane
		) const
		{
			if (plane >= Plane::count)
				return false;
			
			const mth::Vec3f normal{planes[plane]};
			
			return ((planes[plane].w + mth::dot(normal, point)) >= 0.0f);
		}

		bool Frustum::contains(const mth::Bounds3f &b) const
		{
			for (std::size_t i{0}; i < Plane::count; ++ i)
			{
				const mth::Vec3f normal{planes[i]};
				
				mth::Vec3f p{b.min};
				
				if (normal.x >= 0.0f)
					p.x = b.max.x;
				
				if (normal.y >= 0.0f)
					p.y = b.max.y;
				
				if (normal.z >= 0.0f)
					p.z = b.max.z;
				
				if ((planes[i].w + mth::dot(normal, p)) < 0.0f)
					return false;
				
				mth::Vec3f n{b.max};
			
				if (planes[i].x >= 0.0f)
					n.x = b.min.x;
				
				if (planes[i].y >= 0.0f)
					n.y = b.min.y;
				
				if (planes[i].z >= 0.0f)
					n.z = b.min.z;
				
				if ((planes[i].w + mth::dot(normal, n)) <= 0.0f)
					return false;
			}
			
			return true;
		}

		bool Frustum::intersects(const mth::Bounds3f &b) const
		{
			for (std::size_t i{0}; i < Plane::count; ++ i)
			{
				const mth::Vec3f normal{planes[i]};
				
				mth::Vec3f p{b.min};
				
				if (normal.x >= 0.0f)
					p.x = b.max.x;
				
				if (normal.y >= 0.0f)
					p.y = b.max.y;
				
				if (normal.z >= 0.0f)
					p.z = b.max.z;
				
				if ((planes[i].w + mth::dot(normal, p)) < 0.0f)
					return false;
			}
			
			return true;
		}
	}
}
