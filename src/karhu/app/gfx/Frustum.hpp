/**
 * @author		Ava Skoog
 * @date		2021-07-20
 * @copyright   2017-2023 Ava Skoog
 */

//! @todo: Move gfx::Frustum into conv/maths and mth:: instead, make factory or ctor for making one from transform, which is currently in the implementation of Camera::frustum().

#ifndef KARHU_GRAPHICS_FRUSTUM_H_
	#define KARHU_GRAPHICS_FRUSTUM_H_

	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
		namespace gfx
		{
			class Frustum
			{
				public:
					struct Plane { enum { left, right, bottom, top, near, far, count }; };
				
					mth::Scalf tangent, ratio, near, far;
					mth::Vec3f position, forward, right, up;
					mth::Vec4f planes[6];
				
					bool contains(const mth::Vec3f &point) const;
					bool contains(const mth::Vec3f &point, const std::size_t &plane) const;
					bool contains(const mth::Bounds3f &box) const;
					bool intersects(const mth::Bounds3f &box) const;
			};
		}
	}
#endif
