/**
 * @author		Ava Skoog
 * @date		2018-12-29
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gfx/Renderer.hpp>
#include <karhu/app/gfx/components/Animator.hpp>
#include <karhu/app/ecs/Octree.hpp>
#include <karhu/app/App.hpp>

namespace karhu
{
	namespace gfx
	{
		void Renderer::renderGroups
		(
			Pass                 const &      pass,
			ecs::Octree          const *const octree,
			Frustum              const *const frustum,
			gfx::ContainerInputs const &      inputs,
			std::uint32_t        const        widthWindow,
			std::uint32_t        const        heightWindow,
			float                const        scaleWindowX,
			float                const        scaleWindowY,
			res::Material              *const materialOverride,
			bool                 const        shadow
		) const
		{
			// Maximum number before going over to instanced rendering.
			/// @todo: Might want 100s/1000s before instancing? Do some checking.
			/// @todo: Geometry complexity might matter too? Check!
//					const std::size_t countMaxSingle{100000};

			res::Shader const
				*shader    {nullptr},
				*shaderLast{nullptr};
			
			res::Material
				*material    {nullptr},
				*materialLast{nullptr};
			
			int
				instancedLast{-1};
			
			static ContainerInputs
				inputsPerGroup;
			
			#ifdef KARHU_EDITOR
			static ContainerInputs
				inputsPerInstance;
			#endif
			
			if (materialOverride)
			{
				/// @todo: Pass app into this function.
				shader   = app::App::instance()->res().get<res::Shader>(materialOverride->shader);
				material = materialOverride;
			}

			// One group per change in mesh, material and so on.
			// This way we can perform instanced rendering for instances sharing a mesh and material.
			for (const auto &group : pass.groups)
			{
				if
				(
					(shadow && ModeShadows::none == group.modeShadows) ||
					(
						!shadow &&
						(
							ModeShadows::normalOnlyShadow == group.modeShadows ||
						 	ModeShadows::customOnlyShadow == group.modeShadows
						)
					)
				)
					continue;
				
				performResetTexturesamplers();
				
				bool const
					noOverride
					{(
						!materialOverride ||
						(
							shadow                                             &&
							ModeShadows::normal           != group.modeShadows &&
							ModeShadows::normalOnlyShadow != group.modeShadows
						)
					)};
			
				switch (group.mode)
				{
					case Rendermode::mesh:
					{
						if (noOverride)
						{
							shader   = group.mesh.shader;
							material = group.mesh.material;
						}
						
						bindAnimator
						(
							 group.mesh.animator,
							*group.mesh.mesh,
							 group.mesh.LOD,
							*material
						);
						
						break;
					}
					
					case Rendermode::sprite:
					{
						/// @todo: Do regular override shaders work on sprites/transparents?
						if (noOverride)
						{
							shader   = &shaderForQuadOneByOne();
							material = nullptr;
						}
						
						break;
					}
				}
				
				// Simple render of a single mesh.
				if (true)//group.LOD < 1) /// @todo: Figure out when to actually render instanced.
				//if (group.instances.size() <= countMaxSingle)
				{
					if (shaderLast != shader || materialLast != material || instancedLast != group.instanced)
					{
						if (material)
						{
							material->inputs.input("_near", pass.camera->projection().near);
							material->inputs.input("_far", pass.camera->projection().far);
							material->inputs.input("_posCam", pass.transformCamera->global().position);
							material->inputs.input("_dirCam", pass.transformCamera->global().forward());
						}
						
						/// @todo: Or do this last? Or should global overrides be separate?
						if (!group.instanced)
						{
							if (!shader->bindForSingle
							(
								pass,
								group.layers,
								material,
								widthWindow,
								heightWindow,
								scaleWindowX,
								scaleWindowY
							))
								continue;
							
							if (!shader->bindInputsForSingle(inputs))
								continue;
						}
						else
						{
							if (!shader->bindForMultiple
							(
								pass,
								group.layers,
								material,
								widthWindow,
								heightWindow,
								scaleWindowX,
								scaleWindowY
							))
								continue;
							
							if (!shader->bindInputsForMultiple(inputs))
								continue;
						}
					}
					
					shaderLast    = shader;
					materialLast  = material;
					instancedLast = group.instanced;
					
					inputsPerGroup.clearInputs();
					inputsPerGroup.input("_layers", static_cast<int>(group.layers));
					
					bool success{true};
					
					switch (group.mode)
					{
						case Rendermode::mesh:
							if (!group.instanced)
							{
								if (!shader->bindInputsForSingle(material->inputs))
									success = false;
								else if (!group.mesh.mesh->bindForSingle())
									success = false;
							}
							else
							{
								if (!shader->bindInputsForMultiple(material->inputs))
									success = false;
								else if (!group.mesh.mesh->bindForMultiple(group.instances))
									success = false;
							}
							
							break;
						
						case Rendermode::sprite:
							//if (Defaultlayers::GUI == group.layers) /// @todo: Or & operator?
							{
								// Read depth but do not write.
								performSetConditionDepth(ConditionDepth::always);
								performSetDepthmask(false);
							}
						
							/// @todo: Uniform name not hardcoded.
							inputsPerGroup.input("_tint", mth::Vec4f
							{
								group.sprite.tint->r,
								group.sprite.tint->g,
								group.sprite.tint->b,
								group.sprite.tint->a
							});
							
							if (group.sprite.texture)
								/// @todo: Uniform name not hardcoded.
								inputsPerGroup.inputTexture("_texMain", *group.sprite.texture);
							
							break;
					}

					if (!group.instanced)
					{
						if (!shader->bindInputsForSingle(inputsPerGroup))
							success = false;
					}
					else
					{
						if (!shader->bindInputsForMultiple(inputsPerGroup))
							success = false;
					}
					
					if (!success)
						continue;

					if (!group.instanced)
					{
						for (const auto &instance : group.instances)
						{
							if (octree && frustum && !octree->entityIntersectsFrustum(static_cast<ecs::IDEntity>(instance.ID), *frustum))
								continue;
							
							#ifdef KARHU_EDITOR
							/// @todo: Get rid of app singleton from renderer, and existence.
							if (app::ModeApp::server == app::App::instance()->mode())
							{
								inputsPerInstance.input("_entity", instance.ID);
								shader->bindInputsForSingle(inputsPerInstance);
							}
							#endif
							
							shader->bindInputsForSingle(*instance.inputs);
							shader->updateDataForInstanceSingle(instance);
							
							switch (group.mode)
							{
								case Rendermode::mesh:
								{
									const auto &buffer(group.mesh.mesh->buffer());
									
									// Make sure the LOD level is valid.
									if
									(
										group.mesh.LOD >= group.mesh.mesh->countLevelsLOD() ||
										group.mesh.LOD >= buffer.rangesPerLOD.size()
									)
										continue;
									
									if (buffer.geometry.indices.size() == 0)
										performDrawSingleWithoutIndices(buffer);
									else
										performDrawSingleWithIndices(buffer, buffer.rangesPerLOD[group.mesh.LOD]);
									
									break;
								}
								
								case Rendermode::sprite:
								{
									performDrawQuadOneByOne(group.sprite.UVs);
									break;
								}
							}
						}
					}
					else
					{
						#ifdef KARHU_EDITOR
						/// @todo: Get rid of app singleton from renderer, and existence.
						if (app::ModeApp::server == app::App::instance()->mode())
						{
							thread_local std::string uniform;
							
							for (std::size_t i{0}; i < group.instances.size(); ++ i)
							{
								uniform = "_entity[";
								uniform += std::to_string(i);
								uniform += ']';
								
								inputsPerInstance.input(uniform, group.instances[i].ID);
							}
							shader->bindInputsForMultiple(inputsPerInstance);
						}
						#endif
						
						shader->bindInputsForMultiple(*group.instances[0].inputs); /// @todo: Better way of doing this?
						
						// Apply instance data.
						if (!shader->updateDataForInstancesMultiple(group.instances))
							continue;
						
						switch (group.mode)
						{
							case Rendermode::mesh:
							{
								const auto &buffer(group.mesh.mesh->buffer());
					
								// Make sure the LOD level is valid.
								if
								(
									group.mesh.LOD >= group.mesh.mesh->countLevelsLOD() ||
									group.mesh.LOD >= buffer.rangesPerLOD.size()
								)
									continue;
								
								// Draw without index buffer if there is none.
								if (buffer.geometry.indices.size() == 0)
									performDrawMultipleWithoutIndices(buffer, group.instances.size());
								// Draw using the index buffer if there is one.
								else
									performDrawMultipleWithIndices(buffer, buffer.rangesPerLOD[group.mesh.LOD], group.instances.size());
								
								break;
							}
								
							case Rendermode::sprite:
							{
								performDrawQuadsOneByOne(group.instances, group.sprite.UVs);
								break;
							}
						}
					}
				}
				// Instanced render of multiple meshes in one go.
				/*else
				{
					if (!shader->bindForMultiple
					(
						pass,
						group.layers,
						material,
						widthWindow,
						heightWindow,
						scaleWindowX,
						scaleWindowY
					))
						continue;
					
					if (!shader->bindInputsForMultiple(inputs))
						continue;
				
					if (!shader->bindInputsForMultiple(material->inputs))
						continue;
					
					/// @todo: Should per-renderdata inputs be used at all when rendering multiple?
					/// @todo: Fix this section up for sprites too.

					// Bind the mesh.
					if (!group.mesh.mesh->bindForMultiple(group.instances))
						continue;
					
					// Apply instance data.
					if (!shader->updateDataForInstancesMultiple(group.instances))
						continue;
					
					const auto &buffer(group.mesh.mesh->buffer());
		
					// Make sure the LOD level is valid.
					if
					(
						group.mesh.LOD >= group.mesh.mesh->countLevelsLOD() ||
						group.mesh.LOD >= buffer.rangesPerLOD.size()
					)
						continue;
					
					// Draw without index buffer if there is none.
					if (buffer.geometry.indices.size() == 0)
						performDrawMultipleWithoutIndices(buffer, group.instances.size());
					// Draw using the index buffer if there is one.
					else
						performDrawMultipleWithIndices(buffer, buffer.rangesPerLOD[group.mesh.LOD], group.instances.size());
				}*/
			}
			
			performSetDepthmask(true);
		}
		
		bool Renderer::refreshCamera
		(
			Camera              &camera,
			std::uint32_t const &widthWindow,
			std::uint32_t const &heightWindow,
			float         const &scaleWindowX,
			float         const &scaleWindowY
		)
		{
			// If there is no target, use the window dimensions.
			if (!camera.target())
			{
				if (!camera.updateProjection
				(
					widthWindow,
					heightWindow,
					scaleWindowX,
					scaleWindowY
				))
					return false;
			}
			else
			{
				if (!camera.target()->updateSize
				(
					widthWindow,
					heightWindow,
					scaleWindowX,
					scaleWindowY
				))
					return false;
				
				if (!camera.updateProjection
				(
					camera.target()->width(),
					camera.target()->height(),
					1.0f,
					1.0f
				))
					return false;
			}

			return true;
		}
		
		bool Renderer::bindAnimator
		(
			Animator      const *const animator,
			res::Mesh     const &      mesh,
			std::size_t   const &      LOD,
			res::Material       &      material
		) const
		{
			if (!animator || !animator->valid())
			{
				material.inputs.input("_animate", 0);
				return true;
			}

			// Get the geometry at the correct LOD to count jointsets and weightsets.
			if (const auto g = mesh.geometryAtLOD(LOD))
			{
				/// @todo: Can we do joints and weights in a better way than hardcoded numbers?

				int count{0};
				
				if (g->jointsets0.size() > 0)
					++ count;
				
				if (g->jointsets1.size() > 0)
					++ count;
				
				if (g->jointsets2.size() > 0)
					++ count;
				
				material.inputs.input("_animate", count); /// @todo: Again, hardcoded stuff. What do?
				
				if (0 != count)
					/// @todo: Should the string be hardcoded? Should this even be here at all, or is doing joints through material renderer implementation specific?
					material.inputs.input("_joints", animator->matrices().data(), animator->matrices().size());
			}
			else
			{
				material.inputs.input("_animate", 0);
				return false;
			}
			
			return true;
		}
	}
}
