/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_APP_H_
	#define KARHU_APP_APP_H_

	#include <karhu/core/log.hpp>
	#include <karhu/app/backend/Backend.hpp>
	#include <karhu/app/Scriptmanager.hpp>

	#include <karhu/app/win/Window.hpp>
	#include <karhu/app/win/Context.hpp>
	#include <karhu/app/gfx/Renderer.hpp>
	#include <karhu/app/gfx/Rendertarget.hpp>
	#include <karhu/res/Texture.hpp>
	#include <karhu/res/Material.hpp>
	#include <karhu/app/AdapterResourcebank.hpp>
	#include <karhu/app/ecs/World.hpp>
	#include <karhu/app/ecs/Octree.hpp>
	#include <karhu/app/loc/Localiser.hpp>

	#include <karhu/app/edt/support.hpp>

	#if KARHU_EDITOR_SERVER_SUPPORTED
		#include <karhu/app/edt/Editor.hpp>
	#endif

	#include <string>

	////// BEGIN TEMPORARY TEST STUFF

	#include <karhu/app/gfx/components/Camera.hpp>
	#include <karhu/app/gfx/Rendertarget.hpp>

	#include <chrono>
	#include <vector>
	#include <map>

	#include <karhu/app/backend/implementation/audio/SDL/lib/soloud/soloud.h>
	#include <karhu/app/backend/implementation/audio/SDL/lib/soloud/soloud_wav.h>

	////// END TEMPORARY TEST STUFF

	namespace karhu
	{
		namespace app
		{
			class App;
			
			class Hook
			{
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(conv::Serialiser const &);
				
					conv::JSON::Obj *userdata();
					conv::JSON::Obj const *userdata() const;
					conv::JSON::Val *userdata(std::string const &, conv::JSON::Val);
					conv::JSON::Val *userdata(std::string const &);
					conv::JSON::Val const *userdata(std::string const &) const;
				
					template<typename T>
					bool userdataSerialise(std::string const &, T const &);
				
					template<typename T>
					bool userdataDeserialise(std::string const &, T &) const;
				
				public:
					virtual bool registerTypes(App &) = 0;
					virtual bool registerSystemsAndListeners(App &) = 0;
					virtual bool registerScripting(App &) = 0;
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
					virtual bool registerEditors(App &, edt::Editor &) = 0;
					#endif
				
					virtual bool init(App &, res::IDResource const startscene, int argc, char *argv[]) = 0;
				
					virtual void updateBeforeECS(App &, float const dt, float const dtFixed, float const timestep) = 0;
					virtual void updateBeforeRender(App &, float const dt, float const dtFixed, float const timestep) = 0;
					virtual void updateAfterECS(App &, float const dt, float const dtFixed, float const timestep) = 0;
				
					virtual ~Hook() {}
				
				protected:
					virtual void performSerialise(conv::Serialiser &) const = 0;
					virtual void performDeserialise(conv::Serialiser const &) = 0;
				
				private:
					conv::JSON::Val
						m_userdata;
			};
			
			enum class ModeApp : std::int32_t
			{
				standalone,
				client,
				server
			};
			
			class App
			{
				public:
					static App *instance() noexcept;
					static void instance(App *const app) noexcept;

				public:
					App
					(
						const char *caption,
						const char *ext,
						const char *org,
						const char *proj,
						Backend &backend,
					 	Hook &hook
					);

					App(const App &) = delete;
					App(App &&) = delete;
					App &operator=(const App &) = delete;
					App &operator=(App &&) = delete;
				
					const char *caption() const { return m_caption.c_str(); }
				
					Backend &backend() { return *m_backend; }

					gfx::SubsystemGraphics &gfx() { return *m_backend->graphics(); }
					snd::SubsystemAudio &snd() { return *m_backend->audio(); }
					win::SubsystemWindow &win() { return m_backend->windows(); }
					inp::SubsystemInput &inp() { return m_backend->input(); }
					fio::SubsystemFile &fio() { return m_backend->files(); }
					phx::SubsystemPhysics &phx() { return m_backend->physics(); }

					AdapterResourcebank &res() { return *m_res; } /// @todo: Should this return pointer?
					Scriptmanager &scr() noexcept { return m_scripts; }
					ecs::World &ecs() noexcept { return m_ECS; }
					ecs::Octree &octree() noexcept { return m_octree; }
					loc::Localiser &loc() noexcept { return m_loc; }
					void reloadLocalisations(std::string const &locale = {});
				
					ModeApp mode() const noexcept { return m_mode; }

					#ifdef KARHU_EDITOR
						module::SubsystemModule &modules() noexcept { return m_backend->modules(); }
				
						#if KARHU_EDITOR_SERVER_SUPPORTED
						edt::Editor &editor() noexcept { return *m_editor; }
						#endif
					#endif
				
					conv::JSON::Obj *userdataLocal();
					conv::JSON::Obj const *userdataLocal() const;
					conv::JSON::Val *userdataLocal(std::string const &, conv::JSON::Val);
					conv::JSON::Val *userdataLocal(std::string const &);
					conv::JSON::Val const *userdataLocal(std::string const &) const;
					char const *pathUserdataLocal() const;
					bool loadUserdataLocal();
					bool saveUserdataLocal();
				
					template<typename T>
					bool userdataLocalSerialise(std::string const &, T const &);
				
					template<typename T>
					bool userdataLocalDeserialise(std::string const &, T &) const;
				
					/// @todo: userdataGlobal saving to platform-specific application storage.

					bool init(int argc, char *argv[]);
					void run();
				
					void startAudio();
				
					mth::Vec2u rendersize() const;
					mth::Vec2f renderscale() const;

					virtual ~App();
				
				private:
					static void performUpdate(float const dt, float const dtFixed, float const timestep)
					{
					 	c_instance->update(dt, dtFixed, timestep);
					}
				
				private:
					void update(float const dt, float const dtFixed, float const timestep);
					Nullable<res::IDResource> loadStartscene();

				private:
					static App *c_instance;

				private:
					Backend *m_backend{nullptr};
					Hook &m_hook;
				
					bool m_valid{false};
					
					std::string m_caption;
				
					struct
					{
						std::string ext, org, proj;
					} m_identifier;
				
					Scriptmanager m_scripts;
					std::unique_ptr<AdapterResourcebank> m_res;
					ecs::World m_ECS;
					ecs::Octree m_octree;
					loc::Localiser m_loc;
				
					struct CameraGUI
					{
						std::unique_ptr<gfx::Rendertarget> target;
						std::unique_ptr<gfx::Texture>      texture, depth;
					} m_cameraGUI;
				
					/// @todo: Should these be unmanaged unique_ptr too?
					win::Window  *m_window {nullptr};
					win::Context *m_context{nullptr};
				
					std::unique_ptr<res::Material>
						m_materialComposite,
						m_materialDepth,
						m_materialWatercolour,
						m_materialWatercolourEdges;
				
					ModeApp
						m_mode{ModeApp::standalone};
				
					#ifdef KARHU_EDITOR
						#if KARHU_EDITOR_SERVER_SUPPORTED
						std::unique_ptr<edt::Editor> m_editor;
						#endif
				
						static void callbackMessageFromServer(App &, karhu::network::Protocol::TypeMessage const &, conv::JSON::Val const &);
					#endif

					////// BEGIN TEMPORARY TEST STUFF
				
					bool initTemporaryStuff();
				
					std::unique_ptr<gfx::Texture> textext;

					/// @todo: Move SoLoud stuff over into the audio backend.
					SoLoud::Soloud gSoloud; // SoLoud engine
					SoLoud::Wav gWave;      // One wave file
					SoLoud::Wav gOgg;       // One wave file
				
					////// END TEMPORARY TEST STUFF
			};
			
			template<typename T>
			bool Hook::userdataSerialise(std::string const &name, T const &data)
			{
				if (auto const ud = userdata())
				{
					conv::Serialiser
						ser{*ud};
					
					ser << karhuINn(name.c_str(), data);
					return true;
				}
				
				return false;
			}
	
			template<typename T>
			bool Hook::userdataDeserialise(std::string const &name, T &data) const
			{
				if (auto const ud = userdata())
				{
					conv::Serialiser
						ser{*ud};
					
					ser >> karhuOUTn(name.c_str(), data);
					return true;
				}
				
				return false;
			}
	
			template<typename T>
			bool App::userdataLocalSerialise(std::string const &name, T const &data)
			{
				return m_hook.userdataSerialise(name, data);
			}
	
			template<typename T>
			bool App::userdataLocalDeserialise(std::string const &name, T &data) const
			{
				return m_hook.userdataDeserialise(name, data);
			}
		}
	}
#endif
