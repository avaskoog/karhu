/**
 * @author		Ava Skoog
 * @date		2018-03-16
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Should this all be ifdeffed?
/// @todo: Figure out a way to represent parent-child relationships in the scene file (probably just an ID named 'parent').

#ifndef KARHU_APP_TOOL_SERIALISER_ECS_H_
	#define KARHU_APP_TOOL_SERIALISER_ECS_H_

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/res/Bank.hpp>
	#include <karhu/res/Scene.hpp>
	#include <karhu/conv/types.hpp>

	#include <stack>

	namespace karhu
	{
		namespace ecs
		{
			class World;
		}

		namespace app
		{
			class AdapterResourcebank;
			
			namespace tool
			{
				/**
				 * Serialises entities and components.
				 *
				 * This serialiser is specifically for editors and similar tooling
				 * and offers a human-readable JSON interface where editing has been
				 * prioritised as opposed to speed, which a final build should
				 * optimise its serialisation method for.
				 */
				class SerialiserECS
				{
					public:
						enum class OverrideActive : std::uint8_t
						{
							none,
							active,
							inactive
						};
					
					public:
						SerialiserECS(AdapterResourcebank &DB);
					
						bool initWorld(ecs::World &);

						bool serialiseSceneFromWorld
						(
							res::Scene      const &scene,
							ecs::World            &world,
							ecs::IDScene    const  IDScene,
							conv::JSON::Val       &outJSON
						);
					
						Result<ecs::IDEntity> deserialiseSceneToWorldAndGetIDTop
						(
							res::Scene     const &scene,
							ecs::World           &world,
							ecs::IDScene   const  IDScene,
							OverrideActive const  overrideActive = OverrideActive::none
						);

						bool deserialiseSceneToWorld
						(
							res::Scene   const &scene,
							ecs::World         &world,
							ecs::IDScene const  IDScene
						);
					
						bool serialiseEntity
						(
							ecs::IDEntity   const,
							conv::JSON::Obj       &,
							ecs::World            &
						);

						bool deserialiseEntity
						(
							conv::JSON::Obj            const &,
							ecs::World                       &,
							ecs::IDScene               const,
							std::vector<ecs::IDEntity>       &IDs,
							OverrideActive             const  overrideActive = OverrideActive::none
						);

					private:
						AdapterResourcebank &m_DB;
				};
			}
		}
	}
#endif
