/**
 * @author		Ava Skoog
 * @date		2018-03-16
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/tool/SerialiserECS.hpp>
#include <karhu/app/AdapterResourcebank.hpp>
#include <karhu/app/gfx/components/Transform.hpp>
#include <karhu/app/ecs/World.hpp>
#include <karhu/app/App.hpp>
#include <karhu/res/Bank.hpp>
#include <karhu/res/Scene.hpp>

#include <karhu/app/edt/support.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
	#include <karhu/app/edt/UserdataEntity.hpp>
#endif

namespace
{
	using namespace karhu;
	using namespace karhu::app;
	using namespace karhu::app::tool;
	
	/// @todo: Should this be inside an editor ifdef and should it just use the editor userdata type instead?

	struct InfoEntity
	{
		ecs::IDScene scene;
		bool top;
	};

	static thread_local std::stack<InfoEntity> s_infoEntity;
	static bool s_keepPrefabsSeparate{false}; /// @todo: Not sure keep prefabs separate is necessary.
	
	static Nullable<conv::JSON::Val> serialiseOverriddenValue
	(
		conv::JSON::Val const &prefab,
		conv::JSON::Val const &instance
	)
	{
		if (prefab.type() != instance.type())
			return {false};
		
		switch (prefab.type())
		{
			// With objects we only need to serialise the named values that differ.
			case JSON::TypeValue::object:
			{
				conv::JSON::Obj values;
				
				for (auto const &it : *prefab.object())
				{
					auto const
						&key(it.first);
					
					auto const
						&valuePrefab(it.second);
					
					auto const
						valueInstance(instance.object()->get(key));
					
					if (!valueInstance)
						continue;
					
					if (valuePrefab != *valueInstance)
					{
						auto const
							overridden(serialiseOverriddenValue(valuePrefab, *valueInstance));
						
						if (overridden)
							values.push({key, std::move(*overridden)});
					}
				}
				
				// Add values existing only on the instance to the prefab.
				for (auto const &it : *instance.object())
				{
					auto const
						&key(it.first);
					
					auto const
						&valueInstance(it.second);
					
					auto const
						valuePrefab(prefab.object()->get(key));
					
					if (valuePrefab)
						continue;
					
					values.push({key, valueInstance});
				}
				
				if (0 != values.count())
					return {true, std::move(values)};
				
				break;
			}
			
			// Unfortunately, at least for now, arrays like
			// single values have to be overridden in full.
			default:
			{
				if (prefab != instance)
					return {true, instance};
				
				break;
			}
		}
			
		return {false};
	}
	
	static void serialiseOverrides
	(
		res::Scene    const &prefab,
		ecs::World          &world,
		ecs::IDEntity const  instance,
		conv::JSON::Val     &outOverrides
	)
	{
		outOverrides = nullptr;
		
		if (!prefab.entities())
			return;
		
		conv::JSON::Arr r;
		
		// Go through the entities in the prefab.
		for (std::size_t i{0}; i < prefab.entities()->count(); ++ i)
		{
			ecs::IDEntity const
				IDE{instance + static_cast<ecs::IDEntity>(i)};
			
			auto const
				componentsInstance(world.componentsByIDInEntity(IDE));
			
			auto const
				entityPrefab(prefab.entities()->get(i));
			
			if (!entityPrefab || !entityPrefab->object())
				continue;
			
			auto const
				componentsPrefab(entityPrefab->object()->getArray("components"));
			
			if (!componentsPrefab || 0 == componentsPrefab->count())
				continue;
			
			/// @todo: Support adding extra components to prefab override.
			if (componentsInstance.size() != componentsPrefab->count())
				continue;
			
			// Find the components of the corresponding entity in the ECS.
			for (std::size_t j{0}; j < componentsInstance.size(); ++ j)
			{
				auto const
					jt(std::next(componentsInstance.begin(), static_cast<std::ptrdiff_t>(j)));

				const ecs::IDComponent
					IDC(jt->first);

				const ecs::IDTypeComponent
					IDT(jt->second);
				
				auto const
					valuesPrefab(componentsPrefab->get(j));
				
				if (!valuesPrefab || !valuesPrefab->object())
					continue;
				
				conv::JSON::Val
					valuesInstance{conv::JSON::Obj{}};
				
				world.serialiseComponentInEntity(IDC, IDT, IDE, *valuesInstance.object());
				
				auto const
					dataPrefab(valuesPrefab->object()->get("data"));
				
				auto const
					dataInstance(valuesInstance.object()->get("data"));
				
				if
				(
					!dataPrefab                                     ||
					!dataInstance                                   ||
					JSON::TypeValue::object != dataPrefab->type()   ||
					JSON::TypeValue::object != dataInstance->type()
				)
					continue;
				
				auto const
					valuesOverridden(serialiseOverriddenValue(*dataPrefab, *dataInstance));
				
				if (valuesOverridden)
				{
					conv::JSON::Obj
						entry;
					
					entry.push({"entity",    i});
					entry.push({"component", j});
					entry.push({"values",    std::move(*valuesOverridden)});
					
					r.push(std::move(entry));
				}
			}
		}
		
		outOverrides = std::move(r);
	}
}

namespace karhu
{
	namespace app
	{
		namespace tool
		{
			/// @todo: Should we really have this circular AdapterResourcebank dependency? Maybe just take App and call resources() or even just use App::instance()...
			SerialiserECS::SerialiserECS(AdapterResourcebank &DB)
			:
			m_DB{DB}
			{
			}
			
			bool SerialiserECS::initWorld(ecs::World &world)
			{
				/// @todo: Doing this the slow way right now but this should be done by the tool continuously updating a file that keeps track of these values across all scenes so that they do not have to be opened with all their other fluff and looked through.

				std::vector<ecs::IDScene>     IDsUsedScene;
				std::vector<ecs::IDEntity>    IDsUsedEntity;
				std::vector<ecs::IDComponent> IDsUsedComponent;

				auto const scenes(m_DB.getAll<res::Scene>());
				for (auto const &scene : scenes)
				{
					if (scene.second)
					{
						IDsUsedScene.emplace_back(static_cast<ecs::IDScene>(scene.first));

						// Look through the array of entities and get their ID's.
						for (auto const &entity : *scene.second->entities())
						{
							auto const e(entity.object());
							
							if (!e)
								return false;

							auto const IDEntity(e->getInt("id"));
							
							if (!IDEntity)
								return false;

							IDsUsedEntity.emplace_back(static_cast<ecs::IDEntity>(*IDEntity));

							auto const components(e->getArray("components"));
							
							if (!components)
								return false;

							// Look through the list of components and get their ID's.
							for (auto const &component : *components)
							{
								auto const c(component.object());
								
								if (!c)
									return false;

								if (auto const IDComponent = c->getInt("id"))
									IDsUsedComponent.emplace_back(static_cast<ecs::IDComponent>(*IDComponent));
								else
									return false;
							}
						}
					}
				}
				
				return world.init
				(
					IDsUsedScene,
					IDsUsedEntity,
					IDsUsedComponent
				);
			}

			bool SerialiserECS::serialiseSceneFromWorld
			(
				res::Scene      const &scene,
				ecs::World            &world,
				ecs::IDScene    const  IDScene,
				conv::JSON::Val       &outJSON
			)
			{
				auto const entities(world.entitiesInScene(IDScene));
				
				conv::JSON::Val root{conv::JSON::Obj{}};
				auto obj(root.object());
				
				if (!obj)
					return false;
				
				auto arrEntities(obj->set("entities", conv::JSON::Arr{}).array());
				
				if (!arrEntities)
					return false;
				
				std::vector<ecs::IDEntity> entitiesByIndex;
				
				for (ecs::IDEntity const entity : entities)
				{
					conv::JSON::Obj e;
					
					/// @todo: Should this be dependent on editor anyway? Maybe we should just have a userdata thing that keeps track of this stuff either way, might have uses at runtime…
					#if KARHU_EDITOR_SERVER_SUPPORTED
					bool destroyed{false};
					
					world.forEachParentOfEntity
					(
						entity,
						[&world, &destroyed](ecs::IDEntity const ID) -> bool
						{
							auto data{static_cast<edt::UserdataEntity const *const>
							(
								world.entityUserdata(ID)
							)};
							
							if (data && data->destroyed)
							{
								destroyed = true;
								return false;
							}
							
							return true;
						},
						true
					);
					
					// Do not save destroyed entities.
					if (destroyed)
						continue;
					
					auto data{static_cast<edt::UserdataEntity const *const>
					(
						world.entityUserdata(entity)
					)};
					
					if (data && 0 != data->scene && scene.identifier() != data->scene)
					{
						/// Ignore prefab children completely.
						if (data->top)
						{
							e.set("scene", data->scene);
							
							// Compare to the original prefab to check for overrides.
							auto const
								ref(m_DB.get<res::Scene>(data->scene));
							
							if (ref)
							{
								if (0 != ref->entities()->count())
								{
									auto const
										entityPrefab(ref->entities()->get(0)->object());
									
									if (entityPrefab)
									{
										auto const
											namePrefab(entityPrefab->getString("name"));
										
										auto const
											nameEntity(world.entityName(entity));
										
										if (!namePrefab || *namePrefab != nameEntity)
											e.set("name", nameEntity);
									}
								}
				
								conv::JSON::Val
									overrides;
								
								serialiseOverrides(*ref, world, entity, overrides);
								
								if (JSON::TypeValue::array == overrides.type())
								 	e.set("overrides", std::move(overrides));
							}
							
							arrEntities->push(std::move(e));
							entitiesByIndex.emplace_back(entity);
						}
					}
					else
					#endif
					if (serialiseEntity(entity, e, world))
					{
						arrEntities->push(std::move(e));
						entitiesByIndex.emplace_back(entity);
					}
					else
						return false;
				}
				
				// Parent-child relationships.
				if (auto const children = world.childrenInScene(IDScene))
				{
					conv::JSON::Arr arr;
					
					for (auto const &it : *children)
					{
						auto const
							parent(std::find
							(
								entitiesByIndex.begin(),
								entitiesByIndex.end(),
								it.first
							)),
							child(std::find
							(
								entitiesByIndex.begin(),
								entitiesByIndex.end(),
								it.second
							));
						
						if (parent != entitiesByIndex.end() && child != entitiesByIndex.end())
						{
							conv::JSON::Arr pair;
							pair.push(std::distance(entitiesByIndex.begin(), parent));
							pair.push(std::distance(entitiesByIndex.begin(), child));
							arr.push(std::move(pair));
						}
					}
					
					if (0 != arr.count())
						obj->set("parents", std::move(arr));
				}
				
				outJSON = std::move(root);
				
				return true;
			}
			
			bool SerialiserECS::serialiseEntity
			(
				ecs::IDEntity   const  IDEntity,
				conv::JSON::Obj       &e,
				ecs::World            &world
			)
			{
				auto name(world.entityName(IDEntity));
				
				if (!name.empty())
					e.set("name", std::move(name));
				
				{
					ecs::TagsEntity const tags{world.entityTags(IDEntity)};
					
					if (0 != tags)
						e.set("tags", tags);
				}
				
				if (!world.entityActiveSelf(IDEntity))
					e.set("active", false);
				
				auto const components(world.componentsByIDInEntity(IDEntity));
				
				#if KARHU_EDITOR_SERVER_SUPPORTED
				auto data{static_cast<edt::UserdataEntity const *const>
				(
					world.entityUserdata(IDEntity)
				)};
				
				if (data)
				{
					conv::JSON::Obj ud;
					conv::Serialiser ser{ud};
					data->serialise(ser);
					
					if (0 != ud.count())
						e.set("ud", std::move(ud));
				}
				#endif
				
				if (0 != components.size())
				{
					auto arrComponents(e.set("components", conv::JSON::Arr{}).array());
					
					if (!arrComponents)
						return false;
					
					for (auto const &it : components)
					{
						#if KARHU_EDITOR_SERVER_SUPPORTED
						if (data)
						{
							auto const jt(data->components.find(it.first));
							
							// Do not save destroyed components.
							if (jt != data->components.end() && jt->second.destroyed)
								continue;
						}
						#endif
						
						conv::JSON::Obj c;
						
						if (world.serialiseComponentInEntity(it.first, it.second, IDEntity, c))
							arrComponents->push(std::move(c));
						else
							return false;
					}
				}
				
				return true;
			}
			
			Result<ecs::IDEntity> SerialiserECS::deserialiseSceneToWorldAndGetIDTop
			(
				res::Scene     const &scene,
				ecs::World           &world,
				ecs::IDScene   const  IDScene,
				OverrideActive const  overrideActive
			)
			{
				// Deserialise all the entities first, while keeping
				// track of their assigned ID numbers by index.
				
				std::vector<ecs::IDEntity> IDs;
				
				{
					bool
						first{true};
					
					auto const
						IDSceneCurr(static_cast<ecs::IDScene>(scene.identifier()));
					
					s_infoEntity.emplace(InfoEntity{IDSceneCurr, (2 > s_infoEntity.size())});
					
					for (auto const &entity : *(scene.entities()))
					{
						if (auto const e = entity.object()) /// @todo: Error otherwise?
						{
							/// @todo: Should there be editor ifdef around all the prefab stuff?
							/// @todo: Rename prefab to scene since it will store the main scene ID too, and set it not just for the first entity but for all of them, but do set something like a parent/top flag for the first one?
							
							if (!first)
								s_infoEntity.top().top = false;
							
							if (!deserialiseEntity(*e, world, IDScene, IDs, ((first) ? overrideActive : OverrideActive::none)))
							{
								s_infoEntity.pop();
								return {{}, false};
							}
							
							first = false;
						}
					}
				}
				
				s_infoEntity.pop();

				/// @todo: This shouldn't be an error, just return an empty scene. Validate some other way.
//				if (0 == IDs.size())
//				{
//					log::err("Karhu")
//						<< "Error deserialising scene: "
//						   "scene is empty";
//
//					return {{}, false};
//				}
				
				// Connect all parents and children.
				/// @todo: Make this work with prefabs.
				for (auto const &relationship : *(scene.parents()))
				{
					if (auto const pair = relationship.array())
					{
						if (2 != pair->count())
							continue;
						
						auto const parent(pair->get(0)->integer());
						auto const child (pair->get(1)->integer());
						
						if (!parent || !child)
							continue;
						
						auto const indexParent(static_cast<std::size_t>(*parent));
						auto const indexChild (static_cast<std::size_t>(*child));
						auto const count      (scene.entities()->count());
						
						if (indexParent >= count || indexChild  >= count)
							continue;
						
						if (!world.entityParentOf(IDs[indexParent], IDs[indexChild]))
						{
							log::err("Karhu")
								<< "Error deserialising scene: "
								   "failed to set up parent-child relationship between entities "
								<< indexParent
								<< " and "
								<< indexChild;
							
							return {{}, false};
						}
					}
				}
				
				for (auto &c : world.componentsInScene<gfx::Transform>(IDScene))
					c->refresh(world);
				
				return {IDs[0], true};
			}
			
			bool SerialiserECS::deserialiseSceneToWorld
			(
				res::Scene   const &scene,
				ecs::World         &world,
				ecs::IDScene const  IDScene
			)
			{
				auto const r(deserialiseSceneToWorldAndGetIDTop(scene, world, IDScene));
				return r.success;
			}
			
			bool SerialiserECS::deserialiseEntity
			(
				conv::JSON::Obj            const &e,
				ecs::World                       &world,
				ecs::IDScene               const  IDScene,
				std::vector<ecs::IDEntity>       &IDs,
				OverrideActive             const  overrideActive
			)
			{
				/// @todo: In editor mode, tag entities from prefabs as such, and their children with yet another tag. Keep track of prefab's original scene ID.
				
				bool active{true};
				OverrideActive overrideActiveScene{OverrideActive::none};
				
				if (OverrideActive::none != overrideActive)
					active = (OverrideActive::active == overrideActive);
				else
				{
					auto const boolActive(e.getBool("active"));
					if (boolActive)
					{
						overrideActiveScene = (*boolActive) ? OverrideActive::active : OverrideActive::inactive;
						active = *boolActive;
					}
				}
				
				auto flags(ecs::CreateEntity::all);
		
				if (!active)
					flags &= ~ecs::CreateEntity::active;
				
				// Is the "entity" just a reference to another scene?
				if (auto const scene = e.getInt("scene"))
				{
					res::Scene *const ref{world.app().res().get<res::Scene>(static_cast<res::IDResource>(*scene))};
					
					if (!ref)
					{
						log::err("Karhu")
							<< "Could not deserialise scene: error deserialising scene reference "
							<< static_cast<int>(*scene)
							<< ": resource not found";
						
						return false;
					}
					else
					{
						ecs::IDScene ID;
						
						if (s_keepPrefabsSeparate)
						{
						 	ID = world.m_genIDScene.next();
							
						 	auto const s(world.createScene(ID));
							
						 	if (!s.success)
							{
								world.m_genIDScene.release(ID);
								
								log::err("Karhu")
									<< "Could not deserialise scene: error creating scene for prefab "
									<< static_cast<int>(*scene)
									<< ": "
									<< s.error;
								
								return false;
							}
						}
						else
							ID = IDScene;
						
						auto const s(deserialiseSceneToWorldAndGetIDTop
						(
							*ref,
							world,
							ID,
							overrideActiveScene
						));
						
						if (!s.success)
						{
							if (s_keepPrefabsSeparate)
								world.m_genIDScene.release(ID);
								
							return false;
						}
						
						IDs.emplace_back(s.value);
						
						if (auto const name = e.getString("name"))
							world.entityName(s.value, *name);
						
						if (auto const overrides = e.getArray("overrides"))
						{
							if (0 != overrides->count())
							{
								for (auto const &entry : *overrides)
								{
									if (auto const o = entry.object())
									{
										auto const
											entity(o->getInt("entity"));
										
										auto const
											component(o->getInt("component"));
										
										auto const values
											(o->getObject("values"));
										
										if (!entity || !component || !values)
											continue;
										
										auto const
											IDE(s.value + static_cast<ecs::IDEntity>(*entity));
										
										auto const
											map(world.componentsByIDInEntity(IDE));
										
										auto const
											it(std::next(map.begin(), *component));
										
										const ecs::IDComponent
											IDC(it->first);
										
										const ecs::IDTypeComponent
											IDT(it->second);
										
										world.deserialiseExistingComponentInEntity(IDE, IDC, IDT, *values);
									}
								}
							}
						}
					}
				}
				else
				{
					ecs::IDEntity const ID  {world.m_genIDEntity.next()};
					auto          const name(e.getString("name"));
					auto          const tags(e.getInt   ("tags"));
					
					auto const s(world.createEntityInScene
					(
						ID,
						IDScene,
						flags,
						(name) ? *name : std::string{}
					));
					
					if (!s.success)
					{
						log::err("Karhu")
							<< "Could not deserialise scene: error deserialising entity "
							<< static_cast<std::uint64_t>(ID)
							<< ": "
							<< s.error;
						
						return false;
					}
					
					#if KARHU_EDITOR_SERVER_SUPPORTED
					if (!s_infoEntity.empty())
					{
						auto
							data(std::make_unique<edt::UserdataEntity>());
						
						data->scene = static_cast<ecs::IDScene>(s_infoEntity.top().scene);
						data->top   = s_infoEntity.top().top;
						
						if (auto const ud = e.getObject("ud"))
						{
							conv::Serialiser ser{*ud};
							data->deserialise(ser);
						}
						
						world.entityUserdata(ID, std::move(data));
					}
					#endif
					
					if (auto const components = e.getArray("components"))
						for (auto const &component : *components)
							if (auto const c = component.object()) /// @todo: Error otherwise?
								if (!world.deserialiseComponentInEntity(ID, *c))
									return false;
					
					IDs.emplace_back(ID);
					
					if (tags)
						world.entityTags(ID, static_cast<ecs::TagsEntity>(*tags));
				}
				
				return true;
			}
		}
	}
}
