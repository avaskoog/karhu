/**
 * @author		Ava Skoog
 * @date		2018-11-01
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/AdapterResourcebank.hpp>
#include <karhu/app/gfx/resources/Rig.hpp>
#include <karhu/app/gfx/resources/Animation.hpp>
#include <karhu/app/gfx/resources/Material.hpp>
#include <karhu/app/gfx/resources/Font.hpp>
#include <karhu/app/App.hpp>

/// @todo: Update stuff to use the new save/load/de/serialise for resources.

namespace karhu
{
	namespace app
	{
		AdapterResourcebank::AdapterResourcebank
		(
			std::unique_ptr<conv::AdapterFilesystemRead>  &&read,
			std::unique_ptr<conv::AdapterFilesystemWrite> &&write
		)
		:
		res::Bank{std::move(read), std::move(write)},
		m_serECS {*this}
		{
			registerDerivedType<res::Rig,       gfx::Rig>();
			registerDerivedType<res::Animation, gfx::Animation>();
			registerDerivedType<res::Material,  gfx::Material>();
			registerDerivedType<res::Font,      gfx::Font>();
		}
	}
}
