/**
 * @author		Ava Skoog
 * @date		2018-03-16
 * @copyright	2017-2018 Ava Skoog
 */

/*
 * This file just includes all component types.
 */

#ifndef KARHU_APP_COMPONENTS_H_
	#define KARHU_APP_COMPONENTS_H_

	#include <karhu/app/graphics/components/Animator.hpp>
	#include <karhu/app/graphics/components/Camera.hpp>
	#include <karhu/app/graphics/components/Renderdata.hpp>
	#include <karhu/app/graphics/components/Transform.hpp>

	#include <karhu/app/audio/components/Audiosource.hpp>
#endif
