/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_FILE_SUBSYSTEM_FILE_H_
	#define KARHU_FILE_SUBSYSTEM_FILE_H_

	#include <karhu/app/Subsystem.hpp>
	#include <karhu/conv/files.hpp>
	#include <karhu/core/Buffer.hpp>
	#include <karhu/core/Byte.hpp>
	#include <karhu/core/Nullable.hpp>

	#include <string>
	#include <memory>

	namespace karhu
	{
		namespace fio
		{
			class SubsystemFile : public subsystem::Subsystem
			{
				public:
					class AdapterRead : public conv::AdapterFilesystemRead
					{
						private:
							AdapterRead(SubsystemFile &filesystem) : m_filesystem{filesystem} {}
				
						public:
							AdapterRead() = delete;
							AdapterRead(AdapterRead &&) = default;
							AdapterRead(AdapterRead const &) = default;
							AdapterRead &operator=(AdapterRead &&) = default;
							AdapterRead &operator=(AdapterRead const &) = default;
				
							Result<std::string> readStringFromResource
							(
								char const *filepathRelativeToResourcepath
							) override;
				
							Result<Buffer<Byte>> readBytesFromResource
							(
								char const *filepathRelativeToResourcepath
							) override;
				
						private:
							SubsystemFile &m_filesystem;
				
						friend class SubsystemFile;
					};
				
					class AdapterWrite : public conv::AdapterFilesystemWrite
					{
						private:
							AdapterWrite(SubsystemFile &filesystem) : m_filesystem{filesystem} {}
				
						public:
							AdapterWrite() = delete;
							AdapterWrite(AdapterWrite &&) = default;
							AdapterWrite(AdapterWrite const &) = default;
							AdapterWrite &operator=(AdapterWrite &&) = default;
							AdapterWrite &operator=(AdapterWrite const &) = default;
						
							Status writeStringToResource
							(
								char                      const *filepathRelativeToResourcepath,
								char                      const *data,
								conv::ModeFilesystemWrite const  mode = conv::ModeFilesystemWrite::truncate
							) override;
						
							Status writeBytesToResource
							(
								char                      const *filepathRelativeToResourcepath,
								Buffer<Byte>              const &data,
								conv::ModeFilesystemWrite const  mode = conv::ModeFilesystemWrite::truncate
							) override;
				
						private:
							SubsystemFile &m_filesystem;
				
						friend class SubsystemFile;
					};
					
				public:
					bool init(int argc, char *argv[], const std::string &basepath);
					void deinit();
				
					const std::string &basepathResources() const noexcept { return m_basepath; }

					Nullable<std::string>  readStringFromResource(const char *filepath);
					Nullable<Buffer<Byte>> readBytesFromResource (const char *filepath);
				
					bool writeStringToResource
					(
						char                      const *filepath,
						char                      const *data,
						conv::ModeFilesystemWrite const  mode = conv::ModeFilesystemWrite::truncate
					);
				
					bool writeBytesToResource
					(
						char                      const *filepath,
						Buffer<Byte>              const &data,
						conv::ModeFilesystemWrite const  mode = conv::ModeFilesystemWrite::truncate
					);
				
					AdapterRead  &adapterRead()  { return m_adapterRead; }
					AdapterWrite &adapterWrite() { return m_adapterWrite; }

					~SubsystemFile();

				public:
					void logVersions() override;
				
				protected:
					/**
					 * Returns the path to the system-specific directory where program resources can be read.
					 *
					 * @return The resource base path.
					 */
					virtual std::string pathForResources() const = 0;
				
					/**
					 * Returns the path to the system-specific directory where program preferences can be read and written.
					 *
					 * @param ext  The application identitfier's extension.
					 * @param org  The application identitfier's organisation name.
					 * @param proj The application identitfier's project name.
					 *
					 * @return The preference base path.
					 */
					virtual std::string pathForPreferences(const char *ext, const char *org, const char *proj) const = 0;
				
					virtual Nullable<std::string>  performReadStringFromResource(const char *filepath) = 0;
					virtual Nullable<Buffer<Byte>> performReadBytesFromResource (const char *filepath) = 0;
				
				private:
					std::string m_basepath;
					bool m_readonly{true};
					AdapterRead m_adapterRead{*this};
					AdapterWrite m_adapterWrite{*this};
			};
		}
	}
#endif
