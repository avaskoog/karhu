/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/fio/SubsystemFile.hpp>
#include <karhu/app/fio/lib/physfs/physfs.h>
#include <karhu/core/log.hpp>
#include <karhu/core/platform.hpp>

#include <memory>

namespace karhu
{
	namespace fio
	{
		bool SubsystemFile::init(int argc, char *argv[], const std::string &basepath)
		{
			if (basepath.length() > 0)
			{
				m_readonly = false;
				m_basepath = basepath;
				m_basepath += "res";
			}
			else
			{
				m_basepath = pathForResources();
				#ifndef KARHU_PLATFORM_ANDROID
					m_basepath += "res.zip";
				#endif
			}
			
			log::dbg("Karhu")
				<< "Resource path set to '"
				<< m_basepath
				<< '\'';

			#ifndef KARHU_PLATFORM_ANDROID
				// PhysicsFS demands a non-null argument.
				#ifndef KARHU_PLATFORM_WEB
				const char *arg{(argc > 0) ? argv[0] : ""};
				#else
				const char *arg{""};
				#endif
			
				if (0 == PHYSFS_init(arg))
				{
					log::err("Karhu") << "Error initialising PhysicsFS: " << PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode());
					return false;
				}
				
				if (0 == PHYSFS_mount(m_basepath.c_str(), "/", 0))
				{
					log::err("Karhu") << "Error mounting resource directory '" << m_basepath << "' in PhysicsFS: " << PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode());
					return false;
				}
			
				if (!m_readonly)
					if (0 == PHYSFS_setWriteDir(m_basepath.c_str()))
						// Keep running anyway so do not return false.
						log::err("Karhu") << "Error setting write directory '" << m_basepath << "' in PhysicsFS: " << PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode());
			#endif

			return true;
		}
		
		void SubsystemFile::deinit()
		{
			#ifndef KARHU_PLATFORM_ANDROID
				PHYSFS_deinit();
			#endif
		}
		
		Nullable<std::string> SubsystemFile::readStringFromResource(const char *filepath)
		{
			// Android already zips its resources so we avoid double zipping them and
			// do not use PhysicsFS at all on there, but just access files directly.
			#ifdef KARHU_PLATFORM_ANDROID
				return performReadStringFromResource(filepath);
			#else
				if (PHYSFS_File *file = PHYSFS_openRead(filepath))
				{
					const PHYSFS_sint64 size{PHYSFS_fileLength(file)};
					if (size > 0)
					{
						std::unique_ptr<char[]> buffer{new char[static_cast<std::size_t>(size + 1)]};
						const PHYSFS_sint64 result{PHYSFS_readBytes(file, &buffer[0], static_cast<PHYSFS_uint64>(size))};
						
						PHYSFS_close(file);
						
						if (result == size)
						{
							buffer[static_cast<std::size_t>(size)] = '\0';
							return {true, buffer.get()};
						}
						else
							log::err("Karhu") << "Error reading file to string '" << filepath << "': " << PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode());
					}
				}
				else
					log::err("Karhu") << "Error opening file for reading to string '" << filepath << "': " << PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode());
			#endif
		
			return {};
		}
		
		Nullable<Buffer<Byte>> SubsystemFile::readBytesFromResource(const char *filepath)
		{
			// Android already zips its resources so we avoid double zipping them and
			// do not use PhysicsFS at all on there, but just access files directly.
			#ifdef KARHU_PLATFORM_ANDROID
				return performReadBytesFromResource(filepath);
			#else
				if (PHYSFS_File *file = PHYSFS_openRead(filepath))
				{
					const PHYSFS_sint64 size{PHYSFS_fileLength(file)};
					if (size > 0)
					{
						auto buffer(std::make_unique<Byte[]>(static_cast<std::size_t>(size)));
						const PHYSFS_sint64 result{PHYSFS_readBytes(file, &buffer[0], static_cast<PHYSFS_uint64>(size))};
						
						PHYSFS_close(file);
						
						if (result == size)
							return {true, Buffer<Byte>{std::move(buffer), static_cast<std::size_t>(size)}};
						else
							log::err("Karhu") << "Error reading file to bytes '" << filepath << "': " << PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode());
					}
					else
						PHYSFS_close(file);
				}
				else
					log::err("Karhu") << "Error opening file for reading to bytes '" << filepath << "': " << PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode());
			#endif

			return {};
		}
		
		bool SubsystemFile::writeStringToResource
		(
			char                      const *filepath,
			char                      const *data,
			conv::ModeFilesystemWrite const  mode
		)
		{
			if (m_readonly)
			{
				log::err("Karhu")
					<< "Error opening file for writing from string '"
					<< filepath
					<< "': "
					<< "filesystem is in readonly mode";
				
				return false;
			}
			
			std::size_t const size(std::strlen(data));
			
			if (0 == size)
				return true;
			
			if
			(
				PHYSFS_File *file =
				(
					(conv::ModeFilesystemWrite::truncate == mode)
					?
						PHYSFS_openWrite(filepath)
					:
						PHYSFS_openAppend(filepath)
				)
			)
			{
				PHYSFS_sint64 const
					result{PHYSFS_writeBytes(file, data, static_cast<PHYSFS_uint64>(size))};
				
				PHYSFS_close(file);
				
				if (result == static_cast<PHYSFS_sint64>(size))
					return true;
				else
					log::err("Karhu")
						<< "Error writing file from string '"
						<< filepath
						<< "': "
						<< PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode());
			}
			else
				log::err("Karhu")
					<< "Error opening file for writing from string '"
					<< filepath
					<< "': "
					<< PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode());
			
			return false;
		}
		
		bool SubsystemFile::writeBytesToResource
		(
			char                      const *filepath,
			Buffer<Byte>              const &data,
			conv::ModeFilesystemWrite const  mode
		)
		{
			if (m_readonly)
			{
				log::err("Karhu")
					<< "Error opening file for writing from string '"
					<< filepath
					<< "': "
					<< "filesystem is in readonly mode";
				
				return false;
			}
			
			auto const size(static_cast<PHYSFS_sint64>(data.size));
			
			if (0 == size)
				return true;
			
			if
			(
				PHYSFS_File *file =
				(
					(conv::ModeFilesystemWrite::truncate == mode)
					?
						PHYSFS_openWrite(filepath)
					:
						PHYSFS_openAppend(filepath)
				)
			)
			{
				PHYSFS_sint64 const result{PHYSFS_writeBytes(file, data.data.get(), data.size)};
				
				PHYSFS_close(file);
				
				if (result == size)
					return true;
				else
					log::err("Karhu")
						<< "Error writing file from string '"
						<< filepath
						<< "': "
						<< PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode());
			}
			else
				log::err("Karhu")
					<< "Error opening file for writing from string '"
					<< filepath
					<< "': "
					<< PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode());
			
			return false;
		}
		
		void SubsystemFile::logVersions()
		{
			PHYSFS_Version v;
			PHYSFS_getLinkedVersion(&v);
			
			log::msg("Karhu")
				<< "PhysicsFS "
				<< static_cast<int>(v.major)
				<< '.'
				<< static_cast<int>(v.minor)
				<< '.'
				<< static_cast<int>(v.patch);
		}
		
		SubsystemFile::~SubsystemFile() {}
		
		Result<std::string> SubsystemFile::AdapterRead::readStringFromResource
		(
			const char *filepathRelativeToResourcepath
		)
		{
			auto r(m_filesystem.readStringFromResource
			(
				filepathRelativeToResourcepath
			));
			
			if (r)
				return {std::move(*r), true};
			
			std::stringstream s;
			s
				<< "Failed to read to string from file '"
				<< filepathRelativeToResourcepath
				<< '\'';
			
			return {{}, false, s.str()};
		}

		Result<Buffer<Byte>> SubsystemFile::AdapterRead::readBytesFromResource
		(
			const char *filepathRelativeToResourcepath
		)
		{
			auto r(m_filesystem.readBytesFromResource
			(
				filepathRelativeToResourcepath
			));
			
			if (r)
				return {std::move(*r), true};
			
			std::stringstream s;
			s
				<< "Failed to read to bytes from file '"
				<< filepathRelativeToResourcepath
				<< '\'';
			
			return {{}, false, s.str()};
		}
		
		Status SubsystemFile::AdapterWrite::writeStringToResource
		(
			char                      const *filepathRelativeToResourcepath,
			char                      const *data,
			conv::ModeFilesystemWrite const  mode
		)
		{
			auto r(m_filesystem.writeStringToResource
			(
				filepathRelativeToResourcepath,
				data,
				mode
			));
			
			if (r)
				return {true};
			
			std::stringstream s;
			s
				<< "Failed to write from string from file '"
				<< filepathRelativeToResourcepath
				<< '\'';
			
			return {false, s.str()};
		}

		Status SubsystemFile::AdapterWrite::writeBytesToResource
		(
			char                      const *filepathRelativeToResourcepath,
			Buffer<Byte>              const &data,
			conv::ModeFilesystemWrite const  mode
		)
		{
			auto r(m_filesystem.writeBytesToResource
			(
				filepathRelativeToResourcepath,
				data,
				mode
			));
			
			if (r)
				return {true};
			
			std::stringstream s;
			s
				<< "Failed to write from bytes from file '"
				<< filepathRelativeToResourcepath
				<< '\'';
			
			return {false, s.str()};
		}
	}
}
