/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_AUDIO_SUBSYSTEM_AUDIO_H_
	#define KARHU_AUDIO_SUBSYSTEM_AUDIO_H_

	#include <karhu/app/Subsystem.hpp>

	namespace karhu
	{
		namespace snd
		{
			/**
			 * Base class for an audio subsystem.
			 *
			 * Derived classes should provide an implementation to carry
			 * out all of the supported audio operations and to handle
			 * the data and and so on.
			 *
			 * Nothing should be done before initialisation is explicitly
			 * invoked by a call to init(), and so the constructor should
			 * not do anything. This is so that lightweight instances can
			 * be instantiated in order to fetch information about the
			 * subsystem, such as the name, from virtual method implementations.
			 *
			 * @see SubsystemDynamic for more virtual methods to implement.
			 */
			class SubsystemAudio : public subsystem::SubsystemDynamic
			{
			};

			/**
			 * Use this to provide a list of supported audio subsystems to backends.
			 */
			template<class... Subsystems>
			struct ListSubsystemsAudio : public subsystem::ListSubsystems<SubsystemAudio, Subsystems...>
			{
			};
		}
	}
#endif
