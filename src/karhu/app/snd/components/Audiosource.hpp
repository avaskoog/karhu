/**
 * @author		Ava Skoog
 * @date		2017-11-20
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_AUDIO_AUDIOSOURCE_H_
	#define KARHU_AUDIO_AUDIOSOURCE_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/ecs/common.hpp>

	namespace karhu
	{
		namespace snd
		{
			struct Audiosource : public ecs::Component
			{
				using ecs::Component::Component;
				
				/// @todo: Serialisation.
				public:
					#if KARHU_EDITOR_SERVER_SUPPORTED
					void editorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) override {}
					bool editorEdit(app::App &, edt::Editor &) override { return false; }
					#endif
			};
		}
	}
#endif
