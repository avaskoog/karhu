/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BACKEND_SUBSYSTEM_H_
	#define KARHU_APP_BACKEND_SUBSYSTEM_H_

	#include <karhu/core/meta.hpp>

	#include <type_traits>
	#include <vector>
	#include <functional>
	#include <memory>

	namespace karhu
	{
		namespace app
		{
			class App;
		}
		
		namespace subsystem
		{
			class Subsystem
			{
				public:
					virtual void logVersions() = 0;
				
					virtual ~Subsystem() = default;
			};
			
			/**
			 * Base class for dynamic subsystems that can be listed so that
			 * multiple types, possible to swap out at runtime, can be provided
			 * when deriving the backend base class and providing an implementation.
			 *
			 * This should not be inherited directly, but rather the derived types
			 * SubsystemGraphics and SubsystemAudio should be derived from in order
			 * to provide implementations for those two specific types of subsystems.
			 */
			class SubsystemDynamic : public Subsystem
			{
				public:
					/**
					 * Checks whether the instance is of the derived dynamic subsystem type specified.
					 *
					 * @return Whether the instance is of the type specified as a template parameter.
					 */
					template<class TSubsystem>
					std::enable_if_t<std::is_base_of<SubsystemDynamic, TSubsystem>{}, bool> is() const
					{
						return dynamic_cast<const TSubsystem *>(this);
					}

				public:
					/**
					 * Initialises the subsystem.
					 *
					 * @return Whether initialisation was successful.
					 */
					bool init(app::App &app) { return performInit(app); }

					/**
					 * Deïntialises the subsystem.
					 *
					 * @return Whether deïnitialisation was successful.
					 */
					bool deinit(app::App &app) { return performDeinit(app); }

				public:
					/**
					 * Returns the name of the subsystem.
					 */
					virtual const char *name() const = 0;
				
				protected:
					/**
					 * Initialises the subsystem.
					 *
					 * @param app The application.
					 *
					 * @return Whether initialisation was successful.
					 */
					virtual bool performInit(app::App &app) = 0;

					/**
					 * Deïnitialises the subsystem.
					 *
					 * @param app The application.
					 *
					 * @return Whether deïnitialisation was successful.
					 */
					virtual bool performDeinit(app::App &app) = 0;
			};

			/**
			 * A list of subsystems derived from the type specified as the first parameter.
			 * You should not use this class directly, but use the derived classes that hide
			 * away the first parameter: ListSubsystemsGraphics and ListSubsystemsAudio.
			 */
			template<class TSubsystem, class... TSubsystems>
			struct ListSubsystems
			{
				// Validate everything.
				static_assert((sizeof...(TSubsystems) > 0), "At least one subsystem has to be specified");
				static_assert(std::is_base_of<SubsystemDynamic, TSubsystem>{}, "Only dynamic subsystems can be listed");
				static_assert(meta::base<TSubsystem, TSubsystems...>(), "All subsystems must be derived from the same base");

				// For ease of use and in case anything changes.
				using Factory = std::function<std::unique_ptr<TSubsystem>()>;
				using Factories = std::vector<Factory>;

				/*
				 * Returns whether the specified types are in the list.
				 */
				template<class First, class... Rest>
				static constexpr std::enable_if_t<(sizeof...(Rest) > 0), bool> contains() noexcept
				{
					return (contains<First>() || contains<Rest...>());
				}

				template<class First>
				static constexpr bool contains() noexcept { return meta::contains<First, TSubsystems...>(); }

				/*
				 * Generates factories to instantiate the types in the list.
				 */
				static Factories generateFactories()
				{
					std::vector<Factory> v{sizeof...(TSubsystems)};
					addFactory<0, TSubsystems...>(v);
					return v;
				}

				private:
					// These are helpers.

					template<std::size_t Index, typename First, class... Rest>
					static std::enable_if_t<(sizeof...(Rest) > 0), void> addFactory(Factories &v)
					{
						addFactory<Index, First>(v);
						addFactory<Index + 1, Rest...>(v);
					}

					template<std::size_t Index, typename First>
					static void addFactory(Factories &v)
					{
						v[Index] = []{ return std::make_unique<First>(); };
					}
			};
		}
	}
#endif
