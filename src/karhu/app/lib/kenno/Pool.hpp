/**
 * @author		Ava Skoog
 * @date		2018-09-22
 * @copyright	2018-2021 Ava Skoog
 */

#ifndef KENNO_POOL_H_
	#define KENNO_POOL_H_

	#include "Result.hpp"
	#include "byte.hpp"

	#include <unordered_map>
	#include <map>
	#include <memory>
	#include <sstream>
	#include <algorithm>
	#include <functional>
	#include <type_traits>
	#include <cstdint>

	namespace kenno
	{
		/**
		 * The datatype of any kind of pool must inherit this
		 * in order to get an identifier.
		 */
		class Poolable
		{
			public:
				using ID = std::uint64_t;
			
			public:
				Poolable(const ID &identifier) : m_identifier{identifier} {}
			
				Poolable(const Poolable &o) = default;
				Poolable(Poolable &&o) = default;
			
				Poolable &operator=(const Poolable &) = default;
				Poolable &operator=(Poolable &&) = default;
			
				virtual ~Poolable() = default;
			
				ID identifier() const noexcept { return m_identifier; }
			
			private:
				ID m_identifier;
			
			friend class Pool;
		};

		/**
		 * A pool contains subpools that are essentially
		 * lists of elements of a particular type that get
		 * registered dynamically along with the type and an
		 * ID number so that they can be accessed at runtime
		 * using input from for example an editor.
		 */
		class Pool
		{
			public:
				using ID = std::uint16_t;

				/**
				 * Contains a pointer to and available element count
				 * of the array containing elements of a certain type.
				 */
				template<typename T>
				struct ViewSubpool
				{
					std::size_t count{0};
					T *data{nullptr};
				};

			private:
				using IDSigned = std::make_signed_t<ID>;
			
				using Data = std::unique_ptr<Byte[]>;
				using Lookup = std::map<Poolable::ID, std::ptrdiff_t>;

				// These are used to generate functions that know the underlying
				// type of a pool in situations where the type is no longer available.
				using FSwap = std::function<void(Data &data, Lookup &lookup, const std::ptrdiff_t &, const std::ptrdiff_t &t)>;
				using FDestruct = std::function<void(Data &, const std::ptrdiff_t &)>;
				using FMove = std::function<void(Data &, Data &, const std::ptrdiff_t &)>;

				struct DataSubpool
				{
					bool growable;

					std::size_t
						typesize,
						count,
						startcount,
						index{0};
					
					FSwap fSwap;
					FDestruct fDestruct;
					FMove fMove;

					Data data;
					Lookup lookup;
				};

			private:
				/*
				 * Used internally to connect an ID to a datatype.
				 */
				template<typename T>
				static IDSigned getOrAddID(const IDSigned identifier = -1)
				{
					static_assert(std::is_base_of<Poolable, T>{}, "Subpool datatype must inherit Poolable!");
					
					static IDSigned value{-1};
					
					if (identifier >= 0)
						value = identifier;
					
					return value;
				}
			
			public:
				template<typename T>
				static void registerType(ID const identifier)
				{
					if (!hasType<T>())
						getOrAddID<T>(static_cast<IDSigned>(identifier));
				}
				
				template<typename T>
				static ID IDOfType()
				{
					static_assert(std::is_base_of<Poolable, T>{}, "Subpool datatype must inherit Poolable!");
					
					const IDSigned identifier{getOrAddID<T>()};
					return (identifier < 0) ? 0 : static_cast<ID>(identifier);
				}
			
				template<typename T>
				static bool hasType()
				{
					static_assert(std::is_base_of<Poolable, T>{}, "Subpool datatype must inherit Poolable!");
					
					const IDSigned identifier{getOrAddID<T>()};
					return (identifier >= 0);
				}

			public:
				template<typename T>
				Status createSubpoolWithCountAndGrowability
				(
					const ID &identifier,
					const std::size_t &count,
					const bool growable
				);
			
				void clearAllSubpools();
				bool clearSubpool(const ID &identifier);
			
				template<typename T>
				bool clearSubpool();
			
				template<typename T>
				ViewSubpool<T> subpool();
			
				template<typename T, typename... Ts>
				Result<T *> createInstance(const Poolable::ID &identifier, Ts &&... argsForConstructor);
			
				template<typename T>
				Result<T *> emplaceInstance(T &&instance);

				bool destroyInstance(const Poolable::ID &identifier, const bool callDestructor = true);

				template<typename T>
				T *instance(const Poolable::ID &identifier);
			
				void *data(const Poolable::ID &identifier);
			
				~Pool();
			
			private:
				Status growSubpool(DataSubpool &);
			
			private:
				std::unordered_map<ID, DataSubpool> m_subpools;
		};
		
		/**
		 * Adds a subpool for a new datatype using the
		 * specified (unique) ID and initial element capacity.
		 *
		 * @param identifier A unique ID number for the datatype.
		 * @param count      The initial maximum number of elements that can be stored.
		 * @param growable   Whether the subpool should increase its size when running out of space.
		 *
		 * @return Whether the pool was successfully created.
		 */
		template<typename T>
		Status Pool::createSubpoolWithCountAndGrowability
		(
			const ID &identifier,
			const std::size_t &count,
			const bool growable
		)
		{
			static_assert(std::is_base_of<Poolable, T>{}, "Subpool datatype must inherit Poolable!");

			if (m_subpools.find(identifier) != m_subpools.end())
				return {false, "Could not create subpool: type already registered"};
			
			DataSubpool p;
			p.growable   = growable;
			p.typesize   = sizeof(T);
			p.count      =
			p.startcount = count;
			p.data       = std::make_unique<Byte[]>(p.count * p.typesize);
			
			if (!p.data)
				return {false, "Could not create subpool: allocation failed"};
			
			p.fSwap = [](Data &d, Lookup &lookup, const std::ptrdiff_t &first, const std::ptrdiff_t &last)
			{
				auto data(reinterpret_cast<T *>(d.get()));
				auto a(data + first);
				auto b(data + last);
			
				*a = std::move(*b);
				b->~T();
				
				auto kt(lookup.find((data + first)->identifier()));
				kt->second = first;
			};
			
			p.fDestruct = [](Data &d, const std::ptrdiff_t &index)
			{
				auto data(reinterpret_cast<T *>(d.get()));
				(data + index)->~T();
			};
			
			p.fMove = [](Data &from, Data &to, const std::ptrdiff_t &index)
			{
				auto a(reinterpret_cast<T *>(from.get()));
				auto b(reinterpret_cast<T *>(to.get()));
				new (b + index) T{std::move(*(a + index))};
				(a + index)->~T();
			};
			
			if (!p.data)
				return {false, "Could not create subpool: buffer allocation failed"};
			
			m_subpools.emplace(identifier, std::move(p));
			getOrAddID<T>(static_cast<IDSigned>(identifier));
			return {true};
		}

		/**
		 * Tries to clear all instances in the specified subpool.
		 *
		 * @return Whether the pool was found and successfully cleared.
		 */
		template<typename T>
		bool Pool::clearSubpool()
		{
			static_assert(std::is_base_of<Poolable, T>{}, "Subpool datatype must inherit Poolable!");
			
			const IDSigned IDPool{getOrAddID<T>()};
			if (IDPool >= 0)
				return clearSubpool(static_cast<ID>(IDPool));
			
			return false;
		}
		
		/**
		 * Returns a view into the array containing the elements
		 * as a wrapper struct with a pointer to the data and the
		 * number of available elements to be iterated over.
		 * The pointer will be null on failure.
		 *
		 * @return The view into the subpool.
		 */
		template<typename T>
		Pool::ViewSubpool<T> Pool::subpool()
		{
			static_assert(std::is_base_of<Poolable, T>{}, "Subpool datatype must inherit Poolable!");

			const IDSigned IDPool{getOrAddID<T>()};
			if (IDPool >= 0)
			{
				auto it(m_subpools.find(static_cast<ID>(IDPool)));
				if (it != m_subpools.end())
				{
					return
					{
						// Index instead of count since we want the actual
						// number of elements in use and not the full capacity.
						it->second.index,
						reinterpret_cast<T *const>(it->second.data.get())
					};
				}
			}
			
			return {};
		}

		/**
		 * Tries to add another instance with the specified ID to
		 * the pool containing elements of the specified type,
		 * forwarding the remaining arguments to its constructor.
		 * Fails if the subpool does not exist or, unless growable,
		 * is full, or if the specified ID is already in use by
		 * another instance.
		 *
		 * @param identifier         A unique ID number for the instance.
		 * @param argsForConstructor Arguments to construct the instance.
		 *
		 * @return A result struct with a pointer to the new instance on success (do not store).
		 */
		template<typename T, typename... Ts>
		Result<T *> Pool::createInstance(const Poolable::ID &identifier, Ts &&... argsForConstructor)
		{
			static_assert(std::is_base_of<Poolable, T>{}, "Subpool datatype must inherit Poolable!");
			
			using namespace std::literals::string_literals;
			
			const IDSigned IDPool{getOrAddID<T>()};
			if (IDPool < 0)
				return {nullptr, false, "Could not create instance: no pool for type"};
			
			// Find the pool.
			auto it(m_subpools.find(static_cast<ID>(IDPool)));
			if (it == m_subpools.end())
				return {nullptr, false, "Could not create instance: no pool for type"};
			
			// Check if the ID is already registered.
			if (it->second.lookup.find(identifier) != it->second.lookup.end())
			{
				std::stringstream err;
				err << "Could not create instance: ID " << static_cast<std::int64_t>(identifier) << " taken";
				return {nullptr, false, err.str()};
			}
			
			// Check that there is space.
			if (it->second.index == it->second.count)
			{
				if (!it->second.growable)
					return {nullptr, false, "Could not create instance: pool full"};
				else
				{
					auto s(growSubpool(it->second));
					if (!s.success)
						return {nullptr, false, "Could not create instance: "s + s.error};
				}
			}

			// Construct the new instance in place.
			auto data(reinterpret_cast<T *>(it->second.data.get()));
			new(data + it->second.index) T{identifier, std::forward<Ts>(argsForConstructor)...};
			
			// Add the new instance to the lookup table.
			it->second.lookup.emplace(identifier, it->second.index);
			
			// Increase the index and return the object.
			return {(data + (it->second.index ++)), true};
		}

		/**
		 * Tries to move (move semantics) an existing instance into
		 * the pool containing elements of the corresponding type.
		 * Fails if the subpool does not exist or, unless growable,
		 * is full, or if the ID of the instance is already in use
		 * by another one. Remember to call destroyInstance() with
		 * the destructor flag set to false on the original pool
		 * where the instance came from or the destructor will get
		 * called on an invalid instance at some point.
		 *
		 * @param instance The instance to be moved into the pool.
		 *
		 * @return A result struct with a pointer to the new instance on success (do not store).
		 */
		template<typename T>
		Result<T *> Pool::emplaceInstance(T &&instance)
		{
			static_assert(std::is_base_of<Poolable, T>{}, "Subpool datatype must inherit Poolable!");
			
			using namespace std::literals::string_literals;
			
			const IDSigned IDPool{getOrAddID<T>()};
			if (IDPool < 0)
				return {nullptr, false, "Could not emplace instance: no pool for type"};

			// Find the pool.
			auto it(m_subpools.find(static_cast<ID>(IDPool)));
			if (it == m_subpools.end())
				return {nullptr, false, "Could not emplace instance: no pool for type"};
			
			// Check that there is space.
			if (it->second.index == it->second.count)
			{
				if (!it->second.growable)
					return {nullptr, false, "Could not emplace instance: pool full"};
				else
				{
					auto s(growSubpool(it->second));
					if (!s.success)
						return {nullptr, false, "Could not create instance: "s + s.error};
				}
			}

			// Check if the ID is already registered.
			const Poolable::ID identifier{instance.identifier()};
			if (it->second.lookup.find(identifier) != it->second.lookup.end())
			{
				std::stringstream err;
				err << "Could not emplace instance: ID " << static_cast<std::int64_t>(identifier) << " taken";
				return {nullptr, false, err.str()};
			}

			// Move the existing instance in place.
			auto data(reinterpret_cast<T *>(it->second.data.get()));
			*(data + it->second.index) = std::move(instance);
			
			// Add the new instance to the lookup table.
			it->second.lookup.emplace(identifier, it->second.index);

			// Increase the index and return the object.
			return {(data + (it->second.index ++)), true};
		}
		
		/**
		 * Tries to find the instance of the specified type and ID.
		 *
		 * @param identifier The unique ID number.
		 *
		 * @return A pointer to the instance on success or null.
		 */
		template<typename T>
		T *Pool::instance(const Poolable::ID &identifier)
		{
			static_assert(std::is_base_of<Poolable, T>{}, "Subpool datatype must inherit Poolable!");

			const IDSigned IDPool{getOrAddID<T>()};
			if (IDPool >= 0)
			{
				auto it(m_subpools.find(static_cast<ID>(IDPool)));
				if (it != m_subpools.end())
				{
					auto jt(it->second.lookup.find(identifier));
					if (jt != it->second.lookup.end())
					{
						auto data(reinterpret_cast<T *>(it->second.data.get()));
						return (data + jt->second);
					}
				}
			}
			
			return nullptr;
		}
	}
#endif
