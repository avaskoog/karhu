/**
 * @author		Ava Skoog
 * @date		2018-09-22
 * @copyright	2018-2021 Ava Skoog
 */

#include "World.hpp"

#include <thread>
#include <iterator>

namespace kenno
{
	/**
	 * Updates the world with all of its systems.
	 * Call once every frame on the main thread.
	 *
	 * @param dt The delta time in seconds since the last frame.
	 */
	void World::update(const float dt, const float dtFixed, const float timestep)
	{
		// Events first in case systems need to
		// catch them to do setup before updating.
 
		for (auto &e : m_typesEvents)
			e.second.fDispatchEvents(m_events, dt, dtFixed, timestep);

		m_events.events.clearAllSubpools();
		
		for (auto &system : m_systems)
			system.second.fUpdate(system.second.system, *this, dt, dtFixed, timestep);
	}
	
	/**
	 * Tries to fetch the name registered for the event type.
	 * Returns an empty string if the type has not been registered.
	 *
	 * @param identifier The unique ID number of the event type.
	 *
	 * @return A string containing the name on success.
	 */
	std::string World::nameOfTypeEvent(const IDTypeEvent identifier) const
	{
		auto const it(m_typesEvents.find(identifier));

		if (it != m_typesEvents.end())
			return it->second.name;
		
		return {};
	}

	/**
	 * Tries to destroy the event listener by the specified ID.
	 *
	 * @param identifier The unique ID number of the event listener.
	 *
	 * @return A struct containing the success state and an error message on failure.
	 */
	Status World::destroyListenerToEvent(const IDListener identifier)
	{
		using namespace std::literals::string_literals;
		
		if (!m_events.listeners.destroyInstance(identifier))
		{
			std::stringstream err;
			err
				<< "Could not destroy event listener "
				<< static_cast<std::int64_t>(identifier)
				<< ": not found";
			return {false, err.str()};
		}
	
		return {true};
	}

	/**
	 * Tries to create a new scene with the specified ID.
	 *
	 * @param identifier The unique ID number for the scene.
	 *
	 * @return A struct containing the success state and an error message on failure.
	 */
	Status World::createScene(const IDScene identifier)
	{
		if (m_scenes.find(identifier) != m_scenes.end())
		{
			std::stringstream err;
			err
				<< "Could not create scene with ID "
				<< static_cast<std::int64_t>(identifier)
				<< ": scene already exists";
			return {false, err.str()};
		}

		auto scene(std::make_unique<DataScene>(identifier));
		if (!scene)
			return {false, "Could not create scene: allocation failed"};
		
		auto it(m_scenes.emplace(identifier, std::move(scene)));
		if (!it.second)
			return {false, "Could not create scene: allocation failed"};

		for (auto &i : m_subpools)
		{
			auto const s(addPoolToScene(*it.first->second, i.first, i.second));
			
			if (!s.success)
			{
				m_scenes.erase(it.first);
				return {false, std::move(s.error)};
			}
		}
		
		m_IDsScenes.emplace(identifier);
		
		return {true};
	}

	/**
	 * Tries to destroy the scene with the specified ID.
	 *
	 * @param identifier The unique ID number of the scene.
	 *
	 * @return A struct containing the success state and an error message on failure.
	 */
	Status World::destroyScene(const IDScene identifier)
	{
		auto it(m_scenes.find(identifier));
		if (it == m_scenes.end())
		{
			std::stringstream err;
			err
				<< "Could not create scene with ID "
				<< static_cast<std::int64_t>(identifier)
				<< ": scene does not exist";
			return {false, err.str()};
		}
		
		if (DataScene::State::loading == it->second->state)
		{
			std::stringstream err;
			err
				<< "Could not create scene with ID "
				<< static_cast<std::int64_t>(identifier)
				<< ": scene is busy ";
			return {false, err.str()};
		}
		
		// No need to clean up entities or components
		// before as the pool goes with the scene.
		m_scenes.erase(it);
		
		auto jt(m_IDsScenes.find(identifier));
		if (jt != m_IDsScenes.end())
			m_IDsScenes.erase(jt);
		
		return {true};
	}
	
	/**
	 * Returns whether the scene by the specified ID
	 * has been loaded, i.e. is not busy with an
	 * asynchronous manipulation. Returns false if the
	 * scene does not exist and so an invalid ID is safe.
	 *
	 * @param identifier The unique ID number of the scene.
	 *
	 * @return Whether the scene has been loaded.
	 */
	bool World::sceneLoaded(const IDScene identifier) const
	{
		auto const it(m_scenes.find(identifier));
		if (it == m_scenes.end())
			return false;
		
		return (DataScene::State::loading != it->second->state);
	}
	
	/**
	 * Returns whether the scene by the specified ID
	 * encountered an error during an asynchronous manipulation.
	 * Returns false if the scene does not exist and so an
	 * invalid ID is safe.
	 *
	 * @param identifier The unique ID number of the scene.
	 *
	 * @return Whether the scene has been loaded with an error.
	 */
	bool World::sceneLoadedWithError(const IDScene identifier) const
	{
		auto const it(m_scenes.find(identifier));
		if (it == m_scenes.end())
			return false;
		
		return (DataScene::State::error == it->second->state);
	}
	
	/**
	 * Tries to set the active state of the scene.
	 *
	 * @param identifier The unique ID number of the scene.
	 * @param set        Whether to set the scene active.
	 *
	 * @return Whether the scene was found and the state successfully set.
	 */
	bool World::sceneActive(const IDScene identifier, const bool set)
	{
		auto it(m_scenes.find(identifier));
		if (it == m_scenes.end())
			return false;
		
		it->second->active = set;
		return true;
	}

	const std::set<IDEntity> *World::grandparentsInScene(const IDScene identifier) const
	{
		auto const it(m_scenes.find(identifier));
		
		if (it == m_scenes.end())
			return nullptr;
		
		return &it->second->grandparents;
	}
	
	const std::map<IDEntity, IDEntity> *World::parentsInScene(const IDScene identifier) const
	{
		auto const it(m_scenes.find(identifier));
		
		if (it == m_scenes.end())
			return nullptr;
		
		return &it->second->parents;
	}
	
	const std::multimap<IDEntity, IDEntity> *World::childrenInScene(const IDScene identifier) const
	{
		auto const it(m_scenes.find(identifier));
		
		if (it == m_scenes.end())
			return nullptr;
		
		return &it->second->children;
	}
	
	/**
	 * Returns whether the scene by the specified ID is
	 * currently active, or false if it does not exist.
	 *
	 * @param identifier The unique ID number of the scene.
	 *
	 * @return Whether the scene, if found, is active.
	 */
	bool World::sceneActive(const IDScene identifier) const
	{
		auto const it(m_scenes.find(identifier));
		if (it == m_scenes.end())
			return false;
		
		return it->second->active;
	}

	/**
	 * Starts up an asynchronous process where the
	 * specified callback gets called while the scene's
	 * state is set to loading, locking the systems from
	 * having access to it, so that a scene can have
	 * entities and components loaded in the background
	 * according to the user's implementation without
	 * freezing the rest of the program, making it
	 * possible to stream level data without load screens,
	 * or at least to show an animated load screen while
	 * waiting for the data.
	 *
	 * @param identifier The unique ID of the scene to manipulate.
	 * @param f          The function to call asynchronously.
	 *
	 * @return A struct containing the success state of whether the asynchronous process was successfully started and an error message on failure.
	 */
	Status World::manipulateSceneAsync
	(
		const IDScene identifier,
		const CallbackManipulateSceneAsync &f
	)
	{
		auto it(m_scenes.find(identifier));
		if (it == m_scenes.end())
		{
			std::stringstream err;
			err
				<< "Could not manipulate scene by ID "
				<< static_cast<std::int64_t>(identifier)
				<< " asynchronously: scene does not exist";
			return {false, err.str()};
		}
		
		if (DataScene::State::loading == it->second->state)
		{
			std::stringstream err;
			err
				<< "Could not manipulate scene by ID "
				<< static_cast<std::int64_t>(identifier)
				<< " asynchronously: scene is busy";
			return {false, err.str()};
		}
		
		it->second->state = DataScene::State::loading;
		it->second->future = std::async
		(
			std::launch::async,
			performManipulateSceneAsync,
			this,
			it->second.get(),
			f
		);
		
		return {true};
	}
	
	/**
	 * Tries to create a new entity with the specified
	 * entity ID in the scene by the specified scene ID.
	 *
	 * @param IDE   The unique ID number of the entity.
	 * @param IDS   The unique ID number of the scene.
	 * @param flags Optional flags for creation (everything turned on by default).
	 * @param name  An optional name for the entity.
	 *
	 * @return A struct containing the success state and an error message on failure.
	 */
	Status World::createEntityInScene
	(
		const IDEntity      IDE,
		const IDScene       IDS,
		const CreateEntity  flags,
		const std::string  &name
	)
	{
		// Validate the scene first since it is faster.
		auto it(m_scenes.find(IDS));
		if (it == m_scenes.end())
		{
			std::stringstream err;
			err
				<< "Could not create entity by ID "
				<< static_cast<std::int64_t>(IDE)
				<< " in scene "
				<< static_cast<std::int64_t>(IDS)
				<< ": scene does not exist";
			return {false, err.str()};
		}
		
		// Make sure the ID is not taken in any scene.
		for (auto &scene : m_scenes)
		{
			auto it(scene.second->entities.find(IDE));
			if (it != scene.second->entities.end())
			{
				std::stringstream err;
				err
					<< "Could not create entity by ID "
					<< static_cast<std::int64_t>(IDE)
					<< ": ID exists in scene "
					<< static_cast<std::int64_t>(scene.first);
				return {false, err.str()};
			}
		}

		auto const active(static_cast<bool>(flags & CreateEntity::active));

		if
		(
			!it->second->entities.emplace
			(
				IDE,
				DataScene::DataEntity
				{
					name,
					active
				}
			).second
		)
		{
			std::stringstream err;
			err
				<< "Could not create entity by ID "
				<< static_cast<std::int64_t>(IDE)
				<< ": allocation failed";
			return {false, err.str()};
		}
		
		it->second->grandparents.emplace(IDE);
		
		queueEvent<EEntityCreated>(IDE, 0, 0);
		
		if (active)
			queueEvent<EEntityActive>(IDE, 0, 0);
		else
			queueEvent<EEntityInactive>(IDE, 0, 0);
		
		auto
			jt (it->second->entities.find(IDE));

		/// @todo: Why was this here?
//		for (auto &component : jt->second.components)
//			setComponentActiveInEntity(component.second.ID, IDE, false, true);

		return {true};
	}
	
	bool World::entityUserdata(const IDEntity identifier, std::unique_ptr<Userdata> &&data)
	{
		// Make sure the ID is not taken in any scene.
		for (auto &scene : m_scenes)
		{
			auto it(scene.second->entities.find(identifier));
			if (it != scene.second->entities.end())
			{
				it->second.data = std::move(data);
				return true;
			}
		}
		
		return false;
	}
	
	Userdata *World::entityUserdata(const IDEntity identifier)
	{
		// Make sure the ID is not taken in any scene.
		for (auto const &scene : m_scenes)
		{
			auto const it(scene.second->entities.find(identifier));
			if (it != scene.second->entities.end())
				return it->second.data.get();
		}
		
		return nullptr;
	}
	
	std::vector<IDEntity> World::entitiesInScene(const IDScene ID) const
	{
		std::vector<IDEntity> r;
		
		auto const it(m_scenes.find(ID));
		
		if (it == m_scenes.end())
			return r;
		
		if (DataScene::State::loading == it->second->state)
			return r;
		
		for (auto const &e : it->second->entities)
			r.emplace_back(e.first);
		
		return r;
	}
	
	std::vector<IDEntity> World::entitiesInActiveScenes() const
	{
		std::vector<IDEntity> r;
		
		for (auto &scene : m_scenes)
		{
			if (DataScene::State::loading == scene.second->state)
				continue;
			
			if (!scene.second->active)
				continue;
			
			for (auto const &e : scene.second->entities)
				r.emplace_back(e.first);
		}
		
		return r;
	}
	
	/**
	 * Tries to destroy the entity with the specified ID.
	 *
	 * @param identifier The unique ID number of the entity.
	 *
	 * @return A struct containing the success state and an error message on failure.
	 */
	Status World::destroyEntity(const IDEntity identifier)
	{
		// Make sure the ID is not taken in any scene.
		for (auto &scene : m_scenes)
		{
			auto it(scene.second->entities.find(identifier));
			if (it != scene.second->entities.end())
			{
				// First remove the components of the entity.
				while (it->second.components.size() > 0)
				{
					auto jt(it->second.components.begin());
					destroyComponentInEntity(jt, it, scene.second->pool);
				}
				
				// Then remove the actual entity.
				scene.second->entities.erase(it);
				
				// If this was a grandparent, remove it.
				{
					auto jt(scene.second->grandparents.find(identifier));
					if (jt != scene.second->grandparents.end())
						scene.second->grandparents.erase(jt);
				}
				
				// Remove any parent connection.
				{
					auto jt(scene.second->parents.find(identifier));
					if (jt != scene.second->parents.end())
					{
						// Remove the child from the parent.
						auto kt(scene.second->children.equal_range(jt->second));
						for (auto lt(kt.first); lt != kt.second; ++ lt)
						{
							if (lt->second == identifier)
							{
								scene.second->children.erase(lt);
								break;
							}
						}
						
						// Remove the parent from the child.
						scene.second->parents.erase(jt);
					}
				}
				
				// Remove the children as well.
				{
					auto jt(scene.second->children.equal_range(identifier));
					if (jt.first != jt.second)
					{
						// Make a copy of the list since the iterator
						// might become invalidated as we recurse.
						std::vector<IDEntity> children(static_cast<std::size_t>(std::distance(jt.first, jt.second)));
						std::size_t i{0};
						for (auto kt(jt.first); kt != jt.second; ++ kt)
							children[i ++] = kt->second;
						
						// Remove the range of children from the lookup table.
						scene.second->children.erase(jt.first, jt.second);
						
						// Recursively remove every child as well.
						for (auto &child : children)
							destroyEntity(child);
					}
				}
				
				queueEvent<EEntityDestroyed>(identifier, 0, 0);
				
				return {true};
			}
		}

		std::stringstream err;
		err
			<< "Could not destroy entity by ID "
			<< static_cast<std::int64_t>(identifier)
			<< ": entity does not exist";
		return {false, err.str()};
	}
	
	bool World::destroyChildrenOfEntity(const IDEntity identifier)
	{
		for (auto &scene : m_scenes)
		{
			auto const it(scene.second->entities.find(identifier));
			if (it != scene.second->entities.end())
			{
				auto const jt(scene.second->children.equal_range(identifier));
				
				if (jt.first != jt.second)
				{
					std::vector<IDEntity> children(static_cast<std::size_t>(std::distance(jt.first, jt.second)));
					std::size_t i{0};
					for (auto kt(jt.first); kt != jt.second; ++ kt)
						children[i ++] = kt->second;
					
					for (const IDEntity child : children)
						destroyEntity(child);
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Tries to move the entity by the specified entity ID
	 * to the scene by the specified scene ID.
	 *
	 * @param IDE The unique ID number of the entity to move.
	 * @param IDS The unique ID number of the target scene.
	 *
	 * @return A struct containing the success state and an error message on failure.
	 */
	Status World::moveEntityToScene(const IDEntity IDE, const IDScene IDS)
	{
		return moveEntityToScene(IDE, IDS, true);
	}

	/**
	 * Tries to set the name of the entity by the specified ID
	 * by copying another string object. Does not need to be unique.
	 *
	 * @param identifier The unique ID number of the entity.
	 * @param name       The name to set.
	 *
	 * @return Whether the entity was found and the name successfully set.
	 */
	bool World::entityName(const IDEntity identifier, const std::string &name)
	{
		for (auto &scene : m_scenes)
		{
			auto it(scene.second->entities.find(identifier));
			if (it != scene.second->entities.end())
			{
				it->second.name = name;
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Tries to set the name of the entity by the specified ID
	 * by moving a string object. Does not need to be unique.
	 *
	 * @param identifier The unique ID number of the entity.
	 * @param name       The name to set.
	 *
	 * @return Whether the entity was found and the name successfully set.
	 */
	bool World::entityName(const IDEntity identifier, std::string &&name)
	{
		for (auto &scene : m_scenes)
		{
			auto it(scene.second->entities.find(identifier));
			if (it != scene.second->entities.end())
			{
				it->second.name = std::move(name);
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Tries to set the name of the entity by the specified ID
	 * from a null-terminated character array. Does not need to be unique.
	 *
	 * @param identifier The unique ID number of the entity.
	 * @param name       The name to set.
	 *
	 * @return Whether the entity was found and the name successfully set.
	 */
	bool World::entityName(const IDEntity identifier, const char *name)
	{
		return entityName(identifier, std::string{name});
	}
	
	/**
	 * Tries to return the name of the entity by the specified ID.
	 *
	 * @param identifier The unique ID number of the entity.
	 *
	 * @return The name of the entity if found, or an empty string.
	 */
	std::string World::entityName(const IDEntity identifier) const
	{
		for (auto &scene : m_scenes)
		{
			auto it(scene.second->entities.find(identifier));
			if (it != scene.second->entities.end())
				return it->second.name;
		}
		
		return {};
	}

	/**
	 * Tries to set the tags of the entity by the specified ID.
	 *
	 * @param identifier The unique ID number of the entity.
	 * @param tags       The tags to set.
	 *
	 * @return Whether the entity was found and the tags successfully set.
	 */
	bool World::entityTags(const IDEntity identifier, const TagsEntity tags)
	{
		for (auto &scene : m_scenes)
		{
			auto it(scene.second->entities.find(identifier));
			if (it != scene.second->entities.end())
			{
				it->second.tags = tags;
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Tries to return the tags of the entity by the specified ID.
	 *
	 * @param identifier The unique ID number of the entity.
	 *
	 * @return The tags of the entity if found, or zero.
	 */
	TagsEntity World::entityTags(const IDEntity identifier) const
	{
		for (auto &scene : m_scenes)
		{
			auto it(scene.second->entities.find(identifier));
			if (it != scene.second->entities.end())
				return it->second.tags;
		}
		
		return 0;
	}
	
	/**
	 * Finds and returns all entities matching the specified tags.
	 *
	 * @param tags The tag, or tags, to match.
	 *
	 * @return A vector containing the identifiers of the entities found.
	 */
	std::vector<IDEntity> World::entitiesByTags(TagsEntity const tags) const
	{
		std::vector<IDEntity> r;
		
		for (auto &scene : m_scenes)
			for (auto const &data : scene.second->entities)
				if (tags & data.second.tags)
					r.emplace_back(data.first);
		
		return r;
	}
	
	/**
	 * Tries to set the active state of the entity by the specified ID.
	 *
	 * @param identifier The unique ID number of the entity.
	 *
	 * @return Whether the operation was successful.
	 */
	bool World::entityActive(const IDEntity identifier, const bool set)
	{
		for (auto &scene : m_scenes)
		{
			auto it(scene.second->entities.find(identifier));
			
			if (it != scene.second->entities.end())
			{
				if (it->second.active != set)
				{
					it->second.active = set;
					
					bool const
						activeInHierarchy {entityActiveInHierarchy(identifier)},
						changedInHierarchy{(set == activeInHierarchy)};
					
					if (changedInHierarchy)
					{
						if (set)
							queueEvent<EEntityActive>(identifier, 0, 0);
						else
							queueEvent<EEntityInactive>(identifier, 0, 0);
					}
					
					for (auto &component : it->second.components)
					{
						auto c(componentInEntity(component.second.ID, identifier));
						
						c->m_activeInHierarchy = activeInHierarchy;
						
						if (changedInHierarchy && c->activeSelf())
						{
							if (set)
								queueEvent<EComponentActive>(identifier, component.first, component.second.ID);
							else
								queueEvent<EComponentInactive>(identifier, component.first, component.second.ID);
						}
					}
					
					forEachChildOfEntity
					(
						identifier,
						[
							this,
							&set,
							&scene = *scene.second
						]
						(const IDEntity ID)
						{
							auto it(scene.entities.find(ID));
							
							if (it != scene.entities.end())
							{
								bool const
									activeInHierarchy {entityActiveInHierarchy(ID)},
									changedInHierarchy{(set == activeInHierarchy)};
								
								if (changedInHierarchy)
								{
									if (set)
										queueEvent<EEntityActive>(ID, 0, 0);
									else
										queueEvent<EEntityInactive>(ID, 0, 0);
								}
								
								for (auto &component : it->second.components)
								{
									auto c(componentInEntity(component.second.ID, ID));
									
									c->m_activeInHierarchy = activeInHierarchy;
									
									if (changedInHierarchy && c->activeSelf())
									{
										if (set)
											queueEvent<EComponentActive>(ID, component.first, component.second.ID);
										else
											queueEvent<EComponentInactive>(ID, component.first, component.second.ID);
									}
								}
							}
							
							return true;
						}
					);
				}
				
				return true;
			}		
		}
		
		return false;
	}
	
	/**
	 * Returns whether the entity by the specified ID is active.
	 * Will return false if any parent scene or entity is not active.
	 *
	 * @param identifier The unique ID number of the entity.
	 *
	 * @return The active state of the entity or false if not found.
	 */
	bool World::entityActiveInHierarchy(const IDEntity identifier) const
	{
		return getEntityActive(identifier, true);
	}
	
	/**
	 * Returns whether the entity by the specified ID is active.
	 * Does not consider whether any parent scene or entity is not active.
	 *
	 * @param identifier The unique ID number of the entity.
	 *
	 * @return The active state of the entity or false if not found.
	 */
	bool World::entityActiveSelf(const IDEntity identifier) const
	{
		return getEntityActive(identifier, false);
	}

	/**
	 * Tries to set the entity specified by the first ID
	 * as the parent of the entity specified by the second.
	 * A parent and a child must share the same scene.
	 *
	 * @param IDParent The unique ID number of the entity to be set as parent.
	 * @param IDChild  The unique ID number of the entity to be set as child.
	 *
	 * @return Whether the relationship has been set up.
	 */
	bool World::entityParentOf(const IDEntity IDParent, const IDEntity IDChild)
	{
		if (IDParent == IDChild)
			return false;
		
		// First make sure both entities exist.
		DataScene::DataEntity *parent{nullptr}, *child{nullptr};
		DataScene *sceneParent{nullptr}, *sceneChild{nullptr};
		for (auto &scene : m_scenes)
		{
			if (!parent)
			{
				auto it(scene.second->entities.find(IDParent));
				if (it != scene.second->entities.end())
				{
					parent = &it->second;
					sceneParent = scene.second.get();
				}
			}
			
			if (!child)
			{
				auto it(scene.second->entities.find(IDChild));
				if (it != scene.second->entities.end())
				{
					child = &it->second;
					sceneChild = scene.second.get();
				}
			}
			
			if (parent && child)
				break;
		}
		
		if (!parent || !child)
			return false;
		
		if (sceneParent != sceneChild)
			return false;

		bool const
			childActiveBefore{entityActiveInHierarchy(IDChild)};
		
		// Make sure they are not already parent and child.
		IDEntity parentOld{0};
		bool hasParentOld{false};
		{
			auto it(sceneChild->parents.find(IDChild));
			if (it != sceneChild->parents.end())
			{
				if (IDParent == it->second)
				{
					// Returns true rather than false since
					// the relationship does exist after this.
					return true;
				}
				else
				{
					hasParentOld = true;
					parentOld = it->second;
				}
			}
			
			if (!hasParentOld)
			{
				bool childIsParentOfParent{false};
				
				forEachParentOfEntity
				(
					IDParent,
					[&IDChild, &childIsParentOfParent](IDEntity const ID) -> bool
					{
						if (ID == IDChild)
						{
							childIsParentOfParent = true;
							return false;
						}
						
						return true;
					}
				);
				
				if (childIsParentOfParent)
					return false;
			}
		}

		// Remove any previous parent connection.
		entityUnparent(IDChild, *sceneChild);
		
		// If the parent was previously a child of the child,
		// we have to tie up some loose ends after this.
		if (hasParentOld)
		{
			bool parentIsChild{false};
			forEachParentOfEntity(IDParent, [&parentIsChild, &IDChild](const IDEntity ID)
			{
				if (ID == IDChild)
				{
					parentIsChild = true;
					return false;
				}
				
				return true;
			});
			
			if (parentIsChild)
				// Since the same situation might occur
				// recursively, do the full routine again.
				entityParentOf(parentOld, IDParent);
		}

		// Establish the new connection.
		sceneChild->parents.emplace(IDChild, IDParent);
		sceneParent->children.emplace(IDParent, IDChild);
		
		{
			auto it{sceneChild->grandparents.find(IDChild)};
			if (it != sceneChild->grandparents.end())
				sceneChild->grandparents.erase(it);
		}
		
		if (entityActiveInHierarchy(IDChild) != childActiveBefore)
			for (auto const &component : child->components)
				setComponentActiveInEntity(component.second.ID, IDChild, !childActiveBefore, true, false);

		return true;
	}

	/**
	 * Removes the entity by the specified ID from its
	 * parent if it has one and moves it to the top of
	 * the hierarchy of its containing scene.
	 *
	 * @param identifier The unique ID number of the entity.
	 *
	 * @return Whether the entity was unparented.
	 */
	bool World::entityUnparent(const IDEntity identifier)
	{
		for (auto &scene : m_scenes)
		{
			auto it(scene.second->entities.find(identifier));
			if (it != scene.second->entities.end())
				return entityUnparent(identifier, *scene.second);
		}
		
		return false;
	}
	
	/**
	 * Iterates backwards through every parent of the entity
	 * by the specified ID, and optionally the entity itself.
	 * The callback receives the ID of each entity to work on
	 * and can return false to break out of the loop.
	 *
	 * @param identifier  The unique ID number of the entity to start from.
	 * @param f           The callback to handle each entity found.
	 * @param includeSelf Optionally sets whether to include the starting entity itself (false by default).
	 *
	 * @return Whether the loop was fully carried out without abortion by the callback.
	 */
	bool World::forEachParentOfEntity
	(
		const IDEntity identifier,
	 	const CallbackTraverseEntities &f,
		const bool includeSelf
	) const
	{
		if (includeSelf)
			if (!f(identifier))
				return false;

		for (auto const &scene : m_scenes)
		{
			bool found{false};
			auto it(scene.second->parents.find(identifier));
			while (it != scene.second->parents.end())
			{
				found = true;
				
				if (!f(it->second))
					return false;
				
				it = scene.second->parents.find(it->second);
			}
			
			if (found)
				return true;
		}
		
		return true;
	}

	/**
	 * Iterates recursively through every child, grandchild
	 * and so on of the entity by the specified ID, and
	 * optionally the entity itself. The callback receives the
	 * ID of each entity to work on and can return false to
	 * break out of the loop.
	 *
	 * @param identifier  The unique ID number of the entity to start from.
	 * @param f           The callback to handle each entity found.
	 * @param includeSelf Optionally sets whether to include the starting entity itself (false by default).
	 * @param recursive   Optionally sets whether to include grandchildren (true by default).
	 *
	 * @return Whether the loop was fully carried out without abortion by the callback.
	 */
	bool World::forEachChildOfEntity
	(
		const IDEntity identifier,
	 	const CallbackTraverseEntities &f,
		const bool includeSelf,
		const bool recursive
	) const
	{
		if (includeSelf)
			if (!f(identifier))
				return false;

		bool aborted{false};
		std::function<bool(const DataScene &scene, const IDEntity ID)> traverse;
		traverse = ([&f, &recursive, &traverse, &aborted](const DataScene &scene, const IDEntity ID)
		{
			auto it(scene.children.equal_range(ID));
			bool found{false};
			for (auto jt = it.first; jt != it.second; ++ jt)
			{
				found = true;
				
				if (!f(jt->second))
				{
					aborted = true;
					return true;
				}
				
				if (recursive)
					traverse(scene, jt->second);
			}
			
			return found;
		});
		
		for (auto const &scene : m_scenes)
			if (traverse(*scene.second, identifier))
				return !aborted;
		
		return true;
	}

	/**
	 * Tries to set the active state of the component by the specified ID.
	 *
	 * @param IDC The unique ID number of the component.
	 * @param IDE The unique ID number of the entity.
	 * @param set Whether to set the component active.
	 *
	 * @return Whether the component was found and the state successfully set.
	 */
	bool World::componentActiveInEntity
	(
		IDComponent const IDC,
		IDEntity    const IDE,
		bool        const set
	)
	{
		return setComponentActiveInEntity(IDC, IDE, set, false, false);
	}
	
	bool World::setComponentActiveInEntity
	(
		IDComponent const IDC,
		IDEntity    const IDE,
		bool        const set,
		bool        const hierarchy,
		bool        const forceEvent
	)
	{
		auto
			c(componentInEntity(IDC, IDE));
		
		if (!c)
			return false;
		
		if (hierarchy)
			c->m_activeInHierarchy = set;
		else
			c->m_activeSelf = set;
		
		bool const
			activeInHierarchy{c->activeInHierarchy()};
		
		if (forceEvent || set != activeInHierarchy)
		{
			if (activeInHierarchy)
				queueEvent<EComponentActive>(IDE, c->typeComponent(), IDC);
			else
				queueEvent<EComponentInactive>(IDE, c->typeComponent(), IDC);
		}
		
		return true;
	}
	
	/**
	 * Tries to find the activity state of the component
	 * by the specified ID, returning if false if any
	 * parent entity or parent scene is inactive.
	 *
	 * @param IDC The unique ID number of the component.
	 * @param IDE The unique ID number of the entity.
	 *
	 * @return Whether the component is currently active.
	 */
	/// @todo: Get rid of this fully.
	/*
	bool World::componentActiveInEntityInHierarchy
	(
		const IDComponent IDC,
		const IDEntity    IDE
	) const
	{
		return getComponentActiveInEntity(IDC, IDE, true);
	}
	*/
	
	/**
	 * Tries to find the activity state of the component
	 * by the specified ID, not considering whether any
	 * parent entity or parent scene is inactive.
	 *
	 * @param IDC The unique ID number of the component.
	 * @param IDE The unique ID number of the entity.
	 *
	 * @return Whether the component is currently active.
	 */
	/// @todo: Get rid of this fully.
	/*
	bool World::componentActiveInEntitySelf
	(
		const IDComponent IDC,
		const IDEntity    IDE
	) const
	{
		return getComponentActiveInEntity(IDC, IDE, false);
	}
	*/
	
	/**
	 * Tries to add a type of components that the specified
	 * component type depends upon; it must alread be registered.
	 *
	 * @param identifier The unique ID number of the component type.
	 * @param type       The unique ID number of the dependency type.
	 *
	 * @return A struct containing the success state and an error message on failure.
	 */
	Status World::dependentTypeOfComponent
	(
		const IDTypeComponent identifier,
		const IDTypeComponent type
	)
	{
		auto it(m_subpools.find(identifier));
		if (it == m_subpools.end())
		{
			std::stringstream err;
			err
				<< "Could not set dependent types of component type "
				<< static_cast<std::int64_t>(identifier)
				<< ": not registered";
			return {false, err.str()};
		}
		
		auto &d(it->second.dependencies);

		if (type == identifier)
		{
			std::stringstream err;
			err
				<< "Could not set dependent types of component type "
				<< static_cast<std::int64_t>(identifier)
				<< ": cannot depend on itself";
			return {false, err.str()};
		}

		if (std::find(d.begin(), d.end(), type) != d.end())
		{
			std::stringstream err;
			err
				<< "Could not set dependent types of component type "
				<< static_cast<std::int64_t>(identifier)
				<< ": type "
				<< static_cast<std::int64_t>(type)
				<< " already registered ";
			return {false, err.str()};
		}

		auto const jt(m_subpools.find(type));
		if (jt == m_subpools.end())
		{
			std::stringstream err;
			err
				<< "Could not set dependent types of component type "
				<< static_cast<std::int64_t>(identifier)
				<< ": type "
				<< static_cast<std::int64_t>(type)
				<< " not registered";
			return {false, err.str()};
		}
		
		d.emplace_back(type);
		
		return {true};
	}
	
	/**
	 * Returns the size in bytes of the component type by the specified ID.
	 *
	 * @param identifier The ID of the component type.
	 *
	 * @return The size in bytes of the component type, or zero if no type by the specified identifier has been registered.
	 */
	std::size_t World::sizeOfTypeOfComponent(IDTypeComponent const identifier) const
	{
		auto const it(m_subpools.find(identifier));
		
		if (it != m_subpools.end())
			return it->second.typesize;
		
		return 0;
	}
	
	/**
	 * Returns a list containing the ID of each component type registered.
	 *
	 * @return A vector with all component type identifiers.
	 */
	std::vector<IDTypeComponent> World::typesOfComponentsRegistered() const
	{
		std::vector<IDTypeComponent> r;
		
		for (auto const &it : m_subpools)
			r.emplace_back(it.first);
		
		return r;
	}
	
	/**
	 * Tries to fetch the name registered for the component type.
	 * Returns an empty string if the type has not been registered.
	 *
	 * @param identifier The unique ID number of the component type.
	 *
	 * @return A string containing the name on success.
	 */
	std::string World::nameOfTypeComponent(const IDTypeComponent identifier) const
	{
		auto const it(m_subpools.find(identifier));

		if (it != m_subpools.end())
			return it->second.name;
		
		return {};
	}

	/**
	 * Tries to create a component of the specified type
	 * and with the specified ID in the entity with the
	 * specified entity ID.
	 *
	 * @param type  The unique ID of the type of component.
	 * @param IDC   The unique ID number of the component.
	 * @param IDE   The unique ID number of the entity.
	 * @param flags Optional settings for creation (everything turned on by default).
	 *
	 * @return A struct containing the success state as well as a pointer to the component on success and an error message on failure.
	 */
	Result<Component *> World::createComponentInEntity
	(
		const IDTypeComponent type,
		const IDComponent     IDC,
		const IDEntity        IDE,
		const CreateComponent flags
	)
	{
		// Make sure the type has been registered.
		auto jt(m_subpools.find(type));
		if (jt == m_subpools.end())
		{
			std::stringstream err;
			err
				<< "Could not add component by ID "
				<< static_cast<std::int64_t>(IDC)
				<< " to entity "
				<< static_cast<std::int64_t>(IDE)
				<< ": type not registered";
			return {nullptr, false, err.str()};
		}
		
		for (auto &scene : m_scenes)
		{
			// Find the entity in the scene.
			auto it(scene.second->entities.find(IDE));
			
			if (it != scene.second->entities.end())
			{
				// Check that more can actually be created.
				if (0 != jt->second.countPerEntity)
				{
					auto const kt   (it->second.components.equal_range(type));
					auto const count(static_cast<std::size_t>(std::distance(kt.first, kt.second)));
					
					if (count >= jt->second.countPerEntity)
					{
						std::stringstream err;
						err
							<< "Could not add component by ID "
							<< static_cast<std::int64_t>(IDC)
							<< " to entity "
							<< static_cast<std::int64_t>(IDE)
							<< ": limit of "
							<< static_cast<std::int64_t>(count)
							<< " per instance reached";
						return {nullptr, false, err.str()};
					}
				}

				// Create the actual component in the pool.
				auto
					s(jt->second.fCreateComponent(scene.second->pool, IDC, IDE));
				
				if (s.success)
				{
					auto const
						active(static_cast<bool>(flags & CreateComponent::active));
					
					// Register the component in the lookup list.
					it->second.components.emplace
					(
						type,
						DataScene::DataEntity::DataComponent{IDC}
					);
					
					// Active self?
					if (!active)
						setComponentActiveInEntity(IDC, IDE, false, false, false);
					
					// Active in hierarchy?
					if (!entityActiveInHierarchy(IDE))
						setComponentActiveInEntity(IDC, IDE, false, true, true);
					else
						setComponentActiveInEntity(IDC, IDE, true, true, true);
					
					/// @todo: Get rid of this probably now.
					/*
					if (componentActiveInEntityInHierarchy(IDC, IDE))
						queueEvent<EComponentActive>(IDE, type, IDC);
					else
						queueEvent<EComponentInactive>(IDE, type, IDC);
					*/
					if (static_cast<bool>(flags & CreateComponent::createDependencies))
					{
						auto const t(createDependenciesRecursivelyInEntity
						(
							IDE,
							*scene.second,
							it->second.components,
							type
						));
					
						if (!t.success)
							return {nullptr, false, std::move(t.error)};
					}
				}
				
				return s;
			}
		}

		std::stringstream err;
		err
			<< "Could not add component by ID "
			<< static_cast<std::int64_t>(IDC)
			<< " to entity "
			<< static_cast<std::int64_t>(IDE)
			<< ": entity does not exist";
		return {nullptr, false, err.str()};
	}

	/**
	 * Tries to destroy the component with the specified
	 * component ID in the entity with the specified ID.
	 *
	 * @param IDC The unique ID number of the component.
	 * @param IDE The unique ID number of the entity.
	 *
	 * @return A struct containing the success state and an error code on failure.
	 */
	Status World::destroyComponentInEntity
	(
		const IDComponent IDC,
		const IDEntity IDE
	)
	{
		for (auto &scene : m_scenes)
		{
			// Find the entity in the scene.
			auto it(scene.second->entities.find(IDE));
			if (it != scene.second->entities.end())
			{
				auto jt(std::find_if
				(
					it->second.components.begin(),
					it->second.components.end(),
					[&IDC](auto const &v)
					{
						return (IDC == v.second.ID);
					}
				));

				// Remove the entry from the lookup table.
				if (jt != it->second.components.end())
				{
					auto s(destroyComponentInEntity(jt, it, scene.second->pool));
					
					if (!s.success)
						return s;
				}
				
				return {true};
			}
		}

		std::stringstream err;
		err
			<< "Could not remove component by ID "
			<< static_cast<std::int64_t>(IDC)
			<< " from entity "
			<< static_cast<std::int64_t>(IDE)
			<< ": entity does not exist";
		return {false, err.str()};
	}
	
	/**
	 * Tries to find a component by the specified type in
	 * the entity by the specified ID in any of the scenes.
	 *
	 * @param IDT The unique ID of the component type.
	 * @param IDE The unique ID of the entity.
	 *
	 * @return A pointer to the component on success or null (do not hold on to).
	 */
	Component *World::componentByTypeInEntity
	(
		const IDTypeComponent IDT,
		const IDEntity        IDE
	) const
	{
		// Make sure the type has been registered.
		if (m_subpools.find(IDT) == m_subpools.end())
			return nullptr;
	
		for (auto &scene : m_scenes)
		{
			// Find the entity in the scene.
			auto it(scene.second->entities.find(IDE));
			if (it != scene.second->entities.end())
			{
				// Find the component type in the entity.
				auto jt(it->second.components.find(IDT));
				if (jt != it->second.components.end())
				{
					// Find the actual component in the pool.
					return reinterpret_cast<Component *>
					(
						scene.second->pool.data(jt->second.ID)
					);
				}
			}
		}

		return nullptr;
	}
	
	/**
	 * Tries to find a component by the specified ID in
	 * the entity by the specified ID in any of the scenes.
	 *
	 * @param IDC The unique ID of the component.
	 * @param IDE The unique ID of the entity.
	 *
	 * @return A pointer to the component on success or null (do not hold on to).
	 */
	Component *World::componentInEntity
	(
		const IDComponent IDC,
		const IDEntity    IDE
	) const
	{
		for (auto &scene : m_scenes)
		{
			// Find the entity in the scene.
			auto it(scene.second->entities.find(IDE));
			if (it != scene.second->entities.end())
				return reinterpret_cast<Component *>(scene.second->pool.data(IDC));
		}

		return nullptr;
	}
	
	std::map<IDComponent, IDTypeComponent> World::componentsByIDInEntity
	(
		const IDEntity identifier
	) const
	{
		std::map<IDComponent, IDTypeComponent> r;
		
		for (auto &scene : m_scenes)
		{
			// Find the entity in the scene.
			auto it(scene.second->entities.find(identifier));
			if (it != scene.second->entities.end())
			{
				for (auto const &c : it->second.components)
					r.emplace(c.second.ID, c.first);
			}
		}

		return r;
	}

	void World::performManipulateSceneAsync
	(
		World *const world,
		DataScene *const scene,
		const CallbackManipulateSceneAsync &f
	)
	{
		auto const s(f(*world, scene->identifier));
		scene->state = (s) ? DataScene::State::loaded : DataScene::State::error;
	}

	Status World::destroyComponentInEntity
	(
		decltype(DataScene::DataEntity::components)::iterator &component,
		decltype(DataScene::entities)::iterator &entity,
		Pool &pool
	)
	{
		const IDEntity        IDE {entity->first};
		const IDTypeComponent type{component->first};
		const IDComponent     IDC {component->second.ID};
		
		// Check for dependencies.
		for (auto const &c : entity->second.components)
		{
			if (c.first == type)
				continue;
			
			auto const jt(m_subpools.find(c.first));
			auto const &d(jt->second.dependencies);
			if (std::find(d.begin(), d.end(), type) != d.end())
			{
				std::stringstream err;
				err
					<< "Could not remove component by ID "
					<< static_cast<std::int64_t>(IDC)
					<< " from entity "
					<< static_cast<std::int64_t>(IDE)
					<< ": another component depends on it";
				return {false, err.str()};
			}
		}
		
		pool.destroyInstance(IDC);
		entity->second.components.erase(component);
		queueEvent<EComponentDestroyed>(IDE, type, IDC);

		return {true};
	}
	
	// We do no safety checks for existing values in this,
	// since at the point where this gets used they should.
	Status World::createDependenciesRecursivelyInEntity
	(
		const IDEntity identifier,
		DataScene &scene,
		DataScene::DataEntity::Components &components,
		const IDTypeComponent type
	)
	{
		auto it(m_subpools.find(type));
		for (auto const &d : it->second.dependencies)
		{
			// We do however need to check the entity does
			// not already have a component of this type.
			if (components.find(d) != components.end())
				continue;
			
			auto const ID(genIDForComponent());

			auto jt(m_subpools.find(d));
			auto s(jt->second.fCreateComponent(scene.pool, ID, identifier));
			
			if (s.success)
			{
				// Register the component in the lookup list.
				components.emplace(d, DataScene::DataEntity::DataComponent{ID});
				return createDependenciesRecursivelyInEntity
				(
					identifier,
					scene,
					components,
					d
				);
			}
		
			return {false, std::move(s.error)};
		}
		
		return {true};
	}
	
	/**
	 * Returns the immediate parent of the specified entity;
	 * if the entity has no parents, its own ID is returned.
	 *
	 * @param ID The ID of the entity to start from.
	 *
	 * @return The ID of the immediate parent, if there is one, or the entity itself.
	 */
	IDEntity World::entityParentFirst(const IDEntity ID) const
	{
		IDEntity r{ID};
		forEachParentOfEntity(ID, [&r](const IDEntity IDNext) { r = IDNext; return false; });
		return r;
	}
	
	/**
	 * Returns the topmost parent of the specified entity;
	 * if the entity has no parents, its own ID is returned.
	 *
	 * @param ID The ID of the entity to start from.
	 *
	 * @return The ID of the topmost parent, if there is one, or the entity itself.
	 */
	IDEntity World::entityParentLast(const IDEntity ID) const
	{
		IDEntity r{ID};
		forEachParentOfEntity(ID, [&r](const IDEntity IDNext) { r = IDNext; return true; });
		return r;
	}
	
	/**
	 * Returns the topmost child of the specified entity;
	 * if the entity has no children, its own ID is returned.
	 *
	 * @param ID The ID of the entity to start from.
	 *
	 * @return The ID of the topmost child, if there is one, or the entity itself.
	 */
	IDEntity World::entityChildFirst(const IDEntity ID) const
	{
		IDEntity r{ID};
		forEachChildOfEntity(ID, [&r](const IDEntity IDNext) { r = IDNext; return false; });
		return r;
	}
	
	/**
	 * Returns the bottommost child of the specified entity;
	 * if the entity has no children, its own ID is returned.
	 *
	 * @param ID The ID of the entity to start from.
	 *
	 * @return The ID of the bottommost child, if there is one, or the entity itself.
	 */
	IDEntity World::entityChildLast(const IDEntity ID) const
	{
		IDEntity r{ID};
		forEachChildOfEntity(ID, [&r](const IDEntity IDNext) { r = IDNext; return true; });
		return r;
	}
	
	Status World::moveEntityToScene
	(
		const IDEntity IDE,
		const IDScene IDS,
		const bool recurse
	)
	{
		for (auto &scene : m_scenes)
		{
			auto it(scene.second->entities.find(IDE));
			if (it != scene.second->entities.end())
			{
				if (DataScene::State::loading == scene.second->state)
				{
					std::stringstream err;
					err
						<< "Error moving entity "
						<< static_cast<std::int64_t>(IDE)
						<< " to scene "
						<< static_cast<std::int64_t>(IDS)
						<< ": source scene busy";
					return {false, err.str()};
				}

				// Already in this scene; do nothing.
				if (IDS == scene.first)
					return {true};
				
				// Find the other scene.
				auto target(m_scenes.find(IDS));
				if (target == m_scenes.end())
				{
					std::stringstream err;
					err
						<< "Error moving entity "
						<< static_cast<std::int64_t>(IDE)
						<< " to scene "
						<< static_cast<std::int64_t>(IDS)
						<< ": target scene does not exist";
					return {false, err.str()};
				}
				
				if (DataScene::State::loading == target->second->state)
				{
					std::stringstream err;
					err
						<< "Error moving entity "
						<< static_cast<std::int64_t>(IDE)
						<< " to scene "
						<< static_cast<std::int64_t>(IDS)
						<< ": target scene busy";
					return {false, err.str()};
				}

				// Go through and move each component.
				for (auto const &c : it->second.components)
				{
					const IDComponent IDC{c.second.ID};
					auto kt(m_subpools.find(c.first));

					auto s(kt->second.fMoveComponent(scene.second->pool, target->second->pool, IDC));
					
					if (!s.success)
					{
						std::stringstream err;
						err
							<< "Error moving entity "
							<< static_cast<std::int64_t>(IDE)
							<< " to scene "
							<< static_cast<std::int64_t>(IDS)
							<< ": "
							<< s.error;
						return {false, err.str()};
					}
				}
				
				// Move the entire entity and its lookup table.
				target->second->entities.emplace(std::move(it->first), std::move(it->second));
				scene.second->entities.erase(it);
				
				// If recursing, move the child hierarchy as well.
				if (recurse)
				{
					// We only use the loop to collect ID's, not to do anything.
					std::vector<IDEntity> children;
					forEachChildOfEntity(IDE, [&children](const IDEntity ID)
					{
						children.emplace_back(ID);
						return true;
					});
					
					// Now move every child but without recursion,
					// since the above loop caught grandchildren too.
					for (auto const &ID : children)
						moveEntityToScene(ID, IDS, false);
				}
				
				// Deal with the parent entry.
				{
					auto jt(scene.second->parents.find(IDE));
					if (jt != scene.second->parents.end())
					{
						// If this is a child, move it to the new scene.
						if (!recurse)
							target->second->parents.emplace
							(
								std::move(jt->first),
								std::move(jt->second)
							);
						
						scene.second->parents.erase(jt);
					}
				}

				// Deal with the child entries.
				{
					auto jt(scene.second->children.equal_range(IDE));
					if (jt.first != jt.second)
					{
						target->second->children.insert
						(
							std::make_move_iterator(jt.first),
							std::make_move_iterator(jt.second)
						);
						scene.second->children.erase(jt.first, jt.second);
					}
				}

				// Deal with the grandparent entry.
				{
					auto jt(scene.second->grandparents.find(IDE));
					if (jt != scene.second->grandparents.end())
					{
						target->second->grandparents.emplace(IDE);
						scene.second->grandparents.erase(jt);
					}
				}
			}
		}
		
		std::stringstream err;
		err
			<< "Error moving entity "
			<< static_cast<std::int64_t>(IDE)
			<< " to scene "
			<< static_cast<std::int64_t>(IDS)
			<< ": entity or scene not found";
		return {false, err.str()};
	}

	Status World::addPoolToScene
	(
		DataScene &scene,
		const IDTypeComponent identifier,
		DataSubpool &d
	)
	{
		return d.fCreate(scene.pool, identifier, d.countPerChunk, d.growable);
	}
	
	bool World::entityUnparent(const IDEntity IDChild, DataScene &sceneChild)
	{
		auto it(sceneChild.parents.find(IDChild));
		if (it != sceneChild.parents.end())
		{
			bool const
				activeBefore{entityActiveInHierarchy(IDChild)};
			
			// Remove the child from the parent.
			for (auto &scene : m_scenes)
			{
				bool found{false};
				auto jt(scene.second->children.equal_range(it->second));
				for (auto kt(jt.first); kt != jt.second; ++ kt)
				{
					if (IDChild == kt->second)
					{
						scene.second->children.erase(kt);
						found = true;
						break;
					}
				}
				
				if (found)
					break;
			}

			// Remove the parent from the child.
			sceneChild.parents.erase(it);
			
			// It is now a grandparent.
			sceneChild.grandparents.emplace(IDChild);
			
			auto const
				&entity(sceneChild.entities[IDChild]);
			
			// No need to check in hierarchy this time since there is no parent.
			if (entity.active != activeBefore)
				for (auto const &component : entity.components)
					setComponentActiveInEntity(component.second.ID, IDChild, !activeBefore, true, false);
			
			return true;
		}
		
		return false;
	}
	
	bool World::getEntityActive
	(
		const IDEntity identifier,
		const bool      hierarchy
	) const
	{
		for (auto &scene : m_scenes)
		{
			// In hierarchy, parent scene has to be active too.
			if (hierarchy && !scene.second->active)
				return false;

			// Find the entity in the scene.
			auto it(scene.second->entities.find(identifier));
			if (it != scene.second->entities.end())
			{
				// In hierarchy, if any parent is inactive, return false.
				if (!hierarchy)
					return it->second.active;
				
				auto const s(forEachParentOfEntity(identifier, [&scene](const IDEntity ID)
				{
					auto jt(scene.second->entities.find(ID));
					return (!(jt != scene.second->entities.end() && !jt->second.active));
				}));
				
				return (s) ? it->second.active : false;
			}
		}
		
		return false;
	}
	
	/// @todo: Get rid of this fully.
	/*
	bool World::getComponentActiveInEntity
	(
		const IDComponent IDC,
		const IDEntity    IDE,
		const bool        hierarchy
	) const
	{
		for (auto const &scene : m_scenes)
		{
			if (DataScene::State::loading == scene.second->state)
				continue;
			
			if (hierarchy && !scene.second->active)
				return false;

			auto const it(scene.second->entities.find(IDE));
			if (it != scene.second->entities.end())
			{
				if (hierarchy && !entityActiveInHierarchy(IDE))
					return false;

				auto const jt(std::find_if
				(
					it->second.components.begin(),
					it->second.components.end(),
					[&IDC](auto const &v) { return (IDC == v.second.ID); }
				));
				
				if (jt != it->second.components.end())
					return jt->second.active;
			}
		}
		
		return false;
	}
	*/

	World::~World()
	{
		for (auto &system : m_systems)
			system.second.fDelete(system.second.system);
	}
}
