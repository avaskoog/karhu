/**
 * @author		Ava Skoog
 * @date		2018-09-22
 * @copyright	2018-2021 Ava Skoog
 */

#ifndef KENNO_WORLD_H_
	#define KENNO_WORLD_H_

	#include "Pool.hpp"
	#include "Component.hpp"
	#include "Event.hpp"
	#include "System.hpp"
	#include "common.hpp"
	#include "byte.hpp"

	#include <map>
	#include <unordered_map>
	#include <functional>
	#include <type_traits>
	#include <vector>
	#include <set>
	#include <sstream>
	#include <future>
	#include <atomic>
	#include <thread>

	/// @todo: Remove this when done testing.
	#include <iostream>

	/// @todo: Functions to un-/reregister systems/events/components for runtime hotloading? And propagating the changes by recreating stuff? Messy... Probably leave that to user of lib, but leave in hooks to make it easy at least.
	/// @todo: Fix up the ID assignment thing a bit?
	/// @todo: Figure out if there are any issues reusing the future to do async stuff on the same scene twice.
	/// @todo: Do we need a way to abort async?
	/// @todo: System of tags, layers and so on for entities?
	/// @todo: Only conditionally compile name into entities using some define?
	/// @todo: Store names for system types as well (what aboút event types?).
	/// @todo: Queue active/inactive events when components/entities are created too, but figure out if their active state can be set first, for example at creation.
	/// @todo: Lookup table for what scene an entity is in so we don't have to loop through all the scenes?

	namespace kenno
	{
		enum class CreateEntity : std::uint8_t
		{
			none   =  0,
			active =  1 << 0,
			all    = (1 << 1) - 1
		};
		
		enum class CreateComponent : std::uint8_t
		{
			none               =  0,
			active             =  1 << 0,
			createDependencies =  1 << 1,
			all                = (1 << 2) - 1
		};
		
		class World
		{
			public:
				template<typename T>
				using CallbackListenToEvent = std::function<void(World &, const T &, const float dt, const float dtFixed, const float timestep, void *data)>;
			
				using CallbackManipulateSceneAsync = std::function<bool(World &, const IDScene )>;
				using CallbackTraverseEntities     = std::function<bool(const IDEntity )>;
				using CallbackGenIDForComponent    = std::function<IDComponent()>;

			private:
				/// @todo: Maybe these aliases should be moved into wherever they are being used?

				struct DataEvents;
			
				using FCreateSubpool   = std::function<Status(Pool &, const IDTypeComponent, const std::size_t &, const bool)>;
				using FCreateComponent = std::function<Result<Component *>(Pool &, const IDComponent, const IDEntity)>;
				using FMoveComponent   = std::function<Status(Pool &, Pool &, const IDComponent)>;
			
				using FDispatchEvents = std::function<void(DataEvents &, const float dt, const float dtFixed, const float timestep)>;
			
				using FUpdateSystem = std::function<void(void *, World &, const float dt, const float dtFixed, const float timestep)>;
				using FDeleteSystem = std::function<void(void *)>;

				struct DataSubpool
				{
					std::string name;

					std::size_t
						typesize,
						countPerEntity,
						countPerChunk;
					
					bool growable;
					
					FCreateSubpool   fCreate;
					FCreateComponent fCreateComponent;
					FMoveComponent   fMoveComponent;

					std::vector<IDTypeComponent> dependencies;
				};
			
				struct DataTypeEvent
				{
					std::string name;
					FDispatchEvents fDispatchEvents;
				};

				struct DataSystem
				{
					void *system;
					FUpdateSystem fUpdate;
					FDeleteSystem fDelete;
				};
			
				/*
				 * A scene contains the actual ECS, one per scene.
				 * Multiple scenes can coëxist in a world and can
				 * be loaded and unloaded asynchronously while kept
				 * separated in order to enable content streaming.
				 * This is where entities and their components live.
				 */
				struct DataScene
				{
					enum class State
					{
						loaded,
						loading,
						error
					};

					struct DataEntity
					{
						struct DataComponent
						{
							IDComponent ID;
							/// @todo: There used to be more stuff here and might be again so keeping this as a struct for now.
						};

						/// @todo: What about consistent order of components?
						/// @todo: Should we have an outer cache, much like the parents and children, for quicker direct access to the full list?
						using Components = std::unordered_multimap<IDTypeComponent, DataComponent>;

						std::string name;
						bool active{true};
						TagsEntity tags{0};
						Components components;
						std::unique_ptr<Userdata> data;
					};

					DataScene(const IDScene identifier) : identifier{identifier} {}

					IDScene identifier;

					std::map<IDEntity, DataEntity> entities;
					std::map<IDEntity, IDEntity> parents;
					std::multimap<IDEntity, IDEntity> children; // Has to be ordered to preserve child order.
					std::set<IDEntity> grandparents;

					Pool pool;

					std::atomic<State> state{State::loaded};
					std::future<void> future;
					bool active{true};
				};
			
			public:
				/**
				 * Constructor.
				 *
				 * @param fGenIDForComponent Called to generate a unique ID for a component when it cannot be directly specified (such as when dependent components are recursively created).
				 */
				World
				(
					const CallbackGenIDForComponent &fGenIDForComponent,
					const char *namespaceEvents
				)
				:
				genIDForComponent{fGenIDForComponent}
				{
					IDTypeEvent ID{0};
					
					std::string space{namespaceEvents}, n;
					
					if (0 != space.length())
						space += "::";
					
					#define kennoREG_EVENT(type) \
						n = space + #type; \
						registerTypeOfEventRestricted<type>((ID ++), n.c_str());
					
					// Register in the order they should be dispatched.
					kennoREG_EVENT(EEntityCreated)
					kennoREG_EVENT(EEntityDestroyed)
					kennoREG_EVENT(EComponentCreated)
					kennoREG_EVENT(EComponentDestroyed)
					kennoREG_EVENT(EEntityActive)
					kennoREG_EVENT(EEntityInactive)
					kennoREG_EVENT(EComponentActive)
					kennoREG_EVENT(EComponentInactive)
					
					#undef kennoREG_EVENT
				}

				void update(const float dt, const float dtFixed, const float timestep);

				// Systems.

				template<typename T, typename... Ts>
				Result<T *> createSystemWithPriorityAndArgs
				(
					const Priority priority,
					Ts &&... argsForConstructor
				);
			
				// Events.

				template<typename T>
				Status registerTypeOfEvent(const IDTypeEvent identifier, const char *name);
			
				std::string nameOfTypeEvent(const IDTypeEvent identifier) const;
			
				template<typename T>
				IDTypeEvent IDOfTypeEvent() const
				{
					return Pool::IDOfType<T>();
				}

				template<typename TComponent, typename TEvent>
				Status createListenerToEvent
				(
					const IDListener identifier,
					void *data,
					const CallbackListenToEvent<TEvent> &f,
				 	const Priority priority = 0
				);
			
				template<typename TEvent>
				Status createListenerToEvent
				(
					const IDListener identifier,
					void *data,
					const CallbackListenToEvent<TEvent> &f,
				 	const Priority priority = 0
				);
				
				/// @todo: ID version instead of template version of create listener?
			
				template<typename T, typename... Ts>
				Status emitEvent
				(
					const float dt,
					const float dtFixed,
					const float timestep,
					Ts &&... argsForConstructor
				);
			
				template<typename T, typename... Ts>
				Status queueEvent(Ts &&... argsForConstructor);

				Status destroyListenerToEvent(const IDListener identifier);
			
				// Scenes.
			
				Status createScene(const IDScene identifier);
				Status destroyScene(const IDScene identifier);
			
				bool sceneLoaded(const IDScene identifier) const;
				bool sceneLoadedWithError(const IDScene identifier) const;

				bool sceneActive(const IDScene identifier, const bool set);
				bool sceneActive(const IDScene identifier) const;
			
				const std::set<IDScene> &scenes() const { return m_IDsScenes; }
				const std::set<IDEntity> *grandparentsInScene(const IDScene identifier) const;
				const std::map<IDEntity, IDEntity> *parentsInScene(const IDScene identifier) const;
				const std::multimap<IDEntity, IDEntity> *childrenInScene(const IDScene identifier) const;

				Status manipulateSceneAsync
				(
					const IDScene identifier,
					const CallbackManipulateSceneAsync &f
				);

				// Entities.

				Status createEntityInScene
				(
					const IDEntity      IDEntity,
					const IDScene       IDScene,
					const CreateEntity  flags     = CreateEntity::all,
					const std::string  &name      = {}
				);
			
				bool entityUserdata(const IDEntity identifier, std::unique_ptr<Userdata> &&data);
				Userdata *entityUserdata(const IDEntity identifier);
			
				/// @todo: Also add component userdata.
			
				std::vector<IDEntity> entitiesInScene(const IDScene) const;
				std::vector<IDEntity> entitiesInActiveScenes() const;

				Status destroyEntity(const IDEntity identifier);
				bool destroyChildrenOfEntity(const IDEntity identifier);
			
				Status moveEntityToScene(const IDEntity IDE, const IDScene IDS);
			
				bool entityName(const IDEntity identifier, const std::string &name);
				bool entityName(const IDEntity identifier, std::string &&name);
				bool entityName(const IDEntity identifier, const char *name);
				std::string entityName(const IDEntity identifier) const;
			
				bool entityTags(const IDEntity identifier, const TagsEntity tags);
				TagsEntity entityTags(const IDEntity identifier) const;
				std::vector<IDEntity> entitiesByTags(TagsEntity const tags) const;
			
				bool entityActive(const IDEntity identifier, const bool set);
				bool entityActiveInHierarchy(const IDEntity identifier) const;
				bool entityActiveSelf(const IDEntity identifier) const;
			
				bool entityParentOf(const IDEntity IDParent, const IDEntity IDChild);
				bool entityUnparent(const IDEntity identifier);
				IDEntity entityParentFirst(const IDEntity ID) const;
				IDEntity entityParentLast(const IDEntity ID) const;
				IDEntity entityChildFirst(const IDEntity ID) const;
				IDEntity entityChildLast(const IDEntity ID) const;

				bool forEachParentOfEntity
				(
					const IDEntity identifier,
					const CallbackTraverseEntities &f,
					const bool includeSelf = false
				) const;
			
				bool forEachChildOfEntity
				(
					const IDEntity identifier,
					const CallbackTraverseEntities &f,
					const bool includeSelf = false,
					const bool recursive = true
				) const;

				// Components.

				template<typename T>
				Status registerTypeOfComponentWithCountsAndGrowability
				(
					const IDTypeComponent identifier,
					const char *name,
					const std::size_t &countMaxPerEntity,
					const std::size_t &countPerChunkInPool,
					const bool growable
				);
			
				std::vector<IDTypeComponent> typesOfComponentsRegistered() const;
			
				std::size_t sizeOfTypeOfComponent(IDTypeComponent const identifier) const;
			
				template<typename T>
				IDTypeComponent IDOfTypeComponent() const
				{
					return Pool::IDOfType<T>();
				}
			
				Status dependentTypeOfComponent
				(
					const IDTypeComponent identifier,
					const IDTypeComponent type
				);
			
				template<typename T, typename U>
				Status dependentTypeOfComponent();
			
				std::string nameOfTypeComponent(const IDTypeComponent identifier) const;

				template<typename T>
				Result<T *> createComponentInEntity
				(
					const IDComponent     IDC,
					const IDEntity        IDE,
					const CreateComponent flags = CreateComponent::all
					/// @todo: We will want a way to return the created dependencies to the user too, perhaps using an option output vector pointer as the last optional parameter.
				);

				Result<Component *> createComponentInEntity
				(
					const IDTypeComponent type,
					const IDComponent     IDC,
					const IDEntity        IDE,
					const CreateComponent flags = CreateComponent::all
					/// @todo: We will want a way to return the created dependencies to the user too, perhaps using an option output vector pointer as the last optional parameter.
				);

				Status destroyComponentInEntity
				(
					const IDComponent IDC,
					const IDEntity IDE
				);

				template<typename T>
				T *component(const IDComponent identifier) const;

				template<typename T>
				T *componentInEntity(const IDEntity identifier) const;

				template<typename T>
				T *componentInEntity
				(
					const IDComponent IDC,
					const IDEntity    IDE
				) const;
			
				Component *componentByTypeInEntity
				(
					const IDTypeComponent IDT,
					const IDEntity        IDE
				) const;
			
				Component *componentInEntity
				(
					const IDComponent IDC,
					const IDEntity    IDE
				) const;
			
				template<typename T>
				std::vector<T *> componentsInEntity(const IDEntity identifier) const;
			
				std::map<IDComponent, IDTypeComponent> componentsByIDInEntity(const IDEntity identifier) const;

				bool componentActiveInEntity
				(
					IDComponent const IDC,
					IDEntity    const IDE,
					bool        const set
				);
			
				/// @todo: Get rid of these fully.
/*
				bool componentActiveInEntitySelf
				(
					const IDComponent IDC,
					const IDEntity IDE
				) const;
			
				bool componentActiveInEntityInHierarchy
				(
					const IDComponent IDC,
					const IDEntity IDE
				) const;
*/
				/// @todo: Add to check if active with or without considering parents.
			
				template<typename T>
				std::vector<T *> componentsInScene(const IDScene ID) const;
			
				template<typename T>
				std::vector<T *> componentsInActiveScenes() const;
			
				template<typename T>
				std::vector<Pool::ViewSubpool<T>> componentpoolsInActiveScenes() const;
			
				virtual ~World();

			private:
				static void performManipulateSceneAsync
				(
					World *const,
					DataScene *const,
					const CallbackManipulateSceneAsync &
				);

				/*
				 * Used internally to mark a system type as registered.
				 */
				template<typename T>
				static bool checkOrRegisterSystem(const bool set)
				{
					static_assert(std::is_base_of<System<typename T::TypeComponent>, T>{}, "System datatype must inherit System<T>!");
					
					static bool value{false};

					if (set)
						value = true;
					
					return value;
				}

			private:
				template<typename T>
				Status registerTypeOfEventRestricted(const IDTypeEvent identifier, const char *name);
			
				Status destroyComponentInEntity
				(
					decltype(DataScene::DataEntity::components)::iterator &component,
					decltype(DataScene::entities)::iterator &entity,
					Pool &pool
				);
			
				Status createDependenciesRecursivelyInEntity
				(
					const IDEntity ,
					DataScene &,
					DataScene::DataEntity::Components &,
					const IDTypeComponent
				);
			
				bool entityUnparent(const IDEntity IDChild, DataScene &sceneChild);
				Status moveEntityToScene(const IDEntity IDE, const IDScene IDS, const bool recurse);
			
				Status addPoolToScene(DataScene &, const IDTypeComponent , DataSubpool &);
			
				bool getEntityActive(const IDEntity identifier, const bool hierarchy) const;
			
				/// @todo: Get rid of this fully.
				//bool getComponentActiveInEntity(const IDComponent IDC, const IDEntity IDE, const bool hierarchy) const;
				bool setComponentActiveInEntity
				(
					IDComponent const IDC,
					IDEntity    const IDE,
					bool        const set,
					bool        const hierarchy,
					bool        const forceEvent
				);
			
			private:
				struct DataEvents
				{
					template<typename T>
					class DataListener : public Poolable
					{
						public:
							using Poolable::Poolable;
						
							DataListener
							(
								const Poolable::ID identifier,
								const bool typeAny,
								const IDTypeComponent type,
								void *data,
								const CallbackListenToEvent<T> &f
							)
							:
							Poolable{identifier},
							typeAny {typeAny},
							type    {type},
							data    {data},
							callback{f}
							{
							}
						
							bool typeAny;
							IDTypeComponent type;
							void *data{nullptr};
							CallbackListenToEvent<T> callback;
					};
					
					IDEvent IDEventLast{0};
					Pool events, listeners;
					std::map<IDListener, Priority> prioritiesPerListener;
				} m_events;
			
				std::map<IDTypeEvent, DataTypeEvent> m_typesEvents;
				std::set<IDScene> m_IDsScenes;
				std::unordered_map<IDScene, std::unique_ptr<DataScene>> m_scenes;
				std::unordered_map<IDTypeComponent, DataSubpool> m_subpools;
				std::vector<std::pair<Priority, DataSystem>> m_systems; /// @todo: Why isn't this a multimap?
			
				CallbackGenIDForComponent genIDForComponent;
		};
		
		/**
		 * Tries to create a system of the specified type.
		 *
		 * @param priority           Priority value to have some systems run before others (lower number means higher priority; can be negative).
		 * @param argsForConstructor The arguments to be forwarded into the constructor of the system type.
		 *
		 * @return A struct containing the success state and pointer to the created system on succes or an error message on failure.
		 */
		template<typename T, typename... Ts>
		Result<T *> World::createSystemWithPriorityAndArgs
		(
			const Priority priority,
			Ts &&... argsForConstructor
		)
		{
			static_assert(std::is_base_of<System<typename T::TypeComponent>, T>{}, "System type must inherit System<T>!");

//			if (checkOrRegisterSystem<T>(false))
//				return {nullptr, false, "Failed to create system: system of this type already created"};

			if (!checkOrRegisterSystem<T>(false))
				checkOrRegisterSystem<T>(true);
			
			auto r(new T{std::forward<Ts>(argsForConstructor)...});
			
			m_systems.emplace_back
			(
				decltype(m_systems)::value_type
				{
					priority,
					DataSystem
					{
						r,

						// Update a system.
						[](void *system, World &world, const float dt, const float dtFixed, const float timestep)
						{
							auto sys(reinterpret_cast<T *>(system));
							
							if (sys->updatesComponents())
							{
								for (auto &scene : world.m_scenes)
								{
									if (!scene.second->active)
										continue;

									// Cannot access scene if it is loading asynchronously.
									if (DataScene::State::loading == scene.second->state)
										continue;
									
									auto s(scene.second->pool.subpool<typename T::TypeComponent>());
									
									if (0 == s.count || !s.data)
										continue;

									sys->update
									(
										world,
										s,
										dt,
										dtFixed,
										timestep
									);
								}
							}
							else
								sys->updateWithoutComponents
								(
									world,
									dt,
									dtFixed,
									timestep
								);
						},

						// Destroy a system.
						[](void *system)
						{
							delete reinterpret_cast<T *>(system);
						}
					}
				}
			);
			
			std::sort
			(
				m_systems.begin(),
				m_systems.end(),
				[](const auto &a, const auto &b)
				{
					return (a.first < b.first);
				}
			);
			
			return {r, true};
		}
		
		/**
		 * Tries to register a new type of event.
		 *
		 * An event type must be registered for a listener to be able
		 * to subscribe to it and for a systme to be able to emit it.
		 *
		 * @param identifier The unique ID number to assign to the event type (must be > 100, as the range <= 100 is reserved for basic Kenno events).
		 * @param name       The name of the event type as a string.
		 *
		 * @return A struct containing the success state and an error message on failure.
		 */
		template<typename T>
		Status World::registerTypeOfEvent(const IDTypeEvent identifier, const char *name)
		{
			if (identifier <= 100)
			{
				std::stringstream s;
				
				s
					<< "Cannot register event type with ID "
					<< static_cast<int>(identifier)
					<< " because the range 0-100 is reserved for built-in types";
				
				return
				{
					false,
					s.str()
				};
			}
		
			return registerTypeOfEventRestricted<T>(identifier, name);
		}

		/**
		 * Tries to create a listener to the specified event type when
		 * emitted with the ID of a component of the specified type.
		 *
		 * @param identifier The unique ID number of the listener.
		 * @param data       Optional user data that gets passed with the event when emitted.
		 * @param f          The callback that receives the event.
		 *
		 * @return A struct containing the success state and an error message on failure.
		 */
		template<typename TComponent, typename TEvent>
		Status World::createListenerToEvent
		(
			const IDListener identifier,
			void *data,
			const CallbackListenToEvent<TEvent> &f,
			const Priority priority
		)
		{
			static_assert(std::is_base_of<Event, TEvent>{}, "Event type must inherit Event!");
			static_assert(std::is_base_of<Component, TComponent>{}, "Component type must inherit Component!");
			
			using namespace std::literals::string_literals;
			
			if (!Pool::hasType<TEvent>())
				return {false, "Could not create event listener: event type not registered"};
			
			if (!Pool::hasType<TComponent>())
				return {false, "Could not register event listener: component type not registered"};
			
			using L = DataEvents::DataListener<TEvent>;
			auto s(m_events.listeners.createInstance<L>(identifier, false, static_cast<IDTypeComponent>(Pool::IDOfType<TComponent>()), data, f));
			m_events.prioritiesPerListener.emplace(identifier, priority);
			
			if (!s.success)
				return {false, "Could not create event listener: "s + s.error};
			
			return {true};
		}

		/**
		 * Tries to create a listener to the specified event type
		 * regardless of what kind of component emitted it.
		 *
		 * @param identifier The unique ID number of the listener.
		 * @param data       Optional user data that gets passed with the event when emitted.
		 * @param f          The callback that receives the event.
		 *
		 * @return A struct containing the success state and an error message on failure.
		 */
		template<typename TEvent>
		Status World::createListenerToEvent
		(
			const IDListener identifier,
			void *data,
			const CallbackListenToEvent<TEvent> &f,
			const Priority priority
		)
		{
			static_assert(std::is_base_of<Event, TEvent>{}, "Event type must inherit Event!");
			
			using namespace std::literals::string_literals;
			
			if (!Pool::hasType<TEvent>())
				return {false, "Could not create event listener: event type not registered"};
			
			using L = DataEvents::DataListener<TEvent>;
			auto s(m_events.listeners.createInstance<L>(identifier, true, static_cast<IDTypeComponent>(0), data, f));
			m_events.prioritiesPerListener.emplace(identifier, priority);
			
			if (!s.success)
				return {false, "Could not create event listener: "s + s.error};
			
			return {true};
		}

		/**
		 * Tries to emit an event of the specified type immediately.
		 *
		 * @param argsForConstructor The arguments to be passed into the constructor of the event, not including the identifier.
		 *
		 * @return A struct containing the success state or an error message on failure.
		 */
		template<typename T, typename... Ts>
		Status World::emitEvent
		(
			const float dt,
			const float dtFixed,
			const float timestep,
			Ts &&... argsForConstructor
		)
		{
			static_assert(std::is_base_of<Event, T>{}, "Event type must inherit Event!");
			
			using namespace std::literals::string_literals;
			
			using L = DataEvents::DataListener<T>;
			
			if (!Pool::hasType<T>())
				return {false, "Could not emit event: type not registered"};
			
			T e
			{
				m_events.IDEventLast ++,
				std::forward<Ts>(argsForConstructor)...
			};
			
			auto ls(m_events.listeners.subpool<L>());

			if (!ls.data || 0 == ls.count)
				// Not an error, just means no listeners found.
				return {true};

			std::multimap<Priority, L *> ols;

			for (std::size_t i{0}; i < ls.count; ++ i)
			{
				auto l(ls.data + i);
				ols.emplace(m_events.prioritiesPerListener[l->identifier()], l);
			}

			for (auto &it : ols)
			{
				auto l(it.second);
				
				if (l->typeAny || e.typeComponent() == l->type)
					l->callback(*this, e, dt, dtFixed, timestep, l->data);
			}
			
			return {true};
		}

		/**
		 * Tries to queue an event of the specified type for emission on the next frame.
		 *
		 * @param argsForConstructor The arguments to be passed into the constructor of the event, not including the identifier.
		 *
		 * @return A struct containing the success state or an error message on failure.
		 */
		template<typename T, typename... Ts>
		Status World::queueEvent(Ts &&... argsForConstructor)
		{
			static_assert(std::is_base_of<Event, T>{}, "Event type must inherit Event!");
			
			using namespace std::literals::string_literals;
			
			if (!Pool::hasType<T>())
				return {false, "Could not queue event: type not registered"};
			
			auto s(m_events.events.createInstance<T>
			(
				m_events.IDEventLast ++,
				std::forward<Ts>(argsForConstructor)...
			));
			
			if (!s.success)
				return {false, "Could not queue event: "s + s.error};
			
			return {true};
		}

		/**
		 * Tries to register a new type of component available to the ECS.
		 * It immediately becomes available to all existing scenes.
		 *
		 * @param identifier          The unique ID number to assign to the component type.
		 * @param name                The name of the component type, useful for editors.
		 * @param countMaxPerEntity   The maximum number of instances of this type of component per entity (zero meaning infinite).
		 * @param countPerChunkInPool The initial maximum number of elements that can be stored and the amount by which to grow when necessary if growable.
		 * @param growable            Whether the component pool should be able to increase its size when running out of space.
		 *
		 * @return A struct containing the success state and an error message on failure.
		 */
		template<typename T>
		Status World::registerTypeOfComponentWithCountsAndGrowability
		(
			const IDTypeComponent identifier,
			const char *name,
			const std::size_t &countMaxPerEntity,
			const std::size_t &countPerChunkInPool,
			const bool growable
		)
		{
			static_assert(std::is_base_of<Component, T>{}, "Component type must inherit Component!");
			
			for (auto &scene : m_scenes)
				// Cannot access scene if it is loading asynchronously.
				if (DataScene::State::loading == scene.second->state)
					return {false, "Could not register component type: one or more scenes busy async"};
			
			if (Pool::hasType<T>())
				return {false, "Could not register component type: type already registered"};

			if (m_subpools.find(identifier) != m_subpools.end())
				return {false, "Could not register component type: ID already registered"};
			
			Pool::registerType<T>(identifier);
			
			// Generate the function to add the new type to a pool.
			auto it(m_subpools.emplace
			(
				identifier,
				DataSubpool
				{
					name,
					sizeof(T),
					countMaxPerEntity,
					countPerChunkInPool,
					growable,
					
					// Create a subpool.
					[](Pool &p, const IDTypeComponent ID, const std::size_t &count, const bool growable)
					{
						return p.createSubpoolWithCountAndGrowability<T>(ID, count, growable);
					},
					
					// Create an instance.
					[this, identifier](Pool &p, const IDComponent IDC, const IDEntity IDE) -> Result<Component *>
					{
						auto s(p.createInstance<T>(IDC, identifier, IDE));
						
						if (s.success)
						{
							queueEvent<EComponentCreated>(IDE, Pool::IDOfType<T>(), IDC);
							return {s.value, true};
						}
						else
							return {nullptr, false, std::move(s.error)};
					},
					
					// Move an instance to destination pool and remove from source.
					[](Pool &source, Pool &destination, const IDComponent ID) -> Status
					{
						auto i(source.instance<T>(ID));
						
						if (!i)
							return {false, "instance not found in source pool"};
						
						auto s(destination.emplaceInstance(std::move(*i)));
						
						if (s.success)
						{
							source.destroyInstance(ID, false);
							return {true};
						}
						
						return {false, std::move(s.error)};
					}
				}
			));
			
			// Update all the existing pools to support this new type.
			for (auto &scene : m_scenes)
			{
				auto s(addPoolToScene(*scene.second, identifier, it.first->second));
				
				if (!s.success)
					return s;
			}
			
			return {true};
		}

		/**
		 * Tries to add a type of components that the specified
		 * component type depends upon; it must alread be registered.
		 *
		 * @return A struct containing the success state and an error message on failure.
		 */
		template<typename T, typename U>
		Status World::dependentTypeOfComponent()
		{
			static_assert(std::is_base_of<Component, T>{}, "Component type must inherit Component!");
			static_assert(std::is_base_of<Component, U>{}, "Component type must inherit Component!");
			static_assert((!std::is_same<T, U>{}), "Component types must differ!");
			
			// Make sure the types have been registered.

			if (m_subpools.find(Pool::IDOfType<T>()) == m_subpools.end())
				return {false, "Could not set dependent types of component type: type not registered"};
			
			if (m_subpools.find(Pool::IDOfType<U>()) == m_subpools.end())
				return {false, "Could not set dependent types of component type: type not registered"};
			
			return dependentTypeOfComponent(Pool::IDOfType<T>(), Pool::IDOfType<U>());
		}

		/**
		 * Tries to create a component of the specified type
		 * and with the specified ID in the entity with the
		 * specified entity ID.
		 *
		 * @param IDC   The unique ID number of the component.
		 * @param IDE   The unique ID number of the entity.
		 * @param flags Optional settings for creation (everything turned on by default).
		 *
		 * @return A struct containing the success state as well as a pointer to the component on success and an error message on failure.
		 */
		template<typename T>
		Result<T *> World::createComponentInEntity
		(
			const IDComponent     IDC,
			const IDEntity        IDE,
		 	const CreateComponent flags
		)
		{
			static_assert(std::is_base_of<Component, T>{}, "Component type must inherit Component!");

			auto s(createComponentInEntity
			(
				Pool::IDOfType<T>(),
				IDC,
				IDE,
				flags
			));
			
			return {reinterpret_cast<T *>(s.value), s.success, std::move(s.error)};
		}
		
		/**
		 * Tries to find a component of the specified type in
		 * by the specified ID in any of the scenes.
		 *
		 * @param identifier The unique ID of the component.
		 *
		 * @return A pointer to the component on success or null (do not hold on to).
		 */
		template<typename T>
		T *World::component(const IDComponent identifier) const
		{
			static_assert(std::is_base_of<Component, T>{}, "Component type must inherit Component!");
			
			const auto type(Pool::IDOfType<T>());

			// Make sure the type has been registered.
			if (m_subpools.find(type) == m_subpools.end())
				return nullptr;
			
			for (auto &scene : m_scenes)
				return scene.second->pool.instance<T>(identifier);

			return nullptr;
		}

		/**
		 * Tries to find a component of the specified type in
		 * the entity by the specified ID in any of the scenes.
		 *
		 * @param identifier The unique ID of the entity.
		 *
		 * @return A pointer to the component on success or null (do not hold on to).
		 */
		template<typename T>
		T *World::componentInEntity(const IDEntity identifier) const
		{
			static_assert(std::is_base_of<Component, T>{}, "Component type must inherit Component!");
			
			const auto type(Pool::IDOfType<T>());

			// Make sure the type has been registered.
			if (m_subpools.find(type) == m_subpools.end())
				return nullptr;
			
			for (auto &scene : m_scenes)
			{
				// Find the entity in the scene.
				auto it(scene.second->entities.find(identifier));
				if (it != scene.second->entities.end())
				{
					// Find the component type in the entity.
					auto jt(it->second.components.find(type));
					if (jt != it->second.components.end())
					{
						// Find the actual component in the pool.
						return scene.second->pool.instance<T>(jt->second.ID);
					}
				}
			}

			return nullptr;
		}
		
		template<typename T>
		T *World::componentInEntity
		(
			const IDComponent IDC,
			const IDEntity    IDE
		) const
		{
			static_assert(std::is_base_of<Component, T>{}, "Component type must inherit Component!");
			
			const auto type(Pool::IDOfType<T>());

			// Make sure the type has been registered.
			if (m_subpools.find(type) == m_subpools.end())
				return nullptr;
			
			for (auto &scene : m_scenes)
			{
				// Find the entity in the scene.
				auto it(scene.second->entities.find(IDE));
				if (it != scene.second->entities.end())
				{
					// Find the component type in the entity.
					auto jt(it->second.components.equal_range(type));
					if (jt.first != jt.second)
					{
						auto kt(std::find_if(jt.first, jt.second, [&IDC](const auto &v)
						{
							return (v.second.ID == IDC);
						}));
						
						if (kt != jt.second)
							// Find the actual component in the pool.
							return scene.second->pool.instance<T>(kt->second.ID);
					}
				}
			}

			return nullptr;
		}
		
		/**
		 * Tries to find all components of the specified type in
		 * the entity by the specified ID in any of the scenes.
		 *
		 * @param identifier The unique ID of the entity.
		 *
		 * @return A vector containing pointers to components found (do not hold on to).
		 */
		template<typename T>
		std::vector<T *> World::componentsInEntity(const IDEntity identifier) const
		{
			static_assert(std::is_base_of<Component, T>{}, "Component type must inherit Component!");
			
			const auto type(Pool::IDOfType<T>());
			
			std::vector<T *> r;

			// Make sure the type has been registered.
			if (m_subpools.find(type) == m_subpools.end())
				return r;
			
			for (auto &scene : m_scenes)
			{
				// Find the entity in the scene.
				auto it(scene.second->entities.find(identifier));
				if (it != scene.second->entities.end())
				{
					// Find the component type in the entity.
					auto jt(it->second.components.equal_range(type));
					if (jt.first != jt.second)
					{
						r.reserve(std::distance(jt.first, jt.second));
						
						// Find the actual components in the pool.
						for (auto kt(jt.first); kt != jt.second; ++ kt)
						{
							auto s(scene.second->pool.instance<T>(kt->second.ID));
							if (s)
								r.emplace_back(s);
						}
					}
				}
			}
			
			return r;
		}

		/**
		 * Tries to find all components by the specified type in
		 * all currently active scenes that are not busy with async.
		 *
		 * @return A list of pointers to the components found (do not hold on to them).
		 */
		template<typename T>
		std::vector<T *> World::componentsInActiveScenes() const
		{
			static_assert(std::is_base_of<Component, T>{}, "Component type must inherit Component!");
			
			const auto type(Pool::IDOfType<T>());

			std::vector<T *> r;

			// Make sure the type has been registered.
			if (m_subpools.find(type) == m_subpools.end())
				return r;
			
			for (auto &scene : m_scenes)
			{
				if (DataScene::State::loading == scene.second->state)
					continue;
				
				if (!scene.second->active)
					continue;
				
				auto s(scene.second->pool.subpool<T>());
				
				if (0 != s.count && s.data)
					for (std::size_t i{0}; i < s.count; ++ i)
						r.emplace_back(s.data + i);
			}
			
			return r;
		}
		
		template<typename T>
		std::vector<T *> World::componentsInScene(const IDScene ID) const
		{
			static_assert(std::is_base_of<Component, T>{}, "Component type must inherit Component!");
			
			const auto type(Pool::IDOfType<T>());

			std::vector<T *> r;

			// Make sure the type has been registered.
			if (m_subpools.find(type) == m_subpools.end())
				return r;
			
			auto it(m_scenes.find(ID));
			
			if (it != m_scenes.end())
			{
				auto s(it->second->pool.subpool<T>());
				
				if (0 != s.count && s.data)
					for (std::size_t i{0}; i < s.count; ++ i)
						r.emplace_back(s.data + i);
			}
			
			return r;
		}
		
		template<typename T>
		std::vector<Pool::ViewSubpool<T>> World::componentpoolsInActiveScenes() const
		{
			static_assert(std::is_base_of<Component, T>{}, "Component type must inherit Component!");
			
			const auto type(Pool::IDOfType<T>());

			std::vector<Pool::ViewSubpool<T>> r;

			// Make sure the type has been registered.
			if (m_subpools.find(type) == m_subpools.end())
				return r;
			
			for (auto &scene : m_scenes)
			{
				if (DataScene::State::loading == scene.second->state)
					continue;
				
				if (!scene.second->active)
					continue;
				
				auto s(scene.second->pool.subpool<T>());
				
				if (0 != s.count && s.data)
					r.emplace_back(std::move(s));
			}
			
			return r;
		}

		/*
		 * This does the actual event type registration but with
		 * one less check than the public function: whether the ID
		 * is restricted, so it can be used privately to register
		 * those types that are restricted!
		 */
		template<typename T>
		Status World::registerTypeOfEventRestricted(const IDTypeEvent identifier, const char *name)
		{
			static_assert(std::is_base_of<Event, T>{}, "Event type must inherit Event!");
			
			using namespace std::literals::string_literals;
			
			using L = DataEvents::DataListener<T>;
			
			if (Pool::hasType<T>())
				return {false, "Could not register event type: type already registered"};

			if (m_typesEvents.find(identifier) != m_typesEvents.end())
				return {false, "Could not register event type: ID already registered"};
			
			{
				auto s(m_events.events.createSubpoolWithCountAndGrowability<T>
				(
					identifier,
					128, /// @todo: Name or make magic number configurable?
					true
				));
				
				if (!s.success)
					return {false, "Could not register event type: "s + s.error};
			}
			
			{
				auto s(m_events.listeners.createSubpoolWithCountAndGrowability<L>
				(
					identifier,
					32, /// @todo: Name or make magic number configurable?
					true
				));
				
				if (!s.success)
					return {false, "Could not register event type: "s + s.error};
			}
			
			m_typesEvents.emplace
			(
				identifier,
				DataTypeEvent
				{
					name,
					
					// Dispatch event.
					[this](DataEvents &d, const float dt, const float dtFixed, const float timestep)
					{
						auto ls(d.listeners.subpool<L>());
						auto es(d.events.subpool<T>());
						
						if (!ls.data || !es.data)
							return;
						
						if (0 == ls.count || 0 == es.count)
							return;
						
						// Copy all the data so that no issues occur
						// if new events are pushed while processing these.
						
						/// @todo: Or is it better do just do std::vector with a single sort?
						
						thread_local std::multimap<Priority, std::ptrdiff_t> ols;
						ols.clear();
						
						for (std::size_t i{0}; i < ls.count; ++ i)
							ols.emplace
							(
								d.prioritiesPerListener[(ls.data + i)->identifier()],
								static_cast<std::ptrdiff_t>(i)
							);
						
						for (auto &it : ols)
						{
							// Need to fetch pools again every iteration in case new
							// events are spawned and the pool data addresses changed.
							
							auto l(ls.data + it.second);
							
							for (std::size_t j{0}; j < es.count; ++ j)
							{
								auto e(es.data + j);
								
								if (l->typeAny || e->typeComponent() == l->type)
									l->callback(*this, *e, dt, dtFixed, timestep, l->data);
								
								es = d.events.subpool<T>();
							}
							
							ls = d.listeners.subpool<L>();
						}
					}
				}
			);
			
			return {true};
		}
	}
	
	// Bitwise operations for the entity creation bitflags.
	
	inline constexpr kenno::CreateEntity operator~
	(
		const kenno::CreateEntity &v
	) noexcept
	{
		using T = std::underlying_type_t<kenno::CreateEntity>;
		return static_cast<kenno::CreateEntity>(~static_cast<T>(v));
	}

	inline constexpr kenno::CreateEntity operator|
	(
		const kenno::CreateEntity &a,
		const kenno::CreateEntity &b
	) noexcept
	{
		using T = std::underlying_type_t<kenno::CreateEntity>;
		return static_cast<kenno::CreateEntity>(static_cast<T>(a) | static_cast<T>(b));
	}
	
	inline constexpr kenno::CreateEntity operator&
	(
		const kenno::CreateEntity &a,
		const kenno::CreateEntity &b
	) noexcept
	{
		using T = std::underlying_type_t<kenno::CreateEntity>;
		return static_cast<kenno::CreateEntity>(static_cast<T>(a) & static_cast<T>(b));
	}
	
	inline constexpr kenno::CreateEntity &operator|=
	(
		kenno::CreateEntity &a,
		const kenno::CreateEntity &b
	) noexcept
	{
		return a = a | b;
	}
	
	inline constexpr kenno::CreateEntity &operator&=
	(
		kenno::CreateEntity &a,
		const kenno::CreateEntity &b
	) noexcept
	{
		return a = a & b;
	}
	
	// Bitwise operations for the component creation bitflags.
	
	inline constexpr kenno::CreateComponent operator~
	(
		const kenno::CreateComponent &v
	) noexcept
	{
		using T = std::underlying_type_t<kenno::CreateComponent>;
		return static_cast<kenno::CreateComponent>(~static_cast<T>(v));
	}

	inline constexpr kenno::CreateComponent operator|
	(
		const kenno::CreateComponent &a,
		const kenno::CreateComponent &b
	) noexcept
	{
		using T = std::underlying_type_t<kenno::CreateComponent>;
		return static_cast<kenno::CreateComponent>(static_cast<T>(a) | static_cast<T>(b));
	}
	
	inline constexpr kenno::CreateComponent operator&
	(
		const kenno::CreateComponent &a,
		const kenno::CreateComponent &b
	) noexcept
	{
		using T = std::underlying_type_t<kenno::CreateComponent>;
		return static_cast<kenno::CreateComponent>(static_cast<T>(a) & static_cast<T>(b));
	}
	
	inline constexpr kenno::CreateComponent &operator|=
	(
		kenno::CreateComponent &a,
		const kenno::CreateComponent &b
	) noexcept
	{
		return a = a | b;
	}
	
	inline constexpr kenno::CreateComponent &operator&=
	(
		kenno::CreateComponent &a,
		const kenno::CreateComponent &b
	) noexcept
	{
		return a = a & b;
	}
#endif
