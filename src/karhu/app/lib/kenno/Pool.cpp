/**
 * @author		Ava Skoog
 * @date		2018-09-22
 * @copyright	2018-2021 Ava Skoog
 */

#include "Pool.hpp"

#include <cstring> // memcpy

namespace kenno
{	
	/**
	 * Clears all instances in all subpools.
	 */
	void Pool::clearAllSubpools()
	{
		for (auto &pool : m_subpools)
		{
			for (auto &it : pool.second.lookup)
				pool.second.fDestruct(pool.second.data, it.second);
			
			pool.second.lookup.clear();
			pool.second.index = 0;
		}
	}
	
	/**
	 * Tries to clear all instances in the specified subpool.
	 *
	 * @param identifier The unique ID number of the subpool.
	 *
	 * @return Whether the pool was found and successfully cleared.
	 */
	bool Pool::clearSubpool(const ID &identifier)
	{
		auto it(m_subpools.find(identifier));
		
		if (it != m_subpools.end())
		{
			for (auto &jt : it->second.lookup)
				it->second.fDestruct(it->second.data, jt.second);
			
			it->second.lookup.clear();
			it->second.index = 0;
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Tries to destroy the instance by the specified ID.
	 *
	 * @param identifier     The ID of the instance.
	 * @param callDestructor Can be used to prevent calling the destructor if the instance is removed after having been emplaced elsewhere.
	 *
	 * @return Whether the element was successfully found and removed.
	 */
	bool Pool::destroyInstance
	(
		const Poolable::ID &identifier,
		const bool callDestructor
	)
	{
		for (auto &it : m_subpools)
		{
			auto jt(it.second.lookup.find(identifier));
			if (jt != it.second.lookup.end())
			{
				const std::ptrdiff_t i{jt->second};
				
				// Clean up.
				if (callDestructor)
					it.second.fDestruct(it.second.data, i);
				
				// Remove the destroyed instance from the lookup table.
				it.second.lookup.erase(jt);
				
				// Only perform the swap if there are more elements.
				if (it.second.index > 1)
				{
					const std::ptrdiff_t
						first{i},
						last {static_cast<std::ptrdiff_t>(it.second.index - 1)};

					if (first != last)
						it.second.fSwap(it.second.data, it.second.lookup, first, last);
				}
				
				-- it.second.index;
				
				return true;
			}
		}
	
		return false;
	}

	/**
	 * Tries to find the address of the instance by the specified ID.
	 *
	 * @param identifier The unique ID number.
	 *
	 * @return A pointer to the instance on success or null.
	 */
	void *Pool::data(const Poolable::ID &identifier)
	{
		for (auto &pool : m_subpools)
		{
			auto it(pool.second.lookup.find(identifier));
			if (it != pool.second.lookup.end())
			{
				auto data(pool.second.data.get());
				return (data + it->second * static_cast<std::ptrdiff_t>(pool.second.typesize));
			}
		}
		
		return nullptr;
	}
	
	Status Pool::growSubpool(DataSubpool &pool)
	{
		const auto newcount(pool.count + pool.startcount);
		auto data(std::make_unique<Byte[]>(newcount * pool.typesize));
		
		if (!data)
			return {false, "failed to grow pool; allocation failed"};
		
		for (std::size_t i{0}; i < pool.index; ++ i)
			pool.fMove(pool.data, data, static_cast<std::ptrdiff_t>(i));
		
		pool.data = std::move(data);
		pool.count = newcount;
		
		return {true};
	}
	
	Pool::~Pool()
	{
		clearAllSubpools();
	}
}
