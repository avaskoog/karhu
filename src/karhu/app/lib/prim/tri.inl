size_t const countIndices = 3;
size_t const countVertices = 3;
uint32_t const indices[] = { 0, 1, 2 };
float const vertices[] = { -0.5f, 0.0f, 0.5f, 0.5f, 0.0f, 0.5f, 0.0f, 0.0f, -0.5f };
float const normals[] = { 0.0f, 1.0f, -0.0f, 0.0f, 1.0f, -0.0f, 0.0f, 1.0f, -0.0f };
float const texcoords[] = { 0.0f, 1.0f, 1.0f, 1.0f, 0.5f, 0.0f };
