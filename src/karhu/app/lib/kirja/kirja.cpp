/**
 * @author		Ava Skoog
 * @date		2018-06-11
 * @copyright	2018 Ava Skoog
 */

/// @todo: Since most of the code inside the render function uses UTF-32 strings now, maybe allow for any kind of input string. Just need to update XML parser to use the same encoding.
/// @todo: Currently recalculating image bounds to fit snugly around actual rendered pixels; add options to disable or enable that and always stay within line sizes?
/// @todo: Probably have to either render chunks to a separate bitmap without alpha first and then alpha blend the entire result onto the rest of the image to get alpha blending working everywhere. Might be better to just not support text alpha for now, so disabling it for the moment.
/// @todo: Specify if Y is upside down? For OpenGL etc...
/// @todo: Top/middle/bottom vertical alignment.

#include "kirja.hpp"

#include "lib/harfbuzz/hb.h"
#include "lib/harfbuzz/hb-ot.h"

#define STB_TRUETYPE_IMPLEMENTATION
#include "lib/stb_truetype.h"
#undef STB_TRUETYPE_IMPLEMENTATION

#include "lib/minibidi.h"

#include "lib/pugixml/pugixml.hpp"

#include <sstream>
#include <fstream>
#include <vector>
#include <stack>
#include <codecvt>
#include <locale>
#include <cmath>
#include <cstring>

namespace kirja
{
	static constexpr float scalefactor{64.0f};
	static constexpr int   minmax     {1000000};
	
	static constexpr hb_script_t scriptToHB(const Script &script) noexcept
	{
		switch (script)
		{
			case Script::arabic: return HB_SCRIPT_ARABIC;
			case Script::armenian: return HB_SCRIPT_ARMENIAN;
			case Script::bengali: return HB_SCRIPT_BENGALI;
			case Script::cyrillic: return HB_SCRIPT_CYRILLIC;
			case Script::devanagari: return HB_SCRIPT_DEVANAGARI;
			case Script::georgian: return HB_SCRIPT_GEORGIAN;
			case Script::greek: return HB_SCRIPT_GREEK;
			case Script::gujarati: return HB_SCRIPT_GUJARATI;
			case Script::gurmukhi: return HB_SCRIPT_GURMUKHI;
			case Script::hangul: return HB_SCRIPT_HANGUL;
			case Script::han: return HB_SCRIPT_HAN;
			case Script::hebrew: return HB_SCRIPT_HEBREW;
			case Script::hiragana: return HB_SCRIPT_HIRAGANA;
			case Script::kannada: return HB_SCRIPT_KANNADA;
			case Script::katakana: return HB_SCRIPT_KATAKANA;
			case Script::lao: return HB_SCRIPT_LAO;
			case Script::latin: return HB_SCRIPT_LATIN;
			case Script::malayalam: return HB_SCRIPT_MALAYALAM;
			case Script::oriya: return HB_SCRIPT_ORIYA;
			case Script::tamil: return HB_SCRIPT_TAMIL;
			case Script::telugu: return HB_SCRIPT_TELUGU;
			case Script::thai: return HB_SCRIPT_THAI;
			case Script::tibetan: return HB_SCRIPT_TIBETAN;
			case Script::bopomofo: return HB_SCRIPT_BOPOMOFO;
			case Script::braille: return HB_SCRIPT_BRAILLE;
			case Script::canadianSyllabics: return HB_SCRIPT_CANADIAN_SYLLABICS;
			case Script::cherokee: return HB_SCRIPT_CHEROKEE;
			case Script::ethiopic: return HB_SCRIPT_ETHIOPIC;
			case Script::khmer: return HB_SCRIPT_KHMER;
			case Script::mongolian: return HB_SCRIPT_MONGOLIAN;
			case Script::myanmar: return HB_SCRIPT_MYANMAR;
			case Script::ogham: return HB_SCRIPT_OGHAM;
			case Script::runic: return HB_SCRIPT_RUNIC;
			case Script::sinhala: return HB_SCRIPT_SINHALA;
			case Script::syriac: return HB_SCRIPT_SYRIAC;
			case Script::thaana: return HB_SCRIPT_THAANA;
			case Script::yi: return HB_SCRIPT_YI;
			case Script::deseret: return HB_SCRIPT_DESERET;
			case Script::gothic: return HB_SCRIPT_GOTHIC;
			case Script::oldItalic: return HB_SCRIPT_OLD_ITALIC;
			case Script::buhid: return HB_SCRIPT_BUHID;
			case Script::hanunoo: return HB_SCRIPT_HANUNOO;
			case Script::tagalog: return HB_SCRIPT_TAGALOG;
			case Script::tagbanwa: return HB_SCRIPT_TAGBANWA;
			case Script::cypriot: return HB_SCRIPT_CYPRIOT;
			case Script::limbu: return HB_SCRIPT_LIMBU;
			case Script::linearB: return HB_SCRIPT_LINEAR_B;
			case Script::osmanya: return HB_SCRIPT_OSMANYA;
			case Script::shavian: return HB_SCRIPT_SHAVIAN;
			case Script::taiLe: return HB_SCRIPT_TAI_LE;
			case Script::ugaritic: return HB_SCRIPT_UGARITIC;
			case Script::buginese: return HB_SCRIPT_BUGINESE;
			case Script::coptic: return HB_SCRIPT_COPTIC;
			case Script::glagolitic: return HB_SCRIPT_GLAGOLITIC;
			case Script::kharoshthi: return HB_SCRIPT_KHAROSHTHI;
			case Script::newTaiLue: return HB_SCRIPT_NEW_TAI_LUE;
			case Script::oldPersian: return HB_SCRIPT_OLD_PERSIAN;
			case Script::sylotiNagri: return HB_SCRIPT_SYLOTI_NAGRI;
			case Script::tifinagh: return HB_SCRIPT_TIFINAGH;
			case Script::balinese: return HB_SCRIPT_BALINESE;
			case Script::cuneiform: return HB_SCRIPT_CUNEIFORM;
			case Script::nko: return HB_SCRIPT_NKO;
			case Script::phagsPa: return HB_SCRIPT_PHAGS_PA;
			case Script::phoenician: return HB_SCRIPT_PHOENICIAN;
			case Script::carian: return HB_SCRIPT_CARIAN;
			case Script::cham: return HB_SCRIPT_CHAM;
			case Script::kayahLi: return HB_SCRIPT_KAYAH_LI;
			case Script::lepcha: return HB_SCRIPT_LEPCHA;
			case Script::lycian: return HB_SCRIPT_LYCIAN;
			case Script::lydian: return HB_SCRIPT_LYDIAN;
			case Script::olChiki: return HB_SCRIPT_OL_CHIKI;
			case Script::rejang: return HB_SCRIPT_REJANG;
			case Script::saurashtra: return HB_SCRIPT_SAURASHTRA;
			case Script::sundanese: return HB_SCRIPT_SUNDANESE;
			case Script::vai: return HB_SCRIPT_VAI;
			case Script::avestan: return HB_SCRIPT_AVESTAN;
			case Script::bamum: return HB_SCRIPT_BAMUM;
			case Script::egyptianHieroglyphs: return HB_SCRIPT_EGYPTIAN_HIEROGLYPHS;
			case Script::imperialAramaic: return HB_SCRIPT_IMPERIAL_ARAMAIC;
			case Script::inscriptionalPahlavi: return HB_SCRIPT_INSCRIPTIONAL_PAHLAVI;
			case Script::inscriptionalParthian: return HB_SCRIPT_INSCRIPTIONAL_PARTHIAN;
			case Script::javanese: return HB_SCRIPT_JAVANESE;
			case Script::kaithi: return HB_SCRIPT_KAITHI;
			case Script::lisu: return HB_SCRIPT_LISU;
			case Script::meeteiMayek: return HB_SCRIPT_MEETEI_MAYEK;
			case Script::oldSouthArabian: return HB_SCRIPT_OLD_SOUTH_ARABIAN;
			case Script::oldTurkic: return HB_SCRIPT_OLD_TURKIC;
			case Script::samaritan: return HB_SCRIPT_SAMARITAN;
			case Script::taiTham: return HB_SCRIPT_TAI_THAM;
			case Script::taiViet: return HB_SCRIPT_TAI_VIET;
			case Script::batak: return HB_SCRIPT_BATAK;
			case Script::brahmi: return HB_SCRIPT_BRAHMI;
			case Script::mandaic: return HB_SCRIPT_MANDAIC;
			case Script::chakma: return HB_SCRIPT_CHAKMA;
			case Script::meroiticCursive: return HB_SCRIPT_MEROITIC_CURSIVE;
			case Script::meroiticHieroglyphs: return HB_SCRIPT_MEROITIC_HIEROGLYPHS;
			case Script::miao: return HB_SCRIPT_MIAO;
			case Script::sharada: return HB_SCRIPT_SHARADA;
			case Script::soraSompeng: return HB_SCRIPT_SORA_SOMPENG;
			case Script::takri: return HB_SCRIPT_TAKRI;
			case Script::bassaVah: return HB_SCRIPT_BASSA_VAH;
			case Script::caucasianAlbanian: return HB_SCRIPT_CAUCASIAN_ALBANIAN;
			case Script::duployan: return HB_SCRIPT_DUPLOYAN;
			case Script::elbasan: return HB_SCRIPT_ELBASAN;
			case Script::grantha: return HB_SCRIPT_GRANTHA;
			case Script::khojki: return HB_SCRIPT_KHOJKI;
			case Script::khudawadi: return HB_SCRIPT_KHUDAWADI;
			case Script::linearA: return HB_SCRIPT_LINEAR_A;
			case Script::mahajani: return HB_SCRIPT_MAHAJANI;
			case Script::manichaean: return HB_SCRIPT_MANICHAEAN;
			case Script::mendeKikakui: return HB_SCRIPT_MENDE_KIKAKUI;
			case Script::modi: return HB_SCRIPT_MODI;
			case Script::mro: return HB_SCRIPT_MRO;
			case Script::nabataean: return HB_SCRIPT_NABATAEAN;
			case Script::oldNorthArabian: return HB_SCRIPT_OLD_NORTH_ARABIAN;
			case Script::oldPermic: return HB_SCRIPT_OLD_PERMIC;
			case Script::pahawhHmong: return HB_SCRIPT_PAHAWH_HMONG;
			case Script::palmyrene: return HB_SCRIPT_PALMYRENE;
			case Script::pauCinHau: return HB_SCRIPT_PAU_CIN_HAU;
			case Script::psalterPahlavi: return HB_SCRIPT_PSALTER_PAHLAVI;
			case Script::siddham: return HB_SCRIPT_SIDDHAM;
			case Script::tirhuta: return HB_SCRIPT_TIRHUTA;
			case Script::warangCiti: return HB_SCRIPT_WARANG_CITI;
			case Script::ahom: return HB_SCRIPT_AHOM;
			case Script::anatolianHieroglyphs: return HB_SCRIPT_ANATOLIAN_HIEROGLYPHS;
			case Script::hatran: return HB_SCRIPT_HATRAN;
			case Script::multani: return HB_SCRIPT_MULTANI;
			case Script::oldHungarian: return HB_SCRIPT_OLD_HUNGARIAN;
			case Script::signwriting: return HB_SCRIPT_SIGNWRITING;
			case Script::adlam: return HB_SCRIPT_ADLAM;
			case Script::bhaiksuki: return HB_SCRIPT_BHAIKSUKI;
			case Script::marchen: return HB_SCRIPT_MARCHEN;
			case Script::osage: return HB_SCRIPT_OSAGE;
			case Script::tangut: return HB_SCRIPT_TANGUT;
			case Script::newa: return HB_SCRIPT_NEWA;
			case Script::masaramGondi: return HB_SCRIPT_MASARAM_GONDI;
			case Script::nushu: return HB_SCRIPT_NUSHU;
			case Script::soyombo: return HB_SCRIPT_SOYOMBO;
			case Script::zanabazarSquare: return HB_SCRIPT_ZANABAZAR_SQUARE;
			case Script::dogra: return HB_SCRIPT_DOGRA;
			case Script::gunjalaGondi: return HB_SCRIPT_GUNJALA_GONDI;
			case Script::hanifiRohingya: return HB_SCRIPT_HANIFI_ROHINGYA;
			case Script::makasar: return HB_SCRIPT_MAKASAR;
			case Script::medefaidrin: return HB_SCRIPT_MEDEFAIDRIN;
			case Script::oldSogdian: return HB_SCRIPT_OLD_SOGDIAN;
			case Script::sogdian: return HB_SCRIPT_SOGDIAN;
			
			default: return HB_SCRIPT_UNKNOWN;
		}
	}
	
	static std::string str32To8(const std::u32string &s)
	{
		return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.to_bytes(s);
	}

	static std::u32string str8To32(const std::string &s)
	{
		return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.from_bytes(s);
	}
	
	static std::vector<int> strToBidi(std::u32string &input)
	{
		std::vector<int> result(input.length());
		auto l2v(std::make_unique<int[]>(input.length()));

		doBidi
		(
			reinterpret_cast<CHARTYPE *>(&input[0]),
			static_cast<int>(input.size()),
			false,
			true,
			result.data(),
			l2v.get()
		);

		return result;
	}
	
	static std::vector<std::string> strToLines(const char *const input)
	{
		std::vector<std::string> result;
		std::stringstream ss;
		
		for (const char *c{input}; *c; ++ c)
		{
			ss << *c;

			if ('\n' == *(c + 1) || '\0' == *(c + 1))
			{
				if (ss.str().length() > 0)
				{
					result.emplace_back(ss.str());
					ss.str("");
				}
			}
		}

		return result;
	}
	
	static void alphablend(Colour src, Byte *dst)
	{
		// If there is nothing below the pixel previously, just copy the colour.
		if (0 == *(dst + 3))
		{
			*(dst + 0) = src.r;
			*(dst + 1) = src.g;
			*(dst + 2) = src.b;
			*(dst + 3) = src.a;
		}
		// Otherwise, proper alpha blending.
		else
		{
			const float
				sr{static_cast<float>(src.r)},
				sg{static_cast<float>(src.g)},
				sb{static_cast<float>(src.b)},
				sa{static_cast<float>(src.a) / 255.0f},
				ma{1.0f - sa},
				dr{static_cast<float>(*(dst + 0))},
				dg{static_cast<float>(*(dst + 1))},
				db{static_cast<float>(*(dst + 2))},
				da{static_cast<float>(*(dst + 3)) / 255.0f};
			
			*(dst + 0) = static_cast<Byte>(sa * sr + ma * dr);
			*(dst + 1) = static_cast<Byte>(sa * sg + ma * dg);
			*(dst + 2) = static_cast<Byte>(sa * sb + ma * db);
			*(dst + 3) = static_cast<Byte>((sa + ma * da) * 255.0f);
		}
	}
	
	static Result<Colour> parseHex(const char *const input)
	{
		std::string str{input};
							
		if (str.length() > 0 && '#' == str[0])
		{
			str = str.substr(1, str.length() - 1);

			if (str.length() == 6)
				str += "ff";

			if (str.length() == 8)
			{
				static constexpr const char *const valid{"0123456789AaBbCcDdEeFf"};

				bool success{true};
				for (const char *c{str.c_str()}; *c && success; ++ c)
				{
					bool found{false};
					for (const char *v{valid}; *v && !found; ++ v)
						if (*c == *v)
							found = true;

					if (!found)
						success = false;
				}
				
				// Do the components one by one so we don't have to worry about endianness.
				if (success)
				{
					Colour result;
					for (std::size_t i{0}; i < str.length(); i += 2)
					{
						const unsigned long dec{std::stoul(str.substr(i, 2), nullptr, 16)};
						result.components[i / 2] = dec & 0xff;
					}
					return {std::move(result), true};
				}
			}
		}

		std::stringstream err;
		err << "Formatting error: invalid hex value '" << input << "' for colour tag";
		return {{}, false, err.str().c_str()};
	}

	// Declaring this here so as not to expose any
	// of HarfBuzz or stb_truetype in header.
	struct Font::Data
	{
		std::unique_ptr<Byte[]> dataHB, dataSTB;
		hb_font_t *fontHB{nullptr};
		stbtt_fontinfo fontSTB;
		int asc, desc, gap;

		Data() = default;
		Data(Data &&) = default;
		~Data()
		{
			if (fontHB)
				hb_font_destroy(fontHB);
		}
	};
	
	void Fontbank::add(const char *name, Font *const font, const Fontweight weight)
	{
		if (!font)
			return;

		auto it(fonts.find(name));

		if (it == fonts.end())
			it = fonts.emplace(name, Fontdata{}).first;

		it->second.weights[static_cast<std::size_t>(weight)] = font;
	}
	
	void Fontbank::remove(const char *name)
	{
		auto it(fonts.find(name));
		
		if (it != fonts.end())
			fonts.erase(it);
	}
	
	// Due to the forward declared data struct, these
	// all need to be in the source file instead.
	Font::Font() {}
	Font::Font(Font &&o) : data{std::move(o.data)} {}
	Font &Font::operator=(Font &&o) { data = std::move(o.data); return *this; }
	Font::~Font() {}

	Result<Bitmap> bitmapFromInput(const char *const input, const Rendersettings &settings)
	{
		if (!input)
			return {{}, false, "Input is empty"};
		
		if (!settings.fontbank)
			return {{}, false, "No fontbank supplied"};
		
		if (0 == settings.fontbank->fonts.size())
			return {{}, false, "Empty fontbank"};
		
		Font *fontDefault{nullptr};
		{
			auto it(settings.fontbank->fonts.find(settings.font));
			
			if (it == settings.fontbank->fonts.end())
			{
				std::stringstream err;
				err << "Default font '" << settings.font << "' not found in bank";
				return {{}, false, err.str()};
			}
			
			fontDefault = it->second.weights[static_cast<std::size_t>(Fontweight::regular)];
			
			if (!fontDefault)
			{
				std::stringstream err;
				err << "Invalid font '" << settings.font << '\'';
				return {{}, false, err.str()};
			}
		}
		
		const bool horizontal{(Axis::horizontal == settings.axis)};

		// Contains settings that may change by chunk,
		// used by element structs and chunk structs alike.
		struct Settings
		{
			Font *font{nullptr};
			Colour colour;
			unsigned fontsize;
		};

		// Trackers to push and pop onto our stack as we recurse
		// through the XML tree using a loop instead of a
		// recursive function, also track of changed settings.
		struct Element
		{
			pugi::xml_node node;
			Settings settings;
		};

		// Every line of text to be rendered is separated.
		// Each line is separated into chunks with different settings.
		// Each chunk contains every glyph in it to render.
		struct Line
		{
			struct Chunk
			{
				struct Glyph
				{
					hb_codepoint_t cp;
					int x, y;
					std::size_t w, h;
				};
				
				enum class Type : std::uint8_t
				{
					basic,
					rubybase,
					rubytext
				} type{Type::basic};

				std::vector<Glyph> glyphs;
				Bitmapref *image{nullptr};
				
				float w{0.0f}, h{0.0f}, offset{0.0f};
				
				// The text is used with the bidi algorithm
				// before the glyphs are extracted.
				std::u32string text;
				
				// Each chunk has its own settings, like colour.
				Settings settings;
			};
			
			// Each line has at least one chunk.
			std::vector<Chunk> chunks{std::vector<Chunk>(1)};
			
			int
				sizeAlong{0},
				sizePerp{0},
				sizeSpace{0},
				baseline{0},
				minAlong{minmax},
				maxAlong{-minmax},
				minPerp{minmax},
				maxPerp{-minmax};
		};
		
		// Each input text will have at least one line.
		std::vector<Line> lines(1);
		
		// Set up the final buffer to render into.
		Bitmap result;
		
		// Will hold the final alignment.
		Alignment alignment{settings.alignment};

		// Time to start parsing formatting tags.
		pugi::xml_document doc;
		if (const auto status = doc.load_string
		(
			input,
			pugi::parse_default |
			// Allows use to have text nodes with only whitespace.
			pugi::parse_ws_pcdata |
			// Allows use to have top-level text nodes without an element.
			pugi::parse_fragment
		))
		{
			std::size_t lineCurr{0}, chunkCurr{0};

			// Our XML stack will keep track of current settings.
			std::stack<Element> elements;
			
			// Get the top node.
			pugi::xml_node e{doc};
			
			// Push it onto the stack along with the initial settings.
			Element elementCurr{e};
			elementCurr.settings.font = fontDefault;
			elementCurr.settings.fontsize = settings.fontsize;
			elementCurr.settings.colour = settings.colour;
			elements.emplace(elementCurr);
			
			// The first chunk will have the same settings.
			lines[0].chunks[0].settings = elementCurr.settings;

			while (e)
			{
				bool chunkIsToBeCreated{false};
				auto chunktype(Line::Chunk::Type::basic);

				if (pugi::xml_node_type::node_element == e.type())
				{
					// BEGIN ELEMENT.
					chunkIsToBeCreated = true;
					const std::string tag{e.name()};

					// Colour.
					if (tag == "col")
					{
						if (!e.first_child())
							return {{}, false, "Formatting error: colour tag must have end tag"};
						
						if (const auto hex = e.attribute("hex"))
						{
							const auto parsed{parseHex(hex.as_string())}; /// @todo: Can as_string() return null?
							
							if (!parsed.success)
								return {{}, false, std::move(parsed.error)};
							
							elementCurr.settings.colour = std::move(parsed.value);
						}
						else
							return {{}, false, "Formatting error: no value given for colour tag"};
					}
					// Font size.
					else if (tag == "size")
					{
						if (!e.first_child())
							return {{}, false, "Formatting error: size tag must have end tag"};
						
						if (const auto pt = e.attribute("pt"))
						{
							const unsigned value{pt.as_uint()};
							
							if (0 == value)
							{
								std::stringstream err;
								err << "Formatting error: invalid point value '" << pt.as_string() << "' for size tag";
								return {{}, false, err.str()};
							}
							
							elementCurr.settings.fontsize = value;
						}
						else
							return {{}, false, "Formatting error: no value given for size tag"};
					}
					// Font face.
					else if (tag == "font")
					{
						if (!settings.fontbank)
							return {{}, false, "Formatting error: font tag encountered but no bank configured"};
						
						if (!e.first_child())
							return {{}, false, "Formatting error: font tag must have end tag"};
						
						if (const auto src = e.attribute("src"))
						{
							const auto font(settings.fontbank->fonts.find(src.as_string()));
							if (font == settings.fontbank->fonts.end())
							{
								std::stringstream err;
								err << "Formatting error: font source '" << src.as_string() << "' not found in bank";
								return {{}, false, err.str()};
							}
							
							/// @todo: Use current weight according to tags.
							if (!font->second.weights[static_cast<std::size_t>(Fontweight::regular)])
							{
								std::stringstream err;
								err << "Invalid font '" << settings.font << '\'';
								return {{}, false, err.str()};
							}
							
							/// @todo: Use current weight according to tags.
							elementCurr.settings.font = font->second.weights[static_cast<std::size_t>(Fontweight::regular)];
						}
						else
							return {{}, false, "Formatting error: source attribute missing from font tag"};
					}
					// Ruby segment.
					else if (tag == "ruby")
					{
					}
					// Ruby base.
					else if (tag == "rb")
					{
						chunktype = Line::Chunk::Type::rubybase;
					}
					// Ruby text.
					else if (tag == "rt")
					{
						chunktype = Line::Chunk::Type::rubytext;
					}
					// Inline image.
					else if (tag == "img")
					{
						if (!settings.imagebank)
							return {{}, false, "Formatting error: image tag encountered but no bank configured"};
						
						if (e.first_child())
							return {{}, false, "Formatting error: image tag cannot have end tag"};
						
						if (const auto src = e.attribute("src"))
						{
							const auto image(settings.imagebank->images.find(src.as_string()));
							if (image == settings.imagebank->images.end())
							{
								std::stringstream err;
								err << "Formatting error: image source '" << src.as_string() << "' not found in bank";
								return {{}, false, err.str()};
							}
							
							// Insert an image chunk.
							Line::Chunk chunk;
							chunk.settings = elementCurr.settings;
							chunk.image = &image->second;
							chunk.text = U" "; // We need a dummy character for bidi and rechunking.
							lines[lineCurr].chunks.emplace_back(std::move(chunk));
							++ chunkCurr;
						}
						else
							return {{}, false, "Formatting error: source attribute missing from image tag"};
					}
					else
					{
						std::stringstream err;
						err << "Formatting error: unrecognised tag '" << tag << '\'';
						return {{}, false, err.str()};
					}
				}
				else if (pugi::xml_node_type::node_pcdata == e.type())
				{
					auto textlines(strToLines(e.value())); /// @todo: Error check if 0 lines?
					
					// Go through the lines and find the widest one to figure out how large to
					// make the final bitmap before rendering all the glyphs directly into it.
					for (std::size_t i{0}; i < textlines.size(); ++ i)
					{
						// Time to start on a new line?
						if ('\n' == textlines[i][0])
						{
							// Get rid of that initial line break.
							textlines[i] = textlines[i].substr(1, textlines[i].length() - 1);
							
							// Create the line and copy the current settings into its initial chunk.
							lines.emplace_back();
							lines.back().chunks[0].settings = elementCurr.settings;

							++ lineCurr;
							chunkCurr = 0;
							
							if (textlines[i].length() == 0)
								continue;
						}

						lines[lineCurr].chunks[chunkCurr].text = str8To32(textlines[i].c_str());
					}
				}

				if (e.first_child())
				{
					e = e.first_child();
					elementCurr.node = e;
					elements.emplace(elementCurr);
				}
				else if (e.next_sibling())
				{
					e = e.next_sibling();
					if (elements.size() > 0)
						elements.top().node = e;
				}
				else
				{
					e = {};
					while (elements.size() > 0 && !e)
					{
						const auto old{elements.top()};
						
						if ((e = old.node.next_sibling()))
						{
							elements.top().node = e;

							if (pugi::xml_node_type::node_element == old.node.type())
							{
								// END ELEMENT.
								const std::string tag{old.node.name()};
								elementCurr.settings = old.settings;
								chunkIsToBeCreated = true;
							}
						}
						else
							elements.pop();
					}
				}

				if (chunkIsToBeCreated)
				{
					Line::Chunk chunk;
					chunk.settings = elementCurr.settings;
					chunk.type = chunktype;
					lines[lineCurr].chunks.emplace_back(std::move(chunk));
					++ chunkCurr;
				}
			}
		}
		else
		{
			std::stringstream err;
			err << "XML error: " << status.description();
			return {{}, false, err.str()};
		}

		// Apply bidi and create new chunks.
		for (int i{0}; i < static_cast<int>(lines.size()); ++ i)
		{
			auto &line(lines[i]);

			// Concatenate the chunks in the line.
			std::u32string textline;
			std::size_t lengthTotal{0};
			for (const auto &chunk : line.chunks)
			{
				textline += chunk.text;
				lengthTotal += chunk.text.length();
			}
			
			if (0 != textline.length())
			{
				// Apply the bidirectional algorithm to the entire line.
				const auto indices(strToBidi(textline));
				
				if (lengthTotal > indices.size())
					return {{}, false, "Error applying bidirectional algorithm"};

				// Create new chunks based on the bidirectional algorithm.
				if (line.chunks.size() > 0)
				{
					std::vector<Line::Chunk> chunksNew;
					auto indexLast{indices[0]};
					Line::Chunk *chunkLast{nullptr};

					for (std::size_t j{0}; j < textline.length(); ++ j)
					{
						const auto index{indices[j]};
						auto chunk(&line.chunks[0]);
						
						// Find the chunk that holds the letter with the current index.
						std::size_t length{chunk->text.length()};
						while (index >= length)
						{
							++ chunk; /// @todo: Bounds check?
							length += chunk->text.length();
						}
						
						// If the chunk is empty, skip it.
						if (length > 0)
						{
							// Time to split off into a new chunk?
							if (0 == j || chunkLast != chunk)
							{
								chunksNew.emplace_back();
								chunksNew.back().type = chunk->type;
								chunksNew.back().settings = chunk->settings;
								chunksNew.back().image = chunk->image;
							}
							chunksNew.back().text += textline[j];
						}
						
						indexLast = index;
						chunkLast = chunk;
					}
					
					line.chunks = std::move(chunksNew);
				}
			}
			
			/// @todo: Remove when done testing ruby.
			/*
			for (const auto &c : line.chunks)
			{
				std::stringstream s;
				switch (c.type)
				{
					case Line::Chunk::Type::basic: break;
					case Line::Chunk::Type::rubybase: s << "<r>"; break;
					case Line::Chunk::Type::rubytext: s << "<rt>"; break;
				}
				if (c.image)
					s << "<img>";
				else
					s << str32To8(c.text);
				switch (c.type)
				{
					case Line::Chunk::Type::basic: break;
					case Line::Chunk::Type::rubybase: s << "</r>"; break;
					case Line::Chunk::Type::rubytext: s << "</rt>"; break;
				}
				s << " | ";
				printf("%s", s.str().c_str());
			}
			printf("\n\n");
			*/
		}
		
		// Find the perpendicular sizes per chunk and line.
		for (auto &line : lines)
		{
			/// @todo: Shouldn't the various references to baseline here be on a different axis depending on the axis setting?
			for (auto &chunk : line.chunks)
			{
				// Set up the font scale for stb_truetype and calculate some other values.
				const float
					scale    {stbtt_ScaleForMappingEmToPixels(&chunk.settings.font->data->fontSTB, chunk.settings.fontsize)},
					asc      {static_cast<float>(chunk.settings.font->data->asc) * scale},
					desc     {static_cast<float>(chunk.settings.font->data->desc) * scale},
					gap      {static_cast<float>(chunk.settings.font->data->gap) * scale},
					linesize {asc - desc + gap},
					spacesize{std::max(0.0f, linesize + static_cast<float>(settings.linespace))}, /// @todo: Make linespace a per-chunk setting?
					baseline {asc};

				const int linesizeChunk{static_cast<int>(std::round(linesize))};
				if (linesizeChunk > line.sizePerp)
					line.sizePerp = linesizeChunk;
				
				const int spacesizeChunk{static_cast<int>(std::round(spacesize))};
				if (spacesizeChunk > line.sizeSpace)
					line.sizeSpace = spacesizeChunk;
				
				const int baselineChunk{static_cast<int>(std::round(baseline))};
				if (baselineChunk > line.baseline)
					line.baseline = baselineChunk;
			}
		}
		
		auto updateSize([&settings](const Line::Chunk &chunk) -> float
		{
			float fontsize{static_cast<float>(chunk.settings.fontsize)};
			
			if (Line::Chunk::Type::rubytext == chunk.type)
				fontsize *= settings.rubysize;

			// Set up the font scale for HarfBuzz.
			const int s{static_cast<int>(std::round(fontsize * scalefactor))};
			hb_font_set_scale(chunk.settings.font->data->fontHB, s, s);
			
			// Return the font scale for stb_truetype.
			return stbtt_ScaleForMappingEmToPixels(&chunk.settings.font->data->fontSTB, fontsize);
		});
		
		// The final dimensions will be stored here.
		int
			sizeAlong{0},
			sizePerp {0},
			minAlong {minmax},
			maxAlong {-minmax},
			minPerp  {minmax},
			maxPerp  {-minmax};

		// Collect the information per glyph.
		for (std::size_t i{0}; i < lines.size(); ++ i)
		{
			auto &line(lines[i]);
			
			const float
				baseline{static_cast<float>(line.baseline)},
				linesize{static_cast<float>(line.sizePerp)};

			// These keep track of the position for the whole line.
			float xCurr{0.0f}, yCurr{0.0f};

			auto updateMinMax
			(
				[
					&horizontal,
					&minAlong,
					&maxAlong,
					&minPerp,
					&maxPerp,
					&sizeAlong,
					&sizePerp,
					&xCurr,
					&yCurr,
					&line
				](const Line::Chunk::Glyph &glyph, const bool doAlong)
				{
					const int
						posAlong{static_cast<int>(sizeAlong) + ((horizontal) ? glyph.x : glyph.y)},
						posPerp
						{
							static_cast<int>(sizePerp) +
							(
								(horizontal)
								?
									(static_cast<int>(yCurr) + glyph.y)
								:
									(static_cast<int>(xCurr) + glyph.x)
							)
						};

					const int
						endPerp    {posPerp + static_cast<int>((horizontal) ? glyph.h : glyph.w)},
						minPerpNew {std::min(posPerp, endPerp)},
						maxPerpNew {std::max(posPerp, endPerp)};

					if (minPerpNew < line.minPerp)
						line.minPerp = minPerpNew;
					
					if (maxPerpNew > line.maxPerp)
						line.maxPerp = maxPerpNew;

					if (doAlong)
					{
						const int
							endAlong   {posAlong + static_cast<int>((horizontal) ? glyph.w : glyph.h)},
							minAlongNew{std::min(posAlong, endAlong)},
							maxAlongNew{std::max(posAlong, endAlong)};

						if (minAlongNew < line.minAlong)
							line.minAlong = minAlongNew;
						
						if (maxAlongNew > line.maxAlong)
							line.maxAlong = maxAlongNew;
					}
				}
			);

			for (std::size_t j{0}; j < line.chunks.size(); ++ j)
			{
				auto &chunk(line.chunks[j]);
				
				const float scale{updateSize(chunk)};
				
				// These keep track of the position for this chunk alone.
				// They are added to the positions for the line at the end.
				float xCurrChunk{0.0f}, yCurrChunk{0.0f};
				
				// Handle image chunks differently.
				if (chunk.image)
				{
					// Add a glyph to hold the information.
					Line::Chunk::Glyph glyph;

					glyph.w = chunk.image->width;
					glyph.h = chunk.image->height;

					float inlinement{0};
					const float size{static_cast<float>((horizontal) ? glyph.h : glyph.w)};
					switch (settings.inlinement)
					{
						case Inlinement::before:
						{
							inlinement = baseline - size;
							
							if (!horizontal)
								inlinement -= size;
							
							break;
						}
						
						case Inlinement::middle:
						{
							if (horizontal)
								inlinement = linesize * 0.5f;

							inlinement -= size * 0.5f;

							break;
						}

						case Inlinement::after:
						{
							inlinement = baseline;
							
							if (!horizontal)
								inlinement -= size;

							break;
						}
					}

					glyph.x = static_cast<int>(std::round(xCurr));
					glyph.y = static_cast<int>(std::round(yCurr));

					const int inlinementi{static_cast<int>(std::round(inlinement))};
					if (horizontal)
					{
						glyph.y += inlinementi;
						xCurrChunk += static_cast<float>(glyph.w);
					}
					else
					{
						if (j > 0)
						{
							const auto &prev(line.chunks[j - 1]);
							glyph.y -= prev.offset;
							if (prev.glyphs.size() > 0)
								glyph.y += static_cast<float>(prev.glyphs.back().h);
						}
							
						glyph.x += inlinementi;
						yCurrChunk += static_cast<float>(glyph.h);
					}

					updateMinMax(glyph, true);
					chunk.glyphs.emplace_back(std::move(glyph));
				}
				// Normal text chunk.
				else
				{
					hb_buffer_t *buffer{hb_buffer_create()};

					hb_buffer_add_utf32
					(
						buffer,
						reinterpret_cast<const std::uint32_t *>(chunk.text.c_str()),
						static_cast<int>(chunk.text.length()),
						0,
						static_cast<int>(chunk.text.length())
					);
					
					// Set up the sensible defaults to begin with.
					hb_buffer_guess_segment_properties(buffer);
					
					// Override any defaults depending on the settings.
					/// @todo: Figure out a better way to do this.
		/*
					if (i == 0 && Alignment::automatic == settings.alignment)
					{
						if (HB_DIRECTION_RTL == hb_buffer_get_direction(buffer))
							alignment = Alignment::right;
						else
							alignment = Alignment::left;
					}
		*/
					switch (settings.axis)
					{
						case Axis::horizontal:
							hb_buffer_set_direction(buffer, HB_DIRECTION_LTR);
							break;
						
						case Axis::vertical:
							hb_buffer_set_direction(buffer, HB_DIRECTION_TTB);
							break;
					}

					if (Script::automatic != settings.script)
					{
						const hb_script_t tag{scriptToHB(settings.script)};
						if (HB_SCRIPT_UNKNOWN != tag)
							hb_buffer_set_script(buffer, tag);
					}
					
					if (settings.language.length() > 0)
						hb_buffer_set_language
						(
							buffer,
							hb_language_from_string
							(
								settings.language.c_str(),
								static_cast<int>(settings.language.length())
							)
						);

					hb_shape(chunk.settings.font->data->fontHB, buffer, nullptr, 0);
					
					// Get the glyphs!
					unsigned count;
					hb_glyph_info_t *infos{hb_buffer_get_glyph_infos(buffer, &count)};
					hb_glyph_position_t *positions{hb_buffer_get_glyph_positions(buffer, &count)};

					if (count > 0)
					{
						for (std::size_t k{0}; k < static_cast<std::size_t>(count); ++ k)
						{
							const hb_codepoint_t cp{infos[k].codepoint};

							float
								xOff{static_cast<float>(positions[k].x_offset)  / scalefactor},
								yOff{static_cast<float>(positions[k].y_offset)  / scalefactor},
								xAdv{static_cast<float>(positions[k].x_advance) / scalefactor},
								yAdv{static_cast<float>(positions[k].y_advance) / scalefactor};

							int adv, lsb;
							stbtt_GetGlyphHMetrics(&chunk.settings.font->data->fontSTB, cp, &adv, &lsb);

							int x0, y0, x1, y1;
							stbtt_GetGlyphBitmapBox(&chunk.settings.font->data->fontSTB, cp, scale, scale, &x0, &y0, &x1, &y1);

							if (Line::Chunk::Type::rubytext == chunk.type)
							{
								if (j > 0)
								{
									const auto &prev(line.chunks[j - 1]);
									
									if (horizontal)
									{
										xOff -= prev.w;
										yOff -= chunk.settings.fontsize * 0.9f;
									}
									else
									{
										xOff += chunk.settings.fontsize * 0.75f;
//										yOff -= prev.h;// + std::abs(prev.offset - prev.h);
//										yOff -= yCurr;
//										yOff += prev.offset;
										
										/// @todo: We use this same weird calculation in two places… Function?
										//yOff -= prev.h;
//										if (prev.glyphs.size() > 0)
//											yOff -= static_cast<float>(prev.glyphs.back().h);
									}
								}
							}
							
							if (horizontal)
								yOff += baseline;
							
							const float
								xBearing{static_cast<float>(lsb) * scale},
								yBearing{static_cast<float>(y0)},
								x       {std::round(xCurr + xCurrChunk + xOff + xBearing)},
								y       {std::round(yCurr + yCurrChunk + yOff + yBearing)};

							Line::Chunk::Glyph glyph;
							glyph.cp = cp;
							glyph.x = static_cast<int>(x);
							glyph.y = static_cast<int>(y);// * ((horizontal) ? 1 : -1);
							glyph.w = static_cast<std::size_t>(x1 - x0);
							glyph.h = static_cast<std::size_t>(y1 - y0);
							updateMinMax(glyph, (Line::Chunk::Type::rubytext != chunk.type));
							chunk.h += glyph.h;
							chunk.glyphs.emplace_back(std::move(glyph));

							xCurrChunk += xAdv;
							yCurrChunk -= yAdv;
						}
					}
					else if (horizontal)
						yCurrChunk += line.sizeSpace;
					else
						xCurrChunk += line.sizeSpace;
			
					hb_buffer_destroy(buffer);
				}

				// Used to calculate ruby position.
				chunk.w = std::round(std::abs(xCurrChunk));
				
				if (chunk.glyphs.size() > 1)
					chunk.h = std::abs(chunk.glyphs.front().y - chunk.glyphs.back().y) + chunk.glyphs.back().h;

				chunk.offset = std::round(std::abs(yCurrChunk));

				// Only non-ruby advances the position.
				if (Line::Chunk::Type::rubytext != chunk.type)
				{
					xCurr += xCurrChunk;
					yCurr += yCurrChunk;
				}
				// Now that we know the width of the ruby, figure out the final positioning.
				// As things are currently implemented, we will assume the previous chunk is a ruby base.
				else if (j > 0)
				{
					auto &prev(line.chunks[j - 1]);
					
					// First align the ruby glyphs with the start of the base.
					if (!horizontal)
					{
						const int diff{chunk.glyphs[0].y - prev.glyphs[0].y};
						for (auto &glyph : chunk.glyphs)
							glyph.y -= diff;
					}

					const float
						currsize{(horizontal) ? chunk.w : chunk.h},
						prevsize{(horizontal) ? prev.w  : prev.h};
					
					const float
						min     {static_cast<float>(std::min(currsize, prevsize))},
						max     {static_cast<float>(std::max(currsize, prevsize))},
						diff    {max - min},
						halfdiff{diff * 0.5f};
					
					const int halfdiffi{static_cast<int>(std::round(halfdiff))};
					
					// If the ruby is wider than the base, center the base glyphs.
					if (currsize > prevsize)
					{
						if (horizontal)
							xCurr += diff;
						else
							yCurr += diff;
						
						for (auto &glyph : prev.glyphs)
						{
							if (horizontal)
								glyph.x += halfdiffi;
							else
								glyph.y += halfdiffi;
							
							updateMinMax(glyph, true);
						}
					}
					// If the base is wider than the ruby and there is only one glyph, center it.
					else if (chunk.glyphs.size() == 1)
					{
						for (auto &glyph : chunk.glyphs)
						{
							if (horizontal)
								glyph.x += halfdiffi;
							else
								glyph.y += halfdiffi;
							
							updateMinMax(glyph, false);
						}
					}
					// If the base is wider than the ruby, space the ruby glyphs out.
					else
					{
						const int space{static_cast<int>(std::round(diff / static_cast<float>((chunk.glyphs.size() - 1) + 0.5f)))};
						int movement{space};
						for (std::size_t k{1}; k < chunk.glyphs.size(); ++ k)
						{
							if (horizontal)
								chunk.glyphs[k].x += movement;
							else
								chunk.glyphs[k].y += movement;

							movement += space;
						}
					}
				}
			}
			
			if (line.minAlong < minAlong)
				minAlong = line.minAlong;
			
			if (line.maxAlong > maxAlong)
				maxAlong = line.maxAlong;
			
			if (line.minPerp < minPerp)
				minPerp = line.minPerp;
			
			if (line.maxPerp > maxPerp)
				maxPerp = line.maxPerp;

			line.sizeAlong = std::abs(line.maxAlong - line.minAlong);

			sizePerp += (0 == i) ? line.sizePerp : line.sizeSpace;
		}

		for (std::size_t i{0}; i < lines.size(); ++ i)
		{
			if (0 == lines[i].chunks[0].text.length())
				minPerp -= (0 == i) ? lines[i].sizePerp : lines[i].sizeSpace;
			else
				break;
		}
		
		for (int i{static_cast<int>(lines.size()) - 1}; i >= 0; -- i)
		{
			if (0 == lines[i].chunks[0].text.length())
				maxPerp += (0 == i) ? lines[i].sizePerp : lines[i].sizeSpace;
			else
				break;
		}

		sizeAlong = std::abs(maxAlong - minAlong);
		sizePerp  = std::abs(maxPerp  - minPerp);
		
		if (lines.size() > 1)
			sizePerp += settings.linespace;

		// Final size depends on axis.
		switch (settings.axis)
		{
			case Axis::horizontal:
			{
				result.width  = static_cast<std::size_t>(sizeAlong);
				result.height = static_cast<std::size_t>(sizePerp);
				break;
			}
			
			case Axis::vertical:
			{
				result.width  = static_cast<std::size_t>(sizePerp);
				result.height = static_cast<std::size_t>(sizeAlong);
				break;
			}
		}
		
		if (0 == result.count())
			result.width = result.height = 1;
		
		if (result.width >= minmax || result.height >= minmax)
			return {{}, false, "Invalid buffer size"};

		// Now that we know the full size we can create the final bitmap.
		result.buffer = std::make_unique<Byte[]>(result.count());

		// Reset the buffer.
		std::memset(result.buffer.get(), 0, result.count());
//		for (std::size_t i{0}; i < result.size(); i += 4)
//			result.buffer[i + 3] = 255;

		// Go through the lines with their glyphs and draw them into the bitmap.
		
		float offset{0.0f};
		
		const int
			offsetX{(horizontal) ? (-minAlong) : (-minPerp)},
			offsetY{(horizontal) ? (-minPerp)  : (-minAlong)},
			cc     {result.channelcount};
		
		for (std::size_t i{0}; i < lines.size(); ++ i)
		{
			for (const auto &chunk : lines[i].chunks)
			{
				const float scale{updateSize(chunk)};
				
				for (std::size_t j{0}; j < chunk.glyphs.size(); ++ j)
				{
					const auto &glyph{chunk.glyphs[j]};
					
					float xOff{0.0f}, yOff{0.0f};
					switch (settings.axis)
					{
						case Axis::horizontal:
						{
							yOff = offset;

							if (result.width != lines[i].sizeAlong)
							{
								switch (alignment)
								{
									case Alignment::left:
									//case Alignment::automatic:
										break;
										
									case Alignment::middle:
									{
										const float halfbox {static_cast<float>(result.width) * 0.5f};
										const float halfline{static_cast<float>(lines[i].sizeAlong) * 0.5f};
										xOff += static_cast<int>(std::round(halfbox - halfline));
										break;
									}
										
									case Alignment::right:
									{
										xOff += static_cast<float>(result.width - lines[i].sizeAlong);
										break;
									}
								}
							}
							
							break;
						}
						
						case Axis::vertical:
							xOff = offset;
							break;
					}

					// Unless an image chunk, try to render the glyph using stb_truetype.
					unsigned char *bm{nullptr};
					if (!chunk.image)
					{
						bm = stbtt_GetGlyphBitmap
						(
							&chunk.settings.font->data->fontSTB,
							scale,
							scale,
							glyph.cp,
							nullptr,
							nullptr,
							nullptr,
							nullptr
						);

						if (!bm)
							continue;
					}

					// RENDER THE GLYPH.
					for (int y{0}; y < glyph.h; ++ y)
					{
						for (int x{0}; x < glyph.w * cc; x += cc)
						{
							// Figure out the pixel position in the final buffer.
							int
								xReal{x + static_cast<int>(std::round(glyph.x + xOff + offsetX)) * cc},
								yReal{static_cast<int>(std::round(y + glyph.y + yOff + offsetY))};

							// Validate that we are within bounds.
							if
							(
								xReal < 0 || xReal >= (static_cast<int>(result.width) * cc) ||
								yReal < 0 || yReal >= static_cast<int>(result.height)
							)
								continue;
							
							// Get the pixels from each buffer.
							
							Byte *p{result.buffer.get() + (yReal * result.width * cc + xReal)};
							Colour col;

							if (bm)
							{
								const Byte *const c{bm + (y * glyph.w + x / cc)};
								col = chunk.settings.colour;

								// Disabling text alpha for now. Might come back later. See todo at the top of the file.
								col.a = *c; // static_cast<Byte>(static_cast<float>(col.a) * (static_cast<float>(*c) / 255.0f));
							}
							else
							{
								const Byte *const c{chunk.image->data + (y * glyph.w * cc + x)};
								col = {*(c + 0), *(c + 1), *(c + 2), *(c + 3)};
							}

							// Copy the single-channel glyph bitmap into the RGBA final one.
							alphablend(col, p); /// @todo: Allow colour customisation.
						}
					}
					
					// Since this came from stb_truetype, we will free it its way.
					if (bm)
						stbtt_FreeBitmap(bm, &chunk.settings.font->data->fontSTB.userdata);
				}
			}
			
			offset += lines[i].sizeSpace;
		}
		
		return {std::move(result), true};
	}
	
	struct Data
	{
		std::unique_ptr<Byte[]> data;
		const std::size_t size;
	};
	
	static Result<Data> dataFromFile(const char *const path)
	{
		std::stringstream err;
		
		// Try to open file.
		std::ifstream f{path, std::ifstream::binary};
		if (!f)
		{
			err << "Could not open file '" << path << "'";
			return {{}, false, err.str()};
		}
		
		// Try to get file size.
		f.seekg(0, f.end);
		const std::size_t size(f.tellg());
		if (0 == size || static_cast<int>(size) < 0)
		{
			err << "Invalid file '" << path << "'";
			return {{}, false, err.str()};
		}
		
		// Read the data.
		std::unique_ptr<Byte[]> data{std::make_unique<Byte[]>(size)};
		f.seekg(0, f.beg);
		f.read
		(
			reinterpret_cast<char *>(data.get()),
			static_cast<std::streamsize>(size)
		);
		f.close();
		
		return {Data{std::move(data), std::move(size)}, true};
	}
	
	Result<Font> fontFromFile(const char *const path)
	{
		auto data(dataFromFile(path));
		
		if (!data.success)
			return {{}, false, std::move(data.error)};

		// Now font from memory can do the rest of the job.
		auto r{fontFromMemory(data.value.data.get(), data.value.size)};
		if (r.success)
			return r;
		else
			return {{}, false, std::move(r.error)};
	}
	
	Result<Font> fontFromMemory(const Byte *const data, const std::size_t &size)
	{
		Font result;
		result.data = std::make_unique<Font::Data>();
		
		// HarfBuzz and stb_truetype both need their own copy of the buffer.
		result.data->dataHB = std::make_unique<Byte[]>(size);
		result.data->dataSTB = std::make_unique<Byte[]>(size);
		std::memcpy(result.data->dataHB.get(), data, size);
		std::memcpy(result.data->dataSTB.get(), data, size);

		// Set up the HarfBuzz stuff from the original data pointer.

		hb_blob_t *blob{hb_blob_create
		(
			reinterpret_cast<const char *>(result.data->dataHB.get()),
			static_cast<unsigned>(size),
			HB_MEMORY_MODE_READONLY,
			nullptr,
			nullptr
		)};
		
		if (!blob)
			return {{}, false, "HarfBuzz failed to create blob"};
		
		hb_face_t *face{hb_face_create(blob, 0)};
		
		if (!face)
		{
			hb_blob_destroy(blob);
			return {{}, false, "HarfBuzz failed to create face"};
		}
		
		result.data->fontHB = hb_font_create(face);
		
		if (!result.data->fontHB)
		{
			hb_blob_destroy(blob);
			hb_face_destroy(face);
			return {{}, false, "HarfBuzz failed to create font"};
		}
		
		hb_ot_font_set_funcs(result.data->fontHB);
		hb_font_set_scale
		(
			result.data->fontHB,
			static_cast<int>(scalefactor),
			static_cast<int>(scalefactor)
		);
		
		hb_blob_destroy(blob);
		hb_face_destroy(face);
		
		// Set up the stb_truetype stuff using the copy of the data.

		const int status{stbtt_InitFont
		(
			&result.data->fontSTB,
			reinterpret_cast<const unsigned char *>(result.data->dataSTB.get()),
			stbtt_GetFontOffsetForIndex
			(
				reinterpret_cast<const unsigned char *>(result.data->dataSTB.get()),
				0
			)
		)};
		
		if (0 == status)
			return {{}, false, "stb_truetype failed to initialise font"};

		stbtt_GetFontVMetrics
		(
			&result.data->fontSTB,
			&result.data->asc,
			&result.data->desc,
			&result.data->gap
		);

		return {std::move(result), true};
	}
}
