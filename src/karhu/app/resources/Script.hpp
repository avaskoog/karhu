/**
 * @author		Ava Skoog
 * @date		2019-05-27
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_RESOURCES_SCRIPT_H_
	#define KARHU_APP_RESOURCES_SCRIPT_H_

	#include <karhu/res/Script.hpp>

	namespace karhu
	{
		namespace app
		{
			class Script : public res::Script
			{
				public:
					~Script();
					
				protected:
					Status performInit
					(
						std::vector<std::string> const &extraPathsRelativeToSourcePath
					) override;
				
					Status performLoadInAdditionToMetadata
					(
						const conv::JSON::Obj *const metadata,
						const char *filepathRelativeToResourcepath,
						const char *suffix,
						conv::AdapterFilesystemRead &filesystem
					) override;
				
				private:
					std::string m_filepath;
			};
		}
	}
#endif
