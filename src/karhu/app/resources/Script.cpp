/**
 * @author		Ava Skoog
 * @date		2019-05-27
 * @copyright   2017-2023 Ava Skoog
 */

// @todo: Get rid of uses of app singleton in Script resource implementation.

#include <karhu/app/resources/Script.hpp>
#include <karhu/app/App.hpp>

namespace karhu
{
	namespace app
	{
		Script::~Script()
		{
			/// @todo: Get rid of App::instance().
			if (app::App::instance() && m_module)
				app::App::instance()->scr().unloadModule(m_module->GetName());
		}
		
		
		Status Script::performInit
		(
			std::vector<std::string> const &extraPathsRelativeToSourcePath
		)
		{
			if (extraPathsRelativeToSourcePath.size() > 1)
				return {false, "A script resource takes only one source"};
			
			if (!extraPathsRelativeToSourcePath.empty())
				m_filepath = extraPathsRelativeToSourcePath[0];
			
			return {true};
		}
		
		Status Script::performLoadInAdditionToMetadata
		(
			conv::JSON::Obj             const *const,
			char                        const *filepathRelativeToResourcepath,
			char                        const *,
			conv::AdapterFilesystemRead       &filesystem
		)
		{
			using namespace std::literals::string_literals;
			
			char const
				*const path
				{
					(!m_filepath.empty())
					?
						m_filepath.c_str()
					:
						filepathRelativeToResourcepath
				};
			
			/// @todo: Or is it better to read text for this purpose?
			auto
				contents(filesystem.readStringFromResource(path));
			
			if (!contents.success)
				return {false, std::move(contents.error)};
			
			auto const
				r(app::App::instance()->scr().loadModule
				(
					path,
					contents.value.c_str(),
					contents.value.length()
				));
			
			m_module = r.value;
			
			if (!m_module)
				return {false, std::move(r.error)};
			
			return {true};
		}
	}
}
