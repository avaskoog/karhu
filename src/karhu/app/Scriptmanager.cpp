/**
 * @author		Ava Skoog
 * @date		2019-05-27
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/Scriptmanager.hpp>
#include <karhu/app/App.hpp>
#include <karhu/core/log.hpp>
#include <karhu/core/platform.hpp>

#include <karhu/res/Material.hpp>
#include <karhu/res/Shader.hpp>
#include <karhu/res/Texture.hpp>
#include <karhu/res/Mesh.hpp>
#include <karhu/res/Rig.hpp>
#include <karhu/res/Animation.hpp>
#include <karhu/res/Scene.hpp>
#include <karhu/res/Data.hpp>
#include <karhu/res/Animationcontroller.hpp>
#include <karhu/res/Animationclips.hpp>
#include <karhu/res/Inputmap.hpp>
#include <karhu/res/Font.hpp>
#include <karhu/res/Script.hpp>

#include <karhu/app/gfx/components/Renderdata2D.hpp>
#include <karhu/app/gfx/components/Renderdata3D.hpp>
#include <karhu/app/gfx/components/Animator.hpp>
#include <karhu/app/phx/components/ColliderBox.hpp>

#include <karhu/app/inp/events/EInput.hpp>

#include <karhu/app/lib/angelscript/add_on/scriptstdstring/scriptstdstring.h>

#include <type_traits>

/// @todo: Ugly to have all these separate defines instead of using the public ones.

#define karhuAS_REG_FUNC(space, name, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if \
	( \
		m_engine->RegisterGlobalFunction \
		( \
			decl, \
			karhuAS_FUNCTION(space :: name), \
			karhuAS_CALL_CDECL \
		) < 0 \
	) \
		return false;

#define karhuAS_REG_FUNC_GENERIC(space, name, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if \
	( \
		m_engine->RegisterGlobalFunction \
		( \
			decl, \
			WRAP_FN(space :: name), \
			asCALL_GENERIC \
		) < 0 \
	) \
		return false;

#define karhuAS_REG_FUNCPR(space, name, params, ret, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if \
	( \
		m_engine->RegisterGlobalFunction \
		( \
			decl, \
			karhuAS_FUNCTIONPR(space :: name, params, ret), \
			karhuAS_CALL_CDECL \
		) < 0 \
	) \
		return false;

#define karhuAS_REG_FUNCPR_GENERIC(space, name, params, ret, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if \
	( \
		m_engine->RegisterGlobalFunction \
		( \
			decl, \
			WRAP_FN_PR(space :: name, params, ret), \
			asCALL_GENERIC \
		) < 0 \
	) \
		return false;

#define karhuAS_REG_CTOR(space, name) \
	m_engine->SetDefaultNamespace(#space); \
	if \
	( \
		m_engine->RegisterObjectBehaviour \
		( \
			#name, \
			asBEHAVE_CONSTRUCT, \
			"void f()", \
			karhuAS_FUNCTION_OBJFIRST(::karhu::AS::constructor< space :: name >), \
			karhuAS_CALL_CDECL_OBJFIRST \
		) < 0 \
	) \
		return false;

#define karhuAS_REG_CTOR_A0(space, name, a0type, asa0type) \
	m_engine->SetDefaultNamespace(#space); \
	if \
	( \
		m_engine->RegisterObjectBehaviour \
		( \
			#name, \
			asBEHAVE_CONSTRUCT, \
			"void f( " asa0type " )", \
			karhuAS_FUNCTION_OBJFIRST((::karhu::AS::constructor< space :: name , a0type >)), \
			karhuAS_CALL_CDECL_OBJFIRST \
		) < 0 \
	) \
		return false;

#define karhuAS_REG_CTOR_A0A1(space, name, a0type, a1type, asa0type, asa1type) \
	m_engine->SetDefaultNamespace(#space); \
	if \
	( \
		m_engine->RegisterObjectBehaviour \
		( \
			#name, \
			asBEHAVE_CONSTRUCT, \
			"void f( " asa0type " , " asa1type " )", \
			karhuAS_FUNCTION_OBJFIRST((::karhu::AS::constructor< space :: name , a0type , a1type >)), \
			karhuAS_CALL_CDECL_OBJFIRST \
		) < 0 \
	) \
		return false;

#define karhuAS_REG_CTOR_A0A1_same(space, name, type, astype) \
	karhuAS_REG_CTOR_A0A1(space, name, type, type, astype, astype)

#define karhuAS_REG_CTOR_A0A1A2(space, name, a0type, a1type, a2type, asa0type, asa1type, asa2type) \
	m_engine->SetDefaultNamespace(#space); \
	if \
	( \
		m_engine->RegisterObjectBehaviour \
		( \
			#name, \
			asBEHAVE_CONSTRUCT, \
			"void f( " asa0type " , " asa1type " , " asa2type " )", \
			karhuAS_FUNCTION_OBJFIRST((::karhu::AS::constructor< space :: name , a0type , a1type , a2type >)), \
			karhuAS_CALL_CDECL_OBJFIRST \
		) < 0 \
	) \
		return false;

#define karhuAS_REG_CTOR_A0A1A2_same(space, name, type, astype) \
	karhuAS_REG_CTOR_A0A1A2(space, name, type, type, type, astype, astype, astype)

#define karhuAS_REG_CTOR_A0A1A2A3(space, name, a0type, a1type, a2type, a3type, asa0type, asa1type, asa2type, asa3type) \
	m_engine->SetDefaultNamespace(#space); \
	if \
	( \
		m_engine->RegisterObjectBehaviour \
		( \
			#name, \
			asBEHAVE_CONSTRUCT, \
			"void f( " asa0type " , " asa1type " , " asa2type " , " asa3type " )", \
			karhuAS_FUNCTION_OBJFIRST((::karhu::AS::constructor< space :: name , a0type , a1type , a2type , a3type >)), \
			karhuAS_CALL_CDECL_OBJFIRST \
		) < 0 \
	) \
		return false;

#define karhuAS_REG_CTOR_A0A1A2A3_same(space, name, type, astype) \
	karhuAS_REG_CTOR_A0A1A2A3(space, name, type, type, type, type, astype, astype, astype, astype)

#define karhuAS_REG_DTOR(space, name) \
	m_engine->SetDefaultNamespace(#space); \
	if \
	( \
		m_engine->RegisterObjectBehaviour \
		( \
			#name, \
			asBEHAVE_DESTRUCT, \
			"void f()", \
			karhuAS_FUNCTION_OBJFIRST(::karhu::AS::destructor< space :: name >), \
			karhuAS_CALL_CDECL_OBJFIRST \
		) < 0 \
	) \
		return false;

#define karhuAS_REG_FUNC_n(space, name, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if \
	( \
		m_engine->RegisterGlobalFunction \
		( \
			decl, \
			karhuAS_FUNCTION(name), \
			karhuAS_CALL_CDECL \
		) < 0 \
	) \
		return false;

#define karhuAS_REG_REFTYPE(space, type) \
	{ \
		m_engine->SetDefaultNamespace(#space); \
		const int IDType{m_engine->RegisterObjectType(#type, 0, asOBJ_REF | asOBJ_NOCOUNT)}; \
		if (IDType < 0) \
			return false; \
		else \
			AS::IDForType<space::type>(IDType); \
	}

#define karhuAS_REG_REFTYPE_o(space, type, opts) \
	{ \
		m_engine->SetDefaultNamespace(#space); \
		const int IDType{m_engine->RegisterObjectType(#type, 0, asOBJ_REF | opts)}; \
		if (IDType < 0) \
			return false; \
		else \
			AS::IDForType<space::type>(IDType); \
	}

#define karhuAS_REG_VALTYPE(space, type) \
	{ \
		m_engine->SetDefaultNamespace(#space); \
		const int IDType{m_engine->RegisterObjectType(#type, sizeof(space::type), asOBJ_VALUE)}; \
		if (IDType < 0) \
			return false; \
		else \
			AS::IDForType<space::type>(IDType); \
	}

#define karhuAS_REG_VALTYPE_o(space, type, opts) \
	{ \
		m_engine->SetDefaultNamespace(#space); \
		const int IDType{m_engine->RegisterObjectType(#type, sizeof(space::type), asOBJ_VALUE | opts)}; \
		if (IDType < 0) \
			return false; \
		else \
			AS::IDForType<space::type>(IDType); \
	}

#define karhuAS_REG_TYPEDEF(space, type) \
	m_engine->SetDefaultNamespace(#space); \
	if (m_engine->RegisterTypedef(#type, AS::primname<space::type>()) < 0) \
		return false;

#define karhuAS_REG_ENUM(space, type) \
	m_engine->SetDefaultNamespace(#space); \
	if (m_engine->RegisterEnum(#type) < 0) \
		return false;

#define karhuAS_REG_METHOD(space, type, name, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if (m_engine->RegisterObjectMethod(#type, decl, karhuAS_METHOD(space :: type, name), karhuAS_CALL_THISCALL) < 0) \
		return false;

#define karhuAS_REG_METHODb(space, type, basetype, name, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if (m_engine->RegisterObjectMethod(#type, decl, karhuAS_METHOD(basetype, name), karhuAS_CALL_THISCALL) < 0) \
		return false;

#define karhuAS_REG_METHOD_GENERIC(space, type, name, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if (m_engine->RegisterObjectMethod(#type, decl, WRAP_MFN(space :: type, name), asCALL_GENERIC) < 0) \
		return false;

#define karhuAS_REG_METHODPR(space, type, name, params, ret, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if (m_engine->RegisterObjectMethod(#type, decl, karhuAS_METHODPR(space :: type, name, params, ret), karhuAS_CALL_THISCALL) < 0) \
		return false;

#define karhuAS_REG_METHODPR_OBJLAST(space, type, name, params, ret, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if (m_engine->RegisterObjectMethod(#type, decl, karhuAS_FUNCTIONPR_OBJLAST(name, params, ret), karhuAS_CALL_CDECL_OBJLAST) < 0) \
		return false;

#define karhuAS_REG_METHODPR_OBJFIRST(space, type, name, params, ret, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if (m_engine->RegisterObjectMethod(#type, decl, karhuAS_FUNCTIONPR_OBJFIRST(name, params, ret), karhuAS_CALL_CDECL_OBJFIRST) < 0) \
		return false;

#define karhuAS_REG_METHODPR_GENERIC(space, type, name, params, ret, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if (m_engine->RegisterObjectMethod(#type, decl, WRAP_MFN_PR(space :: type, name, params, ret), asCALL_GENERIC) < 0) \
		return false;

#define karhuAS_REG_METHODPRb(space, type, basetype, name, params, ret, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if (m_engine->RegisterObjectMethod(#type, decl, karhuAS_METHODPR(basetype, name, params, ret), karhuAS_CALL_THISCALL) < 0) \
		return false;

#define karhuAS_REG_METHODPR_OBJLAST_GENERIC(space, type, name, params, ret, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if (m_engine->RegisterObjectMethod(#type, decl, WRAP_OBJ_LAST_PR(name, params, ret), asCALL_GENERIC) < 0) \
		return false;

#define karhuAS_REG_METHODPR_OBJFIRST_GENERIC(space, type, name, params, ret, decl) \
	m_engine->SetDefaultNamespace(#space); \
	if (m_engine->RegisterObjectMethod(#type, decl, WRAP_OBJ_LAST_PR(name, params, ret), asCALL_GENERIC) < 0) \
		return false;

#define karhuAS_REG_PROP(space, type, ptype, name) \
	m_engine->SetDefaultNamespace(#space); \
	if (m_engine->RegisterObjectProperty(#type, ptype " " #name, asOFFSET(space :: type, name)) < 0) \
		return false;

#define karhuAS_REG_ENUMVAL(space, type, val) \
	if (m_engine->RegisterEnumValue(#type, #val, static_cast<int>(space :: type :: val)) < 0) \
		return false;

#define karhuAS_REG_REFTYPE_COUNTED(space, type) \
	{ \
		m_engine->SetDefaultNamespace(#space); \
		const int IDType{m_engine->RegisterObjectType(#type, 0, asOBJ_REF)}; \
		if (IDType < 0) \
			return false; \
		else \
			AS::IDForType<space::type>(IDType); \
		if \
		( \
			m_engine->RegisterObjectBehaviour \
			( \
				#type, \
				asBEHAVE_FACTORY, \
				#space "::" #type " @f()", \
				karhuAS_FUNCTION(::karhu::AS::factory<AS::RC< space :: type >>), \
				karhuAS_CALL_CDECL \
			) < 0 \
		) \
			return false; \
		if \
		( \
			m_engine->RegisterObjectBehaviour \
			( \
				#type, \
				asBEHAVE_ADDREF, \
				"void f()", \
				karhuAS_METHOD(::karhu::AS::RC< space :: type >, f_karhuAS_add), \
				karhuAS_CALL_THISCALL \
			) < 0 \
		) \
			return false; \
		if \
		( \
			m_engine->RegisterObjectBehaviour \
			( \
				#type, \
				asBEHAVE_RELEASE, \
				"void f()", \
				karhuAS_METHOD(::karhu::AS::RC< space :: type >, f_karhuAS_release), \
				karhuAS_CALL_THISCALL \
			) < 0 \
		) \
			return false; \
	}

#define karhuAS_REG_TYPEDATA(space, type, datacategory) \
	{ \
		m_engine->SetDefaultNamespace(#space); \
		auto info(m_engine->GetTypeInfoByName(#type)); \
		auto td(std::make_unique<Scriptmanager::Typedata>()); \
		td->category = datacategory; \
		td->fSerialise = [](conv::Serialiser &ser, const char *name, void *addr) \
		{ \
			ser << karhuINn(name, *reinterpret_cast< space :: type const * >(addr)); \
		}; \
		td->fSerialiseDirectly = [](conv::Serialiser &ser, void *addr) \
		{ \
			serialiseDirectly(ser, *reinterpret_cast< space :: type const * >(addr)); \
		}; \
		td->fDeserialise = [](const conv::Serialiser &ser, const char *name, void *addr) \
		{ \
			ser >> karhuOUTn(name, *reinterpret_cast< space :: type * >(addr)); \
		}; \
		td->fDeserialiseDirectly = [](const conv::Serialiser &ser, void *addr) \
		{ \
			deserialiseDirectly(ser, *reinterpret_cast< space :: type * >(addr)); \
		}; \
		info->SetUserData(td.get()); \
		m_typedata.emplace(info->GetTypeId(), std::move(td)); \
	}

#define karhuAS_REG_REFTYPE_WITH_DATA(space, type) \
	karhuAS_REG_REFTYPE(space, type) \
	karhuAS_REG_TYPEDATA(space, type, Scriptmanager::Typedata::Category::reference)

#define karhuAS_REG_REFTYPE_WITH_DATA_o(space, type, opts) \
	karhuAS_REG_REFTYPE_o(space, type, opts) \
	karhuAS_REG_TYPEDATA(space, type, Scriptmanager::Typedata::Category::reference)

#define karhuAS_REG_VALTYPE_WITH_DATA(space, type) \
	karhuAS_REG_VALTYPE(space, type) \
	karhuAS_REG_TYPEDATA(space, type, Scriptmanager::Typedata::Category::value)

#define karhuAS_REG_VALTYPE_WITH_DATA_o(space, type, opts) \
	karhuAS_REG_VALTYPE_o(space, type, opts) \
	karhuAS_REG_TYPEDATA(space, type, Scriptmanager::Typedata::Category::value)

namespace
{
	using namespace karhu;
	using namespace karhu::app;
	
	/// @todo: Find an AngelScript include solution for editor scripts including resource scripts etc
	static int callbackInclude
	(
		const char           *include,
		const char           *from,
		      CScriptBuilder *builder,
		      void           *data
	)
	{
		auto &app(*static_cast<App *>(data));
		
		std::string path{include}, file{from};
		
		auto
			pdirs(string::split(path, '/')),
			fdirs(string::split(file, '/'));
		
		const std::string pfile{pdirs.back()};
		
		pdirs.pop_back();
		fdirs.pop_back();
		
		for (const auto &p : pdirs)
		{
			if (p == "..")
			{
				if (fdirs.empty())
				{
					log::err("Karhu")
						<< "AngelScript: Failed to resolve include path '"
						<< include
						<< "' relative to '"
						<< from
						<< "': path strays outside resource directory";
					
					return -1;
				}
				
				fdirs.pop_back();
			}
			else
				fdirs.emplace_back(p);
		}
		
		fdirs.emplace_back(pfile);
		
		path = string::join(fdirs, '/');
		
		if (const auto contents = app.fio().readStringFromResource(path.c_str()))
			return builder->AddSectionFromMemory(path.c_str(), contents->c_str());
		else
		{
			log::err("Karhu") << "AngelScript: Failed to load include file '" << include << '\'';
			return -1;
		}
		
		return 0;
	}
	
	static void *proxyArrayviewAt(ArrayviewBasic const &o, std::int32_t const index)
	{
		return (index < o.count) ? static_cast<asBYTE *>(o.address) + (index * o.sizeElement) : nullptr;
	}

	template<typename T>
	static std::enable_if_t<conv::Serialiser::potentiallySerialisable<T>(), void>
	serialiseDirectly(conv::Serialiser &ser, T const &o)
	{
		o.serialise(ser);
	}
	
	template<typename T>
	static std::enable_if_t<!conv::Serialiser::potentiallySerialisable<T>(), void>
	serialiseDirectly(conv::Serialiser &, T const &)
	{
	}
	
	template<typename T>
	static std::enable_if_t<conv::Serialiser::potentiallyDeserialisable<T>(), void>
	deserialiseDirectly(conv::Serialiser const &ser, T &o)
	{
		o.deserialise(ser);
	}
	
	template<typename T>
	static std::enable_if_t<!conv::Serialiser::potentiallyDeserialisable<T>(), void>
	deserialiseDirectly(conv::Serialiser const &, T &)
	{
	}
	
	// Registration.
	
	static bool registerLog(Scriptmanager &);
	static bool registerArrayview(Scriptmanager &);
	static bool registerMaths(Scriptmanager &);
	static bool registerSubsystemGraphics(Scriptmanager &);
	static bool registerSubsystemAudio(Scriptmanager &);
	static bool registerSubsystemWindow(Scriptmanager &);
	static bool registerSubsystemInput(Scriptmanager &);
	static bool registerSubsystemFile(Scriptmanager &);
	static bool registerSubsystemPhysics(Scriptmanager &);
	static bool registerResources(Scriptmanager &);
	static bool registerResourceTexture(Scriptmanager &);
	static bool registerECS(Scriptmanager &);
	static bool registerEventInput(Scriptmanager &);
	static bool registerApp(Scriptmanager &);

	static bool registerComponentScriptable(Scriptmanager &);
	static bool registerComponentTransform(Scriptmanager &);
	static bool registerComponentRenderdata2D(Scriptmanager &);
	static bool registerComponentRenderdata3D(Scriptmanager &);
	static bool registerComponentAnimator(Scriptmanager &);
	static bool registerComponentParticlesystem(Scriptmanager &);
	static bool registerComponentBody(Scriptmanager &);
	static bool registerComponentsColliders(Scriptmanager &);
}

namespace karhu
{
	namespace app
	{
		Scriptmanager::Scriptmanager(App &app)
		:
		m_app{app}
		{
		}
		
		Scriptmanager::~Scriptmanager()
		{
			if (m_engine)
				m_engine->Release();
		}
		
		void Scriptmanager::logVersion()
		{
			log::msg("Karhu")
				<< "AngelScript "
				<< asGetLibraryVersion() <<
				#ifdef karhuAS_PORTABLE
				" (generic calling convention)"
				#else
				" (native calling convention)"
				#endif
				;
		}
		
		bool Scriptmanager::init()
		{
			m_engine = asCreateScriptEngine();
			
			if (!m_engine)
			{
				log::err("Karhu") << "Failed to create AngelScript engine";
				return false;
			}
			
			AS::engine = m_engine;
			
			/// @todo: Some stuff to determine whether we want to set...
//			asEP_ALLOW_UNSAFE_REFERENCES
//			asEP_ALLOW_MULTILINE_STRINGS
//			asEP_PROPERTY_ACCESSOR_MODE
//			asEP_AUTO_GARBAGE_COLLECT
//			asEP_DISALLOW_GLOBAL_VARS
//			asEP_ALWAYS_IMPL_DEFAULT_CONSTRUCT
//			asEP_COMPILER_WARNINGS
			m_engine->SetEngineProperty(asEP_REQUIRE_ENUM_SCOPE, true);
			m_engine->SetEngineProperty(asEP_USE_CHARACTER_LITERALS, true);
			
			m_engine->RegisterTypedef("char", "uint8");
			
			// For some reason this needs to use the regular asMETHOD
			// and asCALL_* regardless of calling convention to work.
			if
			(
				m_engine->SetMessageCallback
				(
					asMETHOD(Scriptmanager, callbackMessage),
					this,
					asCALL_THISCALL
				)
			 	< 0
			)
			{
				log::err("Karhu") << "Failed to register AngelScript message callback";
				return false;
			}
			
			/// @todo: Do we want a #pragma callback too?
			m_scriptbuilder.SetIncludeCallback(callbackInclude, app::App::instance()); /// @todo: Replace with app getting passed to init() above.
			
			RegisterScriptHandle(m_engine);
			
			m_engine->SetDefaultNamespace("std");
			{
				RegisterStdString(m_engine);
				m_IDTypeString = m_engine->GetTypeInfoByName("string")->GetTypeId();
				AS::IDForType<std::string>(m_IDTypeString);
				
				RegisterScriptArray(m_engine, false);
				m_IDTypeVector = m_engine->GetTypeInfoByName("vector")->GetTypeId();
			}
			m_engine->SetDefaultNamespace("");
			
			if (!registerLog(*this))
				return false;
			
			if (!registerArrayview(*this))
				return false;
			
			if (!registerMaths(*this))
				return false;
			
			if (!registerResources(*this))
				return false;
			
			if (!registerSubsystemGraphics(*this))
				return false;
			
			if (!registerSubsystemAudio(*this))
				return false;
				
			if (!registerSubsystemWindow(*this))
				return false;
				
			if (!registerSubsystemInput(*this))
				return false;
				
			if (!registerSubsystemFile(*this))
				return false;
				
			if (!registerSubsystemPhysics(*this))
				return false;
			
			if (!registerECS(*this))
				return false;
			
			if (!registerApp(*this))
				return false;
		
			m_engine->SetDefaultNamespace("");
		
			return true;
		}
		
		bool Scriptmanager::initResources()
		{
			auto &bank(app::App::instance()->res());
		
			#define karhuAS_REG_RESTYPE(type) \
				if (!registerTypeResource<res :: type>(bank.IDOfType<res :: type>(), "res::" #type)) \
					return false;
			
			karhuAS_REG_RESTYPE(Material)
			karhuAS_REG_RESTYPE(Shader)
			karhuAS_REG_RESTYPE(Texture)
			karhuAS_REG_RESTYPE(Mesh)
			karhuAS_REG_RESTYPE(Rig)
			karhuAS_REG_RESTYPE(Animation)
			karhuAS_REG_RESTYPE(Scene)
			karhuAS_REG_RESTYPE(Data)
			karhuAS_REG_RESTYPE(Animationcontroller)
			karhuAS_REG_RESTYPE(Animationclips)
			karhuAS_REG_RESTYPE(Inputmap)
			karhuAS_REG_RESTYPE(Font)
			karhuAS_REG_RESTYPE(Script)
			
			#undef karhuAS_REG_RESTYPE
			
			if (!registerResourceTexture(*this))
				return false;
			
			m_engine->SetDefaultNamespace("");
			
			return true;
		}
		
		bool Scriptmanager::initECS()
		{
			if (!registerComponentScriptable(*this))
				return false;
			
			if (!registerComponentTransform(*this))
				return false;
			
			if (!registerComponentRenderdata2D(*this))
				return false;
			
			if (!registerComponentRenderdata3D(*this))
				return false;
			
			if (!registerComponentAnimator(*this))
				return false;
			
			if (!registerComponentParticlesystem(*this))
				return false;
			
			if (!registerComponentBody(*this))
				return false;
			
			if (!registerComponentsColliders(*this))
				return false;
			
			if (!registerEventInput(*this))
				return false;
			
			m_engine->SetDefaultNamespace("");
			
			return true;
		}
		
		/// @todo: Can't get a script context pool alone working because I need to push/pop states either way a lot of the time, so don't know if there is any point to actually keeping a pool?
		
		asIScriptContext &Scriptmanager::borrowContext()
		{
			if (!m_contexts.empty())
			{
				asIScriptContext *r{m_contexts.back()};
//				m_contexts.pop();
				r->PushState();
				return *r;
			}
			
			asIScriptContext *r{m_engine->CreateContext()};
			m_contexts.push(r);
			r->PushState();
			return *r;
		}
		
		void Scriptmanager::returnContext(asIScriptContext &context)
		{
			context.PopState();
			context.Unprepare();
//			m_contexts.push(&context);
		}
		
		void Scriptmanager::serialise
		(
			asIScriptObject        &obj,
			conv::Serialiser       &ser,
			bool             const  onlyMarked
		)
		{
			const auto it(m_propsWithAttribsByType.find(obj.GetTypeId()));
		
			if (it == m_propsWithAttribsByType.end())
				return;
			
			for (const auto &prop : it->second)
			{
				if (onlyMarked)
				{
					// Should this property be deserialised at all?
					
					bool found{false};
					
					for (const auto &attr : prop.attribs)
					{
						if (attr == "[serialise]")
						{
							found = true;
							break;
						}
					}
					
					if (!found)
						continue;
				}
				
				serialise(obj, prop, ser, onlyMarked);
			}
		}
		
		void Scriptmanager::serialise
		(
			asIScriptObject        &obj,
			PropWithAttribs  const &prop,
			conv::Serialiser       &ser,
			bool             const  onlyMarked
		)
		{
			// Short names as we will be using these a lot below.
			
			char const
				*const n{prop.name.c_str()};
			
			void
				*const p{obj.GetAddressOfProperty(prop.index)};
			
			asITypeInfo const
				*const info{engine().GetTypeInfoById(prop.IDType)};
	
			// Is it a basic type?
	
			if (prop.IDType == asTYPEID_BOOL)
				ser << karhuINn(n, *reinterpret_cast<bool *>(p));
			else if (prop.IDType == asTYPEID_INT8)
				ser << karhuINn(n, *reinterpret_cast<std::int8_t *>(p));
			else if (prop.IDType == asTYPEID_INT16)
				ser << karhuINn(n, *reinterpret_cast<std::int16_t *>(p));
			else if (prop.IDType == asTYPEID_INT32 || (info && info->GetFlags() & asOBJ_ENUM))
				ser << karhuINn(n, *reinterpret_cast<std::int32_t *>(p));
			else if (prop.IDType == asTYPEID_INT64)
				ser << karhuINn(n, *reinterpret_cast<std::int64_t *>(p));
			else if (prop.IDType == asTYPEID_UINT8)
				ser << karhuINn(n, *reinterpret_cast<std::uint8_t *>(p));
			else if (prop.IDType == asTYPEID_UINT16)
				ser << karhuINn(n, *reinterpret_cast<std::uint16_t *>(p));
			else if (prop.IDType == asTYPEID_UINT32)
				ser << karhuINn(n, *reinterpret_cast<std::uint32_t *>(p));
			else if (prop.IDType == asTYPEID_UINT64)
				ser << karhuINn(n, *reinterpret_cast<std::uint64_t *>(p));
			else if (prop.IDType == asTYPEID_FLOAT)
				ser << karhuINn(n, *reinterpret_cast<float *>(p));
			else if (prop.IDType == asTYPEID_DOUBLE)
				ser << karhuINn(n, *reinterpret_cast<double *>(p));
			else if (prop.IDType == m_IDTypeString)
				ser << karhuINn(n, *reinterpret_cast<std::string *>(p));
			else if (prop.IDSubtype > 0)
			{
				auto const types(AS::IDsForTypeTemplatedWithSubtype(prop.IDType));
				
				/// @todo: Figure out how to de/serialise other script template types.
				if (types.first != m_IDTypeVector)
					return;
				
				if (!ser.target())
					return;
				
				conv::JSON::Arr arr;
				
				if (auto arrAS = reinterpret_cast<AS::Vector *>(p))
					serialise(*arrAS, arr, types.second, onlyMarked);
				
				ser.target()->push({n, std::move(arr)});
			}
			else if (prop.IDType & asTYPEID_SCRIPTOBJECT)
			{
				if (!ser.target())
					return;
				
				conv::JSON::Obj subobj;
				conv::Serialiser subser{subobj};
				serialise(*reinterpret_cast<asIScriptObject *>(p), subser, onlyMarked);
				ser.target()->push({n, std::move(subobj)});
			}
			else
			{
				// Is it a registered type?
		
				auto jt(m_typedata.find(prop.IDType));
		
				if (jt == m_typedata.end())
					return;
		
				if (!jt->second->fSerialise)
					return;
		
				if
				(
					(Typedata::Category::reference == jt->second->category &&  prop.reference) ||
					(Typedata::Category::value     == jt->second->category && !prop.reference)
				)
					jt->second->fSerialise(ser, n, p);
			}
		}
		
		void Scriptmanager::serialise
		(
			AS::Vector            &arrAS,
			conv::JSON::Arr       &arr,
			int             const  IDSubtype,
			bool            const  onlyMarked
		)
		{
			asITypeInfo const
				*const info{engine().GetTypeInfoById(IDSubtype)};
			
	 		bool const
	 			isEnum{(info && info->GetFlags() & asOBJ_ENUM)};
			
			if (AS::isPrimitiveID(IDSubtype) || isEnum)
			{
				for (asUINT i{0u}; i < arrAS.GetSize(); ++ i)
				{
					if (IDSubtype == asTYPEID_BOOL)
						arr.push(*static_cast<bool const *>(arrAS.At(i)));
					else if (IDSubtype == asTYPEID_INT8)
						arr.push(*static_cast<std::int8_t const *>(arrAS.At(i)));
					else if (IDSubtype == asTYPEID_UINT8)
						arr.push(*static_cast<std::uint8_t const *>(arrAS.At(i)));
					else if (IDSubtype == asTYPEID_INT16)
						arr.push(*static_cast<std::int16_t const *>(arrAS.At(i)));
					else if (IDSubtype == asTYPEID_UINT16)
						arr.push(*static_cast<std::uint16_t const *>(arrAS.At(i)));
					else if (IDSubtype == asTYPEID_INT32 || isEnum)
						arr.push(*static_cast<std::int32_t const *>(arrAS.At(i)));
					else if (IDSubtype == asTYPEID_UINT32)
						arr.push(*static_cast<std::uint32_t const *>(arrAS.At(i)));
					else if (IDSubtype == asTYPEID_INT64)
						arr.push(*static_cast<std::int64_t const *>(arrAS.At(i)));
					else if (IDSubtype == asTYPEID_UINT64)
						arr.push(*static_cast<std::uint64_t const *>(arrAS.At(i)));
					else if (IDSubtype == asTYPEID_FLOAT)
						arr.push(*static_cast<float const *>(arrAS.At(i)));
					else if (IDSubtype == asTYPEID_DOUBLE)
						arr.push(*static_cast<double const *>(arrAS.At(i)));
				}
			}
			// @todo: Need to check if subtype is vector in which case create CScriptArray / AS::Vector instead
			else if (auto const info = engine().GetTypeInfoById(IDSubtype))
			{
				for (asUINT i{0u}; i < arrAS.GetSize(); ++ i)
				{
					void *so{arrAS.At(i)};
					
					if (!so)
						arr.push(nullptr);
					else
					{
						auto const types(AS::IDsForTypeTemplatedWithSubtype(IDSubtype));
						
						if (types.first == m_IDTypeVector)
						{
							if (auto subarrAS = static_cast<AS::Vector *>(so))
							{
								conv::JSON::Arr subarr;
							
								serialise(*subarrAS, subarr, types.second, onlyMarked);
								arr.push(std::move(subarr));
							}
							else
								arr.push(nullptr);
						}
						else
						{
							conv::JSON::Obj subobj;
							conv::Serialiser subser{subobj};
							
							if (types.first & asTYPEID_SCRIPTOBJECT)
								serialise(*static_cast<asIScriptObject *>(so), subser, onlyMarked);
							else if (types.first & asTYPEID_APPOBJECT)
							{
								auto const it(m_typedata.find(types.first));
								
								if (it != m_typedata.end())
									it->second->fSerialiseDirectly(subser, so);
							}
							else
								so = nullptr;
							
							if (so)
								arr.push(std::move(subobj));
							else
								arr.push(nullptr);
						}
					}
				}
			}
		}
		
		void Scriptmanager::deserialise
		(
			asIScriptObject        &obj,
			conv::Serialiser const &ser,
			bool             const  onlyMarked
		)
		{
			const auto it(m_propsWithAttribsByType.find(obj.GetTypeId()));
		
			if (it == m_propsWithAttribsByType.end())
				return;
			
			for (const auto &prop : it->second)
			{
				if (onlyMarked)
				{
					// Should this property be deserialised at all?
					
					bool found{false};
					
					for (const auto &attr : prop.attribs)
					{
						if (attr == "[serialise]")
						{
							found = true;
							break;
						}
					}
					
					if (!found)
						continue;
				}
				
				deserialise(obj, prop, ser, onlyMarked);
			}
		}
		
		void Scriptmanager::deserialise
		(
			asIScriptObject        &obj,
			PropWithAttribs  const &prop,
			conv::Serialiser const &ser,
			bool             const  onlyMarked
		)
		{
			// Short names as we will be using these a lot below.
			
			char const
				*const n{prop.name.c_str()};
			
			void
				*const p{obj.GetAddressOfProperty(prop.index)};
			
			asITypeInfo const
				*const info{engine().GetTypeInfoById(prop.IDType)};
		
			// Is it a basic type?
		
			if (prop.IDType == asTYPEID_BOOL)
				ser >> karhuOUTn(n, *reinterpret_cast<bool *>(p));
			else if (prop.IDType == asTYPEID_INT8)
				ser >> karhuOUTn(n, *reinterpret_cast<std::int8_t *>(p));
			else if (prop.IDType == asTYPEID_INT16)
				ser >> karhuOUTn(n, *reinterpret_cast<std::int16_t *>(p));
			else if (prop.IDType == asTYPEID_INT32 || (info && info->GetFlags() & asOBJ_ENUM))
				ser >> karhuOUTn(n, *reinterpret_cast<std::int32_t *>(p));
			else if (prop.IDType == asTYPEID_INT64)
				ser >> karhuOUTn(n, *reinterpret_cast<std::int64_t *>(p));
			else if (prop.IDType == asTYPEID_UINT8)
				ser >> karhuOUTn(n, *reinterpret_cast<std::uint8_t *>(p));
			else if (prop.IDType == asTYPEID_UINT16)
				ser >> karhuOUTn(n, *reinterpret_cast<std::uint16_t *>(p));
			else if (prop.IDType == asTYPEID_UINT32)
				ser >> karhuOUTn(n, *reinterpret_cast<std::uint32_t *>(p));
			else if (prop.IDType == asTYPEID_UINT64)
				ser >> karhuOUTn(n, *reinterpret_cast<std::uint64_t *>(p));
			else if (prop.IDType == asTYPEID_FLOAT)
				ser >> karhuOUTn(n, *reinterpret_cast<float *>(p));
			else if (prop.IDType == asTYPEID_DOUBLE)
				ser >> karhuOUTn(n, *reinterpret_cast<double *>(p));
			else if (prop.IDType == m_IDTypeString)
				ser >> karhuOUTn(n, *reinterpret_cast<std::string *>(p));
			else if (prop.IDSubtype > 0)
			{
				auto const types(AS::IDsForTypeTemplatedWithSubtype(prop.IDType));
				
				/// @todo: Figure out how to de/serialise other script template types.
				if (types.first != m_IDTypeVector)
					return;
				
				if (!ser.target())
					return;
				
				auto const arr(ser.target()->getArray(n));
				
				if (!arr)
					return;
				
				if (auto arrAS = reinterpret_cast<AS::Vector *>(p))
					deserialise(*arrAS, *arr, types.second, onlyMarked);
			}
			else if (prop.IDType & asTYPEID_SCRIPTOBJECT)
			{
				if (!ser.target())
					return;
				
				auto const subobj(ser.target()->getObject(n));
				
				if (!subobj)
					return;
				
				conv::Serialiser subser{*subobj};
				
				deserialise(*reinterpret_cast<asIScriptObject *>(p), subser, onlyMarked);
			}
			else
			{
				// Is it a registered type?
			
				auto jt(m_typedata.find(prop.IDType));
			
				if (jt == m_typedata.end())
					return;
				
				if (!jt->second->fDeserialise)
					return;
			
				if
				(
					(Typedata::Category::reference == jt->second->category &&  prop.reference) ||
					(Typedata::Category::value     == jt->second->category && !prop.reference)
				)
					jt->second->fDeserialise(ser, n, p);
			}
		}
		
		void Scriptmanager::deserialise
		(
			AS::Vector            &arrAS,
			conv::JSON::Arr const &arr,
			int             const  IDSubtype,
			bool            const  onlyMarked
		)
		{
			asITypeInfo const
				*const info{engine().GetTypeInfoById(IDSubtype)};
			
	 		bool const
	 			isEnum{(info && info->GetFlags() & asOBJ_ENUM)};
			
	 		arrAS.RemoveRange(0, arrAS.GetSize());
		
			if (AS::isPrimitiveID(IDSubtype) || isEnum)
			{
				arrAS.Resize(static_cast<asUINT>(arr.count()));
				
				asUINT i{0};
				
				for (auto const &element : arr)
				{
					if (auto v = element.boolean())
					{
						if (IDSubtype == asTYPEID_BOOL)
							*static_cast<bool *>(arrAS.At(i)) = static_cast<bool>(*v);
					}
					else if (auto v = element.integer())
					{
						if (IDSubtype == asTYPEID_INT8)
							*static_cast<std::int8_t *>(arrAS.At(i))   = static_cast<std::int8_t>(*v);
						else if (IDSubtype == asTYPEID_UINT8)
							*static_cast<std::uint8_t *>(arrAS.At(i))  = static_cast<std::uint8_t>(*v);
						else if (IDSubtype == asTYPEID_INT16)
							*static_cast<std::int16_t *>(arrAS.At(i))  = static_cast<std::int16_t>(*v);
						else if (IDSubtype == asTYPEID_UINT16)
							*static_cast<std::uint16_t *>(arrAS.At(i)) = static_cast<std::uint16_t>(*v);
						else if (IDSubtype == asTYPEID_INT32 || isEnum)
							*static_cast<std::int32_t *>(arrAS.At(i))  = static_cast<std::int32_t>(*v);
						else if (IDSubtype == asTYPEID_UINT32)
							*static_cast<std::uint32_t *>(arrAS.At(i)) = static_cast<std::uint32_t>(*v);
						else if (IDSubtype == asTYPEID_INT64)
							*static_cast<std::int64_t *>(arrAS.At(i))  = static_cast<std::int64_t>(*v);
						else if (IDSubtype == asTYPEID_UINT64)
							*static_cast<std::uint64_t *>(arrAS.At(i)) = static_cast<std::uint64_t>(*v);
					}
					else if (auto v = element.floating())
					{
						if (IDSubtype == asTYPEID_FLOAT)
							*static_cast<float *>(arrAS.At(i))  = static_cast<float>(*v);
						else if (IDSubtype == asTYPEID_DOUBLE)
							*static_cast<double *>(arrAS.At(i)) = static_cast<double>(*v);
					}
					
					++ i;
				}
			}
			// @todo: Need to check if subtype is vector in which case create CScriptArray / AS::Vector instead
			else if (auto const info = engine().GetTypeInfoById(IDSubtype))
			{
				for (auto const &element : arr)
				{
					if (karhu::JSON::TypeValue::null == element.type())
						arrAS.InsertLast(nullptr);
					else if (void *so = engine().CreateScriptObject(info))
					{
						auto const types(AS::IDsForTypeTemplatedWithSubtype(IDSubtype));
						
						if (types.first == m_IDTypeVector)
						{
							if (element.array())
								deserialise(*static_cast<AS::Vector *>(so), *element.array(), types.second, onlyMarked);
							else
								arrAS.InsertLast(nullptr);
						}
						else
						{
							if (element.object())
							{
								conv::Serialiser const subser{*element.object()};
								
								if (types.first & asTYPEID_SCRIPTOBJECT)
									deserialise(*static_cast<asIScriptObject *>(so), subser, onlyMarked);
								else if (types.first & asTYPEID_APPOBJECT)
								{
									auto const it(m_typedata.find(types.first));
									
									if (it != m_typedata.end())
										it->second->fDeserialiseDirectly(subser, so);
								}
								else
									so = nullptr;
							}
							else
								arrAS.InsertLast(nullptr);
						}
						
						arrAS.InsertLast(so);
						
						if (so)
						{
							if (types.first == m_IDTypeVector)
								static_cast<AS::Vector *>(so)->Release();
							else if (types.first & asTYPEID_SCRIPTOBJECT)
								static_cast<asIScriptObject *>(so)->Release();
						}
					}
				}
			}
		}
		
		bool Scriptmanager::fillPropWithAttribsForType
		(
			PropWithAttribs &inProp,
			asITypeInfo const &type,
			asUINT const index
		)
		{
			std::vector<std::string> attribs
			{
				m_scriptbuilder.GetMetadataForTypeProperty
				(
					type.GetTypeId(),
					static_cast<int>(index)
				)
			};
			
			const char *name{nullptr};
			
			type.GetProperty
			(
				index,
				&name,
				&inProp.IDType,
				nullptr,
				nullptr,
				nullptr,
				&inProp.reference
			);

			if (!name)
				return false;

			inProp.index   = index;
			inProp.name    = name;
			inProp.attribs = attribs;

			asITypeInfo const
				*const proptype{m_engine->GetTypeInfoById(inProp.IDType)};

			if (proptype && proptype->GetSubTypeId() >= 0)
				inProp.IDSubtype = proptype->GetSubTypeId();
			else
				inProp.IDSubtype = -1;
			
			return true;
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Scriptmanager::edit(asIScriptObject &object, PropWithAttribs const &prop)
		{
			/// @todo: Need a special case for undo/redo here that uses AS serialisation instead of byte serialisation, so this should really return false regardless of what happens and generate its own history editor action.
			
			bool
				r{false};
			
			char const
				*const n{prop.name.c_str()};

			void
				*const p(object.GetAddressOfProperty(prop.index));
			
			/// @todo: Put m_app into Scriptmanager, remove app singleton.
			app::App &app{*app::App::instance()};

			asITypeInfo const
				*const info{app.scr().engine().GetTypeInfoById(prop.IDType)};
			
			char const
				*outName;

			int
				outValue;

			if (info && info->GetFlags() & asOBJ_ENUM)
			{
				auto value(reinterpret_cast<int *>(p));
				
				if (ImGui::BeginCombo(n, "enum hei"))
				{
					for (asUINT i{0}, ci{info->GetEnumValueCount()}; i < ci; ++ i)
					{
						outName = info->GetEnumValueByIndex(i, &outValue);
						
						bool const
							selected{(outValue == *value)};
						
						if (ImGui::Selectable(outName, selected))
						{
							*value = outValue;
							r = true;
						}
					}
					ImGui::EndCombo();
				}
			}
			else if (asTYPEID_BOOL == prop.IDType)
				r = ImGui::Checkbox(n, reinterpret_cast<bool *>(p));
			else if (AS::isPrimitiveID(prop.IDType) || m_IDTypeString == prop.IDType)
			{
				if (asTYPEID_INT8 == prop.IDType)
					ImGui::InputScalar(n, ImGuiDataType_S8, p);
				else if (asTYPEID_INT16 == prop.IDType)
					ImGui::InputScalar(n, ImGuiDataType_S16, p);
				else if (asTYPEID_INT32 == prop.IDType)
					ImGui::InputScalar(n, ImGuiDataType_S32, p);
				else if (asTYPEID_INT64 == prop.IDType)
					ImGui::InputScalar(n, ImGuiDataType_S64, p);
				else if (asTYPEID_UINT8 == prop.IDType)
					ImGui::InputScalar(n, ImGuiDataType_U8, p);
				else if (asTYPEID_UINT16 == prop.IDType)
					ImGui::InputScalar(n, ImGuiDataType_U16, p);
				else if (asTYPEID_UINT32 == prop.IDType)
					ImGui::InputScalar(n, ImGuiDataType_U32, p);
				else if (asTYPEID_UINT64 == prop.IDType)
					ImGui::InputScalar(n, ImGuiDataType_U64, p);
				else if (asTYPEID_FLOAT == prop.IDType)
					ImGui::InputScalar(n, ImGuiDataType_Float, p);
				else if (asTYPEID_DOUBLE == prop.IDType)
					ImGui::InputScalar(n, ImGuiDataType_Double, p);
				else if (m_IDTypeString == prop.IDType)
					ImGui::InputText(n, reinterpret_cast<std::string *>(p));
				
				if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
					r = true;
			}
			else if (prop.IDSubtype > 0)
			{
				ImGui::Text("TODO <> %s", n);
			}
			else if (prop.IDType & asTYPEID_SCRIPTOBJECT)
			{
				ImGui::Text("TODO scrobj %s", n);
			}
			else
			{
				ImGui::Text("TODO other %s", n);
			}
			
			return r;
		}
		#endif
		
		Result<asIScriptModule *> Scriptmanager::loadModule
		(
			const char        *name,
			const char        *code,
			const std::size_t &length
		)
		{
			using namespace std::literals::string_literals;
			
			const auto it(m_modules.find(name));
			
			if (it != m_modules.end())
				return {it->second, true};
			
			int err;
			
			err = m_scriptbuilder.StartNewModule(m_engine, name);
			
			if (err < 0)
				return
				{
					nullptr,
					false,
					"Failed to create new AngelScript module for '"s +
					name +
					'\''
				};
			
			err = m_scriptbuilder.AddSectionFromMemory
			(
				name,
				code,
				static_cast<unsigned int>(length)
			);
			
			if (err < 0)
				return
				{
					nullptr,
					false,
					"Failed to add AngelScript module from '"s +
					name +
					'\''
				};
			
			err = m_scriptbuilder.BuildModule();
			
			if (err < 0)
				return
				{
					nullptr,
					false,
					"Failed to build AngelScript module from '"s +
					name +
					'\''
				};
			
			asIScriptModule *r{m_scriptbuilder.GetModule()};
			
			for (asUINT i{0}; i < r->GetObjectTypeCount(); ++ i)
			{
				asITypeInfo *type{r->GetObjectTypeByIndex(i)};
				
				{
					std::vector<std::string> attribs
					{
						m_scriptbuilder.GetMetadataForType
						(
							type->GetTypeId()
						)
					};
					
					if (!attribs.empty())
						m_typesWithAttribs.emplace
						(
							type->GetTypeId(),
							std::move(attribs)
						);
				}
				
				std::vector<PropWithAttribs> props;
				
				for (asUINT j{0}; j < type->GetPropertyCount(); ++ j)
				{
					PropWithAttribs
						outProp;
					
					if (fillPropWithAttribsForType(outProp, *type, j))
						props.emplace_back(std::move(outProp));
				}
			
				m_propsWithAttribsByType.emplace(type->GetTypeId(), std::move(props));
			}
			
			m_modules.emplace(name, r);
			
			return {r, true};
		}
		
		void Scriptmanager::unloadModule(char const *const name)
		{
			const auto it(m_modules.find(name));
			
			if (it == m_modules.end())
				return;
			
			/// @todo: Don't use singleton, give SM an m_app.
			app::App::instance()->ecs().destroyVtableScript(name);
			m_engine->DiscardModule(name);
			
			m_modules.erase(it);
		}
		
		Status Scriptmanager::loadModuleThrowaway
		(
			char        const * const name,
			char        const * const code,
			std::size_t const         length,
			asIScriptEngine   *&      outEngine,
			asIScriptModule   *&      outModule
		)
		{
			using namespace std::literals::string_literals;
			
			int err;
			CScriptBuilder scriptbuilder;
			
			outEngine = asCreateScriptEngine();
			
			if (!outEngine)
				return
				{
					false,
					"Failed to create throwaway AngelScript engine for module '"s +
					name +
					'\''
				};
			
			err = scriptbuilder.StartNewModule(outEngine, name);
			
			if (err < 0)
			{
				unloadEngineThrowaway(*outEngine);
				outEngine = nullptr;
				return
				{
					false,
					"Failed to create throwaway AngelScript module for '"s +
					name +
					'\''
				};
			}
			
			err = scriptbuilder.AddSectionFromMemory
			(
				name,
				code,
				static_cast<unsigned int>(length)
			);
			
			if (err < 0)
			{
				unloadEngineThrowaway(*outEngine);
				outEngine = nullptr;
				return
				{
					false,
					"Failed to add throwaway AngelScript module from '"s +
					name +
					'\''
				};
			}
			
			err = scriptbuilder.BuildModule();
			
			if (err < 0)
			{
				unloadEngineThrowaway(*outEngine);
				outEngine = nullptr;
				return
				{
					false,
					"Failed to build throwaway AngelScript module from '"s +
					name +
					'\''
				};
			}
			
			outModule = scriptbuilder.GetModule();
			return {true};
		}
		
		void Scriptmanager::unloadEngineThrowaway(asIScriptEngine &engine)
		{
			if (&engine != m_engine)
				engine.Release();
		}
		
		std::vector<std::string> const *Scriptmanager::attribsForType
		(
			int const ID
		) const
		{
			auto const it(m_typesWithAttribs.find(ID));
			
			if (it != m_typesWithAttribs.end())
				return &it->second;
			
			return nullptr;
		}
		
		std::vector<Scriptmanager::PropWithAttribs> const *Scriptmanager::propsWithAttribsForType
		(
			int const ID
		) const
		{
			auto const it(m_propsWithAttribsByType.find(ID));
			
			if (it != m_propsWithAttribsByType.end())
				return &it->second;
			
			return nullptr;
		}
		
		Status Scriptmanager::loadDerivedTypeWithFactoryAndOptionalMethodsFromModule
		(
			asIScriptModule &module,
			const char *parentname,
			asITypeInfo *&outType,
			const char *parametersFactory,
			asIScriptFunction *&outFactory,
			const std::map<const char *, asIScriptFunction *&> &outFuncsByDecl
		)
		{
			using namespace std::literals::string_literals;
			
			bool found{false};
		
			const asUINT typecount{module.GetObjectTypeCount()};
			
			for (asUINT i{0}; i < typecount; ++ i)
			{
				asITypeInfo *t{module.GetObjectTypeByIndex(i)};
				
				const asUINT interfacecount(t->GetInterfaceCount());
				
				for (asUINT j{0}; j < interfacecount; ++ j)
				{
					const char *space{t->GetInterface(j)->GetNamespace()};
					
					std::string name;
					
					if (space && std::strlen(space) > 0)
					{
						name += space;
						name += "::";
					}
					
					name += t->GetInterface(j)->GetName();
					
					/// @todo: Check recursively?
					if (name == parentname)
					{
						outType = t;
						found = true;
						break;
					}
				}

				if (found)
					break;
			}
			
			if (!outType)
				return
				{
					false,
					"Failed to load AngelScript type derived from '"s +
					parentname +
					"' in '" +
					module.GetName() +
					": no child type found"
				};
			
			// Get the factory function.
			{
				std::string decl{outType->GetName()};
				decl += "@ "s + outType->GetName() + '(' + parametersFactory + ')';
			
				outFactory = outType->GetFactoryByDecl(decl.c_str());
				
				if (!outFactory)
					return
					{
						false,
						"Failed to load AngelScript type derived from '"s +
						parentname +
						"' in '" +
						module.GetName() +
						": no valid factory function found"
					};
			}
			
//			log::dbg("Karhu") << "Script contains type " << outType->GetName() << " with " << outType->GetMethodCount() << " methods:";
//
//			for (asUINT i{0}; i < outType->GetMethodCount(); ++ i)
//				log::dbg("Karhu") << (i + 1) << ": " << outType->GetMethodByIndex(i)->GetDeclaration();
			
			// Get the optional methods.
			for (const auto &it : outFuncsByDecl)
				it.second = outType->GetMethodByDecl(it.first);
			
			/// @todo: Do we want to store any user data in the type?
	
			return {true};
		}
		
		bool Scriptmanager::loadOptionalMethodsOfDerivedTypeFromModule
		(
			asIScriptModule &module,
			const char *parentname,
			const std::map<const char *, asIScriptFunction *&> &outFuncsByDecl
		)
		{
			asITypeInfo *type{nullptr};
		
			const asUINT typecount{module.GetObjectTypeCount()};
			
			for (asUINT i{0}; i < typecount; ++ i)
			{
				asITypeInfo *const t{module.GetObjectTypeByIndex(i)};
				
				const asUINT interfacecount(t->GetInterfaceCount());
				
				for (asUINT j{0}; j < interfacecount; ++ j)
				{
					/// @todo: Check recursively?
					if (0 == std::strcmp(t->GetInterface(j)->GetName(), parentname))
					{
						type = t;
						break;
					}
				}

				if (type)
					break;
			}
			
			if (!type)
				return false;
			
			// Get the optional methods.
			for (const auto &it : outFuncsByDecl)
				it.second = type->GetMethodByDecl(it.first);
			
			return true;
		}
		
		std::pair<std::string, std::string> Scriptmanager::splitSpaceAndName(std::string name)
		{
			std::string space;
		
			const std::size_t l{name.length() - 1};
		
			for (int i{static_cast<int>(l)}; i >= 0; -- i)
			{
				const auto index(static_cast<std::size_t>(i));
				if (name[index] == ':')
				{
					space = name.substr(0, index - 1);
					name = name.substr(index + 1, l);
					break;
				}
			}
			
			return {space, name};
		}
		
		void Scriptmanager::callbackMessage(const asSMessageInfo &msg)
		{
			std::stringstream s;
			
			if (msg.section && 0 != std::strlen(msg.section))
				s << msg.section << " (" << msg.row << ':' << msg.col << "): ";
			
			s << msg.message;
			
			if (asMSGTYPE_WARNING == msg.type)
				log::wrn("Karhu") << "AngelScript: " << s.str();
			else if (asMSGTYPE_ERROR == msg.type)
				log::err("Karhu") << "AngelScript: " << s.str();
			else
				log::msg("Karhu") << "AngelScript: " << s.str();
		}
	}
}

namespace
{
	using namespace karhu;
	using namespace karhu::app;
	
	static auto logLvl(const log::Level l, const std::string &s) { return new AS::RC<log::Logger>{l, s}; }
	static auto logMsg(const std::string &s) { return new AS::RC<log::Logger>{log::Level::msg, s}; }
	static auto logDbg(const std::string &s) { return new AS::RC<log::Logger>{log::Level::dbg, s}; }
	static auto logErr(const std::string &s) { return new AS::RC<log::Logger>{log::Level::err, s}; }
	static auto logWrn(const std::string &s) { return new AS::RC<log::Logger>{log::Level::wrn, s}; }
	
	bool registerLog(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		
		using namespace std::literals::string_literals;
		
		karhuAS_REG_ENUM(log, Level)
		karhuAS_REG_ENUMVAL(log, Level, msg)
		karhuAS_REG_ENUMVAL(log, Level, dbg)
		karhuAS_REG_ENUMVAL(log, Level, err)
		karhuAS_REG_ENUMVAL(log, Level, wrn)
		
		karhuAS_REG_REFTYPE_COUNTED(log, Logger)
		
		#define karhuAS_REG_LOGOP_t(type, astype) \
			karhuAS_REG_METHODPR(log, Logger, operator<<< type >, (const type &), log::Logger &, "log::Logger @+opShl(const " astype " &in)")
		
		#define karhuAS_REG_LOGOP(type) \
			karhuAS_REG_LOGOP_t(type, #type)
		
		karhuAS_REG_LOGOP_t(std::int8_t,   "int8")
		karhuAS_REG_LOGOP_t(std::int16_t,  "int16")
		karhuAS_REG_LOGOP_t(std::int32_t,  "int")
		karhuAS_REG_LOGOP_t(std::int64_t,  "int64")
		karhuAS_REG_LOGOP_t(std::uint8_t,  "uint8")
		karhuAS_REG_LOGOP_t(std::uint16_t, "uint16")
		karhuAS_REG_LOGOP_t(std::uint32_t, "uint")
		karhuAS_REG_LOGOP_t(std::uint64_t, "uint64")
		karhuAS_REG_LOGOP  (std::string)
		karhuAS_REG_LOGOP  (bool)
		karhuAS_REG_LOGOP  (float)
		karhuAS_REG_LOGOP  (double)
		
		#undef karhuAS_REG_LOGOP
		#undef karhuAS_REG_LOGOP_t
		
		/// @todo: Add all the vectors and so on...
		
		// We want the application caption to be
		// the default log tag of all log functions.
		const std::string params
		{
			"const std::string &in = \""s +
			#ifdef KARHU_APPLICATION_NAME
			KARHU_APPLICATION_NAME +
			#else
			app::App::instance()->caption() +
			#endif
			"\")"
		};
		
		m_engine->SetDefaultNamespace("log");
		
		if
		(
			m_engine->RegisterGlobalFunction
			(
				("Logger @lvl(const log::Level, "s + params).c_str(),
				karhuAS_FUNCTION(logLvl),
				karhuAS_CALL_CDECL
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterGlobalFunction
			(
				("Logger @msg("s + params).c_str(),
				karhuAS_FUNCTION(logMsg),
				karhuAS_CALL_CDECL
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterGlobalFunction
			(
				("Logger @dbg("s + params).c_str(),
				karhuAS_FUNCTION(logDbg),
				karhuAS_CALL_CDECL
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterGlobalFunction
			(
				("Logger @err("s + params).c_str(),
				karhuAS_FUNCTION(logErr),
				karhuAS_CALL_CDECL
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterGlobalFunction
			(
				("Logger @wrn("s + params).c_str(),
				karhuAS_FUNCTION(logWrn),
				karhuAS_CALL_CDECL
			) < 0
		)
			return false;
		
		return true;
	}

	bool registerArrayview(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		
		m_engine->SetDefaultNamespace("");
		
		if
		(
			m_engine->RegisterObjectType
			(
				"Arrayview<class T>",
				0,
				asOBJ_REF | asOBJ_NOCOUNT | asOBJ_TEMPLATE
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectProperty
			(
				"Arrayview<T>",
				"int count",
				asOFFSET(ArrayviewBasic, count)
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectMethod
			(
				"Arrayview<T>",
				"bool empty() const",
				karhuAS_METHOD(ArrayviewBasic, empty),
				karhuAS_CALL_THISCALL
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectMethod
			(
				"Arrayview<T>",
				"T &opIndex(const uint index)",
				karhuAS_FUNCTION(proxyArrayviewAt),
				karhuAS_CALL_CDECL_OBJFIRST
			) < 0
		 )
			return false;
		
		if
		(
			m_engine->RegisterObjectMethod
			(
				"Arrayview<T>",
				"const T &opIndex(const uint index) const",
				karhuAS_FUNCTION(proxyArrayviewAt),
				karhuAS_CALL_CDECL_OBJFIRST
			) < 0
		 )
			return false;
		
		return true;
	}

	bool registerMaths(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		std::map<int, std::unique_ptr<Scriptmanager::Typedata>> &m_typedata{scr.m_typedata}; ///! @todo: Remove when done converting to global func.
		/// @todo: Template this stuff properly.
		
		// Scalars.
	
		karhuAS_REG_TYPEDEF(mth, Scalf)
		karhuAS_REG_TYPEDEF(mth, Scali)
		karhuAS_REG_TYPEDEF(mth, Scalu)
		karhuAS_REG_TYPEDEF(mth, Radsf)
		karhuAS_REG_TYPEDEF(mth, Degsf)
		karhuAS_REG_TYPEDEF(mth, Degsi)
		karhuAS_REG_TYPEDEF(mth, Degsu)
		/*
		#define karhuAS_REG_SWIZZLE_2(vtype, suffix, first, second) \
			karhuAS_REG_METHOD(mth, vtype, first ## second, "mth::Vec2" suffix " " #first #second "() const") \
			karhuAS_REG_METHOD(mth, vtype, second ## first, "mth::Vec2" suffix " " #second #first "() const")

		#define karhuAS_REG_SWIZZLE_3(vtype, suffix, first, second, third) \
			karhuAS_REG_METHOD(mth, vtype, first ## third, "mth::Vec2" suffix " " #first #third "() const") \
			karhuAS_REG_METHOD(mth, vtype, third ## first, "mth::Vec2" suffix " " #third #first "() const") \
			karhuAS_REG_METHOD(mth, vtype, second ## third, "mth::Vec2" suffix " " #second #third "() const") \
			karhuAS_REG_METHOD(mth, vtype, third ## second, "mth::Vec2" suffix " " #third #second "() const") \
			karhuAS_REG_METHOD(mth, vtype, first ## second ## third, "mth::Vec2" suffix " " #first #second #third "()") \
			karhuAS_REG_METHOD(mth, vtype, first ## third ## second, "mth::Vec2" suffix " " #first #third #second "()") \
			karhuAS_REG_METHOD(mth, vtype, second ## first ## third, "mth::Vec2" suffix " " #second #first #third "()") \
			karhuAS_REG_METHOD(mth, vtype, second ## third ## first, "mth::Vec2" suffix " " #second #third #first "()") \
			karhuAS_REG_METHOD(mth, vtype, third ## first ## second, "mth::Vec2" suffix " " #third #first #second "()") \
			karhuAS_REG_METHOD(mth, vtype, third ## second ## first, "mth::Vec2" suffix " " #third #second #first "()") \
		*/
		
		// Preregister all the non-vector types for operator overloads.
		// Methods and properties will be filled in after vector registration.
		
		karhuAS_REG_VALTYPE_WITH_DATA_o(mth, Quatf, asOBJ_POD | asGetTypeTraits<mth::Quatf>() | asOBJ_APP_CLASS_ALLFLOATS)
		karhuAS_REG_VALTYPE_WITH_DATA_o(mth, Mat2f, asOBJ_POD | asGetTypeTraits<mth::Mat2f>() | asOBJ_APP_CLASS_ALLFLOATS)
		karhuAS_REG_VALTYPE_WITH_DATA_o(mth, Mat3f, asOBJ_POD | asGetTypeTraits<mth::Mat3f>() | asOBJ_APP_CLASS_ALLFLOATS)
		karhuAS_REG_VALTYPE_WITH_DATA_o(mth, Mat4f, asOBJ_POD | asGetTypeTraits<mth::Mat4f>() | asOBJ_APP_CLASS_ALLFLOATS)
		
		// Vectors.
		
		// Note:
		// It seems to be an oversight in GLM, but since we don't want to edit
		// that external dependency directly, we have to deal with it here;
		// the signatures for operator+ with four-dimensional vector and scalars
		// for some reason take the scalar by mutable value, whereas the two-
		// and three-dimensional ones take it by constant reference, and so we
		// have to divide the registration in the weird way below...
		
		#define karhuAS_REG_MATHOPa(vtype, otype, astype, opname, asname) \
			karhuAS_REG_METHODPR(mth, vtype, operator opname ## =, (otype), mth::vtype &, "mth::" #vtype " &op" #asname "Assign(" #astype ")")
		
		/// @todo: Using generic convention on these two regardless of overall convention since they wouldn't work properly otherwise. Considering it a temporary hack and hoping to fix it later (perhaps in a future AS version it'll magically work).
		
		#define karhuAS_REG_MATHOPl(vtype, otype, astype, opname, asname) \
			karhuAS_REG_METHODPR_OBJFIRST(mth, vtype, glm::operator opname, (const mth::vtype &, otype), mth::vtype, "mth::" #vtype " op" #asname "(" #astype ") const")
		
		#define karhuAS_REG_MATHOPr(vtype, otype, astype, opname, asname) \
			karhuAS_REG_METHODPR_OBJLAST(mth, vtype, glm::operator opname, (otype, const mth::vtype &), mth::vtype, "mth::" #vtype " op" #asname "_r(" #astype ") const")
		
		#define karhuAS_REG_MATHOPSal(vtype, otype, aotype, astype, aastype, opname, asname) \
			karhuAS_REG_MATHOPa(vtype, aotype, aastype, opname, asname) \
			karhuAS_REG_MATHOPl(vtype, otype, astype, opname, asname)
		
		#define karhuAS_REG_MATHOPSar(vtype, otype, aotype, astype, aastype, opname, asname) \
			karhuAS_REG_MATHOPa(vtype, aotype, aastype, opname, asname) \
			karhuAS_REG_MATHOPr(vtype, otype, astype, opname, asname)
		
		#define karhuAS_REG_MATHOPSlr(vtype, otype, astype, opname, asname) \
			karhuAS_REG_MATHOPl(vtype, otype, astype, opname, asname) \
			karhuAS_REG_MATHOPr(vtype, otype, astype, opname, asname)
		
		#define karhuAS_REG_MATHOPSalr(vtype, otype, aotype, astype, aastype, opname, asname) \
			karhuAS_REG_MATHOPa(vtype, aotype, aastype, opname, asname) \
			karhuAS_REG_MATHOPl(vtype, otype, astype, opname, asname) \
			karhuAS_REG_MATHOPr(vtype, otype, astype, opname, asname)
		
		#define karhuAS_REG_VEC(vtype, stype, opts) \
			karhuAS_REG_VALTYPE_WITH_DATA_o(mth, vtype, asOBJ_POD | asGetTypeTraits<mth::vtype>() | opts) \
			karhuAS_REG_CTOR(mth, vtype) \
			karhuAS_REG_CTOR_A0(mth, vtype, const mth::vtype &, "const mth::" #vtype " &in") \
			karhuAS_REG_CTOR_A0(mth, vtype, const stype, "const " #stype) \
			karhuAS_REG_METHODPR(mth, vtype, operator=, (const mth::vtype &), mth::vtype &, "mth::" #vtype " &opAssign(const mth::" #vtype " &in)") \
			karhuAS_REG_METHODPR_OBJFIRST(mth, vtype, glm::operator==, (const mth::vtype &, const mth::vtype &), bool, "bool opEquals(const mth::" #vtype " &in)") \
			karhuAS_REG_DTOR(mth, vtype) \
			{ \
				using namespace std::literals::string_literals; \
				std::string declaration{#stype " &opIndex("}; \
				declaration += AS::primname<mth::vtype::length_type>(); \
				declaration += ')'; \
				karhuAS_REG_METHODPR(mth, vtype, operator[], (mth::vtype::length_type), stype &, (declaration.c_str())) \
				declaration = "const "s + declaration + " const"; \
				karhuAS_REG_METHODPR(mth, vtype, operator[], (mth::vtype::length_type), stype &, (declaration.c_str())) \
			} \
			karhuAS_REG_MATHOPSar(vtype, const mth::vtype &, const mth::vtype &, const mth::vtype &in, const mth::vtype &in, +, Add) \
			karhuAS_REG_MATHOPSar(vtype, const mth::vtype &, const mth::vtype &, const mth::vtype &in, const mth::vtype &in, -, Sub) \
			karhuAS_REG_MATHOPSar(vtype, const mth::vtype &, const mth::vtype &, const mth::vtype &in, const mth::vtype &in, *, Mul) \
			karhuAS_REG_MATHOPSar(vtype, const mth::vtype &, const mth::vtype &, const mth::vtype &in, const mth::vtype &in, /, Div) \
			karhuAS_REG_PROP(mth, vtype, #stype, x) \
			karhuAS_REG_PROP(mth, vtype, #stype, y)
		
		#define karhuAS_REG_VEC_Z(vtype, stype) \
			karhuAS_REG_PROP(mth, vtype, #stype, z)
		
		#define karhuAS_REG_VEC_W(vtype, stype) \
			karhuAS_REG_PROP(mth, vtype, #stype, w)
		
		#define karhuAS_REG_VEC2(vtype, stype, opts) \
			karhuAS_REG_VEC(vtype, stype, opts) \
			karhuAS_REG_CTOR_A0A1_same(mth, vtype, const stype, "const " #stype) \
			karhuAS_REG_MATHOPSalr(vtype, const stype &, stype, const stype &in, stype, +, Add) \
			karhuAS_REG_MATHOPSalr(vtype, const stype &, stype, const stype &in, stype, -, Sub) \
			karhuAS_REG_MATHOPSalr(vtype, const stype &, stype, const stype &in, stype, *, Mul) \
			karhuAS_REG_MATHOPSalr(vtype, const stype &, stype, const stype &in, stype, /, Div)
		
		#define karhuAS_REG_VEC3(vtype, stype, opts) \
			karhuAS_REG_VEC(vtype, stype, opts) \
			karhuAS_REG_CTOR_A0A1A2_same(mth, vtype, const stype, "const " #stype) \
			karhuAS_REG_VEC_Z(vtype, stype) \
			karhuAS_REG_MATHOPSalr(vtype, const stype &, stype, const stype &in, stype, +, Add) \
			karhuAS_REG_MATHOPSalr(vtype, const stype &, stype, const stype &in, stype, -, Sub) \
			karhuAS_REG_MATHOPSalr(vtype, const stype &, stype, const stype &in, stype, *, Mul) \
			karhuAS_REG_MATHOPSalr(vtype, const stype &, stype, const stype &in, stype, /, Div)

		#define karhuAS_REG_VEC4(vtype, stype, opts) \
			karhuAS_REG_VEC(vtype, stype, opts) \
			karhuAS_REG_CTOR_A0A1A2A3_same(mth, vtype, const stype, "const " #stype) \
			karhuAS_REG_VEC_Z(vtype, stype) \
			karhuAS_REG_VEC_W(vtype, stype) \
			karhuAS_REG_MATHOPSalr(vtype, stype, stype, stype, stype, +, Add) \
			karhuAS_REG_MATHOPSalr(vtype, stype, stype, stype, stype, -, Sub) \
			karhuAS_REG_MATHOPSalr(vtype, stype, stype, stype, stype, *, Mul) \
			karhuAS_REG_MATHOPSalr(vtype, stype, stype, stype, stype, /, Div)
		
		karhuAS_REG_VEC2(Vec2f, mth::Scalf, asOBJ_APP_CLASS_ALLFLOATS)
		karhuAS_REG_MATHOPSlr(Vec2f, const mth::Mat2f &, mth::Mat2f, *, Mul)
		karhuAS_REG_VEC2(Vec2i, mth::Scali, asOBJ_APP_CLASS_ALLINTS)
		karhuAS_REG_VEC2(Vec2u, mth::Scalu, asOBJ_APP_CLASS_ALLINTS)
		
		karhuAS_REG_VEC3(Vec3f, mth::Scalf, asOBJ_APP_CLASS_ALLFLOATS)
		karhuAS_REG_MATHOPSlr(Vec3f, const mth::Quatf &, mth::Quatf, *, Mul)
		karhuAS_REG_MATHOPSlr(Vec3f, const mth::Mat3f &, mth::Mat3f, *, Mul)
		karhuAS_REG_VEC3(Vec3i, mth::Scali, asOBJ_APP_CLASS_ALLINTS)
		karhuAS_REG_VEC3(Vec3u, mth::Scalu, asOBJ_APP_CLASS_ALLINTS)
		
		karhuAS_REG_VEC4(Vec4f, mth::Scalf, asOBJ_APP_CLASS_ALLFLOATS)
		karhuAS_REG_MATHOPSlr(Vec4f, const mth::Quatf &, mth::Quatf, *, Mul)
		karhuAS_REG_MATHOPSlr(Vec4f, const mth::Mat4f &, mth::Mat4f, *, Mul)
		karhuAS_REG_VEC4(Vec4i, mth::Scali, asOBJ_APP_CLASS_ALLINTS)
		karhuAS_REG_VEC4(Vec4u, mth::Scalu, asOBJ_APP_CLASS_ALLINTS)
		
		#undef karhuAS_REG_VEC4
		#undef karhuAS_REG_VEC3
		#undef karhuAS_REG_VEC2
		#undef karhuAS_REG_VEC_W
		#undef karhuAS_REG_VEC_Z
		#undef karhuAS_REG_VEC
		
		// Quaternion.
		
		karhuAS_REG_CTOR_A0(mth, Quatf, const mth::Quatf &, "const mth::Quatf &in")
		karhuAS_REG_CTOR_A0A1A2A3_same(mth, Quatf, const mth::Scalf, "const mth::Scalf")
		karhuAS_REG_MATHOPSalr(Quatf, const mth::Quatf &, const mth::Quatf &, mth::Quatf, mth::Quatf, +, Add) /// @todo: Why not &in in AS?
		karhuAS_REG_MATHOPSalr(Quatf, const mth::Quatf &, const mth::Quatf &, mth::Quatf, mth::Quatf, *, Mul) /// @todo: Why not &in in AS?
		karhuAS_REG_MATHOPSalr(Quatf, const mth::Scalf &, const mth::Scalf &, mth::Scalf, mth::Scalf, *, Mul)
		karhuAS_REG_MATHOPSal(Quatf, const mth::Scalf &, const mth::Scalf &, mth::Scalf, mth::Scalf, /, Div)
		karhuAS_REG_PROP(mth, Quatf, "mth::Scalf", x)
		karhuAS_REG_PROP(mth, Quatf, "mth::Scalf", y)
		karhuAS_REG_PROP(mth, Quatf, "mth::Scalf", z)
		karhuAS_REG_PROP(mth, Quatf, "mth::Scalf", w)

		// Matrices.

		/// @todo: Add props for AS matrices.
		
		/// @todo: Why not vtype &in in AS?
		#define karhuAS_REG_MATOPS(vtype) \
			karhuAS_REG_MATHOPSalr(vtype, const mth::vtype &, const mth::vtype &, mth::vtype, mth::vtype, +, Add) \
			karhuAS_REG_MATHOPSalr(vtype, const mth::Scalf &, const mth::Scalf &, mth::Scalf, mth::Scalf, +, Add) \
			karhuAS_REG_MATHOPSalr(vtype, const mth::vtype &, const mth::vtype &, mth::vtype, mth::vtype, -, Sub) \
			karhuAS_REG_MATHOPSalr(vtype, const mth::Scalf &, const mth::Scalf &, mth::Scalf, mth::Scalf, -, Sub) \
			karhuAS_REG_MATHOPSalr(vtype, const mth::vtype &, const mth::vtype &, mth::vtype, mth::vtype, *, Mul) \
			karhuAS_REG_MATHOPSalr(vtype, const mth::Scalf &, const mth::Scalf &, mth::Scalf, mth::Scalf, *, Mul) \
			karhuAS_REG_MATHOPSalr(vtype, const mth::vtype &, const mth::vtype &, mth::vtype, mth::vtype, /, Div) \
			karhuAS_REG_MATHOPSalr(vtype, const mth::Scalf &, const mth::Scalf &, mth::Scalf, mth::Scalf, /, Div)
		
		karhuAS_REG_CTOR_A0(mth, Mat2f, const mth::Mat2f &, "const mth::Mat2f &in")
		karhuAS_REG_CTOR_A0(mth, Mat2f, const mth::Scalf, "const mth::Scalf")
		karhuAS_REG_MATOPS(Mat2f)
		
		karhuAS_REG_CTOR_A0(mth, Mat3f, const mth::Mat3f &, "const mth::Mat3f &in")
		karhuAS_REG_CTOR_A0(mth, Mat3f, const mth::Scalf, "const mth::Scalf")
		karhuAS_REG_MATOPS(Mat3f)
		
		karhuAS_REG_CTOR_A0(mth, Mat4f, const mth::Mat4f &, "const mth::Mat4f &in")
		karhuAS_REG_CTOR_A0(mth, Mat4f, const mth::Scalf, "const mth::Scalf")
		karhuAS_REG_MATOPS(Mat4f)
		
		#undef karhuAS_REG_MATOPS
		
		#undef karhuAS_REG_MATHOPSalr
		#undef karhuAS_REG_MATHOPSlr
		#undef karhuAS_REG_MATHOPSal
		#undef karhuAS_REG_MATHOPr
		#undef karhuAS_REG_MATHOPl
		#undef karhuAS_REG_MATHOPa
		
		// Transform.

		karhuAS_REG_VALTYPE_WITH_DATA_o(mth, Transformf, asOBJ_POD | asGetTypeTraits<mth::Transformf>() | asOBJ_APP_CLASS_ALLFLOATS)
		karhuAS_REG_CTOR_A0(mth, Transformf, const mth::Transformf &, "const mth::Transformf &in")
		karhuAS_REG_PROP(mth, Transformf, "mth::Vec3f", position)
		karhuAS_REG_PROP(mth, Transformf, "mth::Vec3f", scale)
		karhuAS_REG_PROP(mth, Transformf, "mth::Vec3f", origin)
		karhuAS_REG_PROP(mth, Transformf, "mth::Quatf", rotation)
		// These need to be generic or the data will be garbage…
		karhuAS_REG_METHOD(mth, Transformf, matrix, "mth::Mat4f matrix() const")
		karhuAS_REG_METHOD(mth, Transformf, forward, "mth::Vec3f forward() const")
		karhuAS_REG_METHOD(mth, Transformf, right, "mth::Vec3f right() const")
		karhuAS_REG_METHOD(mth, Transformf, up, "mth::Vec3f up() const")
		karhuAS_REG_METHOD(mth, Transformf, eyerotation, "mth::Quatf eyerotation() const")
		karhuAS_REG_METHOD(mth, Transformf, eyeforward, "mth::Vec3f eyeforward() const")
		karhuAS_REG_METHOD(mth, Transformf, eyeright, "mth::Vec3f eyeright() const")
		karhuAS_REG_METHOD(mth, Transformf, eyeup, "mth::Vec3f eyeup() const")
		
		// Bounds
		
		karhuAS_REG_VALTYPE_WITH_DATA_o(mth, Bounds3f, asOBJ_POD | asGetTypeTraits<mth::Bounds3f>() | asOBJ_APP_CLASS_ALLFLOATS)
		karhuAS_REG_PROP(mth, Bounds3f, "mth::Vec3f", min)
		karhuAS_REG_PROP(mth, Bounds3f, "mth::Vec3f", max)
		karhuAS_REG_METHOD(mth, Bounds3f, size, "mth::Vec3f size() const")
		
		karhuAS_REG_VALTYPE_WITH_DATA_o(mth, Bounds3i, asOBJ_POD | asGetTypeTraits<mth::Bounds3i>() | asOBJ_APP_CLASS_ALLINTS)
		karhuAS_REG_PROP(mth, Bounds3i, "mth::Vec3i", min)
		karhuAS_REG_PROP(mth, Bounds3i, "mth::Vec3i", max)
		karhuAS_REG_METHOD(mth, Bounds3i, size, "mth::Vec3i size() const")
		
		// Box
		
		karhuAS_REG_VALTYPE_WITH_DATA_o(mth, Boxf, asOBJ_POD | asGetTypeTraits<mth::Boxf>() | asOBJ_APP_CLASS_ALLFLOATS)
		karhuAS_REG_PROP(mth, Boxf, "mth::Vec3f", centre)
		karhuAS_REG_PROP(mth, Boxf, "mth::Vec3f", size)
		karhuAS_REG_METHOD(mth, Boxf, bounds, "mth::Bounds3f bounds() const")
		
		karhuAS_REG_VALTYPE_WITH_DATA_o(mth, Boxi, asOBJ_POD | asGetTypeTraits<mth::Boxi>() | asOBJ_APP_CLASS_ALLINTS)
		karhuAS_REG_PROP(mth, Boxi, "mth::Vec3i", centre)
		karhuAS_REG_PROP(mth, Boxi, "mth::Vec3i", size)
		karhuAS_REG_METHOD(mth, Boxi, bounds, "mth::Bounds3i bounds() const")
		
		// Functions.
		
		karhuAS_REG_FUNC(mth, pif, "mth::Scalf pif()")
		
		#define karhuAS_REG_FLOATFUNCS_A1(name) \
			karhuAS_REG_FUNC(mth, name < float >, "float " #name "(const float &in)") \
			karhuAS_REG_FUNC(mth, name < double >, "double " #name "(const double &in)")
		
		#define karhuAS_REG_FLOATFUNCS_A2(name) \
			karhuAS_REG_FUNC(mth, name < float >, "float " #name "(const float &in, const float &in)") \
			karhuAS_REG_FUNC(mth, name < double >, "double " #name "(const double &in, const double &in)")
		
		#define karhuAS_REG_TYPEFUNC_A1(name, type) \
			karhuAS_REG_FUNC(mth, name < type >, #type " " #name "(const " #type " &in)")
		
		#define karhuAS_REG_TYPEFUNC_A1_t(name, cpptype, astype) \
			karhuAS_REG_FUNC(mth, name < cpptype >, #astype " " #name "(const " #astype " &in)")

		#define karhuAS_REG_SCALARFUNCS_A1_s(name) \
			karhuAS_REG_TYPEFUNC_A1(name, float) \
			karhuAS_REG_TYPEFUNC_A1(name, double) \
			karhuAS_REG_TYPEFUNC_A1_t(name, std::int8_t, int8) \
			karhuAS_REG_TYPEFUNC_A1_t(name, std::int16_t, int16) \
			karhuAS_REG_TYPEFUNC_A1_t(name, std::int32_t, int) \
			karhuAS_REG_TYPEFUNC_A1_t(name, std::int64_t, int64)
		
		#define karhuAS_REG_SCALARFUNCS_A1(name) \
			karhuAS_REG_SCALARFUNCS_A1_s(name) \
			karhuAS_REG_TYPEFUNC_A1_t(name, std::uint8_t, uint8) \
			karhuAS_REG_TYPEFUNC_A1_t(name, std::uint16_t, uint16) \
			karhuAS_REG_TYPEFUNC_A1_t(name, std::uint32_t, uint) \
			karhuAS_REG_TYPEFUNC_A1_t(name, std::uint64_t, uint64)
		
		#define karhuAS_REG_TYPEFUNC_A2(name, type) \
			karhuAS_REG_FUNC(mth, name < type >, #type " " #name "(const " #type " &in, const " #type " &in)")
		
		#define karhuAS_REG_TYPEFUNC_A2_t(name, cpptype, astype) \
			karhuAS_REG_FUNC(mth, name < cpptype >, #astype " " #name "(const " #astype " &in, const " #astype " &in)")

		#define karhuAS_REG_SCALARFUNCS_A2(name) \
			karhuAS_REG_TYPEFUNC_A2(name, float) \
			karhuAS_REG_TYPEFUNC_A2(name, double) \
			karhuAS_REG_TYPEFUNC_A2_t(name, std::int8_t, int8) \
			karhuAS_REG_TYPEFUNC_A2_t(name, std::int16_t, int16) \
			karhuAS_REG_TYPEFUNC_A2_t(name, std::int32_t, int) \
			karhuAS_REG_TYPEFUNC_A2_t(name, std::int64_t, int64) \
			karhuAS_REG_TYPEFUNC_A2_t(name, std::uint8_t, uint8) \
			karhuAS_REG_TYPEFUNC_A2_t(name, std::uint16_t, uint16) \
			karhuAS_REG_TYPEFUNC_A2_t(name, std::uint32_t, uint) \
			karhuAS_REG_TYPEFUNC_A2_t(name, std::uint64_t, uint64)
		
		#define karhuAS_REG_TYPEFUNC_A3(name, type) \
			karhuAS_REG_FUNC(mth, name < type >, #type " " #name "(const " #type " &in, const " #type " &in, const " #type " &in)")
		
		#define karhuAS_REG_TYPEFUNC_A3_t(name, cpptype, astype) \
			karhuAS_REG_FUNC(mth, name < cpptype >, #astype " " #name "(const " #astype " &in, const " #astype " &in, const " #astype " &in)")

		#define karhuAS_REG_SCALARFUNCS_A3(name) \
			karhuAS_REG_TYPEFUNC_A3(name, float) \
			karhuAS_REG_TYPEFUNC_A3(name, double) \
			karhuAS_REG_TYPEFUNC_A3_t(name, std::int8_t, int8) \
			karhuAS_REG_TYPEFUNC_A3_t(name, std::int16_t, int16) \
			karhuAS_REG_TYPEFUNC_A3_t(name, std::int32_t, int) \
			karhuAS_REG_TYPEFUNC_A3_t(name, std::int64_t, int64) \
			karhuAS_REG_TYPEFUNC_A3_t(name, std::uint8_t, uint8) \
			karhuAS_REG_TYPEFUNC_A3_t(name, std::uint16_t, uint16) \
			karhuAS_REG_TYPEFUNC_A3_t(name, std::uint32_t, uint) \
			karhuAS_REG_TYPEFUNC_A3_t(name, std::uint64_t, uint64)
		
		#define karhuAS_REG_VECFUNCS_A1(name) \
			karhuAS_REG_TYPEFUNC_A1(name, mth::Vec2f) \
			karhuAS_REG_TYPEFUNC_A1(name, mth::Vec3f) \
			karhuAS_REG_TYPEFUNC_A1(name, mth::Vec4f)
		
		#define karhuAS_REG_VECFUNCS_A1s(name) \
			karhuAS_REG_FUNCPR(mth, name, (const mth::Vec2f &), mth::Scalf, "mth::Scalf " #name "(const mth::Vec2f &in)") \
			karhuAS_REG_FUNCPR(mth, name, (const mth::Vec3f &), mth::Scalf, "mth::Scalf " #name "(const mth::Vec3f &in)") \
			karhuAS_REG_FUNCPR(mth, name, (const mth::Vec4f &), mth::Scalf, "mth::Scalf " #name "(const mth::Vec4f &in)")
		
		#define karhuAS_REG_VECFUNCS_A2(name) \
			karhuAS_REG_TYPEFUNC_A2(name, mth::Vec2f) \
			karhuAS_REG_TYPEFUNC_A2(name, mth::Vec3f) \
			karhuAS_REG_TYPEFUNC_A2(name, mth::Vec4f)
		
		#define karhuAS_REG_VECFUNCS_A2s(name) \
			karhuAS_REG_FUNCPR(mth, name, (const mth::Vec2f &, const mth::Vec2f &), mth::Scalf, "mth::Scalf " #name "(const mth::Vec2f &in, const mth::Vec2f &in)") \
			karhuAS_REG_FUNCPR(mth, name, (const mth::Vec3f &, const mth::Vec3f &), mth::Scalf, "mth::Scalf " #name "(const mth::Vec3f &in, const mth::Vec3f &in)") \
			karhuAS_REG_FUNCPR(mth, name, (const mth::Vec4f &, const mth::Vec4f &), mth::Scalf, "mth::Scalf " #name "(const mth::Vec4f &in, const mth::Vec4f &in)")
		
		#define karhuAS_REG_MATFUNCS_A1(name) \
			karhuAS_REG_TYPEFUNC_A2(name, mth::Mat2f) \
			karhuAS_REG_TYPEFUNC_A2(name, mth::Mat3f) \
			karhuAS_REG_TYPEFUNC_A2(name, mth::Mat4f)
		
		karhuAS_REG_FUNC(mth, upf, "mth::Vec3f upf()")
		karhuAS_REG_FUNC(mth, rightf, "mth::Vec3f rightf()")
		karhuAS_REG_FUNC(mth, forwardf, "mth::Vec3f forwardf()")
		karhuAS_REG_SCALARFUNCS_A2(random)
		karhuAS_REG_SCALARFUNCS_A2(mod)
		karhuAS_REG_FLOATFUNCS_A1(sqrt)
		karhuAS_REG_FLOATFUNCS_A1(cbrt)
		karhuAS_REG_FLOATFUNCS_A1(round)
		karhuAS_REG_FLOATFUNCS_A1(floor)
		karhuAS_REG_FLOATFUNCS_A1(ceil)
		karhuAS_REG_FLOATFUNCS_A1(cos)
		karhuAS_REG_FLOATFUNCS_A1(sin)
		karhuAS_REG_FLOATFUNCS_A1(tan)
		karhuAS_REG_FLOATFUNCS_A1(radians)
		karhuAS_REG_FLOATFUNCS_A1(degrees)
		karhuAS_REG_FLOATFUNCS_A2(atan2)
		karhuAS_REG_SCALARFUNCS_A1_s(sign)
		karhuAS_REG_SCALARFUNCS_A1_s(abs)
		karhuAS_REG_SCALARFUNCS_A2(pow)
		karhuAS_REG_SCALARFUNCS_A2(min)
		karhuAS_REG_SCALARFUNCS_A2(max)
		karhuAS_REG_SCALARFUNCS_A3(clamp)
		karhuAS_REG_VECFUNCS_A1(normalise)
		karhuAS_REG_TYPEFUNC_A1(normalise, mth::Quatf)
		karhuAS_REG_VECFUNCS_A1s(length)
		karhuAS_REG_VECFUNCS_A2s(distance)
		karhuAS_REG_VECFUNCS_A2(dot)
		karhuAS_REG_TYPEFUNC_A2(dot, mth::Quatf)
		karhuAS_REG_TYPEFUNC_A2(cross, mth::Vec3f)
		karhuAS_REG_TYPEFUNC_A2(cross, mth::Quatf)
		karhuAS_REG_TYPEFUNC_A1(inverse, mth::Quatf)
		karhuAS_REG_MATFUNCS_A1(inverse)
		karhuAS_REG_TYPEFUNC_A1(conjugate, mth::Quatf)
		karhuAS_REG_FUNCPR(mth, euler, (const mth::Scalf, const mth::Scalf, const mth::Scalf), mth::Quatf, "mth::Quatf euler(const mth::Scalf, const mth::Scalf, const mth::Scalf)")
		karhuAS_REG_FUNCPR(mth, euler, (const mth::Vec3f &), mth::Quatf, "mth::Quatf euler(const mth::Vec3f &in)")
		karhuAS_REG_FUNCPR(mth, euler, (const mth::Quatf &), mth::Vec3f, "mth::Vec3f euler(const mth::Quatf &in)")
		karhuAS_REG_FUNCPR(mth, translate, (mth::Mat4f const &, mth::Vec3f const &), mth::Mat4f, "mth::Mat4f translate(const mth::Mat4f &in, const mth::Vec3f &in)")
		
		#undef karhuAS_REG_MATFUNCS_A1
		#undef karhuAS_REG_VECFUNCS_A2ret
		#undef karhuAS_REG_VECFUNCS_A2
		#undef karhuAS_REG_VECFUNCS_A1
		#undef karhuAS_REG_SCALARFUNCS_A3
		#undef karhuAS_REG_TYPEFUNC_A3_t
		#undef karhuAS_REG_TYPEFUNC_A3
		#undef karhuAS_REG_SCALARFUNCS_A2
		#undef karhuAS_REG_TYPEFUNC_A2_t
		#undef karhuAS_REG_TYPEFUNC_A2
		#undef karhuAS_REG_SCALARFUNCS_A1
		#undef karhuAS_REG_SCALARFUNCS_A1_s
		#undef karhuAS_REG_TYPEFUNC_A1_t
		#undef karhuAS_REG_TYPEFUNC_A1
		#undef karhuAS_REG_FLOATFUNCS_A2
		#undef karhuAS_REG_FLOATFUNCS_A1
		
		#define karhuAS_REG_LERP(name, type, stype) \
			karhuAS_REG_FUNC(mth, name, #type " " #name "(const " #type " &in, const " #type " &in, const " #stype " &in)")
		
		#define karhuAS_REG_LERP_t(name, type, stype) \
			karhuAS_REG_FUNC_n(mth, (mth :: name < type , stype >), #type " " #name "(const " #type " &in, const " #type " &in, const " #stype " &in)")
		
		karhuAS_REG_LERP_t(lerp, float, float)
		karhuAS_REG_LERP_t(lerp, double, double)
		karhuAS_REG_LERP_t(lerp, mth::Vec2f, float)
		karhuAS_REG_LERP_t(lerp, mth::Vec3f, float)
		karhuAS_REG_LERP_t(lerp, mth::Vec4f, float)
		karhuAS_REG_LERP_t(slerp, mth::Quatf, float)
		karhuAS_REG_LERP(clerp, mth::Radsf, float)
		
		#undef karhuAS_REG_LERP_t
		#undef karhuAS_REG_LERP
		
		karhuAS_REG_FUNC(mth, clength, "mth::Radsf clength(const Radsf &in, const Radsf &in)")
		
		/// @todo: Add the rest of the maths stuff, figure out how to use _PR with the wrapper...
		
		return true;
	}

	bool registerSubsystemGraphics(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		std::map<int, std::unique_ptr<Scriptmanager::Typedata>> &m_typedata{scr.m_typedata}; ///! @todo: Remove when done converting to global func.
		
		karhuAS_REG_REFTYPE(gfx, SubsystemGraphics)
		
		karhuAS_REG_TYPEDEF(gfx, Channelf)
		karhuAS_REG_TYPEDEF(gfx, Channelui)
		
		#define karhuAS_REG_COL(vtype, stype, opts) \
			karhuAS_REG_VALTYPE_WITH_DATA_o(gfx, vtype, asOBJ_POD | asGetTypeTraits<gfx::vtype>() | opts) \
			karhuAS_REG_CTOR_A0(gfx, vtype, const gfx::vtype &, "const gfx::" #vtype " &in") \
			karhuAS_REG_CTOR_A0(gfx, vtype, const stype, "const " #stype) \
			karhuAS_REG_METHODPR(gfx, vtype, operator=, (const gfx::vtype &), gfx::vtype &, "gfx::" #vtype " &opAssign(const gfx::" #vtype " &in)") \
			karhuAS_REG_DTOR(gfx, vtype) \
			karhuAS_REG_PROP(gfx, vtype, #stype, r) \
			karhuAS_REG_PROP(gfx, vtype, #stype, g)
		
		#define karhuAS_REG_COLrg(vtype, stype, opts) \
			karhuAS_REG_COL(vtype, stype, opts) \
			karhuAS_REG_CTOR_A0A1_same(gfx, vtype, const stype, "const " #stype)
		
		#define karhuAS_REG_COLrgb(vtype, stype, opts) \
			karhuAS_REG_COL(vtype, stype, opts) \
			karhuAS_REG_CTOR_A0A1A2_same(gfx, vtype, const stype, "const " #stype) \
			karhuAS_REG_PROP(gfx, vtype, #stype, b)

		#define karhuAS_REG_COLrgba(vtype, stype, opts) \
			karhuAS_REG_COL(vtype, stype, opts) \
			karhuAS_REG_CTOR_A0A1A2A3_same(gfx, vtype, const stype, "const " #stype) \
			karhuAS_REG_PROP(gfx, vtype, #stype, a)
		
		karhuAS_REG_COLrg(ColRGf,  gfx::Channelf,  asOBJ_APP_CLASS_ALLFLOATS)
		karhuAS_REG_COLrg(ColRGui, gfx::Channelui, asOBJ_APP_CLASS_ALLINTS)
		
		karhuAS_REG_COLrgb(ColRGBf,  gfx::Channelf,  asOBJ_APP_CLASS_ALLFLOATS)
		karhuAS_REG_COLrgb(ColRGBui, gfx::Channelui, asOBJ_APP_CLASS_ALLINTS)
		
		karhuAS_REG_COLrgba(ColRGBAf,  gfx::Channelf,  asOBJ_APP_CLASS_ALLFLOATS)
		karhuAS_REG_COLrgba(ColRGBAui, gfx::Channelui, asOBJ_APP_CLASS_ALLINTS)
		
		#undef karhuAS_REG_COL
		#undef karhuAS_REG_COLrg
		#undef karhuAS_REG_COLrgb
		#undef karhuAS_REG_COLrgba
		
		karhuAS_REG_REFTYPE(gfx, ContainerInputs)
		
		#define karhuAS_REG_INPUT(datatype, datatypeAS) \
			karhuAS_REG_METHODPR(gfx, ContainerInputs, input, (std::string const &name, const datatype), void, "void input(const std::string &in name, const " datatypeAS ")")
		
		karhuAS_REG_INPUT(bool,         "bool")
		karhuAS_REG_INPUT(std::int8_t,  "int8")
		karhuAS_REG_INPUT(std::int16_t, "int16")
		karhuAS_REG_INPUT(std::int32_t, "int")
		karhuAS_REG_INPUT(std::int64_t, "int64")
		karhuAS_REG_INPUT(float,        "float")
		karhuAS_REG_INPUT(double,       "double")
		karhuAS_REG_INPUT(mth::Vec2i &, "mth::Vec2i &in")
		karhuAS_REG_INPUT(mth::Vec3i &, "mth::Vec3i &in")
		karhuAS_REG_INPUT(mth::Vec4i &, "mth::Vec4i &in")
		karhuAS_REG_INPUT(mth::Vec2f &, "mth::Vec2f &in")
		karhuAS_REG_INPUT(mth::Vec3f &, "mth::Vec3f &in")
		karhuAS_REG_INPUT(mth::Vec4f &, "mth::Vec4f &in")
		karhuAS_REG_INPUT(mth::Mat2f &, "mth::Mat2f &in")
		karhuAS_REG_INPUT(mth::Mat3f &, "mth::Mat3f &in")
		karhuAS_REG_INPUT(mth::Mat4f &, "mth::Mat4f &in")
		
		karhuAS_REG_METHODPR(gfx, ContainerInputs, inputTexture, (std::string const &name, res::IDResource const), void, "void inputTexture(const std::string &in name, const res::IDResource)")
		
		#undef karhuAS_REG_INPUT
		
		karhuAS_REG_METHODPR(gfx, SubsystemGraphics, inputs, (), gfx::ContainerInputs &, "gfx::ContainerInputs @inputs()")
		
		return true;
	}

	bool registerSubsystemAudio(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		
		karhuAS_REG_REFTYPE(snd, SubsystemAudio)
		
		return true;
	}

	bool registerSubsystemWindow(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		
		karhuAS_REG_REFTYPE(win, SubsystemWindow)
		
		return true;
	}

	bool registerSubsystemInput(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		
		karhuAS_REG_ENUM(inp, Mouse)
		karhuAS_REG_ENUMVAL(inp, Mouse, none)
		karhuAS_REG_ENUMVAL(inp, Mouse, left)
		karhuAS_REG_ENUMVAL(inp, Mouse, right)
		karhuAS_REG_ENUMVAL(inp, Mouse, middle)
		karhuAS_REG_ENUMVAL(inp, Mouse, count)
		
		karhuAS_REG_ENUM(inp, Key)
		karhuAS_REG_ENUMVAL(inp, Key, none)
		karhuAS_REG_ENUMVAL(inp, Key, left)
		karhuAS_REG_ENUMVAL(inp, Key, right)
		karhuAS_REG_ENUMVAL(inp, Key, up)
		karhuAS_REG_ENUMVAL(inp, Key, down)
		karhuAS_REG_ENUMVAL(inp, Key, zero)
		karhuAS_REG_ENUMVAL(inp, Key, one)
		karhuAS_REG_ENUMVAL(inp, Key, two)
		karhuAS_REG_ENUMVAL(inp, Key, three)
		karhuAS_REG_ENUMVAL(inp, Key, four)
		karhuAS_REG_ENUMVAL(inp, Key, five)
		karhuAS_REG_ENUMVAL(inp, Key, six)
		karhuAS_REG_ENUMVAL(inp, Key, seven)
		karhuAS_REG_ENUMVAL(inp, Key, eight)
		karhuAS_REG_ENUMVAL(inp, Key, nine)
		karhuAS_REG_ENUMVAL(inp, Key, a)
		karhuAS_REG_ENUMVAL(inp, Key, b)
		karhuAS_REG_ENUMVAL(inp, Key, c)
		karhuAS_REG_ENUMVAL(inp, Key, d)
		karhuAS_REG_ENUMVAL(inp, Key, e)
		karhuAS_REG_ENUMVAL(inp, Key, f)
		karhuAS_REG_ENUMVAL(inp, Key, g)
		karhuAS_REG_ENUMVAL(inp, Key, h)
		karhuAS_REG_ENUMVAL(inp, Key, i)
		karhuAS_REG_ENUMVAL(inp, Key, j)
		karhuAS_REG_ENUMVAL(inp, Key, k)
		karhuAS_REG_ENUMVAL(inp, Key, l)
		karhuAS_REG_ENUMVAL(inp, Key, m)
		karhuAS_REG_ENUMVAL(inp, Key, n)
		karhuAS_REG_ENUMVAL(inp, Key, o)
		karhuAS_REG_ENUMVAL(inp, Key, p)
		karhuAS_REG_ENUMVAL(inp, Key, q)
		karhuAS_REG_ENUMVAL(inp, Key, r)
		karhuAS_REG_ENUMVAL(inp, Key, s)
		karhuAS_REG_ENUMVAL(inp, Key, t)
		karhuAS_REG_ENUMVAL(inp, Key, u)
		karhuAS_REG_ENUMVAL(inp, Key, v)
		karhuAS_REG_ENUMVAL(inp, Key, w)
		karhuAS_REG_ENUMVAL(inp, Key, x)
		karhuAS_REG_ENUMVAL(inp, Key, y)
		karhuAS_REG_ENUMVAL(inp, Key, z)
		karhuAS_REG_ENUMVAL(inp, Key, space)
		karhuAS_REG_ENUMVAL(inp, Key, backspace)
		karhuAS_REG_ENUMVAL(inp, Key, enter)
		karhuAS_REG_ENUMVAL(inp, Key, escape)
		karhuAS_REG_ENUMVAL(inp, Key, numZero)
		karhuAS_REG_ENUMVAL(inp, Key, numOne)
		karhuAS_REG_ENUMVAL(inp, Key, numTwo)
		karhuAS_REG_ENUMVAL(inp, Key, numThree)
		karhuAS_REG_ENUMVAL(inp, Key, numFour)
		karhuAS_REG_ENUMVAL(inp, Key, numFive)
		karhuAS_REG_ENUMVAL(inp, Key, numSix)
		karhuAS_REG_ENUMVAL(inp, Key, numSeven)
		karhuAS_REG_ENUMVAL(inp, Key, numEight)
		karhuAS_REG_ENUMVAL(inp, Key, numNine)
		karhuAS_REG_ENUMVAL(inp, Key, numEnter)
		karhuAS_REG_ENUMVAL(inp, Key, numEquals)
		karhuAS_REG_ENUMVAL(inp, Key, numAdd)
		karhuAS_REG_ENUMVAL(inp, Key, numSubtract)
		karhuAS_REG_ENUMVAL(inp, Key, numMultiply)
		karhuAS_REG_ENUMVAL(inp, Key, numDivide)
		karhuAS_REG_ENUMVAL(inp, Key, numDecimal)
		karhuAS_REG_ENUMVAL(inp, Key, f1)
		karhuAS_REG_ENUMVAL(inp, Key, f2)
		karhuAS_REG_ENUMVAL(inp, Key, f3)
		karhuAS_REG_ENUMVAL(inp, Key, f4)
		karhuAS_REG_ENUMVAL(inp, Key, f5)
		karhuAS_REG_ENUMVAL(inp, Key, f6)
		karhuAS_REG_ENUMVAL(inp, Key, f7)
		karhuAS_REG_ENUMVAL(inp, Key, f8)
		karhuAS_REG_ENUMVAL(inp, Key, f9)
		karhuAS_REG_ENUMVAL(inp, Key, f10)
		karhuAS_REG_ENUMVAL(inp, Key, f11)
		karhuAS_REG_ENUMVAL(inp, Key, f12)
		karhuAS_REG_ENUMVAL(inp, Key, shift)
		karhuAS_REG_ENUMVAL(inp, Key, shiftLeft)
		karhuAS_REG_ENUMVAL(inp, Key, shiftRight)
		karhuAS_REG_ENUMVAL(inp, Key, alt)
		karhuAS_REG_ENUMVAL(inp, Key, altLeft)
		karhuAS_REG_ENUMVAL(inp, Key, altRight)
		karhuAS_REG_ENUMVAL(inp, Key, control)
		karhuAS_REG_ENUMVAL(inp, Key, controlLeft)
		karhuAS_REG_ENUMVAL(inp, Key, controlRight)
		karhuAS_REG_ENUMVAL(inp, Key, meta)
		karhuAS_REG_ENUMVAL(inp, Key, metaLeft)
		karhuAS_REG_ENUMVAL(inp, Key, metaRight)
		karhuAS_REG_ENUMVAL(inp, Key, count)
		
		karhuAS_REG_ENUM(inp, Button)
		karhuAS_REG_ENUMVAL(inp, Button, none)
		karhuAS_REG_ENUMVAL(inp, Button, left)
		karhuAS_REG_ENUMVAL(inp, Button, right)
		karhuAS_REG_ENUMVAL(inp, Button, up)
		karhuAS_REG_ENUMVAL(inp, Button, down)
		karhuAS_REG_ENUMVAL(inp, Button, a)
		karhuAS_REG_ENUMVAL(inp, Button, b)
		karhuAS_REG_ENUMVAL(inp, Button, x)
		karhuAS_REG_ENUMVAL(inp, Button, y)
		karhuAS_REG_ENUMVAL(inp, Button, select)
		karhuAS_REG_ENUMVAL(inp, Button, start)
		karhuAS_REG_ENUMVAL(inp, Button, guide)
		karhuAS_REG_ENUMVAL(inp, Button, shoulderLeft)
		karhuAS_REG_ENUMVAL(inp, Button, shoulderRight)
		karhuAS_REG_ENUMVAL(inp, Button, triggerLeft)
		karhuAS_REG_ENUMVAL(inp, Button, triggerRight)
		karhuAS_REG_ENUMVAL(inp, Button, stickLeft)
		karhuAS_REG_ENUMVAL(inp, Button, stickRight)
		karhuAS_REG_ENUMVAL(inp, Button, count)
		
		karhuAS_REG_ENUM(inp, Axis)
		karhuAS_REG_ENUMVAL(inp, Axis, none)
		karhuAS_REG_ENUMVAL(inp, Axis, xLeft)
		karhuAS_REG_ENUMVAL(inp, Axis, yLeft)
		karhuAS_REG_ENUMVAL(inp, Axis, xRight)
		karhuAS_REG_ENUMVAL(inp, Axis, yRight)
		karhuAS_REG_ENUMVAL(inp, Axis, triggerLeft)
		karhuAS_REG_ENUMVAL(inp, Axis, triggerRight)
		karhuAS_REG_ENUMVAL(inp, Axis, count)

		karhuAS_REG_REFTYPE(inp, SubsystemInput)
		
		karhuAS_REG_METHOD(inp, SubsystemInput, mouseX,      "const int &mouseX() const")
		karhuAS_REG_METHOD(inp, SubsystemInput, mouseY,      "const int &mouseY() const")
		karhuAS_REG_METHOD(inp, SubsystemInput, mouseXDelta, "const int &mouseXDelta() const")
		karhuAS_REG_METHOD(inp, SubsystemInput, mouseYDelta, "const int &mouseYDelta() const")
		
		karhuAS_REG_TYPEDEF(inp, Layer)
		karhuAS_REG_TYPEDEF(inp, Priority)
		karhuAS_REG_TYPEDEF(inp, Action)
		karhuAS_REG_TYPEDEF(inp, Actions)
		karhuAS_REG_TYPEDEF(inp, Player)
		karhuAS_REG_TYPEDEF(inp, Players)
		karhuAS_REG_TYPEDEF(inp, Code)
		
		karhuAS_REG_ENUM(inp, Type)
		karhuAS_REG_ENUMVAL(inp, Type, press)
		karhuAS_REG_ENUMVAL(inp, Type, hold)
		karhuAS_REG_ENUMVAL(inp, Type, release)
		karhuAS_REG_ENUMVAL(inp, Type, analogue)
		karhuAS_REG_ENUMVAL(inp, Type, direction)
		
		karhuAS_REG_ENUM(inp, Method)
		karhuAS_REG_ENUMVAL(inp, Method, mouse)
		karhuAS_REG_ENUMVAL(inp, Method, keyboard)
		karhuAS_REG_ENUMVAL(inp, Method, controller)
		karhuAS_REG_ENUMVAL(inp, Method, touch)
		karhuAS_REG_ENUMVAL(inp, Method, simulated)
		
		karhuAS_REG_ENUM(inp, Scope)
		karhuAS_REG_ENUMVAL(inp, Scope, global)
		karhuAS_REG_ENUMVAL(inp, Scope, local)
		
		karhuAS_REG_ENUM(inp, Invert)
		karhuAS_REG_ENUMVAL(inp, Invert, none)
		karhuAS_REG_ENUMVAL(inp, Invert, horizontal)
		karhuAS_REG_ENUMVAL(inp, Invert, vertical)
		karhuAS_REG_ENUMVAL(inp, Invert, both)
		
		return true;
	}

	bool registerSubsystemFile(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		
		karhuAS_REG_REFTYPE(fio, SubsystemFile)
	
		return true;
	}

	bool registerSubsystemPhysics(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		
		karhuAS_REG_REFTYPE(phx, SubsystemPhysics)
		
		karhuAS_REG_TYPEDEF(phx, Layer)
		karhuAS_REG_TYPEDEF(phx, Layers)
		
		return true;
	}
	
	struct ProxyresgetT
	{
		static void construct(asITypeInfo *, const asUINT, void *);
		static void destruct(void *) {}
		
		void *opImplConv() const { return handle; }
		
		void *handle;
	};

	bool registerResources(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		std::map<int, std::unique_ptr<Scriptmanager::Typedata>> &m_typedata{scr.m_typedata}; ///! @todo: Remove when done converting to global func.
		
		karhuAS_REG_TYPEDEF(res, IDTypeResource)
		karhuAS_REG_TYPEDEF(res, IDResource)
		
		m_engine->SetDefaultNamespace("res");
		
		// get<T>
		
		if
		(
			m_engine->RegisterObjectType
			(
				"get<class T>",
				sizeof(ProxyresgetT),
				asOBJ_VALUE | asOBJ_TEMPLATE | asGetTypeTraits<ProxyresgetT>()
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectBehaviour
			(
				"get<T>",
				asBEHAVE_CONSTRUCT,
				"void f(int &in, uint)",
				karhuAS_FUNCTION_OBJLAST(ProxyresgetT::construct),
				karhuAS_CALL_CDECL_OBJLAST
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectBehaviour
			(
				"get<T>",
				asBEHAVE_DESTRUCT,
				"void f()",
				karhuAS_FUNCTION_OBJLAST(ProxyresgetT::destruct),
				karhuAS_CALL_CDECL_OBJLAST
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectMethod
			(
				"get<T>",
				"T @opImplConv() const",
				karhuAS_METHOD(ProxyresgetT, opImplConv),
				karhuAS_CALL_THISCALL
			) < 0
		)
			return false;
		
		return true;
	}
	
	struct ProxyECScreateComponentInEntityT
	{
		static void construct(asITypeInfo *, const asUINT, void *);
		static void destruct(void *) {}
		
		void *opImplConv() const { return handle; }
		
		void *handle;
	};

	struct ProxyECScomponentInEntityT
	{
		static void construct(asITypeInfo *, const asUINT, void *);
		static void destruct(void *) {}
		
		void *opImplConv() const { return handle; }
		
		void *handle;
	};
	
	struct ProxyECSRefComponentInEntityT
	{
		static void construct(asITypeInfo *info, asUINT const, void *);
		static void destruct(void *) {}
		
		Reference opImplConv() const { return ref; }
		
		Reference ref;
	};

	bool registerECS(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		
		karhuAS_REG_TYPEDEF(ecs, IDTypeComponent)
		karhuAS_REG_TYPEDEF(ecs, IDComponent)
		karhuAS_REG_TYPEDEF(ecs, IDEntity)
		karhuAS_REG_TYPEDEF(ecs, IDScene)
		karhuAS_REG_TYPEDEF(ecs, TagsEntity)
		karhuAS_REG_TYPEDEF(ecs, TagEntity)
		
		karhuAS_REG_REFTYPE (ecs, Tagbank)
		karhuAS_REG_METHODPR(ecs, Tagbank, get, (std::string const &) const, ecs::TagEntity, "ecs::TagEntity get(const std::string &in) const")
		
		scr.registerTypeComponent<ecs::Component>(0, "ecs::Component");
		
		karhuAS_REG_REFTYPE  (ecs, World)
		karhuAS_REG_METHODPR (ecs, World,               createEntityInScene,     (ecs::IDScene const IDScene, std::string const &),                 ecs::IDEntity,        "ecs::IDEntity createEntityInScene(const ecs::IDScene, const std::string &in = \"\")")
		karhuAS_REG_METHODPR (ecs, World,               destroyEntity,           (ecs::IDEntity const),                                             bool,                 "bool destroyEntity(const ecs::IDEntity)")
		karhuAS_REG_METHODb  (ecs, World, kenno::World, destroyChildrenOfEntity,                                                                                          "bool destroyChildrenOfEntity(const ecs::IDEntity)")
		karhuAS_REG_METHOD   (ecs, World,               destroyComponentInEntity,                                                                                         "bool destroyComponentInEntity(const IDComponent, const IDEntity)")
		karhuAS_REG_METHODPRb(ecs, World, kenno::World, entityParentOf,          (ecs::IDEntity const IDParent, ecs::IDEntity const IDChild),       bool,                 "bool entityParentOf(const ecs::IDEntity, const ecs::IDEntity)")
		karhuAS_REG_METHOD   (ecs, World,               entityActive,                                                                                                     "void entityActive(const ecs::IDEntity identifier, const bool)")
		karhuAS_REG_METHODPR (ecs, World,               tags,                    ()                                                          const, ecs::Tagbank const &, "ecs::Tagbank @tags() const")
		karhuAS_REG_METHODPRb(ecs, World, kenno::World, entityName,              (ecs::IDEntity const identifier, std::string const &name),         bool,                 "bool entityName(const ecs::IDEntity, const std::string &in)")
		karhuAS_REG_METHODPRb(ecs, World, kenno::World, entityName,              (ecs::IDEntity const identifier)                            const, std::string,          "std::string entityName(const ecs::IDEntity) const")
		karhuAS_REG_METHODPRb(ecs, World, kenno::World, entityTags,              (ecs::IDEntity const, ecs::TagsEntity const),                      bool,                 "bool entityTags(const ecs::IDEntity, const ecs::TagsEntity)")
		karhuAS_REG_METHODPRb(ecs, World, kenno::World, entityTags,              (ecs::IDEntity const)                                       const, ecs::TagsEntity,      "ecs::TagsEntity entityTags(const ecs::IDEntity) const")
		
		m_engine->SetDefaultNamespace("ecs");
		
		if (m_engine->RegisterInterface("IScriptable") < 0)
			return false;
		
		// createComponentInEntity<T>
		
		if
		(
			m_engine->RegisterObjectType
			(
				"createComponentInEntity<class T>",
				sizeof(ProxyECScreateComponentInEntityT),
				asOBJ_VALUE | asOBJ_TEMPLATE | asGetTypeTraits<ProxyECScreateComponentInEntityT>()
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectBehaviour
			(
				"createComponentInEntity<T>",
				asBEHAVE_CONSTRUCT,
				"void f(int &in, uint)",
				karhuAS_FUNCTION_OBJLAST(ProxyECScreateComponentInEntityT::construct),
				karhuAS_CALL_CDECL_OBJLAST
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectBehaviour
			(
				"createComponentInEntity<T>",
				asBEHAVE_DESTRUCT,
				"void f()",
				karhuAS_FUNCTION_OBJLAST(ProxyECScreateComponentInEntityT::destruct),
				karhuAS_CALL_CDECL_OBJLAST
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectMethod
			(
				"createComponentInEntity<T>",
				"T @opImplConv() const",
				karhuAS_METHOD(ProxyECScreateComponentInEntityT, opImplConv),
				karhuAS_CALL_THISCALL
			) < 0
		)
			return false;
		
		// componentInEntity<T>
		
		if
		(
			m_engine->RegisterObjectType
			(
				"componentInEntity<class T>",
				sizeof(ProxyECScomponentInEntityT),
				asOBJ_VALUE | asOBJ_TEMPLATE | asGetTypeTraits<ProxyECScomponentInEntityT>()
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectBehaviour
			(
				"componentInEntity<T>",
				asBEHAVE_CONSTRUCT,
				"void f(int &in, uint)",
				karhuAS_FUNCTION_OBJLAST(ProxyECScomponentInEntityT::construct),
				karhuAS_CALL_CDECL_OBJLAST
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectBehaviour
			(
				"componentInEntity<T>",
				asBEHAVE_DESTRUCT,
				"void f()",
				karhuAS_FUNCTION_OBJLAST(ProxyECScomponentInEntityT::destruct),
				karhuAS_CALL_CDECL_OBJLAST
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectMethod
			(
				"componentInEntity<T>",
				"T @opImplConv() const",
				karhuAS_METHOD(ProxyECScomponentInEntityT, opImplConv),
				karhuAS_CALL_THISCALL
			) < 0
		)
			return false;
		
		// Ref<T>
		
		if
		(
			m_engine->RegisterObjectType
			(
				"Ref<class T>",
				sizeof(Reference),
				asOBJ_VALUE | asOBJ_TEMPLATE | asGetTypeTraits<Reference>()
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectBehaviour
			(
				"Ref<T>",
				asBEHAVE_CONSTRUCT,
				"void f(int &in)",
				karhuAS_FUNCTION_OBJFIRST(AS::constructor<Reference>),
				karhuAS_CALL_CDECL_OBJFIRST
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectBehaviour
			(
				"Ref<T>",
				asBEHAVE_CONSTRUCT,
				"void f(const Ref<T> &in)",
				karhuAS_FUNCTION_OBJFIRST(AS::copyconstructor<Reference>),
				karhuAS_CALL_CDECL_OBJFIRST
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectBehaviour
			(
				"Ref<T>",
				asBEHAVE_DESTRUCT,
				"void f()",
				karhuAS_FUNCTION_OBJFIRST(AS::destructor<Reference>),
				karhuAS_CALL_CDECL_OBJFIRST
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectMethod
			(
				"Ref<T>",
				"Ref<T> &opAssign(const Ref<T> &in)",
				karhuAS_FUNCTION_OBJFIRST(AS::opAssign<Reference>),
				karhuAS_CALL_CDECL_OBJFIRST
			) < 0
		)
			return false;
		
		// refComponentInEntity<T>
		
		if
		(
			m_engine->RegisterObjectType
			(
				"refComponentInEntity<class T>",
				sizeof(ProxyresgetT),
				asOBJ_VALUE | asOBJ_TEMPLATE | asGetTypeTraits<ProxyECSRefComponentInEntityT>()
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectBehaviour
			(
				"refComponentInEntity<T>",
				asBEHAVE_CONSTRUCT,
				"void f(int &in, uint)",
				karhuAS_FUNCTION_OBJLAST(ProxyECSRefComponentInEntityT::construct),
				karhuAS_CALL_CDECL_OBJLAST
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectBehaviour
			(
				"refComponentInEntity<T>",
				asBEHAVE_DESTRUCT,
				"void f()",
				karhuAS_FUNCTION_OBJLAST(ProxyECSRefComponentInEntityT::destruct),
				karhuAS_CALL_CDECL_OBJLAST
			) < 0
		)
			return false;
		
		if
		(
			m_engine->RegisterObjectMethod
			(
				"refComponentInEntity<T>",
				"ecs::Ref<T> opImplConv() const",
				karhuAS_METHOD(ProxyECSRefComponentInEntityT, opImplConv),
				karhuAS_CALL_THISCALL
			) < 0
		)
			return false;
		
		return true;
	}

	bool registerResourceTexture(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		
		karhuAS_REG_METHOD(res, Texture, width, "uint width() const")
		karhuAS_REG_METHOD(res, Texture, height, "uint height() const")
		
		return true;
	}

	bool registerEventInput(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		
		karhuAS_REG_PROP(inp, EInput, "inp::Action", action)
		karhuAS_REG_PROP(inp, EInput, "inp::Player", player)
		karhuAS_REG_PROP(inp, EInput, "inp::Type", type)
		karhuAS_REG_PROP(inp, EInput, "mth::Scalf", analogue)
		karhuAS_REG_PROP(inp, EInput, "mth::Vec2f", direction)
		
		return true;
	}

	bool registerApp(Scriptmanager &scr)
	{
		asIScriptEngine *m_engine{&scr.engine()}; ///! @todo: Remove when done converting to global func.
		
		karhuAS_REG_REFTYPE(app, App)
		
		karhuAS_REG_METHOD(app, App, gfx, "gfx::SubsystemGraphics &gfx()")
		karhuAS_REG_METHOD(app, App, snd, "snd::SubsystemAudio &snd()")
		karhuAS_REG_METHOD(app, App, win, "win::SubsystemWindow &win()")
		karhuAS_REG_METHOD(app, App, inp, "inp::SubsystemInput &inp()")
		karhuAS_REG_METHOD(app, App, fio, "fio::SubsystemFile &fio()")
		karhuAS_REG_METHOD(app, App, phx, "phx::SubsystemPhysics &phx()")
		karhuAS_REG_METHOD(app, App, ecs, "ecs::World &ecs()")
		
		return true;
	}
	
	bool registerComponentScriptable(Scriptmanager &scr)
	{
		scr.registerMethod(karhuAS_METHODPTRrp(ecs::Scriptable, message, void, (ecs::IDEntity const, std::string const &)), "void message(const ecs::IDEntity, const std::string &in)");
		scr.registerMethod(karhuAS_METHODPTRrp(ecs::Scriptable, message, void, (ecs::IDEntity const, std::string const &, AS::Ref &)), "void message(const ecs::IDEntity, const std::string &in, ref @&in)");
		scr.registerMethod(karhuAS_METHODPTR  (ecs::Scriptable, scriptobjectAS), "ref @scriptobject() const");
		
		return true;
	}

	bool registerComponentTransform(Scriptmanager &scr)
	{
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Transform, local, mth::Transformf const &, () const));
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Transform, local, void, (mth::Transformf const &)));
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Transform, global, mth::Transformf const &, () const));
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Transform, global, void, (mth::Transformf const &)));
		scr.registerMethod(karhuAS_METHODPTR  (gfx::Transform, matrixLocal));
		scr.registerMethod(karhuAS_METHODPTR  (gfx::Transform, matrixGlobal));
		
		return true;
	}

	bool registerComponentRenderdata2D(Scriptmanager &scr)
	{
		scr.registerMethod(karhuAS_METHODPTRbrp(gfx::Renderdata2D, gfx::Renderdata, instanced, bool, () const));
		scr.registerMethod(karhuAS_METHODPTRbrp(gfx::Renderdata2D, gfx::Renderdata, instanced, void, (bool const)));
		scr.registerMethod(karhuAS_METHODPTRrp (gfx::Renderdata2D, texture, res::IDResource, () const));
		scr.registerMethod(karhuAS_METHODPTRrp (gfx::Renderdata2D, texture, void, (res::IDResource const)));
		scr.registerMethod(karhuAS_METHODPTRrp (gfx::Renderdata2D, billboard, bool, () const));
		scr.registerMethod(karhuAS_METHODPTRrp (gfx::Renderdata2D, billboard, void, (bool const)));
	
		return true;
	}

	bool registerComponentRenderdata3D(Scriptmanager &scr)
	{
		scr.registerMethod(karhuAS_METHODPTRbrp(gfx::Renderdata3D, gfx::Renderdata, instanced, bool, () const));
		scr.registerMethod(karhuAS_METHODPTRbrp(gfx::Renderdata3D, gfx::Renderdata, instanced, void, (bool const)));
		scr.registerMethod(karhuAS_METHODPTRrp (gfx::Renderdata3D, mesh, res::IDResource, () const));
		scr.registerMethod(karhuAS_METHODPTRrp (gfx::Renderdata3D, mesh, void, (res::IDResource const)));
		scr.registerMethod(karhuAS_METHODPTRrp (gfx::Renderdata3D, material, res::IDResource, () const));
		scr.registerMethod(karhuAS_METHODPTRrp (gfx::Renderdata3D, material, void, (res::IDResource const)));
		
		return true;
	}

	bool registerComponentAnimator(Scriptmanager &scr)
	{
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Animator, speed, float, () const));
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Animator, speed, void, (float const)));
		scr.registerMethod(karhuAS_METHODPTR  (gfx::Animator, start));
		scr.registerMethod(karhuAS_METHODPTR  (gfx::Animator, restart));
		scr.registerMethod(karhuAS_METHODPTR  (gfx::Animator, pause));
		scr.registerMethod(karhuAS_METHODPTR  (gfx::Animator, stop));
		scr.registerMethod(karhuAS_METHODPTR  (gfx::Animator, playing));
		scr.registerMethod(karhuAS_METHODPTR  (gfx::Animator, tick));
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Animator, boolean, void, (std::string const &, bool const)));
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Animator, boolean, bool, (std::string const &) const));
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Animator, integer, void, (std::string const &, std::int32_t const)));
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Animator, integer, std::int32_t, (std::string const &) const));
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Animator, floating, void, (std::string const &, float const)));
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Animator, floating, float, (std::string const &) const));
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Animator, trigger, void, (std::string const &)));
		
		return true;
	}

	bool registerComponentParticlesystem(Scriptmanager &scr)
	{
		//! @todo: Replace with proper registerType method on Scriptmanager
		asIScriptEngine *m_engine{&scr.engine()};
		karhuAS_REG_REFTYPE(gfx, Particle)
		
		//! @todo: Use proper simplified registerMember later
		scr.registerMember<gfx::Particle, gfx::Particle, decltype(gfx::Particle::transform), &gfx::Particle::transform>("gfx::Particle", "mth::Transformf", "transform");
		
		scr.registerMethod(karhuAS_METHODPTR(gfx::Particlesystem, reserve));
		scr.registerMethod(karhuAS_METHODPTR(gfx::Particlesystem, count));
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Particlesystem, instance, gfx::Particle &, (std::uint32_t const)));
		scr.registerMethod(karhuAS_METHODPTRrp(gfx::Particlesystem, instance, gfx::Particle const &, (std::uint32_t const) const));
	
		return true;
	}

	bool registerComponentBody(Scriptmanager &scr)
	{
		scr.registerMethod(karhuAS_METHODPTRrp(phx::Body, layer, void, (phx::Layer const)));
		scr.registerMethod(karhuAS_METHODPTRrp(phx::Body, layer, phx::Layer, () const));
		scr.registerMethod(karhuAS_METHODPTRrp(phx::Body, kinematic, void, (bool const)));
		scr.registerMethod(karhuAS_METHODPTRrp(phx::Body, kinematic, bool, () const));
		
		return true;
	}

	bool registerComponentsColliders(Scriptmanager &scr)
	{
		scr.registerMethod(karhuAS_METHODPTRrp(phx::ColliderBox, origin, void, (mth::Vec3f const &)));
		scr.registerMethod(karhuAS_METHODPTRrp(phx::ColliderBox, origin, mth::Vec3f const &, () const));
		scr.registerMethod(karhuAS_METHODPTRrp(phx::ColliderBox, offset, void, (mth::Vec3f const &)));
		scr.registerMethod(karhuAS_METHODPTRrp(phx::ColliderBox, offset, mth::Vec3f const &, () const));
		scr.registerMethod(karhuAS_METHODPTRrp(phx::ColliderBox, size, void, (mth::Vec3f const &)));
		scr.registerMethod(karhuAS_METHODPTRrp(phx::ColliderBox, size, mth::Vec3f const &, () const));
		scr.registerMethod(karhuAS_METHODPTRrp(phx::ColliderBox, rotation, void, (mth::Quatf const &)));
		scr.registerMethod(karhuAS_METHODPTRrp(phx::ColliderBox, rotation, mth::Quatf const &, () const));
		
		return true;
	}
	
	void ProxyresgetT::construct
	(
			  asITypeInfo *info,
		const asUINT       resource,
		void              *addr
	)
	{
		void *handle{nullptr};
		
		if (info->GetSubTypeCount() > 0)
			if (const auto data = reinterpret_cast<Scriptmanager::Typedata *>(info->GetSubType()->GetUserData()))
				if (Scriptmanager::Typedata::Category::resource == data->category)
					handle = app::App::instance()->res().getByType
					(
						data->typeResource,
						resource
					);
		
		new(addr) ProxyresgetT{handle};
	}

	void ProxyECScreateComponentInEntityT::construct
	(
			  asITypeInfo *info,
		const asUINT       entity,
		void              *addr
	)
	{
		void *handle{nullptr};
		
		if (info->GetSubTypeCount() > 0)
			if (const auto data = reinterpret_cast<Scriptmanager::Typedata *>(info->GetSubType()->GetUserData()))
				if (Scriptmanager::Typedata::Category::component == data->category)
					handle = app::App::instance()->ecs().createComponentByTypeInEntity
					(
						data->typeComponent,
						entity
					);
		
		new(addr) ProxyECScomponentInEntityT{handle};
	}

	void ProxyECScomponentInEntityT::construct
	(
			  asITypeInfo *info,
		const asUINT       entity,
		void              *addr
	)
	{
		void *handle{nullptr};
		
		if (info->GetSubTypeCount() > 0)
			if (const auto data = reinterpret_cast<Scriptmanager::Typedata *>(info->GetSubType()->GetUserData()))
				if (Scriptmanager::Typedata::Category::component == data->category)
					handle = app::App::instance()->ecs().componentByTypeInEntity
					(
						data->typeComponent,
						entity
					);
		
		new(addr) ProxyECScomponentInEntityT{handle};
	}

	void ProxyECSRefComponentInEntityT::construct
	(
		asITypeInfo       *info,
		asUINT      const  entity,
		void              *addr
	)
	{
		if (info->GetSubTypeCount() > 0)
		{
			if (const auto data = reinterpret_cast<Scriptmanager::Typedata *>(info->GetSubType()->GetUserData()))
			{
				if (Scriptmanager::Typedata::Category::component == data->category)
				{
					new (addr) ProxyECSRefComponentInEntityT
					{
						app::App::instance()->ecs().refComponentByTypeInEntityRaw
						(
							data->typeComponent,
							entity
						)
					};
					return;
				}
			}
		}
		
		new(addr) ProxyECSRefComponentInEntityT;
	}
}
