/**
 * @author		Ava Skoog
 * @date		2020-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_BITFLAGBANK_H_
	#define KARHU_APP_BITFLAGBANK_H_

	#include <karhu/core/log.hpp>
	#include <karhu/conv/serialise.hpp>

	#include <map>
	#include <vector>
	#include <string>

	namespace karhu
	{
		namespace app
		{
			class BitflagbankBase
			{
				protected:
					void serialiseElements(conv::Serialiser &, char const *const name, std::vector<std::string> const &elements) const;
					void deserialiseElements(conv::Serialiser const &, char const *const name, std::vector<std::string> &elements);
			};
			
			template<typename T, T Offset, T Default>
			class Bitflagbank : private BitflagbankBase
			{
				public:
					void set(char const *name, std::size_t const index);
					T get(char const *name) const;
					T get(std::string const &name) const;
					char const *name(T const) const;
					std::map<T, std::string> const &values() const noexcept { return m_namesByValue; }
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
				
				public:
					virtual char const *label() = 0;
					virtual char const *descriptor() { return label(); }
				
				public:
					virtual ~Bitflagbank() = default;
				
				private:
					std::map<std::string, T> m_valuesByName;
					std::map<T, std::string> m_namesByValue;
			};
		
			template<typename T, T Offset, T Default>
			void Bitflagbank<T, Offset, Default>::set(char const *name, std::size_t const index)
			{
				auto const value(static_cast<T>(static_cast<std::size_t>(Offset) << index));
				
				if (0 == std::strlen(name))
				{
					auto const it(m_namesByValue.find(value));
					
					if (it != m_namesByValue.end())
					{
						log::dbg("Karhu")
							<< "Cleared custom "
							<< descriptor()
							<< " value #"
							<< (index + 1)
							<< " with bitflag "
							<< value
							<< " and name '"
							<< name
							<< '\'';
						
						auto const jt(m_valuesByName.find(it->second));
						
						if (jt != m_valuesByName.end())
							m_valuesByName.erase(jt);
						
						m_namesByValue.erase(it);
					}
				}
				else
				{
					m_valuesByName[name]  = value;
					m_namesByValue[value] = name;
					
					log::dbg("Karhu")
						<< "Added custom "
						<< descriptor()
						<< " value #"
						<< (index + 1)
						<< " with bitflag "
						<< value
						<< " and name '"
						<< name
						<< '\'';
				}
			}
			
			template<typename T, T Offset, T Default>
			T Bitflagbank<T, Offset, Default>::get(char const *name) const
			{
				auto const it(m_valuesByName.find(name));
				
				if (it != m_valuesByName.end())
					return it->second;
				
				return Default;
			}
			
			template<typename T, T Offset, T Default>
			T Bitflagbank<T, Offset, Default>::get(std::string const &name) const
			{
				return get(name.c_str());
			}
			
			template<typename T, T Offset, T Default>
			char const *Bitflagbank<T, Offset, Default>::name(T const value) const
			{
				auto const it(m_namesByValue.find(value));
				
				if (it != m_namesByValue.end())
					return it->second.c_str();
				
				return "";
			}
			
			template<typename T, T Offset, T Default>
			void Bitflagbank<T, Offset, Default>::serialise(conv::Serialiser &ser) const
			{
				std::vector<std::string>
					elements;
				
				T
					last{Offset},
					next;
				
				for (auto const &it : m_namesByValue)
				{
					while ((next = last << 1) != it.first)
						elements.emplace_back();
					
					elements.emplace_back(it.second);
					last = next;
				}
				
				serialiseElements(ser, label(), elements);
			}
			
			template<typename T, T Offset, T Default>
			void Bitflagbank<T, Offset, Default>::deserialise(const conv::Serialiser &ser)
			{
				std::vector<std::string>
					elements;
				
				deserialiseElements(ser, label(), elements);
				
				for (std::size_t i{0}; i < elements.size(); ++ i)
				{
					auto const &name(elements[i]);
					
					if (!name.empty())
						set(name.c_str(), i);
				}
			}
		}
	}
#endif
