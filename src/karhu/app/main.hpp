#ifndef KARHU_MAIN_H_
	#define KARHU_MAIN_H_

	#include <karhu/core/platform.hpp>

	// This way modules can override.
	#ifndef karhuMAIN
		/// @todo: Abstract away? Maybe each backend implementation should have its own main header instead that gets included with the backend header.
		#ifdef KARHU_PLATFORM_IOS
			#include <SDL2/SDL_main.h>
		#endif

		#ifdef KARHU_PLATFORM_WEB
			#include <emscripten/emscripten.h>
		#endif

		#if   defined(KARHU_PLATFORM_ANDROID)
			#define karhuMAIN extern "C" int SDL_main(int argc, char *argv[])
		#elif defined(KARHU_PLATFORM_WEB) && !defined(KARHU_WEB_WITHOUT_PLAY_BUTTON)
			#define karhuMAIN EMSCRIPTEN_KEEPALIVE extern "C" int karhu_web_main(int argc = 0, char **argv = nullptr)
		#else
			#define karhuMAIN extern "C" int main(int argc, char *argv[])
		#endif
	#endif
#endif
