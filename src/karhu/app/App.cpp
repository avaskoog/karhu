/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/App.hpp>
#include <karhu/app/module/Pool.hpp>
#include <karhu/app/win/SubsystemWindow.hpp>
#include <karhu/core/platform.hpp>

#include <karhu/app/resources/Script.hpp>
#include <karhu/app/ecs/scriptables/Script.hpp>
#include <karhu/app/scriptutil.hpp>

#include <karhu/app/nav/resources/Navmesh.hpp>

#include <karhu/app/ecs/common.hpp>
#include <karhu/app/ecs/components/Scriptable.hpp>
#include <karhu/app/ecs/systems/SystemScriptable.hpp>

#include <karhu/app/gfx/systems/SystemTransform.hpp>
#include <karhu/app/gfx/systems/SystemPasses.hpp>
#include <karhu/app/gfx/systems/SystemWorkload3D.hpp>
#include <karhu/app/gfx/systems/SystemWorkload2D.hpp>
#include <karhu/app/gfx/systems/SystemRender.hpp>
#include <karhu/app/gfx/systems/SystemAnimator.hpp>
#include <karhu/app/gfx/systems/SystemParticlesystem.hpp>
#include <karhu/app/gfx/systems/SystemBonebinder.hpp>
#include <karhu/app/gfx/components/Transform.hpp>
#include <karhu/app/gfx/components/Camera.hpp>
#include <karhu/app/gfx/components/Renderdata3D.hpp>
#include <karhu/app/gfx/components/Renderdata2D.hpp>
#include <karhu/app/gfx/components/Animator.hpp>
#include <karhu/app/gfx/components/Light.hpp>
#include <karhu/app/gfx/components/Particlesystem.hpp>
#include <karhu/app/gfx/components/Bonebinder.hpp>
#include <karhu/app/gfx/events/ETransform.hpp>
#include <karhu/app/gfx/events/ERenderdata.hpp>

#include <karhu/app/inp/systems/SystemInput.hpp>
#include <karhu/app/inp/components/Inputtable.hpp>
#include <karhu/app/inp/events/EInput.hpp>
#include <karhu/app/inp/events/EConnection.hpp>

#include <karhu/app/phx/systems/SystemPhysics.hpp>
#include <karhu/app/phx/components/Body.hpp>
#include <karhu/app/phx/components/ColliderBox.hpp>
#include <karhu/app/phx/components/ColliderSphere.hpp>
#include <karhu/app/phx/components/ColliderCapsule.hpp>
#include <karhu/app/phx/components/ColliderCylinder.hpp>
#include <karhu/app/phx/components/ColliderMesh.hpp>
#include <karhu/app/phx/events/ECollision.hpp>

#include <karhu/app/gui/systems/SystemScreen.hpp>
#include <karhu/app/gui/systems/SystemLayout.hpp>
#include <karhu/app/gui/systems/SystemLabel.hpp>
#include <karhu/app/gui/components/Screen.hpp>
#include <karhu/app/gui/components/Container.hpp>
#include <karhu/app/gui/components/Button.hpp>
#include <karhu/app/gui/components/Layout.hpp>
#include <karhu/app/gui/components/Label.hpp>
#include <karhu/app/gui/events/EGUI.hpp>

#include <karhu/app/loc/events/ELocalisation.hpp>

#include <karhu/app/nav/components/Navigable.hpp>

#ifdef KARHU_PLATFORM_WEB
	#include <emscripten/emscripten.h>
#endif

#if KARHU_EDITOR_SERVER_SUPPORTED
	#include <karhu/app/edt/Server.hpp>
	#include <karhu/app/gfx/SubsystemGraphics.hpp>
	#include <karhu/app/gfx/Pipeline.hpp>
#endif

//#include <thread>

////// BEGIN TEMPORARY TEST STUFF

//! @todo: Slett oktregreier når ferdigtesta.

#include <karhu/app/backend/implementation/graphics/GL/Shader.hpp>
#include <karhu/app/gfx/utilities.hpp>
#include <karhu/app/lib/im3d/im3d.h>
#include <karhu/app/lib/im3d/im3d_math.h>
#include <algorithm>
#include <array>
#include <karhu/app/tool/SerialiserECS.hpp>
#include <karhu/app/gfx/resources/Material.hpp>

#include <karhu/app/backend/implementation/audio/SDL/lib/soloud/soloud_error.h>
#include <karhu/app/lib/kirja/kirja.hpp>

#include <chrono>
#include <cstring>

// Callback to call from JavaScript due to Chromium's autoplay policy.
#ifdef KARHU_PLATFORM_WEB
	EMSCRIPTEN_KEEPALIVE
	extern "C" void karhu_web_startAudio()
	{
		karhu::app::App::instance()->startAudio();
	}
#endif

class Clock
{
	public:
		Clock(const char *const action)
		:
		start {std::chrono::steady_clock::now()},
		action{action}
		{
		}
	
		void tell()
		{
			auto diff(std::chrono::steady_clock::now() - start);
			auto ms(std::chrono::duration_cast<std::chrono::nanoseconds>(diff));
			//karhu::log::msg("kirja") << "Time to " << action << ": " << (static_cast<double>(ms.count()) / 1000000000.0) << "s";
		}

	private:
		const std::chrono::steady_clock::time_point start;
		const std::string action;
};

bool renderTextToBitmap
(
	karhu::gfx::Pixelbuffer &buf,
	karhu::app::AdapterResourcebank &db,
	karhu::app::Backend &backend
)
{
	using namespace karhu;
	
	static constexpr char const *text
{
"<font src=\"riffic\">"
"<col hex=\"#f4b642\">"
"This"
"</col>"
" is "
"<img src=\"icon\" />"
" multiline string!"
"</font>\n"

"<font src=\"mgenplus\">"
"<col hex=\"#ffffff\">"
"これは"
"<ruby><rb>日</rb><rt>に</rt><rb>本</rb><rt>ほん</rt><rb>語</rb><rt>ご</rt></ruby>"
"です〜\n"
"<ruby><rb>今日</rb><rt>きょう</rt></ruby>"
"の"
"<ruby><rb>会</rb><rt>かい</rt><rb>議</rb><rt>ぎ</rt></ruby>"
"。"
"</col>"
"</font>\n"

"<size pt=\"40\">"
"اللغة"
"<col hex=\"#bff5ff\"> inline LTR </col>"
"العربية"
"\n"
"والأرقام ١٩٩٠ أيضًا!"
"\n"
"ممم، أحب "
"<img src=\"icon\" />"
" القهوة!"
"</size>"
"\n"

"<font src=\"riffic\">"
"<size pt=\"20\">inline</size>"
" font "
"<size pt=\"60\">SiZÊ</size>\n"

"IMAGE "
"<img src=\"icon\" />"
" TAGS!"
"</font>"
};
	
	// Try to load some fonts and put them in a font bank.
	auto fbuf1(backend.files().readBytesFromResource("amiri-regular.ttf"));
	if (!fbuf1)
		return false;
	
	auto fbuf2(backend.files().readBytesFromResource("RifficFree-Bold.ttf"));
	if (!fbuf2)
		return false;
	
	auto fbuf3(backend.files().readBytesFromResource("rounded-mgenplus-2cp-regular.ttf"));
	if (!fbuf3)
		return false;
	
	auto font1(kirja::fontFromMemory(reinterpret_cast<unsigned char *>(fbuf1->data.get()), fbuf1->size));
	if (!font1.success)
	{
		log::err("kirja") << font1.error;
		return false;
	}
	auto font2(kirja::fontFromMemory(reinterpret_cast<unsigned char *>(fbuf2->data.get()), fbuf2->size));
	if (!font2.success)
	{
		log::err("kirja") << font2.error;
		return false;
	}
	auto font3(kirja::fontFromMemory(reinterpret_cast<unsigned char *>(fbuf3->data.get()), fbuf3->size));
	if (!font3.success)
	{
		log::err("kirja") << font2.error;
		return false;
	}
	kirja::Fontbank fontbank;
	fontbank.add("amiri",    &font1.value);
	fontbank.add("riffic",   &font2.value);
	fontbank.add("mgenplus", &font3.value);

	// Create an image bank with an image to render.
	auto icon(db.get<res::Texture>(12));
	if (!icon)
	{
		log::err("kirja") << "Could not get icon texture";
		return false;
	}
	kirja::Imagebank imagebank;
	imagebank.add("icon", {icon->pixels(), icon->width(), icon->height()});

	// Settings for rendering text to bitmap.
	// Everything except the font is optional and set to a sensible default,
	// but we are setting things manually to explore the possiblities!

	kirja::Rendersettings settings;
	settings.fontbank   = &fontbank;
	settings.imagebank  = &imagebank;
	settings.font       = "amiri";
	settings.fontsize   = 45;
	settings.linespace  = 10;
	settings.alignment  = kirja::Alignment::middle;
	settings.inlinement = kirja::Inlinement::middle;
	settings.axis       = kirja::Axis::horizontal;
	settings.colour     = {220, 220, 175}; // Text alpha not currently supported.

	// Since we are mixing scripts and languages in this example,
	// it is better to let kirja (HarfBuzz) guess, but here is
	// an example of what explicig values might look like:
	
	//settings.script     = kirja::Script::arabic;
	//settings.language   = "ar";
	
	// Try to render the bitmap and time it.
	Clock c{"render bitmap"};
	auto bm(bitmapFromInput(text, settings));
	if (!bm.success)
	{
		log::err("kirja") << bm.error;
		return false;
	}
	c.tell();
	
	//log::msg("kirja") << "Bitmap size: " << bm.value.width << 'x' << bm.value.height;

	buf.reserve(bm.value.width, bm.value.height, gfx::Pixelbuffer::Format::RGBA);
	std::memcpy(buf.components.get(), bm.value.buffer.get(), bm.value.size());

	return true;
}

////// END TEMPORARY TEST STUFF

namespace
{
	using namespace karhu;
	using namespace karhu::app;
	
	class SystemBeforeRender : public ecs::SystemWithoutComponents
	{
		public:
			SystemBeforeRender(Hook &hook)
			:
			m_hook{hook}
			{
			}
		
		public:
			void performUpdateNoComponents
			(
				      App   &app,
				const float  dt,
				const float  dtFixed,
				const float  timestep
			) override
			{
				m_hook.updateBeforeRender(app, dt, dtFixed, timestep);
			}
		
		private:
			Hook &m_hook;
	};
	
	template<typename TBank>
	static void readNamesOfLayersForBank
	(
		TBank &bank,
		char const *filename,
		std::vector<std::string> const &hierarchy,
		conv::JSON::Obj const *const root
	)
	{
		conv::JSON::Obj const *parent{root};
	
		for (std::size_t i{0}; i < hierarchy.size(); ++ i)
		{
			auto const &node(hierarchy[i]);
			
			if ((i + 1) != hierarchy.size())
			{
				if (!(parent = parent->getObject(node)))
					break;
			}
			else
			{
				const auto layers(parent->getArray(node));
				
				if (!layers)
					break;
				
				conv::Serialiser const ser{*parent};
				bank.deserialise(ser);
				
				return;
			}
		}
	
		std::string tree;
	
		for (std::size_t i{0}; i < hierarchy.size(); ++ i)
		{
			tree += '\'';
			tree += hierarchy[i];
			tree += '\'';
			
			if ((i + 1) != hierarchy.size())
				tree += " / ";
		}
	
		log::err("Karhu")
			<< "Failed to load layers in '"
			<< filename
			<< '\''
			<< ": "
			<< "array "
			<< tree
			<< " missing";
	}
	
	static Nullable<conv::JSON::Val> loadJSONForSettings
	(
		App &app,
		char const *filename
	)
	{
		const auto contents(app.fio().readStringFromResource(filename));
		
		if (!contents)
		{
			log::err("Karhu")
				<< "Failed to open resource '"
				<< filename
				<< "' to read settings";
			
			return {false};
		}
	
		std::stringstream stream;
		stream << *contents;
	
		conv::JSON::Parser parser;
	
		const auto data(parser.parse(stream));
	
		if (!data)
		{
			log::err("Karhu")
				<< "Failed to parse JSON in '"
				<< filename
				<< "' to read settings"
				<< ": "
				<< parser.error();
			
			return {false};
		}
	
		const auto root(data->object());
		
		if (!root)
		{
			log::err("Karhu")
				<< "Failed to parse JSON in '"
				<< filename
				<< "' to read settings"
				<< ": "
				<< "root object missing";
			
			return {false};
		}
		
		return data;
	}
}

namespace karhu
{
	namespace app
	{
		/*
		 * HOOK
		 */
		
		void Hook::serialise(conv::Serialiser &ser) const
		{
			ser << karhuINn("_userdata", m_userdata);
			performSerialise(ser);
		}
		
		void Hook::deserialise(conv::Serialiser const &ser)
		{
			ser >> karhuOUTn("_userdata", m_userdata);
			
			if (JSON::TypeValue::object != m_userdata.type())
				m_userdata = conv::JSON::Obj{};
			
			performDeserialise(ser);
		}
		
		conv::JSON::Obj *Hook::userdata()
		{
			return m_userdata.object();
		}
		
		conv::JSON::Obj const *Hook::userdata() const
		{
			return m_userdata.object();
		}

		conv::JSON::Val *Hook::userdata(std::string const &name, conv::JSON::Val data)
		{
			if (auto root = m_userdata.object())
			{
				if (auto val = root->get(name))
					*val = std::move(data);
				else
					return &root->push({name, std::move(data)});
			}
			
			return nullptr;
		}
		
		conv::JSON::Val *Hook::userdata(std::string const &name)
		{
			if (auto root = m_userdata.object())
			 	return root->get(name);
			
			return nullptr;
		}
		
		conv::JSON::Val const *Hook::userdata(std::string const &name) const
		{
			if (auto root = m_userdata.object())
			 	return root->get(name);
			
			return nullptr;
		}
		
		/*
		 * APP USERDATA
		 */
		
		conv::JSON::Obj *App::userdataLocal()
		{
			return m_hook.userdata();
		}
		
		conv::JSON::Obj const *App::userdataLocal() const
		{
			return m_hook.userdata();
		}
		
		conv::JSON::Val *App::userdataLocal(std::string const &name, conv::JSON::Val data)
		{
			return m_hook.userdata(name, std::move(data));
		}
		
		conv::JSON::Val *App::userdataLocal(std::string const &name)
		{
			return m_hook.userdata(name);
		}
		
		conv::JSON::Val const *App::userdataLocal(std::string const &name) const
		{
			return m_hook.userdata(name);
		}
		
		char const *App::pathUserdataLocal() const
		{
			return "_hook.karhudat.json";
		}
		
		bool App::loadUserdataLocal()
		{
			auto const
				contents(fio().readStringFromResource(pathUserdataLocal()));
		
			if (!contents)
			{
				log::err("Karhu")
					<< "Failed to open resource '"
					<< pathUserdataLocal()
					<< "' to deserialise local userdata";
				
				return false;
			}
		
			std::stringstream
				stream;
			
			stream << *contents;
		
			conv::JSON::Parser
				parser;
		
			auto const
				data(parser.parse(stream));
		
			if (!data)
			{
				log::err("Karhu")
					<< "Failed to parse JSON in '"
					<< pathUserdataLocal()
					<< "' to deserialise local userdata: "
					<< parser.error();
				
				return false;
			}
		
			auto const
				root(data->object());
		
			if (root)
			{
				conv::Serialiser ser{*root};
				m_hook.deserialise(ser);
			}
			
			log::msg() << "Successfully loaded local userdata";
			
			return true;
		}
		
		bool App::saveUserdataLocal()
		{
			conv::JSON::Val
				val{conv::JSON::Obj{}};
			
			conv::Serialiser
				ser{*val.object()};
			
			m_hook.serialise(ser);
			
			std::string const
				data{val.dump()};
			
			return fio().writeStringToResource(pathUserdataLocal(), data.c_str());
		}
		
		/*
		 * LOCALISATION
		 */
		
		void App::reloadLocalisations(std::string const &locale)
		{
			m_loc.clearModules();
		
			constexpr char const *filename{"_localisation.csv"};
		
			const auto contents(fio().readStringFromResource(filename));
		
			if (!contents)
			{
				log::err("Karhu") << "Failed to open resource '_localisation.csv'";
				return;
			}
			
			log::msg("Karhu") << "Loaded '_localisation.csv'";
			
			if (!locale.empty())
				m_loc.locale(locale);
			
			if (loc().loadFromCSVInMemory(*contents))
				log::msg("Karhu")
					<< "Successfully loaded in localisations for locale "
					<< m_loc.locale();
			else
				log::err("Karhu") << "Failed to load in localisations";

			ecs().emitEvent<loc::ELocalisation>
			(
				backend().deltatime(),
				backend().deltatimeFixed(),
				backend().timestep()
			);
		}
		
		/*
		 * APP GENERAL
		 */

		App *App::instance() noexcept { return c_instance; }
		void App::instance(App *const app) noexcept { c_instance = app; }
		
		App::App
		(
			const char *caption,
			const char *ext,
			const char *org,
			const char *proj,
			Backend &backend,
			Hook &hook
		)
		:
		m_backend   {&backend},
		m_hook      {hook},
		m_caption   {caption},
		m_identifier{ext, org, proj},
		m_scripts   {*this},
		m_ECS       {*this, m_scripts},
		m_loc       {*this}
		{
			if (c_instance == nullptr)
				c_instance = this;
			
			/// @todo: Just noticed that in macOS the global menu item and dock name for the app remains without this addition; might want to look into.
			#ifdef KARHU_EDITOR
			//m_caption += " | Editor";
			#endif
		}
		
		bool App::init(int argc, char *argv[])
		{
			/// @todo: Quick hack to test the debug client on web until I get arguments working.
			#ifdef KARHU_EDITOR
			#ifdef KARHU_PLATFORM_WEB
			std::unique_ptr<char *[]> fakeargs;
			if (0 == argc)
			{
				argc = 6;
				fakeargs = std::make_unique<char *[]>(argc);
				fakeargs[0] = "";
				fakeargs[1] = "-client";
				fakeargs[2] = "-host";
				fakeargs[3] = "Vanessa-Hudgens.local";
				fakeargs[4] = "-port";
				fakeargs[5] = "14300";
				argv = fakeargs.get();
			}
			#endif
			#endif
			
			// In editor mode, create the logger implementation
			// before anything else, to catch all the logging.
			#if KARHU_EDITOR_SERVER_SUPPORTED
			auto logger(std::make_unique<edt::Logger>(*this));
			log::Logger::implementation(logger.get());
			#endif
			
			static int initi=0;
			log::dbg("Karhu") << "---- INIT " << (++initi) << " ----";
			/// @todo: Fix Karhu versioning.
			log::msg("Karhu") << "Karhu 🐨 0.0.0";

			const auto args(m_backend->separateArgs(static_cast<std::size_t>(argc), argv));
			
			if (!args)
				return false;

			log::msg("DEBUG") << "argc = " << argc;
			log::msg("DEBUG") << "argv = " << static_cast<void *>(&argv[0]);
			
			for (int i = 0; i < argc; ++ i)
				log::msg("DEBUG") << "argv[" << i << "] = " << argv[i];

			log::msg("DEBUG") << "ARGUMENTS:";
			
			for (const auto &arg : *args)
			{
				std::string s;
				for (const auto &brg : arg.second)
				{
					s += ' ';
					s += brg;
				}
				log::msg("DEBUG") << "ARG: " << arg.first << " (" << arg.second.size() <<  ") " << s;
			}
			
			#ifdef KARHU_EDITOR
				std::string basepath;
			
				bool supported
				{
				#if KARHU_EDITOR_SERVER_SUPPORTED
				true
				#else
				false
				#endif
				};
			
				if (!supported || m_backend->findArg(*args, "client"))
				{
					m_mode = ModeApp::client;
					
					log::msg("Karhu") << "Debug client enabled";
					
					if (m_backend->findArg(*args, "offline"))
						log::msg("Karhu") << "Running in offline mode";
				}
				#if KARHU_EDITOR_SERVER_SUPPORTED
				else
				{
					m_mode = ModeApp::server;
				
					if (auto arg = m_backend->findArg(*args, "projdir"))
						if (!arg->second.empty())
							basepath = arg->second[0];
					
					log::msg("Karhu") << "Running in editor mode";
					log::msg("Karhu") << "Debug server enabled";
				}
				#endif
			#endif

			// Always initialise the backend before anything else.
			if (!m_backend->init(*this, argc, argv, *args))
				return false;
			
			// The resource bank needs to be set for subsequent operations to be able to access it.
			m_res = std::make_unique<AdapterResourcebank>
			(
				std::make_unique<fio::SubsystemFile::AdapterRead>
				(
					fio().adapterRead()
				),
				std::make_unique<fio::SubsystemFile::AdapterWrite>
				(
					fio().adapterWrite()
				)
			);
			
			// Register script resource derivation.
			m_res->registerDerivedType<res::Script, Script>();
			
			/// @todo: No longer want all the resource types residing in res::, it's getting too messy, just register each derived type by itself without needing the polymorphism (tho backend-specific implementations might still need it, like gfx::Mesh > GL::Mesh but I feel that can be improved upon too if the GL gets abstraced better)
			/// @todo: Worry about these hardcoded resource IDs later.
			{
				res::IDTypeResource ID{14};
				
				res().registerType<nav::Navmesh>((ID ++), "nav::Navmesh", "nav");
				res().registerDerivedType<nav::Navmesh, nav::Navmesh>();
				
				// ...
			}
			
			// Scripting needs to be initialised after resource bank.
			
			if (!scr().init())
				return false;
			
			if (!scr().initResources())
				return false;
		
			// Try to load up the resource database.
			{
				const auto s(res().load());
				if (!s.success)
				{
					log::err("Karhu") << "Failed to load resource DB: " << s.error;
					return false;
				}
			}

			/// @todo: Finish up module stuff and get back on this.
			/*
			auto pool(modules().createPool("module"));

			#ifdef KARHU_EDITOR /// @todo: Maybe should be KARHU_EDITOR or something instead.
				auto pool(modules().getPool("module"));

				if (pool)
				{
					// LOAD MODULE
					if (modules().updatePoolFromLibrary
					(
						 *pool,
						 "/Users/avaskoog/Documents/karhutestingz/alimcbabber/build/mac/bin/module/debug/libmodule.dylib"
					))
						log::msg("debug") << "Loaded module at startup";
					else
						log::msg("debug") << "Failed to load module at startup";

					auto mod1(pool->create("alimcbabber::Submodular"));
				}
			#endif
			*/

			// Try to create the main game window.
			
			if (!(m_window = win().createWindow()))
				return false;
			
			/// @todo: Load base resolution from config file, perhaps find an optimal default for the current system if there is none.
			/// @todo: Read width and height and graphics backend from settings file? Also allow editor to specify.
			
			std::uint32_t
				w{0},
				h{0};
			
			win::Flags winflags
			{
				win::Flags::graphicsOpenGL |
				win::Flags::resizable      |
				win::Flags::highDPI
			};
			
			for (auto const &arg : *args)
			{
				if (arg.first == "w" && !arg.second.empty())
					w = std::stoi(arg.second[0]);
				else if (arg.first == "h" && !arg.second.empty())
					h = std::stoi(arg.second[0]);
				else if (arg.first == "nodpi")
					winflags &= ~win::Flags::highDPI;
				else if (arg.first == "fillscreen")
					winflags |= win::Flags::fillscreen;
				else if (arg.first == "fullscreen")
					winflags |= win::Flags::fullscreen;
			}
			
			if (0 == w)
				w = 680/2;
			
			if (0 == h)
				h = 425/2+1;
			
			#if KARHU_EDITOR_SERVER_SUPPORTED
			if (ModeApp::server == mode())
				winflags |= win::Flags::fillscreen;
			#endif
			
			if (!m_window->init(m_caption, w, h, winflags))
				return false;
			
			if (!(m_context = win().createContext()))
				return false;
			
			if (!m_context->init(*m_window))
				return false;
			
			m_window->context(m_context);
					
			// Try to initialise the subsystems.
			
			if (!gfx().init(*this))
				return false;
			
			if (!m_backend->initImGui(*m_window))
				return false;
			
			if (!m_backend->initIm3D(*m_window))
			{
				log::err("Karhu") << "Failed to initialise Im3d";
				/// @todo: This fails at least on iOS so fix it.
				//return false;
			}
			
			// Log all versions.

			#ifdef KARHU_PLATFORM_WEB
			{
				auto ev(reinterpret_cast<char *>(emscripten_get_compiler_setting("EMSCRIPTEN_VERSION")));
				log::msg("Karhu") << "Emscripten " << ev;
			}
			#endif
			
			m_backend->logVersions();
			scr().logVersion();

			// Try to create a renderer.
			if (!gfx().initRenderer())
				return false;
			
			{
				// Register all the ECS component types.
				
				/// @todo: Maybe we should do something like with the scriptable registration API where attempts to register non-built-in types aren't even possible?
				karhuREGISTER_TYPE_COMPONENT(ecs(), gfx::Transform,        0,  1)
				karhuREGISTER_TYPE_COMPONENT(ecs(), gfx::Camera,           1,  1)
				karhuREGISTER_TYPE_COMPONENT(ecs(), gfx::Renderdata3D,     2,  0)
				karhuREGISTER_TYPE_COMPONENT(ecs(), gfx::Animator,         3,  1)
				karhuREGISTER_TYPE_COMPONENT(ecs(), inp::Inputtable,       4,  1)
				karhuREGISTER_TYPE_COMPONENT(ecs(), phx::Body,             5,  1)
				karhuREGISTER_TYPE_COMPONENT(ecs(), phx::ColliderBox,      6,  0)
				karhuREGISTER_TYPE_COMPONENT(ecs(), phx::ColliderSphere,   7,  0)
				karhuREGISTER_TYPE_COMPONENT(ecs(), phx::ColliderCapsule,  8,  0)
				karhuREGISTER_TYPE_COMPONENT(ecs(), phx::ColliderMesh,     9,  0)
				karhuREGISTER_TYPE_COMPONENT(ecs(), gfx::Renderdata2D,     10, 0)
				karhuREGISTER_TYPE_COMPONENT(ecs(), gui::Screen,           11, 1)
				karhuREGISTER_TYPE_COMPONENT(ecs(), gui::Container,        12, 1)
				karhuREGISTER_TYPE_COMPONENT(ecs(), gui::Button,           13, 1)
				karhuREGISTER_TYPE_COMPONENT(ecs(), gui::Layout,           14, 1)
				karhuREGISTER_TYPE_COMPONENT(ecs(), gui::Label,            15, 1)
				karhuREGISTER_TYPE_COMPONENT(ecs(), ecs::Scriptable,       16, 0)
				karhuREGISTER_TYPE_COMPONENT(ecs(), gfx::Light,            17, 1)
				karhuREGISTER_TYPE_COMPONENT(ecs(), gfx::Particlesystem,   18, 1)
				karhuREGISTER_TYPE_COMPONENT(ecs(), nav::Navigable,        19, 1)
				karhuREGISTER_TYPE_COMPONENT(ecs(), phx::ColliderCylinder, 20, 0)
				karhuREGISTER_TYPE_COMPONENT(ecs(), gfx::Bonebinder,       21, 0)
				
				// Register the ECS event types.
				
				/// @todo: Maybe we should do something like with the scriptable registration API where attempts to register non-built-in types aren't even possible?
				karhuREGISTER_TYPE_EVENT(ecs(), inp::EInput,        101)
				karhuREGISTER_TYPE_EVENT(ecs(), inp::EConnection,   102)
				karhuREGISTER_TYPE_EVENT(ecs(), phx::ECollision,    103)
				karhuREGISTER_TYPE_EVENT(ecs(), gui::EGUI,          104)
				karhuREGISTER_TYPE_EVENT(ecs(), gfx::ETransform,    105)
				karhuREGISTER_TYPE_EVENT(ecs(), gfx::ERenderdata,   106)
				karhuREGISTER_TYPE_EVENT(ecs(), loc::ELocalisation, 107)

				// Create all the ECS system types.

				phx::SystemPhysics *systemPhysics{nullptr};
				gui::SystemScreen  *systemScreen {nullptr};
				gui::SystemLabel   *systemLabel  {nullptr};
				
				auto r1(ecs().createSystemWithPriorityAndArgs<phx::SystemPhysics>
				(
					ecs::Priorities::preprerender - 3
				));
				
				if (r1.success)
					systemPhysics = r1.value;
				
				if (ModeApp::server != mode())
				{
	//				ecs().createSystemWithPriorityAndArgs<gfx::SystemTransform>
	//				(
	//					ecs::Priorities::preprerender - 2
	//				);
				}
				
				ecs().createSystemWithPriorityAndArgs<inp::SystemInput>
				(
					ecs::Priorities::preprerender - 1
				);
				
				auto r2(ecs().createSystemWithPriorityAndArgs<gui::SystemScreen>
				(
					ecs::Priorities::preprerender
				));
				
				if (r2.success)
					systemScreen = r2.value;
				
				ecs().createSystemWithPriorityAndArgs<gfx::SystemAnimator>
				(
					ecs::Priorities::preprerender
				);
				
				ecs().createSystemWithPriorityAndArgs<gfx::SystemBonebinder>
				(
					ecs::Priorities::preprerender
				);
				
				if (ModeApp::server != mode())
				{
					ecs().createSystemWithPriorityAndArgs<SystemBeforeRender>
					(
						ecs::Priorities::prerender,
						m_hook
					);
				
					ecs().createSystemWithPriorityAndArgs<ecs::SystemScriptableBeforeRender>
					(
						ecs::Priorities::prerender
					);
				}
				
				auto r3(ecs().createSystemWithPriorityAndArgs<gui::SystemLabel>
				(
					ecs::Priorities::render - 4
				));
				
				if (r3.success)
					systemLabel = r3.value;
				
				ecs().createSystemWithPriorityAndArgs<gui::SystemLayout>
				(
					ecs::Priorities::render - 3
				);
				
				ecs().createSystemWithPriorityAndArgs<gfx::SystemPasses>
				(
					ecs::Priorities::render - 2,
					gfx().workload()
				);
				
				ecs().createSystemWithPriorityAndArgs<gfx::SystemWorkload3D>
				(
					ecs::Priorities::render - 1,
					gfx().workload()
				);
				
				ecs().createSystemWithPriorityAndArgs<gfx::SystemWorkload2D>
				(
					ecs::Priorities::render - 1,
					gfx().workload()
				);
				
				ecs().createSystemWithPriorityAndArgs<gfx::SystemParticlesystem>
				(
					ecs::Priorities::render - 1,
					gfx().workload()
				);
				
				ecs().createSystemWithPriorityAndArgs<gfx::SystemRender>
				(
					ecs::Priorities::render,
					*m_window,
					*gfx().renderer(),
					gfx().pipeline(),
					gfx().workload(),
					gfx().inputs()
				);
				
				if (ModeApp::server != mode())
					ecs().createSystemWithPriorityAndArgs<ecs::SystemScriptableAfterRender>
					(
						ecs::Priorities::postrender
					);
				
				// Register event listeners.
				
				if (ModeApp::server != mode())
				{
					// Scriptable.
				
					ecs().createListenerToEvent<ecs::Scriptable, ecs::EComponentCreated>
					(
						nullptr,
						ecs::listenerScriptableCreated,
						-1
					);
					
					ecs().createListenerToEvent<ecs::Scriptable, ecs::EComponentActive>
					(
						nullptr,
						ecs::listenerScriptableActive,
						-1
					);
					
					ecs().createListenerToEvent<ecs::Scriptable, ecs::EComponentInactive>
					(
						nullptr,
						ecs::listenerScriptableInactive,
						-1
					);
					
					ecs().createListenerToEvent<inp::Inputtable, inp::EInput>
					(
						nullptr,
						ecs::listenerScriptableInput,
						-1
					);
					
					// Particlesystem.
					
					ecs().createListenerToEvent<gfx::Particlesystem, ecs::EComponentActive>
					(
						nullptr,
						gfx::SystemParticlesystem::listenerParticlesystemActive
					);
					
					// Animator.

					ecs().createListenerToEvent<gfx::Animator, ecs::EComponentCreated>
					(
						nullptr,
						gfx::SystemAnimator::listenerAnimatorCreated
					);
				}
				
				// GUI screen.

				ecs().createListenerToEvent<inp::Inputtable, inp::EInput>
				(
					systemScreen,
					gui::SystemScreen::listenerInput,
					std::numeric_limits<ecs::Priority>::lowest()
				);
				
				// GUI label.
				
				ecs().createListenerToEvent<loc::ELocalisation>
				(
					systemLabel,
					gui::SystemLabel::listenerLocalisation,
					std::numeric_limits<ecs::Priority>::lowest()
				);
					
				// Physics body.
				
				ecs().createListenerToEvent<phx::Body, ecs::EComponentCreated>
				(
					systemPhysics,
					phx::SystemPhysics::listenerBodyCreated
				);
				
				ecs().createListenerToEvent<phx::Body, ecs::EComponentDestroyed>
				(
					systemPhysics,
					phx::SystemPhysics::listenerBodyDestroyed
				);
				
				// Box collider.
				
				ecs().createListenerToEvent<phx::ColliderBox, ecs::EComponentCreated>
				(
					systemPhysics,
					phx::SystemPhysics::listenerColliderBoxCreated
				);
				
				ecs().createListenerToEvent<phx::ColliderBox, ecs::EComponentDestroyed>
				(
					systemPhysics,
					phx::SystemPhysics::listenerColliderBoxDestroyed
				);
				
				// Sphere collider.
				
				ecs().createListenerToEvent<phx::ColliderSphere, ecs::EComponentCreated>
				(
					systemPhysics,
					phx::SystemPhysics::listenerColliderSphereCreated
				);
				
				ecs().createListenerToEvent<phx::ColliderSphere, ecs::EComponentDestroyed>
				(
					systemPhysics,
					phx::SystemPhysics::listenerColliderSphereDestroyed
				);
				
				// Capsule collider.
				
				ecs().createListenerToEvent<phx::ColliderCapsule, ecs::EComponentCreated>
				(
					systemPhysics,
					phx::SystemPhysics::listenerColliderCapsuleCreated
				);
				
				ecs().createListenerToEvent<phx::ColliderCapsule, ecs::EComponentDestroyed>
				(
					systemPhysics,
					phx::SystemPhysics::listenerColliderCapsuleDestroyed
				);
				
				// Cylinder collider.
				
				ecs().createListenerToEvent<phx::ColliderCylinder, ecs::EComponentCreated>
				(
					systemPhysics,
					phx::SystemPhysics::listenerColliderCylinderCreated
				);
				
				ecs().createListenerToEvent<phx::ColliderCylinder, ecs::EComponentDestroyed>
				(
					systemPhysics,
					phx::SystemPhysics::listenerColliderCylinderDestroyed
				);
				
				// Mesh collider.
				
				ecs().createListenerToEvent<phx::ColliderMesh, ecs::EComponentCreated>
				(
					systemPhysics,
					phx::SystemPhysics::listenerColliderMeshCreated
				);
				
				ecs().createListenerToEvent<phx::ColliderMesh, ecs::EComponentDestroyed>
				(
					systemPhysics,
					phx::SystemPhysics::listenerColliderMeshDestroyed
				);
				
				// Graphics transform
				
				ecs().createListenerToEvent<gfx::Transform, gfx::ETransform>
				(
					nullptr,
					[](app::App &app, const gfx::ETransform &e, float const, float const, float const, void *) -> void
					{
						app.octree().updateEntity(e.entity());
					}
				);
			}
			
			// Allow custom types.
			
			loadUserdataLocal();
			
			if (!m_hook.registerTypes(*this))
				return false;
			
			//if (ModeApp::server != mode())
				if (!m_hook.registerSystemsAndListeners(*this))
					return false;
			
			if (!m_hook.registerScripting(*this))
				return false;
			
			// Now that the ECS things are registered we can bind them for scripting too.
			if (!scr().initECS())
				return false;
			
			/// @todo: Remove this when done testing.
			if (!initTemporaryStuff())
				return false;
			
			reloadLocalisations();
			
			{
				constexpr char const
					*filename{"_graphics.karhudat.json"};
				
				auto
					data(loadJSONForSettings(*this, filename));
				
				if (data)
				{
					auto const
						root(data->object());
					
					readNamesOfLayersForBank(gfx().layers(), filename, {gfx().layers().label()}, root);
				
					if (auto const pipeline = root->getObject("pipeline"))
					{
						conv::Serialiser ser{*pipeline};
						gfx().pipeline().deserialise(ser);
					}
				}
			}
			
			{
				constexpr char const
					*filename{"_physics.karhudat.json"};
				
				auto
					data(loadJSONForSettings(*this, filename));
				
				if (data)
				{
					auto const
						root(data->object());
					
					readNamesOfLayersForBank(phx().layers(), filename, {phx().layers().label()}, root);
				}
			}

			// Set the callback for the window to call every frame.
			m_window->callbackUpdate = performUpdate;
			
			#ifdef KARHU_EDITOR
			if (ModeApp::client == mode())
				m_backend->callbackMessageFromServer = callbackMessageFromServer;
			#endif
			
			m_valid = true;
			
			// At this point we can tell Emscripten that it's safe
			// to start listening for user input to activate audio.
			#ifdef KARHU_PLATFORM_WEB
			emscripten_run_script("karhu_notifyInitDone()");
			#endif
			
			// 0 is not a valid resource (and thereby scene) ID
			// so we reserve it for background ECS stuff.
			{
				const auto s(ecs().createScene(0));
				
				if (!s.success)
				{
					log::err("Karhu") << "Failed to create background scene: " << s.error;
					return false;
				}
			}
			
			// Create default GUI camera.
			{
				m_cameraGUI.texture = gfx().createTexture
				(
					gfx::Texture::Type::colour,
					gfx::Pixelbuffer::Format::RGBA
				);
				
				m_cameraGUI.depth = gfx().createTexture
				(
					gfx::Texture::Type::depth
				);
				
				m_cameraGUI.target = gfx().createRendertarget();
				
				if (!m_cameraGUI.texture || !m_cameraGUI.depth || !m_cameraGUI.target)
				{
					log::err("Karhu") << "Failed to set up GUI camera rendertarget";
					return false;
				}
				
				m_cameraGUI.target->texture(*m_cameraGUI.texture);
				m_cameraGUI.target->depth(*m_cameraGUI.depth);
				m_cameraGUI.target->tieSizeToViewport(true);
				
				ecs::IDEntity const IDE{ecs().createEntityInScene(0, "_cam-gui")};
				
				if (0 == IDE)
				{
					log::err("Karhu") << "Failed to set up GUI camera entity";
					return false;
				}
				
				/// @todo: Shouldn't be needing this if we get dependencies for components working, since camera should depend on transform and create one automatically.
				if (!ecs().createComponentInEntity<gfx::Transform>(IDE))
				{
					log::err("Karhu") << "Failed to set up GUI camera transform component";
					return false;
				}
				
				if (auto c = ecs().createComponentInEntity<gfx::Camera>(IDE))
				{
					c->mode(gfx::ModeCamera::GUI);
					c->layers(gfx::Defaultlayers::GUI);
					c->target(m_cameraGUI.target.get());
					c->clearcolour({0.5f, 0.5f, 0.5f, 0.0f});
					
					gfx::Projection p{c->projection()};
					p.near = -1000.0f;
					p.far  =  1000.0f;
					p.type = gfx::TypeProjection::orthographic;
					c->projection(p);
				}
				else
				{
					log::err("Karhu") << "Failed to set up GUI camera camera component";
					return false;
				}
			}
			
			#if KARHU_EDITOR_SERVER_SUPPORTED
			if (ModeApp::server == mode())
			{
				// Pass ownership of the logger on to the editor.
				m_editor = std::make_unique<edt::Editor>(std::move(logger));
				
				if (!m_editor->initBeforeHook(*this, basepath, argc, argv))
					return false;
				
				if (!m_hook.registerEditors(*this, *m_editor))
					return false;
				
				if (!m_editor->initAfterHook(*this))
					return false;
			}
			#endif
			
			// Load the start scene.
			const auto startscene(loadStartscene());
			if (!startscene)
				return false;
			
			// Initialise the hook.
			#ifdef KARHU_EDITOR
			if (ModeApp::client == mode())
			#endif
				if (!m_hook.init(*this, *startscene, argc, argv))
					return false;
			
			m_octree.init(*this, {{0.0f, 0.0f, 0.0f}, {200.0f, 200.0f, 200.0f}});
			
			return true;
		}
		
		void App::run()
		{
			if (!m_valid)
				return;
			
			m_backend->loop();
		}
static Im3d::Mat4 M{1.0f};
		void App::update(const float dt, const float dtFixed, const float timestep)
		{
			////// BEGIN TEMPORARY TEST STUFF
			
			#if defined(KARHU_EDITOR) || defined(KARHU_DEBUG)
				// Some debug draw testing.
		/*		const auto cam   (ecs().componentsInActiveScenes<gfx::Camera>()[0]);
				const auto campos(ecs().componentInEntity<gfx::Transform>(cam->entity()));
			
				if (const auto w = win().top())
				{
					cam->updateProjection(w->width(), w->height(), w->scaleWidth(), w->scaleHeight());
					cam->recalculateProjection();
				}
			
				const mth::Mat4f
					view    {cam->matrixView(*campos)},
					proj    {cam->matrixProjection()},
					world   {campos->global().matrix()},
					viewproj{proj * view};
			
				const auto camdir{mth::normalise(mth::Vec3f
				{
					view[2][0],
					view[2][1],
					-view[2][2]
				})};
			
				const Im3d::Mat4
					W {mth::matrixIm3D(world)},
					P {mth::matrixIm3D(proj)},
					VP{mth::matrixIm3D(viewproj)};
			
				if (const auto w = win().top())
				{
					gfx().funcIm3DNewFrame()
					(
						dt,
						static_cast<float>(w->width()),
						static_cast<float>(w->height()),
						false,
						{
							campos->global().position.x,
							campos->global().position.y,
							campos->global().position.z
						},
						{camdir.x, camdir.y, camdir.z},
						cam->projection.FOV,
						W,
						P,
						VP,
						{
							static_cast<float>(inp().mouseX()),
							static_cast<float>(inp().mouseY())
						},
						inp().mouseHeld(inp::Mouse::left),
						inp().keyHeld(inp::Key::control),
						inp().keyPressed(inp::Key::l),
						inp().keyHeld(inp::Key::t),
						inp().keyHeld(inp::Key::r),
						inp().keyHeld(inp::Key::s)
					);
				}
			*/
				// Camera frustum debug draw.
			
				if (const auto w = win().top())
				{/*
					{
						const mth::Boxf box{{0.0f, 0.0f, 3.0f}, {20.0f, 10.0f, 30.0f}}, box2{box.centre, box.size * 2.0f};
						const auto b{box.bounds()}, b2{box2.bounds()};
						
						const auto frustum(cam->frustum(*campos));
						const bool seen{frustum.contains(box)};
						
						Im3d::PushDrawState();
						Im3d::PushMatrix();
							Im3d::SetColor({0.0f, 0.0f, 0.0f, 1.0f});
							Im3d::DrawAlignedBoxFilled({b.min.x, b.min.y, b.min.z}, {b.max.x, b.max.y, b.max.z});
						Im3d::PopMatrix();
						Im3d::PopDrawState();
						
						Im3d::PushDrawState();
						Im3d::PushMatrix();
							Im3d::SetColor((seen) ? Im3d::Color{0.0f, 1.0f, 0.0f, 0.3f} : Im3d::Color{1.0f, 0.0f, 0.0f, 0.3f});
							Im3d::DrawAlignedBoxFilled({b2.min.x, b2.min.y, b2.min.z}, {b2.max.x, b2.max.y, b2.max.z});
						Im3d::PopMatrix();
						Im3d::PopDrawState();
					}*/
					/*
					for (const auto &rd : ecs().componentsInActiveScenes<gfx::Renderdata>())
					{
						if (ecs().entityName(rd->entity()) == "pmesh")
						{
							const auto t(ecs().componentInEntity<gfx::Transform>(rd->entity()));
							const auto b(rd->AABB(*t));
							Im3d::PushDrawState();
							Im3d::PushMatrix();
								Im3d::SetColor({1.0f, 1.0f, 0.0f, 0.5f});
								Im3d::DrawAlignedBoxFilled({b.min.x, b.min.y, b.min.z}, {b.max.x, b.max.y, b.max.z});
							Im3d::PopMatrix();
							Im3d::PopDrawState();
							Im3d::PushDrawState();
							Im3d::PushMatrix();
								Im3d::SetColor({0.2f, 0.2f, 0.0f, 0.75f});
								Im3d::SetSize(4.0f);
								Im3d::DrawAlignedBox({b.min.x, b.min.y, b.min.z}, {b.max.x, b.max.y, b.max.z});
							Im3d::PopMatrix();
							Im3d::PopDrawState();
						}
					}
					*/
				/*
					const mth::Vec4f *const planes[]
					{
						&cam->planes().left,
						&cam->planes().right,
						&cam->planes().top,
						&cam->planes().bottom,
						&cam->planes().near,
						&cam->planes().far
					};
					
					const float f{0.95f};

					mth::Vec3f camFwd = campos->global().rotation * mth::forwardf();
					mth::Vec3f camUp = campos->global().rotation * mth::upf();
					mth::Vec3f camRight = campos->global().rotation * mth::rightf();
					
					mth::Vec3f nearCenter = campos->global().position - camFwd * cam->projection.near * 1.1f;
					mth::Vec3f farCenter = campos->global().position - camFwd * cam->projection.far;

					float nearHeight = 2.0f * mth::tan(cam->projection.FOV / 2.0f) * cam->projection.near;
					float farHeight = 2.0f * mth::tan(cam->projection.FOV / 2.0f) * cam->projection.far;
					float nearWidth = nearHeight * cam->ratio();
					float farWidth = farHeight * cam->ratio();
					
					nearHeight *= f;
					farHeight *= f;
					nearWidth *= f;
					farWidth *= f;

					mth::Vec3f farTopLeft = farCenter + camUp * (farHeight*0.5f) - camRight * (farWidth*0.5f);
					mth::Vec3f farTopRight = farCenter + camUp * (farHeight*0.5f) + camRight * (farWidth*0.5f);
					mth::Vec3f farBottomLeft = farCenter - camUp * (farHeight*0.5f) - camRight * (farWidth*0.5f);
					mth::Vec3f farBottomRight = farCenter - camUp * (farHeight*0.5f) + camRight * (farWidth*0.5f);

					mth::Vec3f nearTopLeft = nearCenter + camUp * (nearHeight*0.5f) - camRight * (nearWidth*0.5f);
					mth::Vec3f nearTopRight = nearCenter + camUp * (nearHeight*0.5f) + camRight * (nearWidth*0.5f);
					mth::Vec3f nearBottomLeft = nearCenter - camUp * (nearHeight*0.5f) - camRight * (nearWidth*0.5f);
					mth::Vec3f nearBottomRight = nearCenter - camUp * (nearHeight*0.5f) + camRight * (nearWidth*0.5f);
					
					auto vec([](const mth::Vec3f &v) { return Im3d::Vec3{v.x, v.y, v.z}; });
					auto line([&vec](const mth::Vec3f &a, const mth::Vec3f &b)
					{
						Im3d::DrawLine(vec(a), vec(b), 15.0f, {0.0f, 0.2f, 1.0f, 1.0f});
					});
					
					line(nearTopLeft, nearTopRight);
					line(nearTopRight, nearBottomRight);
					line(nearBottomRight, nearBottomLeft);
					line(nearBottomLeft, nearTopLeft);
					
					line(farTopLeft, farTopRight);
					line(farTopRight, farBottomRight);
					line(farBottomRight, farBottomLeft);
					line(farBottomLeft, farTopLeft);

					line(nearTopLeft, farTopLeft);
					line(nearTopRight, farTopRight);
					line(nearBottomLeft, farBottomLeft);
					line(nearBottomRight, farBottomRight);
				*/
				}
			
				// Recast debug draw.
			/*
				{
					Im3d::PushDrawState();
					Im3d::PushMatrix();
					
					if (input().keyHeld(inp::Key::y))
					{
						const std::size_t nvp2(polymesh.nvp * 2);
						
						for (std::size_t i{0}; i < polymesh.npolys * nvp2; i += nvp2)
						{
							std::vector<unsigned short> verts;
							
							for (std::size_t j{0}; j < polymesh.nvp; ++ j)
							{
								const auto v(*(polymesh.polys + i + j));
								
								if (RC_MESH_NULL_IDX == v)
									break;
								
								verts.emplace_back(v);
							}
							
							for (std::size_t j{0}; j < verts.size(); ++ j)
							{
								const std::size_t k{((j + 1) == verts.size()) ? 0 : (j + 1)};
								const auto ia(verts[j]);
								const auto ib(verts[k]);
								const auto va(polymesh.verts + ia * 3);
								const auto vb(polymesh.verts + ib * 3);
							
								const auto ax{va + 0};
								const auto ay{va + 1};
								const auto az{va + 2};
								
								const auto bx{vb + 0};
								const auto by{vb + 1};
								const auto bz{vb + 2};
								
								Im3d::DrawLine
								(
									{
										polymesh.bmin[0] + static_cast<float>(*ax) * polymesh.cs,
										polymesh.bmin[1] +static_cast<float>(*ay) * polymesh.ch,
										polymesh.bmin[2] +static_cast<float>(*az) * polymesh.cs
									},
									{
										polymesh.bmin[0] +static_cast<float>(*bx) * polymesh.cs,
										polymesh.bmin[1] +static_cast<float>(*by) * polymesh.ch,
										polymesh.bmin[2] +static_cast<float>(*bz) * polymesh.cs
									},
									6.0f,
									{1.0f, 1.0f, 0.0f, 0.5f}
								);
							}
						}
					}
				 
					if (input().keyHeld(inp::Key::b))
					{
						for (std::size_t i{0}; i < pathverts.size() - 1; ++ i)
						{
							Im3d::DrawLine
							(
								pathverts[i],
								pathverts[i + 1],
								15.0f,
								{0.0f, 0.2f, 1.0f, 0.4f}
							);
						}
					}
					
					Im3d::PopMatrix();
					Im3d::PopDrawState();
				}*/
			#endif
			
			////// END TEMPORARY TEST STUFF
			
			#if KARHU_EDITOR_SERVER_SUPPORTED
			if (ModeApp::server == mode())
				m_editor->updateAndRender(*this);
			else
			#endif
				m_hook.updateBeforeECS(*this, dt, dtFixed, timestep);
			
			//m_octree.update(); //! @todo: Should octree update before or after ECS? In between some steps?
			ecs().update(dt, dtFixed, timestep);
			
			//! @todo: Slett oktregreier når ferdigtesta.
			#if KARHU_EDITOR_SERVER_SUPPORTED
			if (ModeApp::client == mode())
			#endif
			m_octree.draw(*this);
			
			#if KARHU_EDITOR_SERVER_SUPPORTED
			if (ModeApp::client == mode())
			#endif
				m_hook.updateAfterECS(*this, dt, dtFixed, timestep);
			
			////// BEGIN TEMPORARY TEST STUFF //////
			
			#if defined(KARHU_EDITOR) || defined(KARHU_DEBUG)
				if (const auto w = win().top())
				{
					/*
					for (const auto t : ecs().componentsInActiveScenes<gfx::Transform>())
					{
						if (!ecs().entityActive(t->entity()))
							continue;
						
						const auto box(ecs().componentInEntity<phx::ColliderBox>(t->entity()));
						
						if (!box)
							continue;*/
						/*
						if (ecs().entityName(t->entity()) == "ao")
						{
							if (Im3d::Gizmo("UnifiedGizmo", M))
							{
								const auto p(M.getTranslation());
								const auto r(M.getRotation());
								const auto s(M.getScale());
								
								t->local().position = mth::Vec3f{p.x, p.y, p.z};
								t->local().rotation = mth::Quatf{glm::make_mat3(static_cast<const float *>(r))};
								t->local().scale    = mth::Vec3f{s.x, s.y, s.z};
							}
						}
						*//*
						Im3d::PushDrawState();
						{
							mth::Transformf tcoll;
							tcoll.position = box->offset();
							tcoll.position -= box->origin();
							const mth::Mat4f model{t->global().matrix() * tcoll.matrix()};
							Im3d::Mat4 M;
							for (int i{0}; i < 16; ++ i) M[i] = *(&(model[0][0]) + i);
							
							//log::msg("DEBUG") << box->size().x << " " << box->size().y << " " << box->size().z;
							
							Im3d::PushMatrix(M);
							{
								mth::Vec3f
									min{box->size() * -0.5f},
									max{box->size() * 0.5f};
								
								Im3d::SetColor(1.0f, 0.0f, 0.0f, 0.1f);
								Im3d::DrawAlignedBoxFilled
								(
									{min.x, min.y, min.z},
									{max.x, max.y, max.z}
								);
								Im3d::SetColor(1.0f, 0.0f, 0.0f, 0.3f);
								Im3d::DrawAlignedBox
								(
									{min.x, min.y, min.z},
									{max.x, max.y, max.z}
								);
							}
							Im3d::PopMatrix();
						}
						Im3d::PopDrawState();
					}*/
					/*
					gfx().funcIm3DEndFrame()
					(
						w->width(),
						w->height(),
						VP
					);*/
				}
			/*
				{
					const ImVec2 ttsize
					{
						static_cast<float>(textext->width()),
						static_cast<float>(textext->height())
					};
					ImGui::Begin("Kirja", nullptr, ttsize);
						/// @todo: Maybe this means we should have a virtual texture method to return a handle for ImGui differently too...
						std::uint32_t texid{*reinterpret_cast<const std::uint32_t *>(textext->handle())};
						ImGui::Image(reinterpret_cast<void *>(texid), ttsize);//, {0, 1}, {1, 0});
					ImGui::End();
				}
		*/
					/*
				ImGui::End();
		
				ImGui::ShowTestWindow();*/
			#endif
/*
			ImGui::Begin("Hello, ImGui");
				if (ImGui::Button("play WAV"))
				{
					gSoloud.play(gWave); // Play the wave
				}
	
				if (ImGui::Button("play OGG"))
				{
					gSoloud.play(gOgg); // Play the wave
				}
			ImGui::End();
*/
			////// END TEMPORARY TEST STUFF //////
			
//			using namespace std::literals::chrono_literals;
//			std::this_thread::sleep_for(1ms);
		}
		
		Nullable<res::IDResource> App::loadStartscene()
		{
			constexpr char const *filename{"_setup.karhudat.json"};
			
			const auto contents(fio().readStringFromResource(filename));
			
			if (!contents)
			{
				log::err("Karhu") <<
					"Failed to open resource "
					"'_setup.karhudat.json' "
					"to read start scene ID";
				
				return {false};
			}
			
			std::stringstream stream;
			stream << *contents;
			
			conv::JSON::Parser parser;
			
			const auto data(parser.parse(stream));
			
			if (!data)
			{
				log::err("Karhu")
					<< "Failed to parse JSON in "
					   "'_setup.karhudat.json' "
					   "to read start scene ID: "
					<< parser.error();
				
				return {false};
			}
			
			const auto root(data->object());
			
			if (!root)
			{
				log::err("Karhu") <<
					"Failed to read JSON in "
					"'_setup.karhudat.json' "
					"to read start scene ID: "
					"root object missing";
				
				return {false};
			}
			
			readNamesOfLayersForBank(ecs().tags(), filename, {ecs().tags().label()}, root);
			
			const auto startscene(root->getInt("startscene"));
			
			if (!startscene)
			{
				log::err("Karhu") <<
					"Failed to read JSON in "
					"'_setup.karhudat.json'"
					" to read start scene ID: "
					"integer field 'startscene' missing";
				
				return {false};
			}
			
			auto const IDStartscene(static_cast<res::IDResource>(*startscene));
			
			auto ref(res().get<res::Scene>(IDStartscene));
			
			if (!ref)
			{
				log::err("Karhu")
					<< "Failed to load start scene: no scene resource with ID "
					<< static_cast<int>(IDStartscene);
				
				return {false};
			}

			const auto r(ecs().createScene());
			
			if (!r.success)
			{
				log::err("Karhu")
					<< "Failed to create start scene "
					<< static_cast<int>(IDStartscene)
					<< ": "
					<< r.error;
				
				return {false};
			}
			
			if (!res().serialiserECS().deserialiseSceneToWorld(*ref, ecs(), r.value))
			{
				log::err("Karhu")
					<< "Failed to deserialise start scene "
					<< static_cast<int>(IDStartscene);
				
				return {false};
			}
		
			#if KARHU_EDITOR_SERVER_SUPPORTED
			if (ModeApp::server == mode())
				m_editor->scene(IDStartscene, r.value);
			#endif
			
			return {true, r.value};
		}
		
		#ifdef KARHU_EDITOR
		void App::callbackMessageFromServer
		(
			App                                         &app,
			karhu::network::Protocol::TypeMessage const &type,
			conv::JSON::Val                       const &value
		)
		{
			using P = karhu::network::Protocol;
			using T = P::TypeMessage;
			using E = edt::EventProtocol;
			
			switch (type)
			{
				case T::debug:
				{
					if (!value.object())
						break;
					
					auto const
						e(value.object()->get("e"));
					
					if (!e || !e->integer())
						break;
					
					E const
						event{static_cast<E>(*e->integer())};
					
					switch (event)
					{
						case E::resourceRefreshed:
						{
							auto const
								IDi(value.object()->getInt("id"));
							
							auto const
								IDTypei(value.object()->getInt("type"));
							
							if (!IDi || !IDTypei)
								break;
							
							auto const
								ID(static_cast<res::IDResource>(*IDi));
							
							auto const
								IDType(static_cast<res::IDTypeResource>(*IDTypei));
							
							log::msg("Karhu")
								<< "Server requested refresh of resource "
								<< ID
								<< " ("
								<< c_instance->res().nameForType(IDType)
								<< ')';
							
							if (c_instance->res().IDOfType<res::Script>() == IDType)
								scriptutil::refreshScript(*c_instance, ID);
							else
								c_instance->res().destroy(ID);
							
							break;
						}
						
						case E::pipelineStepActivated:
						case E::pipelineScaleShadowmap:
						case E::pipelineMarginShadowmap:
						case E::pipelineCutoffShadowmap:
						case E::pipelineBiasShadowmap:
						{
							auto const
								indexi(value.object()->getInt("index"));
							
							auto const
								set(value.object()->get("set"));
							
							if (!indexi || !set)
								break;
							
							auto const
								index(static_cast<std::size_t>(*indexi));
							
							if (index >= app.gfx().pipeline().countSteps())
								break;
							
							char const *name;
							bool        valb;
							float       valf;
							
							if (E::pipelineStepActivated == event)
							{
								if (set->boolean())
									valb = *set->boolean();
								else
									break;
							}
							else if (set->floating())
								valf = *set->floating();
							else
								break;
							
							auto
								&step(app.gfx().pipeline().step(index)->step);
							
							if (E::pipelineStepActivated == event)
							{
								step.active = valb;
								name = "active state";
							}
							else if (E::pipelineScaleShadowmap == event)
							{
								step.scaleShadowmaps = valf;
								name = "shadowmap scale";
							}
							else if (E::pipelineMarginShadowmap == event)
							{
								step.marginShadowmaps = valf;
								name = "shadowmap margin";
							}
							else if (E::pipelineCutoffShadowmap == event)
							{
								step.cutoffShadowmaps = valf;
								name = "shadowmap cutoff";
							}
							else if (E::pipelineBiasShadowmap == event)
							{
								step.biasShadowmaps = valf;
								name = "shadowmap bias";
							}
							
							log::msg("Karhu")
								<< "Server requested pipeline step #"
								<< (index + 1)
								<< ' '
								<< name
								<< " be set to "
								<< ((E::pipelineStepActivated == event) ? valb : valf);
							
							break;
						}
						
						case E::pipelineSizeShadowmapMin:
						case E::pipelineSizeShadowmapMax:
						{
							auto const
								set(value.object()->getInt("set"));
							
							if (!set)
								break;
							
							log::msg("Karhu")
								<< "Server requested pipeline shadowmap "
								<< ((E::pipelineSizeShadowmapMin == event) ? "min" : "max")
								<< "size be set to "
								<< *set;
							
							gfx::Pipeline
								&pipeline(app.gfx().pipeline());
							
							auto
								&ref((E::pipelineSizeShadowmapMin == event) ? pipeline.sizeShadowmapMin : pipeline.sizeShadowmapMax);
							
							ref = static_cast<std::remove_reference_t<decltype(ref)>>(*set);
							
							break;
						}
						
						default:
							break;
					}
					
					break;
				}
				
				default:
					break;
			}
		}
		#endif

		/// @todo: Remove when done testing.
		bool App::initTemporaryStuff()
		{
			std::srand(static_cast<unsigned int>(std::time(nullptr)));
			
			#ifndef KARHU_PLATFORM_WEB
				startAudio();
			#endif
			
			// KIRJA TEXTURE!!
			gfx::Pixelbuffer buf;
			renderTextToBitmap(buf, res(), *m_backend);
			textext = gfx().createTexture();
			textext->setFromBuffer(buf);
			
			if (!textext->bake())
				log::err("DEBUG") << "Failed to bake Kirja texture";
				/*
			{log::msg("kirja") << "tex id = " << *reinterpret_cast<const std::uint32_t *>(textext->handle());}
			{log::msg("kirja") << "tex w = " << textext->width();}
			{log::msg("kirja") << "tex h = " << textext->height();}
*/
			return true;
		}
		
		void App::startAudio()
		{
			log::msg("DEBUG")<<"boutta init soloud";
			int slres = gSoloud.init(); // Initialize SoLoud
			log::msg("DEBUG")<<"slres = " << slres;
			if (SoLoud::SO_NO_ERROR != slres)
				log::err("DEBUG") << "Failed to initialise SoLoud: " << slres;
			else
			{
				{
					const auto content(fio().readBytesFromResource("gong.wav"));
					
					if (content)
					{
						slres = gWave.loadMem(reinterpret_cast<unsigned char *>(content->data.get()), content->size, false, false);
						
						if (SoLoud::SO_NO_ERROR != slres)
							log::err("DEBUG") << "Failed to load WAV with SoLoud: " << slres;
					}
				}
				{
					const auto content(fio().readBytesFromResource("coin.ogg"));
					
					if (content)
					{
						slres = gOgg.loadMem(reinterpret_cast<unsigned char *>(content->data.get()), content->size, false, false);
						
						if (SoLoud::SO_NO_ERROR != slres)
							log::err("debug") << "Failed to load OGG with SoLoud: " << slres;
					}
				}
			}
		}
		
		mth::Vec2u App::rendersize() const
		{
			const auto window(m_backend->windows().top());
			
			if (!window)
				return {};
			
			if (gfx::ModeRendersize::custom == m_backend->graphics()->modeRendersize())
				return m_backend->graphics()->rendersizeCustom();
			
			return
			{
				window->width(),
				window->height()
			};
		}
		
		mth::Vec2f App::renderscale() const
		{
			const auto window(m_backend->windows().top());
			
			if (!window)
				return {};
			
			if (gfx::ModeRendersize::custom == m_backend->graphics()->modeRendersize())
				return {1.0f, 1.0f};
			
			return
			{
				window->scaleWidth(),
				window->scaleHeight()
			};
		}
		
		App::~App()
		{
			gSoloud.deinit();
			
			if (this == c_instance)
				c_instance = nullptr;
		}

		App *App::c_instance{nullptr};
	}
}
