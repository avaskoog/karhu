/**
 * @author		Ava Skoog
 * @date		2017-11-16
 * @copyright   2017-2023 Ava Skoog
 */

// Include this instead of including ImGui headers directly, so that math operations are defines.
#ifndef KARHU_APP_IMGUI_H_
	#define KARHU_APP_IMGUI_H_

	#define IMGUI_DEFINE_MATH_OPERATORS

	#include <karhu/app/lib/imgui/imgui.h>
	#include <karhu/app/lib/imgui/imgui_internal.h>
	#include <karhu/app/lib/imgui/misc/cpp/imgui_stdlib.h>

	#include <type_traits>
	#include <cstdint>

	namespace karhu
	{
		namespace app
		{
			using FuncImGuiDevicesCreate  = IMGUI_API bool        (*)();
			using FuncImGuiDevicesDestroy = IMGUI_API void        (*)();
			using FuncImGuiFontsCreate    = IMGUI_API void        (*)();
			using FuncImGuiRender         = IMGUI_API void        (*)(ImDrawData *);
			using FuncImGuiInit           = IMGUI_API bool        (*)(void *window);
			using FuncImGuiFrame          = IMGUI_API void        (*)(void *window);
			using FuncImGuiEvent          = IMGUI_API bool        (*)(void *event);
			using FuncImGuiClipboardGet   = IMGUI_API char const *(*)(void *);
			using FuncImGuiClipboardSet   = IMGUI_API void        (*)(void *, char const *);
			using FuncImGuiDeinit         = IMGUI_API void        (*)();
			
			template<typename T>
			constexpr ImGuiDataType_ datatypeImGui() noexcept
			{
				static_assert(std::is_arithmetic<T>{}, "Cannot convert non-number to ImGui type");
				
				if (std::is_same<T, std::int8_t>{})
					return ImGuiDataType_S8;
				
				if (std::is_same<T, std::int16_t>{})
					return ImGuiDataType_S16;
				
				if (std::is_same<T, std::int32_t>{})
					return ImGuiDataType_S32;
				
				if (std::is_same<T, std::int64_t>{})
					return ImGuiDataType_S64;
				
				if (std::is_same<T, std::uint8_t>{})
					return ImGuiDataType_U8;
				
				if (std::is_same<T, std::uint16_t>{})
					return ImGuiDataType_U16;
				
				if (std::is_same<T, std::uint32_t>{})
					return ImGuiDataType_U32;
				
				if (std::is_same<T, std::uint64_t>{})
					return ImGuiDataType_U64;
				
				if (std::is_same<T, float>{})
					return ImGuiDataType_Float;
				
				if (std::is_same<T, float>{})
					return ImGuiDataType_Double;
				
				return ImGuiDataType_COUNT;
			}
		}
	}
#endif
