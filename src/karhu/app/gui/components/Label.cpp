/**
 * @author		Ava Skoog
 * @date		2019-05-20
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gui/components/Label.hpp>
#include <karhu/data/serialise.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
	#include <karhu/app/edt/Editor.hpp>
	#include <karhu/app/App.hpp>
	#include <karhu/res/Bank.hpp>
	#include <karhu/app/karhuimgui.hpp>
#endif

namespace karhu
{
	namespace gui
	{
		void Label::font(res::IDResource const font)
		{
			if (font != m_font)
			{
				m_font = std::move(font);
				m_modifications |= Modification::text;
			}
		}
		
		void Label::colour(gfx::ColRGBAf col)
		{
			if (col != m_colour)
			{
				m_colour = std::move(col);
				m_modifications |= Modification::text;
			}
		}
		
		void Label::text(std::string string)
		{
			if (string != m_text)
			{
				m_text = std::move(string);
				m_modifications |= Modification::text;
			}
		}
		
		void Label::localisation(loc::Localisation localisation)
		{
			// TODO: Should this be equality-checked too or too heavy with many args? Rerender probably heavier.
			m_localisation = std::move(localisation);
			m_modifications |= Modification::localisation;
		}
		
		void Label::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("font",   m_font)
				<< karhuINn("colour", m_colour)
				<< karhuINn("text",   m_text)
				<< karhuINn("loc",    m_localisation);
		}
		
		void Label::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUTn("font",   m_font)
				>> karhuOUTn("colour", m_colour)
				>> karhuOUTn("text",   m_text)
				>> karhuOUTn("loc",    m_localisation);
			
			#if KARHU_EDITOR_SERVER_SUPPORTED
			m_keyLoc = m_localisation.key();
			#endif
			
			if (!m_localisation.key().empty())
				m_modifications |= Modification::localisation;
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Label::editorEdit(app::App &app, edt::Editor &editor)
		{
			if
			(
				editor.pickerResource
				(
					"label-font",
					"Font",
					m_font,
					app.res().IDOfType<res::Font>()
				)
			)
				m_modifications |= Modification::text;
			
			ImGui::ColorEdit3
			(
				"Colour",
				&m_colour.r
			);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				m_modifications |= Modification::text;
			
			if (editor.input("label-text", "Text", m_text))
				m_modifications |= Modification::text;
			
			if (editor.input("label-loc", "Loc. key", m_keyLoc))
			{
				m_localisation.key(m_keyLoc);
				m_modifications |= Modification::localisation;
			}
			
			return (Modification::none != m_modifications);
		}
		#endif
	}
}
