/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gui/components/Layout.hpp>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace gui
	{
		void Layout::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(layouting)
				<< karhuIN(position)
				<< karhuIN(origin)
				<< karhuIN(size);
		}

		void Layout::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(layouting)
				>> karhuOUT(position)
				>> karhuOUT(origin)
				>> karhuOUT(size);
			
			positionCalculated = position;
			sizeCalculated     = size;
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Layout::editorEdit(app::App &, edt::Editor &)
		{
			return false;
		}
		#endif
	}
}
