/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GUI_BUTTON_H_
	#define KARHU_GUI_BUTTON_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/gui/components/Focussable.hpp>

	namespace karhu
	{
		namespace gui
		{
			class Button : public Focussable
			{
				public:
					using Focussable::Focussable;

				protected:
					void performSerialise(conv::Serialiser &) const override;
					void performDeserialise(const conv::Serialiser &) override;
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
					void performEditorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) override {}
					bool performEditorEdit(app::App &, edt::Editor &) override;
					#endif
			};
		}
	}
#endif
