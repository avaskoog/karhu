/**
 * @author		Ava Skoog
 * @date		2019-05-20
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GUI_LABEL_H_
	#define KARHU_GUI_LABEL_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/gui/common.hpp>
	#include <karhu/res/Bank.hpp>
	#include <karhu/res/Font.hpp>
	#include <karhu/res/Texture.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/app/loc/Localiser.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/conv/input.hpp>

	namespace karhu
	{
		namespace gui
		{
			/// @todo: Should require Transform and Inputtable and Layout.
			class Label : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
				public:
					void font(res::IDResource const);
					res::IDResource font() const noexcept { return m_font; }
				
					void colour(gfx::ColRGBAf);
					gfx::ColRGBAf const &colour() const noexcept { return m_colour; }
				
					void text(std::string);
					std::string const &text() const noexcept { return m_text; }
				
					void localisation(loc::Localisation);
					loc::Localisation const &localisation() const noexcept { return m_localisation; }
				
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
                    void editorEvent(app::App &, edt::Editor &, edt::Identifier const, edt::Event const &) override {}
                    bool editorEdit(app::App &, edt::Editor &) override;
                    #endif
				
				private:
					enum class Modification : std::uint8_t
					{
						none         = 0,
						text         = 1 << 0,
						localisation = 1 << 1,
					};
					
					karhuBITFLAGGIFY_NESTED(Modification)
				
				// Non-serialised for runtime only.
				private:
					Modification
						m_modifications{Modification::text};
				
					std::unique_ptr<res::Texture>
						m_texture;
				
				// Serialised.
				private:
					res::IDResource
						m_font{0};
				
					gfx::ColRGBAf
						m_colour;
				
					std::string
						m_text;
				
					loc::Localisation
						m_localisation;
				
				private:
					#if KARHU_EDITOR_SERVER_SUPPORTED
					std::string
						m_keyLoc;
					#endif
				
				friend class SystemLabel;
			};
		}
	}
#endif
