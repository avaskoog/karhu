/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GUI_LAYOUT_H_
	#define KARHU_GUI_LAYOUT_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/gui/common.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
		namespace gui
		{
			/// @todo: Require Transform and Renderdata2D.
			class Layout : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
                    void editorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) override {}
                    bool editorEdit(app::App &, edt::Editor &) override;
                    #endif
				
				public:
					Layouting layouting;
				
					mth::Vec2f
						position{0.0f, 0.0f},
						origin  {0.0f, 0.0f},
						size    {1.0f, 1.0f};
				
				public:
					mth::Vec2f
						positionCalculated{position},
						sizeCalculated    {size};
			};
		}
	}
#endif
