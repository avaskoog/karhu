/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gui/components/Screen.hpp>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace gui
	{
		void Screen::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(layer)
				<< karhuIN(layerOnActive)
				<< karhuIN(actionNavigate)
				<< karhuIN(actionConfirm)
				<< karhuIN(actionCancel)
				<< karhuIN(navigation)
				<< karhuIN(delayHoldMax)
				<< karhuIN(delayHoldMin)
				<< karhuIN(accelerationHold);
		}

		void Screen::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(layer)
				>> karhuOUT(layerOnActive)
				>> karhuOUT(actionNavigate)
				>> karhuOUT(actionConfirm)
				>> karhuOUT(actionCancel)
				>> karhuOUT(navigation)
				>> karhuOUT(delayHoldMax)
				>> karhuOUT(delayHoldMin)
				>> karhuOUT(accelerationHold);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Screen::editorEdit(app::App &, edt::Editor &)
		{
			return false;
		}
		#endif
	}
}
