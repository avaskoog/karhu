/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GUI_SCREEN_H_
	#define KARHU_GUI_SCREEN_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/gui/common.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/conv/input.hpp>

	namespace karhu
	{
		namespace gui
		{
			/// @todo: Should require Transform and Inputtable and Layout.
			class Screen : public ecs::Component
			{
				public:
					using Layer = std::uint32_t;
				
				public:
					using ecs::Component::Component;
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
                    void editorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) override {}
                    bool editorEdit(app::App &, edt::Editor &) override;
                    #endif
				
				public:
					Layer layer        {0};
					bool  layerOnActive{true};
				
					inp::Action
						actionNavigate{0},
						actionConfirm {0},
						actionCancel  {0};
				
					Navigation navigation{Navigation::hold};
				
					float
						delayHoldMax    {0.4f},
						delayHoldMin    {0.15f},
						accelerationHold{1.0f};
				
				public:
					std::size_t indexFocussed{0};
			};
		}
	}
#endif
