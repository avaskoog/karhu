/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gui/components/Container.hpp>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace gui
	{
		void Container::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(orientation)
				<< karhuIN(overflow)
				<< karhuIN(wrap)
				<< karhuIN(spacing)
				<< karhuIN(layouting);
		}

		void Container::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(orientation)
				>> karhuOUT(overflow)
				>> karhuOUT(wrap)
				>> karhuOUT(spacing)
				>> karhuOUT(layouting);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Container::editorEdit(app::App &, edt::Editor &)
		{
			return false;
		}
		#endif
	}
}
