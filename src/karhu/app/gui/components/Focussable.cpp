/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gui/components/Focussable.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
#include <karhu/app/edt/Editor.hpp>
#endif

namespace karhu
{
	namespace gui
	{
		void Focussable::serialise(conv::Serialiser &ser) const
		{
			performSerialise(ser);
		}

		void Focussable::deserialise(const conv::Serialiser &ser)
		{
			performDeserialise(ser);
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		void Focussable::editorEvent(app::App &app, edt::Editor &editor, edt::Identifier const caller, edt::Event const &e)
		{
			performEditorEvent(app, editor, caller, e);
		}
		
		bool Focussable::editorEdit(app::App &app, edt::Editor &editor)
		{
			return performEditorEdit(app, editor);
		}
		#endif
	}
}
