/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GUI_CONTAINER_H_
	#define KARHU_GUI_CONTAINER_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/gui/common.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
		namespace gui
		{
			/// @todo: Should require Transform and Layout.
			/// @todo: Paging, for example a container with album photos with a grid of photos per page?
			/// @todo: Scrollbar?
			class Container : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
                    void editorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) override {}
                    bool editorEdit(app::App &, edt::Editor &) override;
                    #endif
				
				public:
					Orientation    orientation{Orientation::vertical};
					Overflow       overflow   {Overflow::none};
					Wrap           wrap       {Wrap::none};
					mth::Scalf spacing    {0.0f};
					Layouting      layouting;
			};
		}
	}
#endif
