/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GUI_FOCUSSABLE_H_
	#define KARHU_GUI_FOCUSSABLE_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/conv/serialise.hpp>

	namespace karhu
	{
		namespace gui
		{
			/// @todo: Should require Transform and Layout.
			class Focussable : public ecs::Component
			{
				public:
					using ecs::Component::Component;
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
					void editorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) override;
					bool editorEdit(app::App &, edt::Editor &) override;
					#endif
				
				protected:
					virtual void performSerialise(conv::Serialiser &) const = 0;
					virtual void performDeserialise(const conv::Serialiser &) = 0;
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
					virtual void performEditorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &e) = 0;
					virtual bool performEditorEdit(app::App &, edt::Editor &) = 0;
					#endif
			};
		}
	}
#endif
