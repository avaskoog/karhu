/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gui/components/Button.hpp>

namespace karhu
{
	namespace gui
	{
		void Button::performSerialise(conv::Serialiser &ser) const
		{
		}

		void Button::performDeserialise(const conv::Serialiser &ser)
		{
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Button::performEditorEdit(app::App &, edt::Editor &)
		{
			return true;
		}
		#endif
	}
}
