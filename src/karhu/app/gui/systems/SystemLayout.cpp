/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gui/systems/SystemLayout.hpp>
#include <karhu/app/gui/components/Container.hpp>
#include <karhu/app/gui/components/Label.hpp>
#include <karhu/app/gfx/components/Transform.hpp>
#include <karhu/app/gfx/components/Renderdata2D.hpp>
#include <karhu/app/App.hpp>

namespace
{
	using namespace karhu;
	using namespace karhu::gui;
	
	inline void calculateRefres
	(
		      mth::Scalf &r,
		const mth::Vec2f &resolution,
		const Mode        mode
	)
	{
		constexpr mth::Scalf refsize{1000.0f};
		const     mth::Vec2f refres {resolution / refsize};
		
		if (Mode::referenceHorizontal == mode)
			r = refres.x;
		else if (Mode::referenceVertical == mode)
			r = refres.y;
	}
	
	inline void calculateUnstretched
	(
			  mth::Scalf &child,
		const mth::Scalf &parent,
		const mth::Scalf &reference,
		const mth::Scalf &relative,
		const Mode        mode
	)
	{
		switch (mode)
		{
			case Mode::absolute:
				break;
			
			case Mode::referenceHorizontal:
			case Mode::referenceVertical:
				child *= reference;
				break;
			
			case Mode::relative:
				child *= relative;
				break;
			
			case Mode::fraction:
				child *= parent;
				break;
		}
	}
	
	inline void calculateStretched
	(
			  mth::Scalf &child,
		const mth::Scalf &parent
	)
	{
		/// @todo: Calculate based on settings for padding, margin and so on.
		child = parent;
	}
	
	inline void calculateConverted
	(
		      mth::Scalf &v,
		const mth::Scalf &parent,
		const mth::Scalf &reference,
		const mth::Scalf &relative,
		const Mode        mode
	)
	{
		// Undo previous changes.
		switch (mode)
		{
			case Mode::absolute:
				break;
			
			case Mode::referenceHorizontal:
			case Mode::referenceVertical:
				v /= reference;
				break;
			
			case Mode::relative:
				v /= relative;
				break;
			
			case Mode::fraction:
				v /= parent;
				break;
		}
	}
	
	inline mth::Vec2f sizeForLayout
	(
		const Layout     &c,
		const mth::Vec2f &parentsize,
		const mth::Vec2f &resolution
	)
	{
		mth::Vec2f refres;
		
		calculateRefres(refres.x, resolution, c.layouting.modeSizeHorizontal);
		calculateRefres(refres.y, resolution, c.layouting.modeSizeVertical);
		
		mth::Vec2f r{c.size};
		
		switch (c.layouting.stretch)
		{
			case Stretch::none:
				calculateUnstretched(r.x, parentsize.x, refres.x, resolution.x, c.layouting.modeSizeHorizontal);
				calculateUnstretched(r.y, parentsize.y, refres.y, resolution.y, c.layouting.modeSizeVertical);
				break;
			
			case Stretch::horizontal:
				calculateStretched(r.x, parentsize.x);
				calculateUnstretched(r.y, parentsize.y, refres.y, resolution.y, c.layouting.modeSizeVertical);
				break;
			
			case Stretch::vertical:
				calculateUnstretched(r.x, parentsize.x, refres.x, resolution.x, c.layouting.modeSizeHorizontal);
				calculateStretched(r.y, parentsize.y);
				break;
			
			case Stretch::both:
				calculateStretched(r.x, parentsize.x);
				calculateStretched(r.y, parentsize.y);
				break;
		}
		
		return r;
	}

	inline mth::Vec3f positionForLayout
	(
		const Layout     &c,
		const mth::Vec2f &childsize,
		const mth::Vec2f &parentsize,
		const mth::Vec2f &resolution
	)
	{
		mth::Vec2f refres;
		
		calculateRefres(refres.x, resolution, c.layouting.modePositionHorizontal);
		calculateRefres(refres.y, resolution, c.layouting.modePositionVertical);
		
		mth::Vec3f r{c.position, 0.0f};
		
		calculateUnstretched(r.x, parentsize.x, refres.x, resolution.x, c.layouting.modePositionHorizontal);
		calculateUnstretched(r.y, parentsize.y, refres.y, resolution.y, c.layouting.modePositionVertical);
		
		/// @todo: Where to take origin into account?
		
		auto xRight ([&childsize, &parentsize, &r]{ r.x = parentsize.x        - r.x - childsize.x; });
		auto xCentre([&childsize, &parentsize, &r]{ r.x = parentsize.x * 0.5f + r.x - childsize.x * 0.5f; });
		auto yTop   ([&childsize, &parentsize, &r]{ r.y = parentsize.y        - r.y - childsize.y; });
		auto yCentre([&childsize, &parentsize, &r]{ r.y = parentsize.y * 0.5f - r.y - childsize.y * 0.5f; });
		
		switch (c.layouting.anchor)
		{
			case Anchor::topLeft:                 yTop();    break;
			case Anchor::centreLeft:              yCentre(); break;
			case Anchor::bottomLeft:                         break;
			case Anchor::topRight:     xRight();  yTop();    break;
			case Anchor::centreRight:  xRight();  yCentre(); break;
			case Anchor::bottomRight:  xRight();             break;
			case Anchor::topCentre:    xCentre(); yTop();    break;
			case Anchor::centreCentre: xCentre(); yCentre(); break;
			case Anchor::bottomCentre: xCentre();            break;
		}
		
		r.x -= childsize.x * c.origin.x;
		r.y -= childsize.y * c.origin.y;
		
		return r;
	}
}

namespace karhu
{
	namespace gui
	{
		void SystemLayout::performUpdateComponents
		(
			      app::App          &app,
			      ecs::Pool<Layout> &components,
			const float,
			const float,
			const float
		)
		{
			mth::Vec2u const
				rendersizeu{app.rendersize()};
			
			mth::Vec2f const
				rendersizef
				{
					static_cast<mth::Scalf>(rendersizeu.x),
					static_cast<mth::Scalf>(rendersizeu.y)
				},
				resolution
				{
					rendersizef *
					app.renderscale()
				};
			
			/// @todo: We might have to order everything by parent-child order or run an extra buffer frame to make sure everything always looks right.
			
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);
				
				if (!c->activeInHierarchy())
					continue;
				
				auto rd   (app.ecs().componentInEntity<gfx::Renderdata2D>(c->entity()));
				auto label(app.ecs().componentInEntity<Label>(c->entity()));
				
				mth::Vec2f parentsize{resolution}, refres, padding;
				
				Layout    *parent   {nullptr};
				Container *container{nullptr};
				
				// Find the first immediate parent with a layout.
				app.ecs().forEachParentOfEntity
				(
					c->entity(),
					[&app, &parent, &container](const ecs::IDEntity &ID)
					{
						// Container must be immediate parent.
						if (!parent)
							if (auto c = app.ecs().componentInEntity<Container>(ID))
								container = c;
						
						if (auto c = app.ecs().componentInEntity<Layout>(ID))
						{
							parent = c;
							return false;
						}
						
						return true;
					}
				);
				
				if (parent)
					parentsize = parent->sizeCalculated;
				
				if (container)
					c->layouting = container->layouting;
				
				padding.x = c->layouting.spacingLeft + c->layouting.spacingRight;
				padding.y = c->layouting.spacingTop  + c->layouting.spacingBottom;
				
				if (container)
				{
					calculateRefres(refres.x, resolution, container->layouting.modePositionHorizontal);
					calculateRefres(refres.y, resolution, container->layouting.modePositionVertical);
					
					auto layout(app.ecs().componentInEntity<Layout>(container->entity()));
			
					calculateUnstretched
					(
						padding.x,
						layout->sizeCalculated.x,
						refres.x,
						resolution.x,
						container->layouting.modePositionHorizontal
					);
					
					calculateUnstretched
					(
						padding.y,
						layout->sizeCalculated.y,
						refres.y,
						resolution.y,
						container->layouting.modePositionVertical
					);
				}
				
				c->sizeCalculated = sizeForLayout
				(
					*c,
					parentsize,
					resolution
				);
				
				if (rd)
					rd->size(c->sizeCalculated);
				
				if (!label)
				{
					if
					(
						Stretch::horizontal == c->layouting.stretch ||
						Stretch::both       == c->layouting.stretch
					)
					{
						c->sizeCalculated.x -= padding.x;
						
						if (rd)
						{
							auto size(rd->size());
							size.x -= padding.x;
							rd->size(size);
						}
					}
					
					if
					(
						Stretch::vertical == c->layouting.stretch ||
						Stretch::both     == c->layouting.stretch
					)
					{
						c->sizeCalculated.y -= padding.y;
						
						if (rd)
						{
							auto size(rd->size());
							size.y -= padding.y;
							rd->size(size);
						}
					}
				}
				
				mth::Vec2f position{};
				
				switch (c->layouting.anchor)
				{
					case Anchor::topLeft:
					case Anchor::centreLeft:
					case Anchor::bottomLeft:
						position.x += c->layouting.spacingLeft;
						break;
					
					case Anchor::topCentre:
					case Anchor::centreCentre:
					case Anchor::bottomCentre:
						position.x += c->layouting.spacingLeft - c->layouting.spacingRight;
						break;
						
					case Anchor::topRight:
					case Anchor::centreRight:
					case Anchor::bottomRight:
						position.x += c->layouting.spacingRight;
						break;
				}
				
				switch (c->layouting.anchor)
				{
					case Anchor::topLeft:
					case Anchor::topRight:
					case Anchor::topCentre:
						position.y += c->layouting.spacingTop;
						break;
						
					case Anchor::centreLeft:
					case Anchor::centreCentre:
					case Anchor::centreRight:
						position.y += c->layouting.spacingTop - c->layouting.spacingBottom;
						break;
					
					case Anchor::bottomLeft:
					case Anchor::bottomCentre:
					case Anchor::bottomRight:
						position.y += c->layouting.spacingBottom;
						break;
				}
				
				if (container)
				{
					c->position = position;
					
					mth::Vec2f sizeAll{}, sizeConverted{};
					bool foundSelf{false};
					
					// Find out which immediate child this entity is.
					app.ecs().forEachChildOfEntity
					(
						container->entity(),
						[
							&app,
							 self        = c->entity(),
							&foundSelf,
							&position    = c->position,
							&container,
							&parentsize,
							&resolution,
							&refres,
							&sizeAll,
							&sizeConverted
						]
						(
							const ecs::IDEntity &ID
						)
						{
							auto c(app.ecs().componentInEntity<Layout>(ID));
							
							if (!c)
								return true;
							
							if (ID == self)
								foundSelf = true;
							
							switch (container->orientation)
							{
								case Orientation::horizontal:
								{
									mth::Scalf size{c->sizeCalculated.x};
									
									calculateConverted
									(
										size,
										parentsize.x,
										refres.x,
										resolution.x,
										container->layouting.modePositionHorizontal
									);
									
									sizeAll.x += size;
									
									if (ID != self)
									{
										sizeAll.x += container->spacing;
										
										if (!foundSelf)
											position.x += size + container->spacing;
									}
									else
										sizeConverted.x = size;
									
									break;
								}
									
								case Orientation::vertical:
								{
									mth::Scalf size{c->sizeCalculated.y};
									
									calculateConverted
									(
										size,
										parentsize.y,
										refres.y,
										resolution.y,
										container->layouting.modePositionVertical
									);
									
									sizeAll.y += size;
									
									if (ID != self)
									{
										sizeAll.y += container->spacing;
										
										if (!foundSelf)
											position.y += size + container->spacing;
									}
									else
										sizeConverted.y = size;
								
									break;
								}
								
								case Orientation::grid:
									/// @todo: Implement grid.
									break;
							}
							
							return true;
						},
						false,
						false
					);

					if
					(
						Orientation::horizontal == container->orientation &&
						(
							Anchor::topCentre    == container->layouting.anchor ||
							Anchor::centreCentre == container->layouting.anchor ||
							Anchor::bottomCentre == container->layouting.anchor
						)
					)
						c->position.x -= sizeAll.x * 0.5f - sizeConverted.x * 0.5f;
				
					if
					(
						Orientation::vertical == container->orientation &&
						(
							Anchor::centreLeft   == container->layouting.anchor ||
							Anchor::centreCentre == container->layouting.anchor ||
							Anchor::centreRight  == container->layouting.anchor
						)
					)
						c->position.y -= sizeAll.y * 0.5f - sizeConverted.y * 0.5f;
					
					/// @todo: Deal with grid.
				}
				else
					c->position += position;
				
				auto tr(app.ecs().componentInEntity<gfx::Transform>(c->entity()));
				mth::Transformf t{tr->local()};

				mth::Vec3f p{positionForLayout
				(
					*c,
					c->sizeCalculated,
					parentsize,
					resolution
				)};
				
				t.position = p;
				
				/// @todo: Can we do this in a nicer way without recalcualting the whole thing again?
				if (!container)
				{
					c->position -= position;
				
					p = positionForLayout
					(
						*c,
						c->sizeCalculated,
						parentsize,
						resolution
					);
				}

				c->positionCalculated.x = p.x;
				c->positionCalculated.y = p.y;

				tr->local(std::move(t));
			}
		}
	}
}
