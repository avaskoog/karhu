/**
 * @author		Ava Skoog
 * @date		2019-05-20
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gui/systems/SystemLabel.hpp>
#include <karhu/app/gui/components/Layout.hpp>
#include <karhu/app/gfx/components/Renderdata2D.hpp>
#include <karhu/app/gfx/resources/Font.hpp>
#include <karhu/app/loc/events/ELocalisation.hpp>

namespace karhu
{
	namespace gui
	{
		void SystemLabel::listenerLocalisation
		(
			      app::App           &app,
			const loc::ELocalisation &e,
			const float               dt,
			const float               dtFixed,
			const float               timestep,
			      void               *data
		)
		{
			auto s(reinterpret_cast<SystemLabel *>(data));
			
			log::msg("Karhu") << "Localisations changed; updating labels.";
			
			auto const
				labels(app.ecs().componentsInActiveScenes<Label>());
			
			for (Label *const label : labels)
				label->m_modifications |= Label::Modification::localisation;
		}
		
		void SystemLabel::performUpdateComponents
		(
				  app::App         &app,
				  ecs::Pool<Label> &components,
			const float            dt,
			const float            dtFixed,
			const float            timestep
		)
		{
			enum Textureaction : std::uint8_t
			{
				none,
				clear,
				refresh
			};
			
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto
					c(components.data + i);
				
				if (!c->activeInHierarchy())
					continue;
				
				Textureaction
					action{none};
				
				if (Label::Modification::none != c->m_modifications)
				{
					if (static_cast<bool>(c->m_modifications & Label::Modification::localisation))
						c->m_text = app.loc().get(c->m_localisation);
					
					res::Font
						*const font{app.res().get<res::Font>(c->font())};
					
					if (!font || 0 == c->text().length())
						action = clear;
					else
					{
						gfx::Textrendersettings
							s;
						
						s.font   = font;
						s.colour = c->colour();
						
						auto const
							bm(gfx::renderTextToBitmap(c->text().c_str(), s));
						
						if (!bm)
							action = clear;
						else
						{
							if (!c->m_texture)
								c->m_texture = app.gfx().createTexture
								(
									res::Texture::Type::colour,
									gfx::Pixelbuffer::Format::RGBA
								);
							
							if (!c->m_texture)
								action = clear;
							else
							{
								c->m_texture->filter(res::Texture::ModeFilter::linear);
								c->m_texture->setFromBuffer(*bm); /// @todo: Perhaps label texture filter mode should be user configurable.
								
								if (!c->m_texture->bake())
									action = clear;
								else
									action = refresh;
							}
						}
					}
				
					c->m_modifications = Label::Modification::none;
				}
				
				auto
					rd(app.ecs().componentInEntity<gfx::Renderdata2D>(c->entity()));
				
				if (!rd)
					continue;
				
				/// @todo: Use constant instead of hardcoded strings for uniform.
				switch (action)
				{
					case none:
						break;
						
					case clear:
						rd->inputs().clearInput("_texMain");
						break;
						
					case refresh:
						rd->inputs().inputTexture("_texMain", *c->m_texture);
						
						rd->size
						({
							static_cast<mth::Scalf>(c->m_texture->width()),
							static_cast<mth::Scalf>(c->m_texture->height())
						});
						
						if (auto layout = app.ecs().componentInEntity<Layout>(c->entity()))
							layout->size = rd->size();
						
						break;
				}
			}
		}
	}
}
