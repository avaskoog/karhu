/**
 * @author		Ava Skoog
 * @date		2019-05-20
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GUI_SYSTEM_LABEL_H_
	#define KARHU_GUI_SYSTEM_LABEL_H_

	#include <karhu/app/ecs/System.hpp>
	#include <karhu/app/gui/components/Label.hpp>

	namespace karhu
	{
		namespace loc
		{
			class ELocalisation;
		}
		
		namespace gui
		{
			class SystemLabel : public ecs::System<Label>
			{
				public:
					static void listenerLocalisation(app::App &, const loc::ELocalisation &, const float, const float, const float, void *);
				
				protected:
					void performUpdateComponents
					(
						      app::App         &app,
							  ecs::Pool<Label> &components,
						const float            dt,
						const float            dtFixed,
						const float            timestep
					) override;
			};
		}
	}
#endif
