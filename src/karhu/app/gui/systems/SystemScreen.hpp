/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GUI_SYSTEM_SCREEN_H_
	#define KARHU_GUI_SYSTEM_SCREEN_H_

	#include <karhu/app/ecs/System.hpp>
	#include <karhu/app/gui/components/Screen.hpp>
	#include <karhu/app/gui/components/Container.hpp>
	#include <karhu/app/gui/components/Focussable.hpp>
	#include <karhu/app/inp/events/EInput.hpp>

	namespace karhu
	{
		namespace gui
		{
			class SystemScreen : public ecs::System<Screen>
			{
				private:
					enum class Direction : std::uint8_t
					{
						none,
						left,
						right,
						up,
						down
					};
				
					enum class Navresult : std::uint8_t
					{
						none,
						backward,
						backwardOpposite,
						inside,
						forward,
						forwardOpposite
					};
				
					struct Navinfo
					{
						Navresult   navigation;
						std::size_t indexNew;
					};
				
					struct ContainerWithFocussables
					{
						Container                 *container;
						std::vector<Focussable *>  focussables;
					};
				
					struct ContainersWithFocussables
					{
						std::vector<ContainerWithFocussables> containers;
						
						std::size_t
							countFocussables,
							indexContainer,
							subindexCurrent;
					};
			
				public:
					static void listenerInput(app::App &, const inp::EInput &, const float, const float, const float, void *);
				
				protected:
					void performUpdateComponents
					(
						      app::App          &app,
							  ecs::Pool<Screen> &components,
						const float             dt,
						const float             dtFixed,
						const float             timestep
					) override;
				
				public:
					void navigate(ecs::World &, Screen &, const mth::Vec2f &direction, const float, const float, const float);
					void confirm(ecs::World &, Screen &, const float, const float, const float);
					void cancel(ecs::World &, Screen &, const float, const float, const float);
				
				private:
					ContainersWithFocussables findContainersWithFocussablesInScreenForCurrentIndex
					(
						ecs::World &,
						Screen     &
					) const;
				
					Focussable *findFocussableInEntity
					(
						      ecs::World    &,
						const ecs::IDEntity &
					) const;
				
					Navinfo navinfoForDirectionInContainer
					(
						const Direction   &,
						const Container   &,
						const std::size_t &indexCurrent,
						const std::size_t &max
					) const;
				
				private:
					bool      m_navigated {false};
					Direction m_navdirLast{Direction::none};
				
					float
						m_timerNavigate  {0.0f},
						m_timerAccelerate{0.0f};
			};
		}
	}
#endif
