/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GUI_SYSTEM_LAYOUT_H_
	#define KARHU_GUI_SYSTEM_LAYOUT_H_

	#include <karhu/app/ecs/System.hpp>
	#include <karhu/app/gui/components/Layout.hpp>

	namespace karhu
	{
		namespace gui
		{
			class SystemLayout : public ecs::System<Layout>
			{
				protected:
					void performUpdateComponents
					(
						      app::App          &app,
							  ecs::Pool<Layout> &components,
						const float             dt,
						const float             dtFixed,
						const float             timestep
					) override;
			};
		}
	}
#endif
