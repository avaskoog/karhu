/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gui/systems/SystemScreen.hpp>
#include <karhu/app/gui/components/Button.hpp>
#include <karhu/app/gui/events/EGUI.hpp>
#include <karhu/app/inp/components/Inputtable.hpp>
#include <karhu/app/inp/SubsystemInput.hpp>
#include <karhu/app/App.hpp>
#include <karhu/core/Bitpair.hpp>

namespace karhu
{
	namespace gui
	{
		void SystemScreen::listenerInput
		(
			      app::App    &app,
			const inp::EInput &e,
			const float        dt,
			const float        dtFixed,
			const float        timestep,
			      void        *data
		)
		{
			auto c(app.ecs().componentInEntity<Screen>(e.entity()));
			
			// Respond only to input events for screens.
			if (!c)
				return;
			
			auto s(reinterpret_cast<SystemScreen *>(data));
			
			if (e.action == c->actionNavigate)
				/// @todo: Tabbing instead of directional navigation?
				/// @todo: Error if event is not a direction?
				s->navigate(app.ecs(), *c, e.direction, dt, dtFixed, timestep);
 			else if (e.action == c->actionConfirm)
				s->confirm(app.ecs(), *c, dt, dtFixed, timestep);
 			else if (e.action == c->actionCancel)
 				s->cancel(app.ecs(), *c, dt, dtFixed, timestep);
			/// @todo: Deal with touch/click; presumably navigate doesn't exist and confirm is just a direct tap/click; what to do about cancel? Will probably need to provide extra interface button.
			/// @todo: Deal with custom actions.
		}
		
		void SystemScreen::performUpdateComponents
		(
			      app::App          &app,
			      ecs::Pool<Screen> &components,
			const float              dt,
			const float,
			const float
		)
		{
			m_timerNavigate   += dt;
			m_timerAccelerate += dt;

			/// @todo: We probably don't want to recreate this sort of thing every frame.
			std::map<Screen::Layer, Screen *> screens;
			
			// First just collect the screens for ordering.
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);

				if (!c->activeInHierarchy())
					continue;
				
				screens.emplace(c->layer, c);
			}
			
			// Now set up the layers.
			
			static inp::Layer layerCurr{inp::Defaultlayers::custom};
			
			for (inp::Layer layer{inp::Defaultlayers::GUI}; layer <= layerCurr; ++ layer)
				app.inp().layerEnabled(layer, false);
			
			layerCurr = inp::Defaultlayers::custom;
			
			for (const auto &screen : screens)
			{
				if (screen.second->layerOnActive)
				{
					if (inp::Defaultlayers::custom == layerCurr)
						layerCurr = inp::Defaultlayers::GUI;
					else
						++ layerCurr;
				}
				
				auto inputtable(app.ecs().componentInEntity<inp::Inputtable>
				(
					screen.second->entity()
				));
				
				inputtable->layer = layerCurr;
			}
			
			if (inp::Defaultlayers::custom != layerCurr)
			{
				app.inp().layerEnabled(layerCurr, true);
				app.inp().layerSolo(layerCurr);
			}
			else
				app.inp().layerSolo(inp::Defaultlayers::none);
			
			// Reset things for next frame.
			
			if (!m_navigated)
			{
				m_navdirLast      = Direction::none;
				m_timerAccelerate =
				m_timerNavigate   = 0.0f;
			}
			
			m_navigated = false;
		}
		
		/// @todo: Skip over inactive GUI buttons etc when navigating a screen.
		void SystemScreen::navigate
		(
			      ecs::World &world,
			      Screen     &c,
			const mth::Vec2f &direction,
			const float       dt,
			const float       dtFixed,
			const float       timestep
		)
		{
			m_navigated = true;
			
			if (0.0f == mth::length(direction))
				return;
			
			Direction d;
			
			if (mth::abs(direction.x) > mth::abs(direction.y))
				d = (direction.x < 0.0f) ? Direction::left : Direction::right;
			else
				d = (direction.y < 0.0f) ? Direction::up : Direction::down;
			
			if
			(
				m_navdirLast == d &&
				(
					Navigation::release == c.navigation ||
					m_timerNavigate < mth::lerp
					(
						c.delayHoldMin,
						c.delayHoldMax,
						1.0f - mth::clamp
						(
							m_timerAccelerate   /
							c.accelerationHold,
							0.0f,
							1.0f
						)
					)
				)
			)
				return;
			
			m_navdirLast    = d;
			m_timerNavigate = 0.0f;
			
			std::size_t indexFocussedOld{c.indexFocussed};

			auto cf(findContainersWithFocussablesInScreenForCurrentIndex(world, c));
			
			// If there is only one child, or none, no navigation can be done.
			if (1 >= cf.countFocussables)
				return;
			
			Focussable *focusOld{nullptr};
			
			if (cf.countFocussables > 0)
			{
				auto &info(cf.containers[cf.indexContainer]);
				focusOld = info.focussables[cf.subindexCurrent];
			}
			
			// Finally figure out what navigation is possible.
			const auto nav(navinfoForDirectionInContainer
			(
				d,
				*cf.containers[cf.indexContainer].container,
				cf.subindexCurrent,
				cf.containers[cf.indexContainer].focussables.size() - 1
			));
			
			// Then carry it out.
			/// @todo: Implement navigation out of a container.
			switch (nav.navigation)
			{
				case Navresult::none:
					break;
					
				case Navresult::backward:
					break;
					
				case Navresult::backwardOpposite:
					break;
					
				case Navresult::inside:
				{
					c.indexFocussed = 0;
					
					for (std::size_t i{0}; i < cf.indexContainer; ++ i)
						c.indexFocussed += cf.containers[i].focussables.size();
					
					c.indexFocussed += nav.indexNew;
					
					break;
				}
					
				case Navresult::forward:
					break;
					
				case Navresult::forwardOpposite:
					break;
			}
			
			if (indexFocussedOld != c.indexFocussed)
			{
				cf = findContainersWithFocussablesInScreenForCurrentIndex(world, c);
				
				Container  *container{nullptr};
				Focussable *focus    {nullptr};
				
				if (cf.countFocussables > 0)
				{
					auto &info(cf.containers[cf.indexContainer]);
					
					container = info.container;
					focus     = info.focussables[cf.subindexCurrent];
				}
			
				world.emitEvent<EGUI>
				(
					dt,
					dtFixed,
					timestep,
					EGUI::Type::focus,
					c,
					container,
					focus,
					focusOld
				);
			}
		}
		
		void SystemScreen::confirm
		(
			       ecs::World &world,
			       Screen     &c,
			const float       dt,
			const float       dtFixed,
			const float       timestep
		)
		{
			auto cf(findContainersWithFocussablesInScreenForCurrentIndex(world, c));
			
			Container  *container{nullptr};
			Focussable *focus    {nullptr};
			
			if (cf.countFocussables > 0)
			{
				auto &info(cf.containers[cf.indexContainer]);
				
				container = info.container;
				focus     = info.focussables[cf.subindexCurrent];
			}
			
			/// @todo: Can remove this after making it impossible to select inactive buttons etc in the first place.
			if (!world.entityActiveInHierarchy(focus->entity()))
				return;
				
			world.emitEvent<EGUI>
			(
				dt,
				dtFixed,
				timestep,
				EGUI::Type::confirm,
				c,
				container,
				focus,
				nullptr
			);
		}
		
		void SystemScreen::cancel
		(
				   ecs::World &world,
				   Screen     &c,
			const float       dt,
			const float       dtFixed,
			const float       timestep
		)
		{
			auto cf(findContainersWithFocussablesInScreenForCurrentIndex(world, c));
			
			Container  *container{nullptr};
			Focussable *focus    {nullptr};
			
			if (cf.countFocussables > 0)
			{
				auto &info(cf.containers[cf.indexContainer]);
				
				container = info.container;
				focus     = info.focussables[cf.subindexCurrent];
			}
			
			/// @todo: Can remove this after making it impossible to select inactive buttons etc in the first place.
			if (!world.entityActiveInHierarchy(focus->entity()))
				return;
			
			world.emitEvent<EGUI>
			(
				dt,
				dtFixed,
				timestep,
				EGUI::Type::cancel,
				c,
				container,
				focus,
				nullptr
			);
		}
		
		SystemScreen::ContainersWithFocussables
		SystemScreen::findContainersWithFocussablesInScreenForCurrentIndex
		(
			ecs::World &world,
			Screen     &c
		) const
		{
			ContainersWithFocussables r;
			
			r.indexContainer   =
			r.subindexCurrent  =
			r.countFocussables = 0;
			
			world.forEachChildOfEntity
			(
				c.entity(),
				[this, &world, &r](const ecs::IDEntity &ID)
				{
					if (auto c = world.componentInEntity<Container>(ID))
					{
						ContainerWithFocussables cf;
						cf.container = c;
						
						world.forEachChildOfEntity
						(
							ID,
							[this, &world, &r, &cf](const ecs::IDEntity &ID)
							{
								// We are only interested in focussable children.
								if (auto c = findFocussableInEntity(world, ID))
								{
									cf.focussables.emplace_back(c);
									++ r.countFocussables;
								}
								
								return true;
							}
						);
						
						if (0 != cf.focussables.size())
							r.containers.emplace_back(std::move(cf));
					}
					
					return true;
				}
			);

			if (1 >= r.countFocussables)
				return r;
	
			// Validate the current index.
			c.indexFocussed = mth::clamp<std::size_t>
			(
				c.indexFocussed,
				0,
				r.countFocussables - 1
			);
			
			// Figure out which container contains the current focus.
			
			r.subindexCurrent = c.indexFocussed;
			
			for
			(
				;
				r.indexContainer < r.containers.size();
				++ r.indexContainer,
				r.subindexCurrent -= r.containers[r.indexContainer].focussables.size()
			)
			{
				if (c.indexFocussed < r.containers[r.indexContainer].focussables.size())
					break;
			}
			
			return r;
		}
		
		Focussable *SystemScreen::findFocussableInEntity
		(
				  ecs::World    &world,
			const ecs::IDEntity &ID
		) const
		{
			if (auto c = world.componentInEntity<Button>(ID))
				return c;
			
			return nullptr;
		}
		
		SystemScreen::Navinfo SystemScreen::navinfoForDirectionInContainer
		(
			const Direction   &d,
			const Container   &c,
			const std::size_t &indexCurrent,
			const std::size_t &max
		) const
		{
			Navinfo r;
			
			r.navigation = Navresult::none;
			r.indexNew   = indexCurrent;
			
			enum Movement : std::uint8_t
			{
				none,
				backward,
				forward
			} movement{none};
			
			switch (c.orientation)
			{
				case Orientation::horizontal:
				{
					switch (d)
					{
						case Direction::up:
							r.navigation = Navresult::backwardOpposite;
							break;
							
						case Direction::down:
							r.navigation = Navresult::forwardOpposite;
							break;
							
						case Direction::left:
							movement = backward;
							break;
							
						case Direction::right:
							movement = forward;
							break;
						
						case Direction::none:
							break;
					}
					
					break;
				}
				
				case Orientation::vertical:
				{
					switch (d)
					{
						case Direction::left:
							r.navigation = Navresult::backwardOpposite;
							break;
							
						case Direction::right:
							r.navigation = Navresult::forwardOpposite;
							break;
							
						case Direction::up:
							movement = backward;
							break;
							
						case Direction::down:
							movement = forward;
							break;
						
						case Direction::none:
							break;
					}
					
					break;
				}
				
				case Orientation::grid:
				{
					switch (d)
					{
						case Direction::left:
						case Direction::right:
						case Direction::up:
						case Direction::down:
						case Direction::none:
							// @todo: Implement grids.
							break;
					}
					
					break;
				}
			}
			
			bool wrap{false};
			
			switch (movement)
			{
				case none:
					break;
				
				case backward:
				{
					r.navigation = Navresult::inside;
				
					if (0 == indexCurrent)
						wrap = true;
					else
						-- r.indexNew;
					
					break;
				}
				
				case forward:
				{
					r.navigation = Navresult::inside;
				
					if (max == indexCurrent)
						wrap = true;
					else
						++ r.indexNew;
					
					break;
				}
			}
			
			if (wrap)
			{
				switch (c.wrap)
				{
					case Wrap::none:
					{
						if (backward == movement)
							r.navigation = Navresult::backward;
						else // Forward.
							r.navigation = Navresult::forward;
					
						break;
					}
						
					case Wrap::around:
					{
						if (backward == movement)
							r.indexNew = max;
						else // Forward.
							r.indexNew = 0;
						
						break;
					}
				}
			}
			
			return r;
		}
	}
}
