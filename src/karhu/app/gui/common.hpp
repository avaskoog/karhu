/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GUI_COMMON_H_
	#define KARHU_GUI_COMMON_H_

	#include <karhu/conv/serialise.hpp>
	#include <karhu/conv/maths.hpp>
	
	#include <cstdint>

	namespace karhu
	{
		namespace gui
		{
			enum class Mode : std::uint8_t
			{
				absolute,
				referenceHorizontal,
				referenceVertical,
				relative,
				fraction
			};
			
			enum class Anchor : std::uint8_t
			{
				topLeft,
				centreLeft,
				bottomLeft,
				topRight,
				centreRight,
				bottomRight,
				topCentre,
				centreCentre,
				bottomCentre
			};
			
			enum class Stretch : std::uint8_t
			{
				none,
				horizontal,
				vertical,
				both
			};
			
			enum class Orientation : std::uint8_t
			{
				horizontal,
				vertical,
				grid /// @todo: Rename to 'both' for consistency?
			};
	
			enum class Overflow : std::uint8_t
			{
				none,
				hide,
				scrollHorizontal,
				scrollVertical
			};
			
			enum class Navigation : std::uint8_t
			{
				hold,
				release
			};
	
			enum class Wrap : std::uint8_t
			{
				none,
				around
				/// @todo: Wrap 'across' for grids?
			};
	
			/// @todo: Different modes for navigation (can hold or not, delay...). Should they be per container or per screen?
			
			struct Layouting
			{
				Mode
					modePositionHorizontal{Mode::referenceVertical},
					modePositionVertical  {Mode::referenceVertical},
					modeSizeHorizontal    {Mode::referenceVertical},
					modeSizeVertical      {Mode::referenceVertical};
				
				mth::Scalf
					spacingLeft  {0.0f},
					spacingRight {0.0f},
					spacingTop   {0.0f},
					spacingBottom{0.0f};
			
				Anchor  anchor {Anchor::topLeft};
				Stretch stretch{Stretch::none};
				
				void serialise(conv::Serialiser &) const;
				void deserialise(const conv::Serialiser &);
			};
		}
	}
#endif
