/**
 * @author		Ava Skoog
 * @date		2019-05-16
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/gui/common.hpp>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace gui
	{
		void Layouting::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(modePositionHorizontal)
				<< karhuIN(modePositionVertical)
				<< karhuIN(modeSizeHorizontal)
				<< karhuIN(modeSizeVertical)
				<< karhuIN(spacingLeft)
				<< karhuIN(spacingRight)
				<< karhuIN(spacingTop)
				<< karhuIN(spacingBottom)
				<< karhuIN(anchor)
				<< karhuIN(stretch);
		}

		void Layouting::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(modePositionHorizontal)
				>> karhuOUT(modePositionVertical)
				>> karhuOUT(modeSizeHorizontal)
				>> karhuOUT(modeSizeVertical)
				>> karhuOUT(spacingLeft)
				>> karhuOUT(spacingRight)
				>> karhuOUT(spacingTop)
				>> karhuOUT(spacingBottom)
				>> karhuOUT(anchor)
				>> karhuOUT(stretch);
		}
	}
}
