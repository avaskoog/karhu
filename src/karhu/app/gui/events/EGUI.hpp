/**
 * @author		Ava Skoog
 * @date		2019-05-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_GUI_EGUI_H_
	#define KARHU_GUI_EGUI_H_

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/app/gui/components/Screen.hpp>
	#include <karhu/app/gui/components/Container.hpp>
	#include <karhu/app/gui/components/Focussable.hpp>

	namespace karhu
	{
		namespace gui
		{
			class EGUI : public ecs::Event
			{
				public:
					enum class Type : std::uint8_t
					{
						focus,
						confirm,
						cancel
					};
				
				public:
					EGUI
					(
						const ecs::IDEvent &      identifier,
						const Type                type,
						      Screen       &      screen,
						      Container    *const container,
						      Focussable   *const focus,
						      Focussable   *const focusPrev
					)
					:
					ecs::Event{identifier, 0, 0, 0},
					type      {type},
					screen    {&screen},
					container {container},
					focus     {focus},
					focusPrev {focusPrev}
					{
					}
				
				public:
					Type        type;
					Screen     *screen;
					Container  *container;
					Focussable *focus, *focusPrev;
			};
		}
	}
#endif
