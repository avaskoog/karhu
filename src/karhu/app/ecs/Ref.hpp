/**
 * @author		Ava Skoog
 * @date		2021-05-30
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_ECS_REF_H_
	#define KARHU_APP_ECS_REF_H_

	#include <karhu/core/Ref.hpp>
	#include <karhu/app/ecs/common.hpp>

	namespace karhu
	{
		namespace ecs
		{
			class World;
			
			template<typename T>
			class Ref : public RefBase<T>
			{
				static_assert(std::is_base_of<Component, T>{}, "Component ref type must derive from Component");
				
				protected:
					using RefBase<T>::RefBase;
				
				friend class World;
				friend class karhu::Refbank;
			};
		}
	}
#endif
