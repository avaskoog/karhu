/**
 * @author		Ava Skoog
 * @date		2018-10-07
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_ECS_COMMON_H_
	#define KARHU_APP_ECS_COMMON_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/lib/kenno/Component.hpp>
	#include <karhu/app/lib/kenno/World.hpp>
	#include <karhu/app/lib/kenno/System.hpp>
	#include <karhu/app/lib/kenno/Event.hpp>
	#include <karhu/app/lib/kenno/common.hpp>

	#include <limits>

	namespace karhu
	{
		namespace app
		{
			class App;
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		namespace edt
		{
			class Editor;
			class IAction;
			struct Event;
		}
		#endif
		
		namespace ecs
		{
			class Component : public kenno::Component
			{
				public:
					using kenno::Component::Component;
				
				#if KARHU_EDITOR_SERVER_SUPPORTED
				public:
					virtual void editorEvent(app::App &, edt::Editor &, edt::Identifier const caller, edt::Event const &) = 0;
					virtual bool editorEdit(app::App &, edt::Editor &) = 0;
				#endif
			};
			
			struct Userdata : public kenno::Userdata
			{
				virtual void serialise(conv::Serialiser &) const = 0;
				virtual void deserialise(const conv::Serialiser &) = 0;
			};
			
			using Priority        = kenno::Priority;
			using IDScene         = kenno::IDScene;
			using IDTypeEvent     = kenno::IDTypeEvent;
			using IDEvent         = kenno::IDEvent;
			using IDListener      = kenno::IDListener;
			using IDEntity        = kenno::IDEntity;
			using IDTypeComponent = kenno::IDTypeComponent;
			using IDComponent     = kenno::IDComponent;
			using TagsEntity      = kenno::TagsEntity;
			using TagEntity       = TagsEntity;
			
			struct DefaulttagsEntity
			{
				enum : TagsEntity
				{
					none    = 0,
					custom  = (1 << 0)
				};
			};

			template<typename TComponent>
			using Pool = kenno::Pool::ViewSubpool<TComponent>;
			
			using Event               = kenno::Event;
			using EComponentCreated   = kenno::EComponentCreated;
			using EComponentDestroyed = kenno::EComponentDestroyed;
			using EEntityCreated      = kenno::EEntityCreated;
			using EEntityDestroyed    = kenno::EEntityDestroyed;
			using EComponentActive    = kenno::EComponentActive;
			using EComponentInactive  = kenno::EComponentInactive;
			using EEntityActive       = kenno::EEntityActive;
			using EEntityInactive     = kenno::EEntityInactive;
			
			using CreateEntity    = kenno::CreateEntity;
			using CreateComponent = kenno::CreateComponent;
			
			struct Priorities
			{
				enum : Priority
				{
					preprerender         = -20000,
					preprerenderCustom   = -15000,
					prerender            = -10000,
					postprerenderCustom  = -5000,
					render               =  0,
					prepostrenderCustom  =  5000,
					postrender           =  10000,
					postpostrenderCustom =  15000
				};
			};
			
			struct Componenttypes
			{
				enum : IDTypeComponent
				{
					custom = 10000
				};
			};
		}
	}
#endif
