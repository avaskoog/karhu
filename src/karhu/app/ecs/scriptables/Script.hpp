/**
 * @author		Ava Skoog
 * @date		2019-05-27
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_ECS_SCRIPTABLES_SCRIPT_H_
	#define KARHU_APP_ECS_SCRIPTABLES_SCRIPT_H_

	#include <karhu/app/ecs/components/Scriptable.hpp>
	#include <karhu/app/ecs/scripting.hpp>

	class asIScriptObject;

	namespace karhu
	{
		namespace ecs
		{
			class Script : public IScriptable
			{
				public:
					Script
					(
						const ecs::IDComponent &component,
						const ecs::IDEntity    &entity,
						const VtableScript     *vtable,
						      asIScriptObject  *object
					);
				
					asIScriptObject *object() const noexcept { return m_object; }
				
					~Script();
				
				public:
					void create(Scriptable &self, app::App &) override;
					void active(Scriptable &self, app::App &) override;
					void inactive(Scriptable &self, app::App &) override;
					void input(Scriptable &self, app::App &, const inp::EInput &, const float dt, const float dtFixed, const float timestep) override;
					void updateBeforeRender(Scriptable &self, app::App &, const float dt, const float dtFixed, const float timestep) override;
					void updateAfterRender(Scriptable &self, app::App &, const float dt, const float dtFixed, const float timestep) override;
					void message(Scriptable &self, app::App &, const ecs::IDEntity from, const std::string &s, void *) override;
					void serialise(conv::Serialiser &) const override;
					void deserialise(const conv::Serialiser &) override;
				
				private:
					VtableScript    const *m_vtable{nullptr};
					asIScriptObject       *m_object{nullptr};
			};
		}
	}
#endif
