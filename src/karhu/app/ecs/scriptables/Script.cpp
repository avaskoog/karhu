/**
 * @author		Ava Skoog
 * @date		2019-05-27
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/ecs/scriptables/Script.hpp>
#include <karhu/app/ecs/World.hpp>
#include <karhu/app/App.hpp>

/// @todo: Update stuff in here to use AS::call(r) instead.

namespace karhu
{
	namespace ecs
	{
		Script::Script
		(
			const ecs::IDComponent &component,
			const ecs::IDEntity    &entity,
			const VtableScript     *vtable,
				  asIScriptObject  *object
		)
		:
		IScriptable{component, entity},
		m_vtable   {vtable},
		m_object   {object}
		{
			if (m_object)
				m_object->AddRef();
		}
		
		void Script::create(Scriptable &self, app::App &app)
		{
			if (!m_vtable || !m_object || !m_vtable->create)
				return;
			
			asIScriptContext &context{app.scr().borrowContext()};
			context.Prepare(m_vtable->create);
			context.SetObject(m_object);
			context.SetArgObject(0, &self);
			context.SetArgObject(1, &app);
			context.Execute();
			app.scr().returnContext(context);
		}
		
		void Script::active(Scriptable &self, app::App &app)
		{
			if (!m_vtable || !m_object || !m_vtable->active)
				return;
			
			asIScriptContext &context{app.scr().borrowContext()};
			context.Prepare(m_vtable->active);
			context.SetObject(m_object);
			context.SetArgObject(0, &self);
			context.SetArgObject(1, &app);
			context.Execute();
			app.scr().returnContext(context);
		}
		
		void Script::inactive(Scriptable &self, app::App &app)
		{
			if (!m_vtable || !m_object || !m_vtable->inactive)
				return;
			
			asIScriptContext &context{app.scr().borrowContext()};
			context.Prepare(m_vtable->inactive);
			context.SetObject(m_object);
			context.SetArgObject(0, &self);
			context.SetArgObject(1, &app);
			context.Execute();
			app.scr().returnContext(context);
		}
		
		void Script::input(Scriptable &self, app::App &app, const inp::EInput &e, const float dt, const float dtFixed, const float timestep)
		{
			if (!m_vtable || !m_object || !m_vtable->input)
				return;
			
			asIScriptContext &context{app.scr().borrowContext()};
			context.Prepare(m_vtable->input);
			context.SetObject(m_object);
			context.SetArgObject(0, &self);
			context.SetArgObject(1, &app);
			context.SetArgObject(2, const_cast<void *>(reinterpret_cast<const void *>(&e)));
			context.SetArgFloat(3, dt);
			context.SetArgFloat(4, dtFixed);
			context.SetArgFloat(5, timestep);
			context.Execute();
			app.scr().returnContext(context);
		}
		
		void Script::updateBeforeRender(Scriptable &self, app::App &app, const float dt, const float dtFixed, const float timestep)
		{
			if (!m_vtable || !m_object || !m_vtable->updateBeforeRender)
				return;
			
			asIScriptContext &context{app.scr().borrowContext()};
			context.Prepare(m_vtable->updateBeforeRender);
			context.SetObject(m_object);
			context.SetArgObject(0, &self);
			context.SetArgObject(1, &app);
			context.SetArgFloat(2, dt);
			context.SetArgFloat(3, dtFixed);
			context.SetArgFloat(4, timestep);
			context.Execute();
			app.scr().returnContext(context);
		}
		
		void Script::updateAfterRender(Scriptable &self, app::App &app, const float dt, const float dtFixed, const float timestep)
		{
			if (!m_vtable || !m_object || !m_vtable->updateAfterRender)
				return;
			
			asIScriptContext &context{app.scr().borrowContext()};
			context.Prepare(m_vtable->updateAfterRender);
			context.SetObject(m_object);
			context.SetArgObject(0, &self);
			context.SetArgObject(1, &app);
			context.SetArgFloat(2, dt);
			context.SetArgFloat(3, dtFixed);
			context.SetArgFloat(4, timestep);
			context.Execute();
			app.scr().returnContext(context);
		}
		
		void Script::message(Scriptable &self, app::App &app, const ecs::IDEntity from, const std::string &s, void *data)
		{
			if (!m_vtable || !m_object || !m_vtable->message)
				return;
			
			asIScriptContext &context{app.scr().borrowContext()};
			
			context.Prepare(m_vtable->message);
			context.SetObject(m_object);
			context.SetArgObject(0, &self);
			context.SetArgObject(1, &app);
			context.SetArgQWord(2, static_cast<asQWORD>(from));
			context.SetArgObject(3, const_cast<std::string *>(&s));
			context.SetArgObject(4, data);
			context.Execute();
			app.scr().returnContext(context);
		}
		
		void Script::serialise(conv::Serialiser &ser) const
		{
			if (m_object)
				app::App::instance()->scr().serialise(*m_object, ser);
		}
		
		void Script::deserialise(const conv::Serialiser &ser)
		{
			if (m_object)
				app::App::instance()->scr().deserialise(*m_object, ser);
		}
		
		Script::~Script()
		{
			if (m_object)
				m_object->Release();
		}
	}
}
