/**
 * @author		Ava Skoog
 * @date		2018-10-20
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_ECS_WORLD_H_
	#define KARHU_APP_ECS_WORLD_H_

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/app/ecs/Ref.hpp>
	#include <karhu/app/Bitflagbank.hpp>
	#include <karhu/app/Scriptmanager.hpp>
	#include <karhu/app/ecs/components/Scriptable.hpp>
	#include <karhu/app/ecs/scriptables/Script.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/data/serialise.hpp>
	#include <karhu/res/Resource.hpp>
	#include <karhu/core/GeneratorID.hpp>
	#include <karhu/core/log.hpp>

	#include <vector>
	#include <functional>
	#include <algorithm>

	namespace karhu
	{
		namespace app
		{
			namespace tool
			{
				class SerialiserECS;
			}
		}
		
		namespace ecs
		{
			class Tagbank : public app::Bitflagbank<TagEntity, DefaulttagsEntity::custom, DefaulttagsEntity::none>
			{
				public:
					char const *label()      override { return "tags"; }
					char const *descriptor() override { return "entity tags"; }
			};
			
			/// @todo: Documentation comment.
			/// @todo: Enforce singleton? Throw error on additional allocation?
			class World : private kenno::World
			{
				public:
					template<typename TEvent>
					using FListenToEvent = std::function<void
					(
						      app::App &,
						const TEvent   &,
						const float    dt,
						const float    dtFixed,
						const float    timestep,
						      void     *
					)>;

				private:
					using FSerialiseComponent = std::function<bool
					(
						const IDComponent     &,
						const IDEntity        &,
						      conv::JSON::Obj &
					)>;
				
					using FDeserialiseComponent = std::function<bool
					(
						const IDComponent     &,
						const IDEntity        &,
						const bool             active,
						const conv::JSON::Obj &
					)>;
				
					using FDeserialiseExistingComponent = std::function<bool
					(
						const IDComponent     &,
						const IDEntity        &,
						const conv::JSON::Obj &
					)>;

				private:
					struct DataPerTypeComponent
					{
						FSerialiseComponent           fSerialiseComponent;
						FDeserialiseComponent         fDeserialiseComponent;
						FDeserialiseExistingComponent fDeserialiseExistingComponent;
					};

				public:
					World(app::App &app, app::Scriptmanager &scripts);
					World(const World &) = delete;
					World(World &&) = delete;
					World &operator=(const World &) = delete;
					World &operator=(World &&) = delete;
				
					app::App &app() noexcept { return m_app; }

					// Use some of the inherited functions as they are.
					// We do not want to expose the ones that allow for
					// a specific ID to be specified, as the ID generators
					// will be used instead; the exception is in the ECS
					// serialiser, which is a friend class to allow it to
					// load scenes, entities, components and so on with
					// specific ID numbers from file.
					using kenno::World::createSystemWithPriorityAndArgs;
					using kenno::World::sceneLoaded;
					using kenno::World::sceneLoadedWithError;
					using kenno::World::sceneActive;
					using kenno::World::scenes;
					using kenno::World::createEntityInScene; /// @todo: Should we allow direct access to entity creation with ID and flags?
					using kenno::World::destroyChildrenOfEntity;
					using kenno::World::entityUserdata;
					using kenno::World::createComponentInEntity; /// @todo: Should we allow direct access to component creation with ID and flags?
					using kenno::World::grandparentsInScene;
					using kenno::World::parentsInScene;
					using kenno::World::childrenInScene;
					using kenno::World::entityParentFirst;
					using kenno::World::entityParentLast;
					using kenno::World::entityChildFirst;
					using kenno::World::entityChildLast;
					using kenno::World::entityParentOf;
					using kenno::World::forEachParentOfEntity;
					using kenno::World::forEachChildOfEntity;
					using kenno::World::typesOfComponentsRegistered;
					using kenno::World::sizeOfTypeOfComponent;
					using kenno::World::component;
					using kenno::World::componentByTypeInEntity;
					using kenno::World::componentInEntity;
					using kenno::World::componentsInEntity;
					using kenno::World::componentsByIDInEntity;
					using kenno::World::componentsInScene;
					using kenno::World::componentsInActiveScenes;
					using kenno::World::componentpoolsInActiveScenes;
					using kenno::World::entityActive;
					using kenno::World::entityActiveInHierarchy;
					using kenno::World::entityActiveSelf;
					using kenno::World::entityName;
					using kenno::World::entityTags;
					using kenno::World::entitiesByTags;
					using kenno::World::entitiesInScene;
					using kenno::World::entitiesInActiveScenes;
					using kenno::World::componentActiveInEntity;
					/// @todo: Remove these fully.
//					using kenno::World::componentActiveInEntityInHierarchy;
//					using kenno::World::componentActiveInEntitySelf;
					using kenno::World::IDOfTypeEvent;
					using kenno::World::nameOfTypeEvent;
					using kenno::World::emitEvent;
					using kenno::World::queueEvent;
					using kenno::World::IDOfTypeComponent;
					using kenno::World::nameOfTypeComponent;
					/// @todo: Add the rest...

					/// @todo: Editor will have to manually load up files and search them for this sort of data in each scene or a generated file that keeps track of these numbers; World should not have to know about it.
					bool init
					(
						const std::vector<IDScene>     &IDsUsedScene,
						const std::vector<IDEntity>    &IDsUsedEntity,
						const std::vector<IDComponent> &IDsUsedComponent
					);
				
					void update(const float dt, const float dtFixed, const float timestep);
				
					template<typename T>
					bool registerTypeEvent
					(
						IDTypeEvent const        identifier,
						char        const *const name
					);
				
					template<typename TComponent, typename TEvent>
					bool createListenerToEvent
					(
						void *data,
						const FListenToEvent<TEvent> &f,
						const Priority priority = 0
					);
				
					template<typename TEvent>
					bool createListenerToEvent
					(
						void *data,
						const FListenToEvent<TEvent> &f,
						const Priority priority = 0
					);
				
					template<typename T>
					bool registerTypeComponent
					(
						const IDTypeComponent &identifier,
						const char            *name,
						const std::size_t     &countMaxPerEntity
					);
				
					template<typename T>
					bool registerTypeIScriptable(const IDTypeIScriptable &, const char *);
				
					std::unique_ptr<IScriptable> createIScriptable
					(
						const IDTypeIScriptable &,
						const IDComponent       &,
						const IDEntity          &,
						const res::IDResource   &script = 0
					);
				
					Result<IDScene> createScene();
					Status createScene(const IDScene identifier);
				
					IDEntity createEntityInScene
					(
						IDScene     const IDScene,
						std::string const &name   = {}
					);
				
					bool destroyEntity(ecs::IDEntity const);
				
					Component *createComponentInEntity
					(
						IDTypeComponent const type,
						IDEntity        const IDEntity
					);
				
					Component *createComponentByTypeInEntity
					(
						const IDTypeComponent &IDType,
						const IDEntity        &IDEntity
					);
				
					template<typename T>
					T *createComponentInEntity
					(
						IDEntity const IDEntity
					)
					{
						IDComponent const ID{m_genIDComponent.next()};
						
						auto r(kenno::World::createComponentInEntity<T>
						(
							ID,
							IDEntity,
							CreateComponent::all
						)); /// @todo: Should we allow for setting active state at creation here?
						
						if (r.success)
						{
							m_refbank.refreshReferences();
							return r.value;
						}
						
						m_genIDComponent.release(ID);
						
						return nullptr;
					}
				
					template<typename T>
					Ref<T> refComponentInEntity(IDComponent const identifier, IDEntity const entity)
					{
						return m_refbank.createReference<Ref<T>, ReftrackerCustom>(identifier, entity);
					}
				
					template<typename T>
					Ref<T> refComponentInEntity(IDEntity const entity)
					{
						if (T *component = componentInEntity<T>(entity))
							return m_refbank.createReference<Ref<T>, ReftrackerCustom>
							(
								static_cast<IDRef>(component->identifier()),
								entity
							);
						
						return Ref<T>{};
					}
				
					Reference refComponentInEntityRaw(IDComponent const identifier, IDEntity const entity);
					Reference refComponentByTypeInEntityRaw(IDTypeComponent const type, IDEntity const entity);
				
					bool destroyComponentInEntity
					(
						IDComponent const IDC,
						IDEntity    const IDE
					);

					/// @todo: Remember to release ID's in destruction functions.
				
					Tagbank &tags() noexcept { return m_tags; }
					Tagbank const &tags() const noexcept { return m_tags; }
				
					// De/serialisation.
				
					bool serialiseComponentInEntity(IDComponent const IDC, IDTypeComponent const IDT, IDEntity const IDE, conv::JSON::Obj &target);
					bool deserialiseComponentInEntity(IDEntity const IDE, conv::JSON::Obj const &target);
				
					bool deserialiseExistingComponentInEntity
					(
						const IDEntity        &IDE,
						const IDComponent     &IDC,
						const IDTypeComponent &IDT,
						const conv::JSON::Obj &target
					);
				
					VtableScript *vtableScript(const res::IDResource &);
					void destroyVtableScript(char const *nameModule);
				
				private:
					template<typename T>
					bool registerTypeIScriptableUnrestricted(const IDTypeIScriptable &type, const char *);

				private:
					static constexpr std::size_t countPerChunk() noexcept { return 64; }
				
					template<typename T>
					static IDTypeIScriptable IDOfTypeIScriptable(const IDTypeIScriptable &ID = 0) noexcept
					{
						static IDTypeIScriptable value{0};
						
						if (0 != ID)
							value = ID;
						
						return value;
					}
				
				private:
					static IDTypeIScriptable c_IDTypeIScriptableLast;

				private:
					app::App &m_app;
					app::Scriptmanager &m_scripts;
				
					GeneratorID<IDScene>     m_genIDScene;
					GeneratorID<IDEntity>    m_genIDEntity;
					GeneratorID<IDComponent> m_genIDComponent;
					GeneratorID<IDListener>  m_genIDListener;
					GeneratorID<IDEvent>     m_genIDEvent;
				
					std::map<IDTypeComponent, DataPerTypeComponent> m_dataPerTypeComponent; /// @todo: Add function to register types and remember to store functions here.
				
					std::map
					<
						IDTypeIScriptable,
						std::function
						<
							std::unique_ptr<IScriptable>
							(
								const IDComponent &,
								const IDEntity    &
							)
						>
					> m_fsCreateIScriptable;
				
					std::map<std::string, std::unique_ptr<VtableScript>> m_vtablesScripts;
				
					Tagbank m_tags;
				
				private:
					class RefbankCustom : public Refbank
					{
						public:
							RefbankCustom(World &world) : world{world} {}
							World &world;
						
						protected:
							virtual void *getPointerForReference(Reftracker const &) override;
							virtual bool onRefcountReachedZero(Reftracker const &) override;
					} m_refbank{*this};
				
					class ReftrackerCustom : public Reftracker
					{
						public:
							ReftrackerCustom(Refbank &bank, IDRef const identifier, IDEntity const entity)
							:
							Reftracker{bank, identifier},
							entity    {entity}
							{
							}
						
						public:
							IDEntity const
								entity;
					};
				
				friend class app::tool::SerialiserECS;
			};
			
			template<typename T>
			bool World::registerTypeEvent
			(
				IDTypeEvent const        identifier,
				char        const *const name
			)
			{
				auto s(kenno::World::registerTypeOfEvent<T>
				(
					identifier,
					name
				));
				
				if (!s.success)
				{
					log::err("Karhu")
						<< "Could not register event type '"
						<< name
						<< "': "
						<< s.error;
					
					return false;
				}
				
				if (!m_scripts.registerTypeEvent<T>(identifier, name))
					return false;
				
				return true;
			}
			
			template<typename TComponent, typename TEvent>
			bool World::createListenerToEvent
			(
				void *data,
				const FListenToEvent<TEvent> &f,
				const Priority priority
			)
			{
				struct Functor
				{
					FListenToEvent<TEvent> f;
					void operator()(kenno::World &world, const TEvent &t, const float dt, const float dtFixed, const float timestep, void *data)
					{
						// Kenno passes us a kenno::World but we need
						// our listeners to receive an ecs::World.
						f(reinterpret_cast<World *>(&world)->app(), t, dt, dtFixed, timestep, data);
					}
				};
				
				const auto ID(m_genIDListener.next());
				const auto s(kenno::World::createListenerToEvent<TComponent, TEvent>
				(
					ID,
					data,
					Functor{f},
					priority
				));
				
				if (!s.success)
				{
					m_genIDListener.release(ID);
					log::err("Karhu")
						<< "Failed to register event listener: "
						<< s.error;
					return false;
				}
				
				return true;
			}
			
			template<typename TEvent>
			bool World::createListenerToEvent
			(
				void *data,
				const FListenToEvent<TEvent> &f,
				const Priority priority
			)
			{
				struct Functor
				{
					FListenToEvent<TEvent> f;
					void operator()
					(
						      kenno::World &world,
						const TEvent       &t,
						const float        dt,
						const float        dtFixed,
						const float        timestep,
						      void         *data
					)
					{
						// Kenno passes us a kenno::World but we need
						// our listeners to receive an ecs::World.
						f
						(
							reinterpret_cast<World *>(&world)->app(),
							t,
							dt,
							dtFixed,
							timestep,
							data
						);
					}
				};
				
				const auto ID(m_genIDListener.next());
				const auto s(kenno::World::createListenerToEvent<TEvent>
				(
					ID,
					data,
					Functor{f},
					priority
				));
				
				if (!s.success)
				{
					m_genIDListener.release(ID);
					
					log::err("Karhu")
						<< "Failed to register event listener: "
						<< s.error;
					
					return false;
				}
				
				return true;
			}
			
			template<typename T>
			bool World::registerTypeComponent
			(
				const IDTypeComponent &identifier,
				const char            *name,
				const std::size_t     &countMaxPerEntity
			)
			{
				auto s(kenno::World::registerTypeOfComponentWithCountsAndGrowability<T>
				(
					identifier,
					name,
					countMaxPerEntity,
					countPerChunk(),
					true
				));
				
				if (!s.success)
				{
					log::err("Karhu")
						<< "Could not register component type '"
						<< name
						<< "': "
						<< s.error;
					
					return false;
				}
				
				m_dataPerTypeComponent.emplace
				(
					identifier,
					DataPerTypeComponent
					{
						// Serialise component.
						[this]
						(
							const IDComponent     &IDC,
							const IDEntity        &IDE,
							      conv::JSON::Obj &target
						) -> bool
						{
							
							auto component(componentInEntity<T>(IDC, IDE));
							
							if (!component)
								return false;
					
							conv::Serialiser ser{target};
							component->serialise(ser);
							
							return true;
						},
				
						// Deserialise component.
						[this]
						(
							const IDComponent     &IDC,
							const IDEntity        &IDE,
							const bool             active,
							const conv::JSON::Obj &target
						) -> bool
						{
							auto flags(CreateComponent::all);
							
							if (!active)
								flags &= ~CreateComponent::active;
						
							auto s(createComponentInEntity<T>(IDC, IDE, flags)); /// @todo: Deserialise active state from JSON.

							if (!s.success)
							{
								log::err("Karhu")
									<< "Could not deserialise scene: error deserialising component "
									<< static_cast<std::uint64_t>(IDC)
									<< ": "
									<< s.error;
								
								return false;
							}

							conv::Serialiser ser{target};
							s.value->deserialise(ser);
							
							return true;
						},
				
						// Deserialise existing component.
						[this]
						(
							const IDComponent     &IDC,
							const IDEntity        &IDE,
							const conv::JSON::Obj &target
						) -> bool
						{
							auto s(componentInEntity<T>(IDC, IDE));

							if (!s)
							{
								log::err("Karhu")
									<< "Could not deserialise scene: error deserialising existing component "
									<< static_cast<std::uint64_t>(IDC);
								
								return false;
							}

							conv::Serialiser ser{target};
							s->deserialise(ser);
							
							return true;
						}
					}
				);
				
				if (!m_scripts.registerTypeComponent<T>(identifier, name))
					return false;
				
				return true;
			}
			
			template<typename T>
			bool World::registerTypeIScriptable
			(
				const IDTypeIScriptable &type,
				const char *name
			)
			{
				static_assert(std::is_base_of<IScriptable, T>{}, "IScriptable type must derive from IScriptable");
				
				if (type <= 100)
				{
					log::err("Karhu")
						<< "Cannot register scriptable interface type with ID "
						<< static_cast<int>(type)
						<< " because the range 0-100 is reserved for built-in types";
					
					return false;
				}
				
				return registerTypeIScriptableUnrestricted<T>(type, name);
			}
			
			template<typename T>
			bool World::registerTypeIScriptableUnrestricted
			(
				const IDTypeIScriptable &type,
				const char *name
			)
			{
				IDOfTypeIScriptable<T>(type);
				
				auto it(m_fsCreateIScriptable.find(type));
				
				if (it != m_fsCreateIScriptable.end())
					return false;
				
				m_fsCreateIScriptable.emplace
				(
					type,
					[]
					(
						const IDComponent &IDC,
						const IDEntity    &IDE
					) -> std::unique_ptr<IScriptable>
					{
						return std::make_unique<T>(IDC, IDE);
					}
				);
				
				if (!m_scripts.registerTypeIScriptable<T>(type, name))
					return false;
				
				return true;
			}
		}
	}

	/// @todo: Move these over to App and make them take an app handle instead of an ECS handle.
	/// @todo: Maybe the versions exposed to the user should not require a manual type ID to be set, or at least automatically put it in the custom range?

	#define karhuREGISTER_TYPE_COMPONENT(objectWorld, nameOfDatatype, IDOfType, maxCountPerEntity) \
		objectWorld . registerTypeComponent< nameOfDatatype >( IDOfType, #nameOfDatatype, maxCountPerEntity );

	#define karhuREGISTER_TYPE_ISCRIPTABLE(objectWorld, nameOfDatatype, IDOfType) \
		objectWorld . registerTypeIScriptable< nameOfDatatype >( IDOfType, #nameOfDatatype );

	#define karhuREGISTER_TYPE_EVENT(objectWorld, nameOfDatatype, IDOfType) \
		objectWorld . registerTypeEvent< nameOfDatatype >( IDOfType, #nameOfDatatype );
#endif
