/**
 * @author		Ava Skoog
 * @date		2019-05-23
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_ECS_COMPONENTS_SCRIPTABLE_H_
	#define KARHU_APP_ECS_COMPONENTS_SCRIPTABLE_H_

	#include <karhu/app/edt/support.hpp>

	#include <karhu/app/scripting.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/res/Bank.hpp>
	#include <karhu/res/Script.hpp>
	#include <karhu/conv/serialise.hpp>

	#include <cstring>
	#include <cstdint>
	#include <memory>

	namespace karhu
	{
		namespace app
		{
			class App;
			class Scriptmanager;
		}
		
		namespace inp
		{
			class EInput;
		}
		
		namespace phx
		{
			/// @todo: Collisions and such.
		}
		
		namespace ecs
		{
			class World;
			
			using IDTypeIScriptable = IDTypeComponent;
			
			class Scriptable;
			class Script;
			
			struct IScriptable
			{
				IScriptable
				(
					const IDComponent &component,
					const IDEntity    &entity
				)
				:
				m_component{component},
				m_entity   {entity}
				{
				}
				
				IDComponent component() const noexcept { return m_component; }
				IDEntity    entity()    const noexcept { return m_entity; }
				
				virtual void create(Scriptable &self, app::App &) = 0;
				virtual void active(Scriptable &self, app::App &) = 0;
				virtual void inactive(Scriptable &self, app::App &) = 0;
				virtual void input(Scriptable &self, app::App &, const inp::EInput &, const float dt, const float dtFixed, const float timestep) = 0;
				virtual void updateBeforeRender(Scriptable &self, app::App &, const float dt, const float dtFixed, const float timestep) = 0;
				virtual void updateAfterRender(Scriptable &self, app::App &, const float dt, const float dtFixed, const float timestep) = 0;
				virtual void message(Scriptable &self, app::App &, const ecs::IDEntity from, const std::string &s, void *data) = 0;
				virtual void serialise(conv::Serialiser &) const = 0;
				virtual void deserialise(const conv::Serialiser &) = 0;
				
				virtual ~IScriptable() = default;
				
				private:
					const IDComponent m_component;
					const IDEntity    m_entity;
			};
			
			class Scriptable : public Component
			{
				public:
					using Priority = std::int32_t;
				
				public:
					using ecs::Component::Component;
				
				public:
					Priority priority() const noexcept { return m_priority; }
					void priority(Priority const);
				
				public:
					void message(const ecs::IDEntity to, const std::string &s)
					{
						if (!m_interface)
							return;
						
						AS::Ref ref;
						dispatchMessage(to, s, &ref);
					}
				
					template<typename T>
					std::enable_if_t
					<
						(
							 std::is_class    <T>{} &&
							!std::is_pointer  <T>{} &&
							!std::is_reference<T>{} &&
							!std::is_const    <T>{} &&
							!std::is_volatile <T>{}
						),
						void
					>
					message
					(
						const ecs::IDEntity  to,
						const std::string   &m,
						      T             *data
					)
					{
						static_assert
						(
							!std::is_same<T, void>{},
							"Cannot explicitly send void pointer as message because the type needs to be known for scripts"
						);
						
						if (!m_interface)
							return;
						
						const Recepient r(findRecepient(to));
						
						if (!r.entity)
							return;
						
						const bool
							scriptSelf  (isScript()),
							&scriptOther(r.script);
						
						asITypeInfo *const info{AS::infoForType<T>()};
						
						// If talking between C++ and AngelScript,
						// only registered handle types are allowed.
						// If talking inside of C++ anything goes,
						// and in AS bindings make sure of validity,
						// so we only need to check across languages.
						if (scriptSelf != scriptOther)
						{
							if (!info)
							{
								log::err("Karhu") << "Cannot send scriptable message with non-registered native type from native code to script";
								return;
							}
							
							const auto f(info->GetFlags());
							
							if
							(
								!(
									(f & asOBJ_REF && !(f & asOBJ_NOHANDLE)) ||
									(f & asOBJ_VALUE && f & asOBJ_ASHANDLE)
								)
							)
							{
								log::err("Karhu") << "Cannot send scriptable message with non-handle native type from native code to script";
								return;
							}
						}
						
						// If the recepient is a script, we have to pass a reference object.
						if (r.script)
						{
							AS::Ref ref{reinterpret_cast<void *>(data), info};
							dispatchMessage(to, m, &ref);
						}
						// Otherwise we can just pass it as is.
						else
							dispatchMessage(to, m, data);
					}
				
					void message(const ecs::IDEntity to, const std::string &s, AS::Ref &ref)
					{
						if (!m_interface)
							return;
						
						const Recepient r{findRecepient(to)};
						
						if (!r.entity)
							return;
						
						// If the recepient is a script, we have to pass the reference object.
						if (r.script)
							dispatchMessage(r, s, &ref);
						// Otherwise we have to extract a valid C++ object.
						// Note that ref.GetTypeId() and ref.GetType()->GetTypeId() return different values!
						else if (AS::isRegisteredIDForType(ref.GetType()->GetTypeId()))
							dispatchMessage(r, s, ref.GetRef());
						else
						{
							const char *const space{ref.GetType()->GetNamespace()};
							std::string name;
							
							if (space && 0 != std::strlen(space))
							{
								name += space;
								name += "::";
							}
							
							name += ref.GetType()->GetName();
							
							log::err("Karhu")
								<< "Cannot send scriptable message with script type '"
								<< name
								<< "' from script to native code";
						}
					}
				
					res::IDResource script() const noexcept { return m_script; }
				
					IScriptable *interface() const noexcept { return m_interface.get(); }
					IScriptable *resetInterface();
				
					asIScriptObject *scriptobject() const;
				
					AS::Ref scriptobjectAS() const
					{
						if (auto o = scriptobject())
							return {o, o->GetObjectType()};
						else
							return {};
					}
				
				public:
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
				
					#if KARHU_EDITOR_SERVER_SUPPORTED
					void editorEvent(app::App &, edt::Editor &, edt::Identifier const, edt::Event const &) override {}
					bool editorEdit(app::App &, edt::Editor &) override;
					#endif
				
				private:
					struct Recepient
					{
						Scriptable *entity{nullptr};
						bool        script{false};
					};
				
					bool isScript() const;
				
					Recepient findRecepient(const ecs::IDEntity);
				
					void dispatchMessage(const Recepient &, const std::string &, void *data);
				
					void dispatchMessage(const ecs::IDEntity to, const std::string &s, void *data)
					{
						const Recepient r{findRecepient(to)};
						
						if (r.entity)
							dispatchMessage(r, s, data);
					}
				
				private:
					Priority m_priority{0};
					IDTypeIScriptable m_typeIScriptable;
				
				private:
					res::IDResource m_script{0};
					std::unique_ptr<IScriptable> m_interface;
				
				friend class SystemScriptable;
				friend class app::Scriptmanager;
			};
		}
	}
#endif
