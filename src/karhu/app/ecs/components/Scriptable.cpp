/**
 * @author		Ava Skoog
 * @date		2019-05-23
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/ecs/components/Scriptable.hpp>
#include <karhu/app/ecs/scriptables/Script.hpp>
#include <karhu/app/ecs/World.hpp>
#include <karhu/app/App.hpp>
#include <karhu/data/serialise.hpp>
#include <karhu/app/edt/dropdown.inl>

namespace karhu
{
	namespace ecs
	{
		void Scriptable::priority(Priority const v)
		{
			m_priority = v;
		}
		
		asIScriptObject *Scriptable::scriptobject() const
		{
			if (const auto script = dynamic_cast<Script *>(m_interface.get()))
				return script->object();
			
			return nullptr;
		}
		
		bool Scriptable::isScript() const
		{
			return static_cast<bool>
			(
				(m_interface)
				?
					dynamic_cast<Script *>(m_interface.get())
				:
					nullptr
			);
		}
		
		Scriptable::Recepient Scriptable::findRecepient(const ecs::IDEntity ID)
		{
			Recepient r;
			
			app::App &app{*app::App::instance()};
			
			if (ID == entity())
				r.entity = this;
			else
				r.entity = app.ecs().componentInEntity<Scriptable>(ID);
			
			if (r.entity && r.entity->m_interface)
				r.script = r.entity->isScript();
			else
				r.entity = nullptr;
			
			return r;
		}
		
		void Scriptable::dispatchMessage
		(
			const Recepient   &r,
			const std::string &s,
			      void        *data
		)
		{
			r.entity->m_interface->message
			(
				*r.entity,
				*app::App::instance(),
				entity(),
				s,
				data
			);
		}
		
		void Scriptable::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("priority",        m_priority)
				<< karhuINn("typeIScriptable", m_typeIScriptable)
				<< karhuINn("script",          m_script);
			
			if (m_interface)
				ser << karhuINn("custom", *m_interface);
		}
		
		void Scriptable::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUTn("priority",        m_priority)
				>> karhuOUTn("typeIScriptable", m_typeIScriptable)
				>> karhuOUTn("script",          m_script);
			
			if (0 != m_typeIScriptable || 0 != m_script)
			{
				/// @todo: Get rid of App::instance().
				m_interface = app::App::instance()->ecs().createIScriptable
				(
					m_typeIScriptable,
					identifier(),
					entity(),
					m_script
				);
				
				if (m_interface)
					ser >> karhuOUTn("custom", *m_interface);
			}
		}
		
		IScriptable *Scriptable::resetInterface()
		{
			if (0 != m_script)
				/// @todo: Get rid of App::instance().
				m_interface = app::App::instance()->ecs().createIScriptable
				(
					m_typeIScriptable,
					identifier(),
					entity(),
					m_script
				);
			
			return m_interface.get();
		}
		
		#if KARHU_EDITOR_SERVER_SUPPORTED
		bool Scriptable::editorEdit(app::App &app, edt::Editor &editor)
		{
			bool
				r    {false},
				valid{false};
			
			// Priority.
			
			ImGui::InputScalar("Priority", app::datatypeImGui<Priority>(), &m_priority);
			
			if (ImGui::IsItemDeactivated() || ImGui::IsItemActivated())
				r = true;
			
			// C++ type.
			
			auto const
				&typesCPP(app.scr().dataPerTypeIScriptable());
			
			auto const
				itCurr(typesCPP.find(m_typeIScriptable));
			
			char const *preview;
			
			if (itCurr != typesCPP.end())
				preview = itCurr->second.name.c_str();
			else
				preview = "(none)";
			
			if (ImGui::BeginCombo("C++", preview))
			{
				ImGui::Selectable("(none)");
			
				for (auto const &type : typesCPP)
				{
					bool const
						selected{(type.first == m_typeIScriptable)};
					
					ImGui::Selectable(type.second.name.c_str(), selected);
					
					if (selected)
						ImGui::SetItemDefaultFocus();
				}
				
				ImGui::EndCombo();
			}
			
			// Script type.
			
			editor.pickerResource("scr-scr", "Script", m_script, app.res().IDOfType<res::Script>());
			
			/// @todo: Actually allocate new IScriptable if the type is set/changed, and figure out how to undo/redo without losing data (runtime serialisation maybe?).
			
			// Type-specific options.
			
			if (m_interface)
			{
				if (auto object = scriptobject())
				{
					if (auto type = object->GetObjectType())
					{
						char const
							*const name {type->GetName()},
							*const space{type->GetNamespace()};
						
						if (name)
						{
							editor.header("Script type options");
							
							ImGui::TextDisabled
							(
								"%s%s%s",
								space,
								((0 != std::strlen(space)) ? "::" : ""),
								name
							);
							valid = true;
						}
						
						if (auto props = app.scr().propsWithAttribsForType(type->GetTypeId()))
						{
							for (auto &prop : *props)
							{
								for (auto &attr : prop.attribs)
								{
									if (attr == "[serialise]")
									{
										r |= app.scr().edit(*object, prop);
										break;
									}
								}
							}
						}
					}
				}
				else
				{
					std::string const name{app.scr().nameTypeIScriptableByID(m_typeIScriptable)};
					
					if (!name.empty())
					{
						editor.header("C++ type options");
						ImGui::TextDisabled("%s", name.c_str());
						valid = true;
					}
				}
			}
			
			if (!valid)
			{
				ImGui::Spacing();
				ImGui::PushStyleColor(ImGuiCol_Text, ImGui::GetColorU32(ImGuiCol_TextDisabled));
				ImGui::Text("No valid C++ or script type has been set.");
				ImGui::PopStyleColor();
			}
			
			return r;
		}
		#endif
	}
}
