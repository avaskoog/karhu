/**
 * @author		Ava Skoog
 * @date		2018-10-20
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/ecs/World.hpp>
#include <karhu/app/App.hpp>

namespace karhu
{
	namespace ecs
	{
		World::World(app::App &app, app::Scriptmanager &scripts)
		:
		kenno::World{[this]() { return m_genIDComponent.next(); }, "Karhu"},
		m_app       {app},
		m_scripts   {scripts}
		{
		}
		
		/**
		 * Initialises the world and needs to be called on startup.
		 *
		 * The ID numbers already in use in the project need to be
		 * specified so that any further ID numbers generated at
		 * runtime will not clash with those that are already in use.
		 *
		 * @param IDsUsedScene     The ID numbers in use by scenes.
		 * @param IDsUsedEntity    The ID numbers in use by entities.
		 * @param IDsUsedComponent The ID numbers in use by components.
		 */
		bool World::init
		(
			const std::vector<IDScene>     &IDsUsedScene,
			const std::vector<IDEntity>    &IDsUsedEntity,
			const std::vector<IDComponent> &IDsUsedComponent
		)
		{
			/// @todo: Can possibly remove/simplify this whole thing now that ID's are no longer stored in scenes.
			
			if (!m_genIDScene.reset(IDsUsedScene))
				return false;
			
			if (!m_genIDEntity.reset(IDsUsedEntity))
				return false;
			
			if (!m_genIDComponent.reset(IDsUsedComponent))
				return false;
			
			return true;
		}
		
		void World::update(const float dt, const float dtFixed, const float timestep)
		{
			kenno::World::update(dt, dtFixed, timestep);
		}

		std::unique_ptr<IScriptable> World::createIScriptable
		(
			const IDTypeIScriptable &type,
			const IDComponent       &IDC,
			const IDEntity          &IDE,
			const res::IDResource   &script
		)
		{
			std::unique_ptr<IScriptable> r;
	
			if (0 != script)
			{
				asIScriptObject *object{nullptr};
				VtableScript    *vtable{nullptr};
				
				asIScriptContext &context{m_app.scr().borrowContext()};
				
				if ((vtable = vtableScript(script)))
				{
					context.Prepare(vtable->factory);

					if (asEXECUTION_FINISHED == context.Execute())
						object = *reinterpret_cast<asIScriptObject **>(context.GetAddressOfReturnValue());
				}
				
				r = std::make_unique<Script>(IDC, IDE, vtable, object);
				
				m_app.scr().returnContext(context);
			}
			else
			{
				auto it(m_fsCreateIScriptable.find(type));
				
				if (it != m_fsCreateIScriptable.end())
					r = it->second(IDC, IDE);
			}
			
			return r;
		}
		
		Result<IDScene> World::createScene()
		{
			const IDScene ID{m_genIDScene.next()};
			
			const auto s(createScene(ID));
			
			if (!s.success)
			{
				m_genIDScene.release(ID);
				return {0, false, std::move(s.error)};
			}
			
			return {ID, true};
		}
		
		Status World::createScene(const IDScene identifier)
		{		
			const auto s(kenno::World::createScene(identifier));
			
			if (!s.success)
				return {false, std::move(s.error)};
			
			return {true};
		}
		
		IDEntity World::createEntityInScene
		(
			IDScene     const IDScene,
			std::string const &name
		)
		{
			const IDEntity ID{m_genIDEntity.next()};
			
			if
			(
				kenno::World::createEntityInScene
				(
					ID,
					IDScene,
					CreateEntity::all, /// @todo: Load active state from JSON.
					name
				).success
			)
				return ID;
			
			m_genIDEntity.release(ID);
			
			return 0;
		}
		
		bool World::destroyEntity(const ecs::IDEntity IDEntity)
		{
			auto const s(kenno::World::destroyEntity(IDEntity));
			
			if (!s.success)
				log::err("Karhu") << s.error;
			
			m_refbank.refreshReferences();
			
			return s.success;
		}
		
		Component *World::createComponentInEntity
		(
			IDTypeComponent const type,
			IDEntity        const IDEntity
		)
		{
			IDComponent const ID{m_genIDComponent.next()};

			auto r(kenno::World::createComponentInEntity
			(
				type,
				ID,
				IDEntity,
				CreateComponent::all
			)); /// @todo: Should we allow for setting active state at creation here?

			if (r.success)
			{
				m_refbank.refreshReferences();
				return dynamic_cast<Component *>(r.value);
			}

			m_genIDComponent.release(ID);

			return nullptr;
		}
		
		Component *World::createComponentByTypeInEntity
		(
			const IDTypeComponent &IDType,
			const IDEntity        &IDEntity
		)
		{
			const IDComponent ID{m_genIDComponent.next()};
			
			auto r(kenno::World::createComponentInEntity(IDType, ID, IDEntity, CreateComponent::all)); /// @todo: Should we allow for setting active state at creation here?
			
			if (r.success)
			{
				m_refbank.refreshReferences();
				return dynamic_cast<Component *>(r.value);
			}
			
			m_genIDComponent.release(ID);
			
			return nullptr;
		}
		
		Reference World::refComponentInEntityRaw
		(
			IDComponent const identifier,
			IDEntity    const entity
		)
		{
			return m_refbank.createReference<Reference, ReftrackerCustom>
			(
				static_cast<IDRef>(identifier),
				entity
			);
		}
		
		Reference World::refComponentByTypeInEntityRaw
		(
			IDTypeComponent const type,
			IDEntity        const entity
		)
		{
			if (kenno::Component *component = kenno::World::componentByTypeInEntity(type, entity))
				return m_refbank.createReference<Reference, ReftrackerCustom>
				(
					static_cast<IDRef>(component->identifier()),
					entity
				);
			
			return {};
		}
		
		bool World::destroyComponentInEntity
		(
			IDComponent const IDC,
			IDEntity    const IDE
		)
		{
			auto const s(kenno::World::destroyComponentInEntity(IDC, IDE));
			
			if (!s.success)
				log::err("Karhu") << s.error;
			
			m_refbank.refreshReferences();
			
			return s.success;
		}
		
		bool World::serialiseComponentInEntity
		(
			IDComponent     const  IDC,
			IDTypeComponent const  IDT,
			IDEntity        const  IDE,
			conv::JSON::Obj       &target
		)
		{
			auto const
				it(m_dataPerTypeComponent.find(IDT));
			
			if (it == m_dataPerTypeComponent.end())
				return false;
			
			target.set("type", IDT);
			
			auto const
				c(componentInEntity(IDC, IDE));
			
			if (!c->activeSelf())
				target.set("active", false);
			
			auto
				data(target.set("data", conv::JSON::Obj{}).object());
			
			if (!data)
				return false;
			
			return it->second.fSerialiseComponent(IDC, IDE, *data);
		}
		
		bool World::deserialiseComponentInEntity(IDEntity const IDE, conv::JSON::Obj const &target)
		{
			const auto typeComponent(target.getInt("type"));
			const auto boolActive(target.getBool("active"));
			
			const bool active{(boolActive) ? *boolActive : true};
		
			if (typeComponent)
			{
				const ecs::IDComponent ID  (m_genIDComponent.next());
				const auto             type(static_cast<ecs::IDTypeComponent>(*typeComponent));
				
				const auto it(m_dataPerTypeComponent.find(type));
				if (it != m_dataPerTypeComponent.end())
				{
					if (auto data = target.getObject("data"))
						return it->second.fDeserialiseComponent(ID, IDE, active, *data);
					else
					{
						conv::JSON::Obj dummy;
						return it->second.fDeserialiseComponent(ID, IDE, active, dummy);
					}
				}
			}

			return false;
		}
		
		bool World::deserialiseExistingComponentInEntity
		(
			const IDEntity        &IDE,
			const IDComponent     &IDC,
			const IDTypeComponent &IDT,
			const conv::JSON::Obj &target
		)
		{	
			const auto it(m_dataPerTypeComponent.find(IDT));
			
			if (it != m_dataPerTypeComponent.end())
				return it->second.fDeserialiseExistingComponent(IDC, IDE, target);

			return false;
		}
		
		VtableScript *World::vtableScript(const res::IDResource &script)
		{
			auto res(app().res().get<res::Script>(script));
			
			if (!res)
			{
				log::err("Karhu")
					<< "Failed to access scriptable script resource with ID "
					<< static_cast<int>(script)
					<< ": invalid scriptable script resource";
				
				return nullptr;
			}
			
			auto it(m_vtablesScripts.find(res->module().GetName()));
			
			if (it != m_vtablesScripts.end())
				return it->second.get();
			
			VtableScript vtable;
			
			const auto r(app().scr().loadDerivedTypeWithFactoryAndOptionalMethodsFromModule
			(
				res->module(),
				"ecs::IScriptable",
				vtable.type,
				"",
				vtable.factory,
				{
					{
						"void create(ecs::Scriptable &, app::App &)",
						vtable.create
					},
					{
						"void active(ecs::Scriptable &, app::App &)",
						vtable.active
					},
					{
						"void inactive(ecs::Scriptable &, app::App &)",
						vtable.inactive
					},
					{
						"void input(ecs::Scriptable &, app::App &, const inp::EInput &, const float, const float, const float)",
						vtable.input
					},
					{
						"void updateBeforeRender(ecs::Scriptable &, app::App &, const float, const float, const float)",
						vtable.updateBeforeRender
					},
					{
						"void updateAfterRender(ecs::Scriptable &, app::App &, const float, const float, const float)",
						vtable.updateAfterRender
					},
					{
						"void message(ecs::Scriptable &, app::App &, const ecs::IDEntity, const std::string &in, ref @)",
						vtable.message
					}
				}
			));
			
			if (!r.success)
			{
				log::err("Karhu")
					<< "Failed to access scriptable script resource with ID "
					<< static_cast<int>(script)
					<< ": "
					<< r.error;
				
				return nullptr;
			}
			
			/// @todo: Should we do this, might not doing this be the source of sporadic failures on web? But the AS game example doesn't do this, and there are no sporadic failures on non-web...
			/// @todo: Where/when/if to release?
//			if (vtable.active)             vtable.active->AddRef();
//			if (vtable.inactive)           vtable.inactive->AddRef();
//			if (vtable.input)              vtable.input->AddRef();
//			if (vtable.updateBeforeRender) vtable.updateBeforeRender->AddRef();
//			if (vtable.updateAfterRender)  vtable.updateAfterRender->AddRef();

			return m_vtablesScripts.emplace
			(
				res->module().GetName(),
				std::make_unique<VtableScript>(std::move(vtable))
			).first->second.get();
		}
		
		void World::destroyVtableScript(const char *nameModule)
		{
			auto it(m_vtablesScripts.find(nameModule));
			
			if (it != m_vtablesScripts.end())
				m_vtablesScripts.erase(it);
		}
		
		void *World::RefbankCustom::getPointerForReference(Reftracker const &tracker)
		{
			return world.componentInEntity
			(
				static_cast<IDComponent>(tracker.identifier),
				static_cast<ReftrackerCustom const &>(tracker).entity
			);
		}
		
		bool World::RefbankCustom::onRefcountReachedZero(Reftracker const &/*tracker*/)
		{
			// Components do not get removed when not referenced.
			return false;
		}
		
		// Start from 1 so that 0 can mean invalid/no type.
		IDTypeIScriptable World::c_IDTypeIScriptableLast{1};
	}
}
