/**
 * @author		Ava Skoog
 * @date		2019-05-27
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_ECS_SCRIPTING_H_
	#define KARHU_APP_ECS_SCRIPTING_H_

	#include <string>

	class asITypeInfo;
	class asIScriptFunction;

	namespace karhu
	{
		namespace ecs
		{
			struct VtableScript
			{
				asITypeInfo *type{nullptr};
				
				asIScriptFunction
					*factory           {nullptr},
					*create            {nullptr},
					*active            {nullptr},
					*inactive          {nullptr},
					*input             {nullptr},
					*updateBeforeRender{nullptr},
					*updateAfterRender {nullptr},
					*message           {nullptr};
			};
		}
	}
#endif
