/**
 * @author		Ava Skoog
 * @date		2019-05-23
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_ECS_SYSTEMS_SYSTEM_SCRIPTABLE_H_
	#define KARHU_APP_ECS_SYSTEMS_SYSTEM_SCRIPTABLE_H_

	#include <karhu/app/ecs/common.hpp>
	#include <karhu/app/ecs/System.hpp>
	#include <karhu/app/ecs/components/Scriptable.hpp>
	#include <karhu/conv/serialise.hpp>

	namespace karhu
	{
		namespace inp
		{
			class EInput;
		}
		
		namespace ecs
		{
			class SystemScriptableBeforeRender : public System<Scriptable>
			{
				protected:
					void performUpdateComponents
					(
						      app::App              &app,
						      ecs::Pool<Scriptable> &components,
						const float                  dt,
						const float                  dtFixed,
						const float                  timestep
					) override;
			};
			
			class SystemScriptableAfterRender : public System<Scriptable>
			{
				protected:
					void performUpdateComponents
					(
						      app::App              &app,
						      ecs::Pool<Scriptable> &components,
						const float                  dt,
						const float                  dtFixed,
						const float                  timestep
					) override;
			};
			
			void listenerScriptableCreated (app::App &, const EComponentCreated  &, const float, const float, const float, void *);
			void listenerScriptableActive  (app::App &, const EComponentActive   &, const float, const float, const float, void *);
			void listenerScriptableInactive(app::App &, const EComponentInactive &, const float, const float, const float, void *);
			void listenerScriptableInput   (app::App &, const inp::EInput        &, const float, const float, const float, void *);
		}
	}
#endif
