/**
 * @author		Ava Skoog
 * @date		2019-05-23
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/ecs/systems/SystemScriptable.hpp>
#include <karhu/app/ecs/World.hpp>
#include <karhu/app/inp/events/EInput.hpp>
#include <karhu/app/App.hpp>

#include <map>

namespace karhu
{
	namespace ecs
	{
		void SystemScriptableBeforeRender::performUpdateComponents
		(
				  app::App              &app,
				  ecs::Pool<Scriptable> &components,
			const float                  dt,
			const float                  dtFixed,
			const float                  timestep
		)
		{
			using P = std::pair<Scriptable *, IScriptable *>;
			
			std::multimap<Scriptable::Priority, P> cs;
			
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);
				
				if (!c->activeInHierarchy())
					continue;
				
				if (auto interface = c->interface())
					cs.emplace(c->priority(), P{c, interface});
			}
			
			for (auto &it : cs)
				it.second.second->updateBeforeRender
				(
					*it.second.first,
					app,
					dt,
					dtFixed,
					timestep
				);
		}
		
		void SystemScriptableAfterRender::performUpdateComponents
		(
				  app::App              &app,
				  ecs::Pool<Scriptable> &components,
			const float                  dt,
			const float                  dtFixed,
			const float                  timestep
		)
		{
			using P = std::pair<Scriptable *, IScriptable *>;
			
			std::multimap<Scriptable::Priority, P> cs;
			
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);
				
				if (!c->activeInHierarchy())
					continue;
				
				if (auto interface = c->interface())
					cs.emplace(c->priority(), P{c, interface});
			}
			
			for (auto &it : cs)
				it.second.second->updateAfterRender
				(
					*it.second.first,
					app,
					dt,
					dtFixed,
					timestep
				);
		}
		
		void listenerScriptableCreated
		(
			      app::App          &app,
			const EComponentCreated &e,
			const float,
			const float,
			const float,
			      void *
		)
		{
			auto c(app.ecs().componentInEntity<Scriptable>(e.entity()));
			
			if (auto interface = c->interface())
				interface->create(*c, app);
		}
		
		void listenerScriptableActive
		(
			      app::App         &app,
			const EComponentActive &e,
			const float,
			const float,
			const float,
			      void *
		)
		{
			auto c(app.ecs().componentInEntity<Scriptable>(e.entity()));
			
			if (auto interface = c->interface())
				interface->active(*c, app);
		}

		void listenerScriptableInactive
		(
			      app::App           &app,
			const EComponentInactive &e,
			const float,
			const float,
			const float,
			      void *
		)
		{
			auto c(app.ecs().componentInEntity<Scriptable>(e.entity()));
			
			if (auto interface = c->interface())
				interface->inactive(*c, app);
		}
		
		void listenerScriptableInput
		(
			      app::App      &app,
			const inp::EInput &e,
			const float          dt,
			const float          dtFixed,
			const float          timestep,
			      void           *
		)
		{
			auto c(app.ecs().componentInEntity<Scriptable>(e.entity()));
			
			if (!c)
				return;
			
			if (auto interface = c->interface())
				interface->input
				(
					*c,
					app,
					e,
					dt,
					dtFixed,
					timestep
				);
		}
	}
}
