/**
 * @author		Ava Skoog
 * @date		2018-10-21
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_ECS_SYSTEM_H_
	#define KARHU_APP_ECS_SYSTEM_H_

	#include <karhu/app/lib/kenno/System.hpp>
	#include <karhu/app/ecs/World.hpp>
	#include <karhu/app/App.hpp>

	namespace karhu
	{
		namespace ecs
		{
			template<typename TComponent>
			class System : public kenno::System<TComponent>
			{
				protected:
					virtual void performUpdateComponents
					(
						      app::App         &app,
						      Pool<TComponent> &components,
						const float            dt,
						const float            dtFixed,
						const float            timestep
						/// @todo: Should we have an alias for timestamps in seconds?
					)
					{
					}
				
					virtual void performUpdateNoComponents
					(
						      app::App &app,
						const float    dt,
						const float    dtFixed,
						const float    timestep
						/// @todo: Should we have an alias for timestamps in seconds?
					)
					{
					}
				
				private:
					void performUpdate
					(
						      kenno::World                         &world,
						      kenno::Pool::ViewSubpool<TComponent> &components,
						const float                                dt,
						const float                                dtFixed,
						const float                                timestep
					) override
					{
						app::App &app(reinterpret_cast<World *>(&world)->app());
						
						#ifdef KARHU_EDITOR
						if (app::ModeApp::server == app.mode() && /*!app.backend().shouldRepaintEditor() &&*/ !app.backend().shouldRepaintViewportEditor())
							return;
						#endif
						
						performUpdateComponents
						(
							app,
							components,
							dt,
							dtFixed,
							timestep
						);
					}
				
					void performUpdateWithoutComponents
					(
						      kenno::World &world,
						const float        dt,
						const float        dtFixed,
						const float        timestep
					) override
					{
						app::App &app(reinterpret_cast<World *>(&world)->app());
						
						#ifdef KARHU_EDITOR
						if (app::ModeApp::server == app.mode() && /*!app.backend().shouldRepaintEditor() &&*/ !app.backend().shouldRepaintViewportEditor())
							return;
						#endif
						
						performUpdateNoComponents
						(
							app,
							dt,
							dtFixed,
							timestep
						);
					}
			};
			
			class SystemWithoutComponents : public System<kenno::Component>
			{
				public:
					bool updatesComponents() const noexcept override { return false; }
			};
		}
	}
#endif
