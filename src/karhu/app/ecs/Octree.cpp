/**
 * @author		Ava Skoog
 * @date		2021-07-10
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/ecs/Octree.hpp>
#include <karhu/app/ecs/World.hpp>
#include <karhu/app/App.hpp>
#include <karhu/core/log.hpp>

#include <karhu/app/karhuim3d.hpp>

//! @todo: Slett oktregreier når ferdigtesta.
#include <karhu/app/edt/support.hpp>

namespace karhu
{
	namespace ecs
	{
		bool Octree::init(app::App &app, mth::Boxf const &centreAndMinsize)
		{
			m_app = &app;
			
			mth::Vec3f
				sizeNearestPOT{1.0f, 1.0f, 1.0f};
			
			constexpr mth::Scalf
				halflimit{0.5f * std::numeric_limits<mth::Scalf>::max()};
			
			for (mth::Vec3f::length_type i{0}; i < sizeNearestPOT.length(); ++ i)
			{
				while (sizeNearestPOT[i] < centreAndMinsize.size[i])
				{
					if (sizeNearestPOT[i] <= halflimit)
						sizeNearestPOT[i] *= 2.0f;
					else
					{
						log::err("Karhu")
							<< "Requested octree minimum size on "
							<< ('x' + static_cast<char>(i))
							<< " axis too large: "
							<< centreAndMinsize.size[i];
						
						return false;
					}
				}
			}
			
			m_root.bounds.min = centreAndMinsize.centre - sizeNearestPOT * 0.5f;
			m_root.bounds.max = centreAndMinsize.centre + sizeNearestPOT * 0.5f;
			m_root.box.centre = centreAndMinsize.centre;
			m_root.box.size   = sizeNearestPOT;
			
			//! @todo: Få componentsInActiveScenes til å funka med baseklasse.
			//! @todo: Change to Renderdata when done with Octree and Ref has been set up properly.
			auto const
				components(app.ecs().componentsInActiveScenes<gfx::Renderdata3D>());
			
			Entitydatas
				datas;
			
			//! @todo: Change to Renderdata when done with Octree and Ref has been set up properly.
			for (gfx::Renderdata3D const *const component : components)
			{
				ecs::IDEntity const
					entity{component->entity()};
				
				datas.emplace
				(
					entity,
					Entitydata
					{
						app.ecs().refComponentInEntity<gfx::Transform>(entity),
						//! @todo: Få Ref til å funka med baseklasse.
						//! @todo: Kanskje versjon av refComponentInEntity som tek direkte peikar til komponent.
						//! @todo: Change to Renderdata when done with Octree and Ref has been set up properly.
						app.ecs().refComponentInEntity<gfx::Renderdata3D>(component->identifier(), entity)
					}
				);
			}
			
log::msg("DEBUG") << "oct datas x " << datas.size();
			
			insertEntities(m_root, std::move(datas));

int fin = 0;
std::function<int (Octant const &)> countfin;
countfin = [&countfin](Octant const &oc) -> int {
	int f = (int)oc.entities.size();
	if (oc.children)
		for (auto const &c : *oc.children)
			f += countfin(c);
	return f;
};
fin = countfin(m_root);

log::msg("DEBUG") << "oct final x " << fin;
			
			return true;
		}
		
		void Octree::update()
		{
			bool
				changed{false};
			
			if (!m_entitiesToAdd.empty())
			{
				changed = true;
				
				Entitydatas
					datas;
			
				for (ecs::IDEntity const entity : m_entitiesToAdd)
					datas.emplace
					(
						entity,
						Entitydata
						{
							m_app->ecs().refComponentInEntity<gfx::Transform>(entity),
							m_app->ecs().refComponentInEntity<gfx::Renderdata3D>(entity)
						}
					);
				
				if (!datas.empty())
					insertEntities(m_root, std::move(datas));
				
				m_entitiesToAdd.clear();
			}
			
			if (!m_entitiesToRemove.empty())
			{
				changed = true;
				
				for (ecs::IDEntity const entity : m_entitiesToRemove)
				{
					auto const
						it(m_octantsByEntity.find(entity));
					
					if (it == m_octantsByEntity.end())
						continue;
					
					auto const
						jt(it->second->entities.find(entity));
					
					if (jt != it->second->entities.end())
					{
						it->second->entities.erase(jt);
						
						if (it->second->entities.empty())
							it->second->parent->active &= ~(1 << it->second->index);
						
//						if (!static_cast<bool>(it->second->parent->active))
//							it->second->parent
					}
					
					m_octantsByEntity.erase(it);
				}
				
				m_entitiesToRemove.clear();
			}
			
			if (!m_entitiesToUpdate.empty())
			{
				changed = true;
				
				for (ecs::IDEntity const entity : m_entitiesToUpdate)
				{
					auto const
						it(m_octantsByEntity.find(entity));
					
					if (it == m_octantsByEntity.end())
						continue; //! @todo: Call addEntity?
				
					Octant
						*octant{it->second};
					
					auto const
						jt(octant->entities.find(entity));
					
					//! @todo: Is this all that's needed? Should we assert on this?
					if (jt == octant->entities.end())
					{
						m_octantsByEntity.erase(it);
						continue;
					}
				
					Entitydata
						&data{jt->second};
					
					if (!data.renderdata || !data.transform)
						//! @todo: Should we remove it?
						continue;
					
					mth::Bounds3f const
						AABB{data.renderdata->AABB(data.transform->global())};
					
					// gå opp og finn fyrste forelder inkl noverande oktant som held eininga
					while (octant->parent && !octant->bounds.contains(AABB))
						octant = octant->parent;
				
					// gå ned igjen så langt som mogeleg, kan bruka insertEntities igjen her?
					if (octant != it->second)
					{
						Entitydatas
							datas;
						
						datas.emplace(jt->first, std::move(data));
						
						it->second->entities.erase(jt);
						
						if (it->second->entities.empty())
							it->second->parent->active &= ~(1 << it->second->index);
						
						m_octantsByEntity.erase(it);
						
						insertEntities(*octant, std::move(datas));
					}
				}
				
				m_entitiesToUpdate.clear();
			}
			
			//! @todo: Update active octants etc if empty now
//			if (changed)
//				refresh(m_root);
		}
		
		void Octree::addEntity(ecs::IDEntity const entity)
		{
			m_entitiesToAdd.emplace(entity);
		}
		
		void Octree::removeEntity(ecs::IDEntity const entity)
		{
			m_entitiesToRemove.emplace(entity);
		}
		
		void Octree::updateEntity(ecs::IDEntity const entity)
		{
			m_entitiesToUpdate.emplace(entity);
		}
		
		std::pair<std::ptrdiff_t, std::ptrdiff_t> Octree::entityDepthAndIndexInOctree
		(
			ecs::IDEntity const entity
		) const
		{
			auto const
				it(m_octantsByEntity.find(entity));
			
			if (it == m_octantsByEntity.end())
				return {-1, -1};
			
			return
			{
				static_cast<std::ptrdiff_t>(it->second->depth),
				static_cast<std::ptrdiff_t>(it->second->index)
			};
		}
		
		bool Octree::entityIntersectsFrustum(ecs::IDEntity const entity, gfx::Frustum const &frustum) const
		{
			auto const
				it(m_octantsByEntity.find(entity));
			
			if (it == m_octantsByEntity.end())
				//! @todo: Temporary octree so Renderdata2D doesn't get culled; figure out what to do here once 2D is in place.
				return true;//false;
			
			if (!it->second->parent)
				return true;
			
			if (!frustum.intersects(it->second->bounds))
				return false;
			
			auto const
				jt(it->second->entities.find(entity));
			
			if (jt == it->second->entities.end())
				return false; //! @todo: Assert?
			
			mth::Bounds3f const
				AABB{jt->second.renderdata->AABB(jt->second.transform->global())};
			
			return frustum.intersects(AABB);
		}
		
		void Octree::insertEntities
		(
			Octant      &octant,
			Entitydatas  datas
		)
		{
			auto const
				&children(octant.children);
			
			auto const
				subbounds([&octant](mth::Vec3f const &offset) -> mth::Bounds3f
				{
					mth::Vec3f const
						min{octant.bounds.min + octant.box.size * 0.5f * offset};
					
					return {min, min + octant.box.size * 0.5f};
				});
			
			std::array<mth::Bounds3f, 8> const
				boundsPerOctant
				{
					((children) ? (*children)[0].bounds : subbounds({0.0f, 0.0f, 0.0f})),
					((children) ? (*children)[1].bounds : subbounds({1.0f, 0.0f, 0.0f})),
					((children) ? (*children)[2].bounds : subbounds({0.0f, 0.0f, 1.0f})),
					((children) ? (*children)[3].bounds : subbounds({1.0f, 0.0f, 1.0f})),
					((children) ? (*children)[4].bounds : subbounds({0.0f, 1.0f, 0.0f})),
					((children) ? (*children)[5].bounds : subbounds({1.0f, 1.0f, 0.0f})),
					((children) ? (*children)[6].bounds : subbounds({0.0f, 1.0f, 1.0f})),
					((children) ? (*children)[7].bounds : subbounds({1.0f, 1.0f, 1.0f}))
				};
			
			std::array<Entitydatas, 8>
				datasPerOctant;
			
			for (auto it(datas.begin()); it != datas.end();)
			{
				if (!it->second.renderdata || !it->second.transform)
				{
					//! @todo: Should it be removed?
				log::err("DEBUG") << "manglar rd eller tf";
				 	++ it;
					continue;
				}
				
				///! @todo: Make AABB take Ref?
				mth::Bounds3f const
					AABB{it->second.renderdata->AABB(it->second.transform->global())};
				
				//if (octant.bounds.contains(AABB))
				{
					bool
						found{false};
					
					for (std::size_t i{0}; i < boundsPerOctant.size(); ++ i)
					{
						if (boundsPerOctant[i].contains(AABB))
						{
							datasPerOctant[i].emplace(it->first, std::move(it->second));
							it = datas.erase(it);
							found = true;
							octant.active |= (1 << i);
							break;
						}
					}
					
					if (found)
						continue;
				}
				
				++ it;
			}
			
			if (Byte(0) != octant.active)
			{
				if (!octant.children)
				{
					octant.children = std::make_unique<Octants>();
					
					for (std::size_t i{0}; i < octant.children->size(); ++ i)
					{
						(*octant.children)[i].depth      = octant.depth + 1;
						(*octant.children)[i].index      = i;
						(*octant.children)[i].parent     = &octant;
						(*octant.children)[i].box.centre = boundsPerOctant[i].centre();
						(*octant.children)[i].box.size   = boundsPerOctant[i].size();
						(*octant.children)[i].bounds     = std::move(boundsPerOctant[i]);
					}
				}
				
				for (std::size_t i{0}; i < datasPerOctant.size(); ++ i)
				{
					if (datasPerOctant[i].empty())
						continue;
					
					insertEntities((*octant.children)[i], std::move(datasPerOctant[i]));
				}
			}
			
			if (octant.entities.empty())
				octant.entities = std::move(datas);
			else //! @todo: Is this the best way to insert the range?
				for (auto &it : datas)
					octant.entities[it.first] = std::move(it.second);
			
			for (auto &it : octant.entities)
				m_octantsByEntity[it.first] = &octant;
		}
		
		void Octree::draw(app::App &app) const
		{
			/*
			//! @todo: Shouldn't be necessary to do this; probably want to create a common Im3d interface between editor and regular app anyway, some debug class not tied to either App or Editor.
			if (!app.win().top())
				return;
			
			Im3d::PushDrawState();
			drawOctant(app, m_root);
			Im3d::PopDrawState();
			*/
		}
		
		void Octree::drawOctant(app::App &app, Octant const &octant) const
		{
			mth::Vec3f const
				&min{octant.bounds.min},
				&max{octant.bounds.max};
			
			Im3d::SetColor({1.0f, 0.0f, 0.0f, 0.95f});
			Im3d::SetSize(10.0f);
			
			Im3d::DrawAlignedBox({min.x, min.y, min.z}, {max.x, max.y, max.z});
			
			Im3d::SetColor({1.0f, 1.0f, 1.0f, 1.0f});
			Im3d::SetSize(3.0f);
			for (auto const &it : octant.entities)
			{
				//! @todo: Denne funksjonen AABB burde bruka Ref no… Og injiser app istf som parameter?
				mth::Bounds3f const
					AABB{it.second.renderdata->AABB(it.second.transform->global())};
				
				mth::Vec3f const
					&min{AABB.min},
					&max{AABB.max};
				
				Im3d::DrawAlignedBox({min.x, min.y, min.z}, {max.x, max.y, max.z});
			}
			
			if (octant.children)
				for (Octant const &child : *octant.children)
					drawOctant(app, child);
		}
		
		bool Octree::refresh(Octant &octant)
		{
			if (!octant.children)
				octant.active = Byte(0);
			else
				for (std::size_t i{0}; i < octant.children->size(); ++ i)
					if (!refresh((*octant.children)[i]))
						octant.active &= ~(1 << i);
			
			if (!static_cast<bool>(octant.active))
				octant.children.reset();
			
			return (static_cast<bool>(octant.active) || !octant.entities.empty());
		}
		
//! @todo: Slett oktregreier når ferdigtesta.
#if KARHU_EDITOR_SERVER_SUPPORTED
static int s_depth=0;
static std::string s_s;
static gfx::Frustum s_frustum;
void Octree::drawInEditor(edt::Editor &editor, app::App &app) const
{
static bool draw{false}; if (app.inp().keyPressed(inp::Key::six)) draw = !draw;
if(!draw)return;
	gfx::Camera *cam{nullptr};
	gfx::Transform *camtr{nullptr};
	
	//! @todo: Hardcoded ID, will break if changes happen, just for octree debugging during implementation.
	auto const cameras(app.ecs().componentsInActiveScenes<gfx::Camera>());
	for (auto const camera : cameras)
	{
		if (13 != camera->entity())
			continue;
		
		cam = camera;
		camtr = app.ecs().componentInEntity<gfx::Transform>(cam->entity());
	}
	
	s_depth = 0;
	drawOctantInEditor(editor, app, m_root, true);
	
	if (!cam || !camtr)
		return;
	
	editor.pushColour({0.5f, 0.5f, 0.5f, 0.9f});
	editor.pushThickness(5.0f);
	
	s_frustum = cam->frustum(*camtr);
	
	//! @todo: Make this a method of Frustum so that it can be reused, also in shadowmap code?
	
	mth::Scalf const
		width {static_cast<mth::Scalf>(cam->viewport().x)},
		height{static_cast<mth::Scalf>(cam->viewport().y)},
		near  {cam->projection().near},
		far   {cam->projection().far},
		tanHor{cam->projection().FOV * 0.5f},
		tanVer{tanHor * (height / width)},
		xNear {near * tanHor},
		xFar  {far * tanHor},
		yNear {near * tanVer},
		yFar  {far * tanVer};

	std::array<mth::Vec4f, 8> psfrustum;

	psfrustum[0] = { xNear,  yNear, near, 1.0f};
	psfrustum[1] = {-xNear,  yNear, near, 1.0f};
	psfrustum[2] = {-xNear, -yNear, near, 1.0f};
	psfrustum[3] = { xNear, -yNear, near, 1.0f};
	psfrustum[4] = { xFar,   yFar,  far, 1.0f};
	psfrustum[5] = {-xFar,   yFar,  far, 1.0f};
	psfrustum[6] = {-xFar,  -yFar,  far, 1.0f};
	psfrustum[7] = { xFar,  -yFar,  far, 1.0f};
	
	mth::Mat4f const
		transformCam{camtr->matrixGlobal()};

	for (std::size_t i{0}; i < psfrustum.size(); ++ i)
		psfrustum[i] = transformCam * psfrustum[i];
	
	for (std::size_t i{0}; i < 4; ++ i)
	{
		editor.lineInViewport(mth::Vec3f{psfrustum[i]}, mth::Vec3f{psfrustum[(i == 3) ? 0 : (i + 1)]});
		editor.lineInViewport(mth::Vec3f{psfrustum[i]}, mth::Vec3f{psfrustum[i + 4]});
	}
	
	for (std::size_t i{4}; i < 8; ++ i)
		editor.lineInViewport(mth::Vec3f{psfrustum[i]}, mth::Vec3f{psfrustum[(i == 7) ? 4 : (i + 1)]});
	
	editor.popThickness();
	editor.popColour();
}

void Octree::drawOctantForEntityInEditor(edt::Editor &editor, app::App &app, ecs::IDEntity const entity) const
{
static bool draw{false}; if (app.inp().keyPressed(inp::Key::eight)) draw = !draw;
if(!draw)return;
	auto const
		it(m_octantsByEntity.find(entity));
	
	if (it == m_octantsByEntity.end())
		return;
	
	drawOctantInEditor(editor, app, *it->second, false);
	
	auto const
		jt(it->second->entities.find(entity));
	
	if (jt == it->second->entities.end())
		return;
	
	mth::Bounds3f const
		AABB{jt->second.renderdata->AABB(jt->second.transform->global())};

	editor.pushColour({0.25f, 1.0f, 0.0f, 0.9f});
	editor.pushThickness(7.5f);

	editor.boxInViewport(AABB.min, AABB.max);
}

void Octree::drawOctantInEditor(edt::Editor &editor, app::App &app, Octant const &octant, bool const recurse) const
{
//	s_s.clear();
//	for (int i{0}; i < s_depth; ++ i)
//	s_s+="->";
//	s_s+="oc";
//	editor.text(s_s);

bool const intersects{s_frustum.intersects(octant.bounds)};
	
if (!recurse || !octant.children || !static_cast<bool>(octant.active))
{
	if (!recurse)
	{
		editor.pushColour({1.0f, 0.0f, 0.0f, 0.95f});
		editor.pushThickness(10.0f);
	}
	else if (intersects)
	{
		editor.pushColour({1.0f, 0.75f, 0.75f, 0.5f});
		editor.pushThickness(4.0f);
	}
	else
	{
		editor.pushColour({0.5f, 0.0f, 0.75f, 0.1f});
		editor.pushThickness(1.0f);
	}
	
	editor.boxInViewport(octant.bounds.min, octant.bounds.max);
	editor.popThickness();
	editor.popColour();
}
	if (recurse)
	{
		if (intersects)
		{
			editor.pushColour({1.0f, 1.0f, 0.75f, 0.5f});
			editor.pushThickness(2.5f);
		}
		else
		{
			editor.pushColour({1.0f, 1.0f, 1.0f, 0.1f});
			editor.pushThickness(1.0f);
		}
		
		for (auto const &it : octant.entities)
		{
			//! @todo: Denne funksjonen AABB burde bruka Ref no… Og injiser app istf som parameter?
			mth::Bounds3f const
				AABB{it.second.renderdata->AABB(it.second.transform->global())};
			
			editor.boxInViewport(AABB.min, AABB.max);
		}
		editor.popThickness();
		editor.popColour();
	}

	if (recurse)
		if (octant.children && static_cast<bool>(octant.active))
			for (Octant const &child : *octant.children)
				drawOctantInEditor(editor, app, child, true);
}
#endif
	}
}
