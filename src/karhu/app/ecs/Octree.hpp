/**
 * @author		Ava Skoog
 * @date		2021-07-10
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_ECS_OCTREE_H_
	#define KARHU_APP_ECS_OCTREE_H_

	//! @todo: Change to Renderdata when done with Octree and Ref has been set up properly.
	#include <karhu/app/gfx/components/Renderdata3D.hpp>
	#include <karhu/app/gfx/components/Transform.hpp>
	#include <karhu/app/ecs/common.hpp>
	#include <karhu/app/ecs/Ref.hpp>
	#include <karhu/conv/maths.hpp>
	#include <karhu/core/Byte.hpp>

	#include <unordered_map>
	#include <unordered_set>
	#include <array>
	#include <memory>

//! @todo: Slett oktregreier når ferdigtesta.
#include <karhu/app/edt/support.hpp>
#if KARHU_EDITOR_SERVER_SUPPORTED
	#include <karhu/app/edt/Editor.hpp>
#endif

	namespace karhu
	{
		namespace app { class App; }
		namespace gfx { class Frustum; }
		
		namespace ecs
		{
			// Når transform endrar seg, send hending
			// til den som handterer okttreet eller la treet sjå endringsflagg,
			// sånn at treet kan sjå om eininga må flyttast til anna grein;
			// må gjera noko liknande med RD som sjekkar om AABB endrar storleik.

			class Octree
			{
				public:			
					bool init(app::App &app, mth::Boxf const &centreAndMinsize);
					void update();
				
					void addEntity(ecs::IDEntity const);
					void removeEntity(ecs::IDEntity const);
					void updateEntity(ecs::IDEntity const);
				
					std::pair<std::ptrdiff_t, std::ptrdiff_t> entityDepthAndIndexInOctree(ecs::IDEntity const) const;
					bool entityIntersectsFrustum(ecs::IDEntity const, gfx::Frustum const &) const;
				
					void draw(app::App &) const;
				
			//! @todo: Slett oktregreier når ferdigtesta.
			#if KARHU_EDITOR_SERVER_SUPPORTED
			void drawInEditor(edt::Editor &, app::App &) const;
			void drawOctantForEntityInEditor(edt::Editor &, app::App &, ecs::IDEntity const) const;
			#endif
				
				private:
					struct Octant;
					struct Entitydata;
				
					using Octants     = std::array<Octant, 8>;
					using Entitydatas = std::unordered_map<ecs::IDEntity, Entitydata>;
					using Entityqueue = std::unordered_set<ecs::IDEntity>;
				
					void insertEntities(Octant &, Entitydatas);
					bool refresh(Octant &);
				
					void drawOctant(app::App &, Octant const &) const;
				
			//! @todo: Slett oktregreier når ferdigtesta.
			#if KARHU_EDITOR_SERVER_SUPPORTED
			void drawOctantInEditor(edt::Editor &, app::App &, Octant const &, bool const recurse = false) const;
			#endif
				
				private:
					struct Entitydata
					{
						ecs::Ref<gfx::Transform>  transform;
						//! @todo: Change to Renderdata when done with Octree and Ref has been set up properly.
						ecs::Ref<gfx::Renderdata3D> renderdata;
					};
				
					struct Octant
					{
						std::size_t
							depth{0},
							index;
						
						mth::Bounds3f
							bounds{};
						
						mth::Boxf
							box{};

						Byte
							active{};
					
						Octant
							*parent{nullptr};

						std::unique_ptr<Octants>
							children;

						Entitydatas
							entities;
					} m_root;
				
					std::unordered_map<ecs::IDEntity, Octant *>
						m_octantsByEntity;
				
					Entityqueue
						m_entitiesToAdd,
						m_entitiesToRemove,
						m_entitiesToUpdate;
				
					app::App
						*m_app{nullptr};
			};
		}
	}
#endif
