/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_WINDOW_WINDOW_H_
	#define KARHU_WINDOW_WINDOW_H_

	#include <karhu/app/win/common.hpp>

	#include <string>
	#include <memory>
	#include <functional>

	namespace karhu
	{
		namespace win
		{
			class Context;

			/// @todo: Add window options such as resizable, borderless and so on, for non-game uses.
			/**
			 * Abstract base class for a window.
			 * Windows are created using the backend.
			 */
			class Window
			{
				public:
					bool init
					(
						std::string   const &caption,
						std::uint32_t const  width,
						std::uint32_t const  height,
						Flags         const  flags = Flags::none
					);

					void beginFrame() { performBeginFrame(); }
					void endFrame() { performEndFrame(); }

					/**
					 * Returns the title of the window.
					 *
					 * @return The caption.
					 */
					std::string const &caption() const noexcept { return m_caption; }

					/**
					 * Returns the width in pixels of the window.
					 *
					 * @return The width.
					 */
					std::uint32_t width() const noexcept { return m_width; }

					/**
					 * Returns the height in pixels of the window.
					 *
					 * @return The height.
					 */
					std::uint32_t height() const noexcept { return m_height; }
				
					void scale(float const width, float const height);
					float scaleWidth() const noexcept { return m_scaleWidth; }
					float scaleHeight() const noexcept { return m_scaleHeight; }

					Flags flags() const noexcept { return m_flags; }

					void context(win::Context *const context) { m_context = context; }
					win::Context *context() noexcept { return m_context; }
					win::Context const *context() const noexcept { return m_context; }
				
					bool valid() const noexcept { return m_initialised; }
				
					/**
					 * Returns the window's internal handle.
					 */
					virtual void *handle() const noexcept = 0;

					/**
					 * Implementations should call this when the size of
					 * the window changes to update the variables.
					 *
					 * @param width  The new width in pixels.
					 * @param height The new height in pixels.
					 */
					void notifyChangeSize(std::uint32_t const width, std::uint32_t const height);

					virtual ~Window();
				
				public:
					std::function<void(float const dt, float const dtFixed, float const timestep)> callbackUpdate;

				protected:
					virtual void performBeginFrame() = 0;
					virtual void performEndFrame() = 0;

					virtual bool performInit
					(
						std::string   const &caption,
						std::uint32_t const  width,
						std::uint32_t const  height,
						Flags                flags
					) = 0;
					
					virtual void onChangeSize(std::uint32_t const width, std::uint32_t const height) = 0;

				protected:
					bool
						m_initialised{false};
				
					std::string
						m_caption;
				
					std::uint32_t
						m_width,
						m_height;
				
					float
						m_scaleWidth {1.0f},
						m_scaleHeight{1.0f};
				
					Flags
						m_flags{Flags::none};
				
				private:
					win::Context
						*m_context{nullptr};
			};
		}
	}
#endif
