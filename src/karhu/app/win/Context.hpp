/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_WINDOW_CONTEXT_H_
	#define KARHU_WINDOW_CONTEXT_H_

	#include <karhu/app/win/common.hpp>
	#include <karhu/core/log.hpp>

	namespace karhu
	{
		namespace win
        {
			class Window;
			
			class Context
			{
				public:
					bool init(const Window &window);

					virtual void *handle() const { return nullptr; }

					virtual ~Context() = default;
				
				protected:
					virtual bool performInit(const Window &window) = 0;
			};
		}
	}
#endif
