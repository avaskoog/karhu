/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_WINDOW_H_
	#define KARHU_WINDOW_H_

	#include <karhu/core/macros.hpp>
	#include <cstdint>

	namespace karhu
	{
		namespace win
        {
			enum class Flags : std::int32_t
			{
				none = 0,
				
				// We only support OpenGL at the moment,
				// but let's leave some room for the future.
				graphicsOpenGL  = 1 << 0,
				graphicsVulkan  = 1 << 1,
				graphicsMetal   = 1 << 2,
				graphicsDirectX = 1 << 3,

				resizable  = 1 << 4,
				fullscreen = 1 << 5,
				fillscreen = 1 << 6,
				highDPI    = 1 << 7
			};
			
			karhuBITFLAGGIFY(Flags)
			
			/// @todo: Add audio flags.
		}
	}
#endif
