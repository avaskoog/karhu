/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_WINDOW_SUBSYSTEM_WINDOW_H_
	#define KARHU_WINDOW_SUBSYSTEM_WINDOW_H_

	#include <karhu/app/Subsystem.hpp>
	#include <karhu/app/win/Window.hpp>
	#include <karhu/app/win/Context.hpp>
	#include <karhu/core/log.hpp>
	#include <karhu/core/meta.hpp>

	#include <karhu/app/karhuimgui.hpp>

	#include <string>
	#include <vector>
	#include <memory>
	#include <functional>
	#include <type_traits>

	namespace karhu
	{
		namespace win
        {
			/**
			 * Manages all the windows of a backend and allows the backend to create and
			 * manage new ones (but you have to go through the backend to create windows).
			 */
			class SubsystemWindow : public subsystem::Subsystem
			{
				public:
					/**
					 * Tries to create, but not initialise, a window without any context.
					 *
					 * @return A pointer to the created window on success, or null.
					 */
					Window *createWindow()
					{
						if (auto w = performCreateWindow())
						{
							m_windows.emplace_back(std::move(w));
							return m_windows.back().get();
						}

						log::err("Karhu") << "Failed to create window: construction failed";
						return nullptr;
					}

					/**
					 * Tries to create, but not initialise, a context.
					 *
					 * @return A pointer to the created context on success, or null.
					 */
					Context *createContext()
					{
						if (auto c = performCreateContext())
						{
							m_contexts.push_back(std::move(c));
							return m_contexts.back().get();
						}

						log::err("Karhu") << "Failed to create context: construction failed";
						return nullptr;
					}
					
					void beginFrame() { performBeginFrame(); }
					void endFrame() { performEndFrame(); }
					
					void beginFrame(Window &window) { window.beginFrame(); }
					void endFrame(Window &window) { window.endFrame(); }

					/**
					 * Returns whether there are any windows open.
					 * Generally used as the main loop condition, after creating the main window.
					 *
					 * @return Whether there any any windows open.
					 */
					bool open() const noexcept { return (!m_shutdown && m_windows.size() > 0); }
				
					/**
					 * Returns the main (first) window.
					 *
					 * When the main window closes, the entire application is prompted to close
					 * (that is, open() will no longer return true), even if there are secondary
					 * windows still open.
					 *
					 * @return A pointer to the main window, if one has been created, or null.
					 */
					Window *top() const noexcept { return (open()) ? m_windows[0].get() : nullptr; }

					/**
					 * Returns how many windows are currently open.
					 *
					 * @return The number of open windows.
					 */
					std::size_t count() const { return m_windows.size(); }
					
					/**
					 * Returns the window at the specified index.
					 *
					 * @param index The index of the window.
					 *
					 * @return The window at the specified index, if one exists, or null.
					 */
					Window *get(const std::size_t &index) { return (count() > index) ? m_windows[index].get() : nullptr; }
					
					/**
					 * Closes the window at the specified index.
					 * If no index, or zero, is specified, the main window closes
					 * and thus the entire application shuts down.
					 *
					 * @param index Index of window to close, or the main window if omitted. 
					 */
					void close(const std::size_t &index = 0)
					{
						if (count() > index)
						{
							// Main window?
							if (0 == index)
								m_shutdown = true;
							// Secondary window?
							else
								m_windows.erase(m_windows.begin() + static_cast<std::ptrdiff_t>(index)); /// @todo: Queue this?
						}
					}
				
				public:
					virtual app::FuncImGuiInit   funcImGuiInit()   { return nullptr; }
					virtual app::FuncImGuiFrame  funcImGuiFrame()  { return nullptr; }
					virtual app::FuncImGuiEvent  funcImGuiEvent()  { return nullptr; }
					virtual app::FuncImGuiClipboardGet funcImGuiClipboardGet() { return nullptr; }
					virtual app::FuncImGuiClipboardSet funcImGuiClipboardSet() { return nullptr; }
					virtual app::FuncImGuiDeinit funcImGuiDeinit() { return nullptr; }
				
				protected:
					virtual std::unique_ptr<Window>  performCreateWindow()  = 0;
					virtual std::unique_ptr<Context> performCreateContext() = 0;

					virtual void performBeginFrame() = 0;
					virtual void performEndFrame()   = 0;
				
				private:
					bool m_shutdown{false};

					std::vector<std::unique_ptr<Window>>  m_windows;
					std::vector<std::unique_ptr<Context>> m_contexts;
			};
		}
	}
#endif
