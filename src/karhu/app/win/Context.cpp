/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/win/Context.hpp>
#include <karhu/app/win/Window.hpp>

namespace karhu
{
	namespace win
	{
		bool Context::init(const Window &window)
		{
			if (!window.valid())
			{
				log::err("Karhu") << "Failed to create context: invalid window";
				return false;
			}
			
			if (!static_cast<bool>(Flags::graphicsOpenGL & window.flags()))
			{
				log::err("Karhu") << "Failed to create context: invalid graphics option";
				return false;
			}
			
			return performInit(window);
		}
	}
}
