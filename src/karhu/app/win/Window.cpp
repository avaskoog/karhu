/**
 * @author		Ava Skoog
 * @date		2017-09-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/win/Window.hpp>
#include <karhu/app/win/Context.hpp>

namespace karhu
{
	namespace win
	{
		bool Window::init
		(
			std::string   const &caption,
			std::uint32_t const  width,
			std::uint32_t const  height,
			Flags         const  flags
		)
		{
			if (m_initialised)
				return true;

			m_caption = caption;
			m_width   = width;
			m_height  = height;
			m_flags   = flags;

			return (m_initialised = performInit(caption, width, height, flags));
		}
		
		void Window::scale(float const width, float const height)
		{
			if (width <= 0.0f || height <= 0.0f)
				return; /// @todo: Warning?
			
			m_scaleWidth  = width;
			m_scaleHeight = height;
		}
		
		void Window::notifyChangeSize(std::uint32_t const width, std::uint32_t const height)
		{
			m_width  = width;
			m_height = height;

			onChangeSize(width, height);
		}
		
		Window::~Window() {}
	}
}
