/**
 * @author		Ava Skoog
 * @date		2018-11-01
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_ADAPTER_RESOURCEBANK_H_
	#define KARHU_APP_ADAPTER_RESOURCEBANK_H_

	#include <karhu/res/Bank.hpp>
	#include <karhu/app/tool/SerialiserECS.hpp>

	namespace karhu
	{
		namespace app
		{
			class App;

			class AdapterResourcebank : public res::Bank
			{
				private:
					using Bank::Bank;
				
				public:
					AdapterResourcebank
					(
						std::unique_ptr<conv::AdapterFilesystemRead>  &&read,
						std::unique_ptr<conv::AdapterFilesystemWrite> &&write
					);
				
				public:
					/// @todo: Do we really want this..? Like this..?
					tool::SerialiserECS &serialiserECS() noexcept { return m_serECS; }
				
				private:
					/// @todo: Copying all this over from DatabaseRes; might need to make things nicer eventually.
					tool::SerialiserECS m_serECS;
			};
		}
	}
#endif
