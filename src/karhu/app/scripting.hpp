/**
 * @author		Ava Skoog
 * @date		2019-06-10
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_APP_SCRIPTING_H_
	#define KARHU_APP_SCRIPTING_H_

	#include <karhu/core/log.hpp>
	#include <karhu/core/Buffer.hpp>

	#include <karhu/app/lib/angelscript/add_on/scriptarray/scriptarray.h>
	#include <karhu/app/lib/angelscript/add_on/scripthandle/scripthandle.h>

	#include <vector>
	#include <set>
	#include <cstdint>
	#include <string>

	namespace karhu
	{
		namespace app
		{
			class Scriptmanager;
		}
		
		namespace AS
		{
			/// @todo: Would prefer to get rid of global variable for script engine and just make all functions that need it take it as a parameter instead.
			extern asIScriptEngine *engine;
			
			namespace helper
			{
				extern std::set<int> IDsRegistered;
			}
			
			using Ref    = CScriptHandle;
			using Vector = CScriptArray;
			
			template<typename T>
			int IDForType(const int set = -1)
			{
				static int value{-1};
				
				if (set >= 0 && set != value)
				{
					value = set;
					helper::IDsRegistered.emplace(value);
				}
				
				return value;
			}
			
			template<typename T>
			asITypeInfo *infoForType()
			{
				return engine->GetTypeInfoById(IDForType<T>());
			}
			
			inline std::pair<int, int> IDsForTypeTemplatedWithSubtype(int const IDTypeTemplated)
			{
				int supertype{IDTypeTemplated}, subtype{-1};
				
				if (engine)
				{
					if (asITypeInfo const *const proptype = engine->GetTypeInfoById(IDTypeTemplated))
					{
						engine->SetDefaultNamespace(proptype->GetNamespace());
						asITypeInfo const *const basetype{engine->GetTypeInfoByName(proptype->GetName())};
						engine->SetDefaultNamespace("");
						
						if (basetype)
							supertype = basetype->GetTypeId();
						
						subtype = proptype->GetSubTypeId();
					}
				}
				
				return {supertype, subtype};
			}
			
			inline bool isRegisteredIDForType(const int ID)
			{
				return (helper::IDsRegistered.find(ID) != helper::IDsRegistered.end());
			}
			
			template<typename T> constexpr char const *primname()                noexcept { return ""; }
			template<>           constexpr char const *primname<bool>()          noexcept { return "bool"; }
			template<>           constexpr char const *primname<std::int8_t>()   noexcept { return "int8"; }
			template<>           constexpr char const *primname<std::int16_t>()  noexcept { return "int16"; }
			template<>           constexpr char const *primname<std::int32_t>()  noexcept { return "int"; }
			template<>           constexpr char const *primname<std::int64_t>()  noexcept { return "int64"; }
			template<>           constexpr char const *primname<std::uint8_t>()  noexcept { return "uint8"; }
			template<>           constexpr char const *primname<std::uint16_t>() noexcept { return "uint16"; }
			template<>           constexpr char const *primname<std::uint32_t>() noexcept { return "uint"; }
			template<>           constexpr char const *primname<std::uint64_t>() noexcept { return "uint64"; }
			template<>           constexpr char const *primname<float>()         noexcept { return "float"; }
			template<>           constexpr char const *primname<double>()        noexcept { return "double"; }
			
			template<typename T> constexpr bool isPrimitive()                noexcept { return false; }
			template<>           constexpr bool isPrimitive<bool>()          noexcept { return true; }
			template<>           constexpr bool isPrimitive<std::int8_t>()   noexcept { return true; }
			template<>           constexpr bool isPrimitive<std::int16_t>()  noexcept { return true; }
			template<>           constexpr bool isPrimitive<std::int32_t>()  noexcept { return true; }
			template<>           constexpr bool isPrimitive<std::int64_t>()  noexcept { return true; }
			template<>           constexpr bool isPrimitive<std::uint8_t>()  noexcept { return true; }
			template<>           constexpr bool isPrimitive<std::uint16_t>() noexcept { return true; }
			template<>           constexpr bool isPrimitive<std::uint32_t>() noexcept { return true; }
			template<>           constexpr bool isPrimitive<std::uint64_t>() noexcept { return true; }
			template<>           constexpr bool isPrimitive<float>()         noexcept { return true; }
			template<>           constexpr bool isPrimitive<double>()        noexcept { return true; }
			
			inline constexpr char const *primnameByID(int ID) noexcept
			{
				switch (ID)
				{
					case asTYPEID_VOID:   return "void";
					case asTYPEID_BOOL:   return "bool";
					case asTYPEID_INT8:   return "int8";
					case asTYPEID_INT16:  return "int16";
					case asTYPEID_INT32:  return "int32";
					case asTYPEID_INT64:  return "int64";
					case asTYPEID_UINT8:  return "uint8";
					case asTYPEID_UINT16: return "uint16";
					case asTYPEID_UINT32: return "uint32";
					case asTYPEID_UINT64: return "uint64";
					case asTYPEID_FLOAT:  return "float";
					case asTYPEID_DOUBLE: return "double";
					default:              return "";
				}
			}
			
			inline constexpr bool isPrimitiveID(int ID) noexcept
			{
				switch (ID)
				{
					case asTYPEID_VOID:
					case asTYPEID_BOOL:
					case asTYPEID_INT8:
					case asTYPEID_INT16:
					case asTYPEID_INT32:
					case asTYPEID_INT64:
					case asTYPEID_UINT8:
					case asTYPEID_UINT16:
					case asTYPEID_UINT32:
					case asTYPEID_UINT64:
					case asTYPEID_FLOAT:
					case asTYPEID_DOUBLE:
						return true;
						
					default:
						return false;
				}
			}
			
			template<typename T, typename... Ts>
			static T *factory(Ts... args)
			{
				return new T(std::forward<Ts>(args)...);
			}
			
			template<typename T, typename... Ts>
			static void constructor(void *addr, Ts... args)
			{
				new(addr) T(args...);
			}
			
			template<typename T>
			static T &opAssign(T &self, T const &o)
			{
				self = o;
				return self;
			}
			
			template<typename T>
			static void copyconstructor(void *addr, const T &o)
			{
				new(addr) T{o};
			}
			
			template<typename T>
			static void destructor(void *addr)
			{
				reinterpret_cast<T *>(addr)->~T();
			}
			
			// Helper for inheritance.
			template<typename A, typename B>
			static B *refcast(A *a)
			{
				return (a) ? dynamic_cast<B *>(a) : nullptr;
			}
			
			template<typename T>
			struct RC : public T
			{
				using T::T;
				
				void f_karhuAS_add()     { ++ m_karhuAS_count; }
				void f_karhuAS_release() { if (0 == (-- m_karhuAS_count)) delete this; }
				
				int m_karhuAS_count{1};
			};
			
			template<typename T>
			inline void copyVectorASToSTL(const AS::Vector &src, std::vector<T> &dst)
			{
				dst.resize(src.GetSize());
				
				if (!src.IsEmpty())
					for (asUINT i{0}; i < src.GetSize(); ++ i)
						dst[i] = *static_cast<const T *>(src.At(i));
			}
			
			template<typename T>
			inline void copyVectorSTLToAS(const std::vector<T> &src, AS::Vector &dst)
			{
				dst.Resize(src.size());
				
				if (!src.empty())
					for (asUINT i{0}; i < static_cast<asUINT>(src.size()); ++ i)
						dst.SetValue(i, &src[i]);
			}
			
			namespace helper
			{
				template<typename T>
				inline std::enable_if_t
				<
					(
						!std::is_pointer    <T>{} &&
						!std::is_enum       <T>{} &&
						!std::is_fundamental<T>{}
					),
					void
				>
				setArgForCall(asIScriptContext &context, const asUINT index, T arg)
				{
					static_assert
					(
						std::is_reference<T>{},
						"Cannot pass temporary non-primitive to script call"
					);
					
					context.SetArgAddress
					(
						index,
						static_cast<void *>
						(
							const_cast
							<
								std::remove_const_t
								<
									std::remove_reference_t<T>
								> *
							>
							(&arg)
						)
					);
				}
				
				template<typename T>
				inline std::enable_if_t<std::is_pointer<T>{}, void>
				setArgForCall(asIScriptContext &context, const asUINT index, T arg)
				{
					context.SetArgAddress(index, static_cast<void *>(const_cast<std::remove_const_t<T>>(arg)));
				}
				
				template<typename T>
				inline std::enable_if_t
				<
					(
						std::is_same<std::remove_const_t<std::remove_reference_t<T>>, std::uint8_t>{} ||
						std::is_same<std::remove_const_t<std::remove_reference_t<T>>, std::int8_t>{}
					),
					void
				>
				setArgForCall(asIScriptContext &context, const asUINT index, const T arg)
				{
					context.SetArgByte(index, static_cast<asBYTE>(arg));
				}
				
				template<typename T>
				inline std::enable_if_t
				<
					(
						std::is_same<std::remove_const_t<std::remove_reference_t<T>>, std::uint16_t>{} ||
						std::is_same<std::remove_const_t<std::remove_reference_t<T>>, std::int16_t>{}
					),
					void
				>
				setArgForCall(asIScriptContext &context, const asUINT index, const T arg)
				{
					context.SetArgWord(index, static_cast<asWORD>(arg));
				}
				
				template<typename T>
				inline std::enable_if_t
				<
					(
						std::is_same<std::remove_const_t<std::remove_reference_t<T>>, std::uint32_t>{} ||
						std::is_same<std::remove_const_t<std::remove_reference_t<T>>, std::int32_t>{} ||
						std::is_same<std::remove_const_t<std::remove_reference_t<T>>, bool>{}
					),
					void
				>
				setArgForCall(asIScriptContext &context, const asUINT index, const T arg)
				{
					context.SetArgDWord(index, static_cast<asDWORD>(arg));
				}
				
				template<typename T>
				inline std::enable_if_t
				<
					(
						std::is_same<std::remove_const_t<std::remove_reference_t<T>>, std::uint64_t>{} ||
						std::is_same<std::remove_const_t<std::remove_reference_t<T>>, std::int64_t>{}
					),
					void
				>
				setArgForCall(asIScriptContext &context, const asUINT index, const T arg)
				{
					context.SetArgQWord(index, static_cast<asQWORD>(arg));
				}
				
				template<typename T>
				inline std::enable_if_t<std::is_enum<T>{}, void>
				setArgForCall(asIScriptContext &context, const asUINT index, const T arg)
				{
					setArgForCall(context, index, static_cast<std::underlying_type_t<T>>(arg));
				}
				
				inline void setArgForCall(asIScriptContext &context, const asUINT index, const float arg)
				{
					context.SetArgFloat(index, arg);
				}
				
				inline void setArgForCall(asIScriptContext &context, const asUINT index, const double arg)
				{
					context.SetArgDouble(index, arg);
				}
				
				template<typename... Ts>
				inline std::enable_if_t<(sizeof...(Ts) == 0), void>
				setArgsForCall(asIScriptContext &, const asUINT) {}
				
				template<typename T>
				inline void setArgsForCall
				(
					asIScriptContext &context,
					const asUINT index,
					T arg
				)
				{
					setArgForCall<T>(context, index, arg);
				}
				
				template<typename T, typename... Ts>
				inline std::enable_if_t<(sizeof...(Ts) > 0), void>
				setArgsForCall
				(
					asIScriptContext &context,
					const asUINT index,
					T arg,
					Ts... args
				)
				{
					setArgForCall<T>(context, index, arg);
					setArgsForCall<Ts...>(context, index + 1, args...);
				}
				
				template<typename T>
				inline std::enable_if_t<!std::is_pointer<T>{}, T>
				getRetForCall(asIScriptContext &context)
				{
					return *static_cast<T *>(context.GetAddressOfReturnValue());
				}
				
				template<typename T>
				inline std::enable_if_t<std::is_pointer<T>{}, T>
				getRetForCall(asIScriptContext &context)
				{
					return static_cast<T>(context.GetAddressOfReturnValue());
				}
			}
			
			template<typename R, typename... Ts>
			inline R callr
			(
				asIScriptContext &context,
				asIScriptObject *o,
				asIScriptFunction &f,
				Ts... args
			)
			{
				context.Prepare(&f);

				if (o)
					context.SetObject(o);

				helper::setArgsForCall<Ts...>(context, 0, args...);
				
				R r;

				if (asEXECUTION_FINISHED == context.Execute())
					r = helper::getRetForCall<R>(context);
				
				return r;
			}

			template<typename... Ts>
			inline void call
			(
				asIScriptContext &context,
				asIScriptObject *o,
				asIScriptFunction &f,
				Ts... args
			)
			{
				context.Prepare(&f);

				if (o)
					context.SetObject(o);

				helper::setArgsForCall<Ts...>(context, 0, args...);
				
				context.Execute();
			}
		}
	}
#endif
