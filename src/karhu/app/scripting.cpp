/**
 * @author		Ava Skoog
 * @date		2019-06-15
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/app/scripting.hpp>

namespace karhu
{
	namespace AS
	{
		asIScriptEngine *engine{nullptr};
		
		namespace helper
		{
			std::set<int> IDsRegistered;
		}
	}
}
