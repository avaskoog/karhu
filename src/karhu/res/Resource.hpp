/**
 * @author		Ava Skoog
 * @date		2018-10-29
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_RES_RESOURCE_H_
	#define KARHU_RES_RESOURCE_H_

	#include <karhu/conv/files.hpp>
	#include <karhu/conv/types.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/core/result.hpp>

	#include <cstdint>

	namespace karhu
	{
  		namespace res
  		{
  			/// @todo: Move to a common.hpp that can be used to include only simply things like this without the rest of the bulk.
  			using IDTypeResource = std::uint16_t;
  			using IDResource     = std::uint64_t;
			using IndexResource  = std::int16_t;
			
			class Bank;
			
  			/**
  			 * Common interface for engine and toolkit alike to
  			 * interact with a resource file in the resource directory
  			 * of a Karhu project using filesystem adapters.
  			 *
  			 * Derived types should provide abstracted read and write
  			 * access to everything and not reveal underlying details
  			 * of implementation or storage.
  			 */
  			class Resource
  			{
				public:
					/// @todo: Resource init() seems not to be used; is it needed?
					Status init
					(
						std::vector<std::string> const &extraPathsRelativeToSourcePath = {}
					);
					
					Status load
					(
						char const *filepathRelativeToResourcepath,
						char const *suffix,
						conv::AdapterFilesystemRead &filesystem
					);
				
					Status save
					(
						char const *filepathRelativeToResourcepath,
						char const *suffix,
						conv::AdapterFilesystemWrite &filesystem
					);
				
					IDTypeResource typeResource() const noexcept { return m_typeResource; }
				
					void setIdentifier(IDResource const v) { m_identifier = v; } /// @todo: Figure out some better way of going about this; only the resource database should be allowed to touch the ID, but right now they are in different places...
					IDResource identifier() const noexcept { return m_identifier; }
				
					void serialise(conv::Serialiser &) const;
					void deserialise(conv::Serialiser const &);
				
					virtual ~Resource() = default;
				
				protected:
					/**
					 * Returns whether the derived resource type has
					 * any metadata file to be loaded and saved.
					 *
					 * @return Whether a metadata file exists for this type of resource.
					 */
					virtual bool hasMetadata() const noexcept = 0;
				
					/**
					 * Initialises a resource with default values, based
					 * on any additional paths specified, for the specific
					 * resource type represented by the derived class, so
					 * it can be directly saved afterwards as a new resource.
					 *
					 * @param extraPathsRelativeToSourcePath Additional paths to load files from.
					 *
					 * @return A struct containing the success state as well as an error message on failure.
					 */
					virtual Status performInit
					(
						std::vector<std::string> const &extraPathsRelativeToSourcePath
					) = 0;
				
					/**
					 * Tries to load any files besides the metadata,
					 * which the base class will deal with loading.
					 *
					 * @param metadata                       The loaded metadata, if any, for reading in case its information is necessary.
					 * @param filepathRelativeToResourcepath The resource path relative to the resource directory.
					 * @param suffix                         The metadata file extension for this type of resource.
					 * @param filesystem                     The filesystem adapter for reading.
					 *
					 * @return A struct containing the success state as well as an error message on failure.
					 */
					virtual Status performLoadInAdditionToMetadata
					(
						conv::JSON::Obj const *const metadata,
						char const *filepathRelativeToResourcepath,
						char const *suffix,
						conv::AdapterFilesystemRead &filesystem
					)
					{
						return {true};
					}
				
					/**
					 * Tries to save any files besides the metadata,
					 * which the base class will deal with saving.
					 *
					 * @param metadata                       The serialised metadata, if any, about to be saved, in case modifications are necessary.
					 * @param filepathRelativeToResourcepath The resource path relative to the resource directory.
					 * @param suffix                         The metadata file extension for this type of resource.
					 * @param filesystem                     The filesystem adapter for writing.
					 *
					 * @return A struct containing the success state as well as an error message on failure.
					 */
					virtual Status performSaveInAdditionToMetadata
					(
						conv::JSON::Obj *const metadata,
						char const *filepathRelativeToResourcepath,
						char const *suffix,
						conv::AdapterFilesystemWrite &filesystem
					)
					{
						return {true};
					}
				
					/*
					 * Performs any custom serialisation after the ID has been serialised.
					 *
					 * @param ser The serialiser to serialise to using << and karhuPROP.
					 */
					virtual void performSerialise(conv::Serialiser &) const = 0;
				
					/*
					 * Performs any custom deserialisation after the ID has been deserialised.
					 *
					 * @param ser The deserialiser to deserialise from using >> and karhuPROP.
					 */
					virtual void performDeserialise(conv::Serialiser const &) = 0;
				
				protected:
					Status loadJSON
					(
						conv::JSON::Val &v,
						char const *filepathRelativeToResourcepath,
						conv::AdapterFilesystemRead &filesystem
					);
				
					Status saveJSON
					(
						conv::JSON::Val const &v,
						char const *filepathRelativeToResourcepath,
						conv::AdapterFilesystemWrite &filesystem
					);
				
				private:
					IDTypeResource m_typeResource;
					IDResource     m_identifier;
				
				friend class Bank;
			};
		}
	}
#endif
