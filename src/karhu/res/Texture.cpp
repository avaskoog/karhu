/**
 * @author		Ava Skoog
 * @date		2018-10-29
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/res/Texture.hpp>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace res
	{		
		void Texture::performSerialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuINn("format", m_buffer.format)
				<< karhuINn("type",   m_type)
				<< karhuINn("wrap",   m_modeWrap)
				<< karhuINn("filter", m_modeFilter);
		}

		void Texture::performDeserialise(conv::Serialiser const &ser)
		{
			ser
				>> karhuOUTn("format", m_buffer.format)
				>> karhuOUTn("type",   m_type)
				>> karhuOUTn("wrap",   m_modeWrap)
				>> karhuOUTn("filter", m_modeFilter);
		}
	}
}
