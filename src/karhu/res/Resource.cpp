/**
 * @author		Ava Skoog
 * @date		2018-10-28
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/res/Resource.hpp>
#include <karhu/data/serialise.hpp>
#include <karhu/core/log.hpp>

namespace karhu
{
	namespace res
	{		
		Status Resource::init
		(
			std::vector<std::string> const &extraPathsRelativeToSourcePath
		)
		{
			return performInit(extraPathsRelativeToSourcePath);
		}
		
		Status Resource::load
		(
			const char *filepathRelativeToResourcepath,
			const char *suffix,
			conv::AdapterFilesystemRead &filesystem
		)
		{
			conv::JSON::Val metadata;
			
			if (hasMetadata())
			{
				const auto path(std::string{filepathRelativeToResourcepath} + suffix);
				
				const auto s(loadJSON
				(
					metadata,
					path.c_str(),
					filesystem
				));
				
				if (!s.success)
					return s;
				
				if (!metadata.object())
				{
					std::stringstream s;
					s
						<< "Error deserialising from '"
						<< path
						<< "'; JSON root object missing";
						
					return {false, s.str()};
				}
				
				conv::Serialiser ser{*metadata.object()};
				deserialise(ser);
			}
			
			return performLoadInAdditionToMetadata
			(
				((hasMetadata()) ? metadata.object() : nullptr),
				filepathRelativeToResourcepath,
				suffix,
				filesystem
			);
		}

		Status Resource::save
		(
			const char *filepathRelativeToResourcepath,
			const char *suffix,
			conv::AdapterFilesystemWrite &filesystem
		)
		{
			conv::JSON::Val metadata;
			
			if (hasMetadata())
			{
				metadata = conv::JSON::Obj{};
				conv::Serialiser ser{*metadata.object()};
				serialise(ser);
			}
			
			{
				const auto s(performSaveInAdditionToMetadata
				(
					((hasMetadata()) ? metadata.object() : nullptr),
					filepathRelativeToResourcepath,
					suffix,
					filesystem
				));
				
				if (!s.success)
					return s;
			}
			
			if (hasMetadata())
			{
				const auto path(std::string{filepathRelativeToResourcepath} + suffix);
				const auto s(saveJSON
				(
					metadata,
					path.c_str(),
					filesystem
				));
				
				if (!s.success)
					return s;
			}
			
			return {true};
		}
		
		void Resource::serialise(conv::Serialiser &ser) const
		{
			performSerialise(ser);
		}

		void Resource::deserialise(const conv::Serialiser &ser)
		{
			performDeserialise(ser);
		}
		
		Status Resource::loadJSON
		(
			conv::JSON::Val &v,
			const char *filepathRelativeToResourcepath,
			conv::AdapterFilesystemRead &filesystem
		)
		{
			const auto r(filesystem.readStringFromResource
			(
				filepathRelativeToResourcepath
			));
			
			if (r.success)
			{
				conv::JSON::Parser parser;
				std::stringstream s;
				s << r.value;
				if (const auto root = parser.parse(s))
				{
					v = std::move(*root);
					return {true};
				}
				else
				{
					std::stringstream s;
					s
						<< "Error loading '"
						<< filepathRelativeToResourcepath
						<< "'; JSON parse error: "
						<< parser.error();
					return {false, s.str()};
				}
			}
			else
				return {false, std::move(r.error)};
			
			return {true};
		}
		
		Status Resource::saveJSON
		(
			const conv::JSON::Val &v,
			const char *filepathRelativeToResourcepath,
			conv::AdapterFilesystemWrite &filesystem
		)
		{
			return filesystem.writeStringToResource
			(
				filepathRelativeToResourcepath,
				v.dump().c_str()
			);
		}
	}
}
