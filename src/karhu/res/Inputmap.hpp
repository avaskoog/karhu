/**
 * @author		Ava Skoog
 * @date		2019-01-07
 * @copyright	2017-2023 Ava Skoog
 */

#ifndef KARHU_RES_INPUTMAP_H_
	#define KARHU_RES_INPUTMAP_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/conv/input.hpp>
	#include <karhu/conv/maths.hpp>

	#include <vector>

	namespace karhu
	{
  		namespace res
  		{
			class Inputmap : public Resource
			{
				protected:
					bool hasMetadata() const noexcept override
					{
						return true;
					}
				
					Status performInit
					(
						std::vector<std::string> const &extraPathsRelativeToSourcePath
					) override
					{
						/// @todo: Implement this if necessary.
						return {true};
					}
				
					void performSerialise(conv::Serialiser &) const override;
					void performDeserialise(const conv::Serialiser &) override;
				
				public:
					struct Action
					{
						enum class Valuetype : std::uint8_t
						{
							none,
							integer,
							floating
						};
						
						bool                   used{false};
						inp::Type              type;
						inp::Method            method;
						inp::Scope             scope;
						inp::Invert            invert;
						bool                   normalise;
						mth::Scalf             sensitivity;
						mth::Scalf             deadzone;
						std::vector<inp::Code> codes;
						
						void serialise(conv::Serialiser &) const;
						void deserialise(const conv::Serialiser &);
					};
				
					struct Setup
					{
						enum class Type
						{
							buttons,
							axes
						} type{Type::buttons};
						
						std::vector<std::vector<inp::Code>> codes;
						
						void serialise(conv::Serialiser &) const;
						void deserialise(const conv::Serialiser &);
					};
					
					std::vector<Action> actions;
					std::vector<Setup> setups;
			};
		}
	}
#endif
