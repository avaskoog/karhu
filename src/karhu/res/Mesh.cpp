/**
 * @author		Ava Skoog
 * @date		2018-10-30
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/res/Mesh.hpp>
#include <karhu/core/log.hpp>

namespace karhu
{
	namespace res
	{
		/**
		 * Recalculates everything as necessary to prepare the
		 * mesh for rendering after all of the vertex attributes,
		 * indices and so forth have been set and modified the mesh.
		 * The mesh cannot be drawn until it has been baked.
		 *
		 * @return Whether baking was successful.
		 */
		bool Mesh::bake()
		{
			// At least one LOD level is needed.
			if (m_dataPerLOD.size() == 0)
			{
				log::err("Karhu") << "Failed to bake mesh: at least one LOD level of geometry has to be specified";
				return false;
			}
			
			// Clear out the old buffer.
			m_buffer.clear();
			
			// Validate all geometries and start building the buffer, keeping track of offsets and ranges.
			for (std::size_t i{0}, offset{0}; i < m_dataPerLOD.size(); ++ i)
			{
				auto &g(m_dataPerLOD[i].geometry);

				// At least three vertex positions are required.
				if (g.positions.size() < 3)
				{
					log::err("Karhu") << "Failed to bake mesh: less than 3 vertex positions set for LOD level " << i << '!';
					return false;
				}
				
				// At least three indices have to exist, and the amount have to be a multiple of 3.
				if (g.indices.size() > 0 && g.indices.size() < 3)
				{
					log::err("Karhu") << "Failed to bake mesh: less than 3 indices set for LOD level " << i << '!';
					return false;
				}
				
				// At least three indices have to exist, and the amount have to be a multiple of 3.
				if (g.indices.size() > 0 && 0 != (g.indices.size() % 3))
				{
					log::err("Karhu")
						<< "Failed to bake mesh: index count for LOD level "
						<< i << " (" << g.indices.size() << " is not a multiple of 3";
					return false;
				}

				// If normals have been supplied, the count has to match the position count.
				if (g.normals.size() > 0 && g.normals.size() != g.positions.size())
				{
					log::err("Karhu")
						<< "Failed to bake mesh: vertex normal count for LOD level "
						<< i
						<< " ("
						<< g.normals.size()
						<< ") does not match position count ("
						<< g.positions.size()
						<< ")";
					return false;
				}

				// If UV's have been supplied, the count has to match the position count.
				if (g.UVs.size() > 0 && g.UVs.size() != g.positions.size())
				{
					log::err("Karhu")
						<< "Failed to bake mesh: vertex UV count for LOD level "
						<< i
						<< " ("
						<< g.UVs.size()
						<< ") does not match position count ("
						<< g.positions.size()
						<< ")";
					return false;
				}
				
				// If colours have been supplied, the count has to match the position count.
				if (g.colours.size() > 0 && g.colours.size() != g.positions.size())
				{
					log::err("Karhu")
						<< "Failed to bake mesh: vertex colour count for LOD level "
						<< i
						<< " ("
						<< g.colours.size()
						<< ") does not match position count ("
						<< g.positions.size()
						<< ")";
					return false;
				}
				
				// If jointsets have been supplied, the count has to match the position count.
				if (g.jointsets0.size() > 0 && g.jointsets0.size() != g.positions.size())
				{
					log::err("Karhu")
						<< "Failed to bake mesh: vertex jointset 0 count for LOD level "
						<< i
						<< " ("
						<< g.jointsets0.size()
						<< ") does not match position count ("
						<< g.positions.size()
						<< ")";
					return false;
				}
				
				if (g.jointsets1.size() > 0 && g.jointsets1.size() != g.positions.size())
				{
					log::err("Karhu")
						<< "Failed to bake mesh: vertex jointset 1 count for LOD level "
						<< i
						<< " ("
						<< g.jointsets1.size()
						<< ") does not match position count ("
						<< g.positions.size()
						<< ")";
					return false;
				}
				
				if (g.jointsets2.size() > 0 && g.jointsets2.size() != g.positions.size())
				{
					log::err("Karhu")
						<< "Failed to bake mesh: vertex jointset 2 count for LOD level "
						<< i
						<< " ("
						<< g.jointsets2.size()
						<< ") does not match position count ("
						<< g.positions.size()
						<< ")";
					return false;
				}
				
				if (g.tangents.size() > 0 && g.tangents.size() != g.positions.size())
				{
					log::err("Karhu")
						<< "Failed to bake mesh: vertex tangent count for LOD level "
						<< i
						<< " ("
						<< g.tangents.size()
						<< ") does not match position count ("
						<< g.positions.size()
						<< ")";
					return false;
				}
				
				// If weightsets have been supplied, the count has to match the position count.
				if (g.weightsets0.size() > 0 && g.weightsets0.size() != g.positions.size())
				{
					log::err("Karhu")
						<< "Failed to bake mesh: vertex weightset 0 count for LOD level "
						<< i
						<< " ("
						<< g.weightsets0.size()
						<< ") does not match position count ("
						<< g.positions.size()
						<< ")";
					return false;
				}
				
				if (g.weightsets1.size() > 0 && g.weightsets1.size() != g.positions.size())
				{
					log::err("Karhu")
						<< "Failed to bake mesh: vertex weightset 1 count for LOD level "
						<< i
						<< " ("
						<< g.weightsets1.size()
						<< ") does not match position count ("
						<< g.positions.size()
						<< ")";
					return false;
				}
				
				if (g.weightsets2.size() > 0 && g.weightsets2.size() != g.positions.size())
				{
					log::err("Karhu")
						<< "Failed to bake mesh: vertex weightset 2 count for LOD level "
						<< i
						<< " ("
						<< g.weightsets2.size()
						<< ") does not match position count ("
						<< g.positions.size()
						<< ")";
					return false;
				}
				
				if (g.custom.size() > 0 && g.custom.size() != g.positions.size())
				{
					log::err("Karhu")
						<< "Failed to bake mesh: vertex custom count for LOD level "
						<< i
						<< " ("
						<< g.custom.size()
						<< ") does not match position count ("
						<< g.positions.size()
						<< ")";
					return false;
				}
				
				// Normalise the animation weights if any.
				{
					constexpr std::size_t countSets{3};
					
					std::vector<gfx::Weightset> *weightsets[countSets];
					std::size_t count{0};
					
					if (0 != g.weightsets0.size())
						weightsets[count ++] = &g.weightsets0;
					
					if (0 != g.weightsets1.size())
						weightsets[count ++] = &g.weightsets1;
					
					if (0 != g.weightsets2.size())
						weightsets[count ++] = &g.weightsets2;
					
					if (0 != count)
					{
						for (std::size_t j{0}; j < g.positions.size(); ++ j)
						{
							double v{0.0f};
							
							for (std::size_t k{0}; k < count; ++ k)
								for (int l{0}; l < countSets; ++ l)
									v += (*weightsets[k])[j][l];
							
							if (v != 0.0)
							{
								const double f{1.0 / v};
								
								for (std::size_t k{0}; k < count; ++ k)
									for (int l{0}; l < countSets; ++ l)
										(*weightsets[k])[j][l] *= f;
							}
							else
								for (std::size_t k{0}; k < count; ++ k)
										for (int l{0}; l < countSets; ++ l)
											(*weightsets[k])[j][l] = 1.0f;
						}
					}
				}

				// Add to the buffer.
				m_buffer.geometry.append(g);
				m_buffer.rangesPerLOD.emplace_back(offset, g.indices.size());

				// Update to offset the next geometry properly.
				offset += g.indices.size();
			}

			return (m_baked = performBake());
		}

		void Mesh::reset()
		{
			m_baked = false;
			m_dataPerLOD.clear();
			m_buffer.clear();
			performReset();
		}
	}
}
