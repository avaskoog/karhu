/**
 * @author		Ava Skoog
 * @date		2021-05-30
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_RES_REF_H_
	#define KARHU_RES_REF_H_

	#include <karhu/core/Ref.hpp>
	#include <karhu/res/Resource.hpp>

	namespace karhu
	{
		namespace res
		{
			template<typename T>
			class Ref : public RefBase<T>
			{
				static_assert(std::is_base_of<Resource, T>{}, "Resource ref type must derive from Resource");
				
				protected:
					using RefBase<T>::RefBase;
				
				friend class Bank;
				friend class karhu::Refbank;
			};
		}
	}
#endif
