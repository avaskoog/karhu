/**
 * @author		Ava Skoog
 * @date		2018-10-30
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_RES_SHADER_H_
	#define KARHU_RES_SHADER_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/conv/graphics.hpp>
	#include <karhu/core/meta.hpp>

	namespace karhu
	{
		namespace gfx
		{
			struct Pass;
			class Transform;
		}
		
  		namespace res
  		{
			class Material;
		
			/**
			 * Abstract base class for a shader which is used by materials containing settings.
			 */
			class Shader : public Resource
			{
				public:
					enum class TypeGLSL : std::uint8_t
					{
						vert,
						frag
					};
				
					enum class Format : std::uint8_t
					{
						invalid,
						GL,
						GLES,
						metal
					};
				
					enum class Mode : std::uint8_t
					{
						both,
						single,
						multiple
					};
				
					enum class Type : std::uint8_t
					{
						regular, // A regular shader with few to no special cases.
						texture  // Outputs to one or more textures as a fullscreen quad.
					};

					struct DataSource
					{
						Mode mode;
						std::string name, code;
					};
				
				public:
					static constexpr Format strToFormat(const char *s) noexcept
					{
						if (meta::string::equal(s, "gl"))    return Format::GL;
						if (meta::string::equal(s, "gles"))  return Format::GLES;
						if (meta::string::equal(s, "metal")) return Format::metal;
						return Format::invalid;
					}
				
					static constexpr char const *formatToStr(const Format &v) noexcept
					{
						switch (v)
						{
							case Format::invalid: return "";
							case Format::GL:      return "gl";
							case Format::GLES:    return "gles";
							case Format::metal:   return "metal";
						}
					}
				
					static constexpr char const *suffixForMode(const Mode &v) noexcept
					{
						switch (v)
						{
							case Mode::both:     return "-both";
							case Mode::single:   return "-sing";
							case Mode::multiple: return "-mult";
						}
					}
				
					static constexpr char const *suffixForTypeGLSL(const TypeGLSL &v) noexcept
					{
						switch (v)
						{
							case TypeGLSL::vert: return ".vert";
							case TypeGLSL::frag: return ".frag";
						}
					}
				
					static std::string addHeaderToGLSL
					(
						const Format   &format,
						const Mode     &mode,
						const TypeGLSL &type,
						const char     *source
					);
				
					static std::string addDefaultsToGLSL
					(
						Format   const &      format,
						Mode     const &      mode,
						TypeGLSL const &      type,
						char     const *const source
					);
				
					/// @todo: Add a function to resolve a custom #include directive in GLSL files, where <> loads from a default Karhu directory and "" relative to the shader in the project's resources; would only need to be able to parse comments (to see that the #include isn't in one) and the #include directive itself.
				
				public:
					Type type() const noexcept { return m_type; }
				
					bool addSources(const std::vector<DataSource> &sources)
					{
						return performAddSources(sources);
					}
				
					void reset()
					{
						m_baked = false;
						performReset();
					}

					bool bake()
					{
						return (m_baked = performBake());
					}
				
					bool valid() const noexcept { return m_baked; }

					/**
					 * Binds the shader to render a single instance with.
					 *
					 * @return Whether binding was successful.
					 */
					bool bindForSingle
					(
						const gfx::Pass &pass,
						const gfx::Layers &layers,
						const Material *const material,
						const std::uint32_t &widthWindow,
						const std::uint32_t &heightWindow,
						const float &scaleWindowX,
						const float &scaleWindowY
					) const
					{
						return performBindForSingle
						(
							pass,
							layers,
							material,
							widthWindow,
							heightWindow,
							scaleWindowX,
							scaleWindowY
						);
					}

					/**
					 * Binds the shader to render multiple instances together.
					 *
					 * @return Whether binding was successful.
					 */
					bool bindForMultiple
					(
						const gfx::Pass &pass,
						const gfx::Layers &layers,
						const Material *const material,
						const std::uint32_t &widthWindow,
						const std::uint32_t &heightWindow,
						const float &scaleWindowX,
						const float &scaleWindowY
					) const
					{
						// Texture shaders cannot do multiple.
						if (Type::texture == m_type) return false;
						return performBindForMultiple
						(
							pass,
							layers,
							material,
							widthWindow,
							heightWindow,
							scaleWindowX,
							scaleWindowY
						);
					}
				
					bool bindInputsForSingle
					(
						const gfx::ContainerInputs &inputs
					) const
					{
						return performBindInputsForSingle(inputs);
					}
				
					bool bindInputsForMultiple
					(
						const gfx::ContainerInputs &inputs
					) const
					{
						return performBindInputsForMultiple(inputs);
					}
				
					bool updateDataForInstanceSingle(const gfx::Instance &instance) const
					{
						return performUpdateDataForInstanceSingle(instance);
					}
				
					bool updateDataForInstancesMultiple(const std::vector<gfx::Instance> &instances) const
					{
						// Texture shaders cannot do multiple.
						if (Type::texture == m_type) return false;
						return performUpdateDataForInstancesMultiple(instances);
					}
				
				protected:
					Shader(const Type type = Type::regular) : m_type{type} {}
				
				public:
					/**
					 * Returns whether the shader supports a source of the specified format.
					 *
					 * @param format The specified shader format.
					 *
					 * @return Whether the format is supported.
					 */
					virtual bool supportsFormat(const Format &format) const = 0;
				
				protected:
					bool hasMetadata() const noexcept override
					{
						return true;
					}
				
					Status performInit
					(
						std::vector<std::string> const &extraPathsRelativeToSourcePath
					) override;
				
					/// @todo: Implement these if necessary.
					void performSerialise(conv::Serialiser &) const override;
					void performDeserialise(const conv::Serialiser &) override;

					virtual bool performAddSources(const std::vector<DataSource> &) = 0;
					virtual bool performBake() = 0;
					virtual void performReset() = 0;

					virtual bool performBindForSingle
					(
						const gfx::Pass &,
						const gfx::Layers &layers,
						const Material *const,
						const std::uint32_t &widthWindow,
						const std::uint32_t &heightWindow,
						const float &scaleWindowX,
						const float &scaleWindowY
					) const = 0;
				
					virtual bool performBindForMultiple
					(
						const gfx::Pass &,
						const gfx::Layers &layers,
						const Material *const,
						const std::uint32_t &widthWindow,
						const std::uint32_t &heightWindow,
						const float &scaleWindowX,
						const float &scaleWindowY
					) const = 0;
				
					virtual bool performBindInputsForSingle
					(
						const gfx::ContainerInputs &inputs
					) const = 0;
				
					virtual bool performBindInputsForMultiple
					(
						const gfx::ContainerInputs &inputs
					) const = 0;
				
					virtual bool performUpdateDataForInstanceSingle(const gfx::Instance &) const = 0;
					virtual bool performUpdateDataForInstancesMultiple(const std::vector<gfx::Instance> &) const = 0;
				
				protected:
					Type m_type {Type::regular};
					bool m_baked{false};
			};
		}
	}
#endif
