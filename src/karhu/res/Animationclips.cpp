/**
 * @author		Ava Skoog
 * @date		2019-01-04
 * @copyright	2017-2023 Ava Skoog
 */

#include <karhu/res/Animationclips.hpp>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace res
	{
		void Animationclips::performSerialise(conv::Serialiser &ser) const
		{
			ser << karhuIN(clips);
		}
		
		void Animationclips::performDeserialise(const conv::Serialiser &ser)
		{
			ser >> karhuOUT(clips);
		}
	}
}
