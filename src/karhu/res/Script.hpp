/**
 * @author		Ava Skoog
 * @date		2019-05-27
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_RES_SCRIPT_H_
	#define KARHU_RES_SCRIPT_H_

	#include <karhu/res/Resource.hpp>

	class asIScriptModule;

	namespace karhu
	{
  		namespace res
  		{
			class Script : public Resource
			{
				public:
					asIScriptModule &module() const noexcept { return *m_module; }
					
				protected:
					bool hasMetadata() const noexcept override
					{
						return false;
					}
				
					void performSerialise(conv::Serialiser &) const override {}
					void performDeserialise(const conv::Serialiser &) override {}
				
				protected:
					asIScriptModule *m_module{nullptr};
			};
		}
	}
#endif
