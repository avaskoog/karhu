/**
 * @author		Ava Skoog
 * @date		2018-10-30
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_RES_MATERIAL_H_
	#define KARHU_RES_MATERIAL_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/res/Texture.hpp>
	#include <karhu/conv/graphics.hpp>
	#include <karhu/conv/maths.hpp>

	#include <map>
	#include <type_traits>

	namespace karhu
	{
		namespace gfx
		{
			class Rendertarget;
		}
		
  		namespace res
  		{
  			/**
			 * Abstract base class for a material, which contains a
			 * reference to a shader and the settings to apply to it.
			 */
			class Material : public Resource
			{
				public:
					struct Stencil
					{
						gfx::ConditionStencil condition{gfx::ConditionStencil::never};
						gfx::OperationStencil operation{gfx::OperationStencil::keep};

						std::uint8_t ref{1}, mask{0xff};
						
						void serialise(conv::Serialiser &) const;
						void deserialise(const conv::Serialiser &);
					};

				protected:
					bool hasMetadata() const noexcept override
					{
						return true;
					}
				
					Status performInit
					(
						std::vector<std::string> const &extraPathsRelativeToSourcePath
					) override
					{
						/// @todo: Do we need to implement this?
						return {true};
					}
				
					void performSerialise(conv::Serialiser &) const override;
					void performDeserialise(const conv::Serialiser &) override;

				public:
					gfx::Culling        culling       {gfx::Culling::back};
					gfx::Buffertargets  buffertargets {gfx::Buffertargets::defaults};
					gfx::ConditionDepth conditionDepth{gfx::ConditionDepth::always};
					gfx::Queue          queue         {gfx::Defaultqueues::opaque};
					IDResource          shader        {0};
					Stencil             stencil;

				public:
					gfx::ContainerInputs inputs;
					gfx::Rendertarget *output{nullptr};
			};
		}
	}
#endif
