/**
 * @author		Ava Skoog
 * @date		2018-10-30
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/res/Material.hpp>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace res
	{
		void Material::performSerialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(culling)
				<< karhuIN(buffertargets)
				<< karhuIN(conditionDepth)
				<< karhuIN(stencil)
				<< karhuIN(shader)
				<< karhuIN(queue)
				<< karhuIN(inputs);
			/// @todo: Output?
		}

		void Material::performDeserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(culling)
				>> karhuOUT(buffertargets)
				>> karhuOUT(conditionDepth)
				>> karhuOUT(stencil)
				>> karhuOUT(shader)
				>> karhuOUT(queue)
				>> karhuOUT(inputs);
			/// @todo: Output?
		}

		void Material::Stencil::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(condition)
				<< karhuIN(operation)
				<< karhuIN(ref)
				<< karhuIN(mask);
		}

		void Material::Stencil::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(condition)
				>> karhuOUT(operation)
				>> karhuOUT(ref)
				>> karhuOUT(mask);
		}
	}
}
