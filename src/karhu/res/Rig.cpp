/**
 * @author		Ava Skoog
 * @date		2018-10-30
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/res/Rig.hpp>
#include <karhu/core/log.hpp>

namespace
{
	using namespace karhu;
	using namespace karhu::res;
	
	Status addChildrenInOrder
	(
		std::vector<std::unique_ptr<Rig::Child>> &joints,
		Rig &rig
	)
	{
		for (auto &joint : joints)
		{
			if (!joint)
				return {false, "Failed to bake rig: joint is null"};
			
			if (joint->ID < rig.countJoints)
				rig.childrenInOrder.emplace_back(joint.get());
			
			addChildrenInOrder(joint->children, rig);
		}
	
		return {true};
	}
}

namespace karhu
{
	namespace res
	{
		Status Rig::bake()
		{
//			for (auto &root : roots)
//				if (root)
//					calcMatricesBindInverse(*root, transform.matrix());

			//childrenInOrder.resize(countJoints);
			Status
				status{addChildrenInOrder(roots, *this)};
			
			std::sort
			(
				childrenInOrder.begin(),
				childrenInOrder.end(),
				[](auto const &a, auto const &b)
				{
					return (a->ID < b->ID);
				}
			);
			
			return status;
		}
		
		void Rig::reset()
		{
			transform   = {};
			countJoints = 0;
			
			roots.clear();
		}
		
		/// @todo: Was there a reason this was added back..? EDIT: Yes, the fact that we might support non-glTF in the future and might have to calculate them on our own anyway. Doesn't hurt! EDIT 2: This actually doesn't work properly so removing it again for now since glTF provides baked matrices; might want to revisit at some point.
		void Rig::calcMatricesBindInverse(Child &node, const mth::Mat4f &bindParent)
		{
			mth::Mat4f bind{bindParent * node.transform.matrix()};
			node.matrixBindInverse = mth::inverse(bind);

			for (const auto &child : node.children)
				if (child)
					calcMatricesBindInverse(*child, bind);
		}
	}
}
