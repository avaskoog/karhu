/**
 * @author		Ava Skoog
 * @date		2018-10-30
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_RES_RIG_H_
	#define KARHU_RES_RIG_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/conv/maths.hpp>

	#include <string>
	#include <vector>
	#include <map>
	#include <memory>

	namespace karhu
	{
  		namespace res
  		{
			/**
			 * Contains skinned mesh animation data.
			 */
			class Rig : public Resource
			{
				public:
					struct Child
					{
						// The ID defines which jointset/weightset affects this joint.
						std::size_t ID;
						
						// The transform defines the effect the joint has on its vertices.
						mth::Transformf transform;
						
						// The inverse bind matrix relates the joint to the mesh.
						mth::Mat4f matrixBindInverse;
						
						// The name is a unique identifier of the joint so that multiple animations
						// can easily animate the same rig by referring to the same names.
						std::string name;
						
						// Joints may contain further children which inherit parent transformations.
						std::vector<std::unique_ptr<Child>> children;
					};
				
				public:
					Status bake();
					void reset();

				public:
					// The rig may be offset from the mesh it applies to.
					mth::Transformf transform;

					// Precalculated total number of joints.
					std::size_t countJoints{0};
				
					// The root joint which has no parent.
					std::vector<std::unique_ptr<Child>> roots;
				
					// Roots sequentially by ID.
					std::vector<Child *> childrenInOrder;
					
				protected:
					bool hasMetadata() const noexcept override
					{
						return true;
					}
				
					/// @todo: Implement these if necessary.
					void performSerialise(conv::Serialiser &) const override {}
					void performDeserialise(const conv::Serialiser &) override {}
				
				private:
					static void calcMatricesBindInverse(Child &node, const mth::Mat4f &bindParent);
			};
		}
	}
#endif
