/**
 * @author		Ava Skoog
 * @date		2018-10-30
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/res/Shader.hpp>
#include <karhu/data/serialise.hpp>

#include <sstream>

namespace karhu
{
	namespace res
	{
		/**
		 * Adds the correct GLSL version number, precision and
		 * whatever else is necessary for all shaders, depending
		 * on platform, to the top of the provided source code.
		 *
		 * @param format The shader format (GL or GLES).
		 * @param mode   The shader mode.
		 * @param type   The GLSL shader type.
		 * @param source The base source code for the shader.
		 *
		 * @return A string containing the modified shader source.
		 */
		std::string Shader::addHeaderToGLSL
		(
			const Format   &format,
			const Mode     &mode,   // Unused for now but may change in the future.
			const TypeGLSL &type,   // Unused for now but may change in the future.
			const char     *source
		)
		{
			std::stringstream r;
			
			switch (format)
			{
				case Format::GL:
					r << "#version 330 core\n\n";
					break;
				
				case Format::GLES:
					r << "#version 300 es\n\n";
					break;
				
				case Format::invalid:
				case Format::metal:
					break;
			}
			
			r << "precision highp float;\n";
			r << "precision highp int;\n\n";
			r << source;
			
			return r.str();
		}

		/**
		 * Prepares a GLSL shader for use in engine by adding
		 * the correction version number, settings, uniforms
		 * and so on to the file.
		 *
		 * @param format The shader format (GL or GLES).
		 * @param mode   The shader mode.
		 * @param type   The GLSL shader type.
		 * @param source The base source code for the shader.
		 *
		 * @return A string containing the modified shader source.
		 */
		std::string Shader::addDefaultsToGLSL
		(
			Format   const &      format, // Unused for now but may change in the future.
			Mode     const &      mode,
			TypeGLSL const &      type,
			char     const *const source
		)
		{
			std::stringstream r;

			/// @todo: Make Shader::addDefaultsToGLSL() add the layer names from _graphics as automated constants into all shaders.
			/// @todo: Should probably move all this sort of stuff out into file(s) so that the utility needn't be recompiled every time additions are made...
			
			constexpr char const *insShared
			(
				"layout(location = 4) in vec3 _vpos;\n"
				"layout(location = 5) in vec3 _vnormal;\n"
				"layout(location = 6) in vec2 _vUV;\n"
				"layout(location = 7) in vec4 _vcol;\n"
				"layout(location = 8) in vec4 _vjoints0;\n"
				"layout(location = 9) in vec4 _vweights0;\n"
				"layout(location = 10) in vec4 _vjoints1;\n"
				"layout(location = 11) in vec4 _vweights1;\n"
				"layout(location = 12) in vec4 _vjoints2;\n"
				"layout(location = 13) in vec4 _vweights2;\n"
				"layout(location = 14) in vec4 _vtangent;\n"
				"layout(location = 15) in vec4 _vcustom;\n"
			);

			constexpr char const *uniformsShared
			(
				"uniform mat4 _view;\n"
				"uniform mat4 _proj;\n"
			);
			
			if (TypeGLSL::vert == type)
			{
				if (Mode::multiple == mode)
					r << "layout(location = 0) in mat4 _model;\n";

				r << insShared;
			}
			
			r << uniformsShared;
			
			if (Mode::single == mode)
				r << "uniform mat4 _model;\n";
			
			r << "uniform int _layers;\n";

			r << source;
			
			return r.str();
		}

		Status Shader::performInit
		(
			std::vector<std::string> const &extraPathsRelativeToSourcePath
		)
		{
			/// @todo: Implement this to add sources.
			return {true};
		}
		
		void Shader::performSerialise(conv::Serialiser &ser) const
		{
			ser << karhuINn("type", m_type);
			/// @todo: Figure out how to deal with sources/builds.
		}

		void Shader::performDeserialise(const conv::Serialiser &ser)
		{
			ser >> karhuOUTn("type", m_type);
			/// @todo: Figure out how to deal with sources/builds.
		}
	}
}
