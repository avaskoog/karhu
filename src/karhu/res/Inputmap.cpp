/**
 * @author		Ava Skoog
 * @date		2019-01-04
 * @copyright	2017-2023 Ava Skoog
 */

#include <karhu/res/Inputmap.hpp>
#include <karhu/data/serialise.hpp>

#include <karhu/core/log.hpp>

namespace karhu
{
	namespace res
	{
		void Inputmap::performSerialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(actions)
				<< karhuIN(setups);
		}
		
		void Inputmap::performDeserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(actions)
				>> karhuOUT(setups);
		}
		
		void Inputmap::Action::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(used)
				<< karhuIN(type)
				<< karhuIN(method)
				<< karhuIN(scope)
				<< karhuIN(invert)
				<< karhuIN(normalise)
				<< karhuIN(sensitivity)
				<< karhuIN(deadzone)
				<< karhuIN(codes);
		}
		
		void Inputmap::Action::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(used)
				>> karhuOUT(type)
				>> karhuOUT(method)
				>> karhuOUT(scope)
				>> karhuOUT(invert)
				>> karhuOUT(normalise)
				>> karhuOUT(sensitivity)
				>> karhuOUT(deadzone)
				>> karhuOUT(codes);
		}
		
		void Inputmap::Setup::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(type)
				<< karhuIN(codes);
		}

		void Inputmap::Setup::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(type)
				>> karhuOUT(codes);
		}
	}
}
