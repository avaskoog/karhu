/**
 * @author		Ava Skoog
 * @date		2018-10-29
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: We cannot have depth and stencil separately but they have to be combined so do error checking and make the API clearer.

#ifndef KARHU_RES_TEXTURE_H_
	#define KARHU_RES_TEXTURE_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/conv/graphics.hpp>
	#include <karhu/core/log.hpp>

	namespace karhu
	{
  		namespace res
  		{
  			/**
			 * Abstract base class representing a texture, which holds
			 * pixel data that can be modified and used to work with images,
			 * especially for rendering.
			 */
			class Texture : public Resource
			{
				public:
					/**
					 * Texture type.
					 */
					enum class Type : std::uint8_t
					{
						colour,
						depth,
						stencil,
						data,
						shadowdepth
					};

					/**
					 * Defines the wrap mode of a texture.
					 */
					enum class ModeWrap : std::uint8_t
					{
						repeat,
						clamp
					};
				
					enum class ModeFilter : std::uint8_t
					{
						nearest,
						linear
					};
				
				public:
					/**
					 * Returns the type of the texture.
					 */
					Type type() const noexcept { return m_type; }
					/// @todo: Should this have a setter?

					/**
					 * Sets the wrap mode.
					 *
					 * @param mode The wrap mode.
					 */
					void wrap(ModeWrap const mode) noexcept { m_modeWrap = mode; }

					/**
					 * Returns the wrap mode.
					 *
					 * @return The wrap mode.
					 */
					ModeWrap wrap() const noexcept { return m_modeWrap; }

					/**
					 * Sets the filter mode.
					 *
					 * @param mode The filter mode.
					 */
					void filter(ModeFilter const mode) noexcept { m_modeFilter = mode; }

					/**
					 * Returns the filter mode.
					 *
					 * @return The filter mode.
					 */
					ModeFilter filter() const noexcept { return m_modeFilter; }
				
					/**
					 * Returns the format.
					 *
					 * @return The pixel format.
					 */
					gfx::Pixelbuffer::Format format() const noexcept { return m_buffer.format; }
					/// @todo: Should this have a setter?

					/**
					 * Returns the width of the texture in pixels.
					 *
					 * @return The texture width.
					 */
					std::uint32_t width() const noexcept { return m_buffer.width; }
				
					/**
					 * Returns the width of the texture in pixels.
					 *
					 * @return The texture width.
					 */
					std::uint32_t height() const noexcept { return m_buffer.height; }
				
					/**
					 * Sets the pixels from a buffer.
					 *
					 * @param buffer The buffer to copy.
					 */
					void setFromBuffer(const gfx::Pixelbuffer &buffer) { m_buffer = buffer; }
				
					/**
					 * Sets the pixels from a buffer.
					 *
					 * @param buffer The buffer to move.
					 */
					void setFromBuffer(gfx::Pixelbuffer &&buffer) { m_buffer = std::move(buffer); }
				
					/**
					 * Sets the size and format without setting any pixels explicitly.
					 *
					 * @param width  The width to set in pixels.
					 * @param height The height to set in pixels.
					 * @param format The format (number of channels) to set.
					 */
					void reserve(std::uint32_t const width, std::uint32_t const height, gfx::Pixelbuffer::Format const format)
					{
						m_buffer.reserve(width, height, format);
					}
				
					void reserveNull(std::uint32_t const width, std::uint32_t const height)
					{
						m_buffer.components.reset();
						m_buffer.width  = width;
						m_buffer.height = height;
					}
				
					/**
					 * Copies the texture to the buffer.
					 *
					 * @param buffer The buffer to copy to.
					 *
					 * @return Whether the refresh was successful.
					 */
					bool copy(gfx::Pixelbuffer &buffer) const
					{
						return performCopy(buffer);
					}

					/**
					 * Returns the array of pixels as a pointer.
					 * Each element is a struct containing the four colour components.
					 * Use width() and height() to prevent index errors.
					 *
					 * @return The array of pixels or null if not yet reserved.
					 */
					gfx::Pixelbuffer::Components *pixels() noexcept { return m_buffer.components.get(); }
				
					/**
					 * Returns the array of pixels as a pointer.
					 * Each element is a struct containing the four colour components.
					 * Use width() and height() to prevent index errors.
					 *
					 * @return The array of pixels or null if not yet reserved.
					 */
					gfx::Pixelbuffer::Components const *pixels() const noexcept { return m_buffer.components.get(); }

					/**
					 * Tries to apply any changes and make the texture ready for use.
					 * Needs to be called after modifications for them to take effect.
					 *
					 * @return Whether baking was successful.
					 */
					bool bake()
					{
						m_baked = false;
						
						if (m_buffer.width == 0 || m_buffer.height == 0)
						{
							/// @todo: Change to Status instead of bool?
							log::err("Karhu") << "Cannot bake texture with zero width or height";
							return false;
						}
						
						if (performBake())
							m_baked = true;
						
						return m_baked;
					}

					/**
					 * Returns whether the texture has been successfully baked and
					 * is ready for use.
					 *
					 * @return Whether the texture is valid.
					 */
					bool valid() const noexcept { return m_baked; }

				protected:
					Texture
					(
						Type                     const type   = Type::colour,
						gfx::Pixelbuffer::Format const format = gfx::Pixelbuffer::Format::RGB
					)
					:
					m_type  {type},
					m_buffer{format}
					{
					}
				
				public:
					/**
					 * Returns the underlying texture handle,
					 * dependent on the backend implementation.
					 *
					 * @return The underlying texture handle, meant to be casted if the type is known.
					 */
					virtual void const *handle() const noexcept = 0;
				
				protected:
					bool hasMetadata() const noexcept override
					{
						return true;
					}
					
					virtual void performSerialise(conv::Serialiser &) const override;
					virtual void performDeserialise(conv::Serialiser const &) override;
				
					virtual bool performCopy(gfx::Pixelbuffer &) const = 0;
					virtual bool performBake() = 0;
				
				protected:
					Type             m_type      {Type::colour};
					ModeWrap         m_modeWrap  {ModeWrap::clamp};
					ModeFilter       m_modeFilter{ModeFilter::nearest};
					gfx::Pixelbuffer m_buffer;
				
					bool m_baked{false};
			};
		}
	}

	// Bitwise operations for the texture type bitflags.

	inline constexpr karhu::res::Texture::Type operator|
	(
		const karhu::res::Texture::Type &a,
		const karhu::res::Texture::Type &b
	)
	{
		using T = std::underlying_type_t<karhu::res::Texture::Type>;
		return static_cast<karhu::res::Texture::Type>(static_cast<T>(a) | static_cast<T>(b));
	}

	inline constexpr karhu::res::Texture::Type operator&
	(
		const karhu::res::Texture::Type &a,
		const karhu::res::Texture::Type &b
	)
	{
		using T = std::underlying_type_t<karhu::res::Texture::Type>;
		return static_cast<karhu::res::Texture::Type>(static_cast<T>(a) & static_cast<T>(b));
	}

	inline constexpr karhu::res::Texture::Type &operator|=
	(
		karhu::res::Texture::Type &a,
		const karhu::res::Texture::Type &b
	)
	{
		return a = a | b;
	}

	inline constexpr karhu::res::Texture::Type &operator&=
	(
		karhu::res::Texture::Type &a,
		const karhu::res::Texture::Type &b
	)
	{
		return a = a & b;
	}
#endif
