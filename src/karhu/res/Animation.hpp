/**
 * @author		Ava Skoog
 * @date		2018-10-30
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_RES_ANIMATION_H_
	#define KARHU_RES_ANIMATION_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/conv/maths.hpp>

	#include <string>
	#include <cstdint>
	#include <vector>
	#include <map>

	namespace karhu
	{
  		namespace res
  		{
  			/**
			 * Contains skinned mesh animation data.
			 * Does not contain logic or state for animation playback itself.
			 */
			class Animation : public Resource
			{
				public:
					struct Channel
					{
						struct Keyframe
						{
							// The GLM types in the union force us to define this.
							Keyframe() : position{} {}

							float time{0.0f};
							
							union
							{
								mth::Vec3f position;
								mth::Vec3f scale;
								mth::Quatf rotation;
							};
						};

						// What kind of transform to apply to the joint.
						enum class Property : std::uint8_t
						{
							position,
							scale,
							rotation
							// weight?
						} property{Property::position};

						// A keyframe define when the value is set to what.
						std::vector<Keyframe> keyframes;
					};
				
				public:
					bool bake()
					{
						m_baked = false;
						
						// Animation must have a duration!
						if (0.0f == duration)
							return false;
						
						// Animation channels lacking keyframes will be deleted.
						for (auto &channel : channelsPerJoint)
						{
							for (auto it(channel.second.begin()); it != channel.second.end();)
							{
								if (0 == it->keyframes.size())
									it = channel.second.erase(it);
								else
									++ it;
							}
						}
						
						// Joint targets lacking channels will be deleted.
						for (auto it(channelsPerJoint.begin()); it != channelsPerJoint.end();)
						{
							if (0 == it->second.size())
								it = channelsPerJoint.erase(it);
							else
								++ it;
						}

						// Animation must have joint targets!
						if (0 == channelsPerJoint.size())
							return false;
						
						return (m_baked = true);
					}
				
					void reset()
					{
						m_baked = false;
						duration = 0.0f;
						channelsPerJoint.clear();
					}
				
					bool valid() const noexcept { return m_baked; }
					
				protected:
					bool hasMetadata() const noexcept override
					{
						return true;
					}
					
					/// @todo: Implement these if necessary.
					void performSerialise(conv::Serialiser &) const override {}
					void performDeserialise(const conv::Serialiser &) override {}

				public:
					// The latest timestamp of the animation.
					float duration{0.0f};
				
					// The animation is just a bunch of channels.
					// The key is the name of the joint to animate.
					// The same node might need multiple channels.
					std::map<std::string, std::vector<Channel>> channelsPerJoint;
				
				private:
					bool m_baked{false};
			};
		}
	}
#endif
