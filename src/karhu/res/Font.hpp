/**
 * @author		Ava Skoog
 * @date		2019-05-20
 * @copyright	2017-2023 Ava Skoog
 */

#ifndef KARHU_RES_FONT_H_
	#define KARHU_RES_FONT_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/conv/graphics.hpp>
	#include <karhu/conv/types.hpp>
	#include <karhu/core/Nullable.hpp>

	namespace karhu
	{
  		namespace res
  		{
			class Font : public Resource
			{
				public:
					virtual const char *face() const noexcept { return ""; }
				
				protected:
					bool hasMetadata() const noexcept override
					{
						return true;
					}
				
					Status performInit
					(
						std::vector<std::string> const &extraPathsRelativeToSourcePath
					) override
					{
						/// @todo: Implement this if necessary.
						return {true};
					}
				
					void performSerialise(conv::Serialiser &) const override {}
					void performDeserialise(const conv::Serialiser &) override {}
			};
		}
	}
#endif
