/**
 * @author		Ava Skoog
 * @date		2018-10-30
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/res/Data.hpp>

namespace karhu
{
	namespace res
	{
		Status Data::performLoadInAdditionToMetadata
		(
			const conv::JSON::Obj *const,
			const char *filepathRelativeToResourcepath,
			const char *suffix,
			conv::AdapterFilesystemRead &filesystem
		)
		{
			const auto path(std::string{filepathRelativeToResourcepath} + suffix);
			
			auto r(filesystem.readStringFromResource
			(
				path.c_str()
			));
			
			if (!r.success)
				return {false, std::move(r.error)};
			
			conv::JSON::Parser parser;
			std::stringstream s;
			s << r.value;
			auto val(parser.parse(s));
			
			if (!val)
			{
				/// @todo: Error message.
				return {false};
			}
			
			/// @todo: Validate data.
			m_data = std::move(*val);
			
			return {true};
		}

		Status Data::performSaveInAdditionToMetadata
		(
			conv::JSON::Obj *const,
			const char *filepathRelativeToResourcepath,
		 	const char *suffix,
			conv::AdapterFilesystemWrite &filesystem
		)
		{
			const auto path(std::string{filepathRelativeToResourcepath} + suffix);
			auto r(filesystem.writeStringToResource
			(
				path.c_str(),
				m_data.dump().c_str()
			));
			
			if (!r.success)
			{
				/// @todo: Error message.
				return {false};
			}
			
			return {true};
		}
	}
}
