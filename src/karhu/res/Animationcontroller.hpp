/**
 * @author		Ava Skoog
 * @date		2019-01-04
 * @copyright	2017-2023 Ava Skoog
 */

#ifndef KARHU_RES_ANIMATIONCONTROLLER_H_
	#define KARHU_RES_ANIMATIONCONTROLLER_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/conv/maths.hpp>

	namespace karhu
	{
  		namespace res
  		{
			class Animationcontroller : public Resource
			{
				protected:
					bool hasMetadata() const noexcept override
					{
						return true;
					}
				
					Status performInit
					(
						std::vector<std::string> const &/*extraPathsRelativeToSourcePath*/
					) override
					{
						/// @todo: Implement this if necessary.
						return {true};
					}
				
					void performSerialise(conv::Serialiser &) const override;
					void performDeserialise(const conv::Serialiser &) override;
				
				public:
					using Index = std::int16_t;
				
					struct Clip
					{
						/// @todo: Clips should be possible to specify for whether they loop in animation controllers, but the question is whether it should go in the controller or in the clip asset.
						
						enum class Type
						{
							single,
							blendtree
						} type{Type::single};
						
						bool loop;
						Index var;
						std::vector<Index> names;
						
						void serialise(conv::Serialiser &) const;
						void deserialise(const conv::Serialiser &);
					};
				
					struct Value
					{
						enum class Type
						{
							boolean,
							integer,
							floating
						} type;
						
						union
						{
							bool       boolean;
							mth::Scali integer;
							mth::Scalf floating;
						};
						
						void serialise(conv::Serialiser &) const;
						void deserialise(const conv::Serialiser &);
					};
				
					struct ValueWithName : public Value
					{
						std::string name; /// @todo: Possibly editor-only but not final build.
						
						void serialise(conv::Serialiser &) const;
						void deserialise(const conv::Serialiser &);
					};
				
					struct Trigger
					{
						std::string name; /// @todo: Possibly editor-only but not final build.
						
						void serialise(conv::Serialiser &) const;
						void deserialise(const conv::Serialiser &);
					};
				
					struct Transition
					{
						/// @todo: Add option as to whether a transition should be immediate or let the current animation finish first.
						
						struct Comparand
						{
							enum class Type
							{
								value,
								index
							} type;
							
							union
							{
								Value value;
								Index index;
							};
							
							void serialise(conv::Serialiser &) const;
							void deserialise(const conv::Serialiser &);
						} comp;
						
						enum class Operation : std::uint8_t
						{
							equal,
							unequal,
							less,
							lessOrEqual,
							greater,
							greaterOrEqual,
							trigger,
							always
						} op;
						
						enum class Behaviour : std::uint8_t
						{
							immediately,
							finish,
							blend
						} behaviour;
						
						Index src{0}, dst{0}, var{0};
						float blendtime{0.0f};
						
						void serialise(conv::Serialiser &) const;
						void deserialise(const conv::Serialiser &);
					};
				
					Index                      entry{0};
					std::vector<std::string>   names; /// @todo: Possibly editor-only but not final build.
					std::vector<Clip>          clips;
					std::vector<ValueWithName> vars, consts;
					std::vector<Trigger>       triggers;
					std::vector<Transition>    transitions;
			};
		}
	}
#endif
