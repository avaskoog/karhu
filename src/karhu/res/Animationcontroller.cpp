/**
 * @author		Ava Skoog
 * @date		2019-01-04
 * @copyright	2017-2023 Ava Skoog
 */

#include <karhu/res/Animationcontroller.hpp>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace res
	{
		void Animationcontroller::performSerialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(entry)
				<< karhuIN(names)
				<< karhuIN(clips)
				<< karhuIN(vars)
				<< karhuIN(consts)
				<< karhuIN(triggers)
				<< karhuIN(transitions);
		}
		
		void Animationcontroller::performDeserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(entry)
				>> karhuOUT(names)
				>> karhuOUT(clips)
				>> karhuOUT(vars)
				>> karhuOUT(consts)
				>> karhuOUT(triggers)
				>> karhuOUT(transitions);
		}
		
		void Animationcontroller::Clip::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(type)
				<< karhuIN(loop)
				<< karhuIN(var)
				<< karhuIN(names);
		}
		
		void Animationcontroller::Clip::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(type)
				>> karhuOUT(loop)
				>> karhuOUT(var)
				>> karhuOUT(names);
		}
		
		void Animationcontroller::Value::serialise(conv::Serialiser &ser) const
		{
			ser << karhuIN(type);
			
			switch (type)
			{
				case Type::boolean:  ser << karhuINn("val", boolean);  break;
				case Type::integer:  ser << karhuINn("val", integer);  break;
				case Type::floating: ser << karhuINn("val", floating); break;
			}
		}
		
		void Animationcontroller::Value::deserialise(const conv::Serialiser &ser)
		{
			ser >> karhuOUT(type);
			
			switch (type)
			{
				case Type::boolean:  ser >> karhuOUTn("val", boolean);  break;
				case Type::integer:  ser >> karhuOUTn("val", integer);  break;
				case Type::floating: ser >> karhuOUTn("val", floating); break;
			}
		}
		
		void Animationcontroller::ValueWithName::serialise(conv::Serialiser &ser) const
		{
			Value::serialise(ser);
			ser << karhuIN(name);
		}
		
		void Animationcontroller::ValueWithName::deserialise(const conv::Serialiser &ser)
		{
			Value::deserialise(ser);
			ser >> karhuOUT(name);
		}
		
		void Animationcontroller::Trigger::serialise(conv::Serialiser &ser) const
		{
			ser << karhuIN(name);
		}
		
		void Animationcontroller::Trigger::deserialise(const conv::Serialiser &ser)
		{
			ser >> karhuOUT(name);
		}
		
		void Animationcontroller::Transition::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(comp)
				<< karhuIN(op)
				<< karhuIN(behaviour)
				<< karhuIN(src)
				<< karhuIN(dst)
				<< karhuIN(var)
				<< karhuIN(blendtime);
		}
		
		void Animationcontroller::Transition::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(comp)
				>> karhuOUT(op)
				>> karhuOUT(behaviour)
				>> karhuOUT(src)
				>> karhuOUT(dst)
				>> karhuOUT(var)
				>> karhuOUT(blendtime);
		}
		
		void Animationcontroller::Transition::Comparand::serialise(conv::Serialiser &ser) const
		{
			ser << karhuIN(type);
			
			switch (type)
			{
				case Type::value: ser << karhuINn("val", value); break;
				case Type::index: ser << karhuINn("val", index); break;
			}
		}
		
		void Animationcontroller::Transition::Comparand::deserialise(const conv::Serialiser &ser)
		{
			ser >> karhuOUT(type);
			
			switch (type)
			{
				case Type::value: ser >> karhuOUTn("val", value); break;
				case Type::index: ser >> karhuOUTn("val", index); break;
			}
		}
	}
}
