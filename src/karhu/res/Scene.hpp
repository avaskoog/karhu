/**
 * @author		Ava Skoog
 * @date		2018-10-30
 * @copyright	2017-2023 Ava Skoog
 */

#ifndef KARHU_RES_SCENE_H_
	#define KARHU_RES_SCENE_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/conv/types.hpp>

	namespace karhu
	{
  		namespace res
  		{
			class Scene : public Resource
			{
				public:
					conv::JSON::Arr *entities() noexcept { return m_data.object()->getArray("entities"); }
					const conv::JSON::Arr *entities() const noexcept { return m_data.object()->getArray("entities"); }
				
					conv::JSON::Arr *parents() noexcept { return m_data.object()->getArray("parents"); }
					const conv::JSON::Arr *parents() const noexcept { return m_data.object()->getArray("parents"); }
				
					void set(conv::JSON::Val v) { m_data = std::move(v); }
				
				protected:
					bool hasMetadata() const noexcept override
					{
						return false;
					}
				
					Status performInit
					(
						std::vector<std::string> const &//extraPathsRelativeToSourcePath
					) override
					{
						/// @todo: Implement this if necessary.
						return {true};
					}
				
					Status performLoadInAdditionToMetadata
					(
						const conv::JSON::Obj *const metadata,
						const char *filepathRelativeToResourcepath,
						const char *suffix,
						conv::AdapterFilesystemRead &filesystem
					) override;
				
					Status performSaveInAdditionToMetadata
					(
						conv::JSON::Obj *const metadata,
						const char *filepathRelativeToResourcepath,
						const char *suffix,
						conv::AdapterFilesystemWrite &filesystem
					) override;
				
					/// @todo: Implement these if necessary.
					void performSerialise(conv::Serialiser &) const override {}
					void performDeserialise(const conv::Serialiser &) override {}
				
				protected:
					conv::JSON::Val m_data;
			};
		}
	}
#endif
