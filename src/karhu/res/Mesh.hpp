/**
 * @author		Ava Skoog
 * @date		2018-10-30
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_RES_MESH_H_
	#define KARHU_RES_MESH_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/conv/graphics.hpp>

	#include <vector>
	#include <cstdint>
	#include <type_traits>

	namespace karhu
	{
  		namespace res
  		{
			/**
			 * Abstract base class of a 3D mesh, to be implemented for each
			 * graphics subsystem implementation so that the rendering system
			 * can work with it as necessary.
			 */
			class Mesh : public Resource
			{
				public:
					// Baked geometry goes into a single buffer.
					struct Buffer
					{
						struct Range
						{
							std::size_t offset{0}, count{0};
							Range(const std::size_t &offset, const std::size_t &count) : offset{offset}, count{count} {}
						};
						
						gfx::Geometry geometry;     // All the vertex data.
						std::vector<Range> rangesPerLOD; // Index ranges per submesh.
						
						void clear()
						{
							geometry.clear();
							rangesPerLOD.clear();
						}
					};
				
				public:
					/**
					 * Returns a pointer to the geometry at the specified LOD level.
					 * The pointer may be invalidated later so do not hold on to it.
					 *
					 * @param level The LOD level of the geometry to get.
					 *
					 * @return A pointer to the geometry if the level is valid, or null.
					 */
					gfx::Geometry *geometryAtLOD(const std::size_t &level) noexcept
					{
						return (level < m_dataPerLOD.size()) ? &m_dataPerLOD[level].geometry : nullptr;
					}
				
					void distanceAtLOD(const std::size_t &level, const float &value) noexcept
					{
						if (level < m_dataPerLOD.size())
							m_dataPerLOD[level].distance = value;
					}
				
					float distanceAtLOD(const std::size_t &level) noexcept
					{
						return (level < m_dataPerLOD.size()) ? m_dataPerLOD[level].distance : 0.0f;
					}
				
					/**
					 * Returns a pointer to the geometry at the specified LOD level.
					 * The pointer may be invalidated later so do not hold on to it.
					 *
					 * @param level The LOD level of the geometry to get.
					 *
					 * @return A pointer to the geometry if the level is valid, or null.
					 */
					const gfx::Geometry *geometryAtLOD(const std::size_t &level) const noexcept
					{
						return (level < m_dataPerLOD.size()) ? &m_dataPerLOD[level].geometry : nullptr;
					}

					/**
					 * Adds some geometry at the next LOD and returns a reference to it.
					 * The reference may be invalidated later so do not hold on to it.
					 *
					 * @return A reference to the new geometry so that it can be modified.
					 */
					gfx::Geometry &createGeometryAtNextLOD()
					{
						m_dataPerLOD.emplace_back();
						return m_dataPerLOD.back().geometry;
					}

					/**
					 * Adds some geometry at the next LOD and returns a reference to it.
					 * The reference may be invalidated later so do not hold on to it.
					 *
					 * @param geometry The geometry to copy into the new instance.
					 *
					 * @return A reference to the new geometry so that it can be modified.
					 */
					gfx::Geometry &createGeometryAtNextLOD(const gfx::Geometry &geometry)
					{
						m_dataPerLOD.emplace_back(DataPerLOD{geometry, 0.0f});
						return m_dataPerLOD.back().geometry;
					}

					/**
					 * Adds some geometry at the next LOD and returns a reference to it.
					 * The reference may be invalidated later so do not hold on to it.
					 *
					 * @param geometry The geometry to move into the new instance.
					 *
					 * @return A reference to the new geometry so that it can be modified.
					 */
					gfx::Geometry &createGeometryAtNextLOD(gfx::Geometry &&geometry)
					{
						m_dataPerLOD.emplace_back(DataPerLOD{std::move(geometry), 0.0f});
						return m_dataPerLOD.back().geometry;
					}

					/**
					 * Returns the total count of LOD levels of the mesh.
					 *
					 * @return The LOD level count.
					 */
					std::size_t countLevelsLOD() const noexcept { return m_dataPerLOD.size(); }

					bool bake();				
					void reset();

					/**
					 * Returns whether the mesh can be drawn.
					 */
					bool valid() const noexcept { return m_baked; }

					/**
					 * Selects the mesh for drawing a single instance by the rendering implementation.
					 * Will not work if the mesh is not valid (@see valid(), @see bake()).
					 *
					 * @param attributes Bitflags setting which vertex attributes to enable (if available); defaults to all if omitted.
					 *
					 * @return Whether binding was successful.
					 */
					bool bindForSingle(const gfx::Geometry::Attributes attributes = gfx::Geometry::Attribute::all)
					{
						if (!valid()) return false;
						return performBindForSingle(attributes);
					}
				
					/**
					 * Selects the mesh for drawing a multiple instances by the rendering implementation.
					 * Will not work if the mesh is not valid (@see valid(), @see bake()).
					 *
					 * @param instances A list of the instances to be drawn.
					 * @param attributes Bitflags setting which vertex attributes to enable (if available); defaults to all if omitted.
					 *
					 * @return Whether binding was successful.
					 */
					bool bindForMultiple
					(
						const std::vector<gfx::Instance> &instances,
						const gfx::Geometry::Attributes attributes = gfx::Geometry::Attribute::all
					)
					{
						if (!valid()) return false;
						return performBindForMultiple(instances, attributes);
					}
				
					Buffer &buffer() noexcept { return m_buffer; }
					const Buffer &buffer() const noexcept { return m_buffer; }
				
				protected:
					bool hasMetadata() const noexcept override
					{
						return true;
					}
				
					/// @todo: Implement these if necessary.
					void performSerialise(conv::Serialiser &) const override {}
					void performDeserialise(const conv::Serialiser &) override {}
				
				protected:
					/**
					 * Tries to bake the mesh and returns whether successful.
					 * Please output an error message on failure.
					 *
					 * @return Whether successful.
					 */
					virtual bool performBake() = 0;
				
					virtual void performReset() = 0;

					/**
					 * Tries to bind the mesh for drawing a single instance of it.
					 * Please output an error message on failure.
					 * Do not check for validity as this will be done before any calls.
					 *
					 * @param attributes Bitflags setting which vertex attributes to enable (if available); defaults to all if omitted.
					 *
					 * @return Whether binding was successful.
					 */
					virtual bool performBindForSingle(const gfx::Geometry::Attributes attributes) = 0;

					/**
					 * Tries to bind the mesh for drawing multiple instances of it.
					 * Please output an error message on failure.
					 * Do not check for validity as this will be done before any calls.
					 *
					 * @param instances A list of the instances to be drawn in case their information is necessary.
					 * @param attributes Bitflags setting which vertex attributes to enable (if available); defaults to all if omitted.
					 *
					 * @return Whether binding was successful.
					 */
					virtual bool performBindForMultiple
					(
						const std::vector<gfx::Instance> &instances,
						const gfx::Geometry::Attributes attributes
					) = 0;

				private:
					struct DataPerLOD
					{
						gfx::Geometry geometry;
						float distance{0.0f};
					};
				
					std::vector<DataPerLOD> m_dataPerLOD;

					Buffer m_buffer;

					bool m_baked{false};
			};
		}
	}

	// Bitwise operations for the attribute bitflags.

	inline constexpr karhu::gfx::Geometry::Attribute operator|
	(
		const karhu::gfx::Geometry::Attribute &a,
		const karhu::gfx::Geometry::Attribute &b
	)
	{
		using T = std::underlying_type_t<karhu::gfx::Geometry::Attribute>;
		return static_cast<karhu::gfx::Geometry::Attribute>(static_cast<T>(a) | static_cast<T>(b));
	}

	inline constexpr karhu::gfx::Geometry::Attribute operator&
	(
		const karhu::gfx::Geometry::Attribute &a,
		const karhu::gfx::Geometry::Attribute &b
	)
	{
		using T = std::underlying_type_t<karhu::gfx::Geometry::Attribute>;
		return static_cast<karhu::gfx::Geometry::Attribute>(static_cast<T>(a) & static_cast<T>(b));
	}

	inline constexpr karhu::gfx::Geometry::Attribute &operator|=
	(
		karhu::gfx::Geometry::Attribute &a,
		const karhu::gfx::Geometry::Attribute &b
	)
	{
		return a = a | b;
	}

	inline constexpr karhu::gfx::Geometry::Attribute &operator&=
	(
		karhu::gfx::Geometry::Attribute &a,
		const karhu::gfx::Geometry::Attribute &b
	)
	{
		return a = a & b;
	}
#endif
