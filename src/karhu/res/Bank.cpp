/**
 * @author		Ava Skoog
 * @date		2018-10-31
 * @copyright	2017-2023 Ava Skoog
 */

#include <karhu/res/Bank.hpp>
#include <karhu/res/Material.hpp>
#include <karhu/res/Shader.hpp>
#include <karhu/res/Texture.hpp>
#include <karhu/res/Mesh.hpp>
#include <karhu/res/Rig.hpp>
#include <karhu/res/Animation.hpp>
#include <karhu/res/Scene.hpp>
#include <karhu/res/Data.hpp>
#include <karhu/res/Animationcontroller.hpp>
#include <karhu/res/Animationclips.hpp>
#include <karhu/res/Inputmap.hpp>
#include <karhu/res/Font.hpp>
#include <karhu/res/Script.hpp>

namespace karhu
{
	namespace res
	{
		/**
		 * Initialises the adapter with filesystem adapters
		 * so that it can read from and write to the resource
		 * directory of the project.
		 *
		 * @param read  The filesystem input adapter.
		 * @param write The filesystem output adapter.
		 */
		Bank::Bank
		(
			std::unique_ptr<conv::AdapterFilesystemRead>  &&read,
			std::unique_ptr<conv::AdapterFilesystemWrite> &&write
		)
		:
		m_read {std::move(read)},
		m_write{std::move(write)}
		{
			IDTypeResource ID{1};
			
			#define karhuREGISTER_TYPE_RES(nameOfType, suffixOfType) \
				registerType<nameOfType>((ID ++), #nameOfType, suffixOfType);
			
			// IMPORTANT:
			// Only add new types at the end.
			// Files depend on these identifiers
			// always staying consistent in the future.
			
			karhuREGISTER_TYPE_RES(Material,            "mat")
			karhuREGISTER_TYPE_RES(Shader,              "sha")
			karhuREGISTER_TYPE_RES(Texture,             "tex")
			karhuREGISTER_TYPE_RES(Mesh,                "mes")
			karhuREGISTER_TYPE_RES(Rig,                 "rig")
			karhuREGISTER_TYPE_RES(Animation,           "ani")
			karhuREGISTER_TYPE_RES(Scene,               "sce")
			karhuREGISTER_TYPE_RES(Data,                "dat")
			karhuREGISTER_TYPE_RES(Animationcontroller, "acn")
			karhuREGISTER_TYPE_RES(Animationclips,      "acl")
			karhuREGISTER_TYPE_RES(Inputmap,            "imp")
			karhuREGISTER_TYPE_RES(Font,                "fnt")
			karhuREGISTER_TYPE_RES(Script,              "scr")
			/// @todo: Add input actions (ias).
			
			#undef karhuREGISTER_TYPE_RES
			
			#define karhuREGISTER_DERIVED(nameOfType) \
				registerDerivedType<nameOfType, nameOfType>();
			
			karhuREGISTER_DERIVED(Scene)
			karhuREGISTER_DERIVED(Data)
			karhuREGISTER_DERIVED(Animationcontroller)
			karhuREGISTER_DERIVED(Animationclips)
			karhuREGISTER_DERIVED(Inputmap)
			
			#undef karhuREGISTER_DERIVED
		}
		
		Status Bank::load()
		{
			if (!m_read)
			{
				return
				{
					false,
					"Cannot load resourcebank: "
					"no filesystem read adapter has been set"
				};
			}
			
			auto const s(m_DB.load(*m_read));
			
			if (!s.success)
				return s;
			
			return {true};
		}
		
		Status Bank::save()
		{
			if (!m_write)
			{
				return
				{
					false,
					"Cannot save resourcebank: "
					"no filesystem write adapter has been set"
				};
			}
			
			auto const s(m_DB.save(*m_write));
			
			if (!s.success)
				return s;
			
			return {true};
		}
		
		/// @todo: Try to merge with the templated version to avoid duplication.
		Resource *Bank::getByType(IDTypeResource const type, IDResource const ID)
		{
			/// @todo: Error message?
			if (!m_read)
				return nullptr;
		
			auto data(dataForType(type));
			if (!data)
				return nullptr;
		
			if (data)
				for (auto &pool : data->pools)
					if (auto r = pool.getRaw(ID))
						return reinterpret_cast<Resource *>(r);
		
			/// Try to find the instance in the database and load it
			// if it does not exist, or check if it does exist but
			// under a different type, which is considered an error.
			if (auto const entry = m_DB.get(ID))
			{
				if (type != entry->type)
					return nullptr; /// @todo: Error message?
				
				// Allocate the resource in pool.
				Resource *r{data->fCreate(ID)};
				
				if (!r)
					return nullptr; /// @todo: Error message?
				
				r->setIdentifier(ID);
				
				std::vector<std::string> sources;
				
				for (auto const &IDSource : entry->srcs)
				{
					auto const entrySource(m_DB.get(IDSource));
					
					if (!entrySource)
					{
						log::err("Karhu")
							<< "Error loading resource "
							<< static_cast<std::int64_t>(ID)
							<< ": could not find source "
							<< static_cast<std::int64_t>(IDSource);
						
						data->fDestroy(ID);
						return nullptr;
					}
					
					sources.emplace_back(entrySource->path);
				}
				
				{
					auto s(r->init(sources));
					
					if (!s.success)
					{
						log::err("Karhu")
						<< "Error loading resource "
						<< static_cast<std::int64_t>(ID)
						<< ": "
						<< s.error;
						
						data->fDestroy(ID);
						return nullptr;
					}
				}
				
				// Try to load it using its load() if we have a file read adapter.
				auto s(r->load
				(
					entry->path.c_str(),
					suffixForType(type).c_str(),
					*m_read
				));
				
				if (!s.success)
				{
					log::err("Karhu")
						<< "Error loading resource "
						<< static_cast<std::int64_t>(ID)
						<< ": "
						<< s.error;
					
					data->fDestroy(ID);
					return nullptr;
				}
				
				return r;
			}
		
			return nullptr;
		}
		
		void Bank::destroy(IDResource const ID)
		{
			for (auto &data : m_types)
				data.second.fDestroy(ID);
		}
		
		bool Bank::save(IDResource const ID)
		{
			if (!m_write)
				return false;
		
			if (auto const entry = m_DB.get(ID))
			{
				auto it(m_types.find(entry->type));
				
				if (it == m_types.end())
					return false;
				
				for (auto &pool : it->second.pools)
				{
					if (auto r = static_cast<Resource *>(pool.getRaw(ID)))
					{
						auto const s(r->save
						(
							entry->path.c_str(),
							suffixForType(entry->type).c_str(),
							*m_write
						));
						
						return s.success;
					}
				}
			}
			
			return false;
		}
		
		/**
		 * Returns the class name of the
		 * resource type by the specified ID.
		 *
		 * @param ID The unique ID number of the resource type.
		 *
		 * @return The class name of the resource type or an empty string if invalid.
		 */
		std::string Bank::nameForType(IDTypeResource const ID) const
		{
			if (auto const data = dataForType(ID))
				return data->name;
			
			return {};
		}

		/**
		 * Returns the lower case string identifier of
		 * the resource type by the specified ID.
		 *
		 * @param ID The unique ID number of the resource type.
		 *
		 * @return The string identifier of the resource type or an empty string if invalid.
		 */
		std::string Bank::stringForType(IDTypeResource const ID) const
		{
			if (auto const data = dataForType(ID))
				return data->lowercase;
			
			return {};
		}
	
		/**
		 * Returns the file name suffix of metadata files
		 * associated with the resource type by the specified ID.
		 *
		 * @param ID The unique ID number of the resource type.
		 *
		 * @return The file name suffix of the resource type or an empty string if invalid.
		 */
		std::string Bank::suffixForType(IDTypeResource const ID) const
		{
			if (auto const data = dataForType(ID))
				return data->suffix;
			
			return {};
		}
		
		/**
		 * Returns the class names of all resource types by ID.
		 *
		 * @return A map of the class names of the resource types by ID.
		 */
		std::map<IDTypeResource, std::string> Bank::namesForTypes() const
		{
			std::map<IDTypeResource, std::string> r;
			
			for (auto &t : m_types)
				r.emplace(t.first, t.second.name);
			
			return r;
		}
		
		/**
		 * Returns the lower case string identifiers of
		 * all resource types by ID.
		 *
		 * @return A map of the string identifiers of the resource types by ID.
		 */
		std::map<IDTypeResource, std::string> Bank::stringsForTypes() const
		{
			std::map<IDTypeResource, std::string> r;
			
			for (auto &t : m_types)
				r.emplace(t.first, t.second.lowercase);
			
			return r;
		}
		
		/**
		 * Returns the file name suffixes of metadata files
		 * associated with all resource types by ID.
		 *
		 * @return A map of the file name suffixes of the resource types by ID.
		 */
		std::map<IDTypeResource, std::string> Bank::suffixesForTypes() const
		{
			std::map<IDTypeResource, std::string> r;
			
			for (auto &t : m_types)
				r.emplace(t.first, t.second.suffix);
			
			return r;
		}
		
		Bank::DataForType *Bank::dataForType(IDTypeResource const ID)
		{
			auto it(m_types.find(ID));
			if (it != m_types.end())
				return &it->second;
			
			return nullptr;
		}
	
		const Bank::DataForType *Bank::dataForType(IDTypeResource const ID) const
		{
			auto const it(m_types.find(ID));
			if (it != m_types.end())
				return &it->second;
			
			return nullptr;
		}
		
		void *Bank::RefbankCustom::getPointerForReference(Reftracker const &tracker)
		{
			return bank.getByType
			(
				static_cast<ReftrackerCustom const &>(tracker).type,
				static_cast<IDResource>(tracker.identifier)
			);
		}
		
		bool Bank::RefbankCustom::onRefcountReachedZero(Reftracker const &/*tracker*/)
		{
			/// @todo: Unload resources on refcount zero?
			return false;
		}
		
		Bank::~Bank()
		{
			/// @todo: Make sure this is cleared properly.
			for (auto &data : m_types)
				for (auto &pool : data.second.pools)
					data.second.fClear(pool);
		}
		
		void *Bank::Pool::getRaw(IDResource const ID)
		{
			auto const it(m_instances.find(ID));
			if (it == m_instances.end())
				return nullptr;
			
			return m_buffer.data.get() + it->second.address;
		}

		/*
		 * Sets up any missing values in the database.
		 */
		void Bank::Database::init()
		{
			// Root needs to be an object.
			if (!m_root.object())
				m_root = conv::JSON::Obj{};
			
			// There needs to be an integer with the last used ID.
			{
				auto const last(m_root.object()->getInt("last"));
				if (!last)
				{
					m_IDLast = 1;
					m_root.object()->push({"last", m_IDLast});
				}
				else
					m_IDLast = static_cast<IDResource>(*last);
			}
			
			// There needs to be an array of free ID numbers.
			{
				auto const free(m_root.object()->getArray("free"));
				if (!free)
					m_root.object()->push({"free", conv::JSON::Arr{}});
				else
					for (auto const &v : *free)
						if (auto const ID = v.integer())
							if (static_cast<IDResource>(*ID) < m_IDLast)
								m_IDsFree.emplace_back(static_cast<IDResource>(*ID));
			}
			
			// There needs to be an array of resources.
			{
				auto resources(m_root.object()->getArray("resources"));
				if (!resources)
					resources = m_root.object()->push({"resources", conv::JSON::Arr{}}).array();
				
				// Load the entries.
				
				for (auto const &resource : *resources)
				{
					if (auto const o = resource.object())
					{
						auto const ID  (o->getInt   ("id"));
						auto const type(o->getInt   ("type"));
						auto const path(o->getString("path"));
						
						if (!ID || !type || !path)
							continue;
						
						Entry e;
						e.type = static_cast<IDTypeResource>(*type);
						e.path = *path;
						
						auto const srcs(o->getArray("srcs"));
						
						if (srcs)
						{
							for (auto const &src : *srcs)
							{
								auto const IDSource(src.integer());
								
								if (!IDSource)
									continue;
								
								e.srcs.emplace_back(static_cast<IDResource>(*IDSource));
							}
						}
						
						m_entries.emplace(*ID, std::move(e));
					}
				}
			}
		}

		/*
		 * Loads the resource database but no resources.
		 */
		Status Bank::Database::load
		(
			conv::AdapterFilesystemRead &read
		)
		{
			using namespace std::literals::string_literals;
			
			m_entries.clear();
			
			auto const data(read.readStringFromResource(subpathDB()));
			
			// We will just create it if it does not exist, so just return.
			if (!data.success)
			{
				init();
				return {true};
			}
			
			std::stringstream s;
			s << data.value;
			
			conv::JSON::Parser parser;
			
			auto r(parser.parse(s));
			if (!r)
				return {false, "JSON error in asset database: "s + parser.error()};
			
			m_root = std::move(*r);
			init();
			
//			for (auto const &e : m_entries)
//			{
//				log::msg("DEBUG")
//					<< "entry ID "
//					<< e.first
//					<< ", type "
//					<< Bank::instance()->nameForType(e.second.type)
//					<< ", path "
//					<< e.second.path;
//			}
			
			return {true};
		}

		/*
		 * Saves any updates to the resource database since load.
		 */
		Status Bank::Database::save
		(
			conv::AdapterFilesystemWrite &write
		)
		{
			using namespace std::literals::string_literals;
			
			// Update the list of free ID numbers.
			{
				auto free(m_root.object()->getArray("free"));
				
				free->clear();
				
				for (auto const &ID : m_IDsFree)
					free->push(ID);
			}

			// Update the last ID.
			{
				auto last(m_root.object()->get("last"));
				*last = m_IDLast;
			}
			
			auto const r(write.writeStringToResource
			(
				subpathDB(),
				m_root.dump().c_str()
			));
			
			if (!r.success)
				return {false, "Could not open asset database for writing: "s + r.error};
			
			return {true};
		}

		/**
		 * Adds or updates an entry for a resource in the database.
		 *
		 * @param type    The resource type.
		 * @param name    The name of the resource.
		 * @param rename  A different name in case the update is to rename the resource.
		 * @param sources The sources (raw, typeless resources) used by this resource.
		 *
		 * @return The ID of the resource in the database.
		 */
		Nullable<IDResource> Bank::Database::add
		(
			IDTypeResource          const  type,
			std::string             const &name,
			std::string             const &rename,
			bool                    const  overwrite,
			std::vector<IDResource> const &sources
		)
		{
			auto arr(m_root.object()->getArray("resources"));
			
			conv::JSON::Obj *obj{nullptr};
			IDResource ID{};
			
			// Check if the resource is already in the root.
			for (auto &res : *arr)
			{
				if (auto const o = res.object())
				{
					if (auto const p = o->getString("path"))
					{
						if (name == *p)
						{
							// Abort here if overwrite is disabled.
							if (!overwrite)
								return {false};
							
							obj = o;
							break;
						}
					}
				}
			}
			
			// Otherwise create it.
			if (!obj)
			{
				arr->push(conv::JSON::Obj{});
				obj = arr->get(arr->count() - 1)->object();
				ID = nextID();
				obj->push({"id", ID});
			}
			else if (auto const n = obj->getInt("id"))
				ID = static_cast<IDResource>(*n);

			obj->set("path", rename);
			obj->set("type", type);
			
			if (!sources.empty())
			{
				conv::JSON::Arr arr;
				
				for (auto const &source : sources)
					arr.push(source);
				
				obj->set("srcs", std::move(arr));
			}
			
			// Update the entry.
			{
				auto it(m_entries.find(ID));
				if (it != m_entries.end())
				{
					it->second.type = type;
					it->second.path = rename;
					it->second.srcs = sources;
				}
				else
				{
					Entry e;
					
					e.type = type;
					e.path = rename;
					e.srcs = sources;
					
					m_entries.emplace(ID, std::move(e));
				}
			}
			
			return {true, ID};
		}
		
		/**
		 * Tries to find and return the entry with the information
		 * about the resource by the specified ID in the database.
		 *
		 * @param ID The unique ID number of the resource.
		 *
		 * @return A nullable containing a copy of the entry on success.
		 */
		Bank::Database::Entry *Bank::Database::get(IDResource const ID)
		{
			auto it(m_entries.find(ID));
			
			if (it == m_entries.end())
				return nullptr;
			
			return &it->second;
		}
		
		Bank::Database::Entry *Bank::Database::get(std::string const &path, IDResource *outID)
		{
			auto it(std::find_if(m_entries.begin(), m_entries.end(), [&path](auto const &v)
			{
				return (v.second.path == path && !v.second.srcs.empty());
			}));
			
			if (it == m_entries.end())
				return nullptr;
			
			if (outID)
				*outID = it->first;
			
			return &it->second;
		}

		/**
		 * Removes an entry for a resource in the database.
		 *
		 * @param name The name of the resource.
		 */
		void Bank::Database::remove(std::string const &name)
		{
			auto arr(m_root.object()->getArray("resources"));
			
			// Find the resource in the database.
			for (std::size_t i{0}; i < arr->count(); ++ i)
			{
				if (auto const v = arr->get(i))
				{
					if (auto const o = v->object())
					{
						if (auto const p = o->getString("path"))
						{
							if (name == *p)
							{
								// Free the ID up for reüse.
								if (auto const ID = o->getInt("id"))
									clearID(static_cast<IDResource>(*ID));
								
								// Remove the actual entry.
								arr->erase(arr->begin() + static_cast<std::ptrdiff_t>(i));
								
								return;
							}
						}
					}
				}
			}
			
			// Remove it from the entries.
			{
				auto it(std::find_if
				(
					m_entries.begin(),
					m_entries.end(),
					[&name](auto const &v)
					{
						return (v.second.path == name);
					}
				));
				
				if (it != m_entries.end())
					m_entries.erase(it);
			}
		}

		/*
		 * Returns a resource ID that is free to use and
		 * removes it as an option, meaning that it needs
		 * to be stored in the database or cleared again.
		 */
		IDResource Bank::Database::nextID()
		{
			if (0 != m_IDsFree.size())
			{
				auto const ID(m_IDsFree.back());
				m_IDsFree.pop_back();
				return ID;
			}

			return (++ m_IDLast);
		}

		/*
		 * Clears up a resource ID so that it can be used again.
		 */
		void Bank::Database::clearID(IDResource const ID)
		{
			if (ID == m_IDLast)
				-- m_IDLast;
			else if (0 != m_IDsFree.size() && ID < m_IDLast)
			{
				auto it(std::find(m_IDsFree.begin(), m_IDsFree.end(), ID));
				
				if (it == m_IDsFree.end())
					m_IDsFree.emplace_back(ID);
			}
		}
	}
}
