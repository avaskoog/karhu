/**
 * @author		Ava Skoog
 * @date		2019-01-04
 * @copyright	2017-2023 Ava Skoog
 */

#ifndef KARHU_RES_ANIMATIONCLIPS_H_
	#define KARHU_RES_ANIMATIONCLIPS_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/res/Animation.hpp>

	namespace karhu
	{
  		namespace res
  		{
			class Animationclips : public Resource
			{
				protected:
					bool hasMetadata() const noexcept override
					{
						return true;
					}
				
					Status performInit
					(
						std::vector<std::string> const &extraPathsRelativeToSourcePath
					) override
					{
						/// @todo: Implement this if necessary.
						return {true};
					}
				
					void performSerialise(conv::Serialiser &) const override;
					void performDeserialise(const conv::Serialiser &) override;
				
				public:
					std::vector<IDResource> clips;
			};
		}
	}
#endif
