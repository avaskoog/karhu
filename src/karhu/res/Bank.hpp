/**
 * @author		Ava Skoog
 * @date		2018-10-31
 * @copyright	2017-2023 Ava Skoog
 */

/// @todo: Should we make the save/load methods on resources private and only allow the resourcebank to access them to make sure all management happens in that one place? YES.

#ifndef KARHU_RES_RESOURCEBANK_H_
	#define KARHU_RES_RESOURCEBANK_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/res/Ref.hpp>
	#include <karhu/conv/files.hpp>
	#include <karhu/conv/types.hpp>
	#include <karhu/core/Buffer.hpp>
	#include <karhu/core/Byte.hpp>

	#include <string>
	#include <map>
	#include <unordered_map>
	#include <vector>
	#include <cctype>
	#include <functional>
	#include <algorithm>
	#include <memory>

	#include <karhu/core/log.hpp>

	namespace karhu
	{		
  		namespace res
  		{
  			/**
  			 * Provides singleton access to a custom implementation
  			 * of an adapter class that does the actual fetching and
  			 * management of resources. This way toolkit and engine
  			 * can have their own implementations to suit their needs.
  			 */
			class Bank
			{
				private:
					class Pool;

				private:
					using CallbackFactoryResource = std::function<std::unique_ptr<Resource>()>;
					using CallbackCreateResource  = std::function<Resource *(const IDResource &ID)>;
					using CallbackDestroyResource = std::function<void(const IDResource &ID)>;
					using CallbackClearResources  = std::function<void(Pool &)>;
				
				private:
					class Pool
					{
						/// @todo: Figure out ways to tell if a resource pool is completely unused so that it can be deleted again?
						
						private:
							static constexpr std::size_t size() noexcept
							{
								/// @todo: Figure out the average usage and adjust pool size accordingly.
								return sizeof(Byte) * 1024;
							}
						
						public:
							struct Slot
							{
								std::size_t
									address,
									size;
							};
						
						public:
							template<typename T>
							T *create(IDResource const ID);
						
							template<typename T>
							T *get(IDResource const ID);
						
							void *getRaw(IDResource const ID);
						
							template<typename T>
							bool destroy(IDResource const ID);
						
							template<typename T>
							void clear();
						
							const std::unordered_map<IDResource, Slot> &instances() const noexcept
							{
								return m_instances;
							}
						
						private:
							template<typename T>
							T *allocate(IDResource const ID, std::size_t const address);
						
						private:
							Buffer<Byte> m_buffer
							{
								std::make_unique<Byte[]>(size()),
								size()
							};
							
							// Marks the last index used;
							// any previous indices since freed
							// will be stored in the below vector.
							std::size_t m_end{0};
							
							// Lists of slots in use.
							std::unordered_map<IDResource, Slot> m_instances;
						
							// List of slots free to use.
							std::vector<Slot> m_vacancies;
					};
				
					struct DataForType
					{	
						std::string
							name,
							lowercase,
							suffix;
						
						CallbackFactoryResource fFactory;
						CallbackCreateResource  fCreate;
						CallbackDestroyResource fDestroy;
						CallbackClearResources  fClear;
						
						std::vector<Pool> pools;
					};
				
					class Database
					{
						public:
							struct Entry
							{
								IDTypeResource          type;
								IndexResource           index{-1};
								std::string             path;
								std::vector<IDResource> srcs;
							};
						
						public:
							Status load(conv::AdapterFilesystemRead &);
							Status save(conv::AdapterFilesystemWrite &);
						
							Nullable<IDResource> add
							(
								IDTypeResource           const  type,
								std::string              const &name,
								std::string              const &rename,
								bool                     const  overwrite = true,
								std::vector<IDResource>  const &sources   = {}
							);
						
							Entry *get(const IDResource ID);
							Entry *get(std::string const &path, IDResource *outID = nullptr);
						
							std::map<IDResource, Entry> const &all() const noexcept
							{
								return m_entries;
							}
						
							void remove(std::string const &name);
							/// @todo: Probably want remove by ID as well...
					
						private:
							void init();
							IDResource nextID();
							void clearID(IDResource const);
						
						private:
							conv::JSON::Val m_root; /// @todo: Should we even store the JSON at all now that we have the entries and everything else in separate data structures?
						
							std::vector<IDResource> m_IDsFree;
							IDResource m_IDLast;
						
							std::map<IDResource, Entry> m_entries;
					} m_DB;
				
				public:
					static constexpr char const *subpathDB() noexcept
					{
						/// @todo: Should we turn this into a hidden . file instead of a special _ file?
						return "_info.karhuresdb.json";
					}
				
				private:
					template<typename T>
					static IDTypeResource getOrSetIDFor(const IDTypeResource &ID = 0)
					{
						static IDTypeResource value{0};
						
						if (ID != 0)
							value = ID;
						
						return value;
					}
				
				public:
					Bank() = delete;
				
					Bank
					(
						std::unique_ptr<conv::AdapterFilesystemRead>  &&read,
						std::unique_ptr<conv::AdapterFilesystemWrite> &&write
					);
				
					Database &database() noexcept { return m_DB; }
					Database const &database() const noexcept { return m_DB; }
				
					Status load();
					Status save();
				
					template<typename T>
					Nullable<IDResource> create(const std::string &name = {});
				
					void destroy(IDResource const ID);
				
					bool save(IDResource const ID);
			
					template<typename T>
					std::unique_ptr<T> createNonowned();
				
					/// @todo: Should we be able to get resources by name?
				
					template<typename T>
					T *get(IDResource const);
				
					template<typename T>
					Ref<T> ref(IDResource const);
				
					Resource *getByType(IDTypeResource const, IDResource const);
				
					/// @todo: Not sure if keeping this but adding it to make the transition easy for now.
					template<typename T>
					auto getAll()
					{
						std::map<IDResource, T *> r;
						
						if (auto data = dataForType(IDOfType<T>()))
							for (auto &pool : data->pools)
								for (const auto &instance : pool.instances())
									if (const auto obj = pool.template get<T>(instance.first))
										r.emplace(instance.first, obj);
						
						return r;
					}
				
					std::string nameForType(IDTypeResource const ID) const;
					std::string stringForType(IDTypeResource const ID) const;
					std::string suffixForType(IDTypeResource const ID) const;
				
					std::map<IDTypeResource, std::string> namesForTypes() const;
					std::map<IDTypeResource, std::string> stringsForTypes() const;
					std::map<IDTypeResource, std::string> suffixesForTypes() const;
				
					/**
					 * Returns the unique ID number of the resource type
					 * specified by the template argument, or 0 if invalid.
					 *
					 * @return The unique ID number of the resource type.
					 */
					template<typename T>
					IDTypeResource IDOfType() const { return getOrSetIDFor<T>(); }
				
					/**
					 * Returns the class name of the resource
					 * type specified by the template argument.
					 *
					 * @return The class name of the resource type or an empty string if invalid.
					 */
					template<typename T>
					std::string nameForType() const { return nameForType(IDOfType<T>()); }
				
					/**
					 * Returns the lower case string identifier of the
					 * resource type specified by the template argument.
					 *
					 * @return The string identifier of the resource type or an empty string if invalid.
					 */
					template<typename T>
					std::string stringForType() const { return stringForType(IDOfType<T>()); }
				
					/**
					 * Returns the file name suffix of metadata files associated
					 * with the resource type specified by the template argument.
					 *
					 * @return The file name suffix of the resource type or an empty string if invalid.
					 */
					template<typename T>
					std::string suffixForType() const { return suffixForType(IDOfType<T>()); }
				
					template<typename T, typename TDerived>
					void registerDerivedType();
				
					template<typename T>
					void registerType
					(
						const IDTypeResource &ID,
						const char *name,
						const char *suffix
					);
				
					virtual ~Bank();
				
				//private:
					/// @todo: Do these even need to be accessible to derived classes at this point?
					conv::AdapterFilesystemRead  *read()  noexcept { return m_read.get(); }
					conv::AdapterFilesystemWrite *write() noexcept { return m_write.get(); }
				
				private:
					template<typename TDerived>
					void setTypeinfo(DataForType &data);
				
					template<typename T, typename TDerived>
					std::unique_ptr<T> factoryResource();
				
					template<typename T, typename TDerived>
					T *createResource(IDResource const ID);
				
					template<typename T, typename TDerived>
					void destroyResource(IDResource const ID);
				
					DataForType *dataForType(IDTypeResource const ID);
					const DataForType *dataForType(IDTypeResource const ID) const;
				
				private:
					class RefbankCustom : public Refbank
					{
						public:
							RefbankCustom(Bank &bank) : bank{bank} {}
							Bank &bank;
						
						protected:
							virtual void *getPointerForReference(Reftracker const &) override;
							virtual bool onRefcountReachedZero(Reftracker const &) override;
					} m_refbank{*this};
				
					class ReftrackerCustom : public Reftracker
					{
						public:
							ReftrackerCustom(Refbank &bank, IDRef const identifier, IDTypeResource type)
							:
							Reftracker{bank, identifier},
							type      {type}
							{
							}
						
						public:
							IDTypeResource const
								type;
					};
				
				private:
					std::unique_ptr<conv::AdapterFilesystemRead>  m_read;
					std::unique_ptr<conv::AdapterFilesystemWrite> m_write;
				
					std::map<IDTypeResource, DataForType> m_types;
			};
			
			/**
			 * Tries to create a new resource of the specified
			 * type and returns the ID on success.
			 *
			 * If no name is specified, a temporary oen will
			 * be generated, which is suitable for runtime resources
			 * that are not meant to be serialised to disk.
			 *
			 * If the name has already been taken, creation will fail.
			 *
			 * @param name An optional resource name.
			 *
			 * @return A nullable struct containing the ID of the new resource on success.
			 */
			template<typename T>
			Nullable<IDResource> Bank::create(const std::string &name)
			{
				static std::uint64_t counter{0};
				
				std::stringstream s;
				
				if (0 == name.length())
					s << "__tmp-" << (counter ++);
				else
					s << name;
				
				return m_DB.add(IDOfType<T>(), s.str(), s.str(), false);
			}
			
			/**
			 * Tries to find and return a pointer to the
			 * resource by the specified type and ID.
			 * Do not hold on to this pointer.
			 *
			 * @param ID The unique ID number of the resource.
			 *
			 * @return A pointer to the resource if found, or null.
			 */
			template<typename T>
			T *Bank::get(IDResource const ID)
			{
				static_assert
				(
					(
						std::is_base_of<Resource, T>{} &&
						!std::is_same<Resource, T>{}
					),
					"Resource type must inherit Resource!"
				);
				
				/// @todo: Error message?
				if (!m_read)
					return nullptr;
				
				auto data(dataForType(IDOfType<T>()));
				
				if (!data)
					return nullptr;
				
				if (data)
					for (auto &pool : data->pools)
						if (auto r = pool.template get<T>(ID))
							return r;
				
				/// Try to find the instance in the database and load it
				// if it does not exist, or check if it does exist but
				// under a different type, which is considered an error.
				if (const auto entry = m_DB.get(ID))
				{
					if (IDOfType<T>() != entry->type)
						return nullptr; /// @todo: Error message?
				
					// Allocate the resource in pool.
					auto r(dynamic_cast<T *>(data->fCreate(ID)));
					
					if (!r)
						return nullptr; /// @todo: Error message?
				
					r->setIdentifier(ID);
					
					/// @todo: Just make room for entry->srcs.size() and check it doesn't overflow?
					std::vector<std::string> sources;
				
					for (auto const &IDSource : entry->srcs)
					{
						auto const entrySource(m_DB.get(IDSource));
						
						if (!entrySource)
						{
							log::err("Karhu")
								<< "Error loading resource "
								<< static_cast<std::int64_t>(ID)
								<< ": could not find source "
								<< static_cast<std::int64_t>(IDSource);
							
							data->fDestroy(ID);
							return nullptr;
						}
						
						sources.emplace_back(entrySource->path);
					}
					
					{
						auto s(r->init(sources));
						
						if (!s.success)
						{
							log::err("Karhu")
							<< "Error loading resource "
							<< static_cast<std::int64_t>(ID)
							<< ": "
							<< s.error;
							
							data->fDestroy(ID);
							return nullptr;
						}
					}
					
					auto s(r->load
					(
						entry->path.c_str(),
					 	suffixForType<T>().c_str(),
					 	*m_read
					));
					
					if (!s.success)
					{
						log::err("Karhu")
							<< "Error loading resource "
							<< static_cast<std::int64_t>(ID)
							<< ": "
							<< s.error;
						
						data->fDestroy(ID);
						return nullptr;
					}
					
					return r;
				}
				
				return nullptr;
			}
			
			template<typename T>
			std::unique_ptr<T> Bank::createNonowned()
			{
				static_assert
				(
					(
						std::is_base_of<Resource, T>{} &&
						!std::is_same<Resource, T>{}
					),
					"Resource type must inherit Resource!"
				);
				
				/// @todo: Error message?
				if (!m_read)
					return {};
				
				auto data(dataForType(IDOfType<T>()));
				
				if (!data)
					return {};
				
				auto r(data->fFactory());
				
				if (!r)
					return {};
				
				auto p(dynamic_cast<T *>(r.get()));
				
				if (!p)
					return {};
				
				r.release();
				
				return std::unique_ptr<T>{p};
			}
			
			template<typename T>
			Ref<T> Bank::ref(IDResource const identifier)
			{
				return m_refbank.createReference<Ref<T>, ReftrackerCustom>(identifier, IDOfType<T>());
			}
	
			/**
			 * Call this in the constructor of a derived resourcebank
			 * implementation in order to "overload" the exact type
			 * used for a particular resource type by specifying the
			 * base type (e.g. Texture) as the first template
			 * argument and the derived type (e.g. a class that
			 * inherits Texture and is the one you actually
			 * want to get allocated) as the second.
			 */
			template<typename T, typename TDerived>
			void Bank::registerDerivedType()
			{
				if (auto data = dataForType(IDOfType<T>()))
				{
					data->fFactory = [this]()
					{
						return factoryResource<T, TDerived>();
					};
					
					data->fCreate = [this](const IDResource &ID)
					{
						return createResource<T, TDerived>(ID);
					};
					
					data->fDestroy = [this](const IDResource &ID)
					{
						destroyResource<T, TDerived>(ID);
					};
					
					data->fClear = [](Pool &pool)
					{
						pool.clear<TDerived>();
					};
				}
			}
			
			template<typename T>
			void Bank::registerType
			(
				const IDTypeResource &ID,
				const char *name,
				const char *suffix
			)
			{
				using namespace std::literals::string_literals;

				static_assert
				(
					(
						std::is_base_of<Resource, T>{} &&
						!std::is_same<Resource, T>{}
					),
					"Resource type must inherit Resource!"
				);
				
				if (0 == ID || 0 != IDOfType<T>())
					return;
				
				// Register the ID.
				getOrSetIDFor<T>(ID);
				
				// Start filling in the data we need...
				DataForType data;
				
				// The class name for editor and such.
				data.name = name;
				
				// The lowercase name for tools.
				data.lowercase.resize(data.name.length());
				std::transform
				(
					data.name.begin(),
					data.name.end(),
					data.lowercase.begin(),
					[](const auto &c) { return std::tolower(c); }
				);
				
				// The metadata file name suffix.
				data.suffix =
				(
					".karhu"s +
					suffix +
					".json"
				);
				
				m_types.emplace(ID, std::move(data));
			}
		
			template<typename T, typename TDerived>
			std::unique_ptr<T> Bank::factoryResource()
			{
				auto r(std::make_unique<TDerived>());
				r->m_typeResource = IDOfType<T>();
				return r;
			}
			
			/*
			 * Tries to find space for the resource in every pool,
			 * creating a new one if necessary, and then allocates
			 * an instance with the specified ID.
			 */
			template<typename T, typename TDerived>
			T *Bank::createResource(IDResource const ID)
			{
				if (auto data = dataForType(IDOfType<T>()))
				{
					for (auto &pool : data->pools)
						if (auto r = pool.template create<TDerived>(ID))
							return r;
				
					data->pools.emplace_back();
					return data->pools.back().template create<TDerived>(ID);
				}
				
				/// @todo: Error message?
				return nullptr;
			}
		
			/*
			 * Finds the pool with the instance and destroys it.
			 */
			template<typename T, typename TDerived>
			void Bank::destroyResource(IDResource const ID)
			{
				if (auto data = dataForType(IDOfType<T>()))
					for (auto &pool : data->pools)
						if (pool.template destroy<TDerived>(ID))
							return;
				/// @todo: Else error message?
			}
			
			/*
			 * Tries to find space for an instance of the specified type
			 * in the pool and if so allocates it and returns the pointer.
			 * Otherwise returns null.
			 */
			template<typename T>
			T *Bank::Pool::create(IDResource const ID)
			{
				// If there is still room at the end, stick it in.
				if ((m_buffer.size - m_end) >= sizeof(T))
				{
					T *r{allocate<T>(ID, m_end)};
					m_instances.emplace(ID, Slot{m_end, sizeof(T)});
					m_end = std::min(m_end + sizeof(T), m_buffer.size);
					return r;
				}
				
				// Find the vacancy with the snuggest fit.
				// We keep the list ordered, so the first match is always it.
				for (std::size_t i{0}; i < m_vacancies.size(); ++ i)
				{
					if (m_vacancies[i].size >= sizeof(T))
					{
						T *r{allocate<T>(ID, m_vacancies[i].address)};
						m_instances.emplace(ID, Slot{m_vacancies[i].address, sizeof(T)});
						m_vacancies.erase(m_vacancies.begin() + static_cast<std::ptrdiff_t>(i));
						return r;
					}
				}
				
				return nullptr;
			}
			
			/**
			 * Tries to find the resource by the specified ID in the pool.
			 *
			 * @param ID The unique ID number of the instance.
			 *
			 * @return A pointer to the instance if found or null.
			 */
			template<typename T>
			T *Bank::Pool::get(IDResource const ID)
			{
				const auto it(m_instances.find(ID));
				
				if (it == m_instances.end())
					return nullptr;
				
				return reinterpret_cast<T *>(m_buffer.data.get() + it->second.address);
			}
 
			/*
			 * Tries to find the instance with the specified ID in
			 * the pool and if so destroys it and returns true.
			 */
			template<typename T>
			bool Bank::Pool::destroy(IDResource const ID)
			{
				auto it(m_instances.find(ID));
				if (it == m_instances.end())
					return false;
				
				// Call the destructor.
				reinterpret_cast<T *>(m_buffer.data.get() + it->second.address)->~T();
				
				// Turn the instance into a vacancy.
				m_vacancies.emplace_back(std::move(it->second));
				m_instances.erase(it);
				
				// Keep vacancies sorted by size so they are easily found.
				std::sort
				(
					m_vacancies.begin(),
					m_vacancies.end(),
					[](const auto &a, const auto &b)
					{
						return (a.size < b.size);
					}
				);
				
				return true;
			}
 
			template<typename T>
			T *Bank::Pool::allocate
			(
				IDResource  const ID,
				std::size_t const address
			)
			{
				new(m_buffer.data.get() + address) T{};
				m_instances.emplace(ID, Slot{address, sizeof(T)});
				return reinterpret_cast<T *>(m_buffer.data.get() + address);
			}
			
			template<typename T>
			void Bank::Pool::clear()
			{
				/// @todo: Make sure resource pool destructor is working properly.
				while (m_instances.size() > 0)
					destroy<T>(m_instances.begin()->first);
				
				m_instances.clear();
				m_vacancies.clear();
				
				m_end = 0;
			}
		}
	}
#endif
