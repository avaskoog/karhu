/**
 * @author		Ava Skoog
 * @date		2017-07-12
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_NETWORK_MESSENGER_H_
	#define KARHU_NETWORK_MESSENGER_H_

	#include <karhu/core/string.hpp>
	#include <karhu/core/log.hpp>

	#include <array>
	#include <vector>
	#include <queue>

	namespace karhu
	{
		namespace network
		{
			/**
			 * Messages are sent and received over the network.
			 * The length of a message can exceed the buffer size,
			 * in which case it will be split up into multiple
			 * buffers, but the whole message can be returned as
			 * one string as well.
			 *
			 * @see Messager for dealing with messages.
			 */
			template<typename TChar, std::size_t SizeBuffer>
			class Message
			{
				static_assert(string::validTypeChar<TChar>(), "Invalid character type");

				public:
					using Buffer  = std::array<TChar, SizeBuffer>;
					using TString = std::basic_string<TChar>;

				public:
					static constexpr std::size_t buffersize() noexcept { return SizeBuffer; }

				public:
					Message(const TString &text = {});
					Message(const Message &) = default;
					Message(Message &&) = default;

					const std::vector<Buffer> &buffers() const noexcept { return m_buffers; }
					TString text() const;
				
				private:
					void add(const Buffer &b) { m_buffers.emplace_back(b); }
					void clear() { m_buffers.clear(); }
				
				private:
					std::vector<Buffer> m_buffers;
				
				template<typename, std::size_t>
				friend class Messenger;
			};

			/**
			 * This abstract class needs to be implemented for
			 * the underlying networking structure in order to
			 * work properly, but all the message handling is
			 * already implemented so as to be consistent.
			 *
			 * There is no safety checking when popping messages,
			 * so has() needs to be called manually first to
			 * make sure there are messages left.
			 *
			 * Any data that might be necessary for the messenger's
			 * implementation can be passed on construction; the
			 * constructor of Interface takes a data pointer that
			 * is passed on if filled in by a derived class. This
			 * data is then retrieved internally using data().
			 */
			template<typename TChar, std::size_t SizeBuffer>
			class Messenger
			{
				static_assert(string::validTypeChar<TChar>(), "Invalid character type");

				public:
					using TMessage = Message<TChar, SizeBuffer>;
					using TBuffer  = typename TMessage::Buffer;
					using TString  = std::basic_string<TChar>;

				public:
					static constexpr std::size_t buffersize() noexcept { return SizeBuffer; }

				public:
					Messenger(void *data = nullptr) : m_data{data} {}

					bool read();
					bool send(const TString &message);
					bool send(const TMessage &message);

					bool has() const noexcept { return !m_messages.empty(); }
					TMessage pop();

					virtual ~Messenger() = default;
				
				protected:
					void *data() const noexcept { return m_data; }
				
				protected:
					/**
					 * Implements actually getting a buffer's worth of
					 * data from the networked stream.
					 *
					 * Will be used in a while loop so needs to return
					 * false not just on failure to retrieve data, but
					 * also when there is no data left to read.
					 *
					 * @param target The target to place the data into.
					 *
					 * @return Whether successful or any data left.
					 */
					virtual bool performRead(TBuffer &target) = 0;

					/**
					 * Implements sending a buffer across the network.
					 *
					 * @param data Buffer to send.
					 *
					 * @return Whether sending was successful.
					 */
					virtual bool performSend(const TBuffer &data) = 0;

				private:
					void *m_data{nullptr};
					TMessage m_messageCurrent;
					std::queue<TMessage> m_messages;
			};
			
			template<typename TChar, std::size_t SizeBuffer>
			Message<TChar, SizeBuffer>::Message(const TString &text)
			{
				if (text.length() == 0) return;

				// If necessary, use multiple buffers to assemble the message.
				// The use of size() rather than length() here is intentional.
				const std::size_t count{text.size()};
				for (std::size_t i{0}; i < count; i += buffersize())
				{
					// The braces ensure initialisation to 0.
					Buffer b{};

					std::copy
					(
						text.begin() + static_cast<std::ptrdiff_t>(i),
						text.begin() + static_cast<std::ptrdiff_t>(i + std::min(text.length() - i, buffersize())),
						b.begin()
					);
					
					m_buffers.emplace_back(std::move(b));
				}

				// If the message filled up the buffers precisely we
				// need an extra empty buffer for termination.
				if (text.length() % buffersize() == 0)
					m_buffers.emplace_back(Buffer{});
			}
			
			template<typename TChar, std::size_t SizeBuffer>
			typename Message<TChar, SizeBuffer>::TString Message<TChar, SizeBuffer>::text() const
			{
				TString s;
				
				for (auto &i : m_buffers)
				{
					for (auto &j : i)
					{
						if (j != TChar{0})
							s.push_back(j);
						else
							return s;
					}
				}
				
				return s;
			}
			
			template<typename TChar, std::size_t SizeBuffer>
			bool Messenger<TChar, SizeBuffer>::read()
			{
				TBuffer b{};

				while (performRead(b))
				{
					// Make sure the buffer is not empty, unless
					// if it is to terminate an unfinished message.
					if (b[0] != TChar{0} || m_messageCurrent.buffers().size() > 0)
					{
						m_messageCurrent.add(b);

						// Check backwards. Faster.
						for (int i{buffersize() - 1}; i >= 0; -- i)
						{
							// Null terminator means end of message.
							if (b[static_cast<std::size_t>(i)] == TChar{0})
							{
								// Push it on the queue and start on a new one.
								m_messages.push(m_messageCurrent);
								m_messageCurrent.clear();
								break;
							}
						}
					}
				}
				
				return true;
			}
			
			template<typename TChar, std::size_t SizeBuffer>
			bool Messenger<TChar, SizeBuffer>::send(const TString &message)
			{
				return send(TMessage{message});
			}
			
			template<typename TChar, std::size_t SizeBuffer>
			bool Messenger<TChar, SizeBuffer>::send(const TMessage &message)
			{
				bool success{true};
				
				// Keep going even if there is a failure along the way.
				// No need to lose everything because one thing is lost.
				for (auto &i : message.buffers())
					success &= performSend(i);
				
				return success;
			}
			
			template<typename TChar, std::size_t SizeBuffer>
			typename Messenger<TChar, SizeBuffer>::TMessage Messenger<TChar, SizeBuffer>::pop()
			{
				auto m(m_messages.front());
				m_messages.pop();
				return m;
			}
		}
	}
#endif
