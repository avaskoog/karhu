/**
 * @author		Ava Skoog
 * @date		2017-07-20
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_NETWORK_PROTOCOL_H_
	#define KARHU_NETWORK_PROTOCOL_H_

	#include <karhu/conv/types.hpp>
	#include <karhu/core/Nullable.hpp>
	#include <karhu/core/platform.hpp>
	#include <karhu/core/log.hpp>

	namespace karhu
	{
		namespace network
		{
			namespace protocol
			{
				inline constexpr auto          defaulthost() noexcept { return "localhost"; }
				inline constexpr std::uint16_t defaultport() noexcept { return 14300; }

				inline constexpr std::size_t buffersize() noexcept { return 256; }

				namespace server
				{
					inline constexpr std::uint16_t port(const Platform system, const std::uint16_t offset) noexcept
					{
						std::uint16_t c{0};
						for (auto v(static_cast<std::uint16_t>(system)); (v >> (++ c)) != 0;);
						return offset + c * 10;
					}
				}

				namespace client
				{
					inline constexpr std::uint16_t port(const Platform system, const std::uint16_t offset) noexcept
					{
						std::uint16_t c{0};
						for (auto v(static_cast<std::uint16_t>(system)); (v >> (++ c)) != 0;);
						std::uint16_t r(offset + c * 10);
						if (Platform::web == system) r += 1;
						return r;
					}
				}
			}

			/**
			 * This protocol is used by the tools' server and client alike
			 * to be on the same page when talking to each other, and to
			 * ensure consistent management of JSON data sent between them.
			 */
			class Protocol
			{
				public:
					/**
					 * The type of message to serialise.
					 */
					enum class TypeMessage
					{
						invalid = -1,
						other,
						string,
						log,
						module,
						debug
					};

					/**
					 * A message with a type and data.
					 *
					 * If the type is set to invalid, the data will be
					 * a null JSON value, and the error string will be
					 * filled in.
					 */
					struct Message
					{
						TypeMessage     type;
						conv::JSON::Val data;
						conv::String    error;
					};
				
					struct MessageLog
					{
						log::Level   level;
						conv::String tag, text;
					};

				public:
					/// @todo Get MOVE/FORWARD versions in nicely somehow.

					/**
					 * Serialises a log message to string.
					 *
					 * @param level   The log level.
					 * @param tag     The log tag.
					 * @param message The log message.
					 *
					 * @return A serialised JSON string.
					 */
					static conv::String serialiseLog
					(
						const log::Level level,
						const conv::String &tag,
						const conv::String &message
					)
					{
						// Drop the tag if not needed.
						conv::JSON::Obj obj
						{{
							{"m", message},
							{"l", static_cast<conv::Int>(level)}
						}};
						if (tag.length() > 0) obj.push({"t", tag});

						return serialise
						(
							TypeMessage::log,
							std::move(obj)
						);
					}
					
					/**
					 * Serialises any message type to string.
					 *
					 * @param type The message type.
					 *
					 * @return A serialised JSON string.
					 */
					static conv::String serialise
					(
						const TypeMessage type
					)
					{
						return conv::JSON::Val{conv::JSON::Obj{{"t", static_cast<conv::Int>(type)}}}.serialise();
					}

					/**
					 * Serialises any JSON object and the message type to string.
					 *
					 * @param type The message type.
					 * @param data The JSON data.
					 *
					 * @return A serialised JSON string.
					 */
					static conv::String serialise
					(
						const TypeMessage type,
						const conv::JSON::Val &data
					)
					{
						return conv::JSON::Val{conv::JSON::Obj
						{{
							{"t", static_cast<conv::Int>(type)},
							{"d", data}
						}}}.serialise();
					}

					static Nullable<Message> deserialise(const conv::String &data)
					{
						conv::JSON::Parser p;
						std::basic_istringstream<conv::Char> s{data};

						if (auto val = p.parse(s))
						{
							// Top level needs to be an object.
							if (auto o = val->object())
							{
								// Needs to have a type.
								auto t(o->get("t"));
								if (t && t->integer())
								{
									// Data is optional.
									auto d(o->get("d"));

									const auto type(static_cast<TypeMessage>(*t->integer()));

									if (d)
										return {true, type, std::move(*d)};
									else
										return {true, type, nullptr};
								}
							}
						}

						return {false, TypeMessage::invalid, nullptr, p.error()};
					}

					/**
					 * Decodes a log message and fills in a struct with
					 * level, tag and text according to the JSON structure.
					 *
					 * @param m  The message containing the log information.
					 * @param om The struct to output the values into.
					 *
					 * @return Whether anything was successfully logged.
					 */
					static bool log(const Message &m, MessageLog &om)
					{
						if (const auto o = m.data.object())
						{
							const auto l(o->get("l"));
							const auto t(o->get("t"));
							const auto m(o->get("m"));
							
							if (m && m->string())
							{
								om.level =
								(
									(l && l->integer())
									?
										static_cast<log::Level>(*l->integer())
									:
										log::Level::msg
								);
								
								om.tag =
								(
									(t && t->string())
									?
										*t->string()
									:
										conv::String{}
								);
								
								om.text = *m->string();
								
								return true;
							}
						}
						
						return false;
					}
			};
		}
	}
#endif
