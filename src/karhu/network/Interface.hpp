/**
 * @author		Ava Skoog
 * @date		2017-07-13
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_NETWORK_INTERFACE_H_
	#define KARHU_NETWORK_INTERFACE_H_

	#include <karhu/network/Messenger.hpp>
	#include <karhu/network/Protocol.hpp>
	#include <karhu/core/platform.hpp>

	#include <type_traits>
	#include <cstdint>
	#include <atomic>

	namespace karhu
	{
		namespace network
		{
			enum class TypeInterface : std::uint8_t
			{
				server,
				client
			};
			
			enum class State : std::uint8_t
			{
				disconnected,
				connecting,
				connected
			};

			/**
			 * Implements a synchronous server interface to send and
			 * receive messages either as a client or as a server.
			 *
			 * A class derived from Messenger also needs to be
			 * implemented in order for the messaging to work.
			 *
			 * Any data that might be necessary for the messenger's
			 * implementation can be passed on construction.
			 * @see Messenger for more information.
			 */
			template<class TMessenger, TypeInterface Type>
			class Interface
			{
				public:
					/**
					 * Starts the server or client with the settings specified.
					 *
					 * @param system System on which the client runs (affects the port).
					 * @param port   Port offset to which another offset is added depending on client's system.
					 * @param host   Host (IP or name) of server.
					 *
					 * @return True if exited properly, false otherwise.
					 */
					bool connect
					(
						const Platform system       = Platform::current,
							  std::uint16_t    port = network::protocol::defaultport(),
						const std::string     &host = network::protocol::defaulthost()
					);
				
					/**
					 * Starts the server or client with the settings specified
					 * without stalling execution until the connection has been
					 * established; the only threadsafe value to check is connected().
					 *
					 * @param system System on which the client runs (affects the port).
					 * @param port   Port offset to which another offset is added depending on client's system.
					 * @param host   Host (IP or name) of server.
					 *
					 * @return True if exited properly, false otherwise.
					 */
					bool connectAsync
					(
						const Platform system       = Platform::current,
							  std::uint16_t    port = network::protocol::defaultport(),
						const std::string     &host = network::protocol::defaulthost()
					);
				
					void disconnect();
				
					void update();
				
					State state() const { return m_state; }

					TMessenger &messenger() noexcept { return m_messenger; }
					const TMessenger &messenger() const noexcept { return m_messenger; }

					virtual ~Interface() = default;
				
				protected:
					Interface(void *data = nullptr) : m_messenger{data} {}

					/**
					 * Connects according to the specified settings.
					 *
					 * @param system System on which the client runs.
					 * @param port   The final port to connect to.
					 * @param host   Host (IP or name) of server.
					 *
					 * @return True if exited properly, false otherwise.
					 */
					virtual bool performConnect
					(
						const Platform       system,
						const std::uint16_t  port,
						const std::string   &host
					) = 0;

					/**
					 * Connects according to the specified settings
					 * without stalling the execution.
					 *
					 * @param system System on which the client runs.
					 * @param port   The final port to connect to.
					 * @param host   Host (IP or name) of server.
					 *
					 * @return True if exited properly, false otherwise.
					 */
					virtual bool performConnectAsync
					(
						const Platform       system,
						const std::uint16_t  port,
						const std::string   &host
					) = 0;
				
					/**
					 * Called to actively disconnect.
					 */
					virtual void performDisconnect() = 0;
				
					/*
					 * Called when externally disconnected so that any internal
					 * implementation-specific states can be updated properly.
					 */
					virtual void onDisconnect() = 0;
				
					virtual void onUpdate() = 0;

					virtual bool updateAfterMessages() = 0;
				
					/**
					 * Determines what happens when a message is received.
					 * Use the internal derived messenger to send messages.
					 *
					 * @param message The message received.
					 *
					 * @return False in case of an error that should disconnect the interface and stop the loop.
					 */
					virtual bool receive(const typename TMessenger::TMessage &message) = 0;
				
				protected:
					std::atomic<State> m_state{State::disconnected};

				private:
					TMessenger m_messenger;
			};
			
			template<class TMessenger, TypeInterface Type>
			bool Interface<TMessenger, Type>::connect
			(
				const Platform       system,
					  std::uint16_t  port,
				const std::string   &host
			)
			{
				namespace client = network::protocol::client;
				namespace server = network::protocol::server;

				port = (TypeInterface::server == Type) ? server::port(system, port) : client::port(system, port);
				
				m_state = State::connecting;
				
				if (!performConnect(system, port, host))
				{
					m_state = State::disconnected;
					onDisconnect();
					return false;
				}
				
				m_state = State::connected;
				return true;
			}
			
			template<class TMessenger, TypeInterface Type>
			bool Interface<TMessenger, Type>::connectAsync
			(
				const Platform       system,
					  std::uint16_t  port,
				const std::string   &host
			)
			{
				namespace client = network::protocol::client;
				namespace server = network::protocol::server;

				port = (TypeInterface::server == Type) ? server::port(system, port) : client::port(system, port);
				
				m_state = State::connecting;
				
				if (!performConnectAsync(system, port, host))
				{
					m_state = State::disconnected;
					onDisconnect();
					return false;
				}
				
				return true;
			}
			
			template<class TMessenger, TypeInterface Type>
			void Interface<TMessenger, Type>::disconnect()
			{
				performDisconnect();
			}
			
			template<class TMessenger, TypeInterface Type>
			void Interface<TMessenger, Type>::update()
			{
				onUpdate();
				
				if (State::connected != m_state)
					return;

				messenger().read();

				while (messenger().has())
				{
					if (!receive(messenger().pop()))
					{
						m_state = State::disconnected;
						onDisconnect();
						return;
					}
				}
				
				if (!updateAfterMessages())
				{
					m_state = State::disconnected;
					onDisconnect();
					return;
				}
			}
		}
	}
#endif
