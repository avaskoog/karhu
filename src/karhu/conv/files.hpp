/**
 * @author		Ava Skoog
 * @date		2018-10-28
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: Should this be moved over to proj/? Especially since it only access resources anyway? If we do rename dir and/or namespace to res too?

#ifndef KARHU_CONV_FILES_H_
	#define KARHU_CONV_FILES_H_

	#include <karhu/core/Byte.hpp>
	#include <karhu/core/Buffer.hpp>
	#include <karhu/core/result.hpp>
	
	#include <string>

	namespace karhu
	{
  		namespace conv
  		{
			enum class ModeFilesystemWrite
			{
				truncate,
				append
			};
			
  			/**
  			 * Defines an interface for reading from the file system.
  			 * Engine and toolkit use different underlying mechanisms but
  			 * sometimes need to access files using a shared interface,
  			 * such as the assets in a project, and so need to implement
  			 * their own adapters.
  			 */
  			class AdapterFilesystemRead
  			{
				public:
					/**
					 * Loads and returns the contents of a text file from the
					 * specified path, relative to the project's resource directory.
					 *
					 * @param filepathRelativeToResourcepath The relative file path.
					 *
					 * @return A struct containing the string contents of the file on success and an error message on failure.
					 */
					virtual Result<std::string> readStringFromResource
					(
						char const *filepathRelativeToResourcepath
					) = 0;
				
					/**
					 * Loads and returns the contents of a binary file from the
					 * specified path, relative to the project's resource directory.
					 *
					 * @param filepathRelativeToResourcepath The relative file path.
					 *
					 * @return A struct containing a buffer with the bytes and size of the binary contents of the file on success and an error message on failure.
					 */
					virtual Result<Buffer<Byte>> readBytesFromResource
					(
						char const *filepathRelativeToResourcepath
					) = 0;
				
					virtual ~AdapterFilesystemRead() = default;
			};
			
  			/**
  			 * Defines an interface for writing from the file system.
  			 * Engine and toolkit use different underlying mechanisms but
  			 * sometimes need to access files using a shared interface,
  			 * such as the assets in a project, and so need to implement
  			 * their own adapters.
  			 */
  			class AdapterFilesystemWrite
  			{
				public:
					/**
					 * Overwrites the string contents of the text file at the
					 * specified path, relative to the project's resource directory.
					 *
					 * @param filepathRelativeToResourcepath The relative file path.
					 * @param data                           The string contents to write.
					 * @param mode                           Whether to truncate or append to the file; defaults to truncate if omitted.
					 *
					 * @return A struct containing the success state and an optional error message on failure.
					 */
					virtual Status writeStringToResource
					(
						char                const *filepathRelativeToResourcepath,
						char                const *data,
						ModeFilesystemWrite const  mode = ModeFilesystemWrite::truncate
					) = 0;
				
					/**
					 * Overwrites the byte contents of the binary file at the
					 * specified path, relative to the project's resource directory.
					 *
					 * @param filepathRelativeToResourcepath The relative file path.
					 * @param data                           A byte buffer struct containing the binary contents to write and the size.
					 * @param mode                           Whether to truncate or append to the file; defaults to truncate if omitted.
					 *
					 * @return A struct containing the success state and an optional error message on failure.
					 */
					virtual Status writeBytesToResource
					(
						char                const *filepathRelativeToResourcepath,
						Buffer<Byte>        const &data,
						ModeFilesystemWrite const  mode = ModeFilesystemWrite::truncate
					) = 0;
				
					virtual ~AdapterFilesystemWrite() = default;
			};
  		}
	}
#endif
