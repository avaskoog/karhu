/**
 * @author		Ava Skoog
 * @date		2018-10-28
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_CONV_TYPES_H_
	#define KARHU_CONV_TYPES_H_

	#include <karhu/data/JSON.hpp>

	#include <string>
	#include <cstdint>

	namespace karhu
	{
  		namespace conv
  		{
  			using Char   = char;
			using String = std::basic_string<Char>;
  			using Int    = std::int64_t;
  			using Float  = double;
			
  			inline constexpr karhu::JSON::Escape escaping() noexcept
  			{
  				return karhu::JSON::Escape::withoutUnicode;
			}

			/**
			 * The standard JSON setting used across engine and tools.
			 */
			using JSON = karhu::JSON::Context<Char, Int, Float, escaping()>;
		}
	}
#endif
