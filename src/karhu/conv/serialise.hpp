/**
 * @author		Ava Skoog
 * @date		2018-10-29
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_CONV_SERIALISE_H_
	#define KARHU_CONV_SERIALISE_H_

	#include <karhu/data/preserialise.hpp>
	#include <karhu/conv/types.hpp>

	namespace karhu
	{
  		namespace conv
  		{
			using Serialiser = serialise::Serialiser<conv::JSON>;
		}
	}
#endif
