/**
 * @author		Ava Skoog
 * @date		2018-10-39
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/conv/maths.hpp>
#include <karhu/data/serialise.hpp>

namespace karhu
{
	namespace mth
	{
		namespace helper
		{
			std::random_device rd;
			std::mt19937 mt{rd()};
		}
		
		void Transformf::serialise(conv::Serialiser &ser) const
		{
			ser
				<< karhuIN(position)
				<< karhuIN(scale)
				<< karhuIN(origin)
				<< karhuIN(rotation);
		}

		void Transformf::deserialise(const conv::Serialiser &ser)
		{
			ser
				>> karhuOUT(position)
				>> karhuOUT(scale)
				>> karhuOUT(origin)
				>> karhuOUT(rotation);
		}
		
		void decompose(const Mat4f &m, Vec3f *const p, Quatf *const r, Vec3f *const s)
		{
			Vec3f position, scale, skew;
			Vec4f perspective;
			Quatf rotation;
			
			glm::decompose(m, scale, rotation, position, skew, perspective);
			
			if (p) *p = std::move(position);
			if (r) *r = std::move(rotation);
			if (s) *s = std::move(scale);
		}
		
		void decompose(const Mat4f &m, Transformf &tf)
		{
			tf.origin = {};
			decompose(m, &tf.position, &tf.rotation, &tf.scale);
		}
	
		Vec3f position(const Mat4f &m)
		{
			Vec3f p;
			decompose(m, &p, nullptr, nullptr);
			return p;
		}
	
		Quatf rotation(const Mat4f &m)
		{
			Quatf r;
			decompose(m, nullptr, &r, nullptr);
			return r;
		}
	
		Vec3f scale(const Mat4f &m)
		{
			Vec3f s;
			decompose(m, nullptr, nullptr, &s);
			return s;
		}
	}
}
