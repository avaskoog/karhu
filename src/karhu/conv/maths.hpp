/**
 * @author		Ava Skoog
 * @date		2018-10-18
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_CONV_MATHS_H_
	#define KARHU_CONV_MATHS_H_

	#include <karhu/conv/serialise.hpp>

	#define GLM_SWIZZLE

	#include <karhu/lib/glm/glm.hpp>
	#include <karhu/lib/glm/gtc/matrix_transform.hpp>
	#include <karhu/lib/glm/gtx/compatibility.hpp>
	#include <karhu/lib/glm/gtx/quaternion.hpp>
	#include <karhu/lib/glm/gtc/type_ptr.hpp> /// @todo: Should this be here?
	#include <karhu/lib/glm/detail/type_vec2.hpp>
	#include <karhu/lib/glm/detail/type_vec3.hpp>
	#include <karhu/lib/glm/detail/type_vec4.hpp>
	#include <karhu/lib/glm/gtx/matrix_decompose.hpp>

	#include <random>
	#include <cstdint>
	#include <cmath>

	namespace karhu
	{
		namespace mth
		{
			namespace helper
			{
				extern std::random_device rd;
				extern std::mt19937 mt;
			}
		
			// Scalars.
			
			using Scalf = float;
			using Scali = std::int32_t;
			using Scalu = std::uint32_t;
			
			// Angles.
			
			using Radsf = Scalf;
			using Degsf = Scalf;
			using Degsi = Scali;
			using Degsu = Scalu;
			
			// Vectors.
			
			template<typename T>
			using Vec2 = glm::tvec2<T, glm::highp>;
		
			using Vec2f  = Vec2<Scalf>;
			using Vec2i  = Vec2<Scali>;
			using Vec2u  = Vec2<Scalu>;

			template<typename T>
			using Vec3 = glm::tvec3<T, glm::highp>;
		
			using Vec3f  = Vec3<Scalf>;
			using Vec3i  = Vec3<Scali>;
			using Vec3u  = Vec3<Scalu>;

			template<typename T>
			using Vec4 = glm::tvec4<T, glm::highp>;

			using Vec4f  = Vec4<Scalf>;
			using Vec4i  = Vec4<Scali>;
			using Vec4u  = Vec4<Scalu>;
			
			template<typename T>
			inline bool operator<(Vec2<T> const &a, Vec2<T> const &b) { return (a.x < b.x && a.y < b.y); }
			
			template<typename T>
			inline bool operator>(Vec2<T> const &a, Vec2<T> const &b) { return (a.x > b.x && a.y > b.y); }
			
			template<typename T>
			inline bool operator<=(Vec2<T> const &a, Vec2<T> const &b) { return (a == b || a < b); }
			
			template<typename T>
			inline bool operator>=(Vec2<T> const &a, Vec2<T> const &b) { return (a == b || a > b); }
			
			template<typename T>
			inline bool operator<(Vec3<T> const &a, Vec3<T> const &b) { return (a.x < b.x && a.y < b.y && a.z < b.z); }
			
			template<typename T>
			inline bool operator>(Vec3<T> const &a, Vec3<T> const &b) { return (a.x > b.x && a.y > b.y && a.z > b.z); }
			
			template<typename T>
			inline bool operator<=(Vec3<T> const &a, Vec3<T> const &b) { return (a == b || a < b); }
			
			template<typename T>
			inline bool operator>=(Vec3<T> const &a, Vec3<T> const &b) { return (a == b || a > b); }
			
			template<typename T>
			inline bool operator<(Vec4<T> const &a, Vec4<T> const &b) { return (a.x < b.x && a.y < b.y && a.z < b.z && a.w < b.w); }
			
			template<typename T>
			inline bool operator>(Vec4<T> const &a, Vec4<T> const &b) { return (a.x > b.x && a.y > b.y && a.z > b.z && a.w > b.w); }
			
			template<typename T>
			inline bool operator<=(Vec4<T> const &a, Vec4<T> const &b) { return (a == b || a < b); }
			
			template<typename T>
			inline bool operator>=(Vec4<T> const &a, Vec4<T> const &b) { return (a == b || a > b); }
			
			// Quaternions.

			template<typename T>
			using Quat = glm::tquat<T, glm::highp>;
			
			using Quatf = Quat<Scalf>;
			
			// Matrices.
			
			template<typename T>
			using Mat2 = glm::tmat2x2<T, glm::highp>;
			
			using Mat2f = Mat2<Scalf>;
			
			template<typename T>
			using Mat3 = glm::tmat3x3<T, glm::highp>;
			
			using Mat3f = Mat3<Scalf>;
			
			template<typename T>
			using Mat4 = glm::tmat4x4<T, glm::highp>;
			
			using Mat4f = Mat4<Scalf>;
			
			/// @todo: Add more matrices.
			
			template<typename T>
			T random(const T &a, const T &b)
			{
				return static_cast<T>
				(
					std::uniform_real_distribution<double>
					{
						static_cast<double>(a),
						static_cast<double>(b)
					}(helper::mt)
				);
			}
			
			template<typename T>
			inline T pi()
			{
				return glm::pi<T>();
			}
			
			inline Scalf pif() { return pi<Scalf>(); }
			
			template<typename T>
			inline T tau()
			{
				return glm::pi<T>() * T(2);
			}
			
			inline Scalf tauf() { return tau<Scalf>(); }
			
			template<typename T>
			inline T mod(const T &a, const T &b)
			{
				T c(a % b);
				
				while (c < T(0))
					c += b;
				
				return c;
			}
			
			template<>
			inline float mod<float>(const float &a, const float &b)
			{
				// Android compiler can't find this in std namespace.
				float c{fmodf(a, b)};

				while (c < 0.0f)
					c += b;
				
				return c;
			}
			
			template<>
			inline double mod<double>(const double &a, const double &b)
			{
				double c{std::fmod(a, b)};

				while (c < 0.0)
					c += b;
				
				return c;
			}
			
			template<typename T>
			inline T sqrt(const T &v)
			{
				return std::sqrt(v);
			}
			
			template<typename T>
			inline T cbrt(const T &v)
			{
				return std::cbrt(v);
			}
			
			template<typename T>
			inline T round(const T &v)
			{
				return std::round(v);
			}
			
			template<typename T>
			inline T floor(const T &v)
			{
				return std::floor(v);
			}
			
			template<typename T>
			inline T ceil(const T &v)
			{
				return std::ceil(v);
			}
			
			template<typename T>
			inline T pow(const T &a, const T &b)
			{
				return static_cast<T>(std::pow(a, b));
			}
			
			template<typename T>
			inline T cos(const T &v)
			{
				return glm::cos(v);
			}
			
			template<typename T>
			inline T sin(const T &v)
			{
				return glm::sin(v);
			}
			
			template<typename T>
			inline T tan(const T &v)
			{
				return glm::tan(v);
			}
			
			template<typename T>
			inline T abs(const T &v)
			{
				return static_cast<T>(std::abs(v));
			}
			
			template<typename T>
			inline T atan2(const T &a, const T &b)
			{
				return std::atan2(a, b);
			}
			
			template<typename T>
			inline T min(const T &a, const T &b)
			{
				return std::min(a, b);
			}
			
			template<typename T>
			inline T max(const T &a, const T &b)
			{
				return std::max(a, b);
			}
			
			template<typename T>
			inline T clamp(const T &v, const T &min, const T &max)
			{
				return glm::clamp(v, min, max);
			}
			
			template<typename T>
			inline T sign(const T &v)
			{
				// Account for negative 0.
				if (T(0) == mth::abs(v))
					return v;
				
				return (v < T(0)) ? T(-1) : T(1);
			}
			
			template<typename T>
			inline T radians(const T &v)
			{
				return static_cast<T>(glm::radians(v));
			}
			
			template<typename T>
			inline T sradians(const T &v)
			{
				return mth::mod(v, tau<T>());
			}
			
			template<typename T>
			inline T degrees(const T &v)
			{
				return glm::degrees(v);
			}
			
			template<typename T>
			inline T sdegrees(const T &v)
			{
				return mth::mod(v, T(360));
			}
			
			template<typename T>
			inline Vec3<T> forward() noexcept
			{
				return {T(0), T(0), T(1)};
			}
			
			template<typename T>
			inline Vec3<T> backward() noexcept
			{
				return {T(0), T(0), T(-1)};
			}
			
			template<typename T>
			inline Vec3<T> up() noexcept
			{
				return {T(0), T(1), T(0)};
			}
			
			template<typename T>
			inline Vec3<T> right() noexcept
			{
				return {T(1), T(0), T(0)};
			}
			
			inline Vec3f forwardf() noexcept
			{
				return forward<Scalf>();
			}
			
			inline Vec3f upf() noexcept
			{
				return up<Scalf>();
			}
			
			inline Vec3f rightf() noexcept
			{
				return right<Scalf>();
			}
			
			template<typename T>
			inline T length(const Vec2<T> &v)
			{
				return glm::length(v);
			}
			
			template<typename T>
			inline T length(const Vec3<T> &v)
			{
				return glm::length(v);
			}
			
			template<typename T>
			inline T length(const Vec4<T> &v)
			{
				return glm::length(v);
			}
			
			inline Radsf clength(const Radsf &a, const Radsf &b)
			{
				const Radsf
					max{pif() * 2.0f},
					da {std::fmod((b - a), max)};
				
				return std::fmod(da * 2.0f, max) - da;
			}
			
			template<typename T>
			inline T distance(const Vec2<T> &a, const Vec2<T> &b)
			{
				return glm::distance(a, b);
			}
			
			template<typename T>
			inline T distance(const Vec3<T> &a, const Vec3<T> &b)
			{
				return glm::distance(a, b);
			}
			
			template<typename T>
			inline T distance(const Vec4<T> &a, const Vec4<T> &b)
			{
				return glm::distance(a, b);
			}
			
			template<typename T>
			inline T normalise(const T &v)
			{
				return glm::normalize(v);
			}
			
			template<typename T>
			inline T inverse(const T &v)
			{
				return glm::inverse(v);
			}
			
			template<typename T>
			inline T transpose(const T &v)
			{
				return glm::transpose(v);
			}
			
			template<typename TMat, typename TVec>
			inline TMat translate(TMat const &matrix, TVec const &vector)
			{
				return glm::translate(matrix, vector);
			}
			
			template<typename T>
			inline Scalf dot(const T &a, const T &b)
			{
				return glm::dot(a, b);
			}
			
			template<typename T>
			inline T cross(const T &a, const T &b)
			{
				return glm::cross(a, b);
			}
			
			template<typename T>
			inline T conjugate(const T &v)
			{
				return glm::conjugate(v);
			}
			
			template<typename T, typename TScalar>
			inline T lerp(const T &a, const T &b, const TScalar &v)
			{
				return glm::lerp(a, b, v);
			}
			
			inline Radsf clerp(const Radsf &a, const Radsf &b, const Scalf &t)
			{
				return a + clength(a, b) * t;
			}
			
			template<typename T, typename TScalar>
			inline T slerp(const T &a, const T &b, const TScalar &v)
			{
				return glm::slerp(a, b, v);
			}
			
			template<typename TScalar>
			inline Mat4f matrix4x4(const TScalar *const v)
			{
				return glm::make_mat4(v);
			}
			
			template<typename T>
			inline Quatf quaternion(const T &v)
			{
				return glm::toQuat(v);
			}
			
			template<typename T>
			inline Quatf euler(const Vec3<T> &v)
			{
				return Quatf{v};
			}
			
			template<typename T>
			inline Quatf euler(const T x, const T y, const T z)
			{
				return Quatf{Vec3<T>{x, y, z}};
			}
			
			inline mth::Vec3f euler(const Quatf &v)
			{
				return glm::eulerAngles(v);
			}
			
			inline mth::Vec3f seuler(const Quatf &v)
			{
				auto e(glm::eulerAngles(v));
				e.x = sradians(e.x);
				e.y = sradians(e.y);
				e.z = sradians(e.z);
				return e;
			}
			
			inline Mat4f translationmatrix(const Vec3f &p)
			{
				return glm::translate(Mat4f{}, p);
			}
			
			inline Mat4f rotationmatrix(const Quatf &r)
			{
				return glm::mat4_cast(r);
			}
			
			inline Mat4f scalematrix(const Vec3f &s)
			{
				return glm::scale(Mat4f{}, s);
			}
			
			// Transforms.

			struct Transformf
			{
				/**
				 * Calculates the model matrix of the transform.
				 *
				 * @return The calculated model matrix.
				 */
				Mat4f matrix() const
				{
					const Mat4f
						T{translationmatrix( position)},
						R{rotationmatrix   ( rotation)},
						S{scalematrix      ( scale)},
						O{translationmatrix(-origin)};
					
					return T * R * S * O;
				}

				Vec3f
					position{0.0f, 0.0f, 0.0f},
					scale   {1.0f, 1.0f, 1.0f},
					origin  {0.0f, 0.0f, 0.0f};
				
				Quatf rotation{};
				
				Vec3f forward() const { return normalise(rotation * forwardf()); }
				Vec3f right()   const { return normalise(rotation * rightf()); }
				Vec3f up()      const { return normalise(rotation * upf()); }
				
				// Of course weirdness like this shouldn't be needed but the engine was
				// messed up early on and these workarounds are staying till the current
				// game is done, and then it's getting rewritten from the ground up! 8-)
				Quatf eyerotation() const { return rotation * mth::Quatf{0.0f, 0.0f, 1.0f, 0.0f}; }
				Vec3f eyeforward()  const { return normalise(eyerotation() * forwardf()); }
				Vec3f eyeright()    const { return normalise(eyerotation() * rightf()); }
				Vec3f eyeup()       const { return normalise(eyerotation() * upf()); }

				void serialise(conv::Serialiser &) const;
				void deserialise(const conv::Serialiser &);
			};
			
			void decompose(const Mat4f &m, Vec3f *const p, Quatf *const r, Vec3f *const s);
			void decompose(const Mat4f &m, Transformf &tf);
			Vec3f position(const Mat4f &m);
			Quatf rotation(const Mat4f &m);
			Vec3f scale(const Mat4f &m);
			
			template<typename T>
			struct Bounds3
			{
				Vec3<T> min, max;
				
				Vec3<T> size() const { return max - min; }
				Vec3<T> centre() const { return min + size() * T(0.5f); }
				
				bool contains(Bounds3<T> const &o) const
				{
					return (o.min >= min && o.max <= max);
				}
				
				void serialise(conv::Serialiser &) const;
				void deserialise(const conv::Serialiser &);
			};
			
			using Bounds3f = Bounds3<Scalf>;
			using Bounds3i = Bounds3<Scali>;
			
			template<typename T>
			struct Box
			{
				Vec3<T> centre, size;
				
				Bounds3<T> bounds() const
				{
					Bounds3<T> r;
					
					r.min = centre - size / T(2);
					r.max = centre + size / T(2);
					
					return r;
				}
				
				void serialise(conv::Serialiser &) const;
				void deserialise(const conv::Serialiser &);
			};
			
			using Boxf = Box<Scalf>;
			using Boxi = Box<Scali>;
			
			namespace projection
			{
				inline Mat4f perspective
				(
					const Scalf &FOV,
					const Scalf &aspect,
					const Scalf &near,
					const Scalf &far
				)
				{
					return glm::perspective(FOV, aspect, near, far);
				}
				
				inline Mat4f orthographic
				(
					const Scalf &left,
					const Scalf &right,
					const Scalf &bottom,
					const Scalf &top,
					const Scalf &near,
					const Scalf &far
				)
				{
					return glm::ortho(left, right, bottom, top, near, far);
				}
				
				inline Mat4f look
				(
					const Vec3f &from,
					const Vec3f &to,
					const Vec3f &up
				)
				{
					return glm::lookAt(from, to, up);
				}
			}
		}
	}

	// Serialise and deserialise Vec2 and ColRG.

	template<typename T>
	karhu::conv::Serialiser &operator<<
	(
		karhu::conv::Serialiser &ser,
		const karhu::serialise::Prop<const karhu::mth::Vec2<T>> &r
	)
	{
		const struct V
		{
			T x, y;
			void serialise(karhu::conv::Serialiser &ser) const
			{
				ser << karhuIN(x) << karhuIN(y);
			}
		} v{r.value.x, r.value.y};
		return ser << karhuINn(r.name, v);
	}

	template<typename T>
	const karhu::conv::Serialiser &operator>>
	(
		const karhu::conv::Serialiser &ser,
	 	const karhu::serialise::Prop<karhu::mth::Vec2<T>> &r
	)
	{
		struct V
		{
			T x, y;
			void deserialise(const karhu::conv::Serialiser &ser)
			{
				ser >> karhuOUT(x) >> karhuOUT(y);
			}
		} v{r.value.x, r.value.y};
		ser >> karhuOUTn(r.name, v);
		r.value = {v.x, v.y};
		return ser;
	}

	// Serialise and deserialise Vec3 and ColRGB.

	template<typename T>
	karhu::conv::Serialiser &operator<<
	(
		karhu::conv::Serialiser &ser,
		const karhu::serialise::Prop<const karhu::mth::Vec3<T>> &r
	)
	{
		const struct V
		{
			T x, y, z;
			void serialise(karhu::conv::Serialiser &ser) const
			{
				ser << karhuIN(x) << karhuIN(y) << karhuIN(z);
			}
		} v{r.value.x, r.value.y, r.value.z};
		return ser << karhuINn(r.name, v);
	}

	template<typename T>
	const karhu::conv::Serialiser &operator>>
	(
		const karhu::conv::Serialiser &ser,
	 	const karhu::serialise::Prop<karhu::mth::Vec3<T>> &r
	)
	{
		struct V
		{
			T x, y, z;
			void deserialise(const karhu::conv::Serialiser &ser)
			{
				ser >> karhuOUT(x) >> karhuOUT(y) >> karhuOUT(z);
			}
		} v{r.value.x, r.value.y, r.value.z};
		ser >> karhuOUTn(r.name, v);
		r.value = {v.x, v.y, v.z};
		return ser;
	}

	// Serialise and deserialise Vec4 and ColRGBA.

	template<typename T>
	karhu::conv::Serialiser &operator<<
	(
		karhu::conv::Serialiser &ser,
		const karhu::serialise::Prop<const karhu::mth::Vec4<T>> &r
	)
	{
		const struct V
		{
			T x, y, z, w;
			void serialise(karhu::conv::Serialiser &ser) const
			{
				ser << karhuIN(x) << karhuIN(y) << karhuIN(z) << karhuIN(w);
			}
		} v{r.value.x, r.value.y, r.value.z, r.value.w};
		return ser << karhuINn(r.name, v);
	}

	template<typename T>
	const karhu::conv::Serialiser &operator>>
	(
		const karhu::conv::Serialiser &ser,
	 	const karhu::serialise::Prop<karhu::mth::Vec4<T>> &r
	)
	{
		struct V
		{
			T x, y, z, w;
			void deserialise(const karhu::conv::Serialiser &ser)
			{
				ser >> karhuOUT(x) >> karhuOUT(y) >> karhuOUT(z) >> karhuOUT(w);
			}
		} v{r.value.x, r.value.y, r.value.z, r.value.w};
		ser >> karhuOUTn(r.name, v);
		r.value = {v.x, v.y, v.z, v.w};
		return ser;
	}

	// Serialise and deserialise Quat.

	template<typename T>
	karhu::conv::Serialiser &operator<<
	(
		karhu::conv::Serialiser &ser,
		const karhu::serialise::Prop<const karhu::mth::Quat<T>> &r
	)
	{
		const struct V
		{
			T x, y, z, w;
			void serialise(karhu::conv::Serialiser &ser) const
			{
				ser << karhuIN(x) << karhuIN(y) << karhuIN(z) << karhuIN(w);
			}
		} v{r.value.x, r.value.y, r.value.z, r.value.w};
		return ser << karhuINn(r.name, v);
	}

	template<typename T>
	const karhu::conv::Serialiser &operator>>
	(
		const karhu::conv::Serialiser &ser,
	 	const karhu::serialise::Prop<karhu::mth::Quat<T>> &r
	)
	{
		struct V
		{
			T x, y, z, w;
			void deserialise(const karhu::conv::Serialiser &ser)
			{
				ser >> karhuOUT(x) >> karhuOUT(y) >> karhuOUT(z) >> karhuOUT(w);
			}
		} v{r.value.x, r.value.y, r.value.z, r.value.w};
		ser >> karhuOUTn(r.name, v);
		
		// GLM quaternion components have to be set one.
		r.value.x = v.x;
		r.value.y = v.y;
		r.value.z = v.z;
		r.value.w = v.w;

		return ser;
	}

	// Serialise and deserialise Mat2.

	template<typename T>
	karhu::conv::Serialiser &operator<<
	(
		karhu::conv::Serialiser &ser,
		const karhu::serialise::Prop<const karhu::mth::Mat2<T>> &r
	)
	{
		const typename karhu::mth::Mat2<T>::col_type v[]
		{
			r.value[0],
			r.value[1]
		};
		return ser << karhuINn(r.name, v);
	}

	template<typename T>
	const karhu::conv::Serialiser &operator>>
	(
		const karhu::conv::Serialiser &ser,
	 	const karhu::serialise::Prop<karhu::mth::Mat2<T>> &r
	)
	{
		typename karhu::mth::Mat2<T>::col_type v[2];
		ser >> karhuOUTn(r.name, v);
		r.value = {v[0], v[1]};
		return ser;
	}

	// Serialise and deserialise Mat3.

	template<typename T>
	karhu::conv::Serialiser &operator<<
	(
		karhu::conv::Serialiser &ser,
		const karhu::serialise::Prop<const karhu::mth::Mat3<T>> &r
	)
	{
		const typename karhu::mth::Mat3<T>::col_type v[]
		{
			r.value[0],
			r.value[1],
			r.value[2]
		};
		return ser << karhuINn(r.name, v);
	}

	template<typename T>
	const karhu::conv::Serialiser &operator>>
	(
		const karhu::conv::Serialiser &ser,
	 	const karhu::serialise::Prop<karhu::mth::Mat3<T>> &r
	)
	{
		typename karhu::mth::Mat3<T>::col_type v[3];
		ser >> karhuOUTn(r.name, v);
		r.value = {v[0], v[1], v[2]};
		return ser;
	}

	// Serialise and deserialise Mat4.

	template<typename T>
	karhu::conv::Serialiser &operator<<
	(
		karhu::conv::Serialiser &ser,
		const karhu::serialise::Prop<const karhu::mth::Mat4<T>> &r
	)
	{
		const typename karhu::mth::Mat4<T>::col_type v[]
		{
			r.value[0],
			r.value[1],
			r.value[2],
			r.value[3]
		};
		return ser << karhuINn(r.name, v);
	}

	template<typename T>
	const karhu::conv::Serialiser &operator>>
	(
		const karhu::conv::Serialiser &ser,
	 	const karhu::serialise::Prop<karhu::mth::Mat4<T>> &r
	)
	{
		typename karhu::mth::Mat4<T>::col_type v[4];
		ser >> karhuOUTn(r.name, v);
		r.value = {v[0], v[1], v[2], v[3]};
		return ser;
	}

	// These need to be defined after the vector de/serialisation functions to use them.
	namespace karhu
	{
		namespace mth
		{
			template<typename T>
			void Bounds3<T>::serialise(conv::Serialiser &ser) const
			{
				ser << karhuIN(min) << karhuIN(max);
			}
		
			template<typename T>
			void Bounds3<T>::deserialise(const conv::Serialiser &ser)
			{
				ser >> karhuOUT(min) >> karhuOUT(max);
			}
			
			template<typename T>
			void Box<T>::serialise(conv::Serialiser &ser) const
			{
				ser << karhuIN(centre) << karhuIN(size);
			}
			
			template<typename T>
			void Box<T>::deserialise(const conv::Serialiser &ser)
			{
				ser >> karhuOUT(centre) >> karhuOUT(size);
			}
		}
	}
#endif
