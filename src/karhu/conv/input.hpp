/**
 * @author		Ava Skoog
 * @date		2019-01-07
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_CONV_INPUT_H_
	#define KARHU_CONV_INPUT_H_

	#include <cstdint>
	#include <type_traits>

	namespace karhu
	{
		namespace inp
		{
			// Layers are used for inputtable components.
			using Layer = std::int32_t;
			struct Defaultlayers
			{
				enum : Layer
				{
					none   = -1,
					custom =  0,
					GUI    =  10000
				};
			};
	
			// Priorities are used for inputtable components.
			using Priority = std::int16_t;
			
			// Actions are specified as bitflags so
			// that multiple can be specified at once.
			using Action  = std::uint16_t;
			using Actions = Action;
			
			/**
			 * Returns a valid player bitflag for the action at the specified index.
			 *
			 * @param index The index of the action, starting from zero.
			 *
			 * @return A valid bitflag for a single action.
			 */
			inline constexpr Action action(const std::size_t &index) noexcept
			{
				return static_cast<Action>(1 << index);
			}
			
			// Players are specified as bitflags so
			// that multiple can be specified at once.
			using Player  = std::uint16_t;
			using Players = Player;
			
			/**
			 * Returns the set of bitflags that contains all players.
			 *
			 * @return A valid set of bitflags for all players.
			 */
			inline constexpr Players allPlayers() noexcept
			{
				return 0;
			}
			
			/**
			 * Returns a valid player bitflag for the player at the specified index.
			 *
			 * @param index The index of the player, starting from zero.
			 *
			 * @return A valid bitflag for a single player.
			 */
			inline constexpr Player player(const size_t &index) noexcept
			{
				return static_cast<Player>(1 << index);
			}
			
			/**
			 * The type of an input action.
			 */
			enum class Type : std::uint16_t
			{
				press,
				hold,
				release,
				analogue,
				direction
			};
			
			/**
			 * Specifies an input method for an action.
			 */
			enum class Method : std::uint16_t
			{
				mouse,
				keyboard,
				controller,
				touch,
				simulated   // Virtual controller (but virtual is a C++ keyword, so simulated it is).
			};
			
			/**
			 * Specifies action scope.
			 * Global means anywhere (a physical or virtual button press or the like).
			 * Local means on the entity with the inputtable component (e.g. tapping it).
			 */
			enum class Scope : std::uint8_t
			{
				global,
				local
			};
			
			/**
			 * Specifies input inversion mode.
			 */
			enum class Invert : std::uint8_t
			{
				none,
				horizontal,
				vertical,
				both
			};
			
			// Underlying type for key, mouse and controller codes.
			using Code = std::int16_t;
		}
	}
	
	// Bitwise operations for the type bitflags.

	inline constexpr karhu::inp::Type operator|
	(
		const karhu::inp::Type &a,
		const karhu::inp::Type &b
	)
	{
		using T = std::underlying_type_t<karhu::inp::Type>;
		return static_cast<karhu::inp::Type>(static_cast<T>(a) | static_cast<T>(b));
	}
	
	inline constexpr karhu::inp::Type operator&
	(
		const karhu::inp::Type &a,
		const karhu::inp::Type &b
	)
	{
		using T = std::underlying_type_t<karhu::inp::Type>;
		return static_cast<karhu::inp::Type>(static_cast<T>(a) & static_cast<T>(b));
	}
	
	inline constexpr karhu::inp::Type &operator|=
	(
		karhu::inp::Type &a,
		const karhu::inp::Type &b
	)
	{
		return a = a | b;
	}
	
	inline constexpr karhu::inp::Type &operator&=
	(
		karhu::inp::Type &a,
		const karhu::inp::Type &b
	)
	{
		return a = a & b;
	}
#endif
