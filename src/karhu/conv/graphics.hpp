/**
 * @author		Ava Skoog
 * @date		2018-10-18
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_CONV_GRAPHICS_H_
	#define KARHU_CONV_GRAPHICS_H_

	#include <karhu/res/Resource.hpp>
	#include <karhu/conv/maths.hpp>
	#include <karhu/conv/serialise.hpp>
	#include <karhu/core/macros.hpp>

	#include <unordered_map>
	#include <memory>
	#include <cstdint>

	namespace karhu
	{
		namespace res
		{
			class Texture;
		}
		
		namespace gfx
		{
			enum class Culling : std::uint8_t
			{
				none,
				back,
				front,
				both
			};
			
			enum class Buffertarget : std::uint8_t
			{
				none     =  0,
				colour   =  1 << 0,
				depth    =  1 << 1,
				stencil  =  1 << 2,
				all      = (1 << 3) - 1,
				defaults = colour | depth
			};
			
			using Buffertargets = Buffertarget;
			
			karhuBITFLAGGIFY(Buffertarget)
			
			enum class ConditionDepth : std::uint8_t
			{
				never,
				always
			};
			
			enum class ConditionStencil : std::uint8_t
			{
				never,
				always,
				equal,
				unequal,
				less,
				lessOrEqual,
				greater,
				greaterOrEqual
			};

			enum class OperationStencil : std::uint8_t
			{
				keep,
				replace,
				invert,
				zero,
				increment,
				incrementAndWrap,
				decrement,
				decrementAndWrap
			};
			
			// Alias everything for clear and safe code.
			/// @todo: Possible we should move some of this now that we are moving things over into conv/...
			using Index  = std::uint32_t;

			// These set contain references to up to four joints.
			// A weightset defines the weight of each joint.
			// A jointset defines the index of each joint.
			// These are used to specify which joints affect
			// a vertex of a skinned mesh as its bones deform.
			using Weightset = mth::Vec4f;
			using Jointset  = mth::Vec4f;

			using Queue = std::int64_t;
			
			struct Defaultqueues
			{
				enum : Queue
				{
					stencil     = -20000,
					opaque      =  0,
					transparent =  10000,
					UI          =  20000 /// @todo: Do we need UI queue anymore?
				};
			};
			
			enum class Rendermode : std::uint8_t
			{
				mesh,
				sprite
			};

			using Layer  = std::uint64_t;
			using Layers = Layer;
			
			struct Defaultlayers
			{
				enum : Layer
				{
					none     = 0,
					standard = 1 << 0,
					GUI      = 1 << 1,
					custom   = 1 << 2,
					all      = std::numeric_limits<Layer>::max()
				};
			};
			
			// Returns a custom layer bitflag, starting from the custom range after the defaults.
			inline constexpr Layer layer(const std::size_t &i) noexcept
			{
				return static_cast<Layer>(Defaultlayers::custom << i);
			}
			
			using Order = std::int16_t;
			
			// Colours.
			
			/// @todo: Add some nice methods to return default RGB(A) colours like black, white and red.

			template<typename T> using Channel = T;
			using Channelf  = Channel<float>;
			using Channelui = Channel<std::uint8_t>;

			template<typename T>
			inline constexpr T channelmax() noexcept;

			template<>
			inline constexpr Channelf channelmax<Channelf>() noexcept
			{
				return static_cast<Channelf>(1);
			}

			template<>
			constexpr inline Channelui channelmax<Channelui>() noexcept
			{
				return static_cast<Channelui>(255);
			}

			template<typename T>
			struct ColRG
			{
				T r{}, g{};
				
				constexpr ColRG(Channel<T> const r = T{}, Channel<T> const g = T{}) : r{r}, g{g} {}
				constexpr ColRG(ColRG<T> &&) = default;
				constexpr ColRG(ColRG<T> const &) = default;
				constexpr ColRG<T> &operator=(ColRG<T> &&) = default;
				constexpr ColRG<T> &operator=(ColRG<T> const &) = default;
				
				constexpr bool operator==(const ColRG<T> &o) const noexcept
				{
					return (r == o.r && g == o.g);
				}
				
				constexpr bool operator!=(const ColRG<T> &o) const noexcept
				{
					return !operator==(o);
				}
				
				void serialise(conv::Serialiser &ser) const
				{
					ser << karhuIN(r) << karhuIN(g);
				}
				
				void deserialise(const conv::Serialiser &ser)
				{
					ser >> karhuOUT(r) >> karhuOUT(g);
				}
			};

			using ColRGf  = ColRG<Channelf>;
			using ColRGui = ColRG<Channelui>;

			template<typename T>
			struct ColRGB
			{
				T r{}, g{}, b{};
				
				constexpr ColRGB(Channel<T> const r = T{}, Channel<T> const g = T{}, Channel<T> const b = T{}) : r{r}, g{g}, b{b} {}
				constexpr ColRGB(ColRGB<T> &&) = default;
				constexpr ColRGB(ColRGB<T> const &) = default;
				constexpr ColRGB<T> &operator=(ColRGB<T> &&) = default;
				constexpr ColRGB<T> &operator=(ColRGB<T> const &) = default;
				
				constexpr bool operator==(const ColRGB<T> &o) const noexcept
				{
					return (r == o.r && g == o.g && b == o.b);
				}
				
				constexpr bool operator!=(const ColRGB<T> &o) const noexcept
				{
					return !operator==(o);
				}
				
				void serialise(conv::Serialiser &ser) const
				{
					ser << karhuIN(r) << karhuIN(g) << karhuIN(b);
				}
				
				void deserialise(const conv::Serialiser &ser)
				{
					ser >> karhuOUT(r) >> karhuOUT(g) >> karhuOUT(b);
				}
			};

			using ColRGBf  = ColRGB<Channelf>;
			using ColRGBui = ColRGB<Channelui>;

			template<typename T>
			struct ColRGBA
			{
				T r{}, g{}, b{}, a{channelmax<T>()};
				
				constexpr ColRGBA(Channel<T> const r = T{}, Channel<T> const g = T{}, Channel<T> const b = T{}, Channel<T> const a = channelmax<T>()) : r{r}, g{g}, b{b}, a{a} {}
				constexpr ColRGBA(ColRGBA<T> &&) = default;
				constexpr ColRGBA(ColRGBA<T> const &) = default;
				constexpr ColRGBA<T> &operator=(ColRGBA<T> &&) = default;
				constexpr ColRGBA<T> &operator=(ColRGBA<T> const &) = default;
				
				constexpr bool operator==(const ColRGBA<T> &o) const noexcept
				{
					return (r == o.r && g == o.g && b == o.b && a == o.a);
				}
				
				constexpr bool operator!=(const ColRGBA<T> &o) const noexcept
				{
					return !operator==(o);
				}
				
				void serialise(conv::Serialiser &ser) const
				{
					ser << karhuIN(r) << karhuIN(g) << karhuIN(b) << karhuIN(a);
				}
				
				void deserialise(const conv::Serialiser &ser)
				{
					ser >> karhuOUT(r) >> karhuOUT(g) >> karhuOUT(b) >> karhuOUT(a);
				}
			};

			using ColRGBAf  = ColRGBA<Channelf>;
			using ColRGBAui = ColRGBA<Channelui>;
			
			/**
			 * A pixel buffer used for textures and such.
			 *
			 * Contains a buffer of bytes (each representing a colour channel
			 * with values from 0 to 255) determined by the width and height
			 * of the buffer as well as the number of channels (the format).
			 */
			struct Pixelbuffer
			{
				using Component  = gfx::Channelui;
				using Components = Component;

				enum class Format
				{
					BW   = 1, // Black and white.
					BWA  = 2, // Black and white with alpha.
					RGB  = 3, // Red, green and blue.
					RGBA = 4  // Red, green and blue with alpha.
				} format{Format::RGB};

				std::uint32_t width{0}, height{0};
				std::unique_ptr<Component[]> components;

				Pixelbuffer() = default;
				Pixelbuffer(const Format format) : format{format} {}
				Pixelbuffer(Pixelbuffer &&) = default;
				Pixelbuffer &operator=(Pixelbuffer &&) = default;
				~Pixelbuffer() = default;
				
				Pixelbuffer(const Pixelbuffer &);
				Pixelbuffer &operator=(const Pixelbuffer &o);
				
				std::size_t size() const noexcept;
				
				void reserve
				(
					const std::uint32_t &width,
					const std::uint32_t &height,
					const Format format
				);
			};
			
			/**
			 * Contains mesh geometry.
			 */
			struct Geometry
			{
				/**
				 * Lists the various attributes at bitflags in case they are needed.
				 */
				enum class Attribute : std::uint16_t
				{
					none       =  0,
					position   =  1 << 0,
					normal     =  1 << 1,
					UV         =  1 << 2,
					colour     =  1 << 3,
					jointset0  =  1 << 4,
					weightset0 =  1 << 5,
					jointset1  =  1 << 6,
					weightset1 =  1 << 7,
					jointset2  =  1 << 8,
					weightset2 =  1 << 9,
					tangent    =  1 << 10,
					custom     =  1 << 11,
					all        = (1 << 12) - 1
				};
				using Attributes = Attribute;

				// Vertex indices.
				std::vector<Index> indices;

				// Regular vertex attributes.
				std::vector<mth::Vec3f> positions;
				std::vector<mth::Vec3f> normals;
				std::vector<mth::Vec2f> UVs;
				std::vector<ColRGBAf>   colours;
				std::vector<mth::Vec4f> tangents;
				std::vector<mth::Vec4f> custom;

				// Skinning vertex attributes.
				std::vector<Jointset>  jointsets0, jointsets1, jointsets2;
				std::vector<Weightset> weightsets0, weightsets1, weightsets2;
				
				void clear();
				void append(const Geometry &);
			};
			
			/// @todo: Quaternions?
			enum class TypeInput
			{
				boolean,
				integer,
				floating,
				vector2i,
				vector3i,
				vector4i,
				vector2f,
				vector3f,
				vector4f,
				matrix2,
				matrix3,
				matrix4,
				integerv,
				floatingv,
				vector2iv,
				vector3iv,
				vector4iv,
				vector2fv,
				vector3fv,
				vector4fv,
				matrix2v,
				matrix3v,
				matrix4v,
				texture,
				invalid = 0xff
			};
			
			/*
			 * Shader input.
			 */
			struct Input
			{
				// The GLM types in the union force us to define this.
				Input() : boolean{} {}
				
				Input(const Input &o) { *this = o; }
				Input(Input &&o) { *this = std::move(o); }
				
				Input &operator=(const Input &o)
				{
					if (TypeInput::texture == type && TypeInput::texture != o.type)
						texture.~Texref();
					
					type = o.type;
					
					switch (type)
					{
						case TypeInput::boolean:   boolean   = o.boolean;   break;
						case TypeInput::integer:   integer   = o.integer;   break;
						case TypeInput::floating:  floating  = o.floating;  break;
						case TypeInput::vector2i:  vector2i  = o.vector2i;  break;
						case TypeInput::vector3i:  vector3i  = o.vector3i;  break;
						case TypeInput::vector4i:  vector4i  = o.vector4i;  break;
						case TypeInput::vector2f:  vector2f  = o.vector2f;  break;
						case TypeInput::vector3f:  vector3f  = o.vector3f;  break;
						case TypeInput::vector4f:  vector4f  = o.vector4f;  break;
						case TypeInput::matrix2:   matrix2   = o.matrix2;   break;
						case TypeInput::matrix3:   matrix3   = o.matrix3;   break;
						case TypeInput::matrix4:   matrix4   = o.matrix4;   break;
						case TypeInput::integerv:  integerv  = o.integerv;  break;
						case TypeInput::floatingv: floatingv = o.floatingv; break;
						case TypeInput::vector2iv: vector2iv = o.vector2iv; break;
						case TypeInput::vector3iv: vector3iv = o.vector3iv; break;
						case TypeInput::vector4iv: vector4iv = o.vector4iv; break;
						case TypeInput::vector2fv: vector2fv = o.vector2fv; break;
						case TypeInput::vector3fv: vector3fv = o.vector3fv; break;
						case TypeInput::vector4fv: vector4fv = o.vector4fv; break;
						case TypeInput::matrix2v:  matrix2v  = o.matrix2v;  break;
						case TypeInput::matrix3v:  matrix3v  = o.matrix3v;  break;
						case TypeInput::matrix4v:  matrix4v  = o.matrix4v;  break;
						
						case TypeInput::texture:
							new (&texture) Texref{o.texture};
							break;
							
						case TypeInput::invalid:
							break;
					}
					
					return *this;
				}
				
				Input &operator=(Input &&o)
				{
					if (TypeInput::texture == type && TypeInput::texture != o.type)
						texture.~Texref();
					
					type = std::move(o.type);
					o.type = TypeInput::invalid;
					
					switch (type)
					{
						case TypeInput::boolean:   boolean   = std::move(o.boolean);   break;
						case TypeInput::integer:   integer   = std::move(o.integer);   break;
						case TypeInput::floating:  floating  = std::move(o.floating);  break;
						case TypeInput::vector2i:  vector2i  = std::move(o.vector2i);  break;
						case TypeInput::vector3i:  vector3i  = std::move(o.vector3i);  break;
						case TypeInput::vector4i:  vector4i  = std::move(o.vector4i);  break;
						case TypeInput::vector2f:  vector2f  = std::move(o.vector2f);  break;
						case TypeInput::vector3f:  vector3f  = std::move(o.vector3f);  break;
						case TypeInput::vector4f:  vector4f  = std::move(o.vector4f);  break;
						case TypeInput::matrix2:   matrix2   = std::move(o.matrix2);   break;
						case TypeInput::matrix3:   matrix3   = std::move(o.matrix3);   break;
						case TypeInput::matrix4:   matrix4   = std::move(o.matrix4);   break;
						case TypeInput::integerv:  integerv  = std::move(o.integerv);  break;
						case TypeInput::floatingv: floatingv = std::move(o.floatingv); break;
						case TypeInput::vector2iv: vector2iv = std::move(o.vector2iv); break;
						case TypeInput::vector3iv: vector3iv = std::move(o.vector3iv); break;
						case TypeInput::vector4iv: vector4iv = std::move(o.vector4iv); break;
						case TypeInput::vector2fv: vector2fv = std::move(o.vector2fv); break;
						case TypeInput::vector3fv: vector3fv = std::move(o.vector3fv); break;
						case TypeInput::vector4fv: vector4fv = std::move(o.vector4fv); break;
						case TypeInput::matrix2v:  matrix2v  = std::move(o.matrix2v);  break;
						case TypeInput::matrix3v:  matrix3v  = std::move(o.matrix3v);  break;
						case TypeInput::matrix4v:  matrix4v  = std::move(o.matrix4v);  break;
						
						case TypeInput::texture:
							new (&texture) Texref{std::move(o.texture)};
							break;
							
						case TypeInput::invalid:
							break;
					}
					
					return *this;
				}
				
				~Input()
				{
					/// @todo: Do we need to call the 'non-trivial' destructors in here?
					if (TypeInput::texture == type)
						texture.~Texref();
				}
				
				template<typename T>
				struct Array
				{
					const T *data;
					std::size_t count;
				};
				
				struct Texref
				{
					enum class Type : std::uint8_t
					{
						ref,
						ptr
					} type{Type::ref};
					
					res::IDResource ref{0};
					res::Texture *ptr{nullptr};
					
					Texref();
					Texref(Texref &&);
					Texref(const Texref &);
					~Texref();
				
					res::Texture *get() const;
				};
				
				TypeInput type{TypeInput::boolean};

				union
				{
					bool              boolean;
					mth::Scali        integer;
					mth::Scalf        floating;
					mth::Vec2i        vector2i;
					mth::Vec3i        vector3i;
					mth::Vec4i        vector4i;
					mth::Vec2f        vector2f;
					mth::Vec3f        vector3f;
					mth::Vec4f        vector4f;
					mth::Mat2f        matrix2;
					mth::Mat3f        matrix3;
					mth::Mat4f        matrix4;
					Array<mth::Scali> integerv;
					Array<mth::Scalf> floatingv;
					Array<mth::Vec2i> vector2iv;
					Array<mth::Vec3i> vector3iv;
					Array<mth::Vec4i> vector4iv;
					Array<mth::Vec2f> vector2fv;
					Array<mth::Vec3f> vector3fv;
					Array<mth::Vec4f> vector4fv;
					Array<mth::Mat2f> matrix2v;
					Array<mth::Mat3f> matrix3v;
					Array<mth::Mat4f> matrix4v;
					Texref            texture;
				};
				
				void serialise(conv::Serialiser &) const;
				void deserialise(const conv::Serialiser &);
			};
			
			/// @todo: Change all these key std::string to const char *?
			class ContainerInputs
			{
				public:
					void append(const ContainerInputs &o)
					{
						for (const auto &input : o.m_inputs)
							m_inputs[input.first] = input.second;
					}
				
					void input(const std::string &name, const bool value)
					{
						Input v;
						v.type    = TypeInput::boolean;
						v.boolean = value;
						storeInput(name, std::move(v));
					}
				
					template<typename T>
					std::enable_if_t<(!std::is_same<T, bool>{} && std::is_integral<T>{}), void>
					input(const std::string &name, const T value)
					{
						Input v;
						v.type    = TypeInput::integer;
						v.integer = static_cast<mth::Scali>(value);
						storeInput(name, std::move(v));
					}

					template<typename T>
					std::enable_if_t<std::is_floating_point<T>{}, void>
					input(const std::string &name, const T value)
					{
						Input v;
						v.type     = TypeInput::floating;
						v.floating = static_cast<mth::Scalf>(value);
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Vec2i &value)
					{
						Input v;
						v.type     = TypeInput::vector2i;
						v.vector2i = value;
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Vec3i &value)
					{
						Input v;
						v.type     = TypeInput::vector3i;
						v.vector3i = value;
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Vec4i &value)
					{
						Input v;
						v.type     = TypeInput::vector4i;
						v.vector4i = value;
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Vec2f &value)
					{
						Input v;
						v.type     = TypeInput::vector2f;
						v.vector2f = value;
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Vec3f &value)
					{
						Input v;
						v.type     = TypeInput::vector3f;
						v.vector3f = value;
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Vec4f &value)
					{
						Input v;
						v.type     = TypeInput::vector4f;
						v.vector4f = value;
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Mat2f &value)
					{
						Input v;
						v.type    = TypeInput::matrix2;
						v.matrix2 = value;
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Mat3f &value)
					{
						Input v;
						v.type    = TypeInput::matrix3;
						v.matrix3 = value;
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Mat4f &value)
					{
						Input v;
						v.type    = TypeInput::matrix4;
						v.matrix4 = value;
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Scali *const values, const std::size_t &count)
					{
						if (!values || 0 == count) return;
						Input v;
						v.type     = TypeInput::integerv;
						v.integerv = {values, count};
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Scalf *const values, const std::size_t &count)
					{
						if (!values || 0 == count) return;
						Input v;
						v.type      = TypeInput::floatingv;
						v.floatingv = {values, count};
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Vec2i *const values, const std::size_t &count)
					{
						if (!values || 0 == count) return;
						Input v;
						v.type      = TypeInput::vector2iv;
						v.vector2iv = {values, count};
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Vec3i *const values, const std::size_t &count)
					{
						if (!values || 0 == count) return;
						Input v;
						v.type      = TypeInput::vector3iv;
						v.vector3iv = {values, count};
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Vec4i *const values, const std::size_t &count)
					{
						if (!values || 0 == count) return;
						Input v;
						v.type      = TypeInput::vector4iv;
						v.vector4iv = {values, count};
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Vec2f *const values, const std::size_t &count)
					{
						if (!values || 0 == count) return;
						Input v;
						v.type      = TypeInput::vector2fv;
						v.vector2fv = {values, count};
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Vec3f *const values, const std::size_t &count)
					{
						if (!values || 0 == count) return;
						Input v;
						v.type      = TypeInput::vector3fv;
						v.vector3fv = {values, count};
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Vec4f *const values, const std::size_t &count)
					{
						if (!values || 0 == count) return;
						Input v;
						v.type      = TypeInput::vector4fv;
						v.vector4fv = {values, count};
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Mat2f *const values, const std::size_t &count)
					{
						if (!values || 0 == count) return;
						Input v;
						v.type      = TypeInput::matrix2v;
						v.matrix2v = {values, count};
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Mat3f *const values, const std::size_t &count)
					{
						if (!values || 0 == count) return;
						Input v;
						v.type      = TypeInput::matrix3v;
						v.matrix3v = {values, count};
						storeInput(name, std::move(v));
					}
				
					void input(const std::string &name, const mth::Mat4f *const values, const std::size_t &count)
					{
						if (!values || 0 == count) return;
						Input v;
						v.type      = TypeInput::matrix4v;
						v.matrix4v = {values, count};
						storeInput(name, std::move(v));
					}
				
					void inputTexture(const std::string &name, const res::IDResource ID);
					void inputTexture(const std::string &name, res::Texture &texture);
				
					void clearInput(const std::string &name)
					{
						auto it(m_inputs.find(name));
						
						if (it != m_inputs.end())
							m_inputs.erase(it);
					}
				
					void clearInputs() { m_inputs.clear(); }

					/**
					 * Returns the full list of shader inputs for reading.
					 *
					 * @return The read-only list of inputs.
					 */
					const std::unordered_map<std::string, Input> &values() const noexcept { return m_inputs; }
				
					std::unordered_map<std::string, Input> &values() noexcept { return m_inputs; }
				
					/// @todo: Be able to remove inputs?
				
					void serialise(conv::Serialiser &) const;
					void deserialise(const conv::Serialiser &);
				
				private:
					void storeInput(const std::string &name, Input &&v)
					{
						auto it(m_inputs.find(name));
						if (it == m_inputs.end())
							m_inputs.emplace(name, std::move(v));
						else
							it->second = std::move(v);
					}

				protected:
					std::unordered_map<std::string, Input> m_inputs;
			};
			
			struct Instance
			{
			    std::int32_t           ID;
				mth::Mat4f             transform;
				ContainerInputs const *inputs;
			};
		}
	}
#endif
