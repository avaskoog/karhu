/**
 * @author		Ava Skoog
 * @date		2019-03-16
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/conv/graphics.hpp>
#include <karhu/data/serialise.hpp>
#include <karhu/app/App.hpp>
#include <karhu/res/Bank.hpp>
#include <karhu/res/Texture.hpp>

namespace karhu
{
	/// @todo: Hacky hotfix for now; App shouldn't even be known under tool.
	#ifdef KARHU_TOOL
	namespace app
	{
		App *App::instance() noexcept { return nullptr; }
	}
	#endif
	
	namespace gfx
	{
		Pixelbuffer::Pixelbuffer(const Pixelbuffer &o)
		:
		format{o.format},
		width {o.width},
		height{o.height},
		components
		{
			(o.components)
			?
				std::make_unique<Component[]>(o.size())
			:
				nullptr
		}
		{
			if (components)
				std::memcpy(components.get(), o.components.get(), o.size());
		}
		
		Pixelbuffer &Pixelbuffer::operator=(const Pixelbuffer &o)
		{
			reserve(o.width, o.height, o.format);
			std::memcpy(components.get(), o.components.get(), o.size());
			
			return *this;
		}
		
		/**
		 * Returns the byte size of the data in the buffer.
		 *
		 * @return The number of bytes in the buffer.
		 */
		std::size_t Pixelbuffer::size() const noexcept
		{
			return
			(
				static_cast<std::size_t>(width) *
				static_cast<std::size_t>(height) *
				static_cast<std::size_t>(format) *
				sizeof(Pixelbuffer::Components)
			);
		}

		/**
		 * Reserves the correct amount of space in the buffer
		 * for the dimensions and the format specified and
		 * updates the width, height and format properties.
		 *
		 * @param width  The new width in pixels of the buffer.
		 * @param height The new height in pixels of the buffer.
		 * @param format The image format (channel count).
		 */
		void Pixelbuffer::reserve
		(
			const std::uint32_t &width,
			const std::uint32_t &height,
			const Format format
		)
		{
			if (0 == width || 0 == height) return;

			this->format = format;
			this->width  = width;
			this->height = height;

			components = std::make_unique<Component[]>(size());
		}
		
		void Geometry::clear()
		{
			indices.clear();
			positions.clear();
			normals.clear();
			UVs.clear();
			colours.clear();
			jointsets0.clear();
			weightsets0.clear();
			jointsets1.clear();
			weightsets1.clear();
			jointsets2.clear();
			weightsets2.clear();
			tangents.clear();
			custom.clear();
		}

		void Geometry::append(const Geometry &o)
		{
			// Store the offset before appending new vertices.
			const std::size_t offset{positions.size()};

			// All of the vertex attributes can just be appended.
			positions.insert(positions.end(), o.positions.begin(), o.positions.end());
			normals.insert(normals.end(), o.normals.begin(), o.normals.end());
			UVs.insert(UVs.end(), o.UVs.begin(), o.UVs.end());
			colours.insert(colours.end(), o.colours.begin(), o.colours.end());
			
			jointsets0.insert(jointsets0.end(), o.jointsets0.begin(), o.jointsets0.end());
			weightsets0.insert(weightsets0.end(), o.weightsets0.begin(), o.weightsets0.end());
			
			jointsets1.insert(jointsets1.end(), o.jointsets1.begin(), o.jointsets1.end());
			weightsets1.insert(weightsets1.end(), o.weightsets1.begin(), o.weightsets1.end());
			
			jointsets2.insert(jointsets2.end(), o.jointsets2.begin(), o.jointsets2.end());
			weightsets2.insert(weightsets2.end(), o.weightsets2.begin(), o.weightsets2.end());
			
			tangents.insert(tangents.end(), o.tangents.begin(), o.tangents.end());
			custom.insert(custom.end(), o.custom.begin(), o.custom.end());
			
			indices.reserve(indices.size() + o.indices.size());

			for (const auto &i : o.indices)
				indices.emplace_back(i + offset);
		}

		
		void Input::serialise(conv::Serialiser &ser) const
		{
			constexpr char const *n{"value"};
			
			ser << karhuIN(type);
			
			switch (type)
			{
				case TypeInput::boolean:  ser << karhuINn(n, boolean);  break;
				case TypeInput::integer:  ser << karhuINn(n, integer);  break;
				case TypeInput::floating: ser << karhuINn(n, floating); break;
				case TypeInput::vector2i: ser << karhuINn(n, vector2i); break;
				case TypeInput::vector3i: ser << karhuINn(n, vector3i); break;
				case TypeInput::vector4i: ser << karhuINn(n, vector4i); break;
				case TypeInput::vector2f: ser << karhuINn(n, vector2f); break;
				case TypeInput::vector3f: ser << karhuINn(n, vector3f); break;
				case TypeInput::vector4f: ser << karhuINn(n, vector4f); break;
				case TypeInput::matrix2:  ser << karhuINn(n, matrix2);  break;
				case TypeInput::matrix3:  ser << karhuINn(n, matrix3);  break;
				case TypeInput::matrix4:  ser << karhuINn(n, matrix4);  break;

				/// @todo: Probably shouldn't be (de)serialising material arrays because they are not copied but only references.
				case TypeInput::integerv:
				case TypeInput::floatingv:
				case TypeInput::vector2iv:
				case TypeInput::vector3iv:
				case TypeInput::vector4iv:
				case TypeInput::vector2fv:
				case TypeInput::vector3fv:
				case TypeInput::vector4fv:
				case TypeInput::matrix2v:
				case TypeInput::matrix3v:
				case TypeInput::matrix4v:
					break;
					
				case TypeInput::texture:
				{
					// Direct pointers cannot be serialised.
					if (Texref::Type::ref == texture.type)
						ser << karhuINn(n, texture.ref);
					
					break;
				}
				
				case TypeInput::invalid:
					break;
			}
		}

		void Input::deserialise(const conv::Serialiser &ser)
		{
			constexpr char const *n{"value"};

			ser >> karhuOUT(type);
			
			switch (type)
			{
				case TypeInput::boolean:  ser >> karhuOUTn(n, boolean);  break;
				case TypeInput::integer:  ser >> karhuOUTn(n, integer);  break;
				case TypeInput::floating: ser >> karhuOUTn(n, floating); break;
				case TypeInput::vector2i: ser >> karhuOUTn(n, vector2i); break;
				case TypeInput::vector3i: ser >> karhuOUTn(n, vector3i); break;
				case TypeInput::vector4i: ser >> karhuOUTn(n, vector4i); break;
				case TypeInput::vector2f: ser >> karhuOUTn(n, vector2f); break;
				case TypeInput::vector3f: ser >> karhuOUTn(n, vector3f); break;
				case TypeInput::vector4f: ser >> karhuOUTn(n, vector4f); break;
				case TypeInput::matrix2:  ser >> karhuOUTn(n, matrix2);  break;
				case TypeInput::matrix3:  ser >> karhuOUTn(n, matrix3);  break;
				case TypeInput::matrix4:  ser >> karhuOUTn(n, matrix4);  break;

				/// @todo: Probably shouldn't be (de)serialising material arrays because they are not copied but only references.
				case TypeInput::integerv:
				case TypeInput::floatingv:
				case TypeInput::vector2iv:
				case TypeInput::vector3iv:
				case TypeInput::vector4iv:
				case TypeInput::vector2fv:
				case TypeInput::vector3fv:
				case TypeInput::vector4fv:
				case TypeInput::matrix2v:
				case TypeInput::matrix3v:
				case TypeInput::matrix4v:
					break;

				case TypeInput::texture:
				{
					new (&texture) Texref{};
					
					// Direct pointers cannot be serialised.
					
					texture.type = Texref::Type::ref;
					
					ser >> karhuOUTn(n, texture.ref);
					
					break;
				}
				
				case TypeInput::invalid:
					break;
			}
		}
		
		/// @todo: Fix this. App shouldn't even be known under conv/.
		res::Texture *Input::Texref::get() const
		{
			switch (type)
			{
				case Texref::Type::ref: return app::App::instance()->res().get<res::Texture>(ref);
				case Texref::Type::ptr: return ptr;
			}
		}
		
		Input::Texref::Texref() = default;
		Input::Texref::Texref(Texref &&) = default;
		Input::Texref::Texref(const Texref &) = default;
		Input::Texref::~Texref() = default;
		
		/**
		 * Sets a named texture input that will be passed into the shader.
		 * If the value already exists it will be overwritten.
		 * In GLSL this corresponds to a uniform sampler2D.
		 *
		 * @param name  The name of the corresponding input in the shader.
		 * @param ID    The unique ID of the texture resource.
		 */
		void ContainerInputs::inputTexture(const std::string &name, const res::IDResource ID)
		{
			Input v;
			v.type = TypeInput::texture;
			new (&v.texture) Input::Texref{};
			v.texture.type = Input::Texref::Type::ref;
			v.texture.ref = ID;
			storeInput(name, std::move(v));
		}

		void ContainerInputs::inputTexture(const std::string &name, res::Texture &texture)
		{
			Input v;
			v.type = TypeInput::texture;
			new (&v.texture) Input::Texref{};
			v.texture.type = Input::Texref::Type::ptr;
			v.texture.ptr = &texture;
			storeInput(name, std::move(v));
		}
		
		void ContainerInputs::serialise(conv::Serialiser &ser) const
		{
			ser << karhuINn("values", m_inputs);
		}
		
		void ContainerInputs::deserialise(const conv::Serialiser &ser)
		{
			ser >> karhuOUTn("values", m_inputs);
		}
	}
}
