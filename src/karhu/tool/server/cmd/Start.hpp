/**
 * @author		Ava Skoog
 * @date		2017-07-05
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_SERVER_CMD_NEW_H_
	#define KARHU_TOOL_SERVER_CMD_NEW_H_
	
	#include <karhu/tool/Command.hpp>
	#include <karhu/tool/build/cmd/Base.hpp>
	#include <karhu/core/platform.hpp>
	#include <karhu/tool/SDK.hpp>

	#include <cstdint>

	namespace karhu
	{
		namespace tool
		{
			namespace server
			{
				namespace cmd
				{
					/**
					 * Creates a new Karhu project.
					 *
					 * Syntax:
					 * start [--port <offset>=default] [--sys <mac|windows|linux|ios|android|web>=local]
					 */
					class Start : public Command, private shared::cmd::Base
					{
						public:
							Start()
							:
							Command
							{
								Param{"sys", Enforce::optional, ArgsMinMax{1}},
								Param{"conf", Enforce::optional, ArgsMinMax{1}},
								Param{"dir", Enforce::optional, ArgsMinMax{1}}
							}
							{
							}

							bool processOpt(const OptWithVals &) override;
							bool finish() override;

						private:
							std::string m_sys, m_conf, m_dir;
					};
				}
			}
		}
	}
#endif
