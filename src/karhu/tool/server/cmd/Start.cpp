/**
 * @author		Ava Skoog
 * @date		2017-07-05
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/server/cmd/Start.hpp>
#include <karhu/tool/network/ServerDebug.hpp>
#include <karhu/network/Protocol.hpp>
//#include <karhu/tool/code/Hotloader.hpp>
#include <karhu/tool/proj/file/Project.hpp>
#include <karhu/tool/proj/file/build/common.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>
#include <boost/process.hpp>

#include <array>
#include <chrono>
#include <thread>
#include <atomic>
#include <functional>

#include <sstream>

namespace karhu
{
	namespace tool
	{
		namespace server
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool Start::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if (n == "sys")
						m_sys = v[0];
					else if (n == "dir")
						m_dir = v[0];
					else if (n == "conf")
					{
						if (validConfiguration(v[0]))
							m_conf = v[0];
						else
							return false;
					}

					return true;
				}

				/**
				 * Uses the validated information to start the server.
				 */
				bool Start::finish()
				{
					using namespace proj::file::build;
				
					namespace bp    = boost::process;
					namespace tools = SDK::paths::home::tools;
					
					if (m_conf.length() == 0)
						m_conf = config::toString(config::Type::debug);
					
					const auto conf(config::fromString(m_conf.c_str()));
					
					Platform sys{Platform::current};
					
					if (m_sys.length() > 0)
					{
						sys = platform::fromIdentifier(m_sys.c_str());
						if (Platform::unknown == sys)
						{
							log::err() << "Invalid platform '" << m_sys << "' specified"; /// @todo: Print valid?
							return false;
						}
					}

					proj::Location location{m_dir};
					auto project(proj::file::Project::load(location));

					log::msg() << "Loading project at '" << location.path() << "'...";

					if (!project)
					{
						log::err() << "Failed to load project";
						return false;
					}

					const auto build(project->build());
					if (!build)
					{
						log::err() << "Failed to load project build settings";
						return false;
					}
					
					auto port{karhu::network::protocol::defaultport()};
					if (const auto p = build->port())
						port = *p;
					else
						log::wrn() << "Found no port specified for project; defaulting to " << port << '.';

					/// @todo: Maybe only do this when running on PC in case it gets difficult to do on mobile and such…
//					code::Hotloader hotloader;
//					if (!hotloader.refreshModules(*project, conf, true)) return false;

					log::msg() << "Starting up client...";
					auto p(SDK::env::home(tools::karhubuild::local()));
					if (p && boost::filesystem::exists(*p))
					{
						std::vector<std::string> args;
						args.emplace_back("apply");
						
						args.emplace_back("--acts");
						args.emplace_back("run");
						
						args.emplace_back("--conf");
						args.emplace_back(m_conf);

						if (m_sys.length() > 0)
						{
							args.emplace_back("--sys");
							args.emplace_back(m_sys);
						}

						if (m_dir.length() > 0)
						{
							args.emplace_back("--dir");
							args.emplace_back(m_dir);
						}

						args.emplace_back("--server");
						
						bp::child c{*p, args, bp::std_out > bp::null, bp::std_err > bp::null};
						c.detach();
					}

					network::ServerDebug s(*project/*, conf*/);
					
//					if (!s.prepareModules()) return false;

					log::msg() << "Waiting for client...";

					if (!s.connect(sys, port))
					{
						log::err() << "Connection error";
						return false;
					}
					
					std::atomic<bool> run{true};

					std::thread input{[&s](std::atomic<bool> &run)
					{
						std::string buffer;

						while (run.load())
						{
							std::cin >> buffer;

							// Special command to quit.
							if (buffer == "disconnect")
								run.store(false);
							// Otherwise send a message!
							else
							{
								using P = karhu::network::Protocol;
								s.messenger().send(buffer);
							}
						}
					}, std::ref(run)};

					// Start the main server loop!
					while (run.load())
					{
						s.update();
						
						if (karhu::network::State::disconnected == s.state())
							return true;
					}

					return true;
				}
			}
		}
	}
}
