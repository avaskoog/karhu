/**
 * @author		Ava Skoog
 * @date		2017-07-05
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_SERVER_TOOL_H_
	#define KARHU_TOOL_SERVER_TOOL_H_

	#include <karhu/tool/Tool.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace server
			{
				class Tool : public tool::Tool
				{
					public:
						Tool();
				};
			}
		}
	}
#endif
