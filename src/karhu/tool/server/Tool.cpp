/**
 * @author		Ava Skoog
 * @date		2017-07-05
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/server/Tool.hpp>
#include <karhu/tool/server/cmd/Start.hpp>

namespace karhu
{
	namespace tool
	{
		namespace server
		{
			Tool::Tool()
			{
				registerCommand<cmd::Start>("start");
			}
		}
	}
}
