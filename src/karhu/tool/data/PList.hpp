/**
 * @author		Ava Skoog
 * @date		2017-04-04
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_DATA_PLIST_H_
	#define KARHU_TOOL_DATA_PLIST_H_

	#include <karhu/tool/proj/Identifier.hpp>
	#include <karhu/core/Nullable.hpp>

	/// @todo: Replace with Karhu XML interface eventually.
	#include <karhu/lib/tinyxml2/tinyxml2.h>

	#include <vector>
	#include <string>

	namespace karhu
	{
		namespace tool
		{
			namespace data
			{
				class PList
				{
					public:
						bool save(const std::string &file);
						virtual ~PList() = default;

					protected:
						PList
						(
							const proj::Identifier &identifier/*,
							const std::string &copyyears,
							const std::string &copyauthors*/
							/// @todo: Fix copyright.
						);

						bool identifier(const proj::Identifier &ID);
						bool copyright(const std::string &years, const std::string &authors);
						bool createDefaultIdentifier();
						bool createDefaultCopyright();

						void findDict();

					protected:
						tinyxml2::XMLElement *m_dict{nullptr};
						tinyxml2::XMLDocument m_XML;
					
					private:
						const proj::Identifier m_identifier;
				};
				
				class PListMac : public PList
				{
					public:
						PListMac(const proj::Identifier &identifier);
				};

				class PListIOS : public PList
				{
					public:
						PListIOS(const proj::Identifier &identifier);
				};
			}
		}
	}
#endif
