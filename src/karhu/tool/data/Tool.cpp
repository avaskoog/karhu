/**
 * @author		Ava Skoog
 * @date		2017-09-10
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/data/Tool.hpp>
#include <karhu/tool/data/cmd/JSON.hpp>
#include <karhu/tool/data/cmd/PList.hpp>
#include <karhu/tool/data/cmd/GLSL.hpp>

namespace karhu
{
	namespace tool
	{
		namespace data
		{
			Tool::Tool()
			{
				registerCommand<cmd::JSON >("json");
				registerCommand<cmd::PList>("plist");
				registerCommand<cmd::GLSL> ("glsl");
			}
		}
	}
}
