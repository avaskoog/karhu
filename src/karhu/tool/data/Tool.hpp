/**
 * @author		Ava Skoog
 * @date		2017-09-10
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_DATA_TOOL_H_
	#define KARHU_TOOL_DATA_TOOL_H_

	#include <karhu/tool/Tool.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace data
			{
				class Tool : public tool::Tool
				{
					public:
						Tool();
				};
			}
		}
	}
#endif
