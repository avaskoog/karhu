/**
 * @author		Ava Skoog
 * @date		2017-09-10
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/data/cmd/PList.hpp>
#include <karhu/tool/data/PList.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

#include <fstream>

namespace karhu
{
	namespace tool
	{
		namespace data
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool PList::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if (n == "act")
						m_act = v[0];
					else if (n == "file")
						m_file = v[0];
					else if (n == "type")
						m_type = v[0];
					else if (n == "id")
						m_identifier = v[0];

					return true;
				}

				/**
				 * Uses the validated information to return the formatted data.
				 */
				bool PList::finish()
				{
					if (const auto ID = proj::Identifier::create(m_identifier))
					{
						// This is the only supported action for now.
						if (m_act != "create")
						{
							log::err() << "Invalid action '" << m_act << "'. Valid actions are: 'create'.";
							return false;
						}
						
						// Validate type.
						if (m_type != "ios" && m_type != "mac")
						{
							log::err() << "Invalid action '" << m_act << "'. Valid actions are: 'ios' and 'mac'.";
							return false;
						}
						
						// Create the file.
						std::unique_ptr<data::PList> file;
						if      (m_type == "ios") file = std::make_unique<PListIOS>(*ID);
						else if (m_type == "mac") file = std::make_unique<PListMac>(*ID);
						
						if (!file->save(m_file))
						{
							log::err() << "Failed to save plist file to '" << m_file << "'";
							return false;
						}
						
						return true;
					}
					else
					{
						log::err() << "Invalid identifier '" << m_identifier << "'";
						return false;
					}
				}
			}
		}
	}
}
