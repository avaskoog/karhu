/**
 * @author		Ava Skoog
 * @date		2018-10-25
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_DATA_CMD_GLSL_H_
	#define KARHU_TOOL_DATA_CMD_GLSL_H_

	#include <karhu/tool/Command.hpp>
	#include <karhu/res/Shader.hpp>
	#include <karhu/core/meta.hpp>

	#include <vector>
	#include <string>

	namespace karhu
	{
		namespace tool
		{
			namespace data
			{
				namespace cmd
				{
					/**
					 * Converts a set of GLSL shaders written according to Karhu conventions
					 * to one or more shader files for final use depending on the format.
					 *
					 * Syntax:
					 * glsl --act <convert> --format <gl|gles|metal> --ins <files-with-extension...> --out <file-without-extension>
					 */
					class GLSL : public Command
					{
						private:
							enum class Action
							{
								invalid,
								convert
							};

						private:
							static constexpr Action strToAct(const char *s) noexcept
							{
								if (meta::string::equal(s, "convert")) return Action::convert;
								return Action::invalid;
							}
						
						public:
							GLSL()
							:
							Command
							{
								Param{"act",    Enforce::required, ArgsMinMax{1}},
								Param{"format", Enforce::required, ArgsMinMax{1}},
								Param{"ins",    Enforce::required, ArgsMinMax{1, -1}},
								Param{"out",    Enforce::optional, ArgsMinMax{1}},
								Param{"inject", Enforce::optional, ArgsMinMax{1}}
							}
							{
							}

							bool processOpt(const OptWithVals &) override;
							bool finish() override;
						
						private:
							bool validateAction();
							bool validateFormat();
						
							bool convertToGLSL
							(
								const karhu::res::Shader::Format &,
								const karhu::res::Shader::Mode &,
								const std::string &input,
								const std::string &suffix,
								const bool printOutputs = true
							);
						
							bool convertUsingScript
							(
								const karhu::res::Shader::Mode &,
								const std::string &suffix
							);
						
							bool validateTypeGLSL
							(
								const std::string &filename,
								karhu::res::Shader::TypeGLSL &outType,
								std::string &outSuffixless
							);

							void printErrorTypeGLSL(const std::string &filename);
						
							std::string outputpathGLSL
							(
								const std::string &filename,
								const std::string &suffix,
								const karhu::res::Shader::TypeGLSL &type
							);
						
						private:
							Action m_act{Action::invalid};
							karhu::res::Shader::Format m_format{karhu::res::Shader::Format::invalid};
							std::string m_out, m_inject;
							std::vector<std::string> m_ins;
					};
				}
			}
		}
	}
#endif
