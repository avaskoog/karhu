/**
 * @author		Ava Skoog
 * @date		2018-10-25
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/data/cmd/GLSL.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/log.hpp>
#include <karhu/core/platform.hpp>

#include <boost/process.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

#include <fstream>

namespace
{
	using namespace karhu;
	using namespace karhu::tool;
	using namespace karhu::tool::data;
	using namespace karhu::tool::data::cmd;
	
	std::string recursiveLoadAndIncludeGLSL
	(
		std::string              const &directory,
		std::string              const &file,
		std::vector<std::string>       &included
	)
	{
		namespace fs = boost::filesystem;
		
		fs::path pathCurrent{directory};
		pathCurrent.append(file);
		pathCurrent.make_preferred();
		
		std::string const pathCurrentString{pathCurrent.string()};
		
		included.emplace_back(pathCurrentString);
		
		if (!fs::exists(pathCurrent) || !fs::is_regular_file(pathCurrent))
		{
			log::err()
				<< "Could not find file '"
				<< pathCurrentString
				<< "'.";
			
			return {};
		}

		std::ifstream fi{pathCurrentString};
		
		if (!fi.is_open())
		{
			log::err()
				<< "Could not open file '"
				<< pathCurrentString
				<< "' for reading.";
			
			return {};
		}
		
		std::stringstream contents;
		contents << fi.rdbuf();
		
		fi.close();
		
		// Easier to loop through C string since \0 counts as a character too.
		
		std::string const contentsString{contents.str()};
		char const *const contentsCString{contentsString.c_str()};
		std::size_t const length{contentsString.length()};
		char const *const end{contentsCString + length};
		
		constexpr char const * keyword{"#include"};
		constexpr std::size_t lengthKeyword{8};
		
		enum class Block
		{
			code,
			commentSingle,
			commentMulti,
			preprocessorSingle,
			preprocessorMulti
		} block{Block::code};
		
		std::string code;
		std::size_t i{0};
		
		for (char const *c{contentsCString}; c != end; ++ c, ++ i)
		{
			switch (block)
			{
				case Block::code:
				{
					if ('/' == *c)
					{
						if ('/' == *(c + 1))
							block = Block::commentSingle;
						else if ('*' == *(c + 1))
							block = Block::commentMulti;
					}
					else if ('#' == *c)
					{
						if
						(
							(i + lengthKeyword) < length &&
							contentsString.substr(i, lengthKeyword) == keyword
						)
						{
							std::string f;
							
							i += lengthKeyword;
							c += lengthKeyword;
							
							for (; c != end; ++ c, ++ i)
							{
								if (' ' == *c || '\t' == *c || '\"' == *c || '<' == *c || '>' == *c)
									continue;
								
								if ('\n' == *c)
									break;
								
								f += *c;
							}
							
							fs::path fileNext{directory};
							fileNext.append(f);
							fileNext.make_preferred();
							
							auto const it(std::find(included.begin(), included.end(), fileNext.string()));
							
							// Already included this file.
							if (it != included.end())
								continue;
							
							fs::path directoryNext{fileNext};
							directoryNext.remove_filename();
							directoryNext.make_preferred();
							
							fileNext = fileNext.filename();
							fileNext.make_preferred();
							
							code += recursiveLoadAndIncludeGLSL
							(
								directoryNext.string(),
								fileNext.string(),
								included
							);
							
							code += '\n';
							
							continue;
						}
						else
							block = Block::preprocessorSingle;
					}
					
					break;
				}
				
				case Block::commentSingle:
				{
					if ('\n' == *c)
						block = Block::code;
					
					break;
				}
				
				case Block::commentMulti:
				{
					if ('*' == *c && '/' == *(c + 1))
						block = Block::code;
					
					break;
				}
				
				case Block::preprocessorSingle:
				{
					if ('\\' == *c)
						block = Block::preprocessorMulti;
					else if ('\n' == *c)
						block = Block::code;
					
					break;
				}
				
				case Block::preprocessorMulti:
				{
					if ('\n' == *c)
						block = Block::preprocessorSingle;
					
					break;
				}
			}
			
			code += *c;
		}
		
		return code;
	}
}

namespace karhu
{
	namespace tool
	{
		namespace data
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool GLSL::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if      (n == "act")    m_act    = strToAct(v[0].c_str());
					else if (n == "format") m_format = karhu::res::Shader::strToFormat(v[0].c_str());
					else if (n == "out")    m_out    = v[0];
					else if (n == "inject") m_inject = v[0];
					else if (n == "ins")
					{
						m_ins.resize(v.size());
						for (std::size_t i{0}; i < v.size(); ++ i)
							m_ins[i] = v[i];
					}

					return true;
				}

				/**
				 * Uses the validated information to carry out the specified action.
				 */
				bool GLSL::finish()
				{
					/// @todo: Output with mode 'both' for GLSL fragment shaders and all Metal shaders instead.
					
					using namespace std::literals::string_literals;

					if (!validateAction())
						return false;
					
					if (!validateFormat())
						return false;

					switch (m_format)
					{
						case karhu::res::Shader::Format::GL:
						case karhu::res::Shader::Format::GLES:
						{
							const std::string
								suffix{"-"s + karhu::res::Shader::formatToStr(m_format)},
								sing  {suffix + karhu::res::Shader::suffixForMode(karhu::res::Shader::Mode::single)},
								mult  {suffix + karhu::res::Shader::suffixForMode(karhu::res::Shader::Mode::multiple)};
							
							for (const auto &i : m_ins)
							{
								if (!convertToGLSL(m_format, karhu::res::Shader::Mode::single, i, sing))
									return false;

								if (!convertToGLSL(m_format, karhu::res::Shader::Mode::multiple, i, mult))
									return false;
							}

							break;
						}
						
						case karhu::res::Shader::Format::metal:
						{
							if (0 == m_out.length())
							{
								log::err() << "An explicit output name (without extension) must be supplied using '--out'!";
								return false;
							}

							return
							(
								convertUsingScript
								(
									karhu::res::Shader::Mode::single,
									"-msl"s + karhu::res::Shader::suffixForMode(karhu::res::Shader::Mode::single)
								) &&
								convertUsingScript
								(
									karhu::res::Shader::Mode::multiple,
									"-msl"s + karhu::res::Shader::suffixForMode(karhu::res::Shader::Mode::multiple)
								)
							);
						}
						
						case karhu::res::Shader::Format::invalid:
							break;
					}
					
					return true;
				}
				
				bool GLSL::validateAction()
				{
					if (Action::invalid == m_act)
					{
						log::err()
							<< "Invalid action! Must be one of the following: "
							<< "convert";
						return false;
					}
					
					return true;
				}
				
				bool GLSL::validateFormat()
				{
					if (karhu::res::Shader::Format::invalid == m_format)
					{
						log::err()
							<< "Invalid format! Must be one of the following: "
							<< "gl, gles or metal";
						return false;
					}
					
					return true;
				}
				
				bool GLSL::convertToGLSL
				(
					const karhu::res::Shader::Format &format,
					const karhu::res::Shader::Mode &mode,
					const std::string &input,
					const std::string &suffix,
					const bool printOutputs
				)
				{
					namespace fs = boost::filesystem;

					karhu::res::Shader::TypeGLSL typeGLSL;
					std::string suffixless;

					if (!validateTypeGLSL
					(
						fs::path{input}.filename().string(),
						typeGLSL,
						suffixless
					))
						return false;

					if (!fs::exists(input))
					{
						log::err()
							<< "Could not find file '"
							<< input
							<< "'.";
						return false;
					}
					
					fs::path file{input}, directory{input};
					file = file.filename();
					directory.remove_filename();
					
					std::vector<std::string> included;
					
					std::string source = recursiveLoadAndIncludeGLSL
					(
						directory.string(),
						file.string(),
						included
					);
					
					if (!m_inject.empty())
						source = m_inject + '\n' + source;
					
					source = karhu::res::Shader::addHeaderToGLSL
					(
						format,
						mode,
						typeGLSL,
						karhu::res::Shader::addDefaultsToGLSL
						(
							format,
							mode,
							typeGLSL,
							source.c_str()
						).c_str()
					);
					
					const std::string outputpath{outputpathGLSL(suffixless, suffix, typeGLSL)};
					std::ofstream fo{outputpath, std::ios_base::trunc};
					if (!fo.is_open())
					{
						log::err()
							<< "Could not open file '"
							<< outputpath
							<< "' for writing.";
						return false;
					}
					
					fo << source;
					
					if (printOutputs)
					{
						log::msg() << outputpath;
						log::msg() << static_cast<std::int64_t>(mode);
					}

					return true;
				}
				
				bool GLSL::convertUsingScript
				(
					const karhu::res::Shader::Mode &mode,
					const std::string &suffix
				)
				{
					namespace fs      = boost::filesystem;
					namespace bp      = boost::process;
					namespace scripts = SDK::paths::home::scripts;

					const std::string tmpsuffix{suffix + ".karhutmp"};
					std::vector<std::string> ins;

					// Convert to regular GLSL first to get the full files.
					for (const auto &i : m_ins)
					{
						karhu::res::Shader::TypeGLSL t;
						std::string suffixless;

						if (!validateTypeGLSL
						(
							fs::path{i}.filename().string(),
							t,
							suffixless
						))
							return false;
						
						if (!convertToGLSL(karhu::res::Shader::Format::GL, mode, i, tmpsuffix, false))
							return false;

						ins.emplace_back(outputpathGLSL(suffixless, tmpsuffix, t));
					}
					
					// Perform the actual non-GLSL conversion.
					const auto p(SDK::env::home(scripts::convertGLSL::UNIX()));
					if (p && fs::exists(*p))
					{
						std::vector<std::string> args;
						
						// System we are running the script on.
						args.emplace_back(platform::identifier(Platform::current));
						
						// The shader output format.
						args.emplace_back(karhu::res::Shader::formatToStr(m_format));
						
						// The output name without file extension.
						args.emplace_back(m_out + suffix);

						// The input files.
						for (const auto &i : ins)
							args.emplace_back(i);

						bp::ipstream pi;
						boost::process::child c{*p, args, bp::std_out > pi, bp::std_err > stderr};
						
						std::vector<std::string> files;
						std::string line;
						
						auto const getLine([&]()
						{
							if (std::getline(pi, line) && !line.empty())
								files.emplace_back(line);
						});
						
						while (c.running())
							getLine();
						
						getLine();
						
						c.wait();
						
						if (0 != c.exit_code())
						{
							log::err() << "Conversion failed.";
							return false;
						}
						
						for (const auto &f : files)
						{
							log::msg() << f;
							log::msg() << static_cast<std::int64_t>(mode);
						}
					}
					else
					{
						log::err()
							<< "Could not find GLSL conversion script at '"
							<< *p
							<< "'.";
						return false;
					}
					
					// Clean up.
					for (const auto &i : ins)
						if (fs::exists(i))
							fs::remove(i);

					return true;
				}
				
				bool GLSL::validateTypeGLSL
				(
					const std::string &filename,
					karhu::res::Shader::TypeGLSL &outType,
					std::string &outSuffixless
				)
				{
					if (filename.length() < 5)
					{
						printErrorTypeGLSL(filename);
						return false;
					}
					
					outSuffixless = filename.substr(0, filename.length() - 5);

					const std::string suffix{filename.substr(filename.length() - 5, 5)};
					
					if (suffix == karhu::res::Shader::suffixForTypeGLSL(karhu::res::Shader::TypeGLSL::vert))
						outType = karhu::res::Shader::TypeGLSL::vert;
					else if (suffix == karhu::res::Shader::suffixForTypeGLSL(karhu::res::Shader::TypeGLSL::frag))
						outType = karhu::res::Shader::TypeGLSL::frag;
					else
					{
						printErrorTypeGLSL(filename);
						return false;
					}
					
					return true;
				}
				
				void GLSL::printErrorTypeGLSL(const std::string &filename)
				{
					log::err()
						<< "Invalid GLSL file name '"
						<< filename
						<< "'! Must end in one of the following: "
						<< karhu::res::Shader::suffixForTypeGLSL(karhu::res::Shader::TypeGLSL::vert)
						<< " or "
						<< karhu::res::Shader::suffixForTypeGLSL(karhu::res::Shader::TypeGLSL::frag);
				}
				
				std::string GLSL::outputpathGLSL
				(
					const std::string &filename,
					const std::string &suffix,
					const karhu::res::Shader::TypeGLSL &type
				)
				{
					namespace fs = boost::filesystem;
					
					fs::path outputpath{m_out};

					if (karhu::res::Shader::Format::GL != m_format && karhu::res::Shader::Format::GLES != m_format)
						outputpath.remove_filename();

					outputpath.append(filename + suffix + karhu::res::Shader::suffixForTypeGLSL(type));
					outputpath.make_preferred();
					
					return outputpath.string();
				}
			}
		}
	}
}
