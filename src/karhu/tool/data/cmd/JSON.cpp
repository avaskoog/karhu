/**
 * @author		Ava Skoog
 * @date		2017-09-10
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/data/cmd/JSON.hpp>
#include <karhu/conv/types.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

#include <iostream>
#include <fstream>

namespace karhu
{
	namespace tool
	{
		namespace data
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool JSON::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if (n == "act")
						m_act = v[0];
					else if (n == "file")
						m_file = v[0];
					else if (n == "separators")
					{
						m_separators.resize(v.size());
						for (std::size_t i{0}; i < v.size(); ++ i)
							m_separators[i] = v[i];
					}
					else if (n == "data")
					{
						m_type = v[0];
						
						for (std::size_t i{1}; i < v.size(); ++ i)
							m_hierarchy.emplace_back(v[i]);
					}

					return true;
				}

				/**
				 * Uses the validated information to return the formatted data.
				 */
				bool JSON::finish()
				{
					using T = karhu::JSON::TypeValue;
					using N = karhu::JSON::TypeNumber;

					// List of valid types to fetch.
					static const std::array<const std::string, 6> types
					{
						"boolean",
						"string",
						"object",
						"array",
						"integer",
						"float"
					};

					// This is the only supported action for now.
					if (m_act != "format")
					{
						log::err() << "Invalid action '" << m_act << "'. Valid actions are: 'format'.";
						return false;
					}

					// Validate type.
					const auto it(std::find(types.begin(), types.end(), m_type));
					if (it == types.end()) return false;
					
					// Convert to JSON type.
					T type{T::null};
					if (m_type == "integer" || m_type == "float")
						type = T::number;
					else
						type = *karhu::JSON::typeValueFromString(m_type);

					// Try to open the JSON file.
					std::ifstream f{m_file};
					if (!f.is_open()) return false;
					
					// Try to parse the JSON data.
					const auto parsed(conv::JSON::Parser{}.parse(f));
					if (!parsed || !parsed->object()) return false;

					// Try to find the value.
					const auto value(parsed->object()->find(m_hierarchy));
					if (!value) return false;

					// Validate the type.
					if (value->type() != type) return false;

					// If array, output separated values.
					if (value->array())
					{
						std::size_t i{0};
						for (const auto &v : *value->array())
						{
							std::cout << v.stringify(m_separators[0].c_str(), m_separators[1].c_str(), true);
							if ((i + 1) < value->array()->count())
								std::cout << m_separators[0];
							++ i;
						}
					}
					// Same for objects, with an additional key-value separator.
					else if (value->object())
					{
						std::size_t i{0};
						for (const auto &v : *value->object())
						{
							std::cout << v.first << m_separators[1] << v.second.stringify(m_separators[0].c_str(), m_separators[1].c_str(), true);
							if ((i + 1) < value->object()->count())
								std::cout << m_separators[0];
							++ i;
						}
					}
					// Output anything else as string.
					else
						std::cout << value->stringify(m_separators[0].c_str(), m_separators[1].c_str(), true);
					
					return true;
				}
			}
		}
	}
}
