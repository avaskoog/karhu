/**
 * @author		Ava Skoog
 * @date		2017-09-10
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_DATA_CMD_PLIST_H_
	#define KARHU_TOOL_DATA_CMD_PLIST_H_
	
	#include <karhu/tool/Command.hpp>

	#include <vector>
	#include <string>

	namespace karhu
	{
		namespace tool
		{
			namespace data
			{
				namespace cmd
				{
					/**
					 * Returns a formatted string with the value(s) of the key(s) specified.
					 */
					class PList : public Command
					{
						public:
							PList()
							:
							Command
							{
								Param{"act",  Enforce::required, ArgsMinMax{1}},
								Param{"file", Enforce::required, ArgsMinMax{1}},
								Param{"type", Enforce::required, ArgsMinMax{1}},
								Param{"id",   Enforce::required, ArgsMinMax{1}}
							}
							{
							}

							bool processOpt(const OptWithVals &) override;
							bool finish() override;

						private:
							std::string m_act, m_file, m_type, m_identifier;
							std::vector<std::string> m_separators{2}, m_hierarchy;
					};
				}
			}
		}
	}
#endif
