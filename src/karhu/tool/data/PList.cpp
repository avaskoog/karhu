/**
 * @author		Ava Skoog
 * @date		2017-04-04
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/data/PList.hpp>
#include <chrono>
#include <ctime>
#include <sstream>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

namespace
{
	class KeyArr
	{
		public:
			KeyArr(tinyxml2::XMLElement *elementKey = nullptr, tinyxml2::XMLElement *elementArr = nullptr)
			:
			m_elementKey{elementKey},
			m_elementArr{elementArr}
			{
			}

			std::string key() const
			{
				if (m_elementKey && m_elementKey->GetText())
					return m_elementKey->GetText();

				return {};
			}
		
			tinyxml2::XMLElement *arr() noexcept { return m_elementArr; }

		private:
			tinyxml2::XMLElement *m_elementKey{nullptr}, *m_elementArr{nullptr};
	};
	
	template<typename T>
	class Val
	{
		public:
			Val(tinyxml2::XMLElement *elementVal = nullptr)
			:
			m_elementVal{elementVal}
			{
			}
			
			T val() const { return {}; }
			bool val(const T &) { return false; }

		private:
			tinyxml2::XMLElement *m_elementVal{nullptr};
	};

	template<typename T>
	class KeyVal
	{
		public:
			KeyVal(tinyxml2::XMLElement *elementKey = nullptr, tinyxml2::XMLElement *elementVal = nullptr)
			:
			m_elementKey{elementKey},
			m_val       {elementVal}
			{
			}

			std::string key() const
			{
				if (m_elementKey && m_elementKey->GetText())
					return m_elementKey->GetText();
				
				return {};
			}
			
			T val() const { return m_val.val(); }
			bool val(const T &value) { return m_val.val(value); }

		private:
			tinyxml2::XMLElement *m_elementKey{nullptr};
			Val<T> m_val;
	};

	template<>
	std::string Val<std::string>::val() const
	{
		if (m_elementVal && m_elementVal->GetText())
			return m_elementVal->GetText();

		return {};
	};
	
	template<>
	bool Val<std::string>::val(const std::string &value)
	{
		if (m_elementVal)
		{
			m_elementVal->SetText(value.c_str());
			return true;
		}
		
		return false;
	}

	template<>
	bool Val<bool>::val(const bool &value)
	{
		if (m_elementVal)
		{
			m_elementVal->SetValue((value) ? "true" : "false");
			return true;
		}
		
		return false;
	}

	template<>
	bool Val<bool>::val() const
	{
		using namespace std::literals::string_literals;

		if (m_elementVal && m_elementVal->Value())
			return ("true"s == m_elementVal->Value());

		return false;
	};
	
	static tinyxml2::XMLElement *getElementKey(tinyxml2::XMLElement *parent, const std::string &name, const bool create)
	{
		if (parent)
		{
			auto k(parent->FirstChildElement("key"));
			while (k)
			{
				if (k->GetText() && name == k->GetText())
					return k;

				k = k->NextSiblingElement("key");
			}
			
			if (create)
			{
				k = parent->GetDocument()->NewElement("key");
				k->SetText(name.c_str());
				parent->LinkEndChild(k);
				
				return k;
			}
		}
		
		return nullptr;
	}
	
	template<typename T>
	static KeyVal<T> getKeyVal(tinyxml2::XMLElement *parent, const std::string &key, const bool create = false)
	{
		return {};
	}
	
	template<>
	KeyVal<std::string> getKeyVal(tinyxml2::XMLElement *parent, const std::string &key, const bool create)
	{
		if (auto k = getElementKey(parent, key, create))
		{
			auto v(k->NextSiblingElement("string"));
			if (!v && create)
			{
				v = parent->GetDocument()->NewElement("string");
				parent->InsertAfterChild(k, v);
			}
			
			if (v) return {k, v};
		}
		
		return {};
	}
	
	template<>
	KeyVal<bool> getKeyVal(tinyxml2::XMLElement *parent, const std::string &key, const bool create)
	{
		if (auto k = getElementKey(parent, key, create))
		{
			auto v(k->NextSiblingElement("true"));
			if (!v) k->NextSiblingElement("false");
			
			if (!v && create)
			{
				v = parent->GetDocument()->NewElement("false");
				parent->InsertAfterChild(k, v);
			}

			if (v) return {k, v};
		}
		
		return {};
	}
	
	template<typename T>
	bool setKeyVal(tinyxml2::XMLElement *parent, const std::string &key, const T &value, const bool create)
	{
		auto kv(getKeyVal<T>(parent, key, create));
		return kv.val(value);
	}
	
	template<typename T>
	bool createVal(tinyxml2::XMLElement *parent, const T &value)
	{
		return false;
	}
	
	template<>
	bool createVal(tinyxml2::XMLElement *parent, const std::string &value)
	{
		if (parent)
		{
			auto v(parent->GetDocument()->NewElement("string"));
			parent->LinkEndChild(v);
			return Val<std::string>{v}.val(value);
		}
		
		return false;
	}
	
	template<>
	bool createVal(tinyxml2::XMLElement *parent, const bool &value)
	{
		if (parent)
		{
			auto v(parent->GetDocument()->NewElement(""));
			parent->LinkEndChild(v);
			return Val<bool>{v}.val(value);
		}
		
		return false;
	}
	
	KeyArr getKeyArr(tinyxml2::XMLElement *parent, const std::string &key, const bool create)
	{
		if (auto k = getElementKey(parent, key, create))
		{
			auto v(k->NextSiblingElement("array"));
			if (!v && create)
			{
				v = parent->GetDocument()->NewElement("array");
				parent->LinkEndChild(v);
			}

			if (v) return {k, v};
		}

		return {};
	}
}

namespace karhu
{
	namespace tool
	{
		namespace data
		{
			PList::PList(const proj::Identifier &identifier)
			:
			m_identifier{identifier}
			{
				/// @todo: Generate basic plist with defaults that apply to both Mac and iOS.
				constexpr char const *base
				(
					"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
					"<plist version=\"1.0\">"
					"<dict>"
					"</dict>"
					"</plist>"
				);
				
				m_XML.Parse(base);
				findDict();
				
				// Insert the doctype since we cannot parse one.
				{
					if (auto e = m_XML.GetDocument()->NewUnknown("DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\""))
						m_XML.InsertAfterChild(m_XML.FirstChild(), e);
				}

				// Add the values Mac and iOS have in common.
				// Specific values are added in derived constructors.

				//createDefaultIdentifier();
				this->identifier(identifier);
				createDefaultCopyright();
				
				using namespace std::literals::string_literals;

				setKeyVal(m_dict, "CFBundleInfoDictionaryVersion", "6.0"s,                true);
				setKeyVal(m_dict, "CFBundleDevelopmentRegion",     "en"s,                 true);
				setKeyVal(m_dict, "CFBundlePackageType",           "APPL"s,               true);
				setKeyVal(m_dict, "CFBundleExecutable",            "$(EXECUTABLE_NAME)"s, true);
				setKeyVal(m_dict, "CFBundleName",                  "$(EXECUTABLE_NAME)"s, true);
				setKeyVal(m_dict, "CFBundleVersion",               "1"s,                  true);
				setKeyVal(m_dict, "CFBundleShortVersionString",    "1.0"s,                true);
				setKeyVal(m_dict, "CFBundleSignature",             "????"s,               true);
			}

			bool PList::identifier(const proj::Identifier &ID)
			{
				return setKeyVal
				(
					m_dict,
					"CFBundleIdentifier",
					ID.full(),
					true
				);
			}

			/// @todo Copyright string should be a project-global thing to construct, not plist/xcode/apple specific.
			bool PList::copyright(const std::string &years, const std::string &authors)
			{
				std::stringstream s;
				s << "Copyright © " << years << ' ' << authors << ". All rights reserved.";
				
				return setKeyVal(m_dict, "NSHumanReadableCopyright", s.str(), true);
			}

//			bool PList::load(const std::string &file)
//			{
//				if (!m_location.valid()) return false;
//
//				boost::filesystem::path p{m_location.path()};
//				boost::filesystem::path s{file};
//				s.make_preferred();
//				p.append(s.string());
//				p.make_preferred();
//
//				if (tinyxml2::XMLError::XML_SUCCESS == m_XML.LoadFile(p.c_str()))
//				{
//					findDict();
//					return true;
//				}
//
//				return false;
//			}
			
			bool PList::save(const std::string &file)
			{
				return (tinyxml2::XMLError::XML_SUCCESS == m_XML.SaveFile(file.c_str()));
			}

			bool PList::createDefaultCopyright()
			{
				/// @todo: Definitely code up some more convenient Karhu utils for date and time...

				using namespace std::chrono;
				
				auto now(system_clock::to_time_t(system_clock::now()));
				auto local(std::localtime(&now));
				auto year(1900 + local->tm_year);
				
				std::stringstream s;
				s << year;

				return copyright(s.str(), "AUTHOR");
			}
			
			void PList::findDict()
			{
				if (auto plist = m_XML.FirstChildElement("plist"))
					m_dict = plist->FirstChildElement("dict");
				else
					m_dict = nullptr;
			}

			PListMac::PListMac(const proj::Identifier &identifier)
			:
			PList{identifier}
			{
				using namespace std::literals::string_literals;
				
				// Add the values specific to Mac.
				setKeyVal(m_dict, "LSMinimumSystemVersion", "$(MACOSX_DEPLOYMENT_TARGET)"s, true);
				setKeyVal(m_dict, "CFBundleIconFile", "Icon"s, true);
				
				/// @todo: Figure out if these are actually necessary.
				setKeyVal(m_dict, "NSMainNibFile",    "MainMenu"s,      true);
				setKeyVal(m_dict, "NSPrincipalClass", "NSApplication"s, true);
			}

//			bool PListMac::load()
//			{
//				return PList::load(SDK::paths::project::plist::mac());
//			}
//
//			bool PListMac::save()
//			{
//				return PList::save(SDK::paths::project::plist::mac());
//			}
//
//			bool PListIOS::load()
//			{
//				return PList::load(SDK::paths::project::plist::iOS());
//			}
//
//			bool PListIOS::save()
//			{
//				return PList::save(SDK::paths::project::plist::iOS());
//			}

			PListIOS::PListIOS(const proj::Identifier &identifier)
			:
			PList{identifier}
			{
				using namespace std::literals::string_literals;

				// Add the values specific to iOS.
				setKeyVal(m_dict, "LSRequiresIPhoneOS", true, true);
				
				if (auto arr = getKeyArr(m_dict, "UIRequiredDeviceCapabilities", true).arr())
					createVal(arr, "armv7"s);
				
				/// @todo: Add abstractions to set orientations.

				if (auto arr = getKeyArr(m_dict, "UISupportedInterfaceOrientations", true).arr())
					createVal(arr, "UIInterfaceOrientationLandscapeRight"s);
				
				if (auto arr = getKeyArr(m_dict, "UISupportedInterfaceOrientations~ipad", true).arr())
					createVal(arr, "UIInterfaceOrientationLandscapeRight"s);
			}
		}
	}
}
