/**
 * @author		Ava Skoog
 * @date		2017-04-04
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_SDK_H_
	#define KARHU_TOOL_SDK_H_

	#include <karhu/core/Nullable.hpp>
	#include <karhu/core/platform.hpp>
	#include <karhu/data/JSON.hpp>
	#include <cstdlib>
	#include <string>

	namespace karhu
	{
		namespace SDK
		{
			namespace keys
			{
				inline constexpr auto home() noexcept { return "KARHU_HOME"; }
			}

			namespace values
			{
				namespace lib
				{
					namespace name
					{
						inline constexpr auto lowercase() noexcept { return "karhu"; }
						inline constexpr auto formatted() noexcept { return "Karhu"; }
					}
					
					namespace id
					{
						inline constexpr auto ext()  noexcept { return "com"; }
						inline constexpr auto org()  noexcept { return "berza"; }
						inline constexpr auto proj() noexcept { return name::lowercase(); }
					}
				}

				namespace project
				{
					inline constexpr auto defaulttemplate() noexcept { return "project"; }
					inline constexpr auto defaultname()     noexcept { return "Game"; }
				}
			}

			namespace paths
			{
				namespace project
				{
					inline constexpr auto info()  noexcept { return "info.karhuproj.json"; }
					inline constexpr auto build() noexcept { return "build.karhuproj.json"; }
					
					namespace generated
					{
						inline constexpr auto scriptsymbols() noexcept { return "scriptsymbols.inc"; }
					}
					
					namespace res
					{
						inline constexpr auto graphics() noexcept { return "_graphics.karhudat.json"; }
					}
				}
				
				namespace home
				{
					namespace project
					{
						std::string templatedir(const std::string &subfolder = {});
					}
					
					namespace lib
					{
						inline constexpr auto projdir() noexcept { return "lib/karhu"; }
					}
					
					namespace tools
					{
						namespace karhudata
						{
							inline constexpr auto local() noexcept
							{
								if (platform::is(Platform::mac))
									return "tools/mac/karhudata";
								else if (platform::is(Platform::linux))
									return "tools/linux/karhudata";
								
								return "";
							}
						}
						
						namespace karhuproj
						{
							inline constexpr auto local() noexcept
							{
								if (platform::is(Platform::mac))
									return "tools/mac/karhuproj";
								else if (platform::is(Platform::linux))
									return "tools/linux/karhuproj";
								
								return "";
							}
						}
						
						namespace karhubuild
						{
							inline constexpr auto local() noexcept
							{
								if (platform::is(Platform::mac))
									return "tools/mac/karhubuild";
								else if (platform::is(Platform::linux))
									return "tools/linux/karhubuild";
								
								return "";
							}
						}
						
						namespace karhuserver
						{
							inline constexpr auto local() noexcept
							{
								if (platform::is(Platform::mac))
									return "tools/mac/karhuserver";
								else if (platform::is(Platform::linux))
									return "tools/linux/karhuserver";
								
								return "";
							}
						}
					}
					
					namespace scripts
					{
						/// @todo: Fix script paths for missing targets.
						
						namespace push
						{
							inline constexpr auto UNIX() noexcept { return "tools/scripts/unix/karhupush.sh"; }
						}
						
						namespace pull
						{
							inline constexpr auto UNIX() noexcept { return "tools/scripts/unix/karhupull.sh"; }
						}

						namespace SSH
						{
							inline constexpr auto mac() noexcept { return "tools/scripts/unix/karhussh.sh"; }
						}
						
						namespace server
						{
							/// @todo: Shouldn't this be UNIX()..?
							inline constexpr auto web() noexcept { return "tools/scripts/unix/karhuserverweb.sh"; }
						}
						
						namespace generateImages
						{
							inline constexpr auto UNIX() noexcept { return "tools/scripts/unix/karhuimg.sh"; }
						}
						
						namespace convertGLSL
						{
							inline constexpr auto UNIX() noexcept { return "tools/scripts/unix/karhuglslconv.sh"; }
						}
					}
				}
			}

			namespace env
			{
				/**
				 * Returns the home folder of the Karhu SDK installation.
				 * Needs to be set as an environment variable to be retrieved correctly.
				 * The name of the variable is defined by SDK::keys::home().
				 *
				 * @param subpath Optional subpath to append to the home directory in case of success.
				 *
				 * @return The path if the environment variable is found, or an invalid nullable.
				 */
				Nullable<std::string> home(const std::string &subpath = {});
			}
		}
	}
#endif
