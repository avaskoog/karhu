/**
 * @author		Ava Skoog
 * @date		2017-09-19
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/code/Analyser.hpp>
#include <karhu/tool/proj/file/build/Module.hpp>
#include <karhu/core/log.hpp>

#include <clang-c/Index.h>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>
#include <boost/process.hpp>

namespace karhu
{
	namespace tool
	{
		namespace code
		{
			namespace
			{
				using CallbackVisitor = CXChildVisitResult (*)(CXCursor, CXCursor, CXClientData);

				std::string getCursorKindName(CXCursorKind cursorKind)
				{
					CXString kindName{clang_getCursorKindSpelling(cursorKind)};
					std::string result{clang_getCString(kindName)};
					clang_disposeString(kindName);
					return result;
				}

				std::string getCursorSpelling(CXCursor cursor)
				{
					CXString cursorSpelling{clang_getCursorSpelling(cursor)};
					std::string result{clang_getCString(cursorSpelling)};
					clang_disposeString(cursorSpelling);
					return result;
				}

				std::string getTypeSpelling(CXType type)
				{
					CXString spelling{clang_getTypeSpelling(type)};
					std::string result{clang_getCString(spelling)};
					clang_disposeString(spelling);
					return result;
				}

				std::string getTypeKindName(CXTypeKind kind)
				{
					CXString spelling{clang_getTypeKindSpelling(kind)};
					std::string result{clang_getCString(spelling)};
					clang_disposeString(spelling);
					return result;
				}
				
				std::string getCursorFilename(CXCursor cursor)
				{
					std::string result;
					
					CXSourceLocation loc{clang_getCursorLocation(cursor)};
					CXFile file{nullptr};

					clang_getSpellingLocation(loc, &file, nullptr, nullptr, nullptr);
					
					if (file)
					{
						CXString filename{clang_getFileName(file)};
						result = clang_getCString(filename);
						clang_disposeString(filename);
					}

					return result;
				}
				
				Nullable<std::string> outputAST
				(
					const proj::file::build::Config::DataTarget &dataTarget,
					const std::string &path,
					const std::string &file
				)
				{
					namespace fs = boost::filesystem;
					namespace bp = boost::process;
					
					fs::path p{path};
					p.make_preferred();

					if (!fs::exists(p) || !fs::is_directory(p))
						return {};
					
					fs::path clang{bp::search_path("clang++")};
					if (!fs::exists(clang))
					{
						karhu::log::err() << "Error analysing code: could not find clang++";
						return {};
					}
					
					std::vector<std::string> args;

					args.emplace_back("-emit-ast");
					
					// Find the build flags from the target.
					if (dataTarget.data.object())
					{
						using namespace std::literals::string_literals;

						if (const auto vals = dataTarget.data.object()->getArray("includedirs"))
							for (const auto &v: *vals)
								if (v.string())
									args.emplace_back("-I" + *v.string()); /// @todo: Escape?

						if (const auto vals = dataTarget.data.object()->getArray("buildflags"))
							for (const auto &v: *vals)
								if (v.string())
									args.emplace_back(*v.string()); /// @todo: Escape?
						
						if (const auto vals = dataTarget.data.object()->getArray("defines"))
							for (const auto &v: *vals)
								if (v.string())
									args.emplace_back("-D"s + *v.string()); /// @todo: Escape?
					}
					
					args.emplace_back("-std=c++14"); /// @todo: Since Mac target etc don't provide this, but fix this later!

					args.emplace_back(file);
					
					args.emplace_back("-o");
					p.append("derived.ast");
					p.make_preferred();
					args.emplace_back(p.string());

					bp::child c{clang, args, bp::std_out > bp::null, bp::std_err > bp::null};
					c.wait();
					if (0 != c.exit_code())
					{
						karhu::log::msg() << "AST generation failed";
						return {};
					}

					return {true, p.string()};
				}
			}

			/**
			 * Searches through the C++ source files (must be full paths)
			 * specified for classes that directly or indirectly inherit
			 * publically from the (fully specified with all namespaces)
			 * specified class.
			 *
			 * @param dataTarget  The data for the target.
			 * @param config      The configuration for the target.
			 * @param parent      The fully specified parent class to find children of.
			 * @param files       The files to look through; if empty, the includes from the target will be used instead.
			 *
			 * @return A list of fully specified class names of derived classes found.
			 */
			Analyser::DataDerived Analyser::findDerivedClassesInFiles
			(
				const proj::file::build::Config::DataTarget &dataTarget,
				const proj::file::build::Config &config,
				const std::string &parent,
				const std::vector<std::string> &files
			)
			{
				// Will hold all the data to be passed to the libclang callbacks.
				struct Data
				{
					const std::string &parent;
					std::string headername, sourcename, classname;
					DataDerived found;
					CallbackVisitor visitor, basevisitor;
				} data
				{
					parent
				};

				const auto project(config.module().project());
				if (!project) return {};
				
				std::vector<std::string> filesFromTarget;
				if (files.size() == 0)
				{
					const auto f(config.findTargetFiles(dataTarget));
					if (f) filesFromTarget = std::move(*f);
				}
				
				const std::vector<std::string> *filesToUse;
				if (filesFromTarget.size() > 0)
					filesToUse = &filesFromTarget;
				else if (files.size() > 0)
					filesToUse = &files;
				else
					return {};

				for (const auto &file : *filesToUse)
				{
					const auto AST(outputAST(dataTarget, project->location().path(), file));
					if (!AST) return {};
					
					data.sourcename = file;

					// Goes through baseclasses to find the parent.
					data.basevisitor = static_cast<CallbackVisitor>([](CXCursor cursor, CXCursor, CXClientData clientData)
					{
						auto &data(*reinterpret_cast<Data *>(clientData));

						// Is this a parent class?
						if (CXCursor_CXXBaseSpecifier == clang_getCursorKind(cursor))
						{
							// Is the inheritance public?
							if (CX_CXXPublic == clang_getCXXAccessSpecifier(cursor))
							{
								// Is this the parent we are looking for?
								if (getTypeSpelling(clang_getCursorType(cursor)) == data.parent)
								{
									// Done!
									if (data.classname.length() > 0)
									{
										auto it(std::find(data.found.classes.begin(), data.found.classes.end(), data.classname));
										if (it == data.found.classes.end())
											data.found.classes.emplace_back(data.classname);
									}
									
									if (data.headername.length() > 0)
									{
										auto it(std::find(data.found.headers.begin(), data.found.headers.end(), data.headername));
										if (it == data.found.headers.end())
											data.found.headers.emplace_back(data.headername);
									}

									if (data.sourcename.length() > 0)
									{
										auto it(std::find(data.found.sources.begin(), data.found.sources.end(), data.sourcename));
										if (it == data.found.sources.end())
											data.found.sources.emplace_back(data.sourcename);
									}

									return CXChildVisit_Break;
								}
								// Otherwise, keep checking the rest of the parents.
								else
									clang_visitChildren(clang_getCursorDefinition(cursor), data.basevisitor, &data);
							}
						}

						return CXChildVisit_Continue;
					});

					// Goes through all nodes to find classes to investigate the parents of.
					data.visitor = [](CXCursor cursor, CXCursor, CXClientData clientData)
					{
						auto &data(*reinterpret_cast<Data *>(clientData));

						// Is this a class declaration?
						if (CXCursor_ClassDecl == clang_getCursorKind(cursor))
						{
							/// @todo: Better determine if something is actually a header?
							const auto filename(getCursorFilename(cursor));
							if (filename != data.sourcename)
								data.headername = std::move(filename);

							// Explore the parents to find a match!
							data.classname = getTypeSpelling(clang_getCursorType(cursor));
							clang_visitChildren(cursor, data.basevisitor, &data);
						}

						// Keep visiting remaining nodes.
						clang_visitChildren(cursor, data.visitor, &data);

						return CXChildVisit_Continue;
					};

					// Create the translation unit from the AST.
					CXIndex index{clang_createIndex(0, 1)};
					CXTranslationUnit tu{clang_createTranslationUnit(index, AST->c_str())};
					
					// Go through the nodes looking for classes declared as
					// children, direct or indirect, of the parent!
					CXCursor rootCursor{clang_getTranslationUnitCursor(tu)};
					clang_visitChildren(rootCursor, data.visitor, &data);

					// Clean up the allocations.
					clang_disposeTranslationUnit(tu);
					clang_disposeIndex(index);
				}
				
				return data.found;
			}
		}
	}
}
