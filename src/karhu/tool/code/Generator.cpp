/**
 * @author		Ava Skoog
 * @date		2017-09-19
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/code/Generator.hpp>
#include <karhu/core/log.hpp>

#include <fstream>
#include <chrono>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

namespace karhu
{
	namespace tool
	{
		namespace code
		{
			Nullable<std::string> Generator::outputFileToLoadModular
			(
				const Analyser::DataDerived &filesAndClasses,
				const proj::file::Project &project,
				const std::string &target
			)
			{
				namespace fs = boost::filesystem;

				fs::path p{project.location().path()};

				if (!fs::exists(p) && !fs::is_directory(p)) return {};

				p.append("generated");
				if (!fs::exists(p) && !fs::is_directory(p))
				{
					boost::system::error_code err;
					fs::create_directory(p, err);
					if (boost::system::errc::success != err) return {};
				}

				p.append(target);
				if (!fs::exists(p) && !fs::is_directory(p))
				{
					boost::system::error_code err;
					fs::create_directory(p, err);
					if (boost::system::errc::success != err) return {};
				}
				
				p.append("main.cpp");
				p.make_preferred();
				
				std::ofstream o{p.string(), std::ios::trunc};
				if (!o.is_open()) return {};
							
				o << "#include <karhu/app/backend/module/Modular.hpp>\n";
				o << "#include <karhu/app/backend/module/Pool.hpp>\n";
				o << "#include <karhu/app/App.hpp>\n";
				o << "#include <karhu/core/log.hpp>\n";
				
				o << '\n';

				for (const auto &f : filesAndClasses.headers)
					o << "#include \"" << f << "\"\n";
				
				o << '\n';
				
				o << "extern \"C\" void karhu_module_update_pool(karhu::app::backend::module::Pool &pool)\n";
				o << "{\n";
				
				for (const auto &c : filesAndClasses.classes)
					o << "\tpool.registerOrUpdateDerivedType<" << c << ">(\"" << c << "\");\n";

				o << "}\n";
				
				o << '\n';
				
				o << "extern \"C\" void karhu_set_logger_implementation(karhu::log::Implementation *const i)\n";
				o << "{\n";
				o << "\tkarhu::log::Logger::implementation(i);\n";
				o << "}\n";
				
				o << '\n';
				
				o << "extern \"C\" void karhu_set_app_instance(karhu::app::App *const app)\n";
				o << "{\n";
				o << "\tkarhu::app::App::instance(app);\n";
				o << "}\n";

				o << '\n';
				
				return {true, p.string()};
			}
		}
	}
}
