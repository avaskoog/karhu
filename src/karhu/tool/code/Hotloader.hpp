/**
 * @author		Ava Skoog
 * @date		2017-09-21
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_CODE_HOTLOADER_H_
	#define KARHU_TOOL_CODE_HOTLOADER_H_

	#include <karhu/tool/proj/file/Project.hpp>

	#include <string>

	namespace karhu
	{
		namespace tool
		{
			namespace code
			{
				class Hotloader
				{
					public:
						bool refreshModules
						(
							proj::file::Project &project,
							const proj::file::build::config::Type &conf,
							const bool rebuild
						);
					
					private:
						bool refreshModule
						(
							proj::file::Project &project,
							const std::string &target,
							const proj::file::build::config::Type &conf,
							const bool rebuild
						);
				};
			}
		}
	}
#endif
