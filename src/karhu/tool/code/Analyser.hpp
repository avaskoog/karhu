/**
 * @author		Ava Skoog
 * @date		2017-09-19
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_CODE_ANALYSER_H_
	#define KARHU_TOOL_CODE_ANALYSER_H_

	#include <karhu/tool/proj/file/build/Config.hpp>

	#include <vector>
	#include <string>

	namespace karhu
	{
		namespace tool
		{
			namespace code
			{
				class Analyser
				{
					public:
						struct DataDerived
						{
							std::vector<std::string> classes, headers, sources;
						};

					public:
						DataDerived findDerivedClassesInFiles
						(
							const proj::file::build::Config::DataTarget &dataTarget,
							const proj::file::build::Config &config,
							const std::string &parent,
							const std::vector<std::string> &files
						);
				};
			}
		}
	}
#endif
