/**
 * @author		Ava Skoog
 * @date		2017-09-19
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_CODE_GENERATOR_H_
	#define KARHU_TOOL_CODE_GENERATOR_H_

	#include <karhu/tool/proj/file/Project.hpp>
	#include <karhu/tool/code/Analyser.hpp>

	#include <string>
	#include <vector>
	#include <map>

	namespace karhu
	{
		namespace tool
		{
			namespace code
			{
				class Generator
				{
					public:
						Nullable<std::string> outputFileToLoadModular
						(
							const Analyser::DataDerived &filesAndClasses,
							const proj::file::Project &project,
							const std::string &target
						);
				};
			}
		}
	}
#endif
