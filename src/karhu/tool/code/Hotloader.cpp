/**
 * @author		Ava Skoog
 * @date		2017-09-21
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/code/Hotloader.hpp>
#include <karhu/tool/proj/file/build/Module.hpp>
#include <karhu/tool/proj/file/build/Config.hpp>
#include <karhu/tool/proj/file/build/Build.hpp>
#include <karhu/tool/proj/file/build/Target.hpp>
#include <karhu/tool/proj/file/build/common.hpp>
#include <karhu/tool/code/Analyser.hpp>
#include <karhu/tool/code/Generator.hpp>
#include <karhu/conv/types.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>
#include <boost/process.hpp>

#include <algorithm>

namespace karhu
{
	namespace tool
	{
		namespace code
		{
			bool Hotloader::refreshModules
			(
				proj::file::Project &project,
				const proj::file::build::config::Type &conf,
				const bool rebuild
			)
			{
				// Load the build settings for the project.
				auto build(project.build());
				if (!build)
				{
					log::err() << "Failed to load project build settings";
					return false;
				}
				
				// Find the requested modules for the main target.
				/// @todo: This should be for the current target, really; not the main one.
				std::vector<std::string> modulesToBuild;
				{
					const auto t = std::find_if
					(
						build->targets().begin(),
						build->targets().end(),
						[](const auto &v) { return (v.first == "main"); }
					);
					if (t != build->targets().end())
						modulesToBuild.emplace_back(t->first);
				}

				if (modulesToBuild.size() == 0) return true;
				
				for (const auto &target : build->targets())
				{
					if (const auto object = target.second.data().object())
					{
						const auto type(object->getString("type"));
						if (!type)
						{
							log::err() << "Required option 'type' missing from target '" << target.first << "'";
							return false;
						}
						
						if (*type == "module")
						{
							const auto it(std::find(modulesToBuild.begin(), modulesToBuild.end(), target.first));
							if (it == modulesToBuild.end()) continue;
						
							if (!refreshModule(project, target.first, conf, rebuild)) return false;
						}
					}
				}

				return true;
			}

			bool Hotloader::refreshModule
			(
				proj::file::Project &project,
				const std::string &target,
				const proj::file::build::config::Type &configuration,
				const bool rebuild
			)
			{
				namespace bp    = boost::process;
				namespace tools = SDK::paths::home::tools;
				
				log::msg() << "Refreshing module '" << target << "'...";
				
				/// @todo: Break on roadblock? Output warnings?

				// Load the build settings for the project.
				auto build(project.build());
				if (!build) return false;

				// Load the build module for our platform.
				/// @todo: Should this be for the client's platform instead?

				auto module(proj::file::build::Module::loadForPlatform(Platform::current));
				if (!module) return false;

				// Generate the configurations.
				auto conf(module->getConfig(project, configuration, "main"));
				if (!conf) return false;

				// Find the generated module target.
				auto targetGenerated(std::find_if
				(
					conf->targets().begin(),
					conf->targets().end(),
					[&target](const auto &v) { return (v.name == target); }
				));
				if (targetGenerated == conf->targets().end()) return false;
				
				// We need to find the target again but in the actual
				// build file, and not in the generated targets, so
				// that we can update its includes and resave the file.
				const auto targetFile(std::find_if
				(
					build->targets().begin(),
					build->targets().end(),
					[&target](const auto &v) { return (v.first == target); }
				));
				if (targetFile == build->targets().end()) return false;
				
				// Set the includes to all source files in the root.
				if (auto object = targetGenerated->data.object())
				{
					auto includes(object->getArray("includes"));
					if (!includes)
						includes = object->push({"includes", conv::JSON::Arr{}}).array();
					else
						includes->clear();
					
					includes->push("**.cpp");
				}

				// Figure out what files go into the module.
				code::Analyser analyser;
				const auto filesWithClasses(analyser.findDerivedClassesInFiles
				(
					*targetGenerated,
					*conf,
					"karhu::app::backend::module::Modular", /// @todo: Should we hardcode this?
					{}
				));

				// Generate the entry point to the module library.
				code::Generator generator;
				auto pathEntry(generator.outputFileToLoadModular
				(
					filesWithClasses,
					project,
					target
				));
				if (!pathEntry) return false;
				
				// Will be set to true if any new files have been added in
				// which case the project will need to be made again.
				bool make{false};

				// Update the build file with the correct includes.
				if (auto data = targetFile->second.data().object())
				{
					// Find or create the includes array.
					auto includes(data->getArray("includes"));
					if (!includes) includes = data->push({"includes", conv::JSON::Arr{}}).array();

					// Go through the found files and add them.
					for (const auto &file : filesWithClasses.sources)
					{
						// Check if the file already exists.
						const auto it(std::find_if
						(
							includes->begin(),
							includes->end(),
							[&file](const auto &v) { return (v.string() && *v.string() == file); }
						));

						// If it does not, add it.
						if (it == includes->end())
							includes->push(file);
						// If it does, set the make flag.
						else
							make = true;
					}
					
					// Finally, add the generated entry point file
					// if it does not already exist.
					const auto it(std::find_if
					(
						includes->begin(),
						includes->end(),
						[&pathEntry](const auto &v) { return (v.string() && *v.string() == *pathEntry); }
					));

					if (it == includes->end())
						includes->push(*pathEntry);
				}
				
				// Save the updated file!
				if (!build->save())
				{
					log::err() << "Failed to save build file";
					return false;
				}

				if (rebuild)
				{
					log::msg() << "Rebuilding module '" << target << "'...";

					// Use the build command to rebuild the module target.
					// Use apply rather than build directly so that any settings
					// for remote build that may be present are used.
					auto p(SDK::env::home(tools::karhubuild::local()));
					if (p && boost::filesystem::exists(*p))
					{
						// First, make if necessary.
						// We make the main target rather than the module target,
						// so that the main project does not break.
						if (make)
						{
							std::vector<std::string> args;
							args.emplace_back("apply");

							args.emplace_back("--acts");
							args.emplace_back("make");

							args.emplace_back("--dir");
							args.emplace_back(project.location().path());

							// Get rid of output to keep the server console clean and speed things up.
							bp::child c{*p, args, bp::std_out > bp::null, bp::std_err > bp::null};
							c.wait();
							if (0 != c.exit_code())
							{
								log::err() << "Module rebuild failed: make failed";
								return false;
							}
						}

						// We only build the module target, however.
						std::vector<std::string> args;
						args.emplace_back("apply");

						args.emplace_back("--acts");
						if (make) args.emplace_back("make");
						args.emplace_back("build");

						args.emplace_back("--target");
						args.emplace_back(target);
						
						args.emplace_back("--dir");
						args.emplace_back(project.location().path());

						// Get rid of output to keep the server console clean and speed things up.
						bp::child c{*p, args, bp::std_out > bp::null, bp::std_err > bp::null};
						c.wait();
						if (0 != c.exit_code())
						{
							log::err() << "Module rebuild failed: build failed";
							return false;
						}
					}
				}
				
				return true;
			}
		}
	}
}
