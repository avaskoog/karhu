/**
 * @author		Ava Skoog
 * @date		2018-11-03
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/shared/AdapterResourcebank.hpp>

namespace karhu
{
	namespace tool
	{
		using Read  = proj::file::Project::AdapterRead;
		using Write = proj::file::Project::AdapterWrite;
		
		AdapterResourcebank::AdapterResourcebank(proj::file::Project &project)
		:
		res::Bank
		{
			std::make_unique<Read> (project.adapterRead ()),
			std::make_unique<Write>(project.adapterWrite())
		}
		{
		}
	}
}
