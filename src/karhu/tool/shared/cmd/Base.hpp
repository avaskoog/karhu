/**
 * @author		Ava Skoog
 * @date		2017-07-02
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_SHARED_CMD_BASE_H_
	#define KARHU_TOOL_SHARED_CMD_BASE_H_

	#include <karhu/core/Nullable.hpp>
	#include <karhu/tool/proj/Identifier.hpp>
	#include <karhu/tool/proj/Location.hpp>
	#include <karhu/tool/proj/file/Project.hpp>
	#include <string>
	#include <memory>

	namespace karhu
	{
		namespace tool
		{
			namespace shared
			{
				namespace cmd
				{
					class Base
					{
						protected:
							bool loadProject(const std::string &directory);
							bool loadInfo();
							bool loadBuild();
							
							bool validPreact(const std::string &act) const;
							bool validPreacts(const std::vector<std::string> &acts) const;
							bool validConfiguration(const std::string &value) const;

							proj::file::Project &project() const { return **m_project; }
							const proj::Location &location() const { return *m_location; }
						
						private:
							std::unique_ptr<Nullable<proj::file::Project>> m_project;
							std::unique_ptr<proj::Location> m_location;
					};
				}
			}
		}
	}
#endif
