/**
 * @author		Ava Skoog
 * @date		2017-04-17
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/shared/cmd/Base.hpp>
#include <karhu/core/platform.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/process.hpp>

/// @todo: Fix logging.
#include <iostream>

namespace karhu
{
	namespace tool
	{
		namespace shared
		{
			namespace cmd
			{
				bool Base::loadProject(const std::string &directory)
				{
					// Validate the intended location.
					m_location = std::make_unique<proj::Location>(directory);
					if (!m_location->valid())
					{
						log::err() << "Invalid location '" << m_location->path() << "'";
						return false;
					}
					
					// Try to create the actual project.
					m_project = std::make_unique<Nullable<proj::file::Project>>(proj::file::Project::load(*m_location));
					if (!*m_project)
					{
						log::err() << "Failed to load project.";
						return false;
					}
					
					return true;
				}

				bool Base::loadInfo()
				{
					if (!m_project || !*m_project) return false;

					// Load the info file.
					auto info((*m_project)->info());
					if (!info)
					{
						log::err() << "Failed to load project information file.";
						return false;
					}
					
					return true;
				}

				bool Base::loadBuild()
				{
					if (!m_project || !*m_project) return false;

					// Load the info file.
					auto info((*m_project)->build());
					if (!info)
					{
						log::err() << "Failed to load project build file.";
						return false;
					}
					
					return true;
				}
				
				bool Base::validPreact(const std::string &act) const
				{
					static const std::vector<std::string> acts
					{
						"push",
						"pull",
						"update"
					};

					const auto i(std::find(acts.begin(), acts.end(), act));
					if (i == acts.end())
					{
						std::stringstream msg;
						msg << "Invalid pre-build action '" << act << "'. Valid actions are ";
						for (std::size_t i{0}; i < acts.size(); ++ i)
						{
							if (acts.size() == i + 1) msg << "and ";
							log::err() << '\'' << acts[i] << '\'';
							if (acts.size() > i + 1) msg << ", ";
						}
						msg << '.';
						
						log::err() << msg.str();

						return false;
					}
					
					return true;
				}
				
				bool Base::validPreacts(const std::vector<std::string> &acts) const
				{
					for (auto &i : acts)
						if (!validPreact(i))
							return false;
					
					return true;
				}
				
				bool Base::validConfiguration(const std::string &value) const
				{
					static std::array<std::string, 2> configurations
					{
						proj::file::build::config::toString(proj::file::build::config::Type::debug),
						proj::file::build::config::toString(proj::file::build::config::Type::release)
					};

					const auto i(std::find(configurations.begin(), configurations.end(), value));
					if (i == configurations.end())
					{
						std::stringstream msg;
						msg << "Invalid configuration '" << value << "'. Valid configurations are ";
						for (std::size_t i{0}; i < configurations.size(); ++ i)
						{
							if (configurations.size() == i + 1) msg << "and ";
							log::err() << '\'' << configurations[i] << '\'';
							if (configurations.size() > i + 1) msg << ", ";
						}
						msg << '.';
						
						log::err() << msg.str();
						
						return false;
					}
					
					return true;
				}
			}
		}
	}
}
