/**
 * @author		Ava Skoog
 * @date		2018-11-03
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_ADAPTER_RESOURCEBANK_H_
	#define KARHU_TOOL_ADAPTER_RESOURCEBANK_H_

	#include <karhu/res/Bank.hpp>
	#include <karhu/tool/proj/file/Project.hpp>

	namespace karhu
	{
		namespace tool
		{
			class AdapterResourcebank : public res::Bank
			{
				public:
					AdapterResourcebank(proj::file::Project &);
			};
		}
	}
#endif
