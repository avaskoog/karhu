/**
 * @author		Ava Skoog
 * @date		2017-04-04
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/Identifier.hpp>
#include <regex>
#include <sstream>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			Nullable<Identifier> Identifier::create
			(
				const std::string &extension,
				const std::string &organisation,
				const std::string &project
			)
			{
				if (extension.length() == 0 || organisation.length() == 0 || project.length() == 0)
					return {};

				return {true, extension, organisation, project};
			}
			
			Nullable<Identifier> Identifier::create
			(
				const std::string &fullStringSeparatedbyDots
			)
			{
				if (!std::regex_match(fullStringSeparatedbyDots, std::regex{"[a-z0-9]+\\.[a-z0-9]+\\.[a-z0-9]+"}))
					return {};
				
				std::string ext, org, proj;
				
				std::stringstream token;
				for (std::size_t i{0}, index{0}; i < fullStringSeparatedbyDots.length(); ++ i)
				{
					auto &c(fullStringSeparatedbyDots[i]);
					if (c == '.')
					{
						switch (index)
						{
							case 0:
								ext = token.str();
								break;
							
							case 1:
								org = token.str();
								break;
						}

						++ index;
						token.str("");
					}
					else
						token << c;
				}
				
				proj = token.str();
				
				return {true, std::move(ext), std::move(org), std::move(proj)};
			}
			
			std::string Identifier::full() const
			{
				return m_ext + '.' + m_org + '.' + m_proj;
			}
		}
	}
}
