/**
 * @author		Ava Skoog
 * @date		2017-04-16
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/Tool.hpp>
#include <karhu/tool/proj/cmd/New.hpp>
#include <karhu/tool/proj/cmd/Karhu.hpp>
#include <karhu/tool/proj/cmd/Sync.hpp>
#include <karhu/tool/proj/cmd/Asset.hpp>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			Tool::Tool()
			{
				registerCommand<cmd::New>  ("new");
				registerCommand<cmd::Karhu>("karhu");
				registerCommand<cmd::Sync> ("sync");
				registerCommand<cmd::Asset>("asset");
			}
		}
	}
}
