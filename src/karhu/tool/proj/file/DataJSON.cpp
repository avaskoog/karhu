/**
 * @author		Ava Skoog
 * @date		2017-08-19
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/file/DataJSON.hpp>
#include <karhu/tool/proj/file/Project.hpp>
#include <karhu/core/log.hpp>

#include <fstream>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace file
			{
				DataJSON::DataJSON(const Location &location, const std::string &file)
				:
				Data{location, file}
				{
				}
				
				void DataJSON::performSet(const std::string &key, const std::string &value)
				{
					m_root.object()->set(key, value);
				}
				
				Nullable<std::string> DataJSON::performGet(const std::string &key) const
				{
					if (const auto v = m_root.object()->get(key))
						return {true, v->stringify()};
					
					return {};
				}
				
				void DataJSON::performSet(const std::vector<std::string> &key, const std::string &value)
				{
					if (key.empty())
						return;
						
					auto v(&m_root);
					std::size_t i{0};
					
					for (; i < key.size() - 1; ++ i)
					{
						if (v->object())
						{
							auto next(v->object()->get(key[i]));

							if (!next)
								next = &v->object()->push({key[i], conv::JSON::Obj{}});

							v = next;
						}
					}

					v->object()->set(key[i], value);
				}
				
				Nullable<std::string> DataJSON::performGet(const std::vector<std::string> &key) const
				{
					if (key.empty())
						return {};
					
					auto v(&m_root);
					std::size_t i{0};
					
					for (; i < key.size() - 1; ++ i)
						if (v->object())
							if (!(v = v->object()->get(key[i])))
								return {};

					if (v && v->object() && (v = v->object()->get(key[i])))
						return {true, v->stringify()};
					
					return {};
				}

				bool DataJSON::performLoad(std::ifstream &f)
				{
					conv::JSON::Parser parser;
					if (auto o = parser.parse(f))
					{
						m_root = std::move(*o);
						return true;
					}

					log::err() << "Error decoding JSON file '" << m_file << "': " << parser.error();
					return false;
				}

				bool DataJSON::performSave(std::ofstream &f)
				{
					// Use dump() instead of serialise() to get nice formatting;
					// these files are meant to be hand-edited, at least for now.
					f << m_root.dump();
					return true;
				}
			}
		}
	}
}
