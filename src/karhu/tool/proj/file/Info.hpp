/**
 * @author		Ava Skoog
 * @date		2017-04-04
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_FILE_INFO_H_
	#define KARHU_TOOL_PROJ_FILE_INFO_H_

	#include <karhu/tool/proj/file/DataJSON.hpp>
	#include <karhu/tool/proj/Location.hpp>
	#include <karhu/tool/proj/Identifier.hpp>
	#include <karhu/core/Nullable.hpp>
	#include <karhu/data/JSON.hpp>

	/// @todo: Replace with Karhu XML interface eventually.
	#include <karhu/lib/tinyxml2/tinyxml2.h>

	#include <string>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					class Project;

					/**
					 * Interfaces the project information file.
					 * Can only be constructed by a parent project object.
					 *
					 * A newly constructed information object that has not
					 * loaded information from file is still valid.
					 *
					 * @see Project
					 */
					class Info : public DataJSON
					{
						public:
							Info(const Location &location);
						
							void templatename(const std::string &value);
							Nullable<std::string> templatename() const;

							void name(const std::string &value);
							Nullable<std::string> name() const;

							void identifier(const Identifier &value);
							Nullable<Identifier> identifier() const;

						private:
							void setUpDefaults() override;
					};
				}
			}
		}
	}
#endif
