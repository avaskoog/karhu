/**
 * @author		Ava Skoog
 * @date		2017-08-19
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/file/Data.hpp>
#include <karhu/tool/proj/file/Project.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace file
			{
				/**
				 * Constructs the data with a location to load the file from
				 * or save it too, and the filename to use for it.
				 *
				 * @param location The location of the file.
				 * @param file     The name of the file.
				 */
				Data::Data(const Location &location, const std::string &file)
				:
				m_location{location},
				m_file    {file}
				{
				}

				/**
				 * Tries to load the data from file using the location and
				 * name that were specified at creation.
				 *
				 * @return Whether the file was successfully loaded.
				 */
				bool Data::load()
				{
					if (!m_location.valid() || 0 == m_file.length())
					{
						log::err() << "Valid location or filename has not been set for data";
						return false;
					}

					boost::filesystem::path p{m_location.path()};
					p.append(m_file);
					p.make_preferred();

					std::ifstream f{p.string()};
					
					if (!f.is_open())
						log::err() << "Failed to open file '" << p.string() << "' for reading.";
					else
					{
						if (performLoad(f))
						{
							setUpDefaults();
							if (!afterLoad()) return false;
							return true;
						}
					}

					return false;
				}

				/**
				 * Tries to save the data to file using the location and
				 * name that were specified at creation.
				 *
				 * @return Whether the file was successfully saved.
				 */
				bool Data::save()
				{
					if (!m_location.valid() || 0 == m_file.length())
					{
						log::err() << "Valid location or filename has not been set for data";
						return false;
					}

					boost::filesystem::path p{m_location.path()};
					p.append(m_file);
					p.make_preferred();

					std::ofstream f{p.string(), std::ios_base::trunc};
					
					if (!f.is_open())
					{
						log::err() << "Failed to open file '" << p.string() << "' for writing.";
						return false;
					}
					
					if (!beforeSave()) return false;
					return performSave(f);
				}
			}
		}
	}
}
