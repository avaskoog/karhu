/**
 * @author		Ava Skoog
 * @date		2017-04-09
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_FILE_PROJECT_H_
	#define KARHU_TOOL_PROJ_FILE_PROJECT_H_

	#include <karhu/tool/proj/Identifier.hpp>
	#include <karhu/tool/proj/Location.hpp>
	#include <karhu/conv/files.hpp>
	#include <karhu/core/Nullable.hpp>

	#include <karhu/tool/proj/file/Info.hpp>
	#include <karhu/tool/proj/file/build/Build.hpp>

	#include <memory>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					class Project
					{
						public:
							class AdapterRead : public conv::AdapterFilesystemRead
							{
								private:
									AdapterRead(Project &project) : m_project{project} {}
								
								public:
									AdapterRead() = delete;
								
									AdapterRead(AdapterRead &&) = default;
									AdapterRead(const AdapterRead &) = default;
									AdapterRead &operator=(AdapterRead &&) = default;
									AdapterRead &operator=(const AdapterRead &) = default;
								
									Result<std::string> readStringFromResource
									(
										const char *filepathRelativeToResourcepath
									) override;
								
									Result<Buffer<Byte>> readBytesFromResource
									(
										const char *filepathRelativeToResourcepath
									) override;
								
								private:
									Project &m_project;
								
								friend class Project;
							};
						
							class AdapterWrite : public conv::AdapterFilesystemWrite
							{
								private:
									AdapterWrite(Project &project) : m_project{project} {}
								
								public:
									AdapterWrite() = delete;
								
									AdapterWrite(AdapterWrite &&) = default;
									AdapterWrite(const AdapterWrite &) = default;
									AdapterWrite &operator=(AdapterWrite &&) = default;
									AdapterWrite &operator=(const AdapterWrite &) = default;
								
									Status writeStringToResource
									(
										char                      const *filepathRelativeToResourcepath,
										char                      const *data,
										conv::ModeFilesystemWrite const  mode = conv::ModeFilesystemWrite::truncate
									) override;

									Status writeBytesToResource
									(
										char                      const *filepathRelativeToResourcepath,
										Buffer<Byte>              const &data,
										conv::ModeFilesystemWrite const  mode = conv::ModeFilesystemWrite::truncate
									) override;
								
								private:
									Project &m_project;
								
								friend class Project;
							};
						
						public:
							static Nullable<Project> load(const Location &path = {});
							static Nullable<Project> create(const Identifier &identifier, const std::string &folder = {}, const Location &path = {});
						
						public:
							Project(const Project &) = default;
							Project(Project &&) = default;
							Project &operator=(const Project &) = default;
							Project &operator=(Project &&) = default;
						
							const Location &location() const noexcept { return m_location; }
						
							AdapterRead  adapterRead()  { return {*this}; }
							AdapterWrite adapterWrite() { return {*this}; }

							Info *info();
							build::Build *build();

						private:
							Project()
							:
							m_identifier{Identifier::defaults()}
							{
							}

							Project(const Identifier &identifier, const Location &path)
							:
							m_identifier{identifier},
							m_location  {path}
							{
							}

							bool create(const std::string &folder);

							bool validateBasedir() const;
							bool validateSubdir() const;
							std::string subdir() const;
							Location locationSubdir() const;
						
						private:
							Identifier m_identifier;
							Location   m_location;

							std::unique_ptr<Info>         m_info;
							std::unique_ptr<build::Build> m_build;
						
						friend class Nullable<Project>;
					};
				}
			}
		}
	}
#endif
