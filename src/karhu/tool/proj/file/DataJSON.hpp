/**
 * @author		Ava Skoog
 * @date		2017-08-19
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_FILE_DATAJSON_H_
	#define KARHU_TOOL_PROJ_FILE_DATAJSON_H_

	#include <karhu/tool/proj/file/Data.hpp>
	#include <karhu/conv/types.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					class Project;
					
					/**
					 * Interfaces a data file in the project structure that uses
					 * JSON as its format, and offers a way for derived classes to
					 * interface particular files while implementing a default way
					 * of filling them in upon creation. This superclass aids in the
					 * creation, retrieval and modification of values in the file.
					 */
					class DataJSON : public Data
					{
						public:
							DataJSON(const Location &location, const std::string &file);
						
							/**
							 * Returns the JSON root so that JSON values can be manipulated
							 * directly in case there is a need for more complex values than
							 * those provided by the interface inherited from Data (i.e. strings).
							 *
							 * @return The JSON root.
							 */
							conv::JSON::Val &rootJSON() noexcept { return m_root; }

							/**
							 * Returns the JSON root so that JSON values can be manipulated
							 * directly in case there is a need for more complex values than
							 * those provided by the interface inherited from Data (i.e. strings).
							 *
							 * @return The JSON root.
							 */
							const conv::JSON::Val &rootJSON() const noexcept { return m_root; }

						protected:
							/// @todo: Mirror the nested getters/setters in the JSON header itself? Seems practical…
							void performSet(const std::string &key, const std::string &value) override;
							void performSet(const std::vector<std::string> &key, const std::string &value) override;
							Nullable<std::string> performGet(const std::string &key) const override;
							Nullable<std::string> performGet(const std::vector<std::string> &key) const override;
						
							bool performLoad(std::ifstream &f) override;
							bool performSave(std::ofstream &f) override;

						private:
							conv::JSON::Val m_root{conv::JSON::Obj{}};
					};
				}
			}
		}
	}
#endif
