/**
 * @author		Ava Skoog
 * @date		2017-08-31
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_FILE_BUILD_TARGET_H_
	#define KARHU_TOOL_PROJ_FILE_BUILD_TARGET_H_

	#include <karhu/tool/proj/file/DataJSON.hpp>
	#include <karhu/tool/proj/file/build/common.hpp>
	#include <karhu/conv/types.hpp>
	#include <karhu/core/platform.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					namespace build
					{
						/// @todo: Deal with configuration-specific settings on target level?
						class Target
						{
							private:
								struct Option
								{
									JSON::TypeValue type{JSON::TypeValue::string};
									std::vector<std::string> valid;
								};

							public:
								Target(const conv::JSON::Val &data) : m_data{data} {}
								Target(conv::JSON::Val &&data) : m_data{std::move(data)} {}

								conv::JSON::Val &data() noexcept { return m_data; }
								const conv::JSON::Val &data() const noexcept { return m_data; }
								Nullable<conv::JSON::Val> data(const Platform &platform, const config::Type conf) const;
							
								bool valid() const;

							private:
								conv::JSON::Val m_data{conv::JSON::Obj{}};
						};
					}
				}
			}
		}
	}
#endif
