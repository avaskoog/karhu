/**
 * @author		Ava Skoog
 * @date		2017-07-01
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_FILE_BUILD_BUILD_H_
	#define KARHU_TOOL_PROJ_FILE_BUILD_BUILD_H_

	#include <karhu/tool/proj/file/DataJSON.hpp>
	#include <karhu/tool/proj/file/build/Target.hpp>
	#include <karhu/tool/proj/Location.hpp>
	#include <karhu/tool/proj/Identifier.hpp>
	#include <karhu/core/Nullable.hpp>

	#include <cstdint>
	#include <string>
	#include <vector>
	#include <map>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					class Project;

					namespace build
					{
						/**
						 * Interfaces the project build setting file.
						 * Can only be constructed by a parent project object.
						 *
						 * A newly constructed information object that has not
						 * loaded information from file is still valid.
						 *
						 * @see Project
						 */
						class Build : public DataJSON
						{
							public:
								struct SettingsSSH
								{
									std::string name, addr, dir;
								};

								struct SettingsSystem
								{
									std::string name, SSH, teamID;
								};

								/// @todo: Remove this?
								struct SettingsConfigTarget
								{
									std::string type, CPP;
									std::vector<std::string> includes, links, buildflags, linkflags, defines;
								};

								/// @todo: Remove this?
								struct SettingsTarget
								{
									std::string name;
									SettingsConfigTarget general, debug, release;
								};

							public:
								Build(const Location &location);

								void port(const std::uint16_t v);
								Nullable<std::uint16_t> port() const;

								void SSH(const SettingsSSH &vals);
								std::vector<SettingsSSH> SSHs() const;

								void system(const SettingsSystem &vals);
								std::vector<SettingsSystem> systems() const;

								void target(const std::string &name, const Target &);
								void target(const std::string &name, Target &&);
								std::vector<std::pair<std::string, Target>> &targets() noexcept { return m_targets; }
								const std::vector<std::pair<std::string, Target>> &targets() const noexcept { return m_targets; }
							
							protected:
								void setUpDefaults() override;
								bool afterLoad() override;
								bool beforeSave() override;

							private:
								std::vector<std::pair<std::string, Target>> m_targets;
						};
					}
				}
			}
		}
	}
#endif
