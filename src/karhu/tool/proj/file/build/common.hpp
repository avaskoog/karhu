/**
 * @author		Ava Skoog
 * @date		2017-08-25
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_FILE_BUILD_COMMON_H_
	#define KARHU_TOOL_PROJ_FILE_BUILD_COMMON_H_

	#include <karhu/core/meta.hpp>

	#include <cstdint>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					namespace build
					{
						namespace config
						{
							enum class Type : std::uint8_t
							{
								invalid,
								debug,
								release
							};
							
							static constexpr auto toString(const Type t) noexcept
							{
								switch (t)
								{
									case Type::invalid: return "";
									case Type::debug:   return "debug";
									case Type::release: return "release";
								}
							}
							
							static constexpr Type fromString(const char *s) noexcept
							{
								if (meta::string::equal(s, toString(Type::debug)))
									return Type::debug;
								else if (meta::string::equal(s, toString(Type::release)))
									return Type::release;
								
								return Type::invalid;
							}
						}

						namespace type
						{
							enum class Type : std::uint8_t
							{
								invalid,
								app,
								slib,
								dlib
							};
							
							static constexpr auto toString(const Type t) noexcept
							{
								switch (t)
								{
									case Type::invalid: return "";
									case Type::app:     return "app";
									case Type::slib:    return "slib";
									case Type::dlib:    return "dlib";
								}
							}
							
							static constexpr Type fromString(const char *s) noexcept
							{
								if (meta::string::equal(s, toString(Type::app)))
									return Type::app;
								else if (meta::string::equal(s, toString(Type::slib)))
									return Type::slib;
								else if (meta::string::equal(s, toString(Type::dlib)))
									return Type::dlib;

								return Type::invalid;
							}
						}
					}
				}
			}
		}
	}
#endif
