/**
 * @author		Ava Skoog
 * @date		2017-04-06
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_FILE_BUILD_PREMAKE_H_
	#define KARHU_TOOL_PROJ_FILE_BUILD_PREMAKE_H_

	#include <karhu/tool/proj/file/build/Config.hpp>
	#include <karhu/conv/types.hpp>

	#include <map>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					namespace build
					{
						class Premake : public Config
						{
							public:
								Premake
								(
									Module &module,
									const config::Type type,
									const std::string &target,
									const conv::JSON::Val &extrasettings = {},
									const std::string &hint = {}
								);

							protected:
								bool performOutput(const conv::JSON::Arr &settings) override;
								std::string relativePathToProduct(const std::string &name) const override;

							private:
								// Target section.
								bool outputModules(std::ostream &o);
								bool outputWorkspace(std::ostream &o);

								bool outputTarget(std::ostream &o, const DataTarget &target);
								bool outputTargetType(std::ostream &o, const DataTarget &target);
								bool outputTargetBinname(std::ostream &o, const DataTarget &target);
								bool outputTargetFiles(std::ostream &o, const DataTarget &target);
								bool outputTargetLinks(std::ostream &o, const DataTarget &target);
								bool outputTargetDefines(std::ostream &o, const DataTarget &target);

								bool outputTargetOptionalStrings
								(
									std::ostream &o,
									const DataTarget &target,
									const conv::JSON::Obj &data,
									const std::string &element,
									const std::string &name,
									const std::function<std::string(const std::string &)> &modify = [](const std::string &s) { return s; }
								);
								
								bool outputTargetOptionalKeysAndVals
								(
									std::ostream &o,
									const DataTarget &target,
									const conv::JSON::Obj &data,
									const std::string &element,
									const std::string &name
								);

								std::string premakeType(const std::string &);
								std::string pathAbs(const std::string &subfolder = {});
								std::string pathOutputAbs(const std::string &subfolder);
								std::string pathOutputRel(const std::string &top, const std::string &subfolder);
								std::string pathTargetRel(const std::string &top, const std::string &target);
								std::string pathTargetAbs(const std::string &target);
								std::string pathObjRel(const std::string &top, const std::string &target);
								std::string pathObjAbs(const std::string &target);

								void outputString
								(
									std::ostream &o,
									const std::size_t &level,
									const std::string &name,
									const std::string &s
								);

								void outputStrings
								(
									std::ostream &o,
									const std::size_t &level,
									const std::string &name,
									const std::vector<std::string> &s,
									const std::function<std::string(const std::string &)> &modify = [](const std::string &s) { return s; }
								);
							
								void outputKeysAndVals
								(
									std::ostream &o,
									const std::size_t &level,
									const std::string &name,
									const std::map<std::string, std::string> &s
								);
						};
					}
				}
			}
		}
	}
#endif
