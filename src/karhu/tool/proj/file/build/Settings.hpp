/**
 * @author		Ava Skoog
 * @date		2017-08-31
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_FILE_BUILD_SETTINGS_H_
	#define KARHU_TOOL_PROJ_FILE_BUILD_SETTINGS_H_

	#include <karhu/tool/proj/file/DataJSON.hpp>
	#include <karhu/tool/proj/file/build/common.hpp>
	#include <karhu/conv/types.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					namespace build
					{
						class Settings
						{
							public:
								Settings() = default;

								Settings
								(
									const config::Type conf,
									conv::JSON::Obj &&data
								)
								:
								m_conf{conf},
								m_data{std::move(data)}
								{
								}

								Settings
								(
									const config::Type conf,
									const conv::JSON::Obj &data
								)
								:
								m_conf{conf},
								m_data{data}
								{
								}

								Settings(const Settings &) = default;
								Settings(Settings &&) = default;

								const config::Type     conf() const noexcept { return m_conf; }
								const conv::JSON::Obj &data() const noexcept { return m_data; }
								      conv::JSON::Obj &data()       noexcept { return m_data; }

							private:
								const config::Type    m_conf{config::Type::debug};
								      conv::JSON::Obj m_data{conv::JSON::Obj{}};
						};
					}
				}
			}
		}
	}
#endif
