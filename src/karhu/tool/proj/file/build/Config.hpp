/**
 * @author		Ava Skoog
 * @date		2017-04-06
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_FILE_BUILD_CONFIG_H_
	#define KARHU_TOOL_PROJ_FILE_BUILD_CONFIG_H_

	#include <karhu/tool/proj/file/build/Build.hpp>
	#include <karhu/tool/proj/file/build/Settings.hpp>
	#include <karhu/tool/proj/file/build/common.hpp>
	#include <karhu/tool/proj/file/Project.hpp>
	#include <karhu/tool/proj/file/DataJSON.hpp>
	#include <karhu/tool/proj/Location.hpp>
	#include <karhu/conv/types.hpp>
	#include <karhu/core/platform.hpp>

	#include <functional>
	#include <cstdint>
	#include <string>
	#include <map>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					namespace build
					{
						class Module;

						class Config
						{
							public:
								struct DataTarget
								{
									std::string name;
									Settings settings;
									conv::JSON::Val data{conv::JSON::Obj{}};
									conv::JSON::Val dataFull{conv::JSON::Obj{}};
								};

							public:
								Config
								(
									Module &module,
									const config::Type type,
									const std::string &target,
									const conv::JSON::Val &extrasettings = {},
									const std::string &hint = {}
								);

								bool output(const conv::JSON::Arr &settings);
							
								bool loadProjectFundamentals();
							
								Nullable<DataTarget> getDataForTarget
								(
									const std::string &name,
									const Target &target
								) const;

								virtual bool performOutput(const conv::JSON::Arr &settings) { return false; }
								virtual std::string relativePathToProduct(const std::string &name) const { return {}; }

								      Module       &module()       noexcept { return m_module; }
								const Module       &module() const noexcept { return m_module; }
								const config::Type &config() const noexcept { return m_type; }
							
								      std::vector<DataTarget> &targets()       noexcept { return m_targets; }
								const std::vector<DataTarget> &targets() const noexcept { return m_targets; }

								const Identifier           &identifier()    const noexcept { return m_identifier; }
								const std::string          &name()          const noexcept { return m_name; }
								const conv::JSON::Val &extrasettings() const noexcept { return m_extrasettings; }
								const std::string          &hint()          const noexcept { return m_hint; }
								const Build                *build()         const noexcept { return m_build; }

								bool ignores(const DataTarget &, const std::string &key) const;

								std::string nameTarget(const std::string &) const;
								std::string pathAbsFromRel(const std::string &root, const std::string &path) const;
								Nullable<std::string> rootTarget(const DataTarget &) const;
								Nullable<std::string> typeTarget(const DataTarget &) const;

								bool findAndOrderDependentTargets();
								bool outputDataTarget(const DataTarget &target);
								Nullable<std::vector<std::string>> findTargetFiles(const DataTarget &target) const;
							
								virtual ~Config() = default;
							
							protected:
								void outputIndent(std::ostream &o, const std::size_t &level);

							private:
								void setUpFolders();
							
								bool recurseTargetFiles
								(
									const std::string &root,
									std::vector<std::string> tokens,
									const std::function<void(const std::string &)> &action
								) const;
								std::vector<std::string> splitPath(const std::string &path) const;

							private:
								Module &m_module;
								config::Type m_type;

								std::string m_target;
								std::vector<DataTarget> m_targets;

								Identifier m_identifier{Identifier::defaults()};
								conv::JSON::Val m_extrasettings;
								std::string m_name, m_hint;
								Build *m_build{nullptr};
						};
					}
				}
			}
		}
	}
#endif
