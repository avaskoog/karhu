/**
 * @author		Ava Skoog
 * @date		2017-04-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/file/build/CMake.hpp>
#include <karhu/tool/proj/file/build/Module.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/platform.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

#include <fstream>

/// @todo: For Android release build, ANDROID_APK_RELEASE has to be set to one for the Apk module (see line 36 of Apk.cmake) which requires a password during build for signing. Figure this out.
/// @todo: Currently specifying by hand in the Android module settings to add libexternal.so to apklibs when linking Karhu but this should be done automatically when generating the CMake file.

/*
to be added to android release settings:
{
	"filters":
	{
		"type": ["app"],
		"config": "release"
	},
	"vars":
	{
		"ANDROID_APK_RELEASE": "1"
	}
},
*/

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace file
			{
				namespace build
				{
					CMake::CMake
					(
						Module &module,
						const config::Type type,
						const std::string &target,
						const conv::JSON::Val &extrasettings,
						const std::string &hint
					)
					:
					Config{module, type, target, extrasettings, hint}
					{
					}
					
					bool CMake::performOutput(const conv::JSON::Arr &settings)
					{
						log::msg() << "CMAKE GENERATION";

						if (!loadProjectFundamentals()) return false;
						
						if (!findAndOrderDependentTargets()) return false;

						// Open a file for the workspace.
						boost::filesystem::path p{module().project()->location().path()};
						p.append("generated");
						p.append(platform::identifier(module().platform()));
						p.append("CMakeLists.txt");
						p.make_preferred();
						std::ofstream o{p.string(), std::ios::trunc};
						if (!o.is_open())
						{
							log::err() << "Failed to open file '" << p.string() << "' for writing";
							return false;
						}

						if (!outputWorkspace(o)) return false;

						for (const auto &target : targets())
						{
							log::msg() << "Generating target: " << target.name;
							
							if (!outputDataTarget(target)) return false;
							if (!outputTarget(o, target)) return false;
						}
						
						return true;
					}
					
					std::string CMake::relativePathToProduct(const std::string &name) const
					{
						return "";
					}
					
					bool CMake::outputWorkspace(std::ostream &o)
					{
						// The minimum CMake version. Might change in the future.
						outputFunc(o, 0, "cmake_minimum_required", {"VERSION", "3.6"});

						// Create the project with C++.
						outputFunc(o, 0, "project", {identifier().proj(), "CXX"});
						
						if (!outputModules(o)) return false;
						
						return true;
					}

					bool CMake::outputModules(std::ostream &o)
					{
						// Add the module path.
						boost::filesystem::path p{module().path()};
						p.append("cmake");
						p.make_preferred();
						
						outputVar(o, 0, "CMAKE_MODULE_PATH", p.string(), false);
						
						// Standard modules.
						outputFunc(o, 0, "include", {"FindPackageHandleStandardArgs"}, false);
						
						// Find all the modules.
						std::vector<std::string> modules;
						for (const auto &target : targets())
							if (const auto mods = target.settings.data().getArray("modules"))
								for (const auto &m : *mods)
									if (m.string())
										modules.emplace_back(*m.string());

						if (modules.size() == 0) return true;

						for (const auto &m : modules)
						{
							auto subpath(p);
							subpath.append(m + ".cmake");
							subpath.make_preferred();

							if (!boost::filesystem::exists(p))
							{
								log::err() << "CMake module at '" << p.string() << "' does not exist";
								return false;
							}
							
							outputFunc(o, 0, "include", {m, "REQUIRED"}, false);
						}
						
						o << '\n';
						
						return true;
					}
					
					bool CMake::outputTarget(std::ostream &o, const DataTarget &target)
					{
						// Output all the variables.
						if (!outputTargetVars(o, target)) return false;

						// Output the target definition.
						if (!outputTargetDefinition(o, target)) return false;
						
						// Output all the properties.
						if (!outputTargetProps(o, target)) return false;

						// Include directories.
						if (!outputTargetBuildflags(o, target)) return false;
						
						// Definitions.
						if (!outputTargetDefines(o, target)) return false;

						// Include directories.
						if (!outputTargetIncludedirs(o, target)) return false;

						// Link directories, flags and libraries are all rolled into one.
						if (!outputTargetLinks(o, target)) return false;

						// For now, this just checks whether to create an Android APK.
						if (!outputTargetCommands(o, target)) return false;
						
						// Configurations.
						outputVar(o, 0, "CMAKE_CONFIGURATION_TYPES", "Release;Debug", false);
						outputVar(o, 0, "CMAKE_BUILD_TYPE", "Release;Debug");
						
						return true;
					}

					bool CMake::outputTargetVars(std::ostream &o, const DataTarget &target)
					{
						if (ignores(target, "vars")) return true;
						
						if (const auto vars = target.settings.data().getObject("vars"))
						{
							for (const auto &var : *vars)
							{
								if (var.second.string())
									outputVar(o, 0, var.first, *var.second.string(), false);
								else
								{
									log::err()
										<< "Build module variable '" << var.first
										<< "' has to be string; cannot be "
										<< var.second.typedump() << '!';
									return false;
								}
							}

							if (vars->count() > 0) o << '\n';
						}
						
						return true;
					}
					
					bool CMake::outputTargetDefinition(std::ostream &o, const DataTarget &target)
					{
						if (const auto type = typeFinalTarget(target))
						{
							// We will find the files here instead of trying
							// to mess with globs and such in the generated file.
							const auto files(findTargetFiles(target));
							if (!files) return false;
							
							std::stringstream s;
							s << '\"';
							for (std::size_t i{0}; i < files->size(); ++ i)
							{
								// Prepend full basepath.
								boost::filesystem::path p{(*files)[i]};
								p.make_preferred();

//								if (p.is_relative())
//								{
//									p = boost::filesystem::path{module().project()->location().path()};
//									p.append((*files)[i]);
//									p.make_preferred();
//								}

								s << p.string();
								if ((i + 1) < files->size()) s << ';';
							}
							s << '\"';
							
							const auto f(s.str());
							
							log::msg() << "FILES FOUND:";
							log::msg() << f;

							// Check what type to print.
							/// @todo: Add files.
							if (*type == "app")
								outputFunc(o, 0, "add_executable", {nameTarget(target.name), f});
							else if (*type == "slib" || *type == "dlib")
								outputFunc(o, 0, "add_library", {nameTarget(target.name), cmakeType(*type), f});
							else
							{
								log::err() << "Invalid type '" << *type << "' for target '" << target.name << "'";
								return false;
							}
						}
						
						return true;
					}

					bool CMake::outputTargetProps(std::ostream &o, const DataTarget &target)
					{
						if (ignores(target, "props")) return true;
						
						using namespace std::literals::string_literals;
						
						if (const auto props = target.settings.data().getObject("props"))
						{
							for (const auto &prop : *props)
							{
								if (prop.second.string())
									outputProp(o, 0, target, prop.first, *prop.second.string(), false);
								else
								{
									log::err()
										<< "Build module property '" << prop.first
										<< "' has to be string; cannot be "
										<< prop.second.typedump() << '!';
									return false;
								}
							}

							if (props->count() > 0) o << '\n';
						}
						
						return true;
					}
					
					bool CMake::outputTargetBuildflags(std::ostream &o, const DataTarget &target)
					{
						if (ignores(target, "buildflags")) return true;
						
						std::vector<std::string> values;

						// Add the initial arguments to the function.
						values.emplace_back(nameTarget(target.name));
						values.emplace_back("PUBLIC");
						
						// Enable optimisations for release build.
						if (config::Type::release == config())
						{
							values.emplace_back("-O3");
							values.emplace_back("-s");
							values.emplace_back("-ffast-math");
						}
						
						// Add the directories.
						if (const auto flags = target.data.object()->getArray("buildflags"))
							for (const auto &flag : *flags)
								if (flag.string())
									values.emplace_back(*flag.string());

						if (values.size() > 0)
							outputFunc(o, 0, "target_compile_options", values);

						return true;
					}
					
					bool CMake::outputTargetDefines(std::ostream &o, const DataTarget &target)
					{
						using namespace std::literals::string_literals;

						if (ignores(target, "defines")) return true;
						
						std::vector<std::string> values;

						// Add the initial arguments to the function.
						values.emplace_back(nameTarget(target.name));
						values.emplace_back("PUBLIC");
						
						// Enable debug flag for debug build.
						if (config::Type::debug == config())
						{
							values.emplace_back("DEBUG");
							values.emplace_back("NDEBUG");
							values.emplace_back("NDK_DEBUG=0");
						}
						
						// Add the directories.
						if (const auto defines = target.data.object()->getArray("defines"))
							for (const auto &define : *defines)
								if (define.string())
									//values.emplace_back("\"\\\""s + string::escapeQuotesDouble<char>(*define.string()) + "\\\"\"");
									values.emplace_back(*define.string());

						if (values.size() > 0)
							outputFunc(o, 0, "target_compile_definitions", values);

						return true;
					}
					
					bool CMake::outputTargetIncludedirs(std::ostream &o, const DataTarget &target)
					{
						if (ignores(target, "includedirs")) return true;
						
						using namespace std::literals::string_literals;
						
						const auto root(rootTarget(target));
						if (!root) return false;

						std::vector<std::string> values;
						
						// Add the initial arguments to the function.
						values.emplace_back(nameTarget(target.name));
						values.emplace_back("PUBLIC");

						// Add the directories.
						if (const auto dirs = target.data.object()->getArray("includedirs"))
							for (const auto &dir : *dirs)
								if (dir.string())
									values.emplace_back("\""s + pathAbsFromRel(*root, *dir.string()) + '\"');

						if (values.size() > 0)
							outputFunc(o, 0, "target_include_directories", values);
						
						return true;
					}
					
					bool CMake::outputTargetLinks(std::ostream &o, const DataTarget &target)
					{
						using namespace std::literals::string_literals;
						
						const auto root(rootTarget(target));
						if (!root) return false;
						
						if (const auto type = typeFinalTarget(target))
						{
							std::vector<std::string> values;
							
							// Add the initial arguments to the function.
							values.emplace_back(nameTarget(target.name));
							values.emplace_back("PUBLIC");
							
							// Add the linker flags.
							if (!ignores(target, "linkflags"))
								if (const auto flags = target.data.object()->getArray("linkflags"))
									for (const auto &flag : *flags)
										if (flag.string())
											values.emplace_back(*flag.string());

							// Add the library directories.
							if (!ignores(target, "linkdirs"))
								if (const auto dirs = target.data.object()->getArray("linkdirs"))
									for (const auto &dir : *dirs)
										if (dir.string())
											values.emplace_back("-L\""s + pathAbsFromRel(*root, *dir.string()) + '\"');
							
							// Add the actual libraries.
							if (!ignores(target, "links"))
								if (const auto libs = target.data.object()->getArray("links"))
									for (const auto &lib : *libs)
										if (lib.string())
											values.emplace_back(*lib.string());

							if (values.size() > 0)
								outputFunc(o, 0, "target_link_libraries", values);
						}

						return true;
					}
					
					bool CMake::outputTargetCommands(std::ostream &o, const DataTarget &target)
					{
						using namespace std::literals::string_literals;
						
						const auto type(typeTarget(target));
						if (!type) return false;
						
						if (*type != "app") return true;

						const auto modules(target.settings.data().getArray("modules"));
						if (modules)
						{
							for (const auto &m : *modules)
							{
								// If an Apk module has been loaded, make an APK!
								if (m.string() && *m.string() == "Apk")
								{
									std::vector<std::string> args;
									
									// Add the name of the project.
									args.emplace_back(nameTarget(target.name));

									// Add the full bundle identifier.
									args.emplace_back(identifier().full());
									
									// Add the library files, including to the target itself.
									std::stringstream libraries;
									
									if (const auto libs = target.settings.data().getArray("apklibs"))
										for (const auto &lib : *libs)
											if (lib.string())
												libraries << *lib.string() << ';';
									
									libraries << "${LIBRARY_OUTPUT_PATH}lib" << nameTarget(target.name) << ".so";

									args.emplace_back(libraries.str());

									// Assets.
									std::stringstream assetlist;
									if (const auto assets = target.settings.data().getArray("apkassets"))
									{
										std::size_t i{0};
										for (const auto &asset : *assets)
										{
											if (asset.string())
											{
												assetlist << *asset.string();

												if ((i + 1) < assets->count())
													assetlist << ';';
											}
											
											++ i;
										}
									}
									args.emplace_back(assetlist.str());

									// Resources.
									std::stringstream reslist;
									if (const auto resources = target.settings.data().getArray("apkresources"))
									{
										std::size_t i{0};
										for (const auto &res : *resources)
										{
											if (res.string())
											{
												reslist << *res.string();

												if ((i + 1) < resources->count())
													reslist << ';';
											}
											
											++ i;
										}
									}
									args.emplace_back(reslist.str());
									
									// We do not want a data folder.
									args.emplace_back("");
									
									// Quote all the values.
									for (auto &arg : args)
										arg = "\""s + arg + '"';

									// Go!
									outputFunc(o, 0, "android_create_apk", args);
									break;
								}
							}
						}
						
						return true;
					}

					void CMake::outputVar
					(
						std::ostream &o,
						const std::size_t &level,
						const std::string &name,
						const std::string &value,
						const bool emptyLine
					)
					{
						using namespace std::literals::string_literals;
						outputFunc(o, 0, "set", {name, "\""s + value + '\"'}, emptyLine);
					}

					void CMake::outputProp
					(
						std::ostream &o,
						const std::size_t &level,
						const DataTarget &target,
						const std::string &name,
						const std::string &value,
						const bool emptyLine
					)
					{
						using namespace std::literals::string_literals;
						outputFunc
						(
							o, 0,
							"set_property",
							{
								"TARGET",
								nameTarget(target.name),
								"PROPERTY",
								name,
								"\""s + value + '\"'
							},
							emptyLine
						);
					}
					
					void CMake::outputFunc
					(
						std::ostream &o,
						const std::size_t &level,
						const std::string &name,
						const std::vector<std::string> &args,
						const bool emptyLine
					)
					{
						outputIndent(o, level);
						o << name << '(';
						for (std::size_t i{0}; i < args.size(); ++ i)
						{
							o << args[i];
							if ((i + 1) < args.size()) o << ' ';
						}
						o << ")\n";
						if (emptyLine) o << '\n';
					}
					
					Nullable<std::string> CMake::typeFinalTarget(const DataTarget &target) const
					{
						// Check the type.
						std::string type;
						if (const auto t = typeTarget(target))
							type = *t;
						else
							return {};

						// A CMake kind property will override.
						if (!ignores(target, "cmakekind"))
						{
							if (const auto cmakekind = target.settings.data().getString("cmakekind"))
								type = *cmakekind;
						}
						
						return {true, type};
					}

					std::string CMake::cmakeType(const std::string &type) const
					{
						if      (type == "slib")   return "STATIC";
						else if (type == "dlib")   return "SHARED";
						else if (type == "module") return "SHARED"; /// @todo: Change this conditionally based on config…

						return "";
					}
				}
			}
		}
	}
}
