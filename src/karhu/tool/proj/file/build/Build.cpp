/**
 * @author		Ava Skoog
 * @date		2017-04-04
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/file/build/Build.hpp>
#include <karhu/tool/proj/file/build/Target.hpp>
#include <karhu/tool/proj/file/Project.hpp>
#include <karhu/network/Protocol.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/log.hpp>

#include <fstream>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace file
			{
				namespace build
				{
					Build::Build(const Location &location)
					:
					DataJSON{location, SDK::paths::project::build()} /// @todo: Move string out of SDK header and in here?
					{
						// Default empty document.
						setUpDefaults();
					}

					void Build::port(const std::uint16_t value)
					{
						rootJSON().object()->set("port", value);
					}
					
					Nullable<std::uint16_t> Build::port() const
					{
						/// @todo: Validate port?
						if (auto v = rootJSON().object()->get("port"))
							if (v->integer())
								return {true, static_cast<std::uint16_t>(*v->integer())};
						
						return {};
					}

					void Build::SSH(const SettingsSSH &vals)
					{
						auto l(rootJSON().object()->get("sshs")->object());

						l->set(vals.name, conv::JSON::Obj
						{{
							{"addr", vals.addr},
							{"dir",  vals.dir}
						}});
					}
					
					std::vector<Build::SettingsSSH> Build::SSHs() const
					{
						std::vector<SettingsSSH> result;

						if (auto s = rootJSON().object()->get("sshs"))
						{
							for (auto &n : *s->object())
							{
								SettingsSSH s;
								
								s.name = n.first;
								s.addr = *n.second.object()->get("addr")->string();
								s.dir  = *n.second.object()->get("dir")->string();
								
								result.emplace_back(std::move(s));
							}
						}

						return result;
					}
					
					void Build::system(const SettingsSystem &vals)
					{
						auto l(rootJSON().object()->get("systems")->object());
						auto &o(l->set(vals.name, conv::JSON::Obj{}));

						if (vals.SSH.length() > 0)
							o.object()->push({"ssh", vals.SSH});
						
						if (vals.teamID.length() > 0)
							o.object()->push({"team-id", vals.teamID});
					}
					
					std::vector<Build::SettingsSystem> Build::systems() const
					{
						std::vector<SettingsSystem> result;
						
						if (auto s = rootJSON().object()->get("systems"))
						{
							for (auto &n : *s->object())
							{
								SettingsSystem s;

								s.name = n.first;
								
								if (auto v = n.second.object()->get("ssh"))
									s.SSH = *v->string();
								
								if (auto v = n.second.object()->get("team-id"))
									s.teamID = *v->string();

								result.emplace_back(std::move(s));
							}
						}
						
						return result;
					}
					
					void Build::setUpDefaults()
					{
						namespace pf = platform;

						// Add a system list if there is none.
						auto s(rootJSON().object()->get("systems"));
						if (!s) s = &rootJSON().object()->push({"systems", conv::JSON::Obj{}});

						// Add any missing default systems.
						auto trySetSystem([this, &s](const std::string &name)
						{
							if (!s->object()->get(name))
								system({name});
						});
						
						trySetSystem(pf::identifier(Platform::mac));
						trySetSystem(pf::identifier(Platform::linux));
						trySetSystem(pf::identifier(Platform::windows));
						trySetSystem(pf::identifier(Platform::iOS));
						trySetSystem(pf::identifier(Platform::android));
						trySetSystem(pf::identifier(Platform::web));
						
						// Add a target list if there is none.
						auto t(rootJSON().object()->get("targets"));
						if (!t) t = &rootJSON().object()->push({"targets", conv::JSON::Obj{}});
						
						// Add any missing main default target.
						auto main = t->object()->get("main");
						if (!main)
						{
							main = &t->object()->push({"main", conv::JSON::Obj{}});
							
							// The name of the target is always main, and the
							// actual name is extracted from the info file.
							SettingsTarget s;
							s.name = "main";
							
							// C++ 14 application.
							s.general.type = "app";
							s.general.CPP  = "14";
							
							// Includes all source files and links against Karhu.
							s.general.includes.emplace_back("**.cpp");
							s.general.links.emplace_back("karhu");
							
							// Encourage good code!
							s.general.buildflags.emplace_back("-Wall");
							s.general.buildflags.emplace_back("-Wextra");
							s.general.buildflags.emplace_back("-Wpedantic");
							s.general.buildflags.emplace_back("-Werror");
						}

						// Add the default port offset for the debug tools.
						if (!port())
							port(network::protocol::defaultport());
					}
					
					bool Build::afterLoad()
					{
						if (!rootJSON().object()) return true;
						
						if (auto targets = rootJSON().object()->getObject("targets"))
						{
							for (auto &i : *targets)
							{
								if (i.second.object())
								{
									m_targets.emplace_back(std::move(i.first), std::move(i.second));
									if (!m_targets.back().second.valid()) return false;
								}
							}

							targets->clear();
						}

						// Order main target last.
						/// @todo: Order targets by linking dependencies?
						std::sort(m_targets.begin(), m_targets.end(), [](const auto &a, const auto &b)
						{
							return (a.first != "main");
						});

						return true;
					}
					
					bool Build::beforeSave()
					{
						if (!rootJSON().object()) return true;
						if (m_targets.size() == 0) return true;
						
						auto targets(rootJSON().object()->getObject("targets"));
						if (!targets)
							targets = rootJSON().object()->push({"targets", conv::JSON::Obj{}}).object();

						targets->clear();

						for (const auto &i : m_targets)
						{
							if (!i.second.valid()) return false;
							targets->push({i.first, i.second.data()});
						}
						
						return true;
					}
					
					void Build::target(const std::string &name, const Target &target)
					{
						m_targets.emplace_back(name, target);
					}
					
					void Build::target(const std::string &name, Target &&target)
					{
						m_targets.emplace_back(name, std::move(target));
					}
				}
			}
		}
	}
}
