/**
 * @author		Ava Skoog
 * @date		2017-08-18
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/file/build/Module.hpp>
#include <karhu/tool/proj/file/build/Target.hpp>
#include <karhu/tool/proj/file/Info.hpp>
#include <karhu/tool/proj/file/build/Premake.hpp>
#include <karhu/tool/proj/file/build/CMake.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace file
			{
				namespace build
				{
					/**
					 * Tries to load a target and its settings for the specified platform.
					 * This requires the target module to exist on the current platform.
					 *
					 * @param platform The target system.
					 *
					 * @return A nullable containing a target object if found, or null.
					 */
					Nullable<Module> Module::loadForPlatform(const karhu::Platform &platform)
					{
						namespace fs = boost::filesystem;
						namespace pf = platform;
						
						fs::path p{"targets"}; /// @todo: Unhardcode this path?
						p.append(pf::identifier(Platform::current));
						p.append(pf::identifier(platform));
						p.make_preferred();
						
						const std::string basesubpath{p.string()};
						const auto basepath(SDK::env::home(p.string()));
						
						if (!basepath || !fs::exists(*basepath) || !fs::is_directory(*basepath))
						{
							log::err()
								<< "Module directory not found at '" << p.string() << "'. "
								<< "Make sure the KARHU_HOME environment variable has been set "
								<< "and that the module exists.";

							return {};
						}
						
						p.append("settings.karhutarget.json"); /// @todo: Unhardcode this path?
						p.make_preferred();
						
						const auto settingspath(SDK::env::home(p.string()));

						if (!settingspath || !fs::exists(*settingspath))
						{
							log::err()
								<< "Module settings not found at '" << p.string() << "'. "
								<< "Make sure the KARHU_HOME environment variable has been set.";

							return {};
						}

						std::ifstream f{*settingspath};
						
						if (!f.is_open())
						{
							log::err() << "Failed to open file '" << *settingspath << "'.";
							return {};
						}
						
						conv::JSON::Parser parser;
						if (auto o = parser.parse(f))
							return {true, std::move(*o), platform, *basepath, basesubpath};
						else
						{
							log::err() << "Error decoding file '" << *settingspath << "': " << parser.error();
							return {};
						}

						return {};
					}
					
					/**
					 * Tries to create buildfiles for the specified project, reading its
					 * settings files to retrieve variables. Outputs an error on failure.
					 *
					 * @param project The project to create the buildfiles for.
					 *
					 * @return Whether buildfiles were successfully written.
					 */
					bool Module::outputBuildfiles
					(
						Project &project,
						const config::Type conf,
						const std::string &target,
						const conv::JSON::Val &extrasettings,
						const std::string &hint
					)
					{
						/// @todo Check requirements such as min. Karhu version.

						m_project = &project;

						if (!m_root.object()) return false; /// @todo: Error message?

						// Type is compulsory.
						const auto type(m_root.object()->findString({"buildsystem", "type"}));
						if (type)
						{
							// Modules are optional.
//							std::vector<std::string> mods;
//							if (const auto modules = m_root.object()->findArray({"buildsystem", "modules"}))
//								for (const auto &module : *modules)
//									if (const auto s = module.string())
//										mods.push_back(*s);

							if (*type == "premake")
								return outputBuildfiles(TypeOutput::premake, conf, target, extrasettings, hint);
							else if (*type == "cmake")
								return outputBuildfiles(TypeOutput::cmake, conf, target, extrasettings, hint);
							else
								log::err() << "Invalid build system type '" << *type << "'";
						}
						else
							log::err() << "'type' not found in target build system settings";

						return false;
					}
					
					bool Module::outputBuildfiles
					(
						const TypeOutput type,
						const config::Type conf,
						const std::string &target,
						const conv::JSON::Val &extrasettings,
						const std::string &hint
					)
					{
						m_files.clear();

						// We need target module's build settings.
						const auto settings(arrayJSONSettings());
						if (!settings)
						{
							log::err() << "Could not find 'settings' array in target's settings.karhutarget.json file";
							return false;
						}

						// All configuration file handles interface the same.
						std::unique_ptr<Config> config;
						
						// We just need to create the correct underlying type.
						switch (type)
						{
							case TypeOutput::premake:
								config = std::make_unique<Premake>(*this, conf, target, extrasettings, hint);
								break;
							
							case TypeOutput::cmake:
								config = std::make_unique<CMake>(*this, conf, target, extrasettings, hint);
								break;
						}

						// Last step!
						return config->output(*settings);
					}

					std::unique_ptr<Config> Module::getConfig
					(
						Project &project,
						const config::Type conf,
						const std::string &target,
						const conv::JSON::Val &extrasettings,
						const std::string &hint
					)
					{
						/// @todo Check requirements such as min. Karhu version.

						m_project = &project;

						if (!m_root.object()) return {}; /// @todo: Error message?

						m_files.clear();

						// We need target module's build settings.
						const auto settings(arrayJSONSettings());
						if (!settings)
						{
							log::err() << "Could not find 'settings' array in target's settings.karhutarget.json file";
							return {};
						}

						// All configuration file handles interface the same.
						auto config{std::make_unique<Config>(*this, conf, target, extrasettings, hint)};
						
						if (!config->loadProjectFundamentals()) return {};
						if (!config->findAndOrderDependentTargets()) return {};

						return config;
					}
					
					bool Module::replaceVariables
					(
						conv::JSON::Val &v,
						const config::Type conf,
						const std::string &name,
						const Target &target
					) const
					{
						switch (v.type())
						{
							case JSON::TypeValue::string:
							{
								if (!Parser{*this, *v.string(), conf, name, target}.parseVars())
									return false;
								
								break;
							}
							
							case JSON::TypeValue::array:
							{
								for (auto &i : *v.array())
								{
									if (!replaceVariables(i, conf, name, target))
										return false;
								}
								
								break;
							}
							
							case JSON::TypeValue::object:
							{
								for (auto &i : *v.object())
								{
									if (!replaceVariables(i.second, conf, name, target))
										return false;
								}
								
								break;
							}
							
							default:
								break;
						}
						
						return true;
					}
					
					DataJSON *Module::loadFile(const std::string &name) const
					{
						const auto i(m_files.find(name));
						if (i != m_files.end())
							return &i->second;

						DataJSON data{m_project->location(), name + ".karhuproj.json"};
						if (data.load())
							return &m_files.emplace(name, std::move(data)).first->second;

						return nullptr;
					}

					conv::JSON::Arr *Module::arrayJSONSettings()
					{
						if (m_root.object())
							if (const auto buildsystem = m_root.object()->get("buildsystem"))
								if (buildsystem->object())
									if (const auto settings = buildsystem->object()->get("settings"))
										return settings->array();

						return nullptr;
					}

					bool Module::Parser::parseVars()
					{
						while (!done())
						{
							const char &c(get());
							
							// A double $$ indicates a variable.
							if ('$' == c && !done() && '$' == peek())
							{
								get();
								m_start = m_index;
								if (!parseVar()) return false;
							}
							else
								m_stream << c;
						}
						
						m_buffer = m_stream.str();

						return true;
					}
					
					bool Module::Parser::parseVar()
					{
						std::stringstream part;
						
						auto push([this, &part]()
						{
							m_parts.emplace_back(part.str());
							part.str("");
						});

						// Keep going until we find the closing $$, a colon, or EOF.
						while (!done())
						{
							const char &c(get());
							
							// Colon is part separator.
							if (':' == c)
								push();
							// A double $$ indicates end.
							else if ('$' == c && !done() && '$' == peek())
							{
								get();
								push();
								return replaceVar();
							}
							else
								part << c;
						}
						
						/// @todo: Add more info to error such as prop name?
						log::err() << "Error parsing variable: missing closing '$$'.";
						return false;
					}
					
					bool Module::Parser::replaceVar()
					{
						if (m_parts.size() == 0)
						{
							log::err() << "Variable must have a name";
							return false;
						}
						else if (m_parts.size() == 1)
							return replaceFromValue();
						else
							return replaceFromFile();
					}
					
					bool Module::Parser::replaceFromValue()
					{
						const auto &v(m_parts[0]);
						
						if ("KARHU_HOME" == v)
						{
							if (const auto p = SDK::env::home())
								replace(*p);
							else
							{
								log::err() << "Could not find environment variable 'KARHU_HOME'";
								return false;
							}
						}
						else if ("CONFIG" == v)
							replace(config::toString(m_conf));
						else if ("MODULE_ROOT" == v)
							replace(m_parent.path());
						else if ("PROJECT_ROOT" == v)
							// Project and location already validated at this point.
							replace(m_parent.m_project->location().path());
						else if ("TARGET" == v)
							// Project and location already validated at this point.
							replace(m_name);
						else
						{
							log::err() << "Invalid variable '" << v << "'";
							return false;
						}
						
						return true;
					}
					
					bool Module::Parser::replaceFromFile()
					{
						if (const auto f = m_parent.loadFile(m_parts[0]))
						{
							std::vector<std::string> parts;
							std::string name{m_parts[0]};
							
							for (std::size_t i{1}; i < m_parts.size(); ++ i)
							{
								parts.emplace_back(m_parts[i]);
								name += '.' + m_parts[i];
							}
							
							if (const auto o = f->get(parts))
							{
								replace(*o);
								return true;
							}
							else
							{
								log::err() << "Could not find '" << name << "'.";
								return false;
							}
						}
						
						return false;
					}
					
					void Module::Parser::replace(const std::string &s)
					{
						m_stream << s;
						m_parts.clear();
					}

					const conv::JSON::Val *Module::getProjectValue
					(
						const Target &target,
						const config::Type conf,
						const std::string &key
					) const
					{
						if (!target.data().object()) return nullptr;

						// Specialised configurations have override precedence.
/*						auto precedence
						(
							(config::Type::debug == conf)
							?
								target.data().object()->getObject("debug")
							:
								target.data().object()->getObject("release")
						);

						// Otherwise fall back to the general settings.
						if (!precedence)
							precedence = target.data().object();

						if (!precedence) return nullptr;
*/
						const auto precedence(target.data().object());

						// Try to find the value.
						if (const auto v = precedence->get(key))
							return v;

						return nullptr;
					}

					Nullable<Settings> Module::findSettingsForTarget
					(
						const std::string &name,
						const std::string &nameFinal,
						const Target &target,
						const config::Type conf,
						const conv::JSON::Val &extrasettings,
						const std::string &hint
					) const
					{
						using T = JSON::TypeValue;

						static const std::map<std::string, Option> options
						{
							{"overrides",    {T::object}},
							{"appends",      {T::object}},
							{"xcode",        {T::object}},
							{"vars",         {T::object}},
							{"props",        {T::object}},
							{"ignores",      {T::array}},
							{"modules",      {T::array}},
							{"links",        {T::array}},
							{"includedirs",  {T::array}},
							{"linkdirs",     {T::array}},
							{"apklibs",      {T::array}},
							{"apkassets",    {T::array}},
							{"apkresources", {T::array}},
							{"toolset",      {T::string}},
							{"toolprefix",   {T::string}},
							{"binname",      {T::string}},
							{"premakekind",  {T::string}},
							{"cmakekind",    {T::string}}
						};
						
						/// @todo: Remove this testing below when done with it.
						{
							std::stringstream s;
							
							for (const auto &i : options)
							{
								s << '\n' << i.first << ": " << JSON::typeValueToString(i.second.type);
								
								for (const auto &j : i.second.valid)
									s << "\n\t" << j;
							}

							//log::msg("SETTINGS") << s.str() << '\n';
						}
						///////////////////////
					
						// List of individual settings by key, with priority
						// dictated by how many things the filter matched.
						SettingsFiltered f;

						if (!m_root.object())
							return {};

						// Find the array of settings in the target.
						const auto settings(m_root.object()->findArray({"buildsystem", "settings"}));

						if (!settings)
							return {};
						
						// Check if there are extrasettings.
						const bool hasExtras{extrasettings.object() && extrasettings.object()->count() > 0};

						// Go through each setting.
						for (const auto &s : *settings)
						{
							if (!s.object()) continue;
							
							conv::JSON::Val setting{s};

							if (hasExtras)
							{
								auto appends(setting.object()->getObject("appends"));
								if (!appends) appends = setting.object()->push({"appends", conv::JSON::Obj{}}).object();
								
								for (auto &extra : *extrasettings.object())
								{
									auto val(appends->get(extra.first));
									if (!val)
										appends->push({extra.first, extra.second});
									else if (val->type() == extra.second.type())
									{
										if (val->array())
										{
											log::msg("debug") << "appending extra values " << extra.second.dump(); /// @todo: Remove this.
											val->array()->prepend(*extra.second.array());
										}
										/// @todo: Deal with objects and single values?
									}
								}
							}

							// Higher number means higher priority.
							std::uint8_t countMatches{0};

							// Find out if there is any filter at all, and if so, if the
							// filter matches, and if so, how many matches there are.
							const auto filters(setting.object()->getObject("filters"));

							// Find out if the filter matches.
							// If there is no filter at all, that counts as a match.
							bool matches{true};
							if (filters)
							{
								for (const auto &filter : *filters)
								{
									// Get the corresponding value from the project.
									auto pval(getProjectValue(target, conf, filter.first));
									
									if (!pval && hasExtras)
										pval = extrasettings.object()->get(filter.first);
									
									// The following keys and values are not actually found in the
									// project's build settings but are passed to the target and
									// so need to be handled manually instead.
									const bool isConfig{filter.first == "config"},
									           isTarget{filter.first == "target"},
											   isHint  {filter.first == "hint"};
									
									if (!pval && !isConfig && !isTarget && !isHint)
									{
										// Break back out to the setting loop.
										matches = false;
										break;
									}

									// Each filter value is an array. Another loop.
									if (const auto arr = filter.second.array())
									{
										bool found{false};
										for (const auto &val : *arr)
										{
											if (val.string())
											{
												// Is the project value a string?
												if (isConfig || isTarget || isHint || (pval && pval->string()))
												{
													std::string s;
													
													// Config and target are special cases.
													if      (isConfig) s = config::toString(conf);
													else if (isTarget) s = nameFinal;
													else if (isHint)   s = hint;
													else               s = *pval->string();

													if (s == *val.string())
														found = true;
												}
												// Is it an array?
												else if (pval && pval->array())
												{
													// Then we have to check against every value of the array.
													for (const auto &v : *pval->array())
													{
														if (v.string() && *v.string() == *val.string())
														{
															found = true;
															break;
														}
													}
												}
												// Cannot be an object.

												// Done?
												if (found)
												{
													// Break back to the outer filter loop.
													++ countMatches;
													break;
												}
											}
										}

										// If the value was not found in the project, no match.
										if (!found)
										{
											// Break back out to the setting loop.
											matches = false;
											break;
										}
									}
								}
							}

							// If the filter matches, go through each non-filter setting,
							// including overrides, and add them to the resulting container
							// with the correct priority (the number of matches).
							if (matches)
							{
								for (const auto &option : *setting.object())
								{
									if (option.first != "filters")
									{
										// The option must exist.
										const auto it(options.find(option.first));
										if (it == options.end())
										{
											log::err() << "'" << option.first << "' is not a valid build module option";
											return {};
										}
										// The value must be of the right type.
										else if (it->second.type != option.second.type())
										{
											log::err()
												<< "Option '" << option.first
												<< "' cannot be " << JSON::typeValueToString(option.second.type())
												<< '!';
											return {};
										}
										else
											pushOptionAtPriority(f, option.first, countMatches, option.second);
									}
								}
							}
						}
						
						// Sort the options by ascending priority.
						// The chosen datatype, multimap, already does this.
						
						// Start building the final JSON object to return.
						conv::JSON::Val r{conv::JSON::Obj{}};
						
						// Get all the values and avoid duplicates.
						for (auto &filtered : f)
						{
							// Get the JSON type to figure out what to do.
							// No need to validate this as the validation above
							// made sure that no invalid settings were pushed.
							const auto it  (options.find(filtered.first));
							const auto type(it->second.type);

							// If string, keep only the highest priority one.
							if (T::string == type)
							{
								const auto s(filtered.second.rbegin()->second.string());
								if (s) r.object()->set(filtered.first, *s);
							}
							// If object, use only highest priority in case of duplicate keys.
							else if (T::object == type)
							{
								// Create an object with the same name.
								auto &o(r.object()->push({filtered.first, conv::JSON::Obj{}}));

								for (auto &p : filtered.second)
								{
									// Go through each object.
									for (auto &v : *p.second.object())
									{
										// For overrides and appends, we want to accumulate values.
										if (T::array == v.second.type())
										{
											auto arr(o.object()->getArray(v.first));
											if (!arr)
												o.object()->set(v.first, v.second);
											else
												for (const auto &vv : *v.second.array())
													arr->push(vv);
										}
										/// @todo: Fix objects, recursion, ..?
										// Since the values are ordered from lowest priority, we can
										// just keep overwriting any duplicates, and thus the one with
										// the highest priority will be written last and win out.
										else
											o.object()->set(v.first, v.second);
									}
								}
							}
							// Arrays cannot have duplicate keys, but still duplicate values.
							else if (JSON::TypeValue::array == type)
							{
								// Start by collecting the values in a regular vector.
								std::vector<conv::JSON::Val> values;
								for (auto &p : filtered.second)
									for (auto &v : *p.second.array())
										// Just add everything for now.
										values.emplace_back(*v.string());
								
								// Now sort and remove duplicates.
//								std::sort(values.begin(), values.end(), [](const auto &a, const auto &b){ return (*a.string() < *b.string()); });
//								auto last(std::unique(values.begin(), values.end()));
//								values.erase(last, values.end());
								
								// Finally create a JSON array with all the values.
								r.object()->push({filtered.first, conv::JSON::Arr{values}});
							}
						}

						// An error message will have been output if something went wrong.
						if (!replaceVariables(r, conf, name, target)) return false;
						
						/// @todo: Remove this after done testing
						///
						///
//						{
//							log::msg("FINAL") << '\n' << conv::JSON::Val{r}.dump();
//						}
						///
						///
						/////////////////////////

						// Done!
						return {true, conf, std::move(*r.object())};
					}

					void Module::pushOptionAtPriority
					(
						SettingsFiltered &container,
						const std::string &key,
						const std::uint8_t priority,
						const conv::JSON::Val &value
					) const
					{
						auto it(container.find(key));
						if (it == container.end())
							it = container.emplace(key, SettingsOrdered{}).first;
						
						it->second.emplace(priority, value);
					}
				}
			}
		}
	}
}
