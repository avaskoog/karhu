/**
 * @author		Ava Skoog
 * @date		2017-04-06
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_FILE_BUILD_CMAKE_H_
	#define KARHU_TOOL_PROJ_FILE_BUILD_CMAKE_H_

	#include <karhu/tool/proj/file/build/Config.hpp>
	#include <karhu/conv/types.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					namespace build
					{
						class CMake : public Config
						{
							public:
								CMake
								(
									Module &module,
									const config::Type type,
									const std::string &target,
									const conv::JSON::Val &extrasettings = {},
									const std::string &hint = {}
								);
							
							protected:
								bool performOutput(const conv::JSON::Arr &settings) override;
								std::string relativePathToProduct(const std::string &name) const override;
							
							private:
								bool outputWorkspace(std::ostream &o);
								bool outputModules(std::ostream &o);
							
								bool outputTarget(std::ostream &o, const DataTarget &target);
								bool outputTargetVars(std::ostream &o, const DataTarget &target);
								bool outputTargetDefinition(std::ostream &o, const DataTarget &target);
								bool outputTargetProps(std::ostream &o, const DataTarget &target);
								bool outputTargetBuildflags(std::ostream &o, const DataTarget &target);
								bool outputTargetDefines(std::ostream &o, const DataTarget &target);
								bool outputTargetIncludedirs(std::ostream &o, const DataTarget &target);
								bool outputTargetLinks(std::ostream &o, const DataTarget &target);
								bool outputTargetCommands(std::ostream &o, const DataTarget &target);

								void outputVar
								(
									std::ostream &o,
									const std::size_t &level,
									const std::string &name,
									const std::string &value,
									const bool emptyLine = true
								);

								void outputProp
								(
									std::ostream &o,
									const std::size_t &level,
									const DataTarget &target,
									const std::string &name,
									const std::string &value,
									const bool emptyLine = true
								);

								void outputFunc
								(
									std::ostream &o,
									const std::size_t &level,
									const std::string &name,
									const std::vector<std::string> &args,
									const bool emptyLine = true
								);

								Nullable<std::string> typeFinalTarget(const DataTarget &) const;
								std::string cmakeType(const std::string &) const;
						};
					}
				}
			}
		}
	}
#endif
