/**
 * @author		Ava Skoog
 * @date		2017-04-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/file/build/Premake.hpp>
#include <karhu/tool/proj/file/build/Module.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/platform.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

#include <fstream>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace file
			{
				namespace build
				{
					Premake::Premake
					(
						Module &module,
						const config::Type type,
						const std::string &target,
						const conv::JSON::Val &extrasettings,
						const std::string &hint
					)
					:
					Config{module, type, target, extrasettings, hint}
					{
					}
					
					bool Premake::performOutput(const conv::JSON::Arr &settings)
					{
						log::msg() << "PREMAKE GENERATION";

						namespace fs = boost::filesystem;

						fs::path path{module().project()->location().path()};
						path.append("generated");
						path.append(platform::identifier(module().platform()));
						path.append("premake5.lua");
						path.make_preferred();
						
						std::ofstream o{path.string(), std::ios::trunc};

						if (!loadProjectFundamentals()) return false;
						
						if (!findAndOrderDependentTargets()) return false;

						// Modules go first.
						if (!outputModules(o)) return false;

						// There is only one workspace.
						if (!outputWorkspace(o)) return false;

						// Then each individual target contains its own settings.
						for (const auto &target : targets())
						{
							if (!outputDataTarget(target)) return false;
							if (!outputTarget(o, target)) return false;
						}

						return true;
					}
					
					std::string Premake::relativePathToProduct(const std::string &name) const
					{
						boost::filesystem::path p{"bin"};
						p.append(name);
						p.append(config::toString(config()));
						return p.string();
					}

					bool Premake::outputModules(std::ostream &o)
					{
						// Find all the modules.
						std::vector<std::string> modules;
						for (const auto &target : targets())
							if (const auto mods = target.settings.data().getArray("modules"))
								for (const auto &m : *mods)
									if (m.string())
										modules.emplace_back(*m.string());

						if (modules.size() == 0) return true;

						for (const auto &m : modules)
						{
							boost::filesystem::path p{module().path()};
							p.append("premake");
							p.append(m + ".lua");
							p.make_preferred();
							
							if (!boost::filesystem::exists(p))
							{
								log::err() << "Premake module at '" << p.string() << "' does not exist";
								return false;
							}
							
							o << "dofile(\"" << p.string() << "\")\n";
						}

						o << '\n';
						
						return true;
					}
					
					bool Premake::outputWorkspace(std::ostream &o)
					{
						// The workspace contains generic settings for all the targets.
						outputString(o, 0, "workspace", identifier().proj());
						outputString(o, 1, "location", pathOutputAbs("generated"));
						
						boost::filesystem::path pathBuild(pathOutputAbs("build"));
						
						outputString(o, 1, "targetdir", pathTargetAbs("%{prj.name}"));
						outputString(o, 1, "objdir",    pathObjAbs("%{prj.name}"));
						
						outputStrings(o, 1, "configurations", {config::toString(config())});
						
						// Default options and flags.
						switch (config())
						{
							case config::Type::debug:
							{
								outputStrings(o, 1, "defines", {"DEBUG"});
								outputString(o, 1, "symbols", "On");
								break;
							}
							
							case config::Type::release:
							{
								outputString(o, 1, "optimize", "On");
								break;
							}
							
							case config::Type::invalid:
								break;
						}
						
						return true;
					}
					
					bool Premake::outputTarget(std::ostream &o, const DataTarget &target)
					{
						// Add some spacing between targets.
						o << '\n';

						// The "main" target gets the project name instead.
						outputString(o, 0, "project", nameTarget(target.name));

						// The output type (application or library).
						if (!outputTargetType(o, target)) return false;
						
						// The output name.
						if (!outputTargetBinname(o, target)) return false;
						
						// Include and exclude files.
						if (!outputTargetFiles(o, target)) return false;

						// Things to link against.
						if (!outputTargetLinks(o, target)) return false;
						
						// Defines.
						if (!outputTargetDefines(o, target)) return false;

						// Toolset?
						if (const auto toolset = target.settings.data().getString("toolset"))
							outputString(o, 1, "toolset", *toolset);

						// Toolset prefix?
						if (const auto toolset = target.settings.data().getString("toolprefix"))
							outputString(o, 1, "gccprefix", *toolset);
						
						// C++ build flags.
						if (!outputTargetOptionalStrings
						(
							o, target, *target.data.object(),
							"buildflags", "buildoptions"
						)) return false;
						
						// C++ linker flags.
						if (!outputTargetOptionalStrings
						(
							o, target, *target.data.object(),
							"linkflags", "linkoptions"
						)) return false;

						// Xcode-specific settings.
						if (!outputTargetOptionalKeysAndVals
						(
							o, target, target.settings.data(),
							"xcode", "xcodebuildsettings"
						)) return false;
						
						return true;
					}

					bool Premake::outputTargetType(std::ostream &o, const DataTarget &target)
					{
						if (ignores(target, "type")) return true;

						// Get the correct target type.
						const auto type(typeTarget(target));
						if (!type) return false;

						// Get the Premake value. Check overrides.
						std::string value;
						if (const auto over = target.settings.data().getString("premakekind"))
							value = *over;
						else
							value = premakeType(*type);

						outputString(o, 1, "kind", value);

						return true;
					}
					
					bool Premake::outputTargetBinname(std::ostream &o, const DataTarget &target)
					{
						if (ignores(target, "binname")) return true;

						if (const auto name = target.settings.data().getString("binname"))
							outputString(o, 1, "targetname", *name);
						
						return true;
					}
					
					bool Premake::outputTargetFiles(std::ostream &o, const DataTarget &target)
					{
						const auto root(rootTarget(target));
						if (!root) return false;

						/// @todo: Remove this if it is really not needed.
//						const auto includes(target.data.object()->getArray("includes"));
//						if (!includes || includes->count() == 0)
//						{
//							log::err() << "At least one include file has to be specified for the target.";
//							return false;
//						}
						
						// Include paths.
						std::vector<std::string> paths;

						// Always add Karhu.
						if (const auto karhupath = SDK::env::home("src"))
							paths.emplace_back(*karhupath);
						else
						{
							log::err()
								<< "Failed to find Karhu source directory. "
								<< "Make sure that the environment variable KARHU_HOME is set.";
							return false;
						}
						
						// Add the manually specified include paths.
						if (!ignores(target, "includedirs"))
							if (const auto dirs = target.data.object()->getArray("includedirs"))
								for (const auto &dir : *dirs)
									if (dir.string())
										paths.emplace_back(pathAbsFromRel(*root, *dir.string()));

						// Output!
						if (paths.size() > 0)
						{
							using namespace std::literals::string_literals;
							outputStrings(o, 1, "buildoptions", paths, [this, &root](const std::string &s)
							{
								boost::filesystem::path p{pathAbsFromRel(*root, s)};
								p = boost::filesystem::canonical(p);
								p.make_preferred();
								
								if (platform::is(module().platform(), Platform::mac | Platform::iOS | Platform::android))
									return "-I\\\""s + p.string() + "\\\"";
								else
									return "-I"s + p.string();
							});
						}

						// The Premake syntax is already used, so there is no need
						// to modify anything besides sticking the root in front.
						auto modify([this, &root](const std::string &s) { return pathAbsFromRel(*root, s); });
						
						if (!ignores(target, "includes"))
							if (!outputTargetOptionalStrings(o, target, *target.data.object(), "includes", "files", modify))
								return false;

						if (!ignores(target, "excludes"))
							if (!outputTargetOptionalStrings(o, target, *target.data.object(), "excludes", "excludes", modify))
								return false;

						return true;
					}
					
					bool Premake::outputTargetLinks(std::ostream &o, const DataTarget &target)
					{
						const auto root(rootTarget(target));
						if (!root) return false;
						
						// To hold the final values.
						std::vector<std::string> paths;
						std::vector<std::string> values;

						// Get links from module.
//						if (const auto slinks = target.settings.data().getArray("links"))
//							for (const auto &link : *slinks)
//								if (link.string())
//									values.emplace_back(*link.string());

						// Get link directories from module.
						if (!ignores(target, "linkdirs"))
						{
//							if (const auto sdirs = target.settings.data().getArray("linkdirs"))
//							{
//								for (const auto &dir : *sdirs)
//									if (dir.string())
//										paths.emplace_back(*dir.string());
//							}

							// Add the manually specified link paths.
							if (const auto dirs = target.data.object()->getArray("linkdirs"))
								for (const auto &dir : *dirs)
									if (dir.string())
										paths.emplace_back(*dir.string());
						}

						// Check all the target-specific links.
						if (const auto links = target.data.object()->getArray("links"))
						{
							for (const auto &link : *links)
							{
								if (link.string())
								{
									// Is it another target in the same project?
									const auto it(std::find_if
									(
										build()->targets().begin(),
										build()->targets().end(),
										[&link](const auto &v) { return (v.first == *link.string()); }
									));
									if (it != build()->targets().end())
									{
										paths.emplace_back(pathTargetAbs(nameTarget(*link.string())));
										values.emplace_back(*link.string());
									}
									// Is it Karhu?
									/// @todo: Maybe move this to module settings instead…
									else if (*link.string() == "karhu")
									{
										if (const auto path = SDK::env::home(pathTargetRel("lib/karhu", "karhu")))
										{
											paths.emplace_back(*path);
											values.emplace_back(*link.string());
										}
										else
										{
											log::err()
												<< "Path to Karhu binaries not found. "
												<< "Make sure that the environment variable KARHU_HOME has been set.";
											return false;
										}
									}
									// Anything else.
									else
										values.emplace_back(*link.string());
								}
							}
						}
						
						// Check if any paths were found.
						if (paths.size() > 0)
						{
							using namespace std::literals::string_literals;
							outputStrings(o, 1, "linkoptions", paths, [this, &root](const std::string &s)
							{
								if (platform::is(module().platform(), Platform::mac | Platform::iOS | Platform::android))
									return "-L\\\""s + pathAbsFromRel(*root, s) + "\\\"";
								else
									return "-L"s + pathAbsFromRel(*root, s);
							});
						}

						if (values.size() > 0)
							if (!ignores(target, "links"))
								outputStrings(o, 1, "links", values);

						return true;
					}
					
					bool Premake::outputTargetDefines(std::ostream &o, const DataTarget &target)
					{
						if (ignores(target, "defines")) return true;

						if (!outputTargetOptionalStrings(o, target, *target.data.object(), "defines", "defines", [this](const std::string &s)
						{
							using namespace std::literals::string_literals;
							
							if (platform::is(module().platform(), Platform::mac | Platform::iOS | Platform::android))
								return "\\\""s + string::escapeQuotesDouble<char>(s) + "\\\"";
							else
								return string::escapeQuotes<char>(s);
						}))
							return false;
						
						return true;
					}

					bool Premake::outputTargetOptionalStrings
					(
						std::ostream &o,
						const DataTarget &target,
						const conv::JSON::Obj &data,
						const std::string &element,
						const std::string &name,
						const std::function<std::string(const std::string &)> &modify
					)
					{
						if (ignores(target, element)) return true;

						const auto general(data.getArray(element));
						const auto extra  (data.findArray({config::toString(config()), element}));

						std::vector<std::string> v;

						// Add general.
						if (general)
							for (auto &i : *general)
								if (i.string()) /// @todo: Error if value not string?
									v.emplace_back(*i.string());

						// Add configuration-specific.
						if (extra)
							for (auto &i : *extra)
								if (i.string()) /// @todo: Error if value not string?
									v.emplace_back(*i.string());

						// Add some extra optimisations for release build.
						if (element == "buildflags" && config::Type::release == config())
							v.emplace_back("-ffast-math");

						// Remove all duplicate entries.
						std::sort(v.begin(), v.end());
						const auto cutoff(std::unique(v.begin(), v.end()));
						v.erase(cutoff, v.end());

						// Add any remaining.
						if (v.size() > 0)
							outputStrings(o, 1, name, v, modify);

						return true;
					}

					bool Premake::outputTargetOptionalKeysAndVals
					(
						std::ostream &o,
						const DataTarget &target,
						const conv::JSON::Obj &data,
						const std::string &element,
						const std::string &name
					)
					{
						if (ignores(target, element)) return true;

						const auto general(data.getObject(element));
						const auto extra  (data.findObject({config::toString(config()), element}));

						std::map<std::string, std::string> v;

						// Add general.
						if (general)
							for (auto &i : *general)
								if (i.second.string()) /// @todo: Error if value not string?
									v.emplace(i.first, *i.second.string()).first->second = *i.second.string();

						// Add configuration-specific.
						if (extra)
							for (auto &i : *extra)
								if (i.second.string()) /// @todo: Error if value not string?
									v.emplace(i.first, *i.second.string()).first->second = *i.second.string();

						// Add any remaining.
						if (v.size() > 0)
							outputKeysAndVals(o, 1, name, v);

						return true;
					}

					/**
					 * Converts a Karhu target type to a premake target type.
					 *
					 * @param s The Karhu type.
					 *
					 * @return The premake type.
					 */
					std::string Premake::premakeType(const std::string &s)
					{
						if      (s == "app")    return "WindowedApp";
						else if (s == "slib")   return "StaticLib";
						else if (s == "dlib")   return "SharedLib";
						else if (s == "module") return "SharedLib"; /// @todo: Change this conditionally based on config…
						
						return "";
					}
					
					std::string Premake::pathAbs(const std::string &subfolder)
					{
						boost::filesystem::path p{module().project()->location().path()};
						if (subfolder.length() > 0) p.append(subfolder);
						p.make_preferred();
						return p.string();
					}
					
					std::string Premake::pathOutputRel(const std::string &top, const std::string &subfolder)
					{
						boost::filesystem::path p{top};
						p.append(subfolder);
						p.append(karhu::platform::identifier(module().platform()));
						p.make_preferred();
						return p.string();
					}

					std::string Premake::pathOutputAbs(const std::string &subfolder)
					{
						return pathOutputRel(module().project()->location().path(), subfolder);
					}
					
					std::string Premake::pathTargetRel(const std::string &top, const std::string &target)
					{
						boost::filesystem::path p{pathOutputRel(top, "build")};
						p.append("bin");
						p.append(target);
						p.append(config::toString(config()));
						p.make_preferred();
						return p.string();
					}
					
					std::string Premake::pathTargetAbs(const std::string &target)
					{
						return pathTargetRel(module().project()->location().path(), target);
					}
					
					std::string Premake::pathObjRel(const std::string &top, const std::string &target)
					{
						boost::filesystem::path p{pathOutputRel(top, "build")};
						p.append("obj");
						p.append(target);
						p.append(config::toString(config()));
						p.make_preferred();
						return p.string();
					}
					
					std::string Premake::pathObjAbs(const std::string &target)
					{
						return pathObjRel(module().project()->location().path(), target);
					}

					/**
					 * Appends a call to a Premake function with a string argument
					 * to the specified stream at the specified indentation level.
					 *
					 * @param o     The stream to output to.
					 * @param level The indentation level.
					 * @param name  The function name.
					 * @param s     The string argument.
					 */
					void Premake::outputString
					(
						std::ostream &o,
						const std::size_t &level,
						const std::string &name,
						const std::string &s
					)
					{
						outputIndent(o, level);
						o << name << "(\"" << s << "\")\n";
					}
					
					void Premake::outputStrings
					(
						std::ostream &o,
						const std::size_t &level,
						const std::string &name,
						const std::vector<std::string> &s,
						const std::function<std::string(const std::string &)> &modify
					)
					{
						outputIndent(o, level);
						o << name << '\n';
						outputIndent(o, level);
						o << "{\n";
						for (std::size_t i{0}; i < s.size(); ++ i)
						{
							outputIndent(o, level + 1);
							o << '"' << modify(s[i]) << '"';
							if ((i + 1) < s.size()) o << ",";
							o << '\n';
						}
						outputIndent(o, level);
						o << "}\n";
					}

					void Premake::outputKeysAndVals
					(
						std::ostream &o,
						const std::size_t &level,
						const std::string &name,
						const std::map<std::string, std::string> &s
					)
					{
						outputIndent(o, level);
						o << name << '\n';
						outputIndent(o, level);
						o << "{\n";
						std::size_t i{0};
						for (const auto &p : s)
						{
							outputIndent(o, level + 1);
							o << "[\"" << p.first << "\"] = \"" << p.second << '"';
							if ((i + 1) < s.size()) o << ",";
							o << '\n';
							++ i;
						}
						outputIndent(o, level);
						o << "}\n";
					}
				}
			}
		}
	}
}
