/**
 * @author		Ava Skoog
 * @date		2017-08-31
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/file/build/Target.hpp>
#include <karhu/core/log.hpp>

#include <algorithm>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace file
			{
				namespace build
				{
					bool Target::valid() const
					{
						using T = JSON::TypeValue;

						// String, array or object. If any specific values/keys are specified,
						// those found in the build file have to correspond; otherwise anything goes.
						static const std::map<std::string, Option> options
						{
							{"appends",     {T::array}},
							{"includedirs", {T::array}},
							{"includes",    {T::array}},
							{"excludes",    {T::array}},
							{"links",       {T::array}},
							{"modules",     {T::array}},
							{"linkdirs",    {T::array}},
							{"buildflags",  {T::array}},
							{"linkflags",   {T::array}},
							{"defines",     {T::array}},
							{"root",        {T::string}},
							{"cpp",         {T::string, {"14"}}},
							{"type",        {T::string, {"app", "slib", "dlib", "module"}}}
						};

						if (m_data.object())
						{
							// Go through all the options specified in the build file.
							for (const auto &option : *m_data.object())
							{
								// Check if there is a corresponding option registered above.
								const auto it(options.find(option.first));
								if (it == options.end())
								{
									log::err() << '\'' << option.first << "' is not a valid build target option";
									return false;
								}
								// Make sure the types match.
								else if (option.second.type() != it->second.type)
								{
									log::err()
										<< "Option '" << option.first
										<< "' cannot be " << JSON::typeValueToString(option.second.type())
										<< "; must be " << JSON::typeValueToString(it->second.type)
										<< '!';
									return false;
								}
								// If any specific values or keys have been specified, there has to be a match.
								else if (it->second.valid.size() > 0)
								{
									const auto &valid(it->second.valid);
									
									// A string can only have one value.
									if (option.second.type() == T::string)
									{
										const auto jt(std::find(valid.begin(), valid.end(), *option.second.string()));
										if (jt == valid.end())
										{
											log::err()
												<< '\'' << *option.second.string()
												<< "' is not a valid value for the build target option '"
												<< option.first << "'";
											return false;
										}
									}
									// For arrays, we have to check every value.
									else if (option.second.type() == T::array)
									{
										bool found{false};
										std::stringstream keys;
										std::size_t i{0};
										for (const auto &v : *option.second.array())
										{
											if (v.string())
											{
												const auto jt(std::find(valid.begin(), valid.end(), *v.string()));
												if (jt != valid.end())
												{
													found = true;
													break;
												}
												else
												{
													// For the error message.
													keys << '\'' << *v.string() << '\'';
													if ((i + 2) < option.second.array()->count())
														keys << ", ";
													else if ((i + 1) < option.second.array()->count())
														keys << " or ";
												}
												
												++ i;
											}
										}
										
										if (!found)
										{
											log::err()
												<< keys.str() << " is not a valid value for the build target option '"
												<< option.first << "'";
											return false;
										}
									}
									// For objects, we have to check every key.
									else if (option.second.type() == T::object)
									{
										bool found{false};
										std::stringstream keys;
										std::size_t i{0};
										for (const auto &v : *option.second.object())
										{
											const auto jt(std::find(valid.begin(), valid.end(), v.first));
											if (jt != valid.end())
											{
												found = true;
												break;
											}
											else
											{
												// For the error message.
												keys << '\'' << v.first << '\'';
												if ((i + 2) < option.second.array()->count())
													keys << ", ";
												else if ((i + 1) < option.second.array()->count())
													keys << " or ";
											}

											++ i;
										}
										
										if (!found)
										{
											log::err()
												<< keys.str() << " is not a valid key for the build target option '"
												<< option.first << "'";
											return false;
										}
									}
								}
							}
						}

						return true;
					}
					
					/**
					 * Reads the conditionals and adds conditional options.
					 *
					 * @param platform The conditional platform.
					 * @param conf     The conditional configuration.
					 *
					 * @return The JSON data minus the conditionals objects plus any conditional data.
					 */
					Nullable<conv::JSON::Val> Target::data(const Platform &platform, const config::Type conf) const
					{
						// Get the data as is.
						conv::JSON::Val r{data()};
						
						if (!r.object()) return {true, r};

						// Find the appends.
						const auto it(std::find_if(r.object()->begin(), r.object()->end(), [](const auto &v)
						{
							return (v.first == "appends");
						}));
						
						// If there are no appends, we can just stop here.
						/// @todo: Check overrides too?
						if (it == r.object()->end())
							return {true, r};
						
						// Remove the conditionals from the copy.
						r.object()->erase(it);
						
						// Go through the conditionals from the original.
						for (const auto &append : *data().object()->getArray("appends"))
						{
							// There must be an object.
							if (!append.object())
							{
								log::err() << "Target appends must be object; cannot be " << append.typedump() << '!';
								return {};
							}

							// Find the filters.
							const auto filters(append.object()->getObject("filters"));
							if (!filters)
							{
								log::err() << "Target appends must have filters";
								return {};
							}
							
							// Check if the filters match.
							bool match{true};
							for (const auto &filter : *filters)
							{
								// Config is a special case.
								if (filter.first == "config")
								{
									if (!filter.second.string())
									{
										log::err() << "The config filter must take a string value; cannot be " << filter.second.typedump() << '!';
										return {};
									}
									
									const auto type(config::fromString(filter.second.string()->c_str()));
									if (config::Type::invalid == type)
									{
										log::err() << "Invalid config filter type '" << *filter.second.string() << "'";
										return {};
									}
									
									if (conf != type)
									{
										match = false;
										break;
									}
								}
								// Systems another special case.
								else if (filter.first == "systems")
								{
									if (!filter.second.array())
									{
										log::err() << "Filter value must be array; cannot be" << filter.second.typedump() << '!';
										return {};
									}
									
									bool found{false};
									
									for (const auto &system : *filter.second.array())
									{
										if (system.string() && *system.string() == platform::identifier(platform))
										{
											found = true;
											break;
										}
									}
									
									if (!found)
									{
										match = false;
										break;
									}
								}
							}

							// If there is a match, start appending!
							if (match)
							{
								for (const auto &kv : *append.object())
								{
									// Append all non-filter values.
									if (kv.first != "filters")
									{
										if (!kv.second.array())
										{
											log::err() << "Append value must be array; cannot be " << kv.second.typedump() << '!';
											return {};
										}
										
										auto arr(r.object()->getArray(kv.first));
										if (!arr)
											r.object()->push({kv.first, kv.second});
										else
											for (const auto &v : *kv.second.array())
												if (v.string())
													arr->push(v);
										
										/// @todo: Remove doubles?
									}
								}
							}
						}
						
						log::msg("DATAFILTER") << r.dump(); /// @todo: Remove this when done testing.
						return {true, r};
					}
				}
			}
		}
	}
}
