/**
 * @author		Ava Skoog
 * @date		2017-04-06
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/file/build/Config.hpp>
#include <karhu/tool/proj/file/build/Module.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

#include <algorithm>
#include <fstream>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace file
			{
				namespace build
				{
					Config::Config
					(
						Module &module,
						const config::Type type,
						const std::string &target,
						const conv::JSON::Val &extrasettings,
						const std::string &hint
					)
					:
					m_module       {module},
					m_type         {type},
					m_target       {target},
					m_extrasettings{extrasettings},
					m_hint         {hint}
					{
					}

					bool Config::output(const conv::JSON::Arr &settings)
					{
						setUpFolders();
						return performOutput(settings);
					}
					
					bool Config::loadProjectFundamentals()
					{
						auto *p(m_module.project());
						if (!p) return false;

						const auto info(p->info());
						if (!info)
						{
							log::err() << "Failed to load project information";
							return false;
						}

						const auto identifier(info->identifier());
						if (!identifier)
						{
							log::err() << "Project identifier missing in information file";
							return false;
						}
						
						m_identifier = *identifier;

						const auto name(info->name());
						if (!name)
						{
							log::err() << "Project name missing in information file";
							return false;
						}
						
						m_name = *name;
						
						// We need project-level build settings.
						const auto build(p->build());
						if (!build)
						{
							log::err() << "Failed to load project build settings";
							return false;
						}

						m_build = &*build;

						return true;
					}
					
					bool Config::ignores(const DataTarget &target, const std::string &key) const
					{
						if (const auto ignores = target.settings.data().getArray("ignores"))
							for (const auto &ignore : *ignores)
								if (ignore.string() && *ignore.string() == key)
									return true;

						return false;
					}

					std::string Config::nameTarget(const std::string &name) const
					{
						return (name == "main") ? identifier().proj() : name;
					}
					
					std::string Config::pathAbsFromRel(const std::string &root, const std::string &path) const
					{
						boost::filesystem::path p{path};
						p.make_preferred();

						if (p.string().length() > 0)
						{
							// Boost does not register as relative if there is a dollar sign
							// (for CMake variables, for example), so we need to check this manually.
							if (p.is_relative() && p.string()[0] != '$')
							{
								p = boost::filesystem::path{module().project()->location().path()};
								p.append(root);
								
								if (path != ".")
									p.append(path);

								p.make_preferred();
							}
						}
						
						return p.string();
					}

					Nullable<std::string> Config::typeTarget(const DataTarget &target) const
					{
						if (const auto r = target.data.object()->getString("type"))
							return {true, *r};
						else
							log::err() << "Required option 'type' missing from target '" << target.name << "'";

						return {};
					}
					
					Nullable<std::string> Config::rootTarget(const DataTarget &target) const
					{
						if (const auto r = target.data.object()->getString("root"))
							return {true, *r};
						else
							log::err() << "Required option 'root' missing in target '" << target.name << "'";

						return {};
					}
					
					bool Config::findAndOrderDependentTargets()
					{
						const auto &targets(build()->targets());
						
						const auto it(std::find_if(targets.begin(), targets.end(), [this](const auto &v)
						{
							return (v.first == m_target);
						}));
						if (it == targets.end())
						{
							log::err() << "Target '" << m_target << "' is missing";
							return false;
						}
						
						auto validateTarget([this](const auto &iterator)
						{
							// An object has to be present with the actual settings.
							const auto object(iterator->second.data().object());
							if (!object)
							{
								log::err()
									<< "Target '" << iterator->first
									<< "' must be an object; cannot be "
									<< iterator->second.data().typedump() << '!';
								return false;
							}

							return true;
						});
						
						if (!validateTarget(it)) return false;

						// Find the settings filtered for this target's settings.
						auto maindata(getDataForTarget(it->first, it->second));
						if (!maindata) return false;
						
						const auto maintype(maindata->data.object()->getString("type"));
						if (!maintype)
						{
							log::err()
								<< "Required option 'type' missing for target '" << m_target << "'";
							return false;
						}
						
						// Find all the links.
						auto findDependencies([this, &maindata, &maintype, &targets, &validateTarget]
						(
							const std::string &element,
							const std::vector<std::string> &types,
							const bool maintypeHasToBeApp
						)
						{
							if (const auto values = maindata->dataFull.object()->getArray(element))
							{
								if (maintypeHasToBeApp && *maintype != "app")
								{
									log::err()
										<< "Target '" << m_target << "' cannot specify '"
										<< element << "' option because its type is not 'app'";
									return false;
								}

								for (const auto &value : *values)
								{
									if (value.string())
									{
										// Obey the rules.
										if (*value.string() == m_target)
										{
											log::err() << "Target '" << m_target << "' cannot specify itself in '" << element << "' option";
											return false;
										}
										else if (*value.string() == "main") /// @todo: Add 'test' and such.
										{
											log::err() << "Target '" << m_target << "' cannot specify 'main' in '" << element << "' option";
											return false;
										}

										// Find references to targets belonging to the same project.
										const auto jt(std::find_if(targets.begin(), targets.end(), [&value](const auto &v)
										{
											return (v.first == *value.string());
										}));
										if (jt != targets.end())
										{
											if (!validateTarget(jt)) return false;

											auto data(getDataForTarget(jt->first, jt->second));
											if (!data) return false;

											// Make sure that the target is of a valid type.
											const auto type(data->data.object()->getString("type"));
											if (type)
											{
												if (std::find(types.begin(), types.end(), *type) == types.end())
												{
													log::err()
														<< "Target '" << m_target << "' cannot specify '" << jt->first
														<< "' in '" << element << "' option because '" << jt->first
														<< "' has the wrong type";
													return false;
												}
											}
											else
											{
												log::err()
													<< "Required option 'type' missing for target '"
													<< jt->first << "' specified in '" << element
													<< "' for target '" << m_target << "'";
												return false;
											}
											
											// Save the target to the list!
											m_targets.emplace_back(std::move(*data));
											
											// Do link-specific things.
											/// @todo: Add to full data as well?
											if (*type != "module")
											{
												// Ignore changes to kind induced by build module,
												// because we want all the dependent links to be static.
												auto ignores(data->settings.data().getArray("ignores"));
												if (!ignores) ignores = data->settings.data().push({"ignores", conv::JSON::Arr{}}).array();
												ignores->push("cmakekind");

												// Write the target to the list of the main target's dependencies.
												/// @todo: Add to data (non-full) as well?
												auto dependencies(maindata->dataFull.object()->getArray("dependencies"));
												if (!dependencies) dependencies = maindata->dataFull.object()->push({"dependencies", conv::JSON::Arr{}}).array();
												dependencies->push(jt->first);

												// Add the link directory.
												boost::filesystem::path p{module().project()->location().path()};
												p.append(platform::identifier(module().platform()));
												p.append(relativePathToProduct(nameTarget(*value.string())));
												p.make_preferred();

												auto links(maindata->data.object()->getArray("linkdirs"));
												if (!links) links = maindata->data.object()->push({"linkdirs", conv::JSON::Arr{}}).array();
												links->push(p.string());
											}
										}
									}
								}
							}
							
							return true;
						});
						
						if (!findDependencies("links",   {"slib", "dlib"}, false)) return false;
						if (!findDependencies("modules", {"module"},       true))  return false;
						
						// Add the primary target last.
						m_targets.emplace_back(std::move(*maindata));
						
						/// @todo: Order the dependent targets by recursive dependency?
						/// @todo: Prevent circular dependencies?
						
						return true;
					}

					bool Config::outputDataTarget(const DataTarget &target)
					{
						// Create the data file with the final values.
						boost::filesystem::path p{module().project()->location().path()};
						p.append("generated");
						p.append(platform::identifier(module().platform()));
						p.append("data");
						p.append(nameTarget(target.name) + ".json");
						std::ofstream od{p.string(), std::ios::trunc};

						if (od.is_open())
							od << target.dataFull.dump();
						else
						{
							log::err() << "Failed to open file '" << p.string() << "' for writing";
							return false;
						}

						return true;
					}
					
					void Config::outputIndent(std::ostream &o, const std::size_t &level)
					{
						for (std::size_t i{0}; i < level; ++ i) o << '\t';
					}

					Nullable<Config::DataTarget> Config::getDataForTarget
					(
						const std::string &name,
						const Target &target
					) const
					{
						using namespace std::literals::string_literals;

						using T = JSON::TypeValue;

						if (const auto settings = m_module.findSettingsForTarget(name, nameTarget(name), target, m_type, m_extrasettings, hint()))
						{
							const auto data(target.data(module().platform(), config()));
							if (!data) return {};

							if (data->object())
							{
								// Make a copy of the entire target.
								conv::JSON::Obj object{*data->object()};
								
								// Add the target name and identifier if this is the main target of an app.
								if (name == "main")
								{
									if (const auto type = object.getString("type"))
									{
										if (*type == "app")
										{
											auto defines(object.getArray("defines"));
											if (!defines) defines = object.push({"defines", conv::JSON::Arr{}}).array();
											
											/// @todo: Abstract this sort of stuff into Target class?
											// Add the name.
											defines->push("KARHU_APPLICATION_NAME=\""s + this->name() + '"');
											
											// Add the identifier.
											defines->push("KARHU_IDENTIFIER_EXT=\""s + identifier().ext() + '"');
											defines->push("KARHU_IDENTIFIER_ORG=\""s + identifier().org() + '"');
											defines->push("KARHU_IDENTIFIER_PROJ=\""s + identifier().proj() + '"');
										}
									}
								}

								// Sort out the overrides.
								if (const auto overrides = settings->data().getObject("overrides"))
								{
									for (const auto &o : *overrides)
									{
										// Find a corresponding value in the target.
										const auto tval(object.getString(o.first));
										if (!tval)
											object.push({o.first, o.second});
										// If it is a string, it will always replace the target's value.
										else if (T::string == o.second.type())
											*tval = *o.second.string();
										// Otherwise an object of conditional keys has to be checked.
										else if (T::object == o.second.type())
										{
											for (const auto &val : *o.second.object())
											{
												if (val.first == *tval && val.second.string())
												{
													*tval = *val.second.string();
													break;
												}
											}
										}
									}
								}

								// Sort out the appeds.
								if (const auto appends = settings->data().getObject("appends"))
								{
									for (const auto &o : *appends)
									{
										// Find a corresponding value in the target.
										const auto tval(object.get(o.first));
										if (!tval)
											object.push({o.first, o.second});
										// Check that the types match.
										else if (tval->type() == o.second.type())
										{
											// Go through all the entries and add or override them.
											if (T::array == o.second.type())
											{
												tval->array()->prepend(*o.second.array());
												
												/// @todo: Remove doubles?
											}
											else if (T::object == o.second.type())
											{
												for (const auto &val : *o.second.object())
													tval->object()->set(val.first, val.second);
											}
										}
										else
										{
											log::err()
												<< "Type of data given for 'appends' for key '"
												<< o.first << "' (" << o.second.typedump()
												<< ") does not match type of corresponding key in target ("
												<< tval->typedump() << ")";
											return {};
										}
									}
								}
								
								// Copy into the full data.
								auto full(object);

								// Remove the ignores.
								if (const auto ignores = settings->data().getArray("ignores"))
								{
									for (const auto &o : *ignores)
									{
										if (o.string())
										{
											// Find a corresponding value in the target.
											const auto it(std::find_if(object.begin(), object.end(), [&o](const auto &v)
											{
												return (v.first == *o.string());
											}));

											// Remove it.
											if (it != object.end())
												object.erase(it);
										}
									}
								}

								return {true, name, std::move(*settings), std::move(object), std::move(full)};
							}
						}

						return {};
					}
					
					void Config::setUpFolders()
					{
						namespace fs = boost::filesystem;

						// Get the basepath to the project.
						fs::path p{module().project()->location().path()};
						
						// Make sure the generated folder exists.
						p.append("generated");
						if (!fs::exists(p)) fs::create_directory(p); /// @todo: Error-check?
						
						p.append(platform::identifier(module().platform()));
						if (!fs::exists(p)) fs::create_directory(p); /// @todo: Error-check?

						// Make sure the data folder exists.
						p.append("data");
						if (!fs::exists(p)) fs::create_directory(p); /// @todo: Error-check?
					}
					
					Nullable<std::vector<std::string>> Config::findTargetFiles(const DataTarget &target) const
					{
						namespace fs = boost::filesystem;

						std::vector<std::string> files;

						const auto root(rootTarget(target));
						if (!root) return {};
						
						fs::path rootFull{module().project()->location().path()};
						rootFull.append(*root);
						rootFull.make_preferred();
						
						if (const auto includes = target.data.object()->getArray("includes"))
						{
							for (const auto &i : *includes)
							{
								if (i.string())
								{
									if (i.string()->length() == 0 || *i.string() == "**" || *i.string() == "*")
									{
										log::err() << "File path cannot be empty or consist exclusively of a wildcard";
										return {};
									}

									auto tokens(splitPath(*i.string()));
									auto action([&files](const std::string &f)
									{
										files.emplace_back(f);
									});
									if (!recurseTargetFiles((fs::path{*i.string()}.is_relative()) ? rootFull.string() : "", tokens, action))
										return false;
								}
							}

							if (files.size() > 0)
							{
								if (const auto excludes = target.data.object()->getArray("excludes"))
								{
									for (const auto &e : *excludes)
									{
										if (e.string())
										{
											auto tokens(splitPath(*e.string()));
											auto action([&files](const std::string &f)
											{
												const auto it(std::find(files.begin(), files.end(), f));
												if (it != files.end()) files.erase(it);
											});
											if (!recurseTargetFiles((fs::path{*e.string()}.is_relative()) ? rootFull.string() : "", tokens, action))
												return false;
										}
									}
								}
							}
						}

						/// @todo: Remove this if it is really not needed.
//						if (files.size() == 0)
//						{
//							log::err() << "Target '" << target.name << "' must include at least one source file";
//							return {};
//						}
						
						return {true, std::move(files)};
					}
					
					bool Config::recurseTargetFiles
					(
						const std::string &root,
						std::vector<std::string> tokens,
						const std::function<void(const std::string &)> &action
					) const
					{
						namespace fs = boost::filesystem;
						
						fs::path p{root};
						
						if (tokens.size() > 0)
						{
							const auto token(tokens[0]);

							if (token == "*" || token == "**")
							{
								if (tokens.size() == 1 || (tokens.size() == 2 && token[1] == '/'))
								{
									log::err() << "Source file cannot be directory";
									return false;
								}
							}
							
							// If infinite recursion, don't stop.
							if (token != "**") tokens.erase(tokens.begin());

							if (token == "*")
							{
								for (fs::directory_iterator file(p); file != fs::directory_iterator(); ++ file)
								{
									try
									{
										fs::path current(file->path());

										// File?
										if (tokens.size() == 0)
										{
											const auto &f(current.string());
											const auto &t(token);

											if (f.length() > t.length() && f.substr(f.length() - t.length(), t.length()) == t)
												action(current.string());
										}
										// Directory?
										else if (fs::is_directory(current) && tokens[0] == current.filename().string())
											return recurseTargetFiles(current.string(), tokens, action);
									}
									catch (const fs::filesystem_error &e)
									{
										return true;
									}
								}
							}
							else if (token == "**")
							{
								for (fs::directory_iterator file(p); file != fs::directory_iterator(); ++ file)
								{
									try
									{
										fs::path current(file->path());
										
										// Directory?
										if (fs::is_directory(current))
										{
											if (tokens[1] == current.filename().string())
												tokens.erase(tokens.begin());

											if (!recurseTargetFiles(current.string(), tokens, action))
												return false;
										}
										// File?
										else if (tokens.size() == 2)
										{
											const auto &f(current.string());
											const auto &t(tokens[1]);

											if (f.length() > t.length() && f.substr(f.length() - t.length(), t.length()) == t)
												action(current.string());
										}
									}
									catch (const fs::filesystem_error &e)
									{
										return true;
									}
								}
							}
							else
							{
								log::msg() << "checking token " << token;
								p.append(token);
								p.make_preferred();
								if (fs::exists(p))
								{
									log::msg() << "exists";
									if (tokens.size() == 0)
										action(p.string());
									else
										return recurseTargetFiles(p.string(), tokens, action);
								}else
									log::msg() << "exists NOT";
							}
						}
						
						return true;
					}
					
					std::vector<std::string> Config::splitPath(const std::string &path) const
					{
						std::vector<std::string> r;
						std::stringstream token;

						// Split all the directories.
						for (std::size_t i{0}; i < path.length(); ++ i)
						{
							const auto &c(path[i]);

							if (c == '*')
							{
								if (token.str().length() > 0)
								{
									r.emplace_back(token.str());
									token.str("");
								}
								
								if ((i + 1) < path.length())
								{
									if (path[i + 1] == '*')
									{
										++ i;
										r.emplace_back("**");
									}
									else
										r.emplace_back("*");
								}
							}
							else
								token << c;
						}

						if (token.str().length() > 0)
							r.emplace_back(token.str());
						
						return r;
					}
				}
			}
		}
	}
}
