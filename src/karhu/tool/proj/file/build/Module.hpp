/**
 * @author		Ava Skoog
 * @date		2017-08-18
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_FILE_BUILD_MODULE_H_
	#define KARHU_TOOL_PROJ_FILE_BUILD_MODULE_H_

	#include <karhu/tool/proj/file/build/Settings.hpp>
	#include <karhu/tool/proj/file/build/common.hpp>
	#include <karhu/tool/proj/file/build/Config.hpp>
	#include <karhu/tool/proj/file/Project.hpp>
	#include <karhu/tool/proj/file/DataJSON.hpp>
	#include <karhu/conv/types.hpp>
	#include <karhu/core/Nullable.hpp>
	#include <karhu/core/platform.hpp>

	#include <unordered_map>
	#include <string>
	#include <vector>
	#include <sstream>
	#include <cstdint>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					namespace build
					{
						class Target;

						/**
						 * Interfaces a build target module under the targets/ subfolder of the
						 * Karhu home root folder, reads its settings, and outputs build files
						 * (like Premake or CMake) for use by the build system depending on the
						 * target system's needs.
						 *
						 * Cannot be created directly for validation reasons; use loadForPlatform().
						 */
						class Module
						{
							public:
								struct Option
								{
									karhu::JSON::TypeValue type{karhu::JSON::TypeValue::string};
									std::vector<std::string> valid;
								};

							private:
								using SettingsOrdered = std::multimap
								<
									std::uint8_t,
									const conv::JSON::Val
								>;
							
								using SettingsFiltered = std::map<std::string, SettingsOrdered>;

							private:
								enum class TypeOutput : std::uint8_t
								{
									premake,
									cmake
								};
							
								class Parser
								{
									public:
										Parser
										(
											const Module &p,
											std::string &buffer,
											const config::Type conf,
											const std::string &name,
											const Target &target
										)
										:
										m_parent{p},
										m_buffer{buffer},
										m_conf  {conf},
										m_name  {name},
										m_target{target}
										{
										}
									
										bool parseVars();
									
									private:
										bool parseVar();
										bool replaceVar();
										bool replaceFromValue();
										bool replaceFromFile();
										void replace(const std::string &);

										char &get() { return m_buffer[m_index ++]; }
										char &peek() const { return m_buffer[m_index]; }
										bool done() const { return (m_index >= m_buffer.size()); }

									private:
										const Module &m_parent;
										std::string &m_buffer;
										config::Type m_conf;
										const std::string &m_name;
										const Target &m_target;

										std::size_t m_start{0}, m_index{0};
										std::stringstream m_stream;
									
										std::vector<std::string> m_parts;
								};

							public:
								static Nullable<Module> loadForPlatform(const Platform &);
							
							public:
								Module(const Module &) = default;
								Module(Module &&) = default;
								Module &operator=(const Module &) = default;
								Module &operator=(Module &&) = default;

								Project *project() { return m_project; }
								const Project *project() const { return m_project; }
								const Platform &platform() const noexcept { return m_platform; }
								const conv::JSON::Val &settings() const noexcept { return m_root; }
								const std::string &path() const noexcept { return m_path; }
								const std::string &subpath() const noexcept { return m_subpath; }

								bool outputBuildfiles
								(
									Project &project,
									const config::Type conf,
									const std::string &target,
									const conv::JSON::Val &extrasettings = {},
									const std::string &hint = {}
								);
							
								std::unique_ptr<Config> getConfig
								(
									Project &project,
									const config::Type conf,
									const std::string &target,
									const conv::JSON::Val &extrasettings = {},
									const std::string &hint = {}
								);

								const conv::JSON::Val *getProjectValue
								(
									const Target &target,
									const config::Type conf,
									const std::string &key
								) const;

								Nullable<Settings> findSettingsForTarget
								(
									const std::string &name,
									const std::string &nameFinal,
									const Target &target,
									const config::Type conf,
									const conv::JSON::Val &extrasettings = {},
									const std::string &hint = {}
								) const;

							private:
								Module() = default;
							
								Module
								(
									conv::JSON::Val &&root,
									const Platform &platform,
									const std::string &path,
									const std::string &subpath
								)
								:
								m_platform{platform},
								m_root    {std::move(root)},
								m_path    {path},
								m_subpath {subpath}
								{
								}

								bool outputBuildfiles
								(
									const TypeOutput,
									const config::Type conf,
									const std::string &target,
									const conv::JSON::Val &extrasettings = {},
									const std::string &hint = {}
								);

								bool replaceVariables
								(
									conv::JSON::Val &v,
									const config::Type conf,
									const std::string &name,
									const Target &target
								) const;
								DataJSON *loadFile(const std::string &) const;
								conv::JSON::Arr *arrayJSONSettings();
							
								void pushOptionAtPriority
								(
									SettingsFiltered &container,
									const std::string &key,
									const std::uint8_t priority,
									const conv::JSON::Val &value
								) const;

							private:
								Project *m_project{nullptr};
								Platform m_platform;
								conv::JSON::Val m_root{conv::JSON::Obj{}};
								std::string m_path, m_subpath;

								mutable std::unordered_map<std::string, DataJSON> m_files;

							friend class Parser;
							friend class Nullable<Module>;
						};
					}
				}
			}
		}
	}
#endif
