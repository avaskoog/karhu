/**
 * @author		Ava Skoog
 * @date		2017-08-19
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_FILE_DATA_H_
	#define KARHU_TOOL_PROJ_FILE_DATA_H_

	#include <karhu/tool/proj/Location.hpp>
	#include <karhu/core/Nullable.hpp>

	#include <vector>
	#include <string>
	#include <fstream>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					class Project;
					
					/**
					 * Interfaces a data file in the project structure that uses
					 * some underlying key-value format, and offers a way for
					 * derived classes to implement it for particular formats
					 * (such as JSON or XML).
					 *
					 * All values are in the form of strings.
					 */
					class Data
					{
						public:
							/**
							 * Constructs the data with a default location and an invalid
							 * filename. This constructor only exists so that uninitialised
							 * data handles can exist in vectors and so on; if used directly,
							 * the data will not have anywhere to load from or save to.
							 */
							Data() = default;

							// Main constructor.
							Data(const Location &location, const std::string &file);

							// Defaults.
							Data(const Data &) = default;
							Data(Data &&) = default;
							Data &operator=(const Data &) = default;
							Data &operator=(Data &&) = default;

							// File I/O.
							bool load();
							bool save();

							/**
							 * Sets the top-level value by the specified key, creating
							 * it if it does not already exist.
							 *
							 * @param key   The name of the value.
							 * @param value The string value to write.
							 */
							void set(const std::string &key, const std::string &value)
							{
								performSet(key, value);
							}

							/**
							 * Returns the top-level value by the specified key if it exists.
							 *
							 * @param key The name of the value.
							 *
							 * @return A nullable containing the value if it exists, or null.
							 */
							Nullable<std::string> get(const std::string &key) const
							{
								return performGet(key);
							}

							/**
							 * Sets the value by the specified key at the end of the specified
							 * hierarchy, creating it if it does not already exist.
							 *
							 * For example, if the value is one level below the root, two names
							 * may be specified in the list of keys: one for the parent element,
							 * and one for the key of the value within that element.
							 *
							 * @param key   A list describing the hierarchy leading up to the value and the last element being the key.
							 * @param value The string value to write.
							 */
							void set(const std::vector<std::string> &key, const std::string &value)
							{
								performSet(key, value);
							}

							/**
							 * Returns the value by the specified key at the end of the specified
							 * hierarchy if it exists.
							 *
							 * For example, if the value is one level below the root, two names
							 * may be specified in the list of keys: one for the parent element,
							 * and one for the key of the value within that element.
							 *
							 * @param key A list describing the hierarchy leading up to the value and the last element being the key.
							 *
							 * @return A nullable containing the value if it exists, or null.
							 */
							Nullable<std::string> get(const std::vector<std::string> &key) const
							{
								return performGet(key);
							}
						
							void set(std::initializer_list<const char *> key, const std::string &value)
							{
								set(std::vector<std::string>{key.begin(), key.end()}, value);
							}
						
							Nullable<std::string> get(std::initializer_list<const char *> key) const
							{
								return get(std::vector<std::string>{key.begin(), key.end()});
							}

							virtual ~Data() {}

						protected:
							/**
							 * Optional method that can be implemented to set up defaults for
							 * values that are expected in the file. Will be called after loading
							 * the file, so it is necessary to make sure that the values do not
							 * already exists before attempting to write them, or the values
							 * loaded from file will be overwritten.
							 *
							 * Also recommended to call in constructor so that an unloaded file
							 * still has the correct values.
							 */
							virtual void setUpDefaults() {}

							virtual void performSet(const std::string &key, const std::string &value) = 0;
							virtual void performSet(const std::vector<std::string> &key, const std::string &value) = 0;
							virtual Nullable<std::string> performGet(const std::string &key) const = 0;
							virtual Nullable<std::string> performGet(const std::vector<std::string> &key) const = 0;

							/**
							 * Implements the reading of the data from the input filstream
							 * specified, which has already been validated and opened.
							 *
							 * @param f The input filestream to read from.
							 *
							 * @return Whether the operation was successful.
							 */
							virtual bool performLoad(std::ifstream &f) = 0;
						
							/**
							 * Implements the writing of the data to the output filstream
							 * specified, which has already been validated and opened.
							 *
							 * @param f The output filestream to write to.
							 *
							 * @return Whether the operation was successful.
							 */
							virtual bool performSave(std::ofstream &f) = 0;
						
							virtual bool afterLoad()  { return true; }
							virtual bool beforeSave() { return true; }

						protected:
							Location    m_location;
							std::string m_file;
					};
				}
			}
		}
	}
#endif
