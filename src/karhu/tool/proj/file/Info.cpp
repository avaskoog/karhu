/**
 * @author		Ava Skoog
 * @date		2017-04-04
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/file/Info.hpp>
#include <karhu/tool/proj/file/Project.hpp>
#include <karhu/tool/SDK.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace file
			{
				Info::Info(const Location &location)
				:
				DataJSON{location, SDK::paths::project::info()} /// @todo: Move string out of SDK header and in here?
				{
					// Default empty document.
					setUpDefaults();
				}

				void Info::templatename(const std::string &value)
				{
					return set("template", value);
				}
				
				Nullable<std::string> Info::templatename() const
				{
					return get("template");
				}

				void Info::name(const std::string &value)
				{
					return set("name", value);
				}
				
				Nullable<std::string> Info::name() const
				{
					return get("name");
				}
				
				void Info::identifier(const Identifier &value)
				{
					set({"id", "ext"},  value.ext ());
					set({"id", "org"},  value.org ());
					set({"id", "proj"}, value.proj());
				}
				
				Nullable<Identifier> Info::identifier() const
				{
					if (const auto ID = rootJSON().object()->get("id"))
					{
						if (ID->object())
						{
							const auto ext (ID->object()->get("ext"));
							const auto org (ID->object()->get("org"));
							const auto proj(ID->object()->get("proj"));

							if (ext && org && proj && ext->string() && org->string() && proj->string())
								return Identifier::create(*ext->string(), *org->string(), *proj->string());
						}
					}

					return {false, Identifier::defaults()};
				}
				
				void Info::setUpDefaults()
				{
					if (!name())       name(SDK::values::project::defaulttemplate());
					if (!name())       name(SDK::values::project::defaultname());
					if (!identifier()) identifier(Identifier::defaults());
				}
			}
		}
	}
}
