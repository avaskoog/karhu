/**
 * @author		Ava Skoog
 * @date		2017-04-09
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/file/Project.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

#include <fstream>

namespace
{
	static bool copyDir(const boost::filesystem::path &source, const boost::filesystem::path &destination);
	
	template<typename T>
	T *tryLoad(std::unique_ptr<T> &pointer, const karhu::tool::proj::Location &location)
	{
		if (!pointer)
		{
			pointer = std::make_unique<T>(location);

			if (!pointer->load())
				pointer.reset();
		}

		return pointer.get();
	}
}

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace file
			{
				Nullable<Project> Project::load(const Location &path)
				{
					if (!path.valid()) return {};

					{
						boost::filesystem::path p{path.path()};
						p.append(SDK::paths::project::info());
						p.make_preferred();

						if (!boost::filesystem::exists(p)) return {};
					}

					return {true, Identifier::defaults(), path};
				}

				Nullable<Project> Project::create(const Identifier &identifier, const std::string &folder, const Location &path)
				{
					Project p{identifier, path};

					if (p.create(folder))
						return {true, std::move(p)};
					
					return {};
				}
				
				bool Project::create(const std::string &folder)
				{
					if (!validateBasedir()) return false;
					
					if (const auto templatedir = SDK::env::home(SDK::paths::home::project::templatedir(folder)))
					{
						if (!boost::filesystem::exists(*templatedir))
						{
							log::err() << "Invalid project template '" << folder << "'.";
							return false;
						}
						
						boost::filesystem::path source{*templatedir};
						boost::filesystem::path dest  {subdir()};

						if (copyDir(source, dest))
						{
							m_location = {dest.string()};
							
							{
								auto s(dest), d(dest);
								s.append("gitignore");
								d.append(".gitignore");
								s.make_preferred();
								d.make_preferred();
								boost::filesystem::rename(s, d);
							}
							
							return true;
						}
					}
					
					return false;
				}
				
				Info *Project::info()
				{
					return tryLoad(m_info, m_location);
				}
				
				build::Build *Project::build()
				{
					return tryLoad(m_build, m_location);
				}
				
				bool Project::validateBasedir() const
				{
					return m_location.valid();
				}
				
				bool Project::validateSubdir() const
				{
					if (!validateBasedir()) return false;
					
					if (!boost::filesystem::exists(subdir()))
						return false;
					
					return true;
				}
				
				std::string Project::subdir() const
				{
					boost::filesystem::path p{m_location.path()};
					p.append(m_identifier.proj());
					p.make_preferred();

					return p.string();
				}
				
				Result<std::string> Project::AdapterRead::readStringFromResource
				(
					const char *filepathRelativeToResourcepath
				)
				{
					namespace fs = boost::filesystem;
					
					fs::path p{m_project.location().path()};
					p.append("res"); /// @todo: Should "res" be put into some SDK constant instead of hardcoding everywhere?
					p.append(filepathRelativeToResourcepath);
					p.make_preferred();
					
					std::ifstream f{p.string()};
					if (!f.is_open())
					{
						std::stringstream s;
						s
							<< "Could not open file '"
							<< p.string()
							<< "' for reading.";
						return {{}, false, s.str()};
					}
					
					std::stringstream s;
					s << f.rdbuf();
					
					return {s.str(), true};
				}

				Result<Buffer<Byte>> Project::AdapterRead::readBytesFromResource
				(
					const char *filepathRelativeToResourcepath
				)
				{
					namespace fs = boost::filesystem;
					
					fs::path p{m_project.location().path()};
					p.append("res"); /// @todo: Should "res" be put into some SDK constant instead of hardcoding everywhere?
					p.append(filepathRelativeToResourcepath);
					p.make_preferred();
					
					std::ifstream f{p.string(), std::ios_base::binary};
					if (!f.is_open())
					{
						std::stringstream s;
						s
							<< "Could not open file '"
							<< p.string()
							<< "' for reading.";
						return {{}, false, s.str()};
					}
					
					Buffer<Byte> buffer;
					
					f.seekg(0, f.end);
					buffer.size = static_cast<std::size_t>(f.tellg());
					buffer.data = std::make_unique<Byte[]>(buffer.size);
					
					f.seekg(0, f.beg);
					f.read
					(
						reinterpret_cast<char *>(buffer.data.get()),
						static_cast<std::streamsize>(buffer.size)
					);
					
					return {std::move(buffer), true};
				}
				
				Status Project::AdapterWrite::writeStringToResource
				(
					char                      const *filepathRelativeToResourcepath,
					char                      const *data,
					conv::ModeFilesystemWrite const  mode
				)
				{
					namespace fs = boost::filesystem;
					
					fs::path p{m_project.location().path()};
					p.append("res"); /// @todo: Should "res" be put into some SDK constant instead of hardcoding everywhere?
					p.append(filepathRelativeToResourcepath);
					p.make_preferred();
					
					std::ofstream f
					{
						p.string(),
						(
							(conv::ModeFilesystemWrite::truncate == mode)
							?
								std::ios_base::trunc
							:
								std::ios_base::app
						)
					};
					
					if (!f.is_open())
					{
						std::stringstream s;
						s
							<< "Could not open file '"
							<< p.string()
							<< "' for writing.";
						return {false, s.str()};
					}
					
					f << data;
					
					return {true};
				}

				Status Project::AdapterWrite::writeBytesToResource
				(
					char                      const *filepathRelativeToResourcepath,
					Buffer<Byte>              const &data,
					conv::ModeFilesystemWrite const  mode
				)
				{
					namespace fs = boost::filesystem;
					
					fs::path p{m_project.location().path()};
					p.append("res"); /// @todo: Should "res" be put into some SDK constant instead of hardcoding everywhere?
					p.append(filepathRelativeToResourcepath);
					p.make_preferred();
					
					std::ofstream f
					{
						p.string(),
						std::ios_base::binary |
						(
							(conv::ModeFilesystemWrite::truncate == mode)
							?
								std::ios_base::trunc
							:
								std::ios_base::app
						)
					};
					
					if (!f.is_open())
					{
						std::stringstream s;
						s
							<< "Could not open file '"
							<< p.string()
							<< "' for writing.";
						return {false, s.str()};
					}
					
					f.write
					(
						reinterpret_cast<char *>(data.data.get()),
						static_cast<std::streamsize>(data.size)
					);
					
					return {true};
				}
			}
		}
	}
}

namespace
{
	/**
	 * Recursively makes a deep copy of a directory.
	 */
	/// @todo: Fix the error logging.
	static bool copyDir(const boost::filesystem::path &source, const boost::filesystem::path &destination)
	{
		namespace fs = boost::filesystem;

		try
		{
			if (!fs::exists(source) || !fs::is_directory(source))
			{
				karhu::log::err() << "Source directory " << source.string() << " does not exist or is not a directory.";
				return false;
			}

			if (fs::exists(destination))
			{
				karhu::log::err() << "Destination directory " << destination.string() << " already exists.";
				return false;
			}

			if (!fs::create_directory(destination))
			{
				karhu::log::err() << "Unable to create destination directory" << destination.string() << '.';
				return false;
			}
		}
		catch (const fs::filesystem_error &e)
		{
			karhu::log::err() << e.what();
			return false;
		}

		// Iterate through the source directory.
		for (fs::directory_iterator file(source); file != fs::directory_iterator(); ++ file)
		{
			try
			{
				fs::path current(file->path());
				if (fs::is_directory(current))
				{
					// Found directory: recursion.
					if(!copyDir(current, destination / current.filename()))
						return false;
				}
				else
				{
					// Found file: copy.
					fs::copy_file(current, destination / current.filename());
				}
			}
			catch (const fs::filesystem_error &e)
			{
				karhu::log::err() << e.what();
			}
		}

		return true;
	}
}
