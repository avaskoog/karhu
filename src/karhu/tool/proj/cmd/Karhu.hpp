/**
 * @author		Ava Skoog
 * @date		2017-06-26
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_CMD_KARHU_H_
	#define KARHU_TOOL_PROJ_CMD_KARHU_H_
	
	#include <karhu/tool/Command.hpp>
	#include <karhu/tool/proj/cmd/Base.hpp>
	#include <string>
	#include <vector>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace cmd
				{
					/**
					 * Creates a new Karhu project.
					 *
					 * Syntax:
					 * karhu [--dir <path>=current]
					 */
					class Karhu : public Command, private Base
					{
						public:
							Karhu()
							:
							Command
							{
								Param{"dir", Enforce::optional, ArgsMinMax{1}}
							}
							{
							}

							bool processOpt(const OptWithVals &) override;
							bool finish() override;

						private:
							std::string m_dir;
					};
				}
			}
		}
	}
#endif
