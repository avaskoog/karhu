/**
 * @author		Ava Skoog
 * @date		2017-04-02
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/cmd/New.hpp>
#include <karhu/tool/proj/Location.hpp>
#include <karhu/tool/proj/Identifier.hpp>
#include <karhu/tool/proj/file/Project.hpp>
#include <karhu/tool/SDK.hpp>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool New::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if      (n == "id")       m_identifier = v[0];
					else if (n == "name")     m_name       = v[0];
					else if (n == "template") m_template   = v[0];
					else if (n == "dir")      m_dir        = v[0];

					return true;
				}

				/**
				 * Uses the validated information to set up the new project.
				 */
				bool New::finish()
				{
					return createNew
					(
						m_dir,
						m_template,
						proj::Identifier::create(m_identifier),
						m_name
					);
				}
			}
		}
	}
}
