/**
 * @author		Ava Skoog
 * @date		2017-07-02
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_CMD_SYNC_H_
	#define KARHU_TOOL_PROJ_CMD_SYNC_H_
	
	#include <karhu/tool/Command.hpp>
	#include <karhu/tool/proj/cmd/Base.hpp>
	#include <string>
	#include <vector>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace cmd
				{
					/**
					 * Creates a new Karhu project.
					 *
					 * Syntax:
					 * sync --acts <push, pull, update> [--m <message>] [--dir <path>=current]
					 */
					class Sync : public Command, private Base
					{
						public:
							Sync()
							:
							Command
							{
								Param{"acts", Enforce::required, ArgsMinMax{1, 3}},
								Param{"m",    Enforce::optional, ArgsMinMax{1}},
								Param{"dir",  Enforce::optional, ArgsMinMax{1}}
							}
							{
							}

							bool processOpt(const OptWithVals &) override;
							bool finish() override;
						
						private:
							bool update();

						private:
							std::string m_message, m_dir;
							std::vector<std::string> m_acts;
					};
				}
			}
		}
	}
#endif
