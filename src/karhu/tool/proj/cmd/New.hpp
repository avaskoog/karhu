/**
 * @author		Ava Skoog
 * @date		2017-04-02
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_CMD_NEW_H_
	#define KARHU_TOOL_PROJ_CMD_NEW_H_
	
	#include <karhu/tool/Command.hpp>
	#include <karhu/tool/proj/cmd/Base.hpp>
	#include <string>
	#include <vector>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace cmd
				{
					/**
					 * Creates a new Karhu project.
					 *
					 * Syntax:
					 * new --id <ext.org.proj> --name <unicode> [--template <folder>=project] [--dir <path>=current]
					 */
					class New : public Command, private Base
					{
						public:
							New()
							:
							Command
							{
								Param{"id",       Enforce::required, ArgsMinMax{1}},
								Param{"name",     Enforce::required, ArgsMinMax{1}},
								Param{"template", Enforce::optional, ArgsMinMax{1}},
								Param{"dir",      Enforce::optional, ArgsMinMax{1}}
							}
							{
							}

							bool processOpt(const OptWithVals &) override;
							bool finish() override;

						private:
							std::string m_identifier, m_name, m_template, m_dir;
					};
				}
			}
		}
	}
#endif
