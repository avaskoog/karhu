/**
 * @author		Ava Skoog
 * @date		2017-06-26
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/cmd/Karhu.hpp>
#include <karhu/tool/proj/Location.hpp>
#include <karhu/tool/proj/Identifier.hpp>
#include <karhu/tool/proj/file/Project.hpp>
#include <karhu/tool/SDK.hpp>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool Karhu::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if (n == "dir") m_dir = v[0];

					return true;
				}

				/**
				 * Uses the validated information to set up the new project.
				 */
				bool Karhu::finish()
				{
					namespace lib = SDK::values::lib;
					
					constexpr auto libname(lib::name::lowercase());
					
					return createNew
					(
						m_dir,
						libname,
						proj::Identifier::create
						(
							lib::id::ext(),
							lib::id::org(),
							lib::id::proj()
						),
						libname
					);
				}
			}
		}
	}
}
