/**
 * @author		Ava Skoog
 * @date		2017-07-02
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_CMD_BASE_H_
	#define KARHU_TOOL_PROJ_CMD_BASE_H_

	#include <karhu/tool/shared/cmd/Base.hpp>
	#include <karhu/tool/proj/Identifier.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace cmd
				{
					class Base : public shared::cmd::Base
					{
						protected:
							bool createNew
							(
								const std::string &directory,
								std::string templatename,
								const Nullable<Identifier> &identifier,
								const std::string &fullname
							);
					};
				}
			}
		}
	}
#endif
