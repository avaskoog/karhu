/**
 * @author		Ava Skoog
 * @date		2018-10-25
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: MOVE ALL THE ASSET MANAGEMENT, ID MANAGEMENT AND SO ON FROM proj::Asset INTO Bank.
/// @todo: Possibly add --force option.

#include <karhu/tool/proj/cmd/Asset.hpp>
#include <karhu/tool/data/cmd/GLSL.hpp>
#include <karhu/tool/SDK.hpp>

#include <karhu/res/Material.hpp>
#include <karhu/res/Shader.hpp>
#include <karhu/res/Texture.hpp>
#include <karhu/res/Mesh.hpp>
#include <karhu/res/Rig.hpp>
#include <karhu/res/Animation.hpp>
#include <karhu/res/Scene.hpp>
#include <karhu/res/Data.hpp>
#include <karhu/res/Script.hpp>

#include <boost/process.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

#define STBI_FAILURE_USERMSG
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define TINYGLTF_IMPLEMENTATION
#define PICOJSON_NO_EXCEPTIONS
#include <karhu/tool/lib/tiny_gltf.h>

#include <fstream>

namespace
{
	using namespace karhu;
	
	static Nullable<tinygltf::Model> loadGLTF(std::string const &path)
	{
		tinygltf::TinyGLTF
			loader;
		
		tinygltf::Model
			model;
		
		std::string
			error;

		if (!loader.LoadASCIIFromFile
		(
			&model,
			&error,
			nullptr,
			path,
			tinygltf::REQUIRE_ALL
		))
		{
			log::err() << error;
			return {false};
		}
		
		return {true, std::move(model)};
	}
	
	static std::string stringToASCII(std::string const &s)
	{
		std::stringstream r;
		
		auto isValid([](const char &c)
		{
			return
			(
				(c >= 'A' && c <= 'Z') ||
				(c >= 'a' && c <= 'z') ||
				(c >= '0' && c <= '9') ||
				c == '-' ||
				c == '_'
			);
		});
		
		for (std::size_t i{0}; i < s.length(); ++ i)
		{
			if (isValid(s[i]))
				r << s[i];
			else if (' ' == s[i] || '.' == s[i])
				r << '-';
		}
		
		return r.str();
	}
}

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool Asset::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if      (n == "act")    m_act    = strToAct(v[0].c_str());
					else if (n == "type")   m_type   = v[0];
					else if (n == "name")   m_name   = v[0];
					else if (n == "rename") m_rename = v[0];
					else if (n == "val")    m_val    = v[0];
					else if (n == "format") m_format = v[0];
					else if (n == "dir")    m_dir    = v[0];
					else if (n == "srcs")
					{
						m_srcs.resize(v.size());
						for (std::size_t i{0}; i < v.size(); ++ i)
							m_srcs[i] = v[i];
					}
					else if (n == "key")
					{
						m_key.resize(v.size());
						for (std::size_t i{0}; i < v.size(); ++ i)
							m_key[i] = v[i];
					}

					return true;
				}
				
				/**
				 * Uses the validated information to perform the selected action.
				 */
				bool Asset::finish()
				{
					namespace fs = boost::filesystem;
					
					if (Action::invalid == m_act)
					{
						log::err()
							<< "Invalid action! Must be one of the following: "
							<< "create, remove, edit, rename, refresh or build";
						
						return false;
					}
					
					if (Action::refresh != m_act)
					{
						if (Action::create != m_act)
						{
							if (m_name.empty())
							{
								log::err() << "Missing '--name'";
								return false;
							}
						}
						
						if (m_type.empty())
						{
							log::err() << "Missing '--type'";
							return false;
						}
					}
					
					if (!loadProject(m_dir))
						return false;
					
					m_bank = std::make_unique<AdapterResourcebank>(project());
					
					if (Action::refresh != m_act)
					{
						const auto typenames(m_bank->stringsForTypes());
						
						const auto it(std::find_if
						(
							typenames.begin(),
							typenames.end(),
							[&type = m_type](const auto &v)
							{
								return (v.second == type);
							}
						));
						
						if (it == typenames.end())
						{
							std::stringstream s;
							s << "Invalid asset type! Must be one of the following: ";
							
							std::size_t i{0};
							for (const auto &n : typenames)
							{
								s << n.second;
							
								if (typenames.size() > 2 && (i + 2) < typenames.size())
									s << ", ";
								else if ((i + 2) == typenames.size())
									s << " or ";
								
								++ i;
							}
							
							log::err() << s.str();
							
							return false;
						}
						
						m_IDType = it->first;
					}
					
					// The base asset directory path.
					fs::path p{location().path()};
					p.append("res"); /// @todo: Should this subdirectory be put into an SDK constant?
					p.make_preferred();
					
					if (!fs::exists(p))
					{
						log::err() << "Could not find any asset directory in project";
						return false;
					}
					
					m_basepath = p.string();
					
					if (!updatePaths())
						return false;

					switch (m_act)
					{
						case Action::invalid: break;
						case Action::create:  return create();
						case Action::remove:  return remove();
						case Action::edit:    return edit();
						case Action::rename:  return rename();
						case Action::build:   return build();
						case Action::refresh: return refresh();
					}

					return false;
				}
				
				bool Asset::create(conv::JSON::Val *DB)
				{
					namespace fs = boost::filesystem;
					
					if
					(
						!typeIs<res::Texture>()   &&
						!typeIs<res::Mesh>()      &&
						!typeIs<res::Rig>()       &&
						!typeIs<res::Animation>()
					)
					{
						if (m_name.empty())
						{
							log::err() << "Missing '--name'";
							return false;
						}
					}
					
					if (!validateSourcecount())
						return false;
					
					if (!validateSources())
						return false;
					
					std::vector<Creation> creations;
					
					if (typeIs<res::Shader>())
						creations = createShader();
					else if (typeIs<res::Texture>())
						creations = createTexture();
					else if (typeIs<res::Mesh>())
						creations = createMesh();
					else if (typeIs<res::Rig>())
						creations = createRig();
					else if (typeIs<res::Animation>())
						creations = createAnimation();
					else if (typeIs<res::Script>())
						creations = createScript();
					else
					{
						log::err() << "'--create' does not apply to this type of resource";
						return false;
					}
					
					if (creations.empty())
						// Lack of output is not an error.
						return true;
					
					conv::JSON::Val DBLocal;
					bool ownDB{true};
					
					if (!DB)
					{
						ownDB = false;
						
						if (!loadDatabase(DBLocal))
							return false;
						
						DB = &DBLocal;
					}
					
					std::vector<res::IDResource>
						sources;
					
					std::vector<std::string const *>
						sourcesFound;
					
					conv::JSON::Arr const *const
						resources{DB->object()->getArray("resources")};
					
					if (resources)
					{
						for (auto const &resource : *resources)
						{
							if (!resource.object())
								break;
							
							auto const
								path(resource.object()->getString("path"));
							
							auto const
								type(resource.object()->getInt("type"));
							
							auto const
								ID(resource.object()->getInt("id"));
							
							if (!path || !type || !ID)
								break;
							
							// Only looking for raw resources.
							if (0 != *type)
								continue;
							
							for (std::string const &source : m_srcs)
							{
								if (source == *path)
								{
									sources.emplace_back(static_cast<res::IDResource>(*ID));
									sourcesFound.emplace_back(&source);
									break;
								}
							}
						}
						
						// Create source entries not found.
						for (std::string const &source : m_srcs)
						{
							auto const
								it(std::find
								(
									sourcesFound.begin(),
									sourcesFound.end(),
									&source
								));
							
							if (it == sourcesFound.end())
							{
								log::msg() << source;
								log::msg() << source;
								
								const auto
									ID(addResToDB(*DB, static_cast<res::IDTypeResource>(0), source, source));
								
								log::msg() << static_cast<std::int64_t>(ID);
								log::msg() << 0;
								
								sources.emplace_back(ID);
							}
						}
					}
					
					for (Creation const &creation : creations)
					{
						if (JSON::TypeValue::null != creation.data.type() && !creation.metapath.empty())
						{
							fs::path metapath{m_basepath};
							metapath.append(creation.metapath);
							metapath.make_preferred();
						
							if (!fs::exists(metapath))
							{
								m_metapath = metapath.string();
								
								if (!saveMetadata(creation.data))
									return false;
							}
						}
						
						{
							log::msg() << creation.name;
							log::msg() << ((creation.metapath.empty()) ? creation.name : creation.metapath);
						
							const auto
								ID(addResToDB(*DB, creation.type, creation.name, creation.name, sources));
							
							log::msg() << static_cast<std::int64_t>(ID);
							log::msg() << static_cast<std::int64_t>(creation.type);
							
							std::stringstream
								ss;
							
							for (std::size_t i{0}; i < sources.size(); ++ i)
							{
								ss << static_cast<std::int64_t>(sources[i]);
								if ((i + 1) < sources.size())
									ss << ' ';
							}
							
							log::msg() << ss.str();
						}
					}

					if (ownDB)
						if (!saveDatabase(*DB))
							return false;
					
					return true;
				}
				
				bool Asset::edit()
				{
					/// @todo: Should we be validating key names and value formats per type..?

					if (!validateExistence())
						return false;
					
					conv::JSON::Val value;
					{
						conv::JSON::Parser parser;
						std::stringstream s;
						s << m_val;
						auto parsed(parser.parse(s));
						
						if (!parsed)
						{
							log::msg()
								<< "JSON error in value: "
								<< parser.error();
							return false;
						}

						value = std::move(*parsed);
					}
					
					conv::JSON::Val data;
					if (!loadMetadata(data))
						return false;
					
					auto root(data.object());
					
					// Go through and see if anything needs to be created.
					if (1 == m_key.size())
						root->set(m_key[0], std::move(value));
					else
					{
						auto curr(root);
						for (std::size_t i{0}; i < m_key.size(); ++ i)
						{
							auto elem(curr->get(m_key[i]));

							if (!elem)
								curr = curr->push({m_key[i], conv::JSON::Obj{}}).object();
							else if (!elem->object())
							{
								*elem = conv::JSON::Obj{};
								curr = elem->object();
							}
							else
								curr = elem->object();
						}
						
						auto elem(root->find(m_key));
						*elem = std::move(value);
					}

					// Rewrite the metafile.
					if (!saveMetadata(data))
						return false;

					return true;
				}
				
				bool Asset::remove()
				{
					namespace fs = boost::filesystem;
					
					if (!validateExistence())
						return false;
					
					conv::JSON::Val DB;
					if (!loadDatabase(DB))
						return false;
					
					removeResFromDB(DB, m_name);
					
					if (!saveDatabase(DB))
						return false;

					auto files{associatedFiles()};
					files.emplace_back(m_metapath);
					
					for (const auto &file : files)
					{
						fs::path p{file};
						p.make_preferred();
						if (fs::exists(p) && fs::is_regular_file(p))
						{
							bool success{true};
							
							try
							{
								fs::remove(p);
							}
							catch (const fs::filesystem_error &e)
							{
								success = false;
								log::err()
									<< "Could not remove file '"
									<< p.string()
									<< "': "
									<< e.what();
							}
							
							if (success)
								log::msg()
									<< "Removed: '"
									<< p.string()
									<< '\'';
						}
					}

					return true;
				}
				
				bool Asset::rename()
				{
					namespace fs = boost::filesystem;
					
					if (!validateExistence(true))
						return false;

					if (m_name == m_rename)
						return true;

					conv::JSON::Val DB;
					if (!loadDatabase(DB))
						return false;

					auto files{renamableFiles()}; /// @todo: Possible we can merge renamableFiles() back with associatedFiles(); have a look when all asset types are implemented.
					files.emplace_back(m_metapath);

					for (const auto &file : files)
					{
						fs::path srcpath{file};
						srcpath.make_preferred();

						if (fs::exists(srcpath) && fs::is_regular_file(srcpath))
						{
							bool success{true};
							
							const std::string filename{fs::path{m_filepath}.filename().string()};
							std::string suffix{srcpath.filename().string()};

							if (suffix.length() < filename.length())
							{
								log::err()
									<< "Unexpected filename '"
									<< srcpath.string()
									<< '\'';
								return false;
							}
							
							suffix = suffix.substr
							(
								filename.length(),
								suffix.length() - filename.length()
							);
							
							fs::path dstpath{srcpath};
							dstpath.remove_filename();
							dstpath.append(m_rename + suffix);
							dstpath.make_preferred();
							
							try
							{
								fs::rename(srcpath, dstpath);
							}
							catch (const fs::filesystem_error &e)
							{
								success = false;
								log::err()
									<< "Could not rename file '"
									<< srcpath.string()
									<< "' to '"
									<< dstpath.string()
									<< "': "
									<< e.what();
							}
							
							if (success)
								log::msg()
									<< "Moved "
									<< srcpath.string()
									<< "' to '"
									<< dstpath.string()
									<< '\'';
						}
					}

					// Adding an existing resource will just update it.
					{
						const auto ID(addResToDB(DB, m_IDType, m_name, m_rename));
						log::msg() << ID;
					}
					
					if (!saveDatabase(DB))
						return false;

					return true;
				}
				
				bool Asset::build()
				{
					if (typeIs<res::Shader>())
						return buildShader();
					
					log::err() << "'--build' does not apply to this type of resource";
					
					return false;
				}
				
				/*
				 * Removes resources from the database if they have disappeared,
				 * and creates them if new files have been added to the folder.
				 */
				bool Asset::refresh()
				{
					namespace fs = boost::filesystem;
					
					conv::JSON::Val DB;
					
					if (!loadDatabase(DB))
						return false;
					
					if (m_srcs.empty())
						refreshResourcesInDirectory(m_basepath, DB);
					else
					{
						std::vector<std::string> const srcs{m_srcs};
						
						for (std::string const &src : srcs)
						{
							fs::path path{m_basepath};
							path.append(src);
							path.make_preferred();
							
							m_name = src;
							
							if (fs::exists(path) && fs::is_regular_file(path))
								if (!refreshResource(path.string(), DB))
									return false;
						}
					}
					
					saveDatabase(DB);
					
					return true;
				}
				
				bool Asset::refreshResourcesInDirectory(std::string const &dirpath, conv::JSON::Val &DB)
				{
					namespace fs = boost::filesystem;
					
					fs::path respath{dirpath};
					respath.make_preferred();
					
					for (fs::directory_iterator it{respath}; it != fs::directory_iterator(); ++ it)
					{
						try
						{
							if (fs::is_directory(it->path()))
							{
								if (it->path().filename() == "gen")
									continue;
								
								if (!refreshResourcesInDirectory(it->path().string(), DB))
									return false;
							}
							else if (fs::is_regular_file(it->path()))
							{
								if (!refreshResource(it->path().string(), DB))
									return false;
							}
						}
						catch (const fs::filesystem_error &e)
						{
							fs::path pathRelative{fs::relative(it->path(), m_basepath)};
							pathRelative.make_preferred();
					
							log::err()
								<< "Failed to refresh resources in '"
								<< pathRelative.string()
								<< "': "
								<< e.what();
						}
					}
					
					return true;
				}
				
				bool Asset::refreshResource(std::string const &path, conv::JSON::Val &DB)
				{
					namespace fs = boost::filesystem;
					
					fs::path pathRelative{fs::relative(path, m_basepath)};
					pathRelative.make_preferred();
					
					auto const ext([&pathRelative](char const *const s) -> bool
					{
						std::size_t const length{std::strlen(s)};
						
						if (pathRelative.string().length() < length)
							return false;
						
						char const *const suffix
						{
							pathRelative.c_str() +
							pathRelative.string().length() -
							length
						};
						
						return (meta::string::equal(s, suffix));
					});
					
					// Is it an image?
					if (ext(".png") || ext(".jpg") || ext(".jpeg"))
					{
						m_IDType = m_bank->IDOfType<res::Texture>();
						
						m_srcs.clear();
						m_srcs.emplace_back(m_name);
							
						return create(&DB);
					}
					// Is it a script?
					else if (ext(".as"))
					{
						m_IDType = m_bank->IDOfType<res::Script>();
						
						m_srcs.clear();
						m_srcs.emplace_back(m_name);
						
						/// @todo: Check includes and update affected files.
						/// @todo: Deal with includes.
						
						return create(&DB);
					}
					// Is it 3D?
					else if (ext(".gltf"))
					{
						m_srcs.clear();
						m_srcs.emplace_back(m_name);
						
						// Bake meshes.
						
						m_IDType = m_bank->IDOfType<res::Mesh>();
						
						if (!create(&DB))
							return false;
						
						// Bake rigs.
						
						m_IDType = m_bank->IDOfType<res::Rig>();
						
						if (!create(&DB))
							return false;
						
						// Bake animations.
						
						m_IDType = m_bank->IDOfType<res::Animation>();
						
						if (!create(&DB))
							return false;
					}
					// Is it a shader?
					/// @todo: Deal with .incl shader files in asset refresh.
					/// @todo: Are we ever going to support geometry shaders? If so, add to asset refresh.
					/// @todo: The asset refresh implementation for shaders is written with the expectation that generally only one GLSL file is modified at a time and that multiple recompilations in a row won't be necessary; possible in the future we might want to make it collect all modified shader files before recompiling anything to avoid more than necessary.
					else if (ext(".vert") || ext(".frag"))
					{
						auto resources(DB.object()->getArray("resources"));
						
						conv::JSON::Int IDFile{0};
						
						// Find the raw resource for this file.
						for (auto const &resource : *resources)
						{
							auto const o(resource.object());
							
							if (!o)
								continue;
							
							auto const type(o->getInt("type"));
							
							if (!type || 0 != *type)
								continue;
							
							auto const path(o->getString("path"));
							
							if (!path || m_name != *path)
								continue;
							
							auto const ID(o->getInt("id"));
							
							if (!ID)
							{
								log::err()
									<< "Could not refresh resource '"
									<< m_name
									<< "': "
									<< "missing ID in resource database";
								
								return false;
							}
							
							IDFile = *ID;
							break;
						}
						
						std::map<conv::JSON::Int, std::string const *> shadersToRebuild;
					
						// Find all the shaders that depend on this file.
						for (auto const &resource : *resources)
						{
							auto const o(resource.object());
							
							if (!o)
								continue;
							
							auto const type(o->getInt("type"));
							
							if (!type || m_bank->IDOfType<res::Shader>() != *type)
								continue;
							
							auto const srcs(o->getArray("srcs"));
							
							if (!srcs)
								continue;
							
							bool use{false};
							
							for (auto const &src : *srcs)
							{
								auto const IDSource(src.integer());
								
								if (IDSource && IDFile == *IDSource)
								{
									use = true;
									break;
								}
							}
							
							if (use)
							{
								auto const path(o->getString("path"));
								
								if (!path)
								{
									log::err()
										<< "Could not refresh resource '"
										<< m_name
										<< "': "
										<< "no valid path in resource database";
									
									return false;
								}
								
								auto const ID(o->getInt("id"));
								
								if (!ID)
								{
									log::err()
										<< "Could not refresh resource '"
										<< m_name
										<< "': "
										<< "no valid ID in resource database";
									
									return false;
								}
								
								shadersToRebuild.emplace(*ID, path);
							}
						}
						
						/// @todo: Just building every shader format for now, but do update this so that only those necessary for the target platform get built. Probably do this by supplying an array of supported shader types in the target configuration JSON file. Remember that at the moment this is duplicated in both asset refresh and build make.
						
						static constexpr std::array<const char *, 2> formats
						{
							"gl",
							"gles"/*,
							"metal"*/
							/// @todo: Work out quirks with metal and reënable it.
						};
						
						m_IDType = m_bank->IDOfType<res::Shader>();
					
						for (auto const &it : shadersToRebuild)
						{
							m_name = *it.second;
							
							updatePaths();
							
							for (const auto &format : formats)
							{
								m_format = format;
								
								if (!buildShader(&DB))
									return false;
							}
							
							log::msg() << m_name;
							log::msg() << m_metapath;
							log::msg() << it.first;
							log::msg() << static_cast<std::int64_t>(m_bank->IDOfType<res::Shader>());
						}
					}
					
					return true;
				}
				
				std::vector<Asset::Creation> Asset::createShader()
				{
					std::vector<Asset::Creation> r;
					
					Creation c;
					
					c.name     = m_name;
					c.metapath = c.name + m_bank->suffixForType<res::Shader>();
					c.type     = m_bank->IDOfType<res::Shader>();
					c.data     = conv::JSON::Obj{};
					
					// These are the raw sources the user works with.
					auto sources(c.data.object()->push({"sources", conv::JSON::Arr{}}).array());
						
					for (const auto &s : m_srcs)
						sources->push(s);
					
					// These are the actual built shaders for runtime.
					c.data.object()->push({"builds", conv::JSON::Arr{}});
					
					r.emplace_back(std::move(c));
					
					return r;
				}
				
				std::vector<Asset::Creation> Asset::createTexture()
				{
					namespace fs = boost::filesystem;
					
					std::vector<Asset::Creation> r;
					
					Creation c;

					c.name     = m_srcs[0];
					c.metapath = c.name + m_bank->suffixForType<res::Texture>();
					c.type     = m_bank->IDOfType<res::Texture>();
					c.data     = conv::JSON::Obj{};
					
					// These can just be set to defaults and
					// then be configured as needed by the user.
					/// @todo: Should we actually open the file and extract format?
					/// @todo: Need to move these enums out of classes to make them available to scripts.
					c.data.object()->push({"format", static_cast<int>(gfx::Pixelbuffer::Format::RGB)});
					c.data.object()->push({"wrap",   static_cast<int>(res::Texture::ModeWrap::repeat)});
					c.data.object()->push({"type",   static_cast<int>(res::Texture::Type::colour)});
					
					r.emplace_back(std::move(c));

					return r;
				}

				std::vector<Asset::Creation> Asset::createMesh()
				{
					namespace fs = boost::filesystem;
					
					std::vector<Asset::Creation> r;
					
					fs::path filepath{m_basepath};
					filepath.append(m_srcs[0]);
					filepath.make_preferred();
					
					auto model(loadGLTF(filepath.string()));
					
					if (!model || 0 == model->meshes.size())
						return r;
					
					std::string const
						path{fs::path{m_srcs[0]}.filename().string()};
					
					Creation c;
					
					c.name     = m_srcs[0]; /// @todo: Only one file for all meshes for now. Could attach mesh name if split.
					c.metapath = c.name + m_bank->suffixForType<res::Mesh>();
					c.type     = m_bank->IDOfType<res::Mesh>();
					c.data     = conv::JSON::Obj{};
					
					c.data.object()->push({"path", path});
					
					r.emplace_back(std::move(c));
					
					return r;
				}
				
				std::vector<Asset::Creation> Asset::createRig()
				{
					namespace fs = boost::filesystem;
					
					std::vector<Asset::Creation> r;
					
					fs::path filepath{m_basepath};
					filepath.append(m_srcs[0]);
					filepath.make_preferred();
					
					auto model(loadGLTF(filepath.string()));
					
					if (!model)
						return r;
					
					std::string const
						path  {fs::path{m_srcs[0]}.filename().string()},
						suffix{m_bank->suffixForType<res::Rig>()};
					
					res::IDTypeResource const
						type{m_bank->IDOfType<res::Rig>()};
					
					for (auto const &skin : model->skins)
					{
						Creation c;
						
						c.name     = m_srcs[0] + '.' + stringToASCII(skin.name);
						c.metapath = c.name + suffix;
						c.type     = type;
						c.data     = conv::JSON::Obj{};
						
						c.data.object()->push({"path", path});
						
						r.emplace_back(std::move(c));
					}
					
					return r;
				}
				
				std::vector<Asset::Creation> Asset::createAnimation()
				{
					namespace fs = boost::filesystem;
					
					std::vector<Asset::Creation> r;
					
					fs::path filepath{m_basepath};
					filepath.append(m_srcs[0]);
					filepath.make_preferred();
					
					auto model(loadGLTF(filepath.string()));
					
					if (!model)
						return r;
					
					std::string const
						path  {fs::path{m_srcs[0]}.filename().string()},
						suffix{m_bank->suffixForType<res::Animation>()};
					
					res::IDTypeResource const
						type{m_bank->IDOfType<res::Animation>()};
					
					for (auto const &animation : model->animations)
					{
						Creation c;
						
						c.name     = m_srcs[0] + '.' + stringToASCII(animation.name);
						c.metapath = c.name + suffix;
						c.type     = type;
						c.data     = conv::JSON::Obj{};
						
						c.data.object()->push({"path", path});
						c.data.object()->push({"name", animation.name});
						
						r.emplace_back(std::move(c));
					}
					
					return r;
				}
				
				std::vector<Asset::Creation> Asset::createScript()
				{
					namespace fs = boost::filesystem;
					
					std::vector<Asset::Creation> r;
					
					Creation c;

					c.name = m_srcs[0];
					c.type = m_bank->IDOfType<res::Script>();
					
					fs::path path{m_basepath};
					path.append(c.name);
					path.make_preferred();
					
					if (!fs::exists(path))
						std::ofstream fo{path.string()};
					
					r.emplace_back(std::move(c));

					return r;
				}
				
				bool Asset::buildShader(conv::JSON::Val *DB)
				{
					namespace fs    = boost::filesystem;
					namespace bp    = boost::process;
					namespace tools = SDK::paths::home::tools;
					
					static constexpr std::array<const char *, 3> formatsShader
					{
						"gl",
						"gles",
						"metal"
					};

					const res::Shader::Format format{res::Shader::strToFormat(m_format.c_str())};
					if (res::Shader::Format::invalid == format)
					{
						std::stringstream s;
						for (std::size_t i{0}; i < formatsShader.size(); ++ i)
						{
							s << formatsShader[i];
							if (formatsShader.size() > 2 && (i + 2) < formatsShader.size())
								s << ", ";
							else if ((i + 2) == formatsShader.size())
								s << " or ";
						}
						
						log::err()
							<< "When '--type' is set to 'shader', "
							<< "one of the following must be set with '--format': "
							<< s.str();

						return false;
					}
					
					conv::JSON::Val data;
					if (!loadMetadata(data))
						return false;
					
					conv::JSON::Val DBLocal;
					bool ownDB{true};
					
					if (!DB)
					{
						ownDB = false;
						
						if (!loadDatabase(DBLocal))
							return false;
						
						DB = &DBLocal;
					}
					
					auto const resources(DB->object()->getArray("resources"));
					
					if (!resources)
						return false;
					
					conv::JSON::Arr const *sources{nullptr};
					
					for (auto const &resource : *resources)
					{
						auto const o(resource.object());
						
						if (!o)
							continue;
						
						auto const
							type(o->getInt("type"));
						
						auto const
							path(o->getString("path"));
						
						if (!type || !path)
							continue;
						
						if (*type == m_bank->IDOfType<res::Shader>() && *path == m_name)
						{
							sources = o->getArray("srcs");
							break;
						}
					}
					
					if (!sources)
					{
						log::err() << "Array 'srcs' missing in resource database entry";
						return false;
					}
					
					// This is not an error, but nothing to do either.
					if (0 == sources->count())
						return true;
					
					std::map<res::IDResource, std::string>
						sourcepathsByID;
					
					for (auto const &source : *sources)
					{
						auto const ID(source.integer());
						
						if (!ID)
						{
							log::err() << "Invalid source ID in array 'srcs' in resource database entry";
							return false;
						}
						
						sourcepathsByID.emplace(static_cast<res::IDResource>(*ID), "");
					}
					
					for (auto const &resource : *resources)
					{
						if (!resource.object())
							continue;
						
						auto const
							ID(resource.object()->getInt("id"));
						
						auto const
							path(resource.object()->getString("path"));
						
						if (!ID || !path)
							continue;
						
						for (auto &it : sourcepathsByID)
						{
							if (*ID == it.first)
							{
								it.second = *path;
								break;
							}
						}
					}

					auto builds(data.object()->getArray("builds"));
					if (!builds)
						builds = data.object()->push({"builds", conv::JSON::Arr{}}).array();
					
					conv::JSON::Obj *obj{nullptr};
					{
						const auto needle(static_cast<conv::JSON::Int>(format));
						for (auto &b : *builds)
						{
							if (auto o = b.object())
							{
								if (const auto f = o->getInt("format"))
								{
									if (needle == *f)
									{
										obj = o;
										break;
									}
								}
							}
						}
						
						if (!obj)
						{
							builds->push(conv::JSON::Obj{});
							obj = builds->get(builds->count() - 1)->object();
							obj->push({"format", needle});
						}
					}
					
					auto arr(obj->getArray("sources"));
					if (!arr)
						arr = obj->push({"sources", conv::JSON::Arr{}}).array();
					// If there was something there previously, clean it up.
					else
					{
						fs::path basepath{m_metapath};
						basepath.remove_filename();
						
						for (const auto &s : *arr)
						{
							if (const auto o = s.object())
							{
								if (const auto f = o->getString("file"))
								{
									fs::path filepath{m_filepath};
						
									filepath.remove_filename();
									filepath.append(*f);
									
									if (fs::exists(filepath) && fs::is_regular_file(filepath))
										fs::remove(filepath);
								}
							}
						}
						
						arr->clear();
					}
					
					// We will be using karhudata to create the files.
					std::vector<std::string> args;
					
					// The command to call.
					args.emplace_back("glsl");
					
					// The action to carry out.
					args.emplace_back("--act");
					args.emplace_back("convert");
					
					// The format.
					args.emplace_back("--format");
					args.emplace_back(m_format);
					
					// Specify the sources as defined in the metafile.
					args.emplace_back("--ins");
					for (const auto &it : sourcepathsByID)
					{
						if (it.second.empty())
						{
							log::err() << "Invalid value in array of sources in shader metadata file";
							return false;
						}
						
						fs::path p{m_basepath};
						p.append(it.second);
						p.make_preferred();
						
						if (!fs::exists(p))
						{
							log::err()
								<< "Source specified in shader metadata not found at '"
								<< p.string()
								<< '\'';
								
							return false;
						}
						
						args.emplace_back(p.string());
					}
					
					// Specify the output name as that of the asset.
					args.emplace_back("--out");
					{
						fs::path p{m_filepath};
						
						p.remove_filename();
						p.append("gen");
						
						if (!fs::exists(p))
							fs::create_directory(p);
						
						if (m_format == "metal")
							p.append(m_name);
						
						p.make_preferred();
						args.emplace_back(p.string());
					}
					
					// Add the graphics layers as constants.
					{
						fs::path file{m_basepath};
						file.append(SDK::paths::project::res::graphics());
						file.make_preferred();
						
						std::ifstream f{file.string()};
						
						if (f.is_open())
						{
							conv::JSON::Parser parser;
					
							auto r(parser.parse(f));
							
							if (r && r->object() && r->object()->getArray("layers"))
							{
								std::stringstream s;
								std::size_t i{0};
								
								for (auto const &layer : *r->object()->getArray("layers"))
								{
									if (layer.string() && !layer.string()->empty())
										s
											<< "const int _LAYER_"
											<< boost::to_upper_copy(stringToASCII(*layer.string()))
											<< " = "
											<< gfx::layer(i)
											<< ";\n";
									
									++ i;
								}
								
								if (!s.str().empty())
								{
									args.emplace_back("--inject");
									args.emplace_back(s.str());
								}
							}
							
							f.close();
						}
					}
					
					std::vector<std::string> files;

					auto p(SDK::env::home(tools::karhudata::local()));
					if (p && fs::exists(*p))
					{
						bp::ipstream pi;
						bp::child c{*p, args, bp::std_out > pi, bp::std_err > stderr};
						
						std::string line;
						
						while (c.running() && std::getline(pi, line) && !line.empty())
							files.emplace_back(line);
						
						c.wait();

						if (0 != c.exit_code())
						{
							log::err() << "Build failed";
							return false;
						}
					}
					else
					{
						log::err()
							<< "Unable to find data tool at '"
							<< *p
							<< '\'';
						
						return false;
					}
					
					for (std::size_t i{0}; i < files.size(); i += 2)
					{
						// Every other line is a filename, the other a mode.
						const auto &f(files[i + 0]);
						const auto &m(files[i + 1]);
						
						res::Shader::Mode mode;
						
						try
						{
							mode = static_cast<res::Shader::Mode>(std::stoi(m));
						}
						catch (const std::exception &e)
						{
							log::err() << "Expected shader mode number but got '" << m << '\'';
							return false;
						}
						
						// We want every path to be relative to the metadata file.
						fs::path metapath{m_metapath};
						metapath.remove_filename();
						fs::path filepath{f};
						filepath = fs::relative(filepath, metapath);
						
						conv::JSON::Obj o;
						o.push({"mode", static_cast<conv::JSON::Int>(mode)});
						o.push({"file", filepath.string()});
						
						arr->push(std::move(o));
					}
					
					if (!saveMetadata(data))
						return false;
					
					return true;
				}
				
				bool Asset::updatePaths()
				{
					namespace fs = boost::filesystem;
					
					// The path to the actual file, if separate from metadata.
					fs::path file{m_basepath};
					file.append(m_name);
					file.make_preferred();

					// The path to the JSON metadata file.
					
					fs::path meta{file.string() + m_bank->suffixForType(m_IDType)};
					meta.make_preferred();
					
					m_filepath = file.string();
					m_metapath = meta.string();
					
					return true;
				}
				
				/*
				 * Checks whether the asset exists in the project.
				 */
				bool Asset::validateExistence(const bool alsoCheckRenamed)
				{
					namespace fs = boost::filesystem;

					// If neither exists the asset is missing.
					if (!fs::exists(m_filepath) && !fs::exists(m_metapath))
					{
						log::err()
							<< "Could not find any "
							<< m_bank->stringForType(m_IDType)
							<< " asset named '"
							<< m_name
							<< '\'';
						return false;
					}
					
					if (alsoCheckRenamed)
					{
						// The path to the actual file, if separate from metadata.
						fs::path file{m_basepath};
						file.append(m_rename);
						file.make_preferred();

						// The path to the JSON metadata file.
						fs::path meta{file.string() + m_bank->suffixForType(m_IDType)};
						meta.make_preferred();
					
						if (fs::exists(file) || fs::exists(meta))
						{
							log::err()
								<< "There already exists some "
								<< m_bank->stringForType(m_IDType)
								<< " asset named '"
								<< m_rename
								<< '\'';
							return false;
						}
					}
					
					return true;
				}
				
				/*
				 * Checks that the number of sources is
				 * valid for the specified asset type.
				 */
				bool Asset::validateSourcecount()
				{
					if (m_srcs.size() > 1 && typeIs<res::Texture>())
					{
						log::err() << "Maximum source count for asset of type texture is 1";
						return false;
					}
					
					return true;
				}
				
				/*
				 * Makes sure the source files exist and are valid files.
				 */
				bool Asset::validateSources()
				{
					/// @todo: Should we validate the format somehow?

					namespace fs = boost::filesystem;
					
					for (auto &src : m_srcs)
					{
						fs::path absolute{m_basepath};
						absolute.append(src);
						absolute.make_preferred();

						if (!fs::exists(absolute))
						{
							log::err()
								<< "Source file not found at '"
								<< src
								<< '\'';
							return false;
						}
						
						if (!fs::is_regular_file(absolute))
						{
							log::err()
								<< "Source file is not a file at at '"
								<< src
								<< '\'';
							return false;
						}
					}
					
					return true;
				}
				
				/*
				 * Sets up any missing values in the database.
				 */
				void Asset::initDatabase(conv::JSON::Val &DB)
				{
					// Root needs to be an object.
					if (!DB.object())
						DB = conv::JSON::Obj{};
					
					// There needs to be an integer with the last used ID.
					{
						const auto free(DB.object()->getInt("last"));
						if (!free)
						{
							m_IDLast = 1;
							DB.object()->push({"last", m_IDLast});
						}
						else
							m_IDLast = *free;
					}
					
					// There needs to be an array of free ID numbers.
					{
						const auto free(DB.object()->getArray("free"));
						if (!free)
							DB.object()->push({"free", conv::JSON::Arr{}});
						else
							for (const auto &v : *free)
								if (const auto ID = v.integer())
									if (*ID < m_IDLast)
										m_IDsFree.emplace_back(*ID);
					}
					
					// There needs to be an array of resources.
					{
						const auto resources(DB.object()->getArray("resources"));
						if (!resources)
							DB.object()->push({"resources", conv::JSON::Arr{}});
					}
				}

				bool Asset::loadDatabase(conv::JSON::Val &DB)
				{
					namespace fs = boost::filesystem;
					
					fs::path p{pathDatabase()};
					p.make_preferred();
					
					// We will just create it if it does not exist, so just return.
					if (!fs::exists(p))
					{
						initDatabase(DB);
						return true;
					}
					
					std::ifstream f{p.string()};
					
					if (!f.is_open())
					{
						log::err() << "Could not open asset database for reading";
						return false;
					}
					
					conv::JSON::Parser parser;
					
					auto r(parser.parse(f));
					if (!r)
					{
						log::err() << "JSON error in asset database: " << parser.error();
						return false;
					}
					
					DB = std::move(*r);
					initDatabase(DB);
					return true;
				}
				
				bool Asset::saveDatabase(conv::JSON::Val &DB)
				{
					namespace fs = boost::filesystem;
					
					fs::path p{pathDatabase()};
					p.make_preferred();
					
					std::ofstream f{p.string(), std::ios_base::trunc};
					
					if (!f.is_open())
					{
						log::err() << "Could open not asset database for writing";
						return false;
					}

					// Update the list of free ID numbes.
					{
						auto free(DB.object()->getArray("free"));
						
						free->clear();
						
						for (const auto &ID : m_IDsFree)
							free->push(ID);
					}

					// Update the last ID.
					{
						auto last(DB.object()->get("last"));
						*last = m_IDLast;
					}
					
					f << DB.dump();
					return true;
				}
				
				bool Asset::loadMetadata(conv::JSON::Val &output)
				{
					std::ifstream fi{m_metapath};

					if (!fi.is_open())
					{
						log::msg()
							<< "Could not open file for reading at '"
							<< m_metapath
							<< '\'';
						return false;
					}
					
					conv::JSON::Parser parser;
					auto data(parser.parse(fi));
					
					if (!data)
					{
						log::msg()
							<< "JSON error in '"
							<< m_metapath
							<< "': "
							<< parser.error();
						return false;
					}
					
					if (!data->object())
					{
						log::msg()
							<< "Could not find JSON object root in '"
							<< m_metapath
							<< '\'';
						return false;
					}
					
					output = std::move(*data);
					return true;
				}
				
				bool Asset::saveMetadata(conv::JSON::Val const &input)
				{
					std::ofstream fo{m_metapath, std::ios_base::trunc};

					if (!fo.is_open())
					{
						log::msg()
							<< "Could not open file for writing at '"
							<< m_metapath
							<< '\'';
						return false;
					}
					
					fo << input.dump();
					return true;
				}
				
				conv::JSON::Int Asset::addResToDB
				(
					conv::JSON::Val &DB,
					res::IDTypeResource const type,
					std::string name,
					std::string rename,
					std::vector<res::IDResource> const &sources
				)
				{
					name = pathRelative(name);
					rename = pathRelative(rename);
					
					auto arr(DB.object()->getArray("resources"));
					
					conv::JSON::Obj *obj{nullptr};
					conv::JSON::Int ID{};
					
					// Check if the resource is already in the DB.
					for (auto &res : *arr)
					{
						if (const auto o = res.object())
						{
							const auto p(o->getString("path"));
							const auto t(o->getInt("type"));
							
							if (p && t)
							{
								if (name == *p && type == static_cast<res::IDTypeResource>(*t))
								{
									obj = o;
									break;
								}
							}
						}
					}
					
					// Otherwise create it.
					if (!obj)
					{
						arr->push(conv::JSON::Obj{});
						obj = arr->get(arr->count() - 1)->object();
						ID = nextIDRes();
						obj->push({"id", ID});
					}
					else if (const auto n = obj->getInt("id"))
						ID = *n;

					obj->set("path", rename);
					obj->set("type", static_cast<conv::JSON::Int>(type));
					
					if (!sources.empty())
					{
						conv::JSON::Arr arr;
						
						for (res::IDResource const source : sources)
							arr.push(source);
						
						obj->set("srcs", std::move(arr));
					}
					// Otherwise leave any existing sources, do not remove them.
					
					return ID;
				}
				
				void Asset::removeResFromDB
				(
					conv::JSON::Val &DB,
					std::string name
				)
				{
					name = pathRelative(name);
					
					auto arr(DB.object()->getArray("resources"));
					
					// Find the resource in the database.
					for (std::size_t i{0}; i < arr->count(); ++ i)
					{
						if (const auto v = arr->get(i))
						{
							if (const auto o = v->object())
							{
								if (const auto p = o->getString("path"))
								{
									if (name == *p)
									{
										// Free the ID up for reüse.
										if (const auto ID = o->getInt("id"))
											clearIDRes(*ID);
										
										// Remove the actual entry.
										arr->erase(arr->begin() + i);
										return;
									}
								}
							}
						}
					}
				}
				
				conv::JSON::Int Asset::nextIDRes()
				{
					if (0 != m_IDsFree.size())
					{
						const auto ID(m_IDsFree.back());
						m_IDsFree.pop_back();
						return ID;
					}

					return (++ m_IDLast);
				}
				
				void Asset::clearIDRes(const conv::JSON::Int &ID)
				{
					if (ID == m_IDLast)
						-- m_IDLast;
					else if (0 != m_IDsFree.size() && ID < m_IDLast)
					{
						auto it(std::find(m_IDsFree.begin(), m_IDsFree.end(), ID));
						if (it == m_IDsFree.end())
							m_IDsFree.emplace_back(ID);
					}
				}
				
				std::string Asset::pathRelative(const std::string &pathFile) const
				{
					namespace fs = boost::filesystem;
					
					fs::path base{m_filepath};
					base.remove_filename();
					base.make_preferred();
					base.remove_trailing_separator();
					base += fs::path::preferred_separator;
					
					fs::path end{m_filepath};
					end.remove_filename();
					end.append(pathFile);
					end.make_preferred();
					end.remove_trailing_separator();
					
					return end.string().substr
					(
						base.string().length(),
						end.string().length() - base.string().length()
					);
				}
				
				std::string Asset::pathDatabase() const
				{
					namespace fs = boost::filesystem;
					
					fs::path p{m_basepath};
					p.append(res::Bank::subpathDB());
					p.make_preferred();
					
					return p.string();
				}
				
				/*
				 * Returns a list of the files associated with
				 * the metadata if there are any other ones.
				 */
				std::vector<std::string> Asset::associatedFiles()
				{
					std::vector<std::string> r;
					
					if (typeIs<res::Shader>())
					{
						conv::JSON::Val data;
						if (!loadMetadata(data))
							return r;
						
						if (const auto sources = data.object()->getArray("sources"))
							for (const auto &s : *sources)
								if (s.string())
									r.emplace_back(*s.string());
						
						if (const auto builds = data.object()->getArray("builds"))
							for (const auto &b : *builds)
								if (const auto o = b.object())
									if (const auto sources = o->getArray("sources"))
										for (const auto &s : *sources)
											if (const auto o = s.object())
												if (const auto f = o->getString("file"))
													r.emplace_back(*f);
					}
					else if (typeIs<res::Texture>())
						r.emplace_back(m_filepath);
					
					return r;
				}
				
				/*
				 * Returns a list of the files associated with
				 * the metadata if there are any other ones.
				 */
				std::vector<std::string> Asset::renamableFiles()
				{
					std::vector<std::string> r;
			
					// Not including files for shaders since they can be built from
					// different sources not necessarily associated with a particular
					// asset that may be reüsed across multiple shaders.
					// Also no need to rename built files; they are hidden from user anyway.
						
					if (typeIs<res::Texture>())
						r.emplace_back(m_filepath);
					
					return r;
				}
			}
		}
	}
}
