/**
 * @author		Ava Skoog
 * @date		2018-10-25
 * @copyright   2017-2023 Ava Skoog
 */

/// @todo: UPDATE ALL CODE TOUCHING DATABASE AND CREATING/REMOVING/CHANGING ASSETS ON DISK/JSON TO USE THE BANK AND FILE ADAPTERS INSTEAD.

/// @todo: SHOULD PROBABLY ADD A NEW TOP LEVEL DIRECTORY TO KARHU CALLED proj/ AND MOVE proj/file/ STUFF IN THERE INSTEAD OF tool/ SO THAT ASSET READING/WRITING CAN BE ABSTRACTED INTO ONE SINGLE PLACE AND NO OTHER PLACE TOUCH THE JSON DIRECTLY. Do have to figure out how to deal with loading the files tho since filesystem isn't in there and the engine has its own SDL stuff, so probably need to write an abstract interface and the engine and the toolkit will have their own version to talk to the shared project classes.
/// @todo: Figure out some way to deal with manipulation of shader sources since they are different from shader assets and can be used across multiple.
/// @todo: Add a 'move' action that moves an asset along with all its associated files to a different subdirectory of res/ (but again might not want to move shader sources for the reasons outline above as to how they differ from shader assets).

#ifndef KARHU_TOOL_PROJ_CMD_ASSET_H_
	#define KARHU_TOOL_PROJ_CMD_ASSET_H_

	#include <karhu/tool/Command.hpp>
	#include <karhu/tool/proj/cmd/Base.hpp>
	#include <karhu/tool/shared/AdapterResourcebank.hpp>
	#include <karhu/conv/types.hpp>
	#include <karhu/core/meta.hpp>

	#include <vector>
	#include <string>

	namespace tinygltf
	{
		class Model;
	}

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace cmd
				{
					/**
					 * Manipulates a Karhu project asset.
					 *
					 * Syntax:
					 * asset --act <create|remove|edit|rename|refresh|build> --type <texture|…> --name <file> [(act=rename) --rename <file>] [(act=create) --srcs <file> ...] [(act=edit) --key parent child ...] [(act=edit) --val <JSON>] [(act=build&type=shader) --format <gl|gles|metal>] [--dir <path>=current]
					 */
					class Asset : public Command, private Base
					{
						private:
							enum class Action
							{
								invalid,
								create,
								remove,
								edit,
								rename,
								build,
								refresh
							};

						private:
							constexpr static Action strToAct(const char *s) noexcept
							{
								if (meta::string::equal(s, "create"))  return Action::create;
								if (meta::string::equal(s, "remove"))  return Action::remove;
								if (meta::string::equal(s, "edit"))    return Action::edit;
								if (meta::string::equal(s, "rename"))  return Action::rename;
								if (meta::string::equal(s, "build"))   return Action::build;
								if (meta::string::equal(s, "refresh")) return Action::refresh;
								return Action::invalid;
							}

						public:
							Asset()
							:
							Command
							{
								Param{"act",    Enforce::required, ArgsMinMax{1}},
								Param{"type",   Enforce::optional, ArgsMinMax{1}},
								Param{"name",   Enforce::optional, ArgsMinMax{1}},
								Param{"rename", Enforce::optional, ArgsMinMax{1}},
								Param{"srcs",   Enforce::optional, ArgsMinMax{1, -1}},
								Param{"key",    Enforce::optional, ArgsMinMax{1, -1}},
								Param{"val",    Enforce::optional, ArgsMinMax{1}},
								Param{"format", Enforce::optional, ArgsMinMax{1}},
								Param{"dir",    Enforce::optional, ArgsMinMax{1}}
							}
							{
							}

							bool processOpt(const OptWithVals &) override;
							bool finish() override;

						private:
							struct Creation
							{
								std::string
									name, metapath;
								
								res::IDTypeResource
									type;
								
								conv::JSON::Val
									data;
							};
						
							template<typename T> bool typeIs() const;
						
							bool create(conv::JSON::Val *DB = nullptr);
							bool edit();
							bool remove();
							bool rename();
							bool build();
							bool refresh();
							bool refreshResourcesInDirectory(std::string const &, conv::JSON::Val &DB);
							bool refreshResource(std::string const &, conv::JSON::Val &DB);
						
							std::vector<Creation> createShader();
							std::vector<Creation> createTexture();
							std::vector<Creation> createMesh();
							std::vector<Creation> createRig();
							std::vector<Creation> createAnimation();
							std::vector<Creation> createScript();
						
							bool buildShader(conv::JSON::Val *DB = nullptr);

							bool updatePaths();
							bool validateExistence(const bool alsoCheckRenamed = false);
							bool validateSourcecount();
							bool validateSources();

							void initDatabase(conv::JSON::Val &);
							bool loadDatabase(conv::JSON::Val &);
							bool saveDatabase(conv::JSON::Val &);
						
							bool loadMetadata(conv::JSON::Val &);
							bool saveMetadata(conv::JSON::Val const &);
						
							conv::JSON::Int addResToDB
							(
								conv::JSON::Val &DB,
								res::IDTypeResource const type,
								std::string name,
								std::string rename,
								std::vector<res::IDResource> const &sources = {}
							);
						
							void removeResFromDB
							(
								conv::JSON::Val &DB,
								std::string name
							);
						
							conv::JSON::Int nextIDRes();
							void clearIDRes(const conv::JSON::Int &);

							std::string pathRelative(const std::string &pathFile) const;
							std::string pathDatabase() const;
							std::vector<std::string> associatedFiles();
							std::vector<std::string> renamableFiles();
						
							std::unique_ptr<AdapterResourcebank> m_bank;

						private:
							Action m_act{Action::invalid};
							res::IDTypeResource m_IDType;
						
							std::string
								m_type,
								m_name,
								m_rename,
								m_val,
								m_format,
								m_dir,
								m_basepath,
								m_filepath,
								m_metapath;

							std::vector<std::string>
								m_srcs,
								m_key;
						
							std::vector<conv::JSON::Int> m_IDsFree;
							conv::JSON::Int m_IDLast;
					};
					
					template<typename T>
					bool Asset::typeIs() const
					{
						return (m_IDType == m_bank->template IDOfType<T>());
					}
				}
			}
		}
	}
#endif
