/**
 * @author		Ava Skoog
 * @date		2017-04-02
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/cmd/Sync.hpp>
#include <karhu/tool/proj/Location.hpp>
#include <karhu/tool/proj/Identifier.hpp>
#include <karhu/tool/proj/file/Project.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/log.hpp>

#include <boost/process.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool Sync::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if (n == "acts")
					{
						if (validPreacts(v))
							m_acts = v;
						else
							return false;
					}
					else if (n == "m")   m_message = v[0];
					else if (n == "dir") m_dir     = v[0];

					return true;
				}

				/**
				 * Uses the validated information to set up the new project.
				 */
				bool Sync::finish()
				{				
					namespace scripts = SDK::paths::home::scripts;

					if (!loadProject(m_dir)) return false;
					
					if (m_message.length() == 0) m_message = "Automated commit.";

					if (std::find(m_acts.begin(), m_acts.end(), "push") != m_acts.end())
					{
						auto p(SDK::env::home(scripts::push::UNIX()));
						if (p && boost::filesystem::exists(*p))
						{
							std::vector<std::string> args;
							args.emplace_back(project().location().path());
							args.emplace_back(m_message);

							boost::process::child c{*p, args};
							c.wait();
							if (0 != c.exit_code())
							{
								log::err() << "Push failed.";
								return false;
							}
						}
						else
						{
							log::err() << "Could not find push script for this target and platform.";
							return false;
						}
					}
					
					if (std::find(m_acts.begin(), m_acts.end(), "pull") != m_acts.end())
					{
						auto p(SDK::env::home(scripts::pull::UNIX()));
						if (p && boost::filesystem::exists(*p))
						{
							std::vector<std::string> args;
							args.emplace_back(project().location().path());

							boost::process::child c{*p, args};
							c.wait();
							if (0 != c.exit_code())
							{
								log::err() << "Pull failed.";
								return false;
							}
						}
						else
						{
							log::err() << "Could not find pull script for this target and platform.";
							return false;
						}
					}
					
					if (std::find(m_acts.begin(), m_acts.end(), "update") != m_acts.end())
						if (!update()) return false;
					
					return true;
				}
				
				bool Sync::update()
				{
					/// @todo: This needs an overhaul…
					namespace p  = SDK::paths::home::project;
					namespace fs = boost::filesystem;
					
					static std::vector<std::string> files
					{
					};
					
					if (!loadInfo()) return false;
					if (!loadBuild()) return false;
					
					const auto templatename(project().info()->templatename());
					if (!templatename)
					{
						log::err() << "Failed to get template name from project info file.";
						return false;
					}
					
					if (const auto templatedir = SDK::env::home(p::templatedir(*templatename)))
					{
						if (!fs::exists(*templatedir))
						{
							log::err() << "Invalid project template '" << *templatename << "'.";
							return false;
						}
						
						fs::path base{*templatedir};
						
						for (auto &i : files)
						{
							fs::path source{base}, dest{project().location().path()};
							
							source.append(i);
							source.make_preferred();
							
							if (!fs::exists(source))
							{
								log::err() << "Could not find template file at path '" << source.string() << "'";
								return false;
							}
							
							dest.append(i);
							dest.make_preferred();

							fs::copy_file(source, dest, fs::copy_option::overwrite_if_exists);
						}
					}
					
					if (!project().info()->save())
					{
						log::err() << "Failed to save info file.";
						return false;
					}
					
					if (!project().build()->save())
					{
						log::err() << "Failed to save build file.";
						return false;
					}
					
					return true;
				}
			}
		}
	}
}
