/**
 * @author		Ava Skoog
 * @date		2017-07-02
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/cmd/Base.hpp>
#include <karhu/tool/proj/Location.hpp>
#include <karhu/tool/proj/Identifier.hpp>
#include <karhu/tool/proj/file/Project.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/log.hpp>

#include <iostream>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			namespace cmd
			{				
				bool Base::createNew
				(
					const std::string &directory,
					std::string templatename,
					const Nullable<proj::Identifier> &identifier,
					const std::string &fullname
				)
				{
					if (templatename.length() == 0)
						templatename = SDK::values::project::defaulttemplate();
				
					// Validate the name.
					if (fullname.length() == 0)
					{
						log::err() << "Invalid name ''";
						return false;
					}

					// Validate the intended location.
					proj::Location location{directory};
					if (!location.valid())
					{
						log::err() << "Invalid location '" << location.path() << "'";
						return false;
					}
					
					// Validate the identifier.
					if (!identifier)
					{
						log::err() << "Invalid identifier.";
						return false;
					}
					
					// Try to create the actual project.
					auto project(proj::file::Project::create(*identifier, templatename, location));
					if (!project)
					{
						log::err() << "Failed to create project.";
						return false;
					}
					
					// Try to get and update the info file.
					auto info(project->info());
					if (!info)
					{
						log::err() << "Failed to get info file.";
						return false;
					}
					
					info->templatename(templatename);
					info->name(fullname);
					info->identifier(*identifier);

					info->save();

					return true;
				}
			}
		}
	}
}
