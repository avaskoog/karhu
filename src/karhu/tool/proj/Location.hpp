/**
 * @author		Ava Skoog
 * @date		2017-04-04
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_LOCATION_H_
	#define KARHU_TOOL_PROJ_LOCATION_H_

	#include <karhu/core/Nullable.hpp>
	#include <string>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				/**
				 * Abstracts a project directory path that can be used to interface with
				 * project objects that need to access files within that directory.
				 */
				class Location
				{
					private:
						static std::string concludedPath(const std::string &pathToDirectory);

					public:
						/**
						 * Tries to construct the location from the specified path,
						 * or if that is omitted, the current working directory.
						 *
						 * @param pathToDirectory The optional path to the directory in which to find the project.
						 */
						Location(const std::string &pathToDirectory = {});

						Location(const Location &) = default;
						Location(Location &&) = default;
					
						Location &operator=(const Location &) = default;
						Location &operator=(Location &&) = default;

						/**
						 * Returns whether the location object holds a valid path.
						 *
						 * @return Whether valid.
						 */
						bool valid() const noexcept { return m_valid; }

						/**
						 * Returns the path of the location.
						 *
						 * @return Path as string.
						 */
						const std::string &path() const noexcept { return m_path; }
					
					private:
						std::string makePath(const std::string &) const;
					
					private:
						std::string m_path;
						bool m_valid{false};
				};
			}
		}
	}
#endif
