/**
 * @author		Ava Skoog
 * @date		2017-04-04
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/proj/Location.hpp>
#include <karhu/tool/SDK.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

namespace karhu
{
	namespace tool
	{
		namespace proj
		{
			Location::Location(const std::string &pathToDirectory)
			:
			m_path {makePath(pathToDirectory)},
			m_valid{boost::filesystem::exists(m_path)}
			{
			}
			
			std::string Location::makePath(const std::string &from) const
			{
				boost::filesystem::path p;

				if (from.size() == 0)
					p = boost::filesystem::current_path();
				else if (from == "karhu")
				{
					const auto dir(SDK::env::home(SDK::paths::home::lib::projdir()));
					if (dir) p = *dir;
				}
				else
					p = from;

				p.make_preferred();
				return p.string();
			}
		}
	}
}
