/**
 * @author		Ava Skoog
 * @date		2017-04-04
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_PROJ_IDENTIFIER_H_
	#define KARHU_TOOL_PROJ_IDENTIFIER_H_

	#include <karhu/core/Nullable.hpp>
	#include <string>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				/**
				 * Interfaces a project identifier, which is split up into three parts:
				 * extension, organisation and project.
				 *
				 * These are usually found together separated by dots (e.g. com.author.game).
				 *
				 * To ensure valid identifiers, these can only be created through static
				 * functions that validate the input or return nothing. Copyable and movable.
				 */
				class Identifier
				{
					public:
						static Identifier defaults() { return {"com", "author", "game"}; }
						static Nullable<Identifier> create
						(
							const std::string &extension,
							const std::string &organisation,
							const std::string &project
						);
						static Nullable<Identifier> create
						(
							const std::string &fullStringSeparatedbyDots
						);

					public:
						Identifier(Identifier const &) = default;
						Identifier(Identifier &&) = default;
						Identifier &operator=(Identifier const &) = default;
						Identifier &operator=(Identifier &&) = default;
					
						Identifier &ext(const std::string &value) { m_ext = value; return *this; }
						const std::string &ext() const { return m_ext; }
						
						Identifier &org(const std::string &value) { m_org = value; return *this; }
						const std::string &org() const { return m_org; }
						
						Identifier &proj(const std::string &value) { m_proj = value; return *this; }
						const std::string &proj() const { return m_proj; }
					
						std::string full() const;

					private:
						Identifier() = default;
						Identifier(const std::string &ext, const std::string &org, const std::string &proj)
						:
						m_ext {ext},
						m_org {org},
						m_proj{proj}
						{
						}

					private:
						std::string m_ext, m_org, m_proj;

					friend class Nullable<Identifier>;
				};
			}
		}
	}
#endif
