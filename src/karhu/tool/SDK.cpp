/**
 * @author		Ava Skoog
 * @date		2017-04-16
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/SDK.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

namespace karhu
{
	namespace SDK
	{
		namespace paths
		{
			namespace home
			{
				namespace project
				{
					std::string templatedir(const std::string &subfolder)
					{
						boost::filesystem::path p{"templates"};
						
						if (subfolder.length() > 0)
							p.append(subfolder);
						else
							p.append(values::project::defaulttemplate());

						p.make_preferred();
						
						return p.string();
					}
				}
			}
		}

		namespace env
		{
			Nullable<std::string> home(const std::string &subpath)
			{
				if (const auto dir = std::getenv(keys::home()))
				{
					if (subpath.length() > 0)
					{
						boost::filesystem::path p{dir};
						p.append(subpath);
						p.make_preferred();
						return {true, p.string()};
					}
					else
						return {true, dir};
				}
				else
					return {};
			}
		}
	}
}
