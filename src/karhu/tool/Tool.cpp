/**
 * @author		Ava Skoog
 * @date		2017-04-02
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/Tool.hpp>

namespace karhu
{
	namespace tool
	{
		int Tool::run(const int &argc, char *argv[])
		{
			if (argc >= 2)
			{
				std::vector<std::string> args;
				for (int i{2}; i < argc; ++ i)
					args.push_back(argv[i]);
				
				return run(argv[1], std::move(args));
			}
			else
			{
				log::err() << "No input command.";
				listCommands(log::Level::err);
			}
			
			return 1;
		}

		int Tool::run(const std::string &command, std::vector<std::string> &&args)
		{
			auto cmd(m_commands.find(command));
			if (cmd != m_commands.end())
			{
				cmd->second->addArgs(std::move(args));
				return cmd->second->run();
			}
			else
			{
				log::err() << "Invalid command '" << command << "'";
				listCommands(log::Level::err);
			}

			return 1;
		}

		void Tool::listCommands(const log::Level level) const
		{
			log::lvl(level, "") << "Commands are:";
			
			for (auto &i : m_commands)
				log::lvl(level, "") << " • " << i.first;
		}
	}
}
