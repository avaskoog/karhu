/**
 * @author		Ava Skoog
 * @date		2017-04-02
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/Command.hpp>
#include <karhu/core/log.hpp>
#include <algorithm>
#include <iostream>

namespace karhu
{
	namespace tool
	{
		int Command::run()
		{
			while (argsLeft())
			{
				auto next(consumeOptWithVals());
				if (!next.opt.valid) return -1;
				if (!processOpt(next)) return -1;
			}

			// Check for missed required parameters.
			for (auto &i : m_opts)
			{
				if (Enforce::required == i.enforce && !i.consumed)
				{
					log::err() << "Missing required option '--" << i.name << "'.";
					listOpts(log::Level::err);
					return -1;
				}
			}
			
			return (finish()) ? 0 : -1;
		}

		void Command::listOpts(const log::Level level) const
		{
			log::lvl(level, "") << "Options are:";
			
			for (auto &i : m_opts)
				log::lvl(level, "") << " • --" << i.name << ((Enforce::optional == i.enforce) ? " (optional)" : " (required)");
		}

		Command::Opt Command::consumeOpt()
		{
			Opt r{false, false};

			if (argsLeft())
			{
				auto &a(consumeArg());
				r.name = a;

				if (a.length() > 2 && a.substr(0, 2) == "--")
				{
					r.name = a.substr(2, a.length() - 2);
					r.opt = true;

					auto i(std::find_if
					(
						m_opts.begin(),
						m_opts.end(),
						[&a, &r](auto &x)
						{
							return (x.name == r.name);
						}
					));

					if (i != m_opts.end())
					{
						i->consumed = true;
						r.valid = true;
						r.param = &*i;
					}
					else
					{
						log::err() << "Invalid option '" << a << "'.";
						listOpts(log::Level::err);
					}
				}
			}
			
			return r;
		}

		Command::OptWithVals Command::consumeOptWithVals()
		{
			OptWithVals r;
			
			if (argsLeft())
			{
				r.opt = consumeOpt();
				if (r.opt.valid)
				{
					while (argsLeft())
					{
						const auto arg(consumeOpt());

						// Stop if this is a valid opt.
						if (arg.opt)
						{
							unconsume();
							break;
						}
						// Otherwise add as arg.
						else
							r.vals.emplace_back(std::move(arg.name));
					}
				}
			
				if (r.opt.valid && r.opt.param)
				{
					if (r.vals.size() < r.opt.param->args.min)
					{
						log::err() << "Option '--" << r.opt.name << "' requires at least " << r.opt.param->args.min << " argument(s), but " << r.vals.size() << " provided.";
						r.opt.valid = false;
					}
					else if (r.opt.param->args.max >= 0 && r.vals.size() > r.opt.param->args.max)
					{
						log::err() << "Option '--" << r.opt.name << "' accepts no more than " << r.opt.param->args.max << " argument(s), but " << r.vals.size() << " provided.";
						r.opt.valid = false;
					}
				}
			}

			return r;
		}
	}
}
