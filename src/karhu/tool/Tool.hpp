/**
 * @author		Ava Skoog
 * @date		2017-04-02
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_TOOL_H_
	#define KARHU_TOOL_TOOL_H_
	#include <karhu/tool/Command.hpp>
	#include <karhu/core/log.hpp>

	#include <memory>
	#include <map>
	#include <type_traits>
	#include <iostream>

	namespace karhu
	{
		namespace tool
		{
			class Tool
			{
				public:
					int run(const int &argc, char *argv[]);
					int run(const std::string &command, std::vector<std::string> &&args);

				protected:
					template<class T>
					void registerCommand(const std::string &name)
					{
						static_assert(std::is_base_of<Command, T>{}, "Type must be derived from karhu::tool::Command");
						m_commands.emplace(name, std::unique_ptr<Command>{new T});
					}

				private:
					void listCommands(const log::Level) const;

				private:
					std::map<std::string, std::unique_ptr<Command>> m_commands;
			};
		}
	}
#endif
