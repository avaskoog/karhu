/**
 * @author		Ava Skoog
 * @date		2017-07-10
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_NETWORK_SERVER_H_
	#define KARHU_TOOL_NETWORK_SERVER_H_

	#include <karhu/network/Interface.hpp>
	#include <karhu/tool/SDK.hpp>
	#include <karhu/core/log.hpp>

	/// @todo: Replace with C++17 STL when available on all platforms.
	#include <karhu/tool/lib/asio/asio.hpp>
	#include <boost/process.hpp>
	#include <boost/filesystem.hpp>

	#include <memory>

	namespace karhu
	{
		namespace tool
		{
			namespace network
			{
				template<typename TChar, std::size_t SizeBuffer>
				class MessengerServer : public karhu::network::Messenger<TChar, SizeBuffer>
				{
					public:
						using TMessenger = karhu::network::Messenger<TChar, SizeBuffer>;
						using TMessage   = typename TMessenger::TMessage;
						using TBuffer    = typename TMessage::Buffer;

					public:
						using TMessenger::Messenger;

					protected:
						bool performRead(TBuffer &) override;
						bool performSend(const TBuffer &) override;
				};

				/**
				 * This is a basic server implementation for the tools.
				 * It is meant to be derived in order to implement any
				 * actual specific server functionality for a particular
				 * tool.
				 *
				 * It currently uses Asio, but this may change, and no
				 * reference or include is done in the header.
				 */
				template<typename TChar, std::size_t SizeBuffer>
				class Server : public karhu::network::Interface<MessengerServer<TChar, SizeBuffer>, karhu::network::TypeInterface::server>
				{
					public:
						struct FunctorAsync
						{
							Server<TChar, SizeBuffer> *s;
							void operator()(const std::error_code &);
						};
					
					public:
						using TMessenger = MessengerServer<TChar, SizeBuffer>;
						using TMessage   = typename TMessenger::TMessage;
						using TBuffer    = typename TMessage::Buffer;
					
					public:
						static std::string hostname(const std::uint16_t port);

					public:
						Server() : karhu::network::Interface<TMessenger, karhu::network::TypeInterface::server>{this} {}
					
					protected:
						bool performConnect(const Platform, const std::uint16_t, const std::string &) override;
						bool performConnectAsync(const Platform, const std::uint16_t, const std::string &) override;
						void performDisconnect() override;
						void onDisconnect() override;
						void onUpdate() override;

					private:
						bool websockify(const std::uint16_t port);
					
					private:
						std::unique_ptr<asio::io_context>        m_IO;
						std::unique_ptr<asio::ip::tcp::acceptor> m_TCP;
						std::unique_ptr<asio::ip::tcp::socket>   m_socket;

					friend class MessengerServer<TChar, SizeBuffer>;
					friend struct FunctorAsync;
				};
				
				template<typename TChar, std::size_t SizeBuffer>
				bool MessengerServer<TChar, SizeBuffer>::performRead(TBuffer &b)
				{
					auto d(reinterpret_cast<Server<TChar, SizeBuffer> *>(TMessenger::data()));
					auto &socket(d->m_socket);

					if (!socket) return false;

					asio::socket_base::bytes_readable command{true};
					socket->io_control(command);
					const std::size_t readable{command.get()};

					if (readable == 0) return false;
					
					asio::error_code err;
					std::size_t bytecount{socket->read_some(asio::buffer(b), err)};

					return (bytecount > 0 && !err);
				}
				
				template<typename TChar, std::size_t SizeBuffer>
				bool MessengerServer<TChar, SizeBuffer>::performSend(const TBuffer &b)
				{
					auto d(reinterpret_cast<Server<TChar, SizeBuffer> *>(TMessenger::data()));
					auto &socket(d->m_socket);

					if (!socket) return false;

					asio::error_code err;
					std::size_t bytecount{asio::write(*socket, asio::buffer(b), err)};

					return (bytecount > 0 && !err);
				}
				
				template<typename TChar, std::size_t SizeBuffer>
				void Server<TChar, SizeBuffer>::FunctorAsync::operator()
				(
					const std::error_code &e
				)
				{
					if (!e)
					{
						s->m_state = karhu::network::State::connected;
						log::msg("Karhu") << "Client connected";
					}
					else
					{
						s->m_state = karhu::network::State::disconnected;
						log::err("Karhu") << "Connection failed: " << e.message();
					}
				}
				
				template<typename TChar, std::size_t SizeBuffer>
				std::string Server<TChar, SizeBuffer>::hostname
				(
					const std::uint16_t port
				)
				{
					// Android has issues resolving IPv4 on newer versions,
					// but this can be solved by passing the IP directly
					// instead of the hostname to the client, so if this
					// function is able to resolve the IP it will return
					// that directly instead of the host's name.
					
					std::string r;
				
					asio::io_service io_service;
					asio::ip::tcp::resolver resolver(io_service);
					asio::ip::tcp::resolver::query query(asio::ip::host_name(), std::to_string(port));
					asio::error_code ec;
					asio::ip::tcp::resolver::iterator iter = resolver.resolve(query, ec);
					
					if (!ec)
						r = iter->endpoint().address().to_string();
					
					if (r.empty())
						r = asio::ip::host_name();
					
					return r;
				}

				template<typename TChar, std::size_t SizeBuffer>
				bool Server<TChar, SizeBuffer>::performConnect
				(
					const Platform       system,
					const std::uint16_t  port,
					const std::string   &
				)
				{
					namespace server = karhu::network::protocol::server;

					if (platform::is(system, Platform::web))
						if (!websockify(port))
							return false;

					try
					{
						m_IO = std::make_unique<asio::io_context>();
						
						m_TCP = std::make_unique<asio::ip::tcp::acceptor>
						(
							*m_IO,
							asio::ip::tcp::endpoint
							{
								asio::ip::tcp::v4(),
								static_cast<unsigned short>(port)
							}
						);
						
						log::msg("Karhu") << "Firing up socket on port " << port << "...";

						m_socket = std::make_unique<asio::ip::tcp::socket>(*m_IO);
						
						/// @todo: Set server connection timeout, see: https://www.boost.org/doc/libs/1_52_0/doc/html/boost_asio/example/timeouts/blocking_tcp_client.cpp
						m_TCP->accept(*m_socket);
						
						log::msg("Karhu") << "Client connected";
					}
					catch (std::exception &e)
					{
						log::err("Karhu") << "Connection failed: " << e.what();
						return false;
					}

					return true;
				}

				template<typename TChar, std::size_t SizeBuffer>
				bool Server<TChar, SizeBuffer>::performConnectAsync
				(
					const Platform       system,
					const std::uint16_t  port,
					const std::string   &
				)
				{
					namespace server = karhu::network::protocol::server;

					if (platform::is(system, Platform::web))
						if (!websockify(port))
							return false;

					try
					{
						m_IO = std::make_unique<asio::io_context>();
						
						m_TCP = std::make_unique<asio::ip::tcp::acceptor>
						(
							*m_IO,
							asio::ip::tcp::endpoint
							{
								asio::ip::tcp::v4(),
								static_cast<unsigned short>(port)
							}
						);
						
						log::msg("Karhu") << "Firing up socket on port " << port << "...";

						m_socket = std::make_unique<asio::ip::tcp::socket>(*m_IO);
						
						/// @todo: Set server connection timeout, see: https://www.boost.org/doc/libs/1_52_0/doc/html/boost_asio/example/timeouts/blocking_tcp_client.cpp
						m_TCP->async_accept
						(
							*m_socket,
							FunctorAsync{this}
						);
					}
					catch (std::exception &e)
					{
						log::err("Karhu") << "Connection failed: " << e.what();
						return false;
					}

					return true;
				}

				template<typename TChar, std::size_t SizeBuffer>
				void Server<TChar, SizeBuffer>::performDisconnect()
				{
					m_socket.reset();
				}

				template<typename TChar, std::size_t SizeBuffer>
				void Server<TChar, SizeBuffer>::onDisconnect()
				{
					m_socket.reset();
				}
				
				template<typename TChar, std::size_t SizeBuffer>
				void Server<TChar, SizeBuffer>::onUpdate()
				{
					if (m_socket)
						// Necessary for asynchronous connection.
						m_socket->get_io_service().run();
				}
				
				template<typename TChar, std::size_t SizeBuffer>
				bool Server<TChar, SizeBuffer>::websockify(const std::uint16_t port)
				{
					namespace scripts = SDK::paths::home::scripts;

					std::string path;

					auto p(SDK::env::home(scripts::server::web()));
					if (p && boost::filesystem::exists(*p))
						path = std::move(*p);
					else
					{
						log::err("Karhu") << "Web server script not found.";
						return false;
					}
					
					std::vector<std::string> args;
					args.emplace_back(asio::ip::host_name());

					std::stringstream s;
					s << port;
					args.emplace_back(s.str());

					boost::process::spawn(path, args);
					
					return true;
				}
			}
		}
	}
#endif
