/**
 * @author		Ava Skoog
 * @date		2017-07-13
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/network/ServerDebug.hpp>
#include <karhu/tool/proj/file/Project.hpp>
//#include <karhu/tool/code/Hotloader.hpp>
//#include <karhu/tool/proj/file/build/common.hpp>
#include <karhu/network/Protocol.hpp>
#include <karhu/core/log.hpp>
#include <karhu/core/string.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

#include <chrono>
#include <thread>

/// @todo: Fix all the messy code reduplication here and probably move the file generation and recompilation out to Generator or such.
/// @todo: Recompiling the modules should be done before the client is even started and they should be loaded immediately on connect, no delay.
namespace karhu
{
	namespace tool
	{
		namespace network
		{
			ServerDebug::ServerDebug
			(
				proj::file::Project &project
//				,
//				const proj::file::build::config::Type &conf
			)
			:
			m_project{project}
//			,
//			m_conf   {conf}
			{
			}
			/*
			bool ServerDebug::prepareModules()
			{
				namespace fs     = boost::filesystem;
				namespace config = proj::file::build::config;

				const auto build(m_project.build());
				if (!build)
				{
					log::err("Karhu") << "Failed to load project build settings";
					return false;
				}
				
				for (const auto &target : build->targets())
				{
					if (const auto object = target.second.data().object())
					{
						const auto type(object->getString("type"));
						if (!type)
						{
							log::err("Karhu") << "Required option 'type' missing from target '" << target.first << "'";
							return false;
						}
						
						if (*type == "module")
						{
							const auto root(object->getString("root"));
							if (!root)
							{
								log::err("Karhu") << "Required option 'root' missing from target module '" << target.first << "'";
								return false;
							}

							// Watch the source code directory.
							{
								fs::path p{m_project.location().path()};
								p.append(*root);

								if (!addDirWatch(p.string())) return false;
							}

							// Watch the library output directory.
							{
								fs::path p{m_project.location().path()};
								/// @todo: Maybe not hardcode this whole thing as much?
								p.append("build");
								p.append(platform::identifier(Platform::current)); /// @todo: What if it is a different platform?
								p.append("bin");
								p.append(target.first);
								p.append(config::toString(m_conf)); /// @todo: Not hardcode?

								if (!addDirWatch(p.string())) return false;
							}
						}
					}
				}
				
				return true;
			}
			*/
			void ServerDebug::handleFileAction
			(
				FW::WatchID watchid,
				const FW::String &dir,
				const FW::String &filename,
				FW::Action action
			)
			{
				/// @todo: Instead of sleeping, just check timestamps with filesystem and check if difference is larger than 10 ms or whatever, like in the editor filewatcher.
				
				using namespace std::literals::chrono_literals;
				
				namespace fs = boost::filesystem;

				using P = karhu::network::Protocol;
				using T = P::TypeMessage;
				
				/// @todo: Make sure non-Mac platforms also provide full path and not just filename despite docs.
				fs::path p{filename};
				p.make_preferred();
				
				if (fs::exists(p))
				{
					if (fs::is_regular_file(p))
					{/*
						if (p.extension() == ".dylib") /// @todo: Check for other endings on other systems.
						{
							// We are only interested in added libraries.
							if (FW::Actions::Add != action) return;
							
							std::string modulename{p.stem().string()};
							
							if (modulename.length() > 3)
							{
								/// @todo: Do not do this on systems that do not prepend lib-.
								modulename = modulename.substr(3, modulename.length() - 3);

								log::msg("Karhu") << "Asking client to reload module...";
								messenger().send(P::serialise(T::module, conv::JSON::Arr{modulename, p.string()})); /// @todo: Fix final path without lib prefix.
								
								std::this_thread::sleep_for(1000ms); /// @todo: This is a hack for now to avoid reacting to multiple modified events getting fired by the same modification… Figure out how to ACTUALLY solve this.
							}
						}
						else if (p.extension() == ".hpp" || p.extension() == ".cpp") /// @todo: Do more to figure out if it's really a source file?
						{
							if (FW::Actions::Delete == action) return;

							/// @todo: Figure out if it's actually supposed to be compiled into the dynamic library by reading what module it belongs to.
							log::msg("Karhu")
								<< "Detected that file '"
								<< p.filename().string()
								<< "' was "
								<< ((FW::Actions::Add == action) ? "added" : "modified")
								<< '.';

							std::this_thread::sleep_for(1000ms); /// @todo: This is a hack for now to avoid reacting to multiple modified events getting fired by the same modification… Figure out how to ACTUALLY solve this.

							code::Hotloader hotloader;
							hotloader.refreshModules(m_project, m_conf, true);
						}*/
					}
				}
			}

			bool ServerDebug::updateAfterMessages()
			{
				m_filewatcher.update();
				
				using namespace std::literals::chrono_literals;
				std::this_thread::sleep_for(10ms);

				return true;
			}

			bool ServerDebug::receive(const TMessage &message)
			{
				using T = karhu::network::Protocol::TypeMessage;

				const auto text(message.text());
				
				if ("connect" == text) /// @todo: This necessary?
					log::msg("Karhu") << "Client connected";
				else if ("disconnect" == text)
				{
					log::msg("Karhu") << "Disconnecting...";
					return false;
				}

				if (auto v = karhu::network::Protocol::deserialise(text))
				{
					switch (v->type)
					{
						case T::log:
						{
							karhu::network::Protocol::MessageLog m;
							
							if (karhu::network::Protocol::log(*v, m))
								performLog(m.level, m.tag, m.text);
							
							break;
						}

						default:
							break;
					}
				}

				return true;
			}
			
			void ServerDebug::performLog
			(
				const log::Level    level,
				const conv::String &tag,
				const conv::String &text
			)
			{
				log::lvl(level, tag) << text;
			}

			bool ServerDebug::addDirWatch(const std::string &path)
			{
				namespace fs = boost::filesystem;
				
				fs::path p{path};
				p.make_preferred();
				
				if (!fs::exists(p) || !fs::is_directory(p))
				{
					log::wrn("Karhu") << "Directory '" << p.string() << "' specified for watching does not exist";
					return false;
				}

				try
				{
					m_filewatcher.addWatch(p.string(), this);
				}
				catch (FW::FileNotFoundException &e)
				{
					log::wrn("Karhu") << "Failed to add directory '" << p.string() << " for watching'";
					return false;
				}

				return true;
			}
			
			ServerDebug::~ServerDebug()
			{
				if (karhu::network::State::connected == state())
					messenger().send(TMessage{"disconnect"});
			}
		}
	}
}
