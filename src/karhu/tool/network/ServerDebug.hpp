/**
 * @author		Ava Skoog
 * @date		2017-07-13
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_NETWORK_SERVER_DEBUG_H_
	#define KARHU_TOOL_NETWORK_SERVER_DEBUG_H_

	#include <karhu/tool/network/Server.hpp>
	#include <karhu/network/Protocol.hpp>
	#include <karhu/core/Nullable.hpp>
	#include <karhu/tool/proj/file/build/common.hpp>

	/// @todo: Use esfw instead like the editor, if this tool is to remain at all.
	#include <FileWatcher/FileWatcher.h>

	#include <memory>

	namespace karhu
	{
		namespace tool
		{
			namespace proj
			{
				namespace file
				{
					class Project;
				}
			}

			namespace network
			{
				/**
				 * This is the server that can talk to the debug build of
				 * a Karhu application it and receive its logs, for use in
				 * debugging and tools such as an editor.
				 */
				class ServerDebug
				:
				public Server<conv::Char, karhu::network::protocol::buffersize()>,
				public FW::FileWatchListener
				{
					public:
						ServerDebug
						(
							proj::file::Project &project
//							,
//							const proj::file::build::config::Type &conf
						);
					
//						bool prepareModules();
					
						~ServerDebug();
					
					public:
						void handleFileAction
						(
							FW::WatchID watchid,
							const FW::String &dir,
							const FW::String &filename,
							FW::Action action
						) override;

					protected:
						bool updateAfterMessages() override;
						bool receive(const TMessage &message) override;

					protected:
						virtual void performLog
						(
							const log::Level,
							const conv::String &tag,
							const conv::String &text
						);
					
					private:
						bool addDirWatch(const std::string &path);
					
					private:
						FW::FileWatcher m_filewatcher;
						proj::file::Project &m_project;
						proj::file::build::config::Type m_conf;
				};
			}
		}
	}
#endif
