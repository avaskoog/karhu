/**
 * @author		Ava Skoog
 * @date		2017-04-02
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_COMMAND_H_
	#define KARHU_TOOL_COMMAND_H_

	#include <karhu/core/log.hpp>

	#include <string>
	#include <vector>
	#include <iostream>

	namespace karhu
	{
		namespace tool
		{
			class Command
			{
				public:
					enum class Enforce
					{
						required,
						optional
					};
				
					struct ArgsMinMax
					{
						unsigned min{0};
						int max{0};
					};

					struct Param
					{
						std::string name;
						Enforce enforce{Enforce::optional};
						ArgsMinMax args;
						bool consumed{false};
					};

					struct Opt
					{
						bool opt, valid;
						std::string name;
						Param *param{nullptr};
					};

					struct OptWithVals
					{
						Opt opt;
						std::vector<std::string> vals;
					};

				public:
					template<typename... Ts>
					Command(Ts... opts)
					{
						registerOpt(opts...);
					}

					void addArgs(std::vector<std::string> &&args)
					{
						m_args = args;
					}

					int run();

					virtual ~Command() = default;

				protected:
					virtual bool processOpt(const OptWithVals &) = 0;
					virtual bool finish() = 0;
				
				private:
					void listOpts(const log::Level) const;

					bool argsLeft() const { return m_index < m_args.size(); }
					const std::string &consumeArg() { return m_args[m_index ++]; }
					Opt consumeOpt();
					OptWithVals consumeOptWithVals();
					void unconsume() { m_index = (m_index > 0) ? (m_index - 1) : 0; }

					template<typename First, typename... Rest>
					void registerOpt(First &&first, Rest &&... rest)
					{
						//static_assert(std::is_same<Param, First>{}, "Opts have to be param structs");
						
						if (first.args.max >= 0 && first.args.max < first.args.min)
							first.args.max = first.args.min;

						m_opts.emplace_back(first);
						registerOpt(rest...);
					}

					void registerOpt() {}
				
				private:
					std::size_t m_index{0};
					std::vector<Param> m_opts;
					std::vector<std::string> m_args;
			};
		}
	}
#endif
