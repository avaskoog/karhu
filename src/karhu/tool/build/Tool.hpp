/**
 * @author		Ava Skoog
 * @date		2017-04-16
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_BUILD_TOOL_H_
	#define KARHU_TOOL_BUILD_TOOL_H_

	#include <karhu/tool/Tool.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace build
			{
				class Tool : public tool::Tool
				{
					public:
						Tool();
				};
			}
		}
	}
#endif
