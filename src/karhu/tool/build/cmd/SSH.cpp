/**
 * @author		Ava Skoog
 * @date		2017-06-28
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/build/cmd/SSH.hpp>
#include <karhu/tool/proj/Location.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/platform.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/process.hpp>
#include <boost/filesystem.hpp>

namespace karhu
{
	namespace tool
	{
		namespace build
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool SSH::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if (n == "sys")
					{
						if (validTarget(v[0], false))
							m_sys = v[0];
						else
							return false;
					}
					else if (n == "acts")
					{
						if (validActs(v))
							m_acts = v;
						else
							return false;
					}
					else if (n == "preacts")
					{
						if (validPreacts(v))
							m_preacts = v;
						else
							return false;
					}
					else if (n == "kacts")
					{
						if (validActs(v, {"run"})) // Disable 'run' for Karhu target.
							m_kacts = v;
						else
							return false;
					}
					else if (n == "kpreacts")
					{
						if (validPreacts(v))
							m_kpreacts = v;
						else
							return false;
					}
					else if (n == "conf")
					{
						if (validConfiguration(v[0]))
							m_conf = v[0];
						else
							return false;
					}
					else if (n == "settings")  m_settings   = v[0];
					else if (n == "hint")      m_hint       = v[0];
					else if (n == "dir")       m_dir        = v[0];
					else if (n == "target")    m_target     = v[0];
					else if (n == "remotedir") m_remotedir  = v[0];
					else if (n == "addr")      m_addr       = v[0];
					else if (n == "args")      m_args       = v;
					else if (n == "capture")   m_capture    = true;

					return true;
				}

				/**
				 * Uses the validated information to set up the new project.
				 */
				bool SSH::finish()
				{
					namespace scripts = SDK::paths::home::scripts;
					
					if (!loadProject(m_dir)) return false;
					
					std::string path;
					std::vector<std::string> args;
					
					auto p(SDK::env::home(scripts::SSH::mac()));
					if (p && boost::filesystem::exists(*p))
					{
						path = std::move(*p);
						args.emplace_back(project().location().path());
						args.emplace_back(m_sys);
						args.emplace_back(m_target);
						args.emplace_back(m_addr);
						args.emplace_back(m_conf);
						args.emplace_back(m_remotedir);
						args.emplace_back((m_hint.length() > 0) ? m_hint : "none");
						args.emplace_back((m_settings.length() > 0) ? m_settings : "none");
						
						if (std::find(m_acts.begin(), m_acts.end(), "clean") != m_acts.end())
							args.emplace_back("y");
						else
							args.emplace_back("n");
						
						if (std::find(m_acts.begin(), m_acts.end(), "make") != m_acts.end())
							args.emplace_back("y");
						else
							args.emplace_back("n");
						
						if (std::find(m_acts.begin(), m_acts.end(), "build") != m_acts.end())
							args.emplace_back("y");
						else
							args.emplace_back("n");
						
						if (std::find(m_acts.begin(), m_acts.end(), "run") != m_acts.end())
							args.emplace_back("y");
						else
							args.emplace_back("n");
						
						if (std::find(m_preacts.begin(), m_preacts.end(), "push") != m_preacts.end())
							args.emplace_back("y");
						else
							args.emplace_back("n");
						
						if (std::find(m_preacts.begin(), m_preacts.end(), "pull") != m_preacts.end())
							args.emplace_back("y");
						else
							args.emplace_back("n");
						
						if (std::find(m_preacts.begin(), m_preacts.end(), "update") != m_preacts.end())
							args.emplace_back("y");
						else
							args.emplace_back("n");
							
						// Karhu actions.
						if (std::find(m_kacts.begin(), m_kacts.end(), "clean") != m_kacts.end())
							args.emplace_back("y");
						else
							args.emplace_back("n");
						
						if (std::find(m_kacts.begin(), m_kacts.end(), "make") != m_kacts.end())
							args.emplace_back("y");
						else
							args.emplace_back("n");
						
						if (std::find(m_kacts.begin(), m_kacts.end(), "build") != m_kacts.end())
							args.emplace_back("y");
						else
							args.emplace_back("n");

						if (std::find(m_kpreacts.begin(), m_kpreacts.end(), "push") != m_kpreacts.end())
							args.emplace_back("y");
						else
							args.emplace_back("n");
						
						if (std::find(m_kpreacts.begin(), m_kpreacts.end(), "pull") != m_kpreacts.end())
							args.emplace_back("y");
						else
							args.emplace_back("n");
						
						if (std::find(m_kpreacts.begin(), m_kpreacts.end(), "update") != m_kpreacts.end())
							args.emplace_back("y");
						else
							args.emplace_back("n");
						
						args.emplace_back((m_capture) ? "y" : "n");
						
						for (auto &arg : m_args)
							args.emplace_back(arg);
					}
					else
					{
						log::err() << "Could not find SSH script for this target and platform.";
						return false;
					}
					
					boost::process::child c{path, args};
					c.wait();
					if (0 != c.exit_code())
					{
						log::err() << "SSH commands failed.";
						return false;
					}
					
					return true;
				}
			}
		}
	}
}
