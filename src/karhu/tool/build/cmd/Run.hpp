/**
 * @author		Ava Skoog
 * @date		2017-04-17
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_BUILD_CMD_RUN_H_
	#define KARHU_TOOL_BUILD_CMD_RUN_H_

	#include <karhu/tool/Command.hpp>
	#include <karhu/tool/build/cmd/Base.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace build
			{
				namespace cmd
				{
					/**
					 * Runs a compiled Karhu project executable.
					 *
					 * Syntax:
					 * run
					 * [--sys <mac|windows|linux|ios|android|web>=local]
					 * [--conf <debug|release>=debug]
					 * [--args <...>]
					 * [--capture]
					 * [--dir <path>=current]
					 */
					class Run : public Command, private Base
					{
						public:
							Run()
							:
							Command
							{
								Param{"sys",     Enforce::optional, ArgsMinMax{1}},
								Param{"conf",    Enforce::optional, ArgsMinMax{1}},
								Param{"dir",     Enforce::optional, ArgsMinMax{1}},
								Param{"hint",    Enforce::optional, ArgsMinMax{1}},
								Param{"args",    Enforce::optional, ArgsMinMax{1, -1}},
								Param{"server",  Enforce::optional, ArgsMinMax{0, 2}},
								Param{"capture", Enforce::optional}
							}
							{
							}

							bool processOpt(const OptWithVals &) override;
							bool finish() override;
						
						private:
							/// @todo: Fix this duplicate code mess; move from Apply and Run to Base.
							struct Server
							{
								bool use{false};
								std::string host;
								std::uint16_t port{0};
							} m_server;

							std::string m_sys, m_conf, m_hint, m_dir;
							std::vector<std::string> m_args;
							bool m_capture{false};
					};
				}
			}
		}
	}
#endif
