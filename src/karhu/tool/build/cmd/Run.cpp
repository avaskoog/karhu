/**
 * @author		Ava Skoog
 * @date		2017-04-17
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/build/cmd/Run.hpp>
#include <karhu/tool/proj/Location.hpp>
#include <karhu/tool/proj/file/build/Module.hpp>
#include <karhu/tool/proj/file/build/common.hpp>
#include <karhu/tool/network/ServerDebug.hpp>
#include <karhu/network/Protocol.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/platform.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/process.hpp>
#include <boost/filesystem.hpp>

namespace karhu
{
	namespace tool
	{
		namespace build
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool Run::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if (n == "sys")
					{
						if (validTarget(v[0]))
							m_sys = v[0];
						else
							return false;
					}
					else if (n == "conf")
					{
						if (validConfiguration(v[0]))
							m_conf = v[0];
						else
							return false;
					}
					else if (n == "server")
					{
						m_server.use = true;

						// Convert port to integer.
						/// @todo: Validate port.
						if (v.size() >= 1)
						{
							std::stringstream s;
							s << v[0];
							s >> m_server.port;
						}
						
						if (v.size() >= 2) m_server.host = v[1];
					}
					else if (n == "args")    m_args    = v;
					else if (n == "hint")    m_hint    = v[0];
					else if (n == "dir")     m_dir     = v[0];
					else if (n == "capture") m_capture = true;

					return true;
				}

				/**
				 * Uses the validated information to run on the target platform.
				 */
				bool Run::finish()
				{
					namespace scripts  = SDK::paths::home::scripts;
					namespace protocol = karhu::network::protocol;
					namespace pf       = platform;
					
					using namespace proj::file::build;
					
					if (m_sys.length() == 0)
						m_sys = pf::identifier(Platform::current);

					if (m_conf.length() == 0)
						m_conf = config::toString(config::Type::debug);
					
					const auto conf(config::fromString(m_conf.c_str()));

					if (!loadProject(m_dir)) return false;
	
					auto info(project().info());
					if (!info)
					{
						log::err() << "Error accessing project information.";
						return false;
					}
					
					auto identifier(info->identifier());
					auto name(info->name());
					if (!identifier || !name)
					{
						log::err() << "Error accessing project information.";
						return false;
					}

					auto build(project().build());
					if (!build)
					{
						log::err() << "Error accessing project build settings.";
						return false;
					}
					
					if (0 == m_server.port)
					{
						if (auto port = build->port())
							m_server.port = *port;
						else
							m_server.port = protocol::defaultport();
					}

					// Replace localhost with network name so that it works on external devices.
					if (m_server.host.length() == 0 || m_server.host == protocol::defaulthost())
						m_server.host = network::ServerDebug::hostname(m_server.port);

					auto module(Module::loadForPlatform(pf::fromIdentifier(m_sys.c_str())));
					if (!module) return false;
					
					std::vector<std::string> args;
					
					// Build module subdirectory of this script.
					args.emplace_back(module->subpath());
					
					// Working directory.
					args.emplace_back(location().path());
					
					// Target name.
					args.emplace_back(identifier->proj());
					
					// Executable name,
					args.emplace_back(*name);
					
					// Full identifier.
					args.emplace_back(identifier->full());
					
					// Configuration.
					args.emplace_back(m_conf);
					
					// Capture output [y/n]?
					args.emplace_back((m_capture) ? "y" : "n");
					
					// Hint.
					args.emplace_back((m_hint.length() > 0) ? m_hint : "none");

					// Remaining arguments.
					if (m_server.use) addArgsServer(args, m_sys, conf, m_server.host, m_server.port);
					for (auto &i : m_args) args.emplace_back(i);
					
					return tryToRunTool(m_sys, Action::run, args);
				}
			}
		}
	}
}
