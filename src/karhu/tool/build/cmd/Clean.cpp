/**
 * @author		Ava Skoog
 * @date		2017-04-17
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/build/cmd/Clean.hpp>
#include <karhu/tool/proj/Location.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/platform.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/process.hpp>
#include <boost/filesystem.hpp>

namespace karhu
{
	namespace tool
	{
		namespace build
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool Clean::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if (n == "sys")
					{
						if (validTarget(v[0]))
							m_sys = v[0];
						else
							return false;
					}
					else if (n == "dir")  m_dir  = v[0];

					return true;
				}

				/**
				 * Uses the validated information to clean for the target platform.
				 */
				bool Clean::finish()
				{
					if (!loadProject(m_dir)) return false;

					if (m_sys.length() == 0)
						m_sys = platform::identifier(Platform::current);
					
					std::vector<std::string> args;
					args.emplace_back(project().location().path());
					args.emplace_back(m_sys);

					return tryToRunTool(m_sys, Action::clean, args);
				}
			}
		}
	}
}
