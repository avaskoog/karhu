/**
 * @author		Ava Skoog
 * @date		2017-07-01
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_BUILD_CMD_APPLY_H_
	#define KARHU_TOOL_BUILD_CMD_APPLY_H_

	#include <karhu/tool/Command.hpp>
	#include <karhu/tool/build/cmd/Base.hpp>
	#include <karhu/tool/proj/file/build/Build.hpp>
	#include <karhu/tool/proj/file/build/common.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace build
			{
				namespace cmd
				{
					/**
					 * Applies commands according to settings in build file.
					 *
					 * Syntax:
					 * apply
					 * --acts <clean, make, build, run>
					 * [--target <name>=main]
					 * [--settings <json>]
					 * [--hint simulator]
					 * [--preacts <pull, push, update>]
					 * [--kacts <clean, make, build>]
					 * [--kpreacts <pull, push, update>]
					 * [--sys <[mac, windows, linux, ios, android, web]|all>=local]
					 * [--conf <debug|release>=debug]
					 * [--args <...>]
					 * [--server <portoffset>=14300 <host>=localhost]
					 * [--capture]
					 * [--dir <path>=current]
					 */
					class Apply : public Command, private Base
					{
						public:
							Apply()
							:
							Command
							{
								Param{"acts",     Enforce::optional, ArgsMinMax{1, 4}},
								Param{"preacts",  Enforce::optional, ArgsMinMax{1, 3}},
								Param{"kacts",    Enforce::optional, ArgsMinMax{1, 4}},
								Param{"kpreacts", Enforce::optional, ArgsMinMax{1, 3}},
								Param{"target",   Enforce::optional, ArgsMinMax{1}},
								Param{"sys",      Enforce::optional, ArgsMinMax{1, 6}},
								Param{"conf",     Enforce::optional, ArgsMinMax{1}},
								Param{"dir",      Enforce::optional, ArgsMinMax{1}},
								Param{"settings", Enforce::optional, ArgsMinMax{1}},
								Param{"hint",     Enforce::optional, ArgsMinMax{1}},
								Param{"args",     Enforce::optional, ArgsMinMax{1, -1}},
								Param{"server",   Enforce::optional, ArgsMinMax{0, 2}},
								Param{"capture",  Enforce::optional}
							}
							{
							}

							bool processOpt(const OptWithVals &) override;
							bool finish() override;

						private:
							bool applyActions
							(
								const std::string &path,
								const std::string &system,
								const proj::file::build::config::Type &conf,
								const proj::file::build::Build::SettingsSSH *const SSH
							);
							void addArgsServer(std::vector<std::string> &args, const std::string &sys, const proj::file::build::config::Type &conf);

						private:
							/// @todo: Fix this duplicate code mess; move from Apply and Run to Base.
							struct Server
							{
								bool use{false};
								std::string host;
								std::uint16_t port{0};
							} m_server;
						
							std::string m_target{"main"}, m_conf, m_settings, m_hint, m_dir;
							std::vector<std::string> m_systems, m_acts, m_preacts, m_kacts, m_kpreacts, m_args;
							bool m_capture{false};
					};
				}
			}
		}
	}
#endif
