/**
 * @author		Ava Skoog
 * @date		2017-04-17
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_BUILD_CMD_BUILD_H_
	#define KARHU_TOOL_BUILD_CMD_BUILD_H_

	#include <karhu/tool/Command.hpp>
	#include <karhu/tool/build/cmd/Base.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace build
			{
				namespace cmd
				{
					/**
					 * Generates build files for a Karhu project.
					 *
					 * Syntax:
					 * build [--sys <mac|windows|linux|ios|android|web>=local] [--conf <debug|release>=debug] [--dir <path>=current]
					 */
					class Build : public Command, private Base
					{
						public:
							Build()
							:
							Command
							{
								Param{"target", Enforce::optional, ArgsMinMax{1}},
								Param{"sys",    Enforce::optional, ArgsMinMax{1}},
								Param{"conf",   Enforce::optional, ArgsMinMax{1}},
								Param{"hint",   Enforce::optional, ArgsMinMax{1}},
								Param{"dir",    Enforce::optional, ArgsMinMax{1}}
							}
							{
							}

							bool processOpt(const OptWithVals &) override;
							bool finish() override;
						
						private:
							std::string m_target{"main"}, m_sys, m_conf, m_hint, m_dir;
					};
				}
			}
		}
	}
#endif
