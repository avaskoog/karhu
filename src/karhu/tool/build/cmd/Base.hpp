/**
 * @author		Ava Skoog
 * @date		2017-04-17
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_BUILD_CMD_BASE_H_
	#define KARHU_TOOL_BUILD_CMD_BASE_H_

	#include <karhu/tool/shared/cmd/Base.hpp>
	#include <karhu/core/Nullable.hpp>
	#include <karhu/core/platform.hpp>

	#include <string>
	#include <array>

	namespace karhu
	{
		namespace tool
		{
			namespace build
			{
				namespace cmd
				{
					class Base : public shared::cmd::Base
					{
						public:
							enum class Action : std::int8_t
							{
								invalid = -1,
								clean,
								make,
								build,
								run
							};
						
							struct InfoTarget
							{
								Platform platform;
								
								InfoTarget(const Platform &platform)
								:
								platform{platform}
								{
								}
							};

						public:
							static inline constexpr auto actionToString(const Action a)
							{
								switch (a)
								{
									case Action::clean: return "clean";
									case Action::make:  return "make";
									case Action::build: return "build";
									case Action::run:   return "run";
									default:            return "";
								}
							}
						
							static inline constexpr Action stringToAction(const char *s)
							{
								if      (meta::string::equal("clean", s)) return Action::clean;
								else if (meta::string::equal("make",  s)) return Action::make;
								else if (meta::string::equal("build", s)) return Action::build;
								else if (meta::string::equal("run",   s)) return Action::run;

								return Action::invalid;
							}

						public:
							Base();

						protected:
							bool tryToRunTool(const std::string &platform, const Action action, const std::vector<std::string> &args = {});
							void addArgsServer
							(
								std::vector<std::string> &args,
								const std::string &sys,
								const proj::file::build::config::Type &conf,
								const std::string &host,
								const std::uint16_t port
							);
							
							bool validateTargetExists(const std::string &name);
							
							bool native(const std::string &target) const;
							bool validTarget(const std::string &target, const bool checkAvailabilty = true, const bool allowAllFlag = false) const;
							bool validTargets(const std::vector<std::string> &targets, const bool allowAllFlag = false) const;
							bool validActs(const std::vector<std::string> &acts, const std::vector<std::string> &disable = {}) const;
							bool loadAndSaveConfig(const std::string &target);

							std::vector<InfoTarget> findTargetsAvailableToCurrentSysAsHost();
							bool targetAvailable(const Platform &platform, const std::vector<InfoTarget> &targets);
							Nullable<std::string> pathToolFull(const Platform &target, const Action action);

						private:
							const std::array<std::string, 6> m_targets;
					};
				}
			}
		}
	}
#endif
