/**
 * @author		Ava Skoog
 * @date		2017-06-28
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_BUILD_CMD_SSH_H_
	#define KARHU_TOOL_BUILD_CMD_SSH_H_

	#include <karhu/tool/Command.hpp>
	#include <karhu/tool/build/cmd/Base.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace build
			{
				namespace cmd
				{
					/**
					 * Runs a compiled Karhu project executable.
					 *
					 * Syntax:
					 * ssh
					 * --sys <mac|windows|linux|ios|android|web>
					 * --acts <clean, make, build, run>
					 * --conf <debug|release>
					 * --addr <name@host>
					 * --remotedir <path>
					 * [--preacts <pull, push, update>]
					 * [--kacts <clean, make, build>]
					 * [--kpreacts <pull, push, update>]
					 * [--args <...>]
					 * [--capture]
					 * [--dir <path>=current]
					 */
					/// @todo: Maybe allow building for multiple targets at once.
					class SSH : public Command, private Base
					{
						public:
							SSH()
							:
							Command
							{
								Param{"settings",  Enforce::optional, ArgsMinMax{1}},
								Param{"hint",      Enforce::optional, ArgsMinMax{1}},
								Param{"dir",       Enforce::optional, ArgsMinMax{1}},
								Param{"sys",       Enforce::required, ArgsMinMax{1}},
								Param{"acts",      Enforce::optional, ArgsMinMax{1, 4}},
								Param{"preacts",   Enforce::optional, ArgsMinMax{1, 3}},
								Param{"kacts",     Enforce::optional, ArgsMinMax{1, 4}},
								Param{"kpreacts",  Enforce::optional, ArgsMinMax{1, 3}},
								Param{"target",    Enforce::optional, ArgsMinMax{1}},
								Param{"conf",      Enforce::required, ArgsMinMax{1}},
								Param{"addr",      Enforce::required, ArgsMinMax{1}},
								Param{"remotedir", Enforce::required, ArgsMinMax{1}},
								Param{"args",      Enforce::optional, ArgsMinMax{1, -1}},
								Param{"capture",   Enforce::optional}
							}
							{
							}

							bool processOpt(const OptWithVals &) override;
							bool finish() override;

						private:
							std::string m_target{"main"}, m_sys, m_addr, m_conf, m_settings, m_hint, m_dir, m_remotedir;
							std::vector<std::string> m_acts, m_preacts, m_kacts, m_kpreacts, m_args;
							bool m_capture{false};
					};
				}
			}
		}
	}
#endif
