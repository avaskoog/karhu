/**
 * @author		Ava Skoog
 * @date		2017-07-01
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/build/cmd/Apply.hpp>
#include <karhu/tool/proj/Location.hpp>
#include <karhu/tool/proj/file/build/common.hpp>
#include <karhu/tool/network/ServerDebug.hpp>
#include <karhu/network/Protocol.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/platform.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/process.hpp>
#include <boost/filesystem.hpp>

#include <sstream>
#include <regex>

namespace karhu
{
	namespace tool
	{
		namespace build
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool Apply::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if (n == "sys")
					{
						if (validTargets(v, true))
							m_systems = v;
						else
							return false;
					}
					else if (n == "acts")
					{
						if (validActs(v))
							m_acts = v;
						else
							return false;
					}
					else if (n == "preacts")
					{
						if (validPreacts(v))
							m_preacts = v;
						else
							return false;
					}
					else if (n == "kacts")
					{
						if (validActs(v, {"run"})) // Disable 'run' for Karhu target.
							m_kacts = v;
						else
							return false;
					}
					else if (n == "kpreacts")
					{
						if (validPreacts(v))
							m_kpreacts = v;
						else
							return false;
					}
					else if (n == "conf")
					{
						if (validConfiguration(v[0]))
							m_conf = v[0];
						else
							return false;
					}
					else if (n == "server")
					{
						m_server.use = true;

						// Convert port to integer.
						/// @todo: Validate port.
						if (v.size() >= 1)
						{
							std::stringstream s;
							s << v[0];
							s >> m_server.port;
						}
						
						if (v.size() >= 2) m_server.host = v[1];
					}
					else if (n == "dir")      m_dir      = v[0];
					else if (n == "settings") m_settings = v[0];
					else if (n == "hint")     m_hint     = v[0];
					else if (n == "target")   m_target   = v[0];
					else if (n == "args")     m_args     = v;
					else if (n == "capture")  m_capture  = true;

					return true;
				}

				/**
				 * Uses the validated information to set up the new project.
				 */
				bool Apply::finish()
				{
					namespace tools    = SDK::paths::home::tools;
					namespace protocol = karhu::network::protocol;
					namespace pf       = platform;
					
					using namespace proj::file::build;
					
					if (m_conf.length() == 0)
						m_conf = config::toString(config::Type::debug);
					
					const auto conf(config::fromString(m_conf.c_str()));

					if (m_systems.size() == 0)
						m_systems.emplace_back(pf::identifier(Platform::current));
					else if (m_systems.size() == 1 && m_systems[0] == "all")
					{
						m_systems.clear();
						m_systems.emplace_back(pf::identifier(Platform::mac));
						m_systems.emplace_back(pf::identifier(Platform::linux));
						m_systems.emplace_back(pf::identifier(Platform::windows));
						m_systems.emplace_back(pf::identifier(Platform::iOS));
						m_systems.emplace_back(pf::identifier(Platform::android));
						m_systems.emplace_back(pf::identifier(Platform::web));
					}
					
					if (!loadProject(m_dir)) return false;
					
					auto build(project().build());
					if (!build)
					{
						log::err() << "Error accessing project build settings.";
						return false;
					}

					if (0 == m_server.port)
					{
						if (auto port = build->port())
							m_server.port = *port;
						else
							m_server.port = protocol::defaultport();
					}

					// Replace localhost with network name so that it works on external devices.
					if (m_server.host.length() == 0 || m_server.host == protocol::defaulthost())
						m_server.host = network::ServerDebug::hostname(m_server.port);

					const auto SSHs(build->SSHs());

					const auto systems(build->systems());
					for (auto &i : build->systems())
					{
						if (i.SSH.length() > 0 && std::find_if(SSHs.begin(), SSHs.end(), [&i](auto &x){ return (x.name == i.SSH); }) == SSHs.end())
						{
							log::err() << "Invalid SSH configuration '" << i.SSH << "' ";
							log::err() << "specified for system " << i.name << ' ';
							log::err() << "not declared in build file.";
							return false;
						}
					}
					
					for (auto &sys : m_systems)
					{
						if (std::find_if(systems.begin(), systems.end(), [&sys](auto &x) { return (x.name == sys); }) == systems.end())
						{
							log::err() << "Specified system '" << sys << "' was not found in build file.";
							return false;
						}
					}
					
					std::string path;
					std::vector<std::string> args;
					
					auto p(SDK::env::home(tools::karhubuild::local()));
					if (!p || !boost::filesystem::exists(*p))
						return false;

					path = std::move(*p);
					
					for (auto &sys : m_systems)
					{
						log::msg() << "SYSTEM: " << sys;
						
						auto i(std::find_if(systems.begin(), systems.end(), [&sys](auto &x) { return (x.name == sys); }));
						auto j(std::find_if(SSHs.begin(), SSHs.end(), [&i](auto &x) { return (x.name == i->SSH); }));

						// Check whether to perform Karhu actions.
						if (j == SSHs.end() && m_dir != "karhu" && (m_kacts.size() > 0 || m_kpreacts.size() > 0))
						{
							auto p(SDK::env::home(tools::karhubuild::local()));
							if (p && boost::filesystem::exists(*p))
							{
								std::vector<std::string> args;
								args.emplace_back("apply");
								
								if (m_kacts.size() > 0)
								{
									args.emplace_back("--acts");
									for (auto &act : m_kacts)
										args.emplace_back(act);
								}
								
								if (m_kpreacts.size() > 0)
								{
									args.emplace_back("--preacts");
									for (auto &act : m_kpreacts)
										args.emplace_back(act);
								}
								
								args.emplace_back("--sys");
								args.emplace_back(sys);
								
								args.emplace_back("--conf");
								args.emplace_back(m_conf);
								
								if (m_settings.length() > 0)
								{
									args.emplace_back("--settings");
									args.emplace_back(m_settings);
								}
								
								if (m_hint.length() > 0)
								{
									args.emplace_back("--hint");
									args.emplace_back(m_hint);
								}
								
								args.emplace_back("--dir");
								args.emplace_back("karhu");

								std::stringstream cmd;
								cmd << "COMMAND: karhubuild ";
								
								for (auto &arg : args)
									cmd << arg << ' ';
								
								log::msg() << cmd.str();

								boost::process::child c{*p, args};
								c.wait();
								if (0 != c.exit_code())
									return false;
							}
							else
							{
								log::err() << "Could not find karhubuild tool.";
								return false;
							}
						}

						if (!applyActions(path, sys, conf, (j != SSHs.end()) ? &*j : nullptr))
							return false;
					}
					
					return true;
				}
				
				bool Apply::applyActions
				(
					const std::string &path,
					const std::string &system,
					const proj::file::build::config::Type &conf,
					const proj::file::build::Build::SettingsSSH *const SSH
				)
				{
					namespace tools = SDK::paths::home::tools;
					
					std::vector<std::string> args;
					
					auto execute([&path, &args, &system](const std::string &act)
					{
						// Close off everything so the console doesn't get stuck.
						// Will have to figure out a better solution later.
						/// @todo: Fix logging when calling karhubuild apply.
						/// @todo: Closing everything causes issues so need to find a different solution anyway. Don't comment back in. Remove.
						boost::process::child c
						{
							path,
							args
						};
						c.wait();

						if (0 != c.exit_code())
						{
							log::err() << "The " << act << " command for " << system << " failed.";
							return false;
						}

						args.clear();
						
						return true;
					});
					
					if (SSH)
					{
						args.emplace_back("ssh");
						
						args.emplace_back("--acts");
						for (auto &act : m_acts)
							args.emplace_back(act);

						if (m_dir.length() > 0)
						{
							args.emplace_back("--dir");
							args.emplace_back(m_dir);
						}
						
						args.emplace_back("--remotedir");
						args.emplace_back(SSH->dir);
						
						args.emplace_back("--addr");
						args.emplace_back(SSH->addr);
						
						args.emplace_back("--conf");
						args.emplace_back(m_conf);
						
						if (m_settings.length() > 0)
						{
							args.emplace_back("--settings");
							args.emplace_back(m_settings);
						}
						
						if (m_hint.length() > 0)
						{
							args.emplace_back("--hint");
							args.emplace_back(m_hint);
						}
						
						args.emplace_back("--sys");
						args.emplace_back(system);
						
						args.emplace_back("--target");
						args.emplace_back(m_target);
						
						if (m_preacts.size() > 0)
						{
							args.emplace_back("--preacts");
							
							for (auto &act : m_preacts)
								args.emplace_back(act);
						}
						
						if (m_kacts.size() > 0)
						{
							args.emplace_back("--kacts");
							for (auto &act : m_kacts)
								args.emplace_back(act);
						}
						
						if (m_kpreacts.size() > 0)
						{
							args.emplace_back("--kpreacts");
							
							for (auto &act : m_kpreacts)
								args.emplace_back(act);
						}
						
						if (m_args.size() > 0 || m_server.use)
						{
							args.emplace_back("--args");
							
							addArgsServer(args, system, conf);
							
							for (auto &arg : m_args)
								args.emplace_back(arg);
						}
						
						if (m_capture) args.emplace_back("--capture");

						std::stringstream cmd;
						cmd << "COMMAND: karhubuild ";
						
						for (auto &arg : args)
							cmd << arg << ' ';
						
						log::msg() << cmd.str();
						
						if (!execute("SSH"))
							return false;
					}
					else
					{
						if (m_preacts.size() > 0)
						{
							auto p(SDK::env::home(tools::karhuproj::local()));
							if (p && boost::filesystem::exists(*p))
							{
								std::vector<std::string> args;
								args.emplace_back("sync");

								args.emplace_back("--acts");
								for (auto &act : m_preacts)
									args.emplace_back(act);

								if (m_dir.length() > 0)
								{
									args.emplace_back("--dir");
									args.emplace_back(m_dir);
								}

								std::stringstream cmd;
								cmd << "COMMAND: karhuproj ";
								
								for (auto &arg : args)
									cmd << arg << ' ';
								
								log::msg() << cmd.str();

								boost::process::child c{*p, args};
								c.wait();
								if (0 != c.exit_code())
								{
									log::msg() << "Failed pulling";
									return false;
								}
							}
							else
							{
								log::err() << "Could not find karhuproj tool.";
								return false;
							}
						}

						auto apply([this, &args, &system, &conf, &execute](const std::string &act, const bool config, const bool target)
						{
							if (std::find(m_acts.begin(), m_acts.end(), act) == m_acts.end())
								return true;

							args.emplace_back(act);
							
							args.emplace_back("--sys");
							args.emplace_back(system);

							if (target)
							{
								args.emplace_back("--target");
								args.emplace_back(m_target);
							}

							if (config)
							{
								args.emplace_back("--conf");
								args.emplace_back(m_conf);
							}
							
							if (act == "make" && m_settings.length() > 0)
							{
								args.emplace_back("--settings");
								args.emplace_back(m_settings);
							}
							
							if (m_hint.length() > 0)
							{
								args.emplace_back("--hint");
								args.emplace_back(m_hint);
							}

							if (m_dir.length() > 0)
							{
								args.emplace_back("--dir");
								args.emplace_back(m_dir);
							}
							
							if (act == "run")
							{
								if (m_capture)
									args.emplace_back("--capture");

								/// @todo: Use --server in run invocation instead.
								if (m_args.size() > 0 || m_server.use)
								{
									args.emplace_back("--args");
									
									addArgsServer(args, system, conf);
									
									for (auto &arg : m_args)
										args.emplace_back(arg);
								}
							}

							std::stringstream cmd;
							cmd << "COMMAND: karhubuild ";
							
							for (auto &arg : args)
								cmd << arg << ' ';
							
							log::msg() << cmd.str();
							
							return execute(act);
						});
						
						for (auto &act : m_acts)
							if (!apply(act, (act != "clean"), (act == "make" || act == "build")))
								return false;
					}
					
					return true;
				}
				
				void Apply::addArgsServer(std::vector<std::string> &args, const std::string &sys, const proj::file::build::config::Type &conf)
				{
					if (m_server.use)
						Base::addArgsServer(args, sys, conf, m_server.host, m_server.port);
				}
			}
		}
	}
}
