/**
 * @author		Ava Skoog
 * @date		2017-04-17
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/build/cmd/Build.hpp>
#include <karhu/tool/proj/Location.hpp>
#include <karhu/tool/proj/file/build/Module.hpp>
#include <karhu/tool/proj/file/build/common.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/platform.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/process.hpp>
#include <boost/filesystem.hpp>

namespace karhu
{
	namespace tool
	{
		namespace build
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool Build::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if (n == "sys")
					{
						if (validTarget(v[0]))
							m_sys = v[0];
						else
							return false;
					}
					else if (n == "conf")
					{
						if (validConfiguration(v[0]))
							m_conf = v[0];
						else
							return false;
					}
					else if (n == "target") m_target = v[0];
					else if (n == "hint")   m_hint   = v[0];
					else if (n == "dir")    m_dir    = v[0];

					return true;
				}

				/**
				 * Uses the validated information to build for the target platform.
				 */
				bool Build::finish()
				{
					namespace scripts = SDK::paths::home::scripts;
					namespace pf      = platform;
					
					using namespace proj::file::build;

					if (m_sys.length()  == 0)
						m_sys = pf::identifier(Platform::current);

					if (m_conf.length() == 0) 
						m_conf = config::toString(config::Type::debug);

					if (!loadProject(m_dir)) return false;
					
					auto info(project().info());
					if (!info)
					{
						log::err() << "Error accessing project information.";
						return false;
					}
					
					auto identifier(info->identifier());
					if (!identifier)
					{
						log::err() << "Error accessing project information.";
						return false;
					}
					
					auto name(info->name());
					if (!identifier || !name)
					{
						log::err() << "Error accessing project information.";
						return false;
					}

					if (!validateTargetExists(m_target)) return false;
					
					/// @todo: Fix this: duplicate code mirrored in Config.
					const auto nameTarget((m_target == "main") ? identifier->proj() : m_target);

					auto module(Module::loadForPlatform(pf::fromIdentifier(m_sys.c_str())));
					if (!module) return false;

					std::vector<std::string> args;

					// Build module subdirectory of this script.
					args.emplace_back(module->subpath());

					// Working directory.
					args.emplace_back(project().location().path());
					
					// Target name.
					args.emplace_back(identifier->proj());
					
					// Executable name.
					args.emplace_back(*name);
					
					// Configuration (debug or release).
					args.emplace_back(m_conf);
					
					// Target name.
					args.emplace_back(nameTarget);
					
					// Hint.
					args.emplace_back((m_hint.length() > 0) ? m_hint : "none");

					return tryToRunTool(m_sys, Action::build, args);
				}
			}
		}
	}
}
