/**
 * @author		Ava Skoog
 * @date		2017-04-17
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_BUILD_CMD_CLEAN_H_
	#define KARHU_TOOL_BUILD_CMD_CLEAN_H_

	#include <karhu/tool/Command.hpp>
	#include <karhu/tool/build/cmd/Base.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace build
			{
				namespace cmd
				{
					/**
					 * Cleans Karhu project's generated and build subdirectories for the system.
					 *
					 * Syntax:
					 * clean [--sys <mac|windows|linux|ios|android|web>=local] [--dir <path>=current]
					 */
					class Clean : public Command, private Base
					{
						public:
							Clean()
							:
							Command
							{
								Param{"sys",  Enforce::optional, ArgsMinMax{1}},
								Param{"hint", Enforce::optional, ArgsMinMax{1}},
								Param{"dir",  Enforce::optional, ArgsMinMax{1}}
							}
							{
							}

							bool processOpt(const OptWithVals &) override;
							bool finish() override;
						
						private:
							std::string m_sys, m_dir;
					};
				}
			}
		}
	}
#endif
