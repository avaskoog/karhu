/**
 * @author		Ava Skoog
 * @date		2017-04-17
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/build/cmd/Base.hpp>
#include <karhu/tool/proj/file/build/Premake.hpp>
#include <karhu/tool/proj/file/build/common.hpp>
#include <karhu/network/Protocol.hpp>
#include <karhu/core/platform.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/core/log.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/process.hpp>
#include <boost/filesystem.hpp>

/// @todo: Fix logging.
#include <iostream>

namespace karhu
{
	namespace tool
	{
		namespace build
		{
			namespace cmd
			{
				Base::Base()
				:
				m_targets
				{
					platform::identifier(Platform::mac),
					platform::identifier(Platform::windows),
					platform::identifier(Platform::linux),
					platform::identifier(Platform::iOS),
					platform::identifier(Platform::android),
					platform::identifier(Platform::web)
				}
				{
				}
				
				/**
				 * Tries to to run the specified build action for the specified
				 * target system, with any optionally specified arguments.
				 *
				 * Checks the available build target platforms from the current
				 * platform, and tries to find the target platform amongst them,
				 * then tries to find the tool for the build action and run it.
				 *
				 * @param platform The identifier string for the platform.
				 * @param action   The build action to run.
				 * @param args     An optional list of string arguments.
				 *
				 * @return Whether the command was successfully found and executed.
				 */
				bool Base::tryToRunTool(const std::string &platform, const Action action, const std::vector<std::string> &args)
				{
					// Find the targets.
					const auto targets(findTargetsAvailableToCurrentSysAsHost());
					
					// Validate the platform identifier.
					auto pf(platform::fromIdentifier(platform.c_str()));

					if (platform::is(pf, Platform::unknown))
					{
						log::err() << "Invalid platform identifier '" << platform << '\'';
						return false;
					}

					// Find the platform among the targets.
					if (!targetAvailable(pf, targets))
						return false;

					// Get the tool for the action.
					const auto path(pathToolFull(pf, action));
					if (!path)
					{
						log::err()
							<< "Build tool not found for action '" << actionToString(action)
							<< "' and target '" << platform << "'.";

						return false;
					}

					// Run the tool.
					boost::process::environment env{boost::this_process::environment()};
					boost::process::child c{*path, args, env};
					c.wait();
					if (0 != c.exit_code())
					{
						log::err() << "Build action '" << actionToString(action) << "' failed.";
						return false;
					}
					
					return true;
				}
				
				void Base::addArgsServer
				(
					std::vector<std::string> &args,
					const std::string &sys,
					const proj::file::build::config::Type &conf,
					const std::string &host,
					const std::uint16_t port
				)
				{
					namespace pf     = platform;
					namespace client = network::protocol::client;

					args.emplace_back("-client");
					
					args.emplace_back("-host");
					args.emplace_back(host);
					args.emplace_back("-port");
					args.emplace_back(std::to_string(port));
					
					/// @todo: Error check this whole thing…
					const auto build(project().build());
					if (build)
					{
						std::vector<std::string> modules;
						
						for (const auto &target : build->targets())
						{
							if (const auto object = target.second.data().object())
							{
								const auto type(object->getString("type"));
								if (type && *type == "module")
									modules.emplace_back(target.first);
							}
						}
						
						if (modules.size() > 0)
						{
							args.emplace_back("-modules");
							
							for (const auto &m : modules)
							{
								using namespace std::literals::string_literals;

								args.emplace_back(m);
								
								/// @todo: Maybe not hardcode this whole thing as much?
								boost::filesystem::path p{project().location().path()};
								p.append("build");
								p.append(platform::identifier(Platform::current)); /// @todo: What if it is a different platform?
								p.append("bin");
								p.append(m);
								p.append(proj::file::build::config::toString(conf));
								p.append("lib"s + m + ".dylib"); /// @todo: Add something else besides lib- and .dylib depending on platform.
								p.make_preferred();
								
								args.emplace_back(p.string());
							}
						}
					}
				}
				
				/**
				 * Looks through the SDK folders to find available targets
				 * that can be built on the current platform. If the vector
				 * returned is empty, an error message will have been printed.
				 *
				 * @return A vector of any targets found.
				 */
				std::vector<Base::InfoTarget> Base::findTargetsAvailableToCurrentSysAsHost()
				{
					namespace fs = boost::filesystem;
					namespace pf = platform;
					
					const auto basepath(SDK::env::home("targets"));

					std::vector<InfoTarget> r;

					if (!basepath || !fs::exists(*basepath) || !fs::is_directory(*basepath))
					{
						log::err()
							<< "Path to target modules not found! "
							<< "Make sure the KARHU_HOME environment variable has been set.";

						return r;
					}

					for (fs::directory_iterator i{*basepath}; i != fs::directory_iterator(); ++ i)
					{
						const auto hostname(i->path().filename().string());
						if (fs::is_directory(i->path()) && Platform::current == pf::fromIdentifier(hostname.c_str()))
						{
							for (fs::directory_iterator j{i->path()}; j != fs::directory_iterator(); ++ j)
							{
								if (fs::is_directory(j->path()))
								{
									const auto targetname(j->path().filename().string());
									r.emplace_back(pf::fromIdentifier(targetname.c_str()));
								}
							}
						}
					}
					
					if (r.size() == 0)
					{
						log::err() << "No targets found that can be built on this platform";
						return r;
					}

					return r;
				}
				
				/**
				 * Checks whether the specified platform exists in the
				 * list of available platforms passed to the function.
				 *
				 * @param platform The identifier of the platform to check.
				 * @param targets  The list of targets to search.
				 *
				 * @return Whether the platform is in the list.
				 */
				bool Base::targetAvailable(const Platform &platform, const std::vector<InfoTarget> &targets)
				{
					for (auto &i : targets)
						if (platform == i.platform)
							return true;

					log::err() << "Platform '" << platform::identifier(platform) << "' is not a valid target.";
					return false;
				}
				
				/**
				 * Returns the full path to the build tool (a shell script) for the
				 * specified target platform and build action for build from the current
				 * platform, if it exists.
				 *
				 * @param target The target platform to build for.
				 * @param action The build action to perform.
				 *
				 * @return A nullable containing the full path if valid, or null.
				 */
				/// @todo: Move this to Module.
				Nullable<std::string> Base::pathToolFull(const Platform &target, const Action action)
				{
					boost::filesystem::path p{"targets"};
					p.append(platform::identifier(Platform::current));
					p.append(platform::identifier(target));
					p.append("tools");
					
					std::string tool{actionToString(action)};
					
					if (platform::is(Platform::UNIX))
						tool += ".sh";
					else if (platform::is(Platform::windows))
						tool += ".bat";
					
					p.append(tool);

					p.make_preferred();

					if (const auto r = SDK::env::home(p.string()))
						if (boost::filesystem::exists(*r))
							return {true, std::move(boost::filesystem::path{*r}.make_preferred().string())};
					
					return {};
				}
				
				bool Base::validateTargetExists(const std::string &name)
				{
					if (const auto build = project().build())
					{
						const auto &t(build->targets());
						const auto it(std::find_if(t.begin(), t.end(), [&name](const auto &v)
						{
							return (v.first == name);
						}));
						if (it == t.end())
						{
							log::msg() << '\'' << name << "' is not a valid target";
							return false;
						}
					}
					else
					{
						log::msg() << "Build file (build.karhuproj.json) missing";
						return false;
					}
					
					return true;
				}
				
				bool Base::native(const std::string &target) const
				{
					namespace pf = platform;
					
					return
					(
						(pf::is(Platform::mac)     && target == pf::identifier(Platform::mac))     ||
						(pf::is(Platform::windows) && target == pf::identifier(Platform::windows)) ||
						(pf::is(Platform::linux)   && target == pf::identifier(Platform::linux))
					);
				}
				
				bool Base::validTarget(const std::string &target, const bool checkAvailability, const bool allowAllFlag) const
				{
					const auto i(std::find(m_targets.begin(), m_targets.end(), target));
					if (i == m_targets.end() && !(allowAllFlag && target == "all"))
					{
						std::stringstream msg;
						msg << "Invalid target '" << target << "'. Valid targets are ";
						for (std::size_t i{0}; i < m_targets.size(); ++ i)
						{
							if (m_targets.size() == i + 1 && !allowAllFlag) msg << "and ";
							msg << '\'' << m_targets[i] << '\'';
							if (allowAllFlag || m_targets.size() > i + 1) msg << ", ";
						}
						if (allowAllFlag) msg << "and 'all'";
						msg << '.';
						
						log::err() << msg.str();
						
						return false;
					}

					/// @todo: Availability should be checked by scanning build modules.
//					if (checkAvailability)
//					{
//						if (!platform::is(Platform::mac))
//						{
//							if (target == "ios")
//							{
//								log::err() << "iOS build is currently only available on Mac.";
//								return false;
//							}
//							else if (target == "mac")
//							{
//								log::err() << "Mac build is currently only available on Mac.";
//								return false;
//							}
//							else if (target == "android")
//							{
//								log::err() << "Android build is currently only available on Mac.";
//								return false;
//							}
//							else if (target == "web")
//							{
//								log::err() << "Web build is currently only available on Mac.";
//								return false;
//							}
//						}
//						else
//						{
//							if (target == "windows")
//							{
//								log::err() << "Windows build is currently only available on Windows and Linux.";
//								return false;
//							}
//						}
//
//						if (!platform::is(Platform::linux))
//						{
//							if (target == "linux")
//							{
//								log::err() << "Linux build is currently only available on Linux.";
//								return false;
//							}
//						}
//					}
					
					return true;
				}
				
				bool Base::validTargets(const std::vector<std::string> &targets, const bool allowAllFlag) const
				{
					if (allowAllFlag && std::find(targets.begin(), targets.end(), "all") != targets.end())
					{
						if (targets.size() == 1)
							return true;
						else
						{
							log::err() << "Cannot use 'all' flag alongside other system flags.";
							return false;
						}
					}
					
					for (auto &i : targets)
						if (!validTarget(i, false, allowAllFlag))
							return false;
					
					return true;
				}
				
				/**
				 * Makes sure the specified acts are all valid.
				 */
				bool Base::validActs(const std::vector<std::string> &acts, const std::vector<std::string> &disable) const
				{
					static const std::vector<std::string> valid
					{
						"clean",
						"make",
						"build",
						"run"
					};
					
					std::vector<std::string> validFinal = valid;
					
					if (disable.size() > 0)
					{
						for (int i{static_cast<int>(validFinal.size()) - 1}; i >= 0; -- i)
						{
							const auto it(std::find(disable.begin(), disable.end(), validFinal[i]));
							if (it != disable.end())
								validFinal.erase(validFinal.begin() + i);
						}
					}

					for (auto &act : acts)
					{
						const auto it(std::find(validFinal.begin(), validFinal.end(), act));
						if (it == validFinal.end())
						{
							std::stringstream msg;
							msg << "Invalid action '" << act << "'. Valid actions are ";
							for (std::size_t i{0}; i < validFinal.size(); ++ i)
							{
								if (validFinal.size() == i + 1) msg << "and ";
								log::err() << '\'' << validFinal[i] << '\'';
								if (validFinal.size() > i + 1) msg << ", ";
							}
							msg << '.';
							
							log::err() << msg.str();
						
							return false;
						}
					}
					
					return true;
				}
				
				/// @todo: Check where loadAndSaveConfig() is used and whether it needs to exist/change/rename for new target module system.
				bool Base::loadAndSaveConfig(const std::string &target)
				{
					if (!loadInfo()) return false;

					const auto identifier(project().info()->identifier());
					if (!identifier)
					{
						log::err() << "Failed to find project identifier in info file.";
						return false;
					}
					
					const auto name(project().info()->name());
					if (!name)
					{
						log::err() << "Failed to find project name in info file.";
						return false;
					}
					
					return true;
				}
			}
		}
	}
}
