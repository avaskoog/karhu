/**
 * @author		Ava Skoog
 * @date		2017-04-02
 * @copyright   2017-2023 Ava Skoog
 */

#ifndef KARHU_TOOL_BUILD_CMD_MAKE_H_
	#define KARHU_TOOL_BUILD_CMD_MAKE_H_

	#include <karhu/tool/Command.hpp>
	#include <karhu/tool/build/cmd/Base.hpp>
	#include <karhu/tool/proj/file/Project.hpp>
	#include <karhu/tool/shared/AdapterResourcebank.hpp>

	namespace karhu
	{
		namespace tool
		{
			namespace build
			{
				namespace cmd
				{
					/**
					 * Generates build files for a Karhu project.
					 *
					 * Syntax:
					 * make [--sys <mac|windows|linux|ios|android|web>=local] [--dir <path>]
					 */
					class Make : public Command, private Base
					{
						public:
							Make()
							:
							Command
							{
								Param{"target",   Enforce::optional, ArgsMinMax{1}},
								Param{"sys",      Enforce::optional, ArgsMinMax{1}},
								Param{"conf",     Enforce::optional, ArgsMinMax{1}},
								Param{"hint",     Enforce::optional, ArgsMinMax{1}},
								Param{"settings", Enforce::optional, ArgsMinMax{1}},
								Param{"dir",      Enforce::optional, ArgsMinMax{1}}
							}
							{
							}

							bool processOpt(const OptWithVals &) override;
							bool finish() override;
						
						private:
							bool makeShaders();

						private:
							std::vector<std::string> m_targets;
							std::string m_target{"main"}, m_sys, m_conf, m_hint, m_settings, m_dir;
							std::unique_ptr<AdapterResourcebank> m_bank;
					};
				}
			}
		}
	}
#endif
