/**
 * @author		Ava Skoog
 * @date		2017-04-02
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/build/cmd/Make.hpp>
#include <karhu/tool/proj/file/build/Module.hpp>
#include <karhu/tool/proj/file/build/common.hpp>
#include <karhu/tool/proj/file/DataJSON.hpp>
#include <karhu/tool/proj/Location.hpp>
//#include <karhu/tool/code/Hotloader.hpp>
#include <karhu/tool/SDK.hpp>
#include <karhu/res/Shader.hpp>
#include <karhu/conv/types.hpp>
#include <karhu/core/platform.hpp>

#include <boost/process.hpp>

/// @todo: Replace with C++17 STL when available on all platforms.
#include <boost/filesystem.hpp>

#include <fstream>

namespace karhu
{
	namespace tool
	{
		namespace build
		{
			namespace cmd
			{
				/**
				 * Validates each incoming option and its arguments.
				 */
				bool Make::processOpt(const OptWithVals &next)
				{
					const auto &n(next.opt.name);
					const auto &v(next.vals);

					if (n == "sys")
					{
						if (validTarget(v[0]))
							m_sys = v[0];
						else
							return false;
					}
					else if (n == "conf")
					{
						if (validConfiguration(v[0]))
							m_conf = v[0];
						else
							return false;
					}
					else if (n == "settings") m_settings = v[0];
					else if (n == "target")   m_target   = v[0];
					else if (n == "hint")     m_hint     = v[0];
					else if (n == "dir")      m_dir      = v[0];

					return true;
				}

				/**
				 * Uses the validated information to make for the target platform.
				 */
				bool Make::finish()
				{
					using namespace proj::file::build;
				
					namespace scripts = SDK::paths::home::scripts;
					namespace pf      = platform;

					if (m_sys.length() == 0)
						m_sys = pf::identifier(Platform::current);
					
					if (m_conf.length() == 0)
						m_conf = config::toString(config::Type::debug);
					
					const auto conf(config::fromString(m_conf.c_str()));

					conv::JSON::Val settings;
					if (m_settings.length() > 0)
					{
						conv::JSON::Parser parser;
						std::stringstream s;
						s << m_settings;

						if (auto v = parser.parse(s))
							settings = std::move(*v);
						else
						{
							log::err()
								<< "Invalid JSON provided for '--settings': "
								<< parser.error();
							return false;
						}
					}

					if (!loadProject(m_dir))
						return false;
					
					m_bank = std::make_unique<AdapterResourcebank>(project());
					
					if (!loadAndSaveConfig(m_sys))
						return false;
					
					// Make sure the specified target exists.
					if (!validateTargetExists(m_target))
						return false;
					
					// Prepare any assets.
					if (!makeShaders())
						return false;

//					code::Hotloader hotloader;
//					if (!hotloader.refreshModules(project(), conf, false))
//						return false;

					auto module(Module::loadForPlatform(pf::fromIdentifier(m_sys.c_str())));
					if (module)
					{
						if (!module->outputBuildfiles
						(
							project(),
							conf,
							m_target,
							settings,
							m_hint
						))
							return false;
					}
					else
						return false;

					std::vector<std::string> args;
					
					args.emplace_back(module->subpath());
					args.emplace_back(location().path());
					args.emplace_back(project().info()->identifier()->proj());
					args.emplace_back(m_conf);

					/// @todo: Fix this: duplicate code mirrored in Config.
					const auto nameTarget((m_target == "main") ? project().info()->identifier()->proj() : m_target);
					args.emplace_back(nameTarget);

					args.emplace_back((m_hint.length() > 0) ? m_hint : "none");

					return tryToRunTool(m_sys, Action::make, args);
				}
				
				bool Make::makeShaders()
				{
					namespace fs    = boost::filesystem;
					namespace bp    = boost::process;
					namespace tools = SDK::paths::home::tools;
					
					fs::path respath{location().path()};
					respath.append("res"); /// @todo: Should this subdirectory be put into an SDK constant?
					respath.make_preferred();
					
					// Do we have a project resource directory?
					if (!fs::exists(respath))
						return true;
					
					// Do we have a resource database?
					fs::path databasepath{respath};
					databasepath.append(res::Bank::subpathDB());
					databasepath.make_preferred();
					if (!fs::exists(databasepath) || !fs::is_regular_file(databasepath))
						return true;
					
					// Try to load the file.
					std::ifstream fi{databasepath.string()};
					if (!fi.is_open())
					{
						log::err()
							<< "Could not open resource database at '"
							<< databasepath.string()
							<< "' for reading.";
						return false;
					}
					
					// Try to parse the JSON in there.
					conv::JSON::Parser parser;
					const auto data(parser.parse(fi));
					if (!data)
					{
						log::err()
							<< "JSON error in resource database: "
							<< parser.error();
						return false;
					}
					
					if (!data->object())
					{
						log::err() << "Missing root object in resource database.";
						return false;
					}
					
					const auto resources(data->object()->getArray("resources"));
					if (!resources)
					{
						log::err() << "Missing array 'resources' in resource database.";
						return false;
					}
					
					auto p(SDK::env::home(tools::karhuproj::local()));
					if (!p || !boost::filesystem::exists(*p))
					{
						log::err() << "Could not find karhuproj utility! Is $KARHU_HOME set up?";
						return false;
					}
					
					log::msg() << "Building shaders...";
					
					// Look for the shader assets so that they can be built.
					for (const auto &r : *resources)
					{
						if (const auto o = r.object())
						{
							const auto type(o->getInt("type"));
							const auto path(o->getString("path"));
							
							if (!type || !path)
								continue; /// @todo: Actually treat as error?
							
							// We are only interested in shaders.
							if (m_bank->template IDOfType<res::Shader>() != static_cast<res::IDTypeResource>(*type))
								continue;
							
							/// @todo: Just building every shader format for now, but do update this so that only those necessary for the target platform get built. Probably do this by supplying an array of supported shader types in the target configuration JSON file. Remember that at the moment this is duplicated in both asset refresh and build make.
							
							static constexpr std::array<const char *, 2> formats
							{
								"gl",
								"gles"/*,
								"metal"*/
								/// @todo: Work out quirks with metal and reënable it.
							};
							
							for (const auto &format : formats)
							{
								// Set arguments up for building shader.
								
								std::vector<std::string> args;
								args.emplace_back("asset");
								args.emplace_back("--act");
								args.emplace_back("build");
								args.emplace_back("--type");
								args.emplace_back("shader");
								args.emplace_back("--name");
								args.emplace_back(*path);
								args.emplace_back("--format");
								args.emplace_back(format);
								
								log::msg()
									<< "Building shader '"
									<< *path
									<< "' with format '"
									<< format
									<< "'...";
								
								// Run the tool!
								
								bp::child c{*p, args, bp::std_out > stdout, bp::std_err > stderr};
								
								c.wait();
								
								if (0 != c.exit_code())
								{
									log::err() << "Failed to build shader!";
									return false;
								}
								
								log::msg() << "Successfully built shader!";
							}
						}
					}
					
					return true;
				}
			}
		}
	}
}
