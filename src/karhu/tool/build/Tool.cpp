/**
 * @author		Ava Skoog
 * @date		2017-04-16
 * @copyright   2017-2023 Ava Skoog
 */

#include <karhu/tool/build/Tool.hpp>
#include <karhu/tool/build/cmd/Make.hpp>
#include <karhu/tool/build/cmd/Build.hpp>
#include <karhu/tool/build/cmd/Run.hpp>
#include <karhu/tool/build/cmd/Clean.hpp>
#include <karhu/tool/build/cmd/SSH.hpp>
#include <karhu/tool/build/cmd/Apply.hpp>

namespace karhu
{
	namespace tool
	{
		namespace build
		{
			Tool::Tool()
			{
				registerCommand<cmd::Make> ("make");
				registerCommand<cmd::Build>("build");
				registerCommand<cmd::Run>  ("run");
				registerCommand<cmd::Clean>("clean");
				registerCommand<cmd::SSH>  ("ssh");
				registerCommand<cmd::Apply>("apply");
			}
		}
	}
}
