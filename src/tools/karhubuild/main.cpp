#include <karhu/tool/build/Tool.hpp>
#include <karhu/tool/proj/file/build/Module.hpp>

int main(int argc, char *argv[])
{
	using namespace karhu::tool::proj;
	using namespace karhu::tool::proj::file::build;

//	if (auto t = Module::loadForPlatform(karhu::Platform::mac))
//	{
//		Location l{"/Users/avaskoog/Documents/karhutestingz/alimcbabber"};
//		if (auto p = file::Project::load(l))
//		{
//			t->outputBuildfiles(*p, config::Type::release);
//		}
//		else
//			karhu::log::err() << "LOAD PROJECT ERROR";
//	}
//	else
//		karhu::log::err() << "LOAD FOR PLATFORM ERROR";

	return karhu::tool::build::Tool{}.run(argc, argv);
	//return 0;
}
