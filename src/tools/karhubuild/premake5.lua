PROJECTNAME = "karhubuild"

if os.get() == "macosx" then
	nameSystemTarget = "mac"
else
	nameSystemTarget = os.get()
end

workspace(PROJECTNAME)
	location("../../../tmp/generated/" .. PROJECTNAME)
	language "C++"

	architecture "x86_64"

	flags{"C++14"}

	targetdir "../../../tools/%{nameSystemTarget}"
	objdir   ("../../../tmp/build/%{nameSystemTarget}/obj/" .. PROJECTNAME)

	configurations{"Debug"}
	defines{"KARHU_TOOL"}

	filter{"configurations:Debug"}
		symbols "On"
		defines{"DEBUG"}

	filter{"system:macosx"}
		sysincludedirs{"/usr/local/include"}
		buildoptions{"-I/usr/local/include"}

		syslibdirs{"/usr/local/lib"}
		linkoptions{"-L/usr/local/lib", "-L/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib"}

		toolset "clang"

	filter{"system:linux"}
		sysincludedirs{"/usr/local/include"}
		buildoptions{"-I/usr/local/include"}
		linkoptions{"-L/usr/lib/llvm-3.8/lib"}
		toolset      "gcc"
		buildoptions{"-std=c++1y", "-g", "-ggdb"}

project(PROJECTNAME)
	files
	{
		"main.cpp",
		"../../karhu/core/**.cpp",
		"../../karhu/tool/**.cpp",
		"../../karhu/conv/**.cpp",
		"../../karhu/res/**.cpp",
		"../../karhu/proj/**.cpp",
		"../../karhu/lib/tinyxml2/**.cpp"
	}
	excludes
	{
		"../../karhu/tool/server/**",
		"../../karhu/tool/lib/asio/**",
		"../../karhu/tool/lib/efsw/**",
		"../../karhu/tool/code/**",
		"../../karhu/tool/network/Server.cpp",
		"../../karhu/tool/network/ServerDebug.cpp"
	}
	kind  "ConsoleApp"

	includedirs{"../..", "../../karhu/tool/lib", "../../karhu/tool/lib/asio"}
	links{"boost_system", "boost_filesystem"}

	filter{"system:linux"}
		links{"pthread"}
