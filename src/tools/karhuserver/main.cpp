#include <karhu/tool/server/Tool.hpp>

int main(int argc, char *argv[])
{
	return karhu::tool::server::Tool{}.run(argc, argv);
}
