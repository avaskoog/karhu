#!/bin/sh

systemname=$1

if [ "$systemname" != "mac" ] && [ "$systemname" != "linux" ]; then
	echo "An argument for the system ('mac' or 'linux') must be passed to this script!" >&2
	exit 1
fi

cd ${0%/*}

basedir=$(pwd)

premake5 gmake

cd ../../../tmp/generated/karhuserver
make

cd ../..
rm -rf "generated/karhuserver"
rm -rf "build/$systemname/obj/karhuserver"

cd $basedir
