PROJECTNAME = "karhuserver"

if os.get() == "macosx" then
	nameSystemTarget = "mac"
else
	nameSystemTarget = os.get()
end

workspace(PROJECTNAME)
	location("../../../tmp/generated/" .. PROJECTNAME)
	language "C++"

	architecture "x86_64"

	flags{"C++14"}

	targetdir "../../../tools/%{nameSystemTarget}"
	objdir   ("../../../tmp/build/%{nameSystemTarget}/obj/" .. PROJECTNAME)

	configurations{"Release"}
	defines{"KARHU_TOOL"}

	filter{"configurations:Release"}
		optimize "On"

	filter{"system:macosx"}
		sysincludedirs{"/usr/local/include"}
		buildoptions{"-I/usr/local/include"}

		syslibdirs{"/usr/local/lib"}
		linkoptions{"-L/usr/local/lib", "-L/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib"}

		toolset "clang"

	filter{"system:linux"}
		sysincludedirs{"/usr/local/include"}
		buildoptions{"-I/usr/local/include"}
		linkoptions{"-L/usr/lib/llvm-3.8/lib"}
		toolset      "gcc"
		buildoptions{"-std=c++1y"}

project(PROJECTNAME)
	files
	{
		"main.cpp",
		"../../karhu/core/**.cpp",
		"../../karhu/conv/**.cpp",
		"../../karhu/network/**.cpp",
		"../../karhu/res/**.cpp",
		"../../karhu/tool/*.cpp",
		"../../karhu/tool/server/**.cpp",
		"../../karhu/tool/shared/**.cpp",
		"../../karhu/tool/lib/**.cpp",
		"../../karhu/tool/network/**.cpp",
		"../../karhu/tool/proj/**.cpp",
		"../../karhu/lib/tinyxml2/**.cpp"
	}
	excludes
	{
		"../../karhu/tool/lib/asio/**",
		"../../karhu/tool/lib/efsw/**",
	}
	kind  "ConsoleApp"

	includedirs{"../..", "../../karhu/tool/lib", "../../karhu/tool/lib/asio"}
	links{"boost_system", "boost_filesystem", "clang"}

	filter{"system:linux"}
		links{"pthread"}

