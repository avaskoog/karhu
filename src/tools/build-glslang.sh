#!/bin/sh

systemname=$1

if [ "$systemname" != "mac" ] && [ "$systemname" != "linux" ]; then
	echo "An argument for the system ('mac' or 'linux') must be passed to this script!" >&2
	exit 1
fi

cd ${0%/*}
basedir=$(pwd)

cd "glslang-master"
mkdir builddir
cd builddir

cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="$(pwd)/install" ..
if [ ! $? -eq 0 ]; then
	echo "Failed to make glslang" >&2
	cd ..
	rm -rf builddir
	exit 1
fi

make -j4 install
if [ ! $? -eq 0 ]; then
	echo "Failed to build glslang" >&2
	cd ..
	rm -rf builddir
	exit 1
fi

if [ ! -d "./install/bin" ]; then
	echo "Failed to build glslang" >&2
	cd ..
	rm -rf builddir
	exit 1
fi

if [ ! -f "./install/bin/glslangValidator" ]; then
	echo "Failed to build glslangValidator" >&2
	cd ..
	rm -rf builddir
	exit 1
fi

if [ ! -f "./install/bin/spirv-remap" ]; then
	echo "Failed to build spirv-remap" >&2
	cd ..
	rm -rf builddir
	exit 1
fi

rm -rf "../../../../tools/$systemname/glslang"
rm -rf "../../../../tools/$systemname/glslang"
cp -rf "./install/bin/." "../../../../tools/$systemname/glslang"

cd ..
rm -rf builddir

cd "$basedir"
