#!/bin/sh

systemname=$1

if [ "$systemname" != "mac" ] && [ "$systemname" != "linux" ]; then
	echo "An argument for the system ('mac' or 'linux') must be passed to this script!" >&2
	exit 1
fi

cd ${0%/*}

basedir=$(pwd)

cd "SPIRV-Cross-master"

make
if [ ! $? -eq 0 ]; then
	echo "Failed to make SPIRV-Cross" >&2
	exit 1
fi

if [ ! -f "./spirv-cross" ]; then
	echo "Failed to build SPIRV-Cross" >&2
	exit 1
fi

rm "../../../tools/$systemname/spirv-cross"
cp "./spirv-cross" "../../../tools/$systemname/"

make clean

cd "$basedir"
