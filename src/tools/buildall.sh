#!/bin/sh

systemname=$1

if [ "$systemname" != "mac" ] && [ "$systemname" != "linux" ]; then
	echo "An argument for the system ('mac' or 'linux') must be passed to this script!" >&2
	exit 1
fi

cd ${0%/*}

basedir=$(pwd)

cd karhuproj
. ./build.sh "$systemname"

cd ..

cd karhubuild
. ./build.sh "$systemname"

cd ..

cd karhuserver
. ./build.sh "$systemname"

cd ..

cd karhudata
. ./build.sh "$systemname"

cd ..

. ./build-spirv-cross.sh "$systemname"
. ./build-glslang.sh "$systemname"

cd "$basedir"
