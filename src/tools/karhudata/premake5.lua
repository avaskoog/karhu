PROJECTNAME = "karhudata"

if os.get() == "macosx" then
	nameSystemTarget = "mac"
else
	nameSystemTarget = os.get()
end

workspace(PROJECTNAME)
	location("../../../tmp/generated/" .. PROJECTNAME)
	language "C++"

	architecture "x86_64"

	flags{"C++14"}

	targetdir "../../../tools/%{nameSystemTarget}"
	objdir   ("../../../tmp/build/%{nameSystemTarget}/obj/" .. PROJECTNAME)

	configurations{"Release"}
	defines{"KARHU_TOOL"}

	filter{"configurations:Release"}
		optimize "On"

	filter{"system:macosx"}
		sysincludedirs{"/usr/local/include"}
		buildoptions{"-I/usr/local/include"}

		syslibdirs{"/usr/local/lib"}
		linkoptions{"-L/usr/local/lib"}

		toolset "clang"

	filter{"system:linux"}
		sysincludedirs{"/usr/local/include"}
		buildoptions{"-I/usr/local/include"}
		toolset      "gcc"
		buildoptions{"-std=c++1y"}

project(PROJECTNAME)
	files
	{
		"main.cpp",
		"../../karhu/core/**.cpp",
		"../../karhu/conv/**.cpp",
		"../../karhu/proj/**.cpp",
		"../../karhu/res/**.cpp",
		"../../karhu/tool/data/**.cpp",
		"../../karhu/tool/Command.cpp",
		"../../karhu/tool/SDK.cpp",
		"../../karhu/tool/Tool.cpp",
		"../../karhu/tool/proj/Identifier.cpp",
		"../../karhu/lib/tinyxml2/**.cpp",
		"lib/**.cpp"
	}
	excludes
	{
		"../../karhu/tool/server/**",
		"../../karhu/tool/build/**",
		"../../karhu/tool/code/**",
		"../../karhu/tool/lib/asio/**",
		"../../karhu/tool/lib/efsw/**",
		"../../karhu/tool/network/Server.cpp",
		"../../karhu/tool/network/ServerDebug.cpp"
	}
	kind "ConsoleApp"

	includedirs{"../..", "../../karhu/tool/lib"}
	links{"boost_system", "boost_filesystem"}

	filter{"system:linux"}
		links{"pthread"}
