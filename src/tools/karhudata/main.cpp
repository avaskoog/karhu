#include <karhu/tool/data/Tool.hpp>

/// @todo: Fix GLSL > MSL etc.

// kompilera MSL under køyring:
// https://stackoverflow.com/questions/32298719/manually-compile-metal-shaders

int main(int argc, char *argv[])
{
	return karhu::tool::data::Tool{}.run(argc, argv);
}
