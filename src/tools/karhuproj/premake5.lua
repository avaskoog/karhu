PROJECTNAME = "karhuproj"

if os.get() == "macosx" then
	nameSystemTarget = "mac"
else
	nameSystemTarget = os.get()
end

workspace(PROJECTNAME)
	location("../../../tmp/generated/" .. PROJECTNAME)
	language "C++"

	architecture "x86_64"

	flags{"C++14"}

	targetdir "../../../tools/%{nameSystemTarget}"
	objdir   ("../../../tmp/build/%{nameSystemTarget}/obj/" .. PROJECTNAME)

	configurations{"Release"}
	defines{"KARHU_TOOL"}

	filter{"configurations:Release"}
		optimize "On"

	filter{"system:macosx"}
		sysincludedirs{"/usr/local/include"}
		buildoptions{"-I/usr/local/include"}

		syslibdirs{"/usr/local/lib"}
		linkoptions{"-L/usr/local/lib"}

		toolset "clang"

	filter{"system:linux"}
		sysincludedirs{"/usr/local/include"}
		buildoptions{"-I/usr/local/include"}
		toolset      "gcc"
		buildoptions{"-std=c++1y"}

project(PROJECTNAME)
	files
	{
		"main.cpp",
		"../../karhu/core/**.cpp",
		"../../karhu/conv/**.cpp",
		"../../karhu/res/**.cpp",
		"../../karhu/proj/**.cpp",
		"../../karhu/lib/tinyxml2/**.cpp",
		"../../karhu/tool/**.cpp"
	}
	excludes
	{
		"../../karhu/tool/server/**",
		"../../karhu/tool/build/**",
		"../../karhu/tool/code/**",
		"../../karhu/tool/lib/asio/**",
		"../../karhu/tool/lib/efsw/**",
		"../../karhu/tool/network/Server.cpp",
		"../../karhu/tool/network/ServerDebug.cpp"
	}
	kind  "ConsoleApp"

	includedirs{"../..", "../../karhu/tool/lib", "../../karhu/tool/lib/asio"}
	links{"boost_system", "boost_filesystem"}

	filter{"system:linux"}
		links{"pthread"}
