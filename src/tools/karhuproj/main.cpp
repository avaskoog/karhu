#include <karhu/tool/proj/Tool.hpp>

int main(int argc, char *argv[])
{
	// Create new project:
	// new --id <ext.org.proj> --name <unicode> [--template <folder>=project] [--dir <path>=current]
	
	// Create project for karhu lib:
	// karhu [--dir <path>=current]

	// ascii is taken from the end of org, while name is separate
	
	// in plist, CFBundleName is changed from $(PRODUCT_NAME) to the $(EXECUTABLE_NAME) which is set in premake to literal unicode name while $(PRODUCT_NAME) remains ascii
	// https://developer.apple.com/library/content/qa/qa1823/_index.html Important: If your app supports localization, be sure to localize CFBundleDisplayName by adding it to all your language-specific InfoPlist.strings files. Furthermore, be sure to use a name that complies with the App Review Guidelines for your app.
	// CFBundleDisplayName på ios = namn på heimeskjerm???
	
	// $(PRODUCT_NAME:rfc1034identifier)
	
	// definer KARHU_EDITOR i premake om byggjer frå editor
	
	return karhu::tool::proj::Tool{}.run(argc, argv);
}
