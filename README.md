![Karhu logo](res/logo-text-inv-transp.svg)

This is a cross-platform game development library/engine/framework and toolkit based on [SDL 2](http://www.sdl.com/). Progressing rapidly ATM, but still very WIP. The currently supported target systems are:

* macOS
* Linux
* Windows
* iOS
* Android
* web

Currently for personal use and so the tools are only implemented in accordance with my own needs, which is to build for macOS, iOS, Android and web from a macOS machine and for Linux and Windows on a Linux machine. Building is currently not implemented to run on Windows.

There is a [wiki](https://bitbucket.org/avaskoog/karhu/wiki/Home)!

Not everything necessary for this setup is packed into the repository, like an older version of the Android SDK (the build system for Android doesn't work with the latest one) and the [CrystaX NDK](https://www.crystax.net/android/ndk) in place of the regular NDK to enable C++14. Also Emscripten. Might put up a separate zip somewhere later and have an installation script download it or w/e. Many other dependencies are completely external, including SDL 2 itself because it's easier to just install that the standard way using apt-get or Homebrew. I'll try to put as many notes as possible on this in the wiki. In the future I may write some install script to automate as much of this as possible.

To conclude, there is really no point in, or any user-friendly way of, using this for anybody but myself yet, and I'm not really intent on making it so, but I'm keeping the repo public anyway.

Hello, triangle! | Trying out [Kirja](https://bitbucket.org/avaskoog/kirja)
- | -
![Lots of triangles](res/tri.png) | ![Text library](res/kirja.png)
