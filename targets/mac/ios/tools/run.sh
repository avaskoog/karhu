#!/bin/sh

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: executable name
# $5: full identifier
# $6: configuration (debug or release)
# $7: whether to wait to capture output [y/n]
# $8: arbitrary hint
# $…: arguments

modulesubdir=$1
basedir=$2
targetname=$3
exename=$4
identifier=$5
configuration=$6
captureoutput=$7
hint=$8

if [ "$hint" == "simulator" ]; then
	hash ios-sim 2>/dev/null || { echo "ios-sim was not found" >&2; exit 1; }
else
	hash ios-deploy 2>/dev/null || { echo "ios-deploy was not found" >&2; exit 1; }
fi

if [ "$configuration" != "debug" ] && [ "$configuration" != "release" ]; then
	echo "Invalid configuration $configuration" >&2
fi

if [ ! -d $basedir ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

fullexepath="./build/ios/bin/$targetname/$configuration/$exename.app"

cd $basedir

if [ ! -d "$fullexepath" ]; then
	echo "The build command does not appear to have been successfully applied to this project, platform and configuration" >&2
	exit 1
fi

argstring="";
if [ "$#" -gt "8" ]; then
	argstring+="--args \""

	for i in "${@:9}"; do
		# ios-deploy doesn't like initial hyphens so we add an escape character.
		argstring+=" \\\"$i\\\""
	done

	argstring+="\""
fi

if [ "$hint" == "simulator" ]; then
	paramstring="--devicetypeid "

	# First we have to extract the available devices.
	IFS=$'\n'
	devicearr=( $(ios-sim showdevicetypes) )

	# Loop through them to find a good one.
	for element in "${devicearr[@]}"
	do
		# We need to match one that supports GLES3.
		# Any iPad Air should do it.
		if [[ "$element" == *"iPad-Air"* ]]; then
			paramstring+="\"$element\""
			break
		fi
	done

	if [ $captureoutput == "n" ]; then
		paramstring+=" --exit"
	fi

	echo "ios-sim launch \"$fullexepath\" $paramstring $argstring"
	eval ios-sim launch \"$fullexepath\" $paramstring $argstring
else
	paramstring="--justlaunch"
	if [ $captureoutput == "y" ]; then
		paramstring="--noninteractive"
	fi

	echo "ios-deploy $paramstring --bundle \"$fullexepath\" $argstring"
	eval ios-deploy $paramstring --bundle \"$fullexepath\" $argstring
fi

if [ ! $? -eq 0 ]; then
	echo "Running on iOS failed" >&2
	exit 1
fi

exit 0
