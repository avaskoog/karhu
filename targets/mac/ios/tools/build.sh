#!/bin/sh

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: executable name
# $5: configuration (debug or release)
# $6: name of target to build
# $7: arbitrary hint

modulesubdir=$1
basedir=$2
targetname=$3
exename=$4
configuration=$5
buildtargetname=$6
hint=$7

if [ "$configuration" != "debug" ] && [ "$configuration" != "release" ]; then
	echo "Invalid configuration '$configuration'" >&2
	exit 1
fi

if [ ! -d $basedir ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

hash xcodebuild 2>/dev/null || { echo  "xcodebuild was not found" >&2; exit 1; }
hash ruby 2>/dev/null || { echo "ruby was not found" >&2; exit 1; }
hash idevice_id 2>/dev/null || { echo "idevice_id was not found" >&2; exit 1; }

rubyxcodebuildtest=$(gem list xcodeproj -i)

if [ "$rubyxcodebuildtest" != "true" ]; then
	echo "ruby gem xcodebuild not found" >&2
	exit 1
fi

cd $basedir

if  [ ! -d "./generated/ios" ]; then
	echo "The premake command does not appear to have been successfully applied to this project and platform" >&2
	exit 1
fi

if [ "$hint" == "simulator" ]; then
	if [ "$configuration" == "debug" ]; then
		xcodebuild -workspace "./generated/ios/$targetname.xcworkspace" -scheme "$buildtargetname" -sdk iphonesimulator -arch x86_64 VALID_ARCHS="x86_64" -configuration debug
	else
		xcodebuild -workspace "./generated/ios/$targetname.xcworkspace" -scheme "$buildtargetname" -sdk iphonesimulator -arch x86_64 VALID_ARCHS="x86_64" -configuration release
	fi
else
	deviceidentifier=$(idevice_id -l)
	if [ "$deviceidentifier" == "" ]; then
		echo "Could not find a connected device" >&2
		exit 1
	fi

	if [ "$configuration" == "debug" ]; then
		xcodebuild -workspace "./generated/ios/$targetname.xcworkspace" -scheme "$buildtargetname" -sdk iphoneos -configuration debug -destination "platform=iOS,id=$deviceidentifier"
	else
		xcodebuild -workspace "./generated/ios/$targetname.xcworkspace" -scheme "$buildtargetname" -sdk iphoneos -configuration release -destination "platform=iOS,id=$deviceidentifier"
	fi
fi

if [ ! $? -eq 0 ]; then
	echo "Failed to build for iOS" >&2
	exit 1
fi

cd $basedir
exit 0
