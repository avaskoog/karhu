#!/bin/sh

# $1: working directory
# $2: target name

basedir=$1
targetname=$2

if [ ! -d $basedir ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

cd $basedir

rm -rf "./generated/$targetname"
rm -rf "./build/$targetname"

exit 0
