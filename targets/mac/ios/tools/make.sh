#!/bin/sh

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: configuration (debug or release)
# $5: name of target to build
# $6: arbitrary hint

modulesubdir=$1
basedir=$2
targetname=$3
configuration=$4
buildtargetname=$5
hint=$6

if [ "simulator" != "$hint" ] && [ "device" != "$hint" ]; then
	echo "No '--hint' argument provided! Must be 'simulator' or 'device'."
	exit 1
fi

if [ ! -d "$basedir" ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

hash premake5 2>/dev/null || { echo  "premake5 was not found" >&2; exit 1; }

cd "$basedir"

# Check if this is an app target.
targettype=$(karhudata json --act format --file generated/mac/data/$targetname.json --data string type)
if [ "app" = "$targettype" ]; then
	# Load the identifier.
	idext=$(karhudata json --act format --file info.karhuproj.json --data string id ext)
	idorg=$(karhudata json --act format --file info.karhuproj.json --data string id org)
	idproj=$(karhudata json --act format --file info.karhuproj.json --data string id proj)
	identifier="$idext.$idorg.$idproj"

	# Create Info.plist in the generated folder.
	if [ ! -d "generated" ]; then
		mkdir "generated"
	fi
	if [ ! -d "generated/ios" ]; then
		mkdir "generated/ios"
	fi
	karhudata plist --act create --type ios --file "generated/ios/Info.plist" --id $identifier

	if [ ! $? -eq 0 ]; then
		echo "Failed to generate Info.plist" >&2
		exit 1
	fi
fi

cd generated/ios
premake5 xcode4

if [ ! $? -eq 0 ]; then
	echo "Failed to generate iOS build files with premake" >&2
	exit 1
fi

rm -rf "$buildtargetname.xcodeproj/*.xcuserdatad/xcschemes"

echo "
	require 'xcodeproj'

	project = Xcodeproj::Project.open '$buildtargetname.xcodeproj'
	project.recreate_user_schemes
	project.save

	data_dir = Xcodeproj::XCScheme.user_data_dir '$buildtargetname.xcodeproj'
	scheme_name = '$buildtargetname.xcscheme'
	scheme = Xcodeproj::XCScheme.new File.join(data_dir, scheme_name)

	build_action = scheme.build_action
	entries = build_action.entries
	entry = entries.at(0)
	buildable_references = entry.buildable_references
	buildable_reference_original = buildable_references.at(0)

	launch_action = scheme.launch_action;
	buildable_product_runnable = launch_action.buildable_product_runnable;
	buildable_reference = buildable_product_runnable.buildable_reference

	buildable_reference.buildable_name = buildable_reference_original.buildable_name
	buildable_reference.set_reference_target(project.targets.find{|t| t.uuid == buildable_reference_original.target_uuid})

	buildable_product_runnable.buildable_reference = buildable_reference;
	launch_action.buildable_product_runnable = buildable_product_runnable;
	scheme.launch_action = launch_action;

	scheme.save!
" | ruby

# We need to set the icon and splash images.
# TODO: Check if there are custom files to copy instead of the defaults.
# INFO: https://icons8.com/articles/choosing-the-right-size-and-format-for-icons/
# INFO: https://developer.apple.com/design/human-interface-guidelines/ios/icons-and-images/app-icon/
# NAME: https://developer.apple.com/library/archive/qa/qa1686/_index.html
# INFO: https://developer.apple.com/design/human-interface-guidelines/ios/icons-and-images/launch-screen/
# NAME/SIZE: https://github.com/mesmotronic/air-ios-launch-images
# TODO: There is a problem with the Ruby xcodeproj module that makes things go nasty in copy_bundle_res.rb unless a quick hack outlined at the bottom of said file is carried out; try and fix this, preferrably in a way that doesn't mean listing the hack on the wiki but by modifying said file.
# TODO: Use imagemagick to generate the correct sizes:
# MORE: http://www.imagemagick.org/discourse-server/viewtopic.php?t=18545
# CODE: convert image -resize "275x275^" -gravity center -crop 275x275+0+0 +repage resultimage
if [ "app" = "$targettype" ]; then
	# Generate the icons and splashes!
	imgtool="$KARHU_HOME/tools/scripts/unix/karhuimg.sh"
	echo "Generating icons..."
	filelist=$("$imgtool" $basedir ios icon "$KARHU_HOME/$modulesubdir/templates.karhutarget.json")
	echo "Icons generated!"
	echo "Generating splashes..."
	filelist+=$("$imgtool" $basedir ios splash "$KARHU_HOME/$modulesubdir/templates.karhutarget.json")
	echo "Splashes generated!"

	tooldir="$KARHU_HOME/$modulesubdir/tools"
	eval ruby \"$tooldir/copy_bundle_res.rb\" \"$targetname.xcodeproj\" \"$buildtargetname\" $filelist
fi

# Time to zip and copy over app resources.
# TODO: Can we add all the resources in the same command as the icons et cetera? Because Xcode creates one 'resources' folder for every image added which is a bit ugly...
if [ "app" = "$targettype" ]; then
	# Check that there are any resources at all.
	# TODO: Should it be an error if not?
	resdir="$basedir/res"
	if [ -d "$resdir" ]; then
		echo "Packing zip!"

		cd "$resdir"

		# Remove any old zip first to ensure a fresh one.
		zippath="$basedir/generated/ios/res.zip"
		rm "$basedir/generated/ios/res.zip" 2>/dev/null
		zip -r9 $zippath . -x "**.DS_Store"

		# Copy it into the bundle resources.
		cd "$basedir/generated/ios"
		tooldir="$KARHU_HOME/$modulesubdir/tools"
		eval ruby \"$tooldir/copy_bundle_res.rb\" \"$targetname.xcodeproj\" \"$buildtargetname\" "res.zip"
	fi
fi

cd $basedir

exit 0
