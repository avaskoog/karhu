#!/bin/sh

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: executable name
# $5: configuration (debug or release)

modulesubdir=$1
basedir=$2
targetname=$3
exename=$4
configuration=$5

if [ ! -d $basedir ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

if [ "$configuration" != "debug" ] && [ "$configuration" != "release" ]; then
	echo "Invalid configuration '$configuration'" >&2
	exit 1
fi

cd "$basedir"

if  [ ! -d "./build/android" ]; then
	echo "The cmake command does not appear to have been successfully applied to this project and platform" >&2
	exit 1
fi

cd "./build/android"

export ANDROID_HOME="$KARHU_HOME/$modulesubdir/sys/android-sdk"
export PATH="$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools"

configurationtype=$([ "$configuration" == "debug" ] && echo "debug" || echo "release")

cmake --build .

cd "$basedir"
exit 0
