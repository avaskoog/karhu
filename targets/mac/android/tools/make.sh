#!/bin/sh

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: configuration (debug or release)
# $5: name of target to build
# $6: arbitrary hint

modulesubdir=$1
basedir=$2
targetname=$3
configuration=$4
buildtargetname=$5
hint=$6

# @todo: Get API level and such from settings/args as well.
# @todo: Be able to set Android configuration.
# @todo: Does partial rebuild work for Android?

if [ ! -d "$basedir" ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

if [ ! -d "$KARHU_HOME" ]; then
	echo "Invalid KARHU_HOME directory" >&2
	exit 1
fi

hash cmake 2>/dev/null || { echo  "cmake was not found" >&2; exit 1; }

cd "$basedir"

cp -rf "$KARHU_HOME/$modulesubdir/templates/src/java/org" "./generated/android/"
cp "$KARHU_HOME/$modulesubdir/templates/AndroidManifest.xml.in" "./generated/android"
cp "$KARHU_HOME/$modulesubdir/templates/Game.java.in" "./generated/android/"
cp "$KARHU_HOME/$modulesubdir/templates/strings.xml.in" "./generated/android"
cp "$KARHU_HOME/$modulesubdir/templates/styles.xml.in" "./generated/android"

# We need to set the icon and splash image.
# More info: https://stackoverflow.com/questions/5350624/set-icon-for-android-application
# More info: https://stackoverflow.com/questions/5486789/how-do-i-make-a-splash-screen
# More info: https://plus.google.com/+AndroidDevelopers/posts/Z1Wwainpjhd
# INFO: https://icons8.com/articles/choosing-the-right-size-and-format-for-icons/
# INFO: https://github.com/phonegap/phonegap/wiki/App-Icon-Sizes
# INFO: https://github.com/phonegap/phonegap/wiki/App-Splash-Screen-Sizes
# drawable-land-* / drawable-port-* (https://developer.android.com/guide/topics/resources/providing-resources)
# TODO: Use imagemagick to generate the correct sizes:
# MORE: http://www.imagemagick.org/discourse-server/viewtopic.php?t=18545
# CODE: convert image -resize "275x275^" -gravity center -crop 275x275+0+0 +repage resultimage
# Check if this is an app target.
targettype=$(karhudata json --act format --file generated/mac/data/$targetname.json --data string type)
if [ "app" = "$targettype" ]; then
	cd "./generated/android"

	# Generate the icons and splashes!
	imgtool="$KARHU_HOME/tools/scripts/unix/karhuimg.sh"
	echo "Generating icons..."
	"$imgtool" $basedir android icon "$KARHU_HOME/$modulesubdir/templates.karhutarget.json"
	echo "Icons generated!"
	echo "Generating splashes..."
	"$imgtool" $basedir android splash "$KARHU_HOME/$modulesubdir/templates.karhutarget.json"
	echo "Splashes generated!"

	# Create the resource directory and move the files over.
	mkdir "res"

	if [ -d "icon" ]; then
		rsync -a --remove-source-files "icon/" "res/"
		rm -rf "icon"
	fi

	if [ -d "splash" ]; then
		rsync -a --remove-source-files "splash/" "res/"
		rm -rf "splash"
	fi

	# Copy over any extra resources from the target template directory.
	rsync -a "$KARHU_HOME/$modulesubdir/templates/drawable/" "res/"

	# TODO: We have to use API 19 to use xxxhdpi; fix this later, just removing the files for now.
	rm -rf "res/drawable-xxxhdpi"
	rm -rf "res/drawable-land-xxxhdpi"
	rm -rf "res/drawable-port-xxxhdpi"
fi

cd "$basedir"

if [ ! -d build ]; then
	mkdir build
fi

cd build/

if [ ! -d android ]; then
	mkdir android
fi

cd android

export ANDROID_HOME="$KARHU_HOME/$modulesubdir/sys/android-sdk"
export PATH="$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools"

buildtypestring=""
if [ "$configuration" == "debug" ]; then
	buildtypestring="-DCMAKE_BUILD_TYPE=Debug"
else
	buildtypestring="-DCMAKE_BUILD_TYPE=Release"
fi

cmake -DCMAKE_TOOLCHAIN_FILE="$KARHU_HOME/$modulesubdir/cmake/android-cmake/android.toolchain.cmake" \
-DANDROID_NDK="$KARHU_HOME/$modulesubdir/sys/crystax-ndk" \
-DANDROID_ABI=armeabi-v7a \
-DANDROID_NATIVE_API_LEVEL=android-18 \
-DANDROID_TOOLCHAIN_NAME=arm-linux-androideabi-5 \
-DANDROID_COMPILER_VERSION=5 \
-DANDROID_STL=gnustl_static \
-DCMAKE_ANDROID_STL_TYPE=gnustl_static \
-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
$buildtypestring \
"$basedir/generated/android"

if [ ! $? -eq 0 ]; then
	echo "cmake for Android failed" >&2
	exit 1
fi

exit 0
