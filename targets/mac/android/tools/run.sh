#!/bin/sh

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: executable name
# $5: full identifier
# $6: configuration (debug or release)
# $7: whether to wait to capture output [y/n]
# $8: arbitrary hint
# $…: arguments

modulesubdir=$1
basedir=$2
targetname=$3
exename=$4
identifier=$5
configuration=$6
captureoutput=$7
hint=$8

if [ ! -d $basedir ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

if [ "$configuration" != "debug" ] && [ "$configuration" != "release" ]; then
	echo "Invalid configuration '$configuration'" >&2
	exit 1
fi

export ANDROID_HOME=$KARHU_HOME/$modulesubdir/sys/android-sdk
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

hash adb 2>/dev/null || { echo  "adb was not found" >&2; exit 1; }

cd "$basedir"

#apkconfig=$configuration
apkconfig="debug" # TODO: This should be $configuration but for now the name will always be debug because release builds have not been fixed yet.
idproj=$(karhudata json --act format --file info.karhuproj.json --data string id proj)
fullexepath="./build/android/$identifier/bin/$idproj-$apkconfig.apk"

if [ ! -f "$fullexepath" ]; then
	echo "The build command does not appear to have been successfully applied to this project, platform and configuration" >&2
	exit 1
fi

argstring="";
if [ "$#" -gt "8" ]; then
	argcount=$(($#-8))
	argstring+="-e argc $argcount"

	icurr=0
	for i in "${@:9}"; do
		argstring+=" -e arg$icurr \"$i\""
		icurr=$(($icurr+1))
	done
fi

adb uninstall "$identifier"
adb install -r "$fullexepath"

if [ ! $? -eq 0 ]; then
	echo "Installation onto Android device failed" >&2
	exit 1
fi

eval adb shell am start -n \"$identifier/$identifier.Game\" $argstring

if [ ! $? -eq 0 ]; then
	echo "Running on Android device failed" >&2
	exit 1
fi

cd "$basedir"
exit 0
