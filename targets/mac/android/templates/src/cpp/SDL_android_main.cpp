#include <SDL2/SDL_config.h>

#ifdef __ANDROID__

/* Include the SDL main definition header */
#include <SDL2/SDL_main.h>

/*******************************************************************************
                 Functions called by JNI
*******************************************************************************/
#include <jni.h>

#include <android/log.h>

extern "C" { 
/* Called before SDL_main() to initialize JNI bindings in SDL library */
extern void SDL_Android_Init(JNIEnv* env, jclass cls);
 
/* Start up the SDL app */
void Java_org_libsdl_app_SDLActivity_nativeInit(JNIEnv* env, jclass cls, jobjectArray obj)
{
    /* This interface could expand with ABI negotiation, calbacks, etc. */
    SDL_Android_Init(env, cls);

    SDL_SetMainReady();

    __android_log_print(ANDROID_LOG_INFO, "Karhu", "WOW WE ARE IN NATIVE INIT!!");

    // The array contains the command line arguments.
    int stringCount = env->GetArrayLength(obj);

    __android_log_print(ANDROID_LOG_INFO, "Karhu", "We got %i string args!!", stringCount);

    // We want room for the first default argument, the strings, and a dummy.
    char *argv[stringCount + 2];
    argv[0] = SDL_strdup("app_process");
    argv[stringCount + 1] = NULL;

    // Find and add the strings.
    for (int i = 0; i < stringCount; ++ i)
    {
        jstring string = (jstring)(env->GetObjectArrayElement(obj, i));
        const char *rawString = env->GetStringUTFChars(string, 0);

        __android_log_print(ANDROID_LOG_INFO, "Karhu", "Arg %i = '%s'", (i + 1), rawString);
        argv[i + 1] = SDL_strdup(rawString);

        // Don't forget to call `ReleaseStringUTFChars` when you're done.
        env->ReleaseStringUTFChars(string, rawString);
    }

    /* Run the application code! */
    int status;
    status = SDL_main(stringCount + 1, argv);

    /* Do not issue an exit or the whole application will terminate instead of just the SDL thread */
    /* exit(status); */
}
}
#endif /* __ANDROID__ */

/* vi: set ts=4 sw=4 expandtab: */
