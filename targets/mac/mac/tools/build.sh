#!/bin/sh

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: executable name
# $5: configuration (debug or release)
# $6: name of target to build

modulesubdir=$1
basedir=$2
targetname=$3
exename=$4
configuration=$5
buildtargetname=$6

if [ "$configuration" != "debug" ] && [ "$configuration" != "release" ]; then
	echo "Invalid configuration '$configuration'" >&2
	exit 1
fi

if [ ! -d $basedir ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

hash xcodebuild 2>/dev/null || { echo  "xcodebuild was not found" >&2; exit 1; }
hash ruby 2>/dev/null || { echo "ruby was not found" >&2; exit 1; }

rubyxcodebuildtest=$(gem list xcodeproj -i)

if [ "$rubyxcodebuildtest" != "true" ]; then
	echo "ruby gem xcodebuild not found" >&2
	exit 1
fi

cd $basedir

if  [ ! -d "./generated/mac" ]; then
	echo "The premake command does not appear to have been successfully applied to this project and platform" >&2
	exit 1
fi

if [ "$configuration" == "debug" ]; then
	xcodebuild -workspace "./generated/mac/$targetname.xcworkspace" -scheme "$buildtargetname" -configuration Debug
else
	xcodebuild -workspace "./generated/mac/$targetname.xcworkspace" -scheme "$buildtargetname" -configuration Release
fi

if [ ! $? -eq 0 ]; then
	echo "Failed to build for Mac" >&2
	exit 1
fi

cd $basedir
exit 0
