require 'xcodeproj'

def add_file_refs(xcproj, parent_group, group_name, file_paths)
	group = xcproj.new(Xcodeproj::Project::Object::PBXGroup)
	group.name = group_name
	parent_group << group

	file_paths.each do |path|
		next unless File::exists?(path) && File::ftype(path) == 'file'
		ref = xcproj.new(Xcodeproj::Project::Object::PBXFileReference)
		ref.name = File.basename(path)
		ref.path = path
		group << ref
	end
	return group
end

def add_copy_bundle(xcproj, target_name, file_refs)
	xcproj.targets.each do |target|
		next unless target_name === target.name

		copy_bundle_resources = target.resources_build_phase
		file_refs.each do |ref|
			copy_bundle_resources.add_file_reference(ref)
		end
	end
end

def main
	xcproj_path = ARGV.shift
	target_name = ARGV.shift
	group_name = 'resources'
	files = ARGV

	xcproj = Xcodeproj::Project.new(xcproj_path)
	xcproj.initialize_from_file

	group = add_file_refs(xcproj, xcproj.main_group, group_name, files)
	add_copy_bundle(xcproj, target_name, group.children)

	xcproj.save
end

if $0 == __FILE__
	main
end

# Om ein får feil med ascii_plist_annotation i configuration_list.rb:
#
# https://github.com/CocoaPods/Xcodeproj/issues/462
#
# Endr funksjonen til:
#
#def ascii_plist_annotation
#	unless target.nil?
#		" Build configuration list for #{target.isa} \"#{target}\" "
#	else
#		" Build configuration list for <deleted target> "
#	end
#end
