function karhu_notifyInitDone()
{
	karhu_startAudio();
}

function karhu_afterInit()
{
	spinnerElement.className += ' spinner-play';
	statusElement.innerHTML = '▶';
	canvasElement.style.display = 'none';
	buttonElement.addEventListener('click', karhu_startMain);
}

function karhu_startMain()
{
	spinnerElement.className = '';
	statusElement.innerHTML = '';
	spinnerElement.style.display = 'none';
	spinnerElement.style.display = 'block';
	canvasElement.style.display = 'block';
	buttonElement.removeEventListener('click', karhu_startMain);

	window.requestAnimationFrame(function() {
		window.requestAnimationFrame(function() {
			Module.ccall('karhu_web_main');
		});
	});
}
