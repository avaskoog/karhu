---
-- emcc.lua
-- Provides Emscripten-specific configuration strings.
-- Copyright (c) 2002-2015 Jason Perkins and the Premake project
---

	local p = premake

	p.tools.emcc = {}
	local emcc = p.tools.emcc

	local project = p.project
	local clang = premake.tools.clang
	local config = p.config


--
-- Returns list of C preprocessor flags for a configuration.
--

	function emcc.getcppflags(cfg)
		local flags = clang.getcppflags(cfg)
		return flags
	end


--
-- Returns list of C compiler flags for a configuration.
--
	function emcc.getcflags(cfg)
		local flags = clang.getcflags(cfg)
		return flags
	end

	function emcc.getwarnings(cfg)
		local flags = clang.getwarnings(cfg)
		return flags
	end


--
-- Returns list of C++ compiler flags for a configuration.
--
	function emcc.getcxxflags(cfg)
		local flags = clang.getcxxflags(cfg)
		return flags
	end


--
-- Decorate defines for the Emscripten command line.
--

	function emcc.getdefines(defines)
		local flags = clang.getdefines(defines)
		return flags
	end

	function emcc.getundefines(undefines)
		local flags = clang.getundefines(undefines)
		return flags
	end


--
-- Returns a list of forced include files, decorated for the compiler
-- command line.
--
-- @param cfg
--    The project configuration.
-- @return
--    An array of force include files with the appropriate flags.
--

	function emcc.getforceincludes(cfg)
		local flags = clang.getforceincludes(cfg)
		return flags
	end


--
-- Decorate include file search paths for the Emscripten command line.
--

	function emcc.getincludedirs(cfg, dirs, sysdirs)
		local flags = clang.getincludedirs(cfg, dirs, sysdirs)
		return flags

	end

--
-- Return a list of decorated rpaths
--

	function emcc.getrunpathdirs(cfg, dirs)
		local result = {}

		if not ((cfg.system == premake.MACOSX)
				or (cfg.system == premake.LINUX)) then
			return result
		end

		local rpaths = {}

		-- User defined runpath search paths
		for _, fullpath in ipairs(cfg.runpathdirs) do
			local rpath = path.getrelative(cfg.buildtarget.directory, fullpath)
			if not (table.contains(rpaths, rpath)) then
				table.insert(rpaths, rpath)
			end
		end

		-- Automatically add linked shared libraries path relative to target directory
		for _, sibling in ipairs(config.getlinks(cfg, "siblings", "object")) do
			if (sibling.kind == premake.SHAREDLIB) then
				local fullpath = sibling.linktarget.directory
				local rpath = path.getrelative(cfg.buildtarget.directory, fullpath)
				if not (table.contains(rpaths, rpath)) then
					table.insert(rpaths, rpath)
				end
			end
		end

		for _, rpath in ipairs(rpaths) do
			if (cfg.system == premake.MACOSX) then
				rpath = "@loader_path/" .. rpath
			elseif (cfg.system == premake.LINUX) then
				rpath = iif(rpath == ".", "", "/" .. rpath)
				rpath = "$$ORIGIN" .. rpath
			end

			table.insert(result, "-Wl,-rpath,'" .. rpath .. "'")
		end

		return result
	end

--
-- Return a list of LDFLAGS for a specific configuration.
--

	function emcc.ldsymbols(cfg)
		-- OS X has a bug, see http://lists.apple.com/archives/Darwin-dev/2006/Sep/msg00084.html
		return iif(cfg.system == premake.MACOSX, "-Wl,-x", "-s")
	end

	emcc.ldflags = {
		flags = {
			NoClosureCompiler = "",  -- TODO...
			NoMinifyJavaScript = "",
			IgnoreDynamicLinking = "",
		},
		kind = {
			HTMLPage = function(cfg)
				-- ???
			end,
			SharedLib = function(cfg)
				local r = { iif(cfg.system == premake.MACOSX, "-dynamiclib", "-shared") }
				if cfg.system == "windows" and not cfg.flags.NoImportLib then
					table.insert(r, '-Wl,--out-implib="' .. cfg.linktarget.relpath .. '"')
				end
				return r
			end,
			WindowedApp = function(cfg)
				if cfg.system == premake.WINDOWS then return "-mwindows" end
			end,
		},
		linkeroptimize = {  -- TODO...
			Off = "",
			Simple = "",
			On = "",
			Unsafe = ""
		},
		typedarrays = {
			None = "",
			Parallel = "",
			Shared = ""
		}
	}

	function emcc.getldflags(cfg)
		local flags = config.mapFlags(cfg, emcc.ldflags)
		return flags
	end



--
-- Return a list of decorated additional libraries directories.
--

	emcc.libraryDirectories = {
		architecture = {
			x86 = function (cfg)
				local r = {}
				if cfg.system ~= premake.MACOSX then
					table.insert (r, "-L/usr/lib32")
				end
				return r
			end,
			x86_64 = function (cfg)
				local r = {}
				if cfg.system ~= premake.MACOSX then
					table.insert (r, "-L/usr/lib64")
				end
				return r
			end,
		},
		system = {
			wii = "-L$(LIBOGC_LIB)",
		}
	}

	function emcc.getLibraryDirectories(cfg)
		local flags = clang.getLibraryDirectories(cfg)
		return flags
	end



--
-- Return the list of libraries to link, decorated with flags as needed.
--

	function emcc.getlinks(cfg, systemonly, nogroups)
		local flags = clang.getlinks(cfg, systemOnly, nogroups)
		return flags
	end


--
-- Returns makefile-specific configuration rules.
--

	function emcc.getmakesettings(cfg)
		local flags = clang.getmakesettings(cfg)
		return flags
	end


--
-- Retrieves the executable command name for a tool, based on the
-- provided configuration and the operating environment.
--
-- @param cfg
--    The configuration to query.
-- @param tool
--    The tool to fetch, one of "cc" for the C compiler, "cxx" for
--    the C++ compiler, or "ar" for the static linker.
-- @return
--    The executable command name for a tool, or nil if the system's
--    default value should be used.
--

	emcc.tools = {
		cc = "emcc",
		cxx = "em++",
		ar = "emar"
	}

	function emcc.gettoolname(cfg, tool)
		return emcc.tools[tool]
	end