#!/bin/sh

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: executable name
# $5: configuration (debug or release)

modulesubdir=$1
basedir=$2
targetname=$3
exename=$4
configuration=$5

if [ "$configuration" != "debug" ] && [ "$configuration" != "release" ]; then
	echo "Invalid configuration '$configuration'" >&2
	exit 1
fi

if [ ! -d $basedir ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

if [ ! -d $KARHU_HOME ]; then
	echo "Invalid KARHU_HOME directory" >&2
	exit 1
fi

emsdkdir="$KARHU_HOME/$modulesubdir/sys/emsdk-portable"

if  [ ! -d $emsdkdir ]; then
	echo "Cannot find Emscripten SDK for this system in KARHU_HOME" >&2
	exit 1
fi

cd "$emsdkdir"

. ./emsdk_env.sh

if [ ! $? -eq 0 ]; then
	echo "Failed to set up Emscripten environment" >&2
	exit 1
fi

cd "$basedir"

if [ ! -d "./generated/web" ]; then
	echo "The premake command does not appear to have been successfully applied to this project and platform" >&2
	exit 1
fi

cd "$basedir/generated/web"
emmake make config=$configuration #--em-config "$emsdkdir"

if [ ! $? -eq 0 ]; then
	echo "Failed to prebuild for web" >&2
	exit 1
fi

# Load target settings.
settingspath="$(pwd)/data/$targetname.json"

# Check if this is an app target.
targettype=$(karhudata json --act format --file "$settingspath" --data string type)
if [ "app" = "$targettype" ]; then
	# Time to zip and copy over app resources.
	cd "$basedir"

	# Check that there are any resources at all.
	# TODO: Should it be an error if not?
	resdir="$basedir/res"
	if [ -d "$resdir" ]; then
		echo "Packing zip!"

		cd "$resdir"

		# Remove any old zip first to ensure a fresh one.
		zippath="$basedir/generated/web/res.zip"
		rm "$basedir/generated/web/res.zip" 2>/dev/null
		zip -r9 $zippath . -x "**.DS_Store"
	fi

	cd "$basedir/generated/web"

	# Start building the arguments to pass to the compiler.
	buildcommand=""

	# We handle linking manually for web.
	linkvals=$(karhudata json --act format --file "$settingspath" --data array links --separators "|")
	IFS="|" read -r -a linkarr <<< "$linkvals"
	for element in "${linkarr[@]}"
	do
		if [ "OpenGL" = "$element" ]; then
			buildcommand+="-s FULL_ES3=1 "
			buildcommand+="-s USE_WEBGL2=1 "
		elif [ "SDL2" = "$element" ]; then
			buildcommand+="-s USE_SDL=2 "
		elif [ "SDL2_net" = "$element" ]; then
			buildcommand+="-s USE_SDL_NET=2 "
		elif [ "karhu" = "$element" ]; then
			buildcommand+="$KARHU_HOME/lib/karhu/build/web/bin/karhu/$configuration/karhu.bc "
		fi
	done

	jsfile="karhu_with_play_button.js"

	defvals=$(karhudata json --act format --file "$settingspath" --data array defines --separators "|")
	IFS="|" read -r -a defarr <<< "$defvals"
	for element in "${defarr[@]}"
	do
		if [ "KARHU_WEB_WITHOUT_PLAY_BUTTON" = "$element" ]; then
			jsfile="karhu_without_play_button.js"
		fi
	done

	# TODO: Allow WASM to be manually enabled for Karhu web builds?
	#buildcommand+="-s WASM=0 "

	# Apparently have to do this on more recent versions of Emscripten.
	# See FAQ: https://emscripten.org/docs/getting_started/FAQ.html#why-do-i-get-typeerror-module-something-is-not-a-function
	buildcommand+="-s EXTRA_EXPORTED_RUNTIME_METHODS=['ccall'] "

	# Add the target's bytecode.
	# TODO: Figure out how to deal with links in the same project.
	buildcommand+="../../build/web/bin/$targetname/$configuration/$targetname.bc "

	# Minimum memory needed to work properly.
	# Might want to make this a customisable setting later.
	buildcommand+="-s TOTAL_MEMORY=536870912 "

	# Add support for running through --emrun.
	# Possible this should be a debug-only setting.
	buildcommand+="--emrun "

	# Possible this needs to be conditional.
	buildcommand+="--std=c++14 "

	# Embed the resource pack.
	buildcommand+="--embed-file res.zip "

	# Might want to remove this later, or limit to debug.
	buildcommand+="-s DISABLE_EXCEPTION_CATCHING=0 "

	buildcommand+="-s RETAIN_COMPILER_SETTINGS=1 "

	# We seem to be getting AngelScript problems without this.
	# buildcommand+="-s DEMANGLE_SUPPORT=1 "

	if [ "$configuration" != "debug" ]; then
		buildcommand+="-O2 "
	fi

	customshelldir="../../style/web"
	customshellpath="$customshelldir/index.html"

	if [ -f "$customshellpath" ]; then
		buildcommand+="--shell-file $customshellpath "
	fi

	builddirectory="../../build/web/bin/$targetname/$configuration"

	# Finally the output file.
	# Maybe the output name should be the target name rather than index.
	buildcommand+="-o $builddirectory/index.html "

	echo "em++ $buildcommand"

	# Mostly for debugging. Perhaps leave out later.
	export EM_BUILD_VERBOSE=3

	# To fix issues with running out of memory.
	# See: https://github.com/emscripten-core/emscripten/issues/1018#issuecomment-28128732
	# TODO: Figure out how to use V8 instead of node because this didn't fix the memory thing?
	# https://gist.github.com/raphaelr/2578732
	#export EMSCRIPT_MAX_CHUNK_SIZE=4194304
	#export EMCC_CORES=4

	# Try to build!
	em++ $buildcommand

	if [ ! $? -eq 0 ]; then
		echo "Failed to build for web" >&2
		exit 1
	fi

	# Move everything over to a subdirectory with the app name.

	finaldirectory="$builddirectory/$exename"

	if [ -d "$finaldirectory" ]; then
		rm -rf "$finaldirectory"
	fi

	mkdir "$finaldirectory"

	# Set the JS file in a copy of the index in the new destination directory.
	sed "s/KARHU_SCRIPT/$jsfile/g" "$builddirectory/index.html" > "$finaldirectory/index.html"

	# Copy the JS file over.
	cp "$KARHU_HOME/$modulesubdir/templates/$jsfile" "$finaldirectory/$jsfile"

	# Remove the old index.
	rm "$builddirectory/index.html"

	# Move the other two as they are.
	mv "$builddirectory/index.html.mem" "$finaldirectory/index.html.mem"
	mv "$builddirectory/index.js" "$finaldirectory/index.js"

	if [ -f "$builddirectory/index.wasm" ]; then
		mv "$builddirectory/index.wasm" "$finaldirectory/index.wasm"
	fi

	# Copy the logo over.
	cp "$KARHU_HOME/res/logo-transp.svg" "$finaldirectory/_karhu-logo.svg" 2>/dev/null

	# Copy any custom files over.
	if [ -d "$customshelldir" ]; then
		cp -R -n "$customshelldir/" "$finaldirectory"
	fi
fi

cd "$basedir"
exit 0

