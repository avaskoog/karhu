#!/bin/sh

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: executable name
# $5: full identifier
# $6: configuration (debug or release)
# $7: whether to wait to capture output [y/n]
# $8: arbitrary hint
# $…: arguments

modulesubdir=$1
basedir=$2
targetname=$3
exename=$4
identifier=$5
configuration=$6
captureoutput=$7
hint=$8

if [ "$configuration" != "debug" ] && [ "$configuration" != "release" ]; then
	echo "Invalid configuration '$configuration'" >&2
	exit 1
fi

if [ ! -d $basedir ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

if [ ! -d $KARHU_HOME ]; then
	echo "Invalid KARHU_HOME directory" >&2
	exit 1
fi

emsdkdir="$KARHU_HOME/$modulesubdir/sys/emsdk-portable"

if  [ ! -d $emsdkdir ]; then
	echo "Cannot find Emscripten SDK for this system in KARHU_HOME" >&2
	exit 1
fi

cd $emsdkdir
source ./emsdk_env.sh

if [ ! $? -eq 0 ]; then
	echo "Failed to set up Emscripten environment" >&2
	exit 1
fi

fullexepath="./build/web/bin/$targetname/$configuration/$exename/index.html"

cd $basedir

if [ ! -f "$fullexepath" ]; then
	echo "The build command does not appear to have been successfully applied to this project, platform and configuration" >&2
	exit 1
fi

argstring="";
if [ "$#" -gt "8" ]; then
	for i in "${@:9}"; do
		argstring+=" \"$i\""
	done
fi

echo "emrun --browser chrome \"$fullexepath\" $argstring"
eval emrun --browser chrome \"$fullexepath\" $argstring
PID=$!

if [ ! $? -eq 0 ]; then
	echo "Failed to run" >&2
	exit 1
fi

sleep 3
#kill $PID

# TODO: Use npx kill-port to kill server/client if busy? Also websockify.

exit 0
