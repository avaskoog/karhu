#!/bin/sh

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: configuration (debug or release)
# $5: name of target to build
# $6: arbitrary hint

modulesubdir=$1
basedir=$2
targetname=$3
configuration=$4
buildtargetname=$5
hint=$6

if [ ! -d "$basedir" ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

cd "$basedir"

cd generated/web
premake5 gmake

if [ ! $? -eq 0 ]; then
	echo "Failed to generate web build files with premake" >&2
	exit 1
fi

exit 0
