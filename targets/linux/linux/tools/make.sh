#!/bin/sh

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: configuration (debug or release)
# $5: name of target to build
# $6: arbitrary hint

modulesubdir=$1
basedir=$2
targetname=$3
configuration=$4
buildtargetname=$5
hint=$6

if [ ! -d "$basedir" ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

hash premake5 2>/dev/null || { echo  "premake5 was not found" >&2; exit 1; }

cd "$basedir"

# ICON INFO: https://icons8.com/articles/choosing-the-right-size-and-format-for-icons/

cd generated/linux
premake5 gmake

if [ ! $? -eq 0 ]; then
	echo "Failed to generate Linux build files with premake" >&2
	exit 1
fi

exit 0
