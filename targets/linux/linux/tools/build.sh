#!/bin/sh

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: executable name
# $5: configuration (debug or release)

modulesubdir=$1
basedir=$2
targetname=$3
exename=$4
configuration=$5

if [ "$configuration" != "debug" ] && [ "$configuration" != "release" ]; then
	echo "Invalid configuration '$configuration'" >&2
	exit 1
fi

if [ ! -d $basedir ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

hash make 2>/dev/null || { echo  "make was not found" >&2; exit 1; }
hash g++ 2>/dev/null || { echo  "g++ was not found" >&2; exit 1; }

cd "$basedir"

if  [ ! -d "./generated/linux" ]; then
	echo "The premake command does not appear to have been successfully applied to this project and platform" >&2
	exit 1
fi

cd "./generated/linux/"
make config=$configuration

if [ ! $? -eq 0 ]; then
	echo "Failed to build for Linux" >&2
	exit 1
fi

# Linux has an issue with spaces, so make sure the .exe gets the right name.
configurationdir=""
if [ $configuration = "debug" ]; then
	configurationdir="debug"
else
	configurationdir="release"
fi

# Time to zip and copy over app resources.
cd "$basedir"
targettype=$(karhudata json --act format --file generated/linux/data/$targetname.json --data string type)
if [ "app" = "$targettype" ]; then
	# Check that there are any resources at all.
	# TODO: Should it be an error if not?
	resdir="$basedir/res"
	if [ -d "$resdir" ]; then
		echo "Packing zip!"

		cd "$resdir"

		# Remove any old zip first to ensure a fresh one.
		zippath="$basedir/build/linux/bin/$targetname/$configuration/res.zip"
		rm "$basedir/generated/linux/res.zip" 2>/dev/null
		zip -r9 $zippath .
	fi
fi

cd "$basedir"
mv "./build/linux/bin/$targetname/$configurationdir/$targetname" "./build/linux/bin/$targetname/$configurationdir/$exename" 2>/dev/null

exit 0
