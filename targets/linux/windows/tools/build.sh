#!/bin/sh

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: executable name
# $5: configuration (debug or release)

modulesubdir=$1
basedir=$2
targetname=$3
exename=$4
configuration=$5

if [ "$configuration" != "debug" ] && [ "$configuration" != "release" ]; then
	echo "Invalid configuration '$configuration'" >&2
	exit 1
fi

hash make 2>/dev/null || { echo  "make was not found" >&2; exit 1; }

cd "$basedir"

if  [ ! -d "./generated/windows" ]; then
	echo "The premake command does not appear to have been successfully applied to this project and platform" >&2
	exit 1
fi

cd "./generated/windows"
make config=$configuration

if [ ! $? -eq 0 ]; then
	echo "Failed to build for Windows" >&2
	exit 1
fi

cd "$basedir"

# Check if this is an app target.
targettype=$(karhudata json --act format --file generated/windows/data/$targetname.json --data string type)
if [ $targettype = "app" ]; then
	cp $KARHU_HOME/$modulesubdir/sys/sdl2/SDL2.dll build/windows/bin/$targetname/$configuration/
	cp $KARHU_HOME/$modulesubdir/sys/glew/glew32.dll build/windows/bin/$targetname/$configuration/
	cp $KARHU_HOME/$modulesubdir/sys/pthread/libwinpthread-1.dll build/windows/bin/$targetname/$configuration/

	# Time to zip and copy over app resources.
	# Check that there are any resources at all.
	# TODO: Should it be an error if not?
	resdir="$basedir/res"
	if [ -d "$resdir" ]; then
		echo "Packing zip!"

		cd "$resdir"

		# Remove any old zip first to ensure a fresh one.
		zippath="$basedir/build/windows/bin/$targetname/$configuration/res.zip"
		rm "$basedir/generated/windows/res.zip" 2>/dev/null
		zip -r9 $zippath .
	fi
fi

cd "$basedir"
mv "./build/windows/bin/$targetname/$configuration/$targetname" "./build/windows/bin/$targetname/$configuration/$exename.exe" 2>/dev/null

exit 0
