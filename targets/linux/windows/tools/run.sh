#!/bin/bash

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: executable name
# $5: full identifier
# $6: configuration (debug or release)
# $7: whether to wait to capture output [y/n]
# $8: arbitrary hint
# $…: arguments

modulesubdir=$1
basedir=$2
targetname=$3
exename=$4
identifier=$5
configuration=$6
captureoutput=$7
hint=$8

if [ "$configuration" != "debug" ] && [ "$configuration" != "release" ]; then
	echo "Invalid configuration $configuration" >&2
fi

if [ ! -d $basedir ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

hash screen 2>/dev/null || { echo  "screen was not found" >&2; exit 1; }
hash wine 2>/dev/null || { echo  "wine was not found" >&2; exit 1; }

cd "$basedir"

fullexepath="./build/windows/bin/$targetname/$configuration/$exename.exe"

if [ ! -f "$fullexepath" ]; then
	echo "The build command does not appear to have been successfully applied to this project, platform and configuration" >&2
	exit 1
fi

argstring="";
if [ "$#" -gt "8" ]; then
	for i in ${@:9}; do
		argstring+=" \"$i\""
	done
fi

if [ $captureoutput = "y" ]; then
	#SDL_AUDIODRIVER="directsound" eval WINEDEBUG=err-all,warn-all,fixme-all,trace-all wine \"$fullexepath\" $argstring
	eval WINEDEBUG=err-all,warn-all,fixme-all,trace-all wine \"$fullexepath\" $argstring
else
	#SDL_AUDIODRIVER="directsound" eval screen -d -m wine \"$fullexepath\" $argstring
	eval screen -d -m wine \"$fullexepath\" $argstring
fi

exit 0
