#!/bin/bash

# $1: build module subdirectory of this script
# $2: working directory
# $3: target name
# $4: configuration (debug or release)
# $5: name of target to build
# $6: arbitrary hint

modulesubdir=$1
basedir=$2
targetname=$3
configuration=$4
buildtargetname=$5
hint=$6

if [ ! -d "$basedir" ]; then
	echo "Invalid project directory" >&2
	exit 1
fi

hash premake5 2>/dev/null || { echo  "premake5 was not found" >&2; exit 1; }

cd "$basedir"

cd "./generated/windows"

# We need to set the icon.
# More info: https://fragglet.livejournal.com/4448.html
# More info: https://stackoverflow.com/questions/708238/how-do-i-add-an-icon-to-a-mingw-gcc-compiled-executable
# More info: https://www.randomhacks.co.uk/how-to-convert-a-png-to-ico-file-ubuntu-linux/
# TODO: Use imagemagick to generate different sizes.
# INFO: https://icons8.com/articles/choosing-the-right-size-and-format-for-icons/
# INFO: https://docs.microsoft.com/sv-se/windows/desktop/uxguide/vis-icons
# INFO: https://docs.microsoft.com/sv-se/windows/uwp/design/shell/tiles-and-notifications/app-assets#asset-types
# They should all be put into one .ico? If icotool can't do it, imagemagick can, e.g.
# CODE: convert icon-16.png icon-32.png icon-64.png icon-128.png icon.ico
# MORE: https://stackoverflow.com/a/16922387/5012939
# Check if this is an app target.
targettype=$(karhudata json --act format --file data/$targetname.json --data string type)
if [ "app" = "$targettype" ]; then
	# Generate the icons and splashes!
	imgtool="$KARHU_HOME/tools/scripts/unix/karhuimg.sh"
	echo "Generating icons..."
	filelist=$("$imgtool" $basedir windows icon "$KARHU_HOME/$modulesubdir/templates.karhutarget.json")
	echo "Icons generated!"

	# icotool doesn't like quotes so we have to remove them.
	IFS=" " read -r -a filearr <<< "$filelist"
	filelist=""
	for i in "${filearr[@]}"; do
		filelist+=${i//\"/}
		filelist+=" "
	done

	# Create the Icon.ico file.
	icotool -c -o Icon.ico $filelist

	# Copy over the Icon.rc file.
	cp -a "$KARHU_HOME/$modulesubdir/templates/icons/." "./"
fi

premake5 gmake

if [ ! $? -eq 0 ]; then
	echo "Failed to generate cross-compile Windows build files on Linux with premake" >&2
	exit 1
fi

exit 0
