
newoption
{
	trigger = "ios",
	description = "Generates files for iOS on Mac (only)"
}

newoption
{
	trigger     = "web",
	description = "Generates files for web"
}

newoption
{
	trigger     = "windows",
	description = "Cross-compiles for Windows on a different system"
}

if _OPTIONS["ios"] and os.get() ~= "macosx" then
	print("Error: iOS option is only available on Mac.")
	os.exit(1);
end

if _OPTIONS["windows"] and os.get() ~= "linux" then
	print("Error: cross-compile Windows option is only available on Linux.")
	os.exit(1);
end

-- Determines name of subfolders under build/ and generated/
if _OPTIONS["web"] then
	nameSystemTarget = "web"
elseif _OPTIONS["ios"] then
	nameSystemTarget = "ios"
elseif _OPTIONS["windows"] then
	nameSystemTarget = "windows"
elseif os.get() == "macosx" then
	nameSystemTarget = "mac"
else
	nameSystemTarget = os.get()
end

workspace(KARHU_IDENTIFIER_PROJ)
	location     "generated/%{nameSystemTarget}"
	language     "C++"

	if not _OPTIONS["windows"] then
		architecture "x86_64"
	else
		architecture "x86"
	end

	targetdir "build/%{nameSystemTarget}/bin/%{prj.name}/%{cfg.longname}"
	objdir    "build/%{nameSystemTarget}/obj/%{prj.name}/%{cfg.longname}"

	configurations{"Debug", "Release"}

	if not _OPTIONS["web"] then
		flags{"C++14"}
	end

	if _OPTIONS["web"] then
		buildoptions{"-std=c++1y", "-x c++"}
		toolset "emcc"
		gccprefix ""
	end

	sysincludedirs{"src", "src/karhu/lib", "src/karhu/game/lib", "/usr/local/include"}

	filter{"configurations:Debug"}
		symbols "On"
		defines {"DEBUG"}

	filter{"configurations:Release"}
		optimize "On"

	filter{"system:linux"}
		includedirs{"/usr/include/SDL2"} -- Unfortunately have to do this for SDL_net

		-- Windows cross-compile
		if _OPTIONS["windows"] then
			linkoptions{"-static-libstdc++", "-static-libgcc"}

			includedirs{"/usr/x86_64-w64-mingw32/include"}
			libdirs{"lib/sdl2/mingw", "lib/glew/mingw", "/usr/i686-w64-mingw32/lib"}
			defines{"SDL_MAIN_HANDLED=1"}

			toolset      "gcc"
			buildoptions{"-std=c++1y"}
			gccprefix "i686-w64-mingw32-"
		end

	filter{"system:macosx"}
		-- Mac and iOS but not web
		if not _OPTIONS["web"] then
			includedirs{"/usr/local/include/SDL2"} -- Unfortunately have to do this for SDL_net
			sysincludedirs{"/usr/local/include"}
			buildoptions{"-I/usr/local/include"}
			toolset "clang"
		end

		-- Mac
		if not _OPTIONS["ios"] then
			syslibdirs{"/usr/local/lib"}
			linkoptions{"-L/usr/local/lib"}

			xcodebuildsettings
			{
				["ARCHS"] = "$(ARCHS_STANDARD)",
				["SDKROOT"] = "macosx",
				["CLANG_CXX_LANGUAGE_STANDARD"] = "gnu++14",
				["CLANG_CXX_LIBRARY"] = "libc++"
			}
		-- iOS
		elseif not _OPTIONS["web"] then
			xcodebuildsettings
			{
				["ARCHS"] = "$(ARCHS_STANDARD)",
				["SDKROOT"] = "iphoneos",
				["TARGETED_DEVICE_FAMILY"] = "1,2",
				["IPHONEOS_DEPLOYMENT_TARGET"] = "8.1"
			}
		end

project(KARHU_IDENTIFIER_PROJ)
	files{"src/**.cpp", "src/**.c"}
	excludes{"src/karhu/tool/**"}
	
	if _OPTIONS["web"] then
		excludes "src/karhu/game/lib/SDL2_net/**"
		targetname "%{prj.name}.bc"
		kind "None"
	else
		kind "StaticLib"

		filter{"system:macosx"}
			sysincludedirs{"/usr/local/include"}

			if _OPTIONS["ios"] then
				xcodebuildsettings
				{
					["GCC_INPUT_FILETYPE"] = "sourcecode.cpp.objcpp"
				}
			end
	end
