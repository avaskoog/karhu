const int _JOINTCOUNT_MAX = 64;
const int _WEIGHTCOUNT_PER_SET = 4;

uniform int _animate;
uniform mat4 _joints[_JOINTCOUNT_MAX];

out vec3 _fpos;
out vec4 _fcol;
out vec3 _fnormal;
out vec2 _fUV;
out mat4 _fmodel;

// No support for integer vector uniforms, so we need to do this.
int index(float i) { return int(ceil(i - 0.45)); }

void main()
{
	vec4 positionFinal = vec4(_vpos, 1.0);
	vec4 normalFinal = vec4(_vnormal, 0.0);
	
	if (_animate >= 1)
	{
		mat4 matFinal = mat4(0.0);
		
		for (int i = 0; i < _WEIGHTCOUNT_PER_SET; ++ i)
			matFinal += _joints[index(_vjoints0[i])] * _vweights0[i];
		
		if (_animate >= 2)
		{
			for (int i = 0; i < _WEIGHTCOUNT_PER_SET; ++ i)
				matFinal += _joints[index(_vjoints1[i])] * _vweights1[i];
			
			if (_animate >= 3)
			{
				for (int i = 0; i < _WEIGHTCOUNT_PER_SET; ++ i)
					matFinal += _joints[index(_vjoints2[i])] * _vweights2[i];
				
				if (_animate >= 4)
					for (int i = 0; i < _WEIGHTCOUNT_PER_SET; ++ i)
						matFinal += _joints[index(_vjoints3[i])] * _vweights3[i];
			}
		}
		
		positionFinal = matFinal * vec4(_vpos, 1.0);
		normalFinal = matFinal * vec4(_vnormal, 0.0);
	}

	gl_Position = _proj * _view * _model * positionFinal;
	_fpos = positionFinal.xyz;
	_fcol = _vcol;
	_fnormal = normalFinal.xyz;
	_fUV = _vUV;
	_fmodel = _model;
}
