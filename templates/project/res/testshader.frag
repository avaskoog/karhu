in vec3 _fpos;
in vec4 _fcol;
in vec3 _fnormal;
in vec2 _fUV;
in mat4 _fmodel;

out vec4 _col;

uniform sampler2D _texMain;

void main()
{
	mat3 matNormals = transpose(inverse(mat3(_fmodel)));
	vec3 normal = normalize(matNormals * _fnormal);
	
	vec3 dirLight = vec3(0.2, -0.6, 0.2);
	vec3 pos      = vec3(_fmodel * vec4(_fpos, 1.0));
	
	float bright = dot(normal, normalize(-dirLight));
//	bright /= length(surfToLight) * length(normal);
	bright = clamp(bright, 0.0, 1.0);
	
	float factor = 4.0;
	bright = floor(bright * factor) / factor;
	
	float ambient = 0.3;
	bright = clamp(bright + ambient, 0.0, 1.0);

	_col.rgb = texture(_texMain, _fUV).rgb;
	_col.a = 1.0;
	_col.rgb *= bright;
}
