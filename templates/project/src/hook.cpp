/**
 * The de/serialisation must be implemented in the source file.
 * The JSON file that will be de/serialised to from is res/_hook.karhudat.json.
 */

#include "hook.hpp"
#include <karhu/res/resser.hpp>

#if KARHU_EDITOR_SERVER_SUPPORTED
#include <karhu/app/edt/Editor.hpp>
#endif

namespace game
{
	using namespace karhu;
	
	bool Hook::registerTypes(app::App &app)
	{
		return true;
	}
	
	bool Hook::registerSystemsAndListeners(app::App &app)
	{
		return true;
	}
	
	bool Hook::registerScripting(app::App &app)
	{
		return true;
	}
	
	#if KARHU_EDITOR_SERVER_SUPPORTED
	bool Hook::registerEditors(app::App &app, edt::Editor &editor)
	{
//		if (!editor.registerEditor<ClassDerivingFromIEditor>(true, "Title"))
//			return false;
	
		return true;
	}
	#endif

	/*
	 * Startup initialisation after main application initialisation.
	 * Should return whether initialisation was successful, as the
	 * application will terminate upon failure.
	 */
	bool Hook::init
	(
		app::App              &app,
		res::IDResource const  startscene,
		int                    argc,
		char                  *argv[]
	)
	{
		return true;
	}

	/*
	 * Called every frame before the ECS systems are updated.
	 */
	void Hook::updateBeforeECS(app::App &app, float const dt, float const dtFixed, float const timestep)
	{
	}

	/*
	 * Called every frame in the middle of the update steps of the
	 * ECS systems, before the render system is updated but after
	 * physics and such.
	 */
	void Hook::updateBeforeRender(app::App &app, float const dt, float const dtFixed, float const timestep)
	{
	}

	/*
	 * Called every frame after the ECS systems are updated.
	 */
	void Hook::updateAfterECS(app::App &app, float const dt, float const dtFixed, float const timestep)
	{
	}
	
	void Hook::performSerialise(conv::Serialiser &ser) const
	{
		// ser << karhuIN(variableToSerialise);
		// ser << karhuIN("different-name", variableToSerialise);
		// << can be chained!
	}

	void Hook::performDeserialise(conv::Serialiser const &ser)
	{
		// ser >> karhuOUT(variableToDeserialise);
		// ser >> karhuOUTn("different-name", variableToDeserialise);
		// >> can be chained!
	}
}
