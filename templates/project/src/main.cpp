#include <karhu/app/main.hpp>
#include <karhu/app/App.hpp>
#include <karhu/app/backend/Backend.hpp>
#include KARHU_BACKEND_PATH_HEADER
#include "hook.hpp"

karhuMAIN
{
	using namespace karhu;
	
	app::implementation::Backend backend;
	
	game::Hook hook;
	
	app::App app
	{
		KARHU_APPLICATION_NAME,
		KARHU_IDENTIFIER_EXT,
		KARHU_IDENTIFIER_ORG,
		KARHU_IDENTIFIER_PROJ,
		backend,
		hook
	};
	
	if (!app.init(argc, argv))
		return -1;

	app.run();

	return 0;
}
