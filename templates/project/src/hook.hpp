/*
 * This file is used to set up the application as desired.
 * Feel free to leave everything unimplemented but do not remove any function.
 */

#ifndef GAME_HOOK_H_
	#define GAME_HOOK_H_

	#include <karhu/app/App.hpp>
	#include <karhu/app/edt/support.hpp>
	#include <karhu/res/Resource.hpp>
	#include <karhu/conv/serialise.hpp>

	namespace game
	{
		using namespace karhu;
		
		class Hook : public app::Hook
		{
			public:
				bool registerTypes(app::App &app) override;
				bool registerSystemsAndListeners(app::App &) override;
				bool registerScripting(app::App &) override;
			
				#if KARHU_EDITOR_SERVER_SUPPORTED
				bool registerEditors(app::App &, edt::Editor &) override;
				#endif
				
				bool init
				(
					app::App              &app,
					res::IDResource const  startscene,
					int                    argc,
					char                  *argv[]
				) override;
		
				void updateBeforeECS(app::App &app, float const dt, float const dtFixed, float const timestep) override;
				void updateBeforeRender(app::App &app, float const dt, float const dtFixed, float const timestep) override;
				void updateAfterECS(app::App &app, float const dt, float const dtFixed, float const timestep) override;
			
			protected:
				void performSerialise(conv::Serialiser &) const override;
				void performDeserialise(conv::Serialiser const &) override;
			
			private:
				// Variables to de/serialise here.
				// Note that App's userdataLocal methods also serialise into here.
		};
	}
#endif
